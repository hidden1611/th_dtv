package th.dtv;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class ChooseListDialog extends AlertDialog {
    private LinearLayout mChooseLinearLayout;
    private CustomListView mChooseList;
    private ChooseListAdapter mChooseListAdapter;
    private OnItemClickListener mChooseListItemClickListener;
    private List<String> mChooseStringList;
    private Context mContext;
    int mHighLightIndex;
    int mSelectedId;
    int mTextResId;
    int mTitleResId;
    private TextView mTitleTextView;


    /* renamed from: th.dtv.ChooseListDialog.2 */
    class C00302 implements OnItemClickListener {
        C00302() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            switch (position) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    if (ChooseListDialog.this.mSelectedId == 0) {
                    }
                default:
            }
        }
    }

    /* renamed from: th.dtv.ChooseListDialog.3 */
    class C00313 implements OnItemSelectedListener {
        C00313() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            ChooseListDialog.this.mChooseListAdapter.setSelectItem(position);
            ChooseListDialog.this.mSelectedId = position;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class ChooseListAdapter extends BaseAdapter {
        private Context cont;
        private LayoutInflater mInflater;
        private int selectItem;

        class ViewHolder {
            ImageView icon;
            TextView text;

            ViewHolder() {
            }
        }

        public ChooseListAdapter(Context context) {
            this.cont = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return ChooseListDialog.this.mChooseStringList != null ? ChooseListDialog.this.mChooseStringList.size() : 0;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void setSelectItem(int position) {
            this.selectItem = position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.choose_list_dialog_list_item, null);
                holder = new ViewHolder();
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.text = (TextView) convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.text.setText((CharSequence) ChooseListDialog.this.mChooseStringList.get(position));
            holder.text.setTextColor(-1);
            if (position == ChooseListDialog.this.mHighLightIndex) {
                holder.icon.setImageResource(R.drawable.check_pressed);
            } else {
                holder.icon.setImageResource(R.drawable.check_default);
            }
            return convertView;
        }
    }


    public ChooseListDialog(Context context, int titleResId, ArrayList<String> textList, int highlightIndex, OnItemClickListener listener) {
        super(context, R.style.MyDialog);
        this.mContext = null;
        this.mChooseStringList = null;
        this.mChooseListAdapter = null;
        this.mSelectedId = 0;
        this.mTitleResId = 0;
        this.mTextResId = 0;
        this.mHighLightIndex = 0;
        this.mContext = context;
        Log.d("ChooseListDialog", " highlightIndex " + highlightIndex);
        this.mTitleResId = titleResId;
        this.mChooseStringList = textList;
        this.mHighLightIndex = highlightIndex;
        this.mChooseListItemClickListener = listener;
        if (this.mChooseListItemClickListener == null) {
            this.mChooseListItemClickListener = new C00302();
        }
    }

    public ChooseListDialog(Context context) {
        super(context);
        this.mContext = null;
        this.mChooseStringList = null;
        this.mChooseListAdapter = null;
        this.mSelectedId = 0;
        this.mTitleResId = 0;
        this.mTextResId = 0;
        this.mHighLightIndex = 0;
        this.mContext = context;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_list_dialog);
        this.mTitleTextView = (TextView) findViewById(R.id.choose_title_textview);
        this.mTitleTextView.setText(this.mTitleResId);
        this.mChooseList = (CustomListView) findViewById(R.id.choose_listview);
        if (this.mTextResId > 0) {
            this.mChooseStringList = new ArrayList();
            for (String secStr : this.mContext.getResources().getStringArray(this.mTextResId)) {
                this.mChooseStringList.add(secStr);
            }
        }
        this.mChooseListAdapter = new ChooseListAdapter(this.mContext);
        this.mChooseList.setAdapter(this.mChooseListAdapter);
        this.mChooseList.setOnItemClickListener(this.mChooseListItemClickListener);
        this.mChooseList.setOnItemSelectedListener(new C00313());
        if (this.mChooseStringList.size() > 0) {
            this.mChooseLinearLayout = (LinearLayout) findViewById(R.id.choose_linearlayout);
            LayoutParams linearParams = (LayoutParams) this.mChooseLinearLayout.getLayoutParams();
            linearParams.height = this.mChooseStringList.size() < 11 ? (this.mChooseStringList.size() * 48) * (SETTINGS.getWindowHeight() / 720) : (SETTINGS.getWindowHeight() / 720) * 480;
            this.mChooseLinearLayout.setLayoutParams(linearParams);
            this.mChooseList.setCustomSelection(this.mHighLightIndex);
        }
    }
}
