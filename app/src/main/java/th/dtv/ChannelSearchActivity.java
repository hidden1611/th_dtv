package th.dtv;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_t_area;
import th.dtv.mw_data.search_status_dvbs_blind_scan_params;
import th.dtv.mw_data.search_status_dvbs_blind_scan_progress;
import th.dtv.mw_data.search_status_dvbs_params;
import th.dtv.mw_data.search_status_dvbt_params;
import th.dtv.mw_data.search_status_result;
import th.dtv.mw_data.search_status_update_channel_list;
import th.dtv.mw_data.tuner_signal_status;

public class ChannelSearchActivity extends DtvBaseActivity {
    private dvb_t_area areaInfo;
    private boolean bChangetoAll;
    private boolean bChangetoNew;
    private boolean bGotoPlay;
    private boolean bOKKey;
    TextView blind_tuner_info;
    private TextView dvb_t_tuner_info;
    private DyWTEDia dywteDig;
    boolean flag;
    private Handler mHandler;
    private Handler mwMsgHandler;
    ProgressBar prg_Proress;
    ProgressBar prg_signalQuality;
    ProgressBar prg_signalStrength;
    private dvb_s_sat satInfo;
    SearchStatusDia searchStatusDig;
    int search_type;
    int system_type;
    TextView txv_progress;
    List<TextView> txv_radioChannels;
    TextView txv_signalQuality;
    TextView txv_signalStrength;
    TextView txv_status;
    List<TextView> txv_tvChannels;

    /* renamed from: th.dtv.ChannelSearchActivity.1 */
    class C00251 extends Handler {
        C00251() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    System.out.println("MSG_GO_TO_PLAY");
                    if (ChannelSearchActivity.this.searchStatusDig != null && ChannelSearchActivity.this.searchStatusDig.isShowing()) {
                        ChannelSearchActivity.this.searchStatusDig.dismiss();
                        ChannelSearchActivity.this.searchStatusDig = null;
                    }
                    if (ChannelSearchActivity.this.search_type == 2) {
                        ChannelSearchActivity.this.finish();
                    } else {
                        ChannelSearchActivity.this.gotoPlayActivity();
                    }
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    System.out.println("MSG_BACK");
                    if (ChannelSearchActivity.this.searchStatusDig != null && ChannelSearchActivity.this.searchStatusDig.isShowing()) {
                        ChannelSearchActivity.this.searchStatusDig.dismiss();
                        ChannelSearchActivity.this.searchStatusDig = null;
                    }
                    ChannelSearchActivity.this.finish();
                default:
            }
        }
    }

    private class DyWTEDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        TextView mContent;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        /* renamed from: th.dtv.ChannelSearchActivity.DyWTEDia.1 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.deldia_title);
            this.mContent = (TextView) findViewById(R.id.delete_content);
            this.mTitle.setText(R.string.tips);
            this.mContent.setText(R.string.dywte);
            this.mBtnCancel.requestFocus();
        }

        public DyWTEDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.mContent = null;
            this.mContext = context;
        }
    }

    class MWmessageHandler extends Handler {
        int search_sat_count;

        public MWmessageHandler(Looper looper) {
            super(looper);
            this.search_sat_count = 1;
        }

        public void handleMessage(Message msg) {
            int event_type = (msg.what >> 16) & 65535;
            int sub_event_type = msg.what & 65535;
            switch (event_type) {
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    TextView textView;
                    StringBuilder stringBuilder;
                    Object[] objArr;
                    String s;
                    switch (sub_event_type) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            ((RelativeLayout) ChannelSearchActivity.this.findViewById(R.id.RelativeLayout_Loading)).setVisibility(View.INVISIBLE);
                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_SEARCH_START : " + event_type + " " + sub_event_type);
                            if (ChannelSearchActivity.this.search_type == 0) {
                                ChannelSearchActivity.this.txv_status.setText(ChannelSearchActivity.this.getResources().getString(R.string.Search));
                            }
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
//                            search_status_update_channel_list paramsInfo = msg.obj;
//                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_UPDATE_CHANNEL_LIST : " + event_type + " " + sub_event_type);
//                            if (paramsInfo != null) {
//                                int i;
//                                for (i = 0; i < paramsInfo.tv_channels_name.length; i++) {
//                                    textView = (TextView) ChannelSearchActivity.this.txv_tvChannels.get(i);
//                                    stringBuilder = new StringBuilder();
//                                    objArr = new Object[1];
//                                    objArr[0] = Integer.valueOf(paramsInfo.tv_channels_number[i]);
//                                    textView.setText(stringBuilder.append(String.format("%03d.   ", objArr)).append(paramsInfo.tv_channels_name[i]).toString());
//                                }
//                                for (i = 0; i < paramsInfo.radio_channels_name.length; i++) {
//                                    textView = (TextView) ChannelSearchActivity.this.txv_radioChannels.get(i);
//                                    stringBuilder = new StringBuilder();
//                                    objArr = new Object[1];
//                                    objArr[0] = Integer.valueOf(paramsInfo.radio_channels_number[i]);
//                                    textView.setText(stringBuilder.append(String.format("%03d.   ", objArr)).append(paramsInfo.radio_channels_name[i]).toString());
//                                }
//                            }
                        case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                            String s_22k;
                            String s_pol;
                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_UPDATE_DVB_S_BLIND_SCAN_TUNNING_INFO : " + event_type + " " + sub_event_type);
                            int onoff_22K = msg.arg1;
                            int pol = msg.arg2;
                            if (onoff_22K == 0) {
                                s_22k = ChannelSearchActivity.this.getResources().getString(R.string.off);
                            } else {
                                s_22k = ChannelSearchActivity.this.getResources().getString(R.string.on);
                            }
                            if (pol == 0) {
                                s_pol = ChannelSearchActivity.this.getResources().getString(R.string.Vertical);
                            } else {
                                s_pol = ChannelSearchActivity.this.getResources().getString(R.string.Horizontal);
                            }
                            System.out.println("onoff_22K " + onoff_22K);
                            System.out.println("pol " + pol);
                            ChannelSearchActivity.this.blind_tuner_info.setText(ChannelSearchActivity.this.getResources().getString(R.string._22KHz) + " : " + s_22k + "      " + ChannelSearchActivity.this.getResources().getString(R.string.Polarization) + " : " + s_pol);
                        case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
//                            search_status_dvbs_blind_scan_progress paramsInfo2 = msg.obj;
//                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_UPDATE_DVB_S_BLIND_SCAN_PROGRESS : " + event_type + " " + sub_event_type);
//                            if (paramsInfo2 != null) {
//                                if (paramsInfo2.locked) {
//                                    ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
//                                } else {
//                                    ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                                }
//                                if (this.search_sat_count > 1) {
//                                    ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo2.progress_total);
//                                    ChannelSearchActivity.this.txv_progress.setText(paramsInfo2.progress_total + "%");
//                                    return;
//                                }
//                                ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo2.progress);
//                                ChannelSearchActivity.this.txv_progress.setText(paramsInfo2.progress + "%");
//                            }
                        case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
//                            search_status_dvbs_blind_scan_params paramsInfo3 = msg.obj;
//                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_UPDATE_DVB_S_BLIND_SCAN_PARAMS_INFO : " + event_type + " " + sub_event_type);
//                            if (paramsInfo3 != null) {
//                                StringBuilder append;
//                                Object[] objArr2;
//                                if (paramsInfo3.sym > 0) {
//                                    append = new StringBuilder().append(paramsInfo3.sat_name).append("    ");
//                                    objArr2 = new Object[2];
//                                    objArr2[0] = Integer.valueOf(paramsInfo3.frq);
//                                    objArr2[1] = Integer.valueOf(paramsInfo3.sym);
//                                    s = append.append(String.format("%05dMHz / %05dKbps / ", objArr2)).toString();
//                                    if (paramsInfo3.pol == 0) {
//                                        s = s + "V";
//                                    } else {
//                                        s = s + "H";
//                                    }
//                                } else {
//                                    append = new StringBuilder().append(paramsInfo3.sat_name).append("    ");
//                                    objArr2 = new Object[1];
//                                    objArr2[0] = Integer.valueOf(paramsInfo3.frq);
//                                    s = append.append(String.format("%05dMHz   ", objArr2)).append(ChannelSearchActivity.this.getResources().getString(R.string.Search)).toString();
//                                }
//                                this.search_sat_count = paramsInfo3.search_sat_count;
//                                if (paramsInfo3.search_sat_count > 1) {
//                                    ChannelSearchActivity.this.txv_status.setText((paramsInfo3.search_sat_pos + 1) + "/" + paramsInfo3.search_sat_count + "   " + s);
//                                } else {
//                                    ChannelSearchActivity.this.txv_status.setText(s);
//                                }
//                            }
                        case MW.f0xe3564d8 /*5*/:
//                            search_status_dvbs_params paramsInfo4 = msg.obj;
//                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_UPDATE_DVB_S_PARAMS_INFO : " + event_type + " " + sub_event_type);
//                            if (paramsInfo4 != null) {
//                                if (paramsInfo4.pol == 0) {
//                                    s = "V";
//                                } else {
//                                    s = "H";
//                                }
//                                if (paramsInfo4.search_sat_count > 1) {
//                                    textView = ChannelSearchActivity.this.txv_status;
//                                    stringBuilder = new StringBuilder().append(paramsInfo4.search_sat_pos + 1).append("/").append(paramsInfo4.search_sat_count).append("   ").append(paramsInfo4.sat_name);
//                                    objArr = new Object[2];
//                                    objArr[0] = Integer.valueOf(paramsInfo4.frq);
//                                    objArr[1] = Integer.valueOf(paramsInfo4.sym);
//                                    stringBuilder = stringBuilder.append(String.format("   %dMHz / %dKbps / ", objArr)).append(s);
//                                    objArr = new Object[2];
//                                    objArr[0] = Integer.valueOf(paramsInfo4.search_tp_pos + 1);
//                                    objArr[1] = Integer.valueOf(paramsInfo4.search_tp_count);
//                                    textView.setText(stringBuilder.append(String.format("    %d/%d", objArr)).toString());
//                                } else {
//                                    textView = ChannelSearchActivity.this.txv_status;
//                                    stringBuilder = new StringBuilder().append(paramsInfo4.sat_name);
//                                    objArr = new Object[2];
//                                    objArr[0] = Integer.valueOf(paramsInfo4.frq);
//                                    objArr[1] = Integer.valueOf(paramsInfo4.sym);
//                                    stringBuilder = stringBuilder.append(String.format("   %dMHz / %dKbps / ", objArr)).append(s);
//                                    objArr = new Object[2];
//                                    objArr[0] = Integer.valueOf(paramsInfo4.search_tp_pos + 1);
//                                    objArr[1] = Integer.valueOf(paramsInfo4.search_tp_count);
//                                    textView.setText(stringBuilder.append(String.format("    %d/%d", objArr)).toString());
//                                }
//                                System.out.println("paramsInfo.progress:" + paramsInfo4.progress);
//                                System.out.println("paramsInfo.progress_total:" + paramsInfo4.progress_total);
//                                System.out.println("paramsInfo.search_sat_pos:" + paramsInfo4.search_sat_pos);
//                                System.out.println("paramsInfo.search_sat_count:" + paramsInfo4.search_sat_count);
//                                if (ChannelSearchActivity.this.search_type == 1) {
//                                    ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                                    if (paramsInfo4.search_sat_count > 1) {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo4.progress_total);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo4.progress_total + "%");
//                                    } else {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo4.progress);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo4.progress + "%");
//                                    }
//                                }
//                                if (ChannelSearchActivity.this.search_type == 2) {
//                                    ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                                    if (paramsInfo4.search_tp_count > 1) {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo4.progress_total);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo4.progress_total + "%");
//                                        return;
//                                    }
//                                    ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo4.progress);
//                                    ChannelSearchActivity.this.txv_progress.setText(paramsInfo4.progress + "%");
//                                }
//                            }
                        case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
//                            search_status_dvbt_params paramsInfo5 = msg.obj;
//                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO : " + event_type + " " + sub_event_type);
//                            if (paramsInfo5 != null) {
//                                TextView textView2;
//                                StringBuilder append2;
//                                String str;
//                                String str2;
//                                Object[] objArr3;
//                                int i2;
//                                String str3;
//                                if (paramsInfo5.search_area_count > 1) {
//                                    textView2 = ChannelSearchActivity.this.txv_status;
//                                    append2 = new StringBuilder().append(paramsInfo5.search_area_pos + 1).append("/").append(paramsInfo5.search_area_count).append("   ").append(SETTINGS.get_area_string_by_index(ChannelSearchActivity.this, paramsInfo5.search_area_index)).append("   CH - ").append(paramsInfo5.channel_number);
//                                    str2 = "  / %d KHz / %d MHz %d/%d  %s";
//                                    objArr3 = new Object[5];
//                                    objArr3[0] = Integer.valueOf(paramsInfo5.frq);
//                                    objArr3[1] = Integer.valueOf(paramsInfo5.bandwidth);
//                                    objArr3[2] = Integer.valueOf(paramsInfo5.search_tp_pos + 1);
//                                    objArr3[3] = Integer.valueOf(paramsInfo5.search_tp_count);
//                                    if (paramsInfo5.front_end_type == 4) {
//                                        i2 = paramsInfo5.dvb_t2_plp_id;
//                                        str3 = ")";
//                                        str = "(PLP :" + i2 + r18;
//                                    } else {
//                                        str = "";
//                                    }
//                                    objArr3[4] = str;
//                                    textView2.setText(append2.append(String.format(str2, objArr3)).toString());
//                                } else {
//                                    textView2 = ChannelSearchActivity.this.txv_status;
//                                    append2 = new StringBuilder().append(SETTINGS.get_area_string_by_index(ChannelSearchActivity.this, paramsInfo5.search_area_index)).append("   CH - ").append(paramsInfo5.channel_number);
//                                    str2 = "  / %d KHz / %d MHz  %d/%d  %s";
//                                    objArr3 = new Object[5];
//                                    objArr3[0] = Integer.valueOf(paramsInfo5.frq);
//                                    objArr3[1] = Integer.valueOf(paramsInfo5.bandwidth);
//                                    objArr3[2] = Integer.valueOf(paramsInfo5.search_tp_pos + 1);
//                                    objArr3[3] = Integer.valueOf(paramsInfo5.search_tp_count);
//                                    if (paramsInfo5.front_end_type == 4) {
//                                        i2 = paramsInfo5.dvb_t2_plp_id;
//                                        str3 = ")";
//                                        str = "(PLP :" + i2 + r18;
//                                    } else {
//                                        str = "";
//                                    }
//                                    objArr3[4] = str;
//                                    textView2.setText(append2.append(String.format(str2, objArr3)).toString());
//                                }
//                                System.out.println("paramsInfo.progress:" + paramsInfo5.progress);
//                                System.out.println("paramsInfo.progress_total:" + paramsInfo5.progress_total);
//                                System.out.println("paramsInfo.search_area_pos:" + paramsInfo5.search_area_pos);
//                                System.out.println("paramsInfo.search_area_count:" + paramsInfo5.search_area_count);
//                                if (ChannelSearchActivity.this.search_type == 1) {
//                                    ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                                    if (paramsInfo5.search_area_count > 1) {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo5.progress_total);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo5.progress_total + "%");
//                                    } else {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo5.progress);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo5.progress + "%");
//                                    }
//                                }
//                                if (ChannelSearchActivity.this.search_type == 2) {
//                                    ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                                    if (paramsInfo5.search_tp_count > 1) {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo5.progress_total);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo5.progress_total + "%");
//                                    } else {
//                                        ChannelSearchActivity.this.prg_Proress.setProgress(paramsInfo5.progress);
//                                        ChannelSearchActivity.this.txv_progress.setText(paramsInfo5.progress + "%");
//                                    }
//                                }
//                                if (ChannelSearchActivity.this.dvb_t_tuner_info.getVisibility() != 0) {
//                                    ChannelSearchActivity.this.dvb_t_tuner_info.setVisibility(View.VISIBLE);
//                                }
//                                textView2 = ChannelSearchActivity.this.dvb_t_tuner_info;
//                                append2 = new StringBuilder().append(ChannelSearchActivity.this.getResources().getString(R.string.Ant_Power)).append(" : ");
//                                if (SETTINGS.get_ant_power_status() == 0) {
//                                    str = ChannelSearchActivity.this.getResources().getString(R.string.off);
//                                } else {
//                                    str = ChannelSearchActivity.this.getResources().getString(R.string.on);
//                                }
//                                textView2.setText(append2.append(str).toString());
//                            }
                        case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_SAVE_DATA_START : " + event_type + " " + sub_event_type);
                            ChannelSearchActivity.this.SearchStatus(sub_event_type);
                        case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_SAVE_DATA_END : " + event_type + " " + sub_event_type);
                            ChannelSearchActivity.this.SearchStatus(sub_event_type);
                        case MW.SEARCH_STATUS_SEARCH_END /*9*/:
//                            search_status_result paramsInfo6 = msg.obj;
//                            Log.d("ChannelSearchActivity", " mwEventCallback : MW.SEARCH_STATUS_SEARCH_END : " + event_type + " " + sub_event_type);
//                            ChannelSearchActivity.this.prg_Proress.setProgress(100);
//                            ChannelSearchActivity.this.txv_progress.setText("100%");
//                            if (paramsInfo6 != null && !ChannelSearchActivity.this.flag) {
//                                if (ChannelSearchActivity.this.searchStatusDig == null) {
//                                    ChannelSearchActivity.this.searchStatusDig = new SearchStatusDia(ChannelSearchActivity.this, R.style.MyDialog);
//                                    ChannelSearchActivity.this.searchStatusDig.show();
//                                }
//                                ChannelSearchActivity.this.searchStatusDig.setMyTiTle(ChannelSearchActivity.this.getResources().getString(R.string.search_result));
//                                ChannelSearchActivity.this.searchStatusDig.showBtn(true);
//                                if (paramsInfo6.tv_service_count == 0 && paramsInfo6.radio_service_count == 0) {
//                                    ChannelSearchActivity.this.bGotoPlay = false;
//                                    ChannelSearchActivity.this.searchStatusDig.setMyText(ChannelSearchActivity.this.getResources().getString(R.string.no_program));
//                                } else {
//                                    ChannelSearchActivity.this.bGotoPlay = true;
//                                    ChannelSearchActivity.this.searchStatusDig.setMyText(String.format(ChannelSearchActivity.this.getResources().getString(R.string.TV) + "  :  %d       " + ChannelSearchActivity.this.getResources().getString(R.string.Radio) + "  :  %d", new Object[]{Integer.valueOf(paramsInfo6.tv_service_count), Integer.valueOf(paramsInfo6.radio_service_count)}));
//                                }
//                                ChannelSearchActivity.this.autoClose(3000, paramsInfo6);
//                            }
                        default:
                    }
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
//                    tuner_signal_status paramsInfo7 = msg.obj;
//                    if (paramsInfo7 != null) {
//                        Log.d("ChannelSearchActivity", " mwEventCallback : MW.EVENT_TYPE_TUNER_STATUS : " + paramsInfo7.locked + " " + paramsInfo7.error + " " + sub_event_type + " " + paramsInfo7.strength + " " + paramsInfo7.quality);
//                        if (paramsInfo7.locked) {
//                            if (ChannelSearchActivity.this.search_type == 2) {
//                                ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
//                                ChannelSearchActivity.this.prg_Proress.setProgress(0);
//                            }
//                            ChannelSearchActivity.this.prg_signalStrength.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
//                            ChannelSearchActivity.this.prg_signalQuality.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
//                        } else {
//                            if (ChannelSearchActivity.this.search_type == 2) {
//                                ChannelSearchActivity.this.prg_Proress.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                                ChannelSearchActivity.this.prg_Proress.setProgress(0);
//                            }
//                            ChannelSearchActivity.this.prg_signalStrength.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                            ChannelSearchActivity.this.prg_signalQuality.setProgressDrawable(ChannelSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
//                        }
//                        ChannelSearchActivity.this.prg_signalStrength.setProgress(paramsInfo7.strength);
//                        ChannelSearchActivity.this.prg_signalQuality.setProgress(paramsInfo7.quality);
//                        if (paramsInfo7.error) {
//                            ChannelSearchActivity.this.txv_signalStrength.setText("I2C");
//                            ChannelSearchActivity.this.txv_signalQuality.setText("Error");
//                            return;
//                        }
//                        ChannelSearchActivity.this.txv_signalStrength.setText(paramsInfo7.strength + "%");
//                        ChannelSearchActivity.this.txv_signalQuality.setText(paramsInfo7.quality + "%");
//                    }
                default:
            }
        }
    }

    class MyTask extends TimerTask {
        private search_status_result result;

        public MyTask(search_status_result result) {
            this.result = result;
        }

        public void run() {
            if (!ChannelSearchActivity.this.bOKKey) {
                if (this.result.radio_service_count > 0 || this.result.tv_service_count > 0) {
                    ChannelSearchActivity.this.mHandler.sendMessageDelayed(ChannelSearchActivity.this.mHandler.obtainMessage(0), 100);
                } else {
                    ChannelSearchActivity.this.mHandler.sendMessageDelayed(ChannelSearchActivity.this.mHandler.obtainMessage(1), 100);
                }
            }
        }
    }

    public class SearchStatusDia extends Dialog {
        TextView deldia_title;
        Button dialog_button_ok;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTextView;
        RelativeLayout rl_btn;


        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.search_result_dialog);
            this.mTextView = (TextView) findViewById(R.id.delete_content);
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.deldia_title.setText(ChannelSearchActivity.this.getResources().getString(R.string.save));
            this.mTextView.setText(ChannelSearchActivity.this.getResources().getString(R.string.savedata) + ChannelSearchActivity.this.getResources().getString(R.string.ellipsis));
            this.rl_btn = (RelativeLayout) findViewById(R.id.rl_btn);
            this.dialog_button_ok = (Button) findViewById(R.id.dialog_button_ok);
            this.dialog_button_ok.requestFocus();
            if (ChannelSearchActivity.this.dywteDig != null) {
                ChannelSearchActivity.this.dywteDig.dismiss();
            }
        }

        public void showBtn(boolean isShow) {
            if (isShow) {
                this.rl_btn.setVisibility(View.VISIBLE);
            } else {
                this.rl_btn.setVisibility(View.GONE);
            }
        }

        public void setMyText(String string) {
            this.mTextView.setText(string);
        }

        public void setMyTiTle(String string) {
            this.deldia_title.setText(string);
        }

        public SearchStatusDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mTextView = null;
            this.deldia_title = null;
            this.rl_btn = null;
            this.dialog_button_ok = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            return false;
        }
    }

    public ChannelSearchActivity() {
        this.txv_tvChannels = new ArrayList();
        this.txv_radioChannels = new ArrayList();
        this.flag = false;
        this.searchStatusDig = null;
        this.dywteDig = null;
        this.bChangetoAll = false;
        this.bChangetoNew = false;
        this.satInfo = new dvb_s_sat();
        this.areaInfo = new dvb_t_area();
        this.dvb_t_tuner_info = null;
        this.bGotoPlay = false;
        this.bOKKey = false;
        this.mHandler = new C00251();
    }

    private void ShowExitDialog() {
        this.dywteDig = new DyWTEDia(this, R.style.MyDialog);
        this.dywteDig.show();
        this.dywteDig.getWindow().addFlags(2);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    ShowExitDialog();
                    break;
                case MW.RET_INVALID_TELETEXT_COLOR /*24*/:
                case MW.RET_INVALID_DATA_LENGTH /*25*/:
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void SearchStatus(int Event) {
        if (this.searchStatusDig == null) {
            this.searchStatusDig = new SearchStatusDia(this, R.style.MyDialog);
            this.searchStatusDig.show();
        }
        if (Event == 7) {
            this.searchStatusDig.setMyText(getResources().getString(R.string.savedata) + getResources().getString(R.string.ellipsis));
        } else if (Event == 8) {
            this.searchStatusDig.setMyText(getResources().getString(R.string.saveend));
        }
    }

    public void autoClose(int time, search_status_result result) {
        new Timer().schedule(new MyTask(result), (long) time);
    }

    private void gotoPlayActivity() {
        Intent intent = new Intent();
        intent.setFlags(67108864);
        intent.addFlags(268435456);
        intent.putExtra("change_to_all", this.bChangetoAll);
        intent.putExtra("change_to_new", this.bChangetoNew);
        intent.setClass(this, DtvMainActivity.class);
        startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.channel_search);
        this.txv_status = (TextView) findViewById(R.id.TextView_Status);
        this.blind_tuner_info = (TextView) findViewById(R.id.blind_tuner_info);
        this.dvb_t_tuner_info = (TextView) findViewById(R.id.dvb_t_tuner_info);
        this.prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
        this.prg_signalStrength.setMax(100);
        this.prg_signalStrength.setProgress(0);
        this.prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
        this.prg_signalQuality.setMax(100);
        this.prg_signalQuality.setProgress(0);
        this.prg_Proress = (ProgressBar) findViewById(R.id.ProgressBar_Progress);
        this.prg_Proress.setMax(100);
        this.prg_Proress.setProgress(0);
        this.txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
        this.txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);
        this.txv_progress = (TextView) findViewById(R.id.TextView_Progress_Percent);
        this.txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_1));
        this.txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_2));
        this.txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_3));
        this.txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_4));
        this.txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_5));
        this.txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_1));
        this.txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_2));
        this.txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_3));
        this.txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_4));
        this.txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_5));
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        this.flag = false;
        Log.d("ChannelSearchActivity", " onCreate");
    }

    protected void onStart() {
        super.onStart();
        Log.d("ChannelSearchActivity", " onStart");
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("ChannelSearchActivity", " onNewIntent");
    }

    protected void onPause() {
        super.onPause();
        if (this.system_type == 0) {
            MW.search_dvb_s_stop();
        } else {
            MW.search_dvb_t_stop();
        }
        if (this.searchStatusDig != null) {
            this.searchStatusDig.dismiss();
        }
        enableMwMessageCallback(null);
        Log.d("ChannelSearchActivity", " onPause");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d("ChannelSearchActivity", " onRestart");
    }

    protected void onResume() {
        int i;
        Log.d("ChannelSearchActivity", " onResume start");
        super.onResume();
        SETTINGS.send_led_msg("SCAN");
        enableMwMessageCallback(this.mwMsgHandler);
        this.system_type = getIntent().getIntExtra("system_type", 0);
        this.search_type = getIntent().getIntExtra("search_type", 0);
        ArrayList<Integer> search_region_list = new ArrayList();
        search_region_list = getIntent().getIntegerArrayListExtra("search_sat_index");
        ArrayList<Integer> search_tp_list = new ArrayList();
        search_tp_list = getIntent().getIntegerArrayListExtra("search_tp_index");
        ((RelativeLayout) findViewById(R.id.RelativeLayout_Loading)).setVisibility(View.VISIBLE);
        if (search_region_list.size() > 1) {
            this.bChangetoAll = true;
        } else if (this.system_type == 0) {
            if (MW.db_dvb_s_get_current_sat_info(0, this.satInfo) == 0) {
                System.out.println("search_tp_list.get(0).intValue() ===" + ((Integer) search_tp_list.get(0)).intValue());
                System.out.println("satInfo.sat_index ===" + this.satInfo.sat_index);
                if (((Integer) search_region_list.get(0)).intValue() != this.satInfo.sat_index) {
                    this.bChangetoNew = true;
                }
            } else {
                System.out.println("channelsearch db_dvb_s_get_current_sat_info failed");
            }
        } else if (MW.db_dvb_t_get_current_area_info(this.areaInfo) == 0) {
            System.out.println("search_tp_list.get(0).intValue() ===" + ((Integer) search_tp_list.get(0)).intValue());
            System.out.println("areaInfo.area_index ===" + this.areaInfo.area_index);
            if (((Integer) search_region_list.get(0)).intValue() != this.areaInfo.area_index) {
                this.bChangetoNew = true;
            }
        } else {
            System.out.println("channelsearch db_dvb_t_get_current_area_info failed");
        }
        System.out.println("Region Count : " + search_region_list.size());
        int[] search_region_index_list = new int[search_region_list.size()];
        for (i = 0; i < search_region_list.size(); i++) {
            search_region_index_list[i] = ((Integer) search_region_list.get(i)).intValue();
            System.out.println("search_region_index_list: " + search_region_index_list[i]);
        }
        System.out.println("Tp Count : " + search_tp_list.size());
        int[] search_tp_index_list = new int[search_tp_list.size()];
        for (i = 0; i < search_tp_list.size(); i++) {
            search_tp_index_list[i] = ((Integer) search_tp_list.get(i)).intValue();
            System.out.println("search_tp_index_list: " + search_tp_index_list[i]);
        }
        System.out.println("search_service_type: " + SETTINGS.get_search_service_type());
        System.out.println("search_tv_type: " + SETTINGS.get_search_tv_type());
        System.out.println("search_type: " + this.search_type);
        if (this.system_type == 1) {
            MW.search_dvb_t_start(this.search_type, search_region_index_list, search_tp_index_list, 5);
        } else {
            MW.search_dvb_s_start(this.search_type, search_region_index_list, search_tp_index_list, 5);
        }
        this.prg_signalStrength.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_unlock));
        this.prg_signalQuality.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_unlock));
        this.prg_Proress.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_unlock));
        if (this.search_type == 0) {
            ((TextView) findViewById(R.id.TextView_TunerStatus_S)).setVisibility(View.INVISIBLE);
            ((TextView) findViewById(R.id.TextView_TunerStatus_Q)).setVisibility(View.INVISIBLE);
            this.prg_signalStrength.setVisibility(View.INVISIBLE);
            this.prg_signalQuality.setVisibility(View.INVISIBLE);
            this.txv_signalStrength.setVisibility(View.INVISIBLE);
            this.txv_signalQuality.setVisibility(View.INVISIBLE);
            this.prg_Proress.setProgress(0);
            this.txv_progress.setText("0%");
        } else if (this.search_type == 2) {
            this.prg_Proress.setProgress(0);
            this.txv_progress.setText("0%");
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        } else {
            this.prg_Proress.setProgress(0);
            this.txv_progress.setText("0%");
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        }
        Log.d("ChannelSearchActivity", " onResume end");
    }

    protected void onStop() {
        super.onStop();
        Log.d("ChannelSearchActivity", " onStop");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d("ChannelSearchActivity", " onDestroy");
    }
}
