package th.dtv;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import th.dtv.activity.SatelliteListActivity;
import th.dtv.activity.SelectedSatelliteListActivity;
import th.dtv.activity.TpListActivity;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.tuner_signal_status;

public class AntennaSetupActivity extends DtvBaseActivity {
    private LinearLayout ChannelNo_item;
    private LinearLayout DiseqcPort_item;
    private LinearLayout DiseqcType_item;
    private LinearLayout LNBHighFreq_item;
    private LinearLayout LNBLowFreq_item;
    private LinearLayout LNBPower_item;
    private LinearLayout LNBType_item;
    private LinearLayout Onoff22K_item;
    private LinearLayout SCRFrequency_item;
    private LinearLayout SCRPosition_item;
    private LinearLayout Satellite_item;
    ArrayList<Integer> SelSatList;
    ArrayList<Integer> SelTpList;
    private LinearLayout Transponder_item;
    private LinearLayout UnicableType_item;
    public boolean bFromManualSearch;
    Button bt_motorSettingOK;
    int currentSatIndex;
    int currentSatPos;
    int currentTpIndex;
    DigitsEditText et_SCRFrequency;
    DigitsEditText et_lnbHighFreq;
    DigitsEditText et_lnbLowFreq;
    boolean fSaveSatTpFlag;
    private int[][] lnbTypeList;
    private editOnFocusChangeListener mEditOnFocusChangeListener;
    private editTextWatcher mEditTextWatcher;
    private OnItemSelectedListener mOnSelectedListener;
    private Handler mwMsgHandler;
    int pre22KHz_ItemType;
    int preChannelNo_ItemType;
    int preDiseqcPort_ItemType;
    int preSCRPosition_ItemType;
    ProgressBar prg_signalQuality;
    ProgressBar prg_signalStrength;
    private int[] satIndexList;
    dvb_s_sat satInfo;
    private ComboLayout spn_22Khz;
    private ComboLayout spn_SCRPosition;
    private ComboLayout spn_channelNo;
    private ComboLayout spn_diseqcPort;
    private ComboLayout spn_diseqcType;
    private ComboLayout spn_lnbPower;
    private ComboLayout spn_lnbType;
    private ComboLayout spn_satellite;
    private ComboLayout spn_transponder;
    private ComboLayout spn_unicableType;
    dvb_s_tp tpInfo;
    TextView txv_signalQuality;
    TextView txv_signalStrength;

    /* renamed from: th.dtv.AntennaSetupActivity.1 */
    class C00111 implements OnItemClickListener {
        C00111() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            AntennaSetupActivity.this.currentSatPos = position;
            if (AntennaSetupActivity.this.currentSatIndex != AntennaSetupActivity.this.satIndexList[position]) {
                AntennaSetupActivity.this.currentSatIndex = AntennaSetupActivity.this.satIndexList[position];
                AntennaSetupActivity.this.RefreshSatelliteInfo(true);
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.2 */
    class C00122 implements OnItemClickListener {
        C00122() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            AntennaSetupActivity.this.satInfo.lnb_type = AntennaSetupActivity.this.lnbTypeList[position][0];
            AntennaSetupActivity.this.satInfo.lnb_low = AntennaSetupActivity.this.lnbTypeList[position][1];
            AntennaSetupActivity.this.satInfo.lnb_high = AntennaSetupActivity.this.lnbTypeList[position][2];
            AntennaSetupActivity.this.RefreshLNBType();
            AntennaSetupActivity.this.fSaveSatTpFlag = true;
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.3 */
    class C00133 implements OnItemClickListener {
        C00133() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            AntennaSetupActivity.this.satInfo.k22 = position;
            MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
            AntennaSetupActivity.this.LockCurrentTP();
            AntennaSetupActivity.this.fSaveSatTpFlag = true;
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.4 */
    class C00144 implements OnItemClickListener {
        C00144() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (AntennaSetupActivity.this.satInfo.unicable_type != position) {
                AntennaSetupActivity.this.satInfo.unicable_type = position;
                AntennaSetupActivity.this.satInfo.scr_number = 0;
                AntennaSetupActivity.this.satInfo.scr_position = 0;
                AntennaSetupActivity.this.satInfo.scr_bandPassFrequency = MW.db_get_scr_bp_frequency(AntennaSetupActivity.this.satInfo.unicable_type, AntennaSetupActivity.this.satInfo.scr_number);
                MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                AntennaSetupActivity.this.LockCurrentTP();
                AntennaSetupActivity.this.RefreshBackGround();
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.5 */
    class C00155 implements OnItemClickListener {
        C00155() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (AntennaSetupActivity.this.satInfo.lnb_power != position) {
                AntennaSetupActivity.this.satInfo.lnb_power = position;
                MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                AntennaSetupActivity.this.LockCurrentTP();
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.6 */
    class C00166 implements OnItemClickListener {
        C00166() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (AntennaSetupActivity.this.satInfo.scr_number != position) {
                AntennaSetupActivity.this.satInfo.scr_number = position;
                AntennaSetupActivity.this.satInfo.scr_bandPassFrequency = MW.db_get_scr_bp_frequency(AntennaSetupActivity.this.satInfo.unicable_type, AntennaSetupActivity.this.satInfo.scr_number);
                MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                AntennaSetupActivity.this.et_SCRFrequency.setDigitsText(Integer.toString(AntennaSetupActivity.this.satInfo.scr_bandPassFrequency));
                AntennaSetupActivity.this.LockCurrentTP();
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.7 */
    class C00177 implements OnItemClickListener {
        C00177() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (AntennaSetupActivity.this.satInfo.diseqc_type != position) {
                AntennaSetupActivity.this.satInfo.diseqc_type = position;
                AntennaSetupActivity.this.satInfo.diseqc_port = 0;
                MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                AntennaSetupActivity.this.LockCurrentTP();
                AntennaSetupActivity.this.RefreshBackGround();
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.8 */
    class C00188 implements OnItemClickListener {
        C00188() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (AntennaSetupActivity.this.satInfo.diseqc_port != position) {
                AntennaSetupActivity.this.satInfo.diseqc_port = position;
                MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                AntennaSetupActivity.this.LockCurrentTP();
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetupActivity.9 */
    class C00199 implements OnItemClickListener {
        C00199() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (AntennaSetupActivity.this.satInfo.scr_position != position) {
                AntennaSetupActivity.this.satInfo.scr_position = position;
                MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                AntennaSetupActivity.this.LockCurrentTP();
                AntennaSetupActivity.this.fSaveSatTpFlag = true;
            }
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    tuner_signal_status paramsInfo = (tuner_signal_status) msg.obj;
                    if (paramsInfo != null) {
                        if (paramsInfo.locked) {
                            AntennaSetupActivity.this.prg_signalStrength.setProgressDrawable(AntennaSetupActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
                            AntennaSetupActivity.this.prg_signalQuality.setProgressDrawable(AntennaSetupActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
                            SETTINGS.set_green_led(true);
                        } else {
                            AntennaSetupActivity.this.prg_signalStrength.setProgressDrawable(AntennaSetupActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
                            AntennaSetupActivity.this.prg_signalQuality.setProgressDrawable(AntennaSetupActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
                            SETTINGS.set_green_led(false);
                        }
                        AntennaSetupActivity.this.prg_signalStrength.setProgress(paramsInfo.strength);
                        AntennaSetupActivity.this.prg_signalQuality.setProgress(paramsInfo.quality);
                        if (paramsInfo.error) {
                            AntennaSetupActivity.this.txv_signalStrength.setText("I2C");
                            AntennaSetupActivity.this.txv_signalQuality.setText("Error");
                            return;
                        }
                        AntennaSetupActivity.this.txv_signalStrength.setText(paramsInfo.strength + "%");
                        AntennaSetupActivity.this.txv_signalQuality.setText(paramsInfo.quality + "%");
                    }
                default:
            }
        }
    }

    public class ScanActionDig extends Dialog {
        private LinearLayout channel_type_ll;
        private TextView channel_type_opt;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private LinearLayout nit_search_ll;
        private TextView nit_search_opt;
        private LinearLayout scan_type_ll;
        private TextView scan_type_opt;
        private LinearLayout service_type_ll;
        private TextView servicetype_opt;

//        /* renamed from: th.dtv.AntennaSetupActivity.ScanActionDig.1 */
//        class C00201 implements OnKeyListener {
//            C00201() {
//            }
//
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == 0) {
//                    switch (keyCode) {
//                        case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                        case MW.RET_MEMORY_ERROR /*22*/:
//                            if (ScanActionDig.this.nit_search_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.on))) {
//                                ScanActionDig.this.nit_search_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.off));
//                            } else {
//                                ScanActionDig.this.nit_search_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.on));
//                            }
//                            return true;
//                    }
//                }
//                return false;
//            }
//        }
//
//        /* renamed from: th.dtv.AntennaSetupActivity.ScanActionDig.2 */
//        class C00212 implements OnKeyListener {
//            C00212() {
//            }
//
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == 0) {
//                    switch (keyCode) {
//                        case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                        case MW.RET_MEMORY_ERROR /*22*/:
//                            if (ScanActionDig.this.channel_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.All))) {
//                                ScanActionDig.this.channel_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.FTAOnly));
//                            } else {
//                                ScanActionDig.this.channel_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.All));
//                            }
//                            return true;
//                    }
//                }
//                return false;
//            }
//        }
//
//        /* renamed from: th.dtv.AntennaSetupActivity.ScanActionDig.3 */
//        class C00223 implements OnKeyListener {
//            C00223() {
//            }
//
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == 0) {
//                    switch (keyCode) {
//                        case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                            if (ScanActionDig.this.servicetype_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.All))) {
//                                ScanActionDig.this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.Radio));
//                                return true;
//                            } else if (ScanActionDig.this.servicetype_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.Radio))) {
//                                ScanActionDig.this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.TV));
//                                return true;
//                            } else if (!ScanActionDig.this.servicetype_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.TV))) {
//                                return true;
//                            } else {
//                                ScanActionDig.this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.All));
//                                return true;
//                            }
//                        case MW.RET_MEMORY_ERROR /*22*/:
//                            if (ScanActionDig.this.servicetype_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.All))) {
//                                ScanActionDig.this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.TV));
//                                return true;
//                            } else if (ScanActionDig.this.servicetype_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.TV))) {
//                                ScanActionDig.this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.Radio));
//                                return true;
//                            } else if (!ScanActionDig.this.servicetype_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.Radio))) {
//                                return true;
//                            } else {
//                                ScanActionDig.this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.All));
//                                return true;
//                            }
//                    }
//                }
//                return false;
//            }
//        }
//
//        /* renamed from: th.dtv.AntennaSetupActivity.ScanActionDig.4 */
//        class C00234 implements OnKeyListener {
//            C00234() {
//            }
//
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == 0) {
//                    switch (keyCode) {
//                        case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                            if (ScanActionDig.this.scan_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.manual_scan))) {
//                                ScanActionDig.this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.blind_scan));
//                                ScanActionDig.this.RefreshNitSearchBg(false);
//                                return true;
//                            } else if (ScanActionDig.this.scan_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.blind_scan))) {
//                                ScanActionDig.this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.auto_scan));
//                                ScanActionDig.this.RefreshNitSearchBg(true);
//                                return true;
//                            } else if (!ScanActionDig.this.scan_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.auto_scan))) {
//                                return true;
//                            } else {
//                                ScanActionDig.this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.manual_scan));
//                                ScanActionDig.this.RefreshNitSearchBg(true);
//                                return true;
//                            }
//                        case MW.RET_MEMORY_ERROR /*22*/:
//                            if (ScanActionDig.this.scan_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.manual_scan))) {
//                                ScanActionDig.this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.auto_scan));
//                                ScanActionDig.this.RefreshNitSearchBg(true);
//                                return true;
//                            } else if (ScanActionDig.this.scan_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.auto_scan))) {
//                                ScanActionDig.this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.blind_scan));
//                                ScanActionDig.this.RefreshNitSearchBg(false);
//                                return true;
//                            } else if (!ScanActionDig.this.scan_type_opt.getText().equals(AntennaSetupActivity.this.getResources().getString(R.string.blind_scan))) {
//                                return true;
//                            } else {
//                                ScanActionDig.this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.manual_scan));
//                                ScanActionDig.this.RefreshNitSearchBg(true);
//                                return true;
//                            }
//                    }
//                }
//                return false;
//            }
//        }

        private void RefreshBg() {
            if (this.channel_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.servicetype_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.scan_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.nit_search_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
            }
            RefreshNitSearchBg(this.nit_search_opt.isFocusable());
        }

        private void RefreshNitSearchBg(boolean enabled) {
            if (enabled) {
                this.nit_search_opt.setEnabled(true);
                this.nit_search_opt.setFocusable(true);
                if (this.nit_search_opt.isFocused()) {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                    return;
                } else {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                    return;
                }
            }
            this.nit_search_opt.setEnabled(false);
            this.nit_search_opt.setFocusable(false);
            this.nit_search_ll.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.satellite_scan_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.satellite_scan_dialog, null);
            this.channel_type_ll = (LinearLayout) findViewById(R.id.channel_type_ll);
            this.service_type_ll = (LinearLayout) findViewById(R.id.service_type_ll);
            this.scan_type_ll = (LinearLayout) findViewById(R.id.scan_type_ll);
            this.nit_search_ll = (LinearLayout) findViewById(R.id.nit_search_ll);
            this.channel_type_opt = (TextView) findViewById(R.id.channel_type_opt);
            this.servicetype_opt = (TextView) findViewById(R.id.servicetype_opt);
            this.scan_type_opt = (TextView) findViewById(R.id.scan_type_opt);
            this.nit_search_opt = (TextView) findViewById(R.id.nit_search_opt);
            if (SETTINGS.get_search_nit()) {
                this.nit_search_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.on));
            } else {
                this.nit_search_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.off));
            }
            if (SETTINGS.get_search_service_type() == 0) {
                this.channel_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.All));
            } else {
                this.channel_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.FTAOnly));
            }
            if (SETTINGS.get_search_tv_type() == 0) {
                this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.All));
            } else if (SETTINGS.get_search_tv_type() == 1) {
                this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.TV));
            } else if (SETTINGS.get_search_tv_type() == 2) {
                this.servicetype_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.Radio));
            }
//            this.nit_search_opt.setOnKeyListener(new C00201());
//            this.channel_type_opt.setOnKeyListener(new C00212());
//            this.servicetype_opt.setOnKeyListener(new C00223());
            if (MW.db_dvb_s_get_tp_count(0, AntennaSetupActivity.this.currentSatIndex) == 0) {
                this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.blind_scan));
                RefreshNitSearchBg(false);
            } else {
                this.scan_type_opt.setText(AntennaSetupActivity.this.getResources().getString(R.string.manual_scan));
//                this.scan_type_opt.setOnKeyListener(new C00234());
            }
            this.scan_type_opt.requestFocus();
            RefreshBg();
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            int i2 = 2;
            if (keyEvent.getAction() != 0) {
                switch (i) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (i) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.nit_search_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.scan_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.scan_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.scan_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.channel_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.nit_search_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TYPE /*23*/:
                    boolean z;
                    int i3;
                    int i4;
                    if (this.nit_search_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.on))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (this.channel_type_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.All))) {
                        i3 = 0;
                    } else {
                        i3 = 1;
                    }
                    if (this.servicetype_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.All))) {
                        i4 = 0;
                    } else if (this.servicetype_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.TV))) {
                        i4 = 1;
                    } else if (this.servicetype_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.Radio))) {
                        i4 = 2;
                    } else {
                        i4 = 0;
                    }
                    if (this.scan_type_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.auto_scan))) {
                        i2 = 1;
                    } else if (this.scan_type_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.blind_scan))) {
                        i2 = 0;
                    } else if (!this.scan_type_opt.getText().toString().equals(AntennaSetupActivity.this.getResources().getString(R.string.manual_scan))) {
                        i2 = 0;
                    }
                    SETTINGS.set_search_service_type(i3);
                    SETTINGS.set_search_tv_type(i4);
                    SETTINGS.set_search_nit(z);
                    AntennaSetupActivity.this.bFromManualSearch = true;
                    Intent intent;
                    if (MW.db_dvb_s_get_tp_count(0, AntennaSetupActivity.this.currentSatIndex) > 0 || i2 == 0) {
                        intent = new Intent();
                        intent.setFlags(67108864);
                        intent.setClass(AntennaSetupActivity.this, ChannelSearchActivity.class);
                        intent.putExtra("system_type", 0);
                        intent.putExtra("search_type", i2);
                        AntennaSetupActivity.this.getSelSatData();
                        intent.putIntegerArrayListExtra("search_sat_index", AntennaSetupActivity.this.SelSatList);
                        AntennaSetupActivity.this.getSelTpData();
                        intent.putIntegerArrayListExtra("search_tp_index", AntennaSetupActivity.this.SelTpList);
                        AntennaSetupActivity.this.startActivity(intent);
                    } else {
                        AntennaSetupActivity.this.ShowToastInformation(AntennaSetupActivity.this.getResources().getString(R.string.add_tp), 1);
                        intent = new Intent();
                        intent.setFlags(67108864);
                        intent.putExtra("selected_sat_index", AntennaSetupActivity.this.currentSatPos);
                        intent.putExtra("selected_tp_index", 0);
                        intent.setClass(AntennaSetupActivity.this, TpListActivity.class);
                        AntennaSetupActivity.this.startActivity(intent);
                        dismiss();
                    }
                    dismiss();
                    break;
            }
            return super.onKeyDown(i, keyEvent);
        }

        public ScanActionDig(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    class editOnFocusChangeListener implements OnFocusChangeListener {
        editOnFocusChangeListener() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                ((EditText) v).selectAll();
            }
        }
    }

    private class editTextWatcher implements TextWatcher {
        private void setValue(EditText et, int value) {
            if (et == AntennaSetupActivity.this.et_lnbLowFreq) {
                if (value <= 20000) {
                    AntennaSetupActivity.this.satInfo.lnb_low = value;
                    MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                } else {
                    return;
                }
            } else if (et == AntennaSetupActivity.this.et_lnbHighFreq) {
                if (value <= 20000) {
                    AntennaSetupActivity.this.satInfo.lnb_high = value;
                    MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                } else {
                    return;
                }
            } else if (et == AntennaSetupActivity.this.et_SCRFrequency) {
                if (value <= 4095) {
                    AntennaSetupActivity.this.satInfo.scr_bandPassFrequency = value;
                    MW.db_dvb_s_set_sat_info(0, AntennaSetupActivity.this.satInfo);
                } else {
                    return;
                }
            }
            AntennaSetupActivity.this.LockCurrentTP();
            AntennaSetupActivity.this.fSaveSatTpFlag = true;
        }

        public void afterTextChanged(Editable paramEditable) {
            int value = 0;
            if (AntennaSetupActivity.this.et_lnbLowFreq.isFocused() && !AntennaSetupActivity.this.et_lnbLowFreq.hasSelection()) {
                if (AntennaSetupActivity.this.et_lnbLowFreq.getText().toString().length() > 0) {
                    value = Integer.valueOf(AntennaSetupActivity.this.et_lnbLowFreq.getText().toString()).intValue();
                }
                setValue(AntennaSetupActivity.this.et_lnbLowFreq, value);
            } else if (AntennaSetupActivity.this.et_lnbHighFreq.isFocused() && !AntennaSetupActivity.this.et_lnbHighFreq.hasSelection()) {
                if (AntennaSetupActivity.this.et_lnbHighFreq.getText().toString().length() > 0) {
                    value = Integer.valueOf(AntennaSetupActivity.this.et_lnbHighFreq.getText().toString()).intValue();
                }
                setValue(AntennaSetupActivity.this.et_lnbHighFreq, value);
            } else if (AntennaSetupActivity.this.et_SCRFrequency.isFocused() && !AntennaSetupActivity.this.et_SCRFrequency.hasSelection()) {
                if (AntennaSetupActivity.this.et_SCRFrequency.getText().toString().length() > 0) {
                    value = Integer.valueOf(AntennaSetupActivity.this.et_SCRFrequency.getText().toString()).intValue();
                }
                setValue(AntennaSetupActivity.this.et_SCRFrequency, value);
            }
        }

        public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        }

        public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        }
    }

    public AntennaSetupActivity() {
        this.lnbTypeList = new int[][]{new int[]{0, 5150, 5150}, new int[]{0, 5750, 5750}, new int[]{0, 5950, 5950}, new int[]{0, 9750, 9750}, new int[]{0, 10000, 10000}, new int[]{0, 10050, 10050}, new int[]{0, 10450, 10450}, new int[]{0, 10600, 10600}, new int[]{0, 10700, 10700}, new int[]{0, 10750, 10750}, new int[]{0, 11250, 11250}, new int[]{0, 11300, 11300}, new int[]{1, 5150, 5750}, new int[]{2, 5750, 5150}, new int[]{3, 9750, 10550}, new int[]{3, 9750, 10600}, new int[]{3, 9750, 10700}, new int[]{3, 9750, 10750}, new int[]{4, 9750, 10600}};
        this.currentSatIndex = -1;
        this.currentTpIndex = -1;
        this.currentSatPos = 0;
        this.bFromManualSearch = false;
        this.satInfo = new dvb_s_sat();
        this.tpInfo = new dvb_s_tp();
        this.fSaveSatTpFlag = false;
        this.pre22KHz_ItemType = -1;
        this.preChannelNo_ItemType = -1;
        this.preDiseqcPort_ItemType = -1;
        this.preSCRPosition_ItemType = -1;
        this.mOnSelectedListener = new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };
        this.SelSatList = new ArrayList();
        this.SelTpList = new ArrayList();
        this.mEditTextWatcher = new editTextWatcher();
        this.mEditOnFocusChangeListener = new editOnFocusChangeListener();
    }

    private void LockCurrentTP() {
        MW.tuner_dvb_s_lock_tp(0, this.currentSatIndex, this.currentTpIndex, false);
    }

    private void RefreshBackGround() {
        if (this.spn_satellite.isFocused()) {
            this.Satellite_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.Satellite_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_lnbType.isFocused()) {
            this.LNBType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.LNBType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_transponder.isFocused()) {
            this.Transponder_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else if (this.spn_transponder.isEnabled()) {
            this.Transponder_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        } else {
            this.Transponder_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        }
        if (this.spn_22Khz.isFocused()) {
            this.Onoff22K_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.Onoff22K_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_unicableType.isFocused()) {
            this.UnicableType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.UnicableType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_lnbPower.isFocused()) {
            this.LNBPower_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.LNBPower_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_channelNo.isFocused()) {
            this.ChannelNo_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.ChannelNo_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_diseqcType.isFocused()) {
            this.DiseqcType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.DiseqcType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.et_SCRFrequency.isFocused()) {
            this.SCRFrequency_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.SCRFrequency_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_diseqcPort.isFocused()) {
            this.DiseqcPort_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.DiseqcPort_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_SCRPosition.isFocused()) {
            this.SCRPosition_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.SCRPosition_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        RefreshSatelliteSpecailConfigs();
    }

    private void initLinearLayout() {
        this.Satellite_item = (LinearLayout) findViewById(R.id.Satellite_item);
        this.LNBType_item = (LinearLayout) findViewById(R.id.LNBType_item);
        this.LNBLowFreq_item = (LinearLayout) findViewById(R.id.LNBLowFreq_item);
        this.LNBHighFreq_item = (LinearLayout) findViewById(R.id.LNBHighFreq_item);
        this.Transponder_item = (LinearLayout) findViewById(R.id.Transponder_item);
        this.Onoff22K_item = (LinearLayout) findViewById(R.id.Onoff22K_item);
        this.UnicableType_item = (LinearLayout) findViewById(R.id.UnicableType_item);
        this.LNBPower_item = (LinearLayout) findViewById(R.id.LNBPower_item);
        this.ChannelNo_item = (LinearLayout) findViewById(R.id.ChannelNo_item);
        this.DiseqcType_item = (LinearLayout) findViewById(R.id.DiseqcType_item);
        this.SCRFrequency_item = (LinearLayout) findViewById(R.id.SCRFrequency_item);
        this.DiseqcPort_item = (LinearLayout) findViewById(R.id.DiseqcPort_item);
        this.SCRPosition_item = (LinearLayout) findViewById(R.id.SCRPosition_item);
    }

    private void FillSatelliteItems() {
        int satCount = MW.db_dvb_s_get_selected_sat_count(0);
        ArrayList items = new ArrayList();
        items.clear();
        this.satIndexList = new int[satCount];
        for (int i = 0; i < satCount; i++) {
            if (MW.db_dvb_s_get_selected_sat_info(0, i, this.satInfo) == 0) {
                String str = "%03d   %03d.%d    %s    %s";
                Object[] objArr = new Object[5];
                objArr[0] = Integer.valueOf(i + 1);
                objArr[1] = Integer.valueOf(this.satInfo.sat_degree_dec);
                objArr[2] = Integer.valueOf(this.satInfo.sat_degree_point);
                objArr[3] = this.satInfo.sat_position == 0 ? "E" : "W";
                objArr[4] = this.satInfo.sat_name;
                items.add(String.format(str, objArr));
                this.satIndexList[i] = this.satInfo.sat_index;
            }
        }
        this.spn_satellite.initView((int) R.string.Satellite, items, this.currentSatPos, new C00111());
    }

    private void FillLNBTypeItems() {
        int i;
        ArrayList items = new ArrayList();
        items.clear();
        for (i = 0; i < this.lnbTypeList.length; i++) {
            switch (this.lnbTypeList[i][0]) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    items.add("" + this.lnbTypeList[i][1]);
                    break;
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    items.add("" + this.lnbTypeList[i][1] + " - " + this.lnbTypeList[i][2]);
                    break;
                case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
//                    items.add(getString(R.string.Universal) + " ( " + this.lnbTypeList[i][1] + " - " + this.lnbTypeList[i][2] + " )");
                    break;
                default:
                    break;
            }
        }
        if (this.satInfo != null) {
            int initIndex = 0;
            i = 0;
            while (i < this.lnbTypeList.length) {
                if (this.lnbTypeList[i][0] == this.satInfo.lnb_type && this.lnbTypeList[i][1] == this.satInfo.lnb_low && this.lnbTypeList[i][2] == this.satInfo.lnb_high) {
                    initIndex = i;
                    this.spn_lnbType.initView((int) R.string.LNBType, items, initIndex, new C00122());
                }
                i++;
            }
            this.spn_lnbType.initView((int) R.string.LNBType, items, initIndex, new C00122());
        }
    }

    private void Fill22KHzItems() {
        ArrayList items = new ArrayList();
        items.clear();
        if (this.satInfo.lnb_type != 3) {
            this.pre22KHz_ItemType = 0;
            items.add(getString(R.string.off));
            items.add(getString(R.string.on));
        } else {
            this.pre22KHz_ItemType = 1;
            items.add(getString(R.string.Auto));
            items.add(getString(R.string.Auto));
        }
        if (this.satInfo != null) {
//            this.spn_22Khz.initView((int) R.string._22KHz, items, this.satInfo.k22, new C00133());
        }
    }

    private void FillUnicableTypeItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add("1 Sat 4*SCR");
        items.add("1 Sat 8*SCR");
        items.add("2 Sat 4*SCR");
        items.add("2 Sat 8*SCR");
        if (this.satInfo != null) {
//            this.spn_unicableType.initView((int) R.string.Unicable, items, this.satInfo.unicable_type, new C00144());
        }
    }

    private void RefreshSatelliteSpecailConfigs() {
        if (this.satInfo.lnb_type == 4) {
            this.Onoff22K_item.setVisibility(View.INVISIBLE);
            this.LNBPower_item.setVisibility(View.INVISIBLE);
            this.DiseqcType_item.setVisibility(View.INVISIBLE);
            this.DiseqcPort_item.setVisibility(View.INVISIBLE);
            this.UnicableType_item.setVisibility(View.VISIBLE);
            this.ChannelNo_item.setVisibility(View.VISIBLE);
            this.SCRFrequency_item.setVisibility(View.VISIBLE);
            this.SCRPosition_item.setVisibility(View.VISIBLE);
            this.et_lnbHighFreq.setDigitsText(Integer.toString(this.satInfo.lnb_high));
            RefreshUnicableTypeConfigs();
            return;
        }
        this.UnicableType_item.setVisibility(View.INVISIBLE);
        this.ChannelNo_item.setVisibility(View.INVISIBLE);
        this.SCRFrequency_item.setVisibility(View.GONE);
        this.SCRPosition_item.setVisibility(View.INVISIBLE);
        this.Onoff22K_item.setVisibility(View.VISIBLE);
        this.LNBPower_item.setVisibility(View.VISIBLE);
        this.DiseqcType_item.setVisibility(View.VISIBLE);
        Fill22KHzItems();
        if (this.satInfo.lnb_type == 0) {
            this.et_lnbHighFreq.setDigitsText(getString(R.string.None));
        } else {
            this.et_lnbHighFreq.setDigitsText(Integer.toString(this.satInfo.lnb_high));
        }
        if (this.satInfo.lnb_type == 3 || this.satInfo.lnb_type == 4) {
            this.spn_22Khz.setEnabled(false);
            this.spn_22Khz.setFocusable(false);
            this.Onoff22K_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        } else {
            this.spn_22Khz.setEnabled(true);
            this.spn_22Khz.setFocusable(true);
        }
        Fill22KHzItems();
        RefreshDiseqcTypeConfigs();
    }

    private void RefreshDiseqcTypeConfigs() {
        if (this.satInfo.diseqc_type <= 2) {
            this.DiseqcPort_item.setVisibility(View.VISIBLE);
            FillDiseqcPortItems();
            if (this.satInfo.diseqc_type == 0) {
                this.spn_diseqcPort.setEnabled(false);
                this.spn_diseqcPort.setFocusable(false);
                this.DiseqcPort_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                return;
            }
            this.spn_diseqcPort.setEnabled(true);
            this.spn_diseqcPort.setFocusable(true);
            return;
        }
        this.DiseqcPort_item.setVisibility(View.INVISIBLE);
    }

    private void RefreshUnicableTypeConfigs() {
        FillSCRPositionItems();
        FillChannelNoItems();
        if (this.satInfo.unicable_type == 0 || this.satInfo.unicable_type == 1) {
            this.spn_SCRPosition.setEnabled(false);
            this.spn_SCRPosition.setFocusable(false);
            this.SCRPosition_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        } else {
            this.spn_SCRPosition.setEnabled(true);
            this.spn_SCRPosition.setFocusable(true);
        }
        this.et_SCRFrequency.setDigitsText(Integer.toString(this.satInfo.scr_bandPassFrequency));
    }

    private void FillLNBPowerItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getString(R.string.off));
        items.add(getString(R.string._13_18_V));
        items.add(getString(R.string._13V));
        items.add(getString(R.string._18V));
        if (this.satInfo != null) {
            this.spn_lnbPower.initView((int) R.string.LNBPower, items, this.satInfo.lnb_power, new C00155());
        }
    }

    private void FillChannelNoItems() {
        if ((this.satInfo.unicable_type != 0 && this.satInfo.unicable_type != 2) || this.preChannelNo_ItemType != 0) {
            if ((this.satInfo.unicable_type != 1 && this.satInfo.unicable_type != 3) || this.preChannelNo_ItemType != 1) {
                ArrayList items = new ArrayList();
                items.clear();
                int i;
                if (this.satInfo.unicable_type == 0 || this.satInfo.unicable_type == 2) {
                    this.preChannelNo_ItemType = 0;
                    for (i = 0; i < 4; i++) {
                        items.add("SCR " + (i + 1));
                    }
                } else {
                    this.preChannelNo_ItemType = 0;
                    for (i = 0; i < 8; i++) {
                        items.add("SCR " + (i + 1));
                    }
                }
                if (this.satInfo != null) {
                    this.spn_channelNo.initView((int) R.string.ChannelNo, items, this.satInfo.scr_number, new C00166());
                }
            }
        }
    }

    private void FillDiseqcTypeItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getString(R.string.None));
        items.add("1.0");
        items.add("1.1");
        if (this.satInfo != null) {
            this.spn_diseqcType.initView((int) R.string.DiseqcType, items, this.satInfo.diseqc_type, new C00177());
        }
    }

    private void FillDiseqcPortItems() {
        ArrayList items = new ArrayList();
        items.clear();
        int i;
        if (this.satInfo.diseqc_type == 0) {
            this.preDiseqcPort_ItemType = 0;
            for (i = 0; i <= 3; i++) {
                items.add(getString(R.string.None));
            }
        } else if (this.satInfo.diseqc_type == 1) {
            this.preDiseqcPort_ItemType = 1;
            for (i = 0; i <= 3; i++) {
                items.add(Integer.toString(i + 1));
            }
        } else {
            this.preDiseqcPort_ItemType = 2;
            for (i = 0; i < 16; i++) {
                items.add(Integer.toString(i + 1));
            }
        }
        if (this.satInfo != null) {
            this.spn_diseqcPort.initView((int) R.string.DiseqcPort, items, this.satInfo.diseqc_port, new C00188());
        }
    }

    private void FillSCRPositionItems() {
        if ((this.satInfo.unicable_type != 0 && this.satInfo.unicable_type != 1) || this.preSCRPosition_ItemType != 0) {
            if ((this.satInfo.unicable_type != 2 && this.satInfo.unicable_type != 3) || this.preSCRPosition_ItemType != 1) {
                ArrayList items = new ArrayList();
                items.clear();
                if (this.satInfo.unicable_type == 0 || this.satInfo.unicable_type == 1) {
                    this.preSCRPosition_ItemType = 0;
                    items.add(getString(R.string.None));
                    items.add(getString(R.string.None));
                } else {
                    this.preSCRPosition_ItemType = 1;
                    items.add("A");
                    items.add("B");
                }
                if (this.satInfo != null) {
                    this.spn_SCRPosition.initView((int) R.string.SCRPosition, items, this.satInfo.diseqc_port, new C00199());
                }
            }
        }
    }

    private void FillTransponderItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            for (int i = 0; i < this.satInfo.tp_count; i++) {
                if (MW.db_dvb_s_get_tp_info(0, this.currentSatIndex, i, this.tpInfo) == 0) {
                    String str = "%d / %d    %05d   %s   %05d";
                    Object[] objArr = new Object[5];
                    objArr[0] = Integer.valueOf(i + 1);
                    objArr[1] = Integer.valueOf(this.satInfo.tp_count);
                    objArr[2] = Integer.valueOf(this.tpInfo.frq);
                    objArr[3] = this.tpInfo.pol == 0 ? "V" : "H";
                    objArr[4] = Integer.valueOf(this.tpInfo.sym);
                    items.add(String.format(str, objArr));
                }
            }
            if (this.satInfo.tp_count == 0) {
                Log.e("LEE", " ======0");
                this.spn_transponder.setEnabled(false);
                this.spn_transponder.setFocusable(false);
                this.Transponder_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                MW.tuner_unlock_tp(0, 0, false);
            } else {
                Log.e("LEE", " !==0");
                this.spn_transponder.setEnabled(true);
                this.spn_transponder.setFocusable(true);
                if (this.spn_transponder.isFocused()) {
                    this.Transponder_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                } else {
                    this.Transponder_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                }
                LockCurrentTP();
            }
            if (this.currentTpIndex == -1) {
                this.currentTpIndex = 0;
            }
            this.spn_transponder.initView((int) R.string.Transponder, items, this.currentTpIndex, new OnItemClickListener() {
                public void onItemClick(AdapterView parent, View v, int position, long id) {
                    AntennaSetupActivity.this.currentTpIndex = position;
                    System.out.println("currentTpIndex" + position);
                    AntennaSetupActivity.this.RefreshTransponderInfo();
                    AntennaSetupActivity.this.fSaveSatTpFlag = true;
                }
            });
        }
    }

    private void RefreshSatelliteInfo(boolean bResetTpIndex) {
        if (MW.db_dvb_s_get_sat_info(0, this.currentSatIndex, this.satInfo) == 0) {
            RefreshBackGround();
            FillLNBPowerItems();
            FillDiseqcTypeItems();
            FillLNBTypeItems();
            if (bResetTpIndex) {
                this.currentTpIndex = 0;
            }
            FillTransponderItems();
            RefreshTransponderInfo();
        }
    }

    private void RefreshTransponderInfo() {
        if (MW.db_dvb_s_get_tp_info(0, this.currentSatIndex, this.currentTpIndex, this.tpInfo) == 0) {
            LockCurrentTP();
        }
    }

    private void RefreshLNBType() {
        if (this.satInfo != null) {
            MW.db_dvb_s_set_sat_info(0, this.satInfo);
            LockCurrentTP();
            RefreshBackGround();
        }
    }

    private void getSelSatData() {
        this.SelSatList.clear();
        this.SelSatList.add(Integer.valueOf(this.currentSatIndex));
    }

    private void getSelTpData() {
        this.SelTpList.clear();
        this.SelTpList.add(Integer.valueOf(this.currentTpIndex));
    }

    private void ScanAction() {
        Dialog scanActionDig = new ScanActionDig(this, R.style.MyDialog);
        scanActionDig.setContentView(R.layout.satellite_scan_dialog);
        scanActionDig.show();
        scanActionDig.getWindow().addFlags(2);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            Intent intent;
            switch (i) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.spn_satellite.hasFocus()) {
                        if (this.DiseqcPort_item.getVisibility() == 0) {
                            if (this.satInfo.diseqc_type != 0) {
                                this.spn_diseqcPort.requestFocus();
                            } else {
                                this.spn_diseqcType.requestFocus();
                            }
                        }
                        if (this.SCRPosition_item.getVisibility() == 0) {
                            if (this.satInfo.unicable_type == 0 || this.satInfo.unicable_type == 1) {
                                this.et_SCRFrequency.requestFocus();
                            } else {
                                this.spn_SCRPosition.requestFocus();
                            }
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_SCRPosition.hasFocus()) {
                        this.et_SCRFrequency.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_lnbType.hasFocus()) {
                        this.spn_satellite.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_transponder.hasFocus()) {
                        this.spn_lnbType.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_22Khz.hasFocus() && this.spn_transponder.isFocusable()) {
                        this.spn_transponder.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (!this.spn_22Khz.hasFocus() || this.spn_transponder.isFocusable()) {
                        if (!this.spn_lnbPower.hasFocus()) {
                            if (!this.spn_diseqcType.hasFocus()) {
                                if (!this.spn_diseqcPort.hasFocus()) {
                                    if (!this.spn_unicableType.hasFocus()) {
                                        if (!this.spn_channelNo.hasFocus()) {
                                            if (!this.et_SCRFrequency.hasFocus()) {
                                                RefreshBackGround();
                                                break;
                                            }
                                            this.spn_channelNo.requestFocus();
                                            RefreshBackGround();
                                            return true;
                                        }
                                        this.spn_unicableType.requestFocus();
                                        RefreshBackGround();
                                        return true;
                                    }
                                    this.spn_transponder.requestFocus();
                                    RefreshBackGround();
                                    return true;
                                }
                                this.spn_diseqcType.requestFocus();
                                RefreshBackGround();
                                return true;
                            }
                            this.spn_lnbPower.requestFocus();
                            RefreshBackGround();
                            return true;
                        }
                        if (this.spn_22Khz.isEnabled()) {
                            this.spn_22Khz.requestFocus();
                        } else if (this.spn_transponder.isFocusable()) {
                            this.spn_transponder.requestFocus();
                        } else {
                            this.LNBType_item.requestFocus();
                        }
                        RefreshBackGround();
                        return true;
                    } else {
                        this.spn_lnbType.requestFocus();
                        RefreshBackGround();
                        return true;
                    }
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.spn_diseqcType.hasFocus()) {
                        if (this.DiseqcPort_item.getVisibility() != 0) {
                            this.spn_satellite.requestFocus();
                        } else if (this.satInfo.diseqc_type == 0) {
                            this.spn_satellite.requestFocus();
                        } else {
                            this.spn_diseqcPort.requestFocus();
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.et_SCRFrequency.hasFocus()) {
                        if (!this.spn_SCRPosition.requestFocus()) {
                            this.spn_satellite.requestFocus();
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_channelNo.hasFocus()) {
                        this.et_SCRFrequency.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.bt_motorSettingOK.hasFocus() || this.spn_diseqcPort.hasFocus() || this.spn_SCRPosition.hasFocus()) {
                        this.spn_satellite.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_satellite.hasFocus()) {
                        this.spn_lnbType.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_lnbType.hasFocus() && this.spn_transponder.isFocusable()) {
                        this.spn_transponder.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_lnbType.hasFocus() && !this.spn_transponder.isFocusable()) {
                        if (this.Onoff22K_item.getVisibility() == 0) {
                            if (this.satInfo.lnb_type == 3) {
                                this.spn_lnbPower.requestFocus();
                            } else {
                                this.spn_22Khz.requestFocus();
                            }
                        }
                        if (this.UnicableType_item.getVisibility() == 0) {
                            this.spn_unicableType.requestFocus();
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_transponder.hasFocus()) {
                        System.out.println("spn_transponder.hasFocus()");
                        if (this.Onoff22K_item.getVisibility() == 0) {
                            if (this.satInfo.lnb_type == 3) {
                                this.spn_lnbPower.requestFocus();
                            } else {
                                this.spn_22Khz.requestFocus();
                            }
                        }
                        if (this.UnicableType_item.getVisibility() == 0) {
                            this.spn_unicableType.requestFocus();
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_unicableType.hasFocus()) {
                        this.spn_channelNo.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_22Khz.hasFocus()) {
                        this.spn_lnbPower.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_lnbPower.hasFocus()) {
                        this.spn_diseqcType.requestFocus();
                        RefreshBackGround();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (this.et_lnbLowFreq.isFocused() || this.et_lnbHighFreq.isFocused()) {
                        return true;
                    }
                    if (this.et_SCRFrequency.isFocused()) {
                        return true;
                    }
                    break;
                case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                    intent = new Intent();
                    intent.setFlags(67108864);
                    intent.setClass(this, SatelliteListActivity.class);
                    intent.putExtra("selected_sat_index", this.currentSatIndex);
                    startActivity(intent);
                    return true;
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    ScanAction();
                    return true;
                case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                    intent = new Intent();
                    intent.setFlags(67108864);
                    intent.setClass(this, SelectedSatelliteListActivity.class);
                    intent.putExtra("selected_sat_index", this.currentSatPos);
                    startActivity(intent);
                    return true;
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    intent = new Intent();
                    intent.setFlags(67108864);
                    intent.setClass(this, TpListActivity.class);
                    intent.putExtra("selected_sat_index", this.currentSatPos);
                    intent.putExtra("selected_tp_index", this.currentTpIndex);
                    startActivity(intent);
                    return true;
            }
        }
        RefreshBackGround();
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    void ShowToastInformation(String text, int duration_mode) {
        LinearLayout toast_view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
        ((TextView) toast_view.findViewById(R.id.toast_text)).setText(text);
        Toast toast = new Toast(this);
        if (duration_mode == 0) {
            toast.setDuration(0);
        } else if (duration_mode == 1) {
            toast.setDuration(1);
        }
        toast.setView(toast_view);
        toast.setGravity(17, 0, -70);
        toast.show();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.antenna_setup);
        getWindow().setSoftInputMode(3);
        initLinearLayout();
        this.spn_satellite = (ComboLayout) findViewById(R.id.Spinner_Satellite);
        this.spn_lnbType = (ComboLayout) findViewById(R.id.Spinner_LNBType);
        this.et_lnbLowFreq = (DigitsEditText) findViewById(R.id.EditText_LNBLowFreq);
        this.et_lnbHighFreq = (DigitsEditText) findViewById(R.id.EditText_LNBHighFreq);
        this.spn_transponder = (ComboLayout) findViewById(R.id.Spinner_Transponder);
        this.et_SCRFrequency = (DigitsEditText) findViewById(R.id.EditText_SCRFrequency);
        this.et_SCRFrequency.addTextChangedListener(this.mEditTextWatcher);
        this.spn_22Khz = (ComboLayout) findViewById(R.id.Spinner_22Khz);
        this.spn_unicableType = (ComboLayout) findViewById(R.id.Spinner_UnicableType);
        this.spn_lnbPower = (ComboLayout) findViewById(R.id.Spinner_LNBPower);
        this.spn_channelNo = (ComboLayout) findViewById(R.id.Spinner_ChannelNo);
        this.spn_diseqcType = (ComboLayout) findViewById(R.id.Spinner_DiseqcType);
        this.spn_diseqcPort = (ComboLayout) findViewById(R.id.Spinner_DiseqcPort);
        this.spn_SCRPosition = (ComboLayout) findViewById(R.id.Spinner_SCRPosition);
        this.prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
        this.prg_signalStrength.setMax(100);
        this.prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
        this.prg_signalQuality.setMax(100);
        this.prg_signalStrength.setProgress(0);
        this.prg_signalQuality.setProgress(0);
        this.txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
        this.txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);
        this.txv_signalStrength.setText("0%");
        this.txv_signalQuality.setText("0%");
        this.bt_motorSettingOK = (Button) findViewById(R.id.Button_MotorSetting_OK);
        this.bt_motorSettingOK.setText("OK");
        this.bt_motorSettingOK.setFocusable(true);
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        this.spn_satellite.requestFocus();
        this.bFromManualSearch = false;
        Log.e("AntennaSetupActivity", " onCreate");
    }

    private void RefreshAllData() {
        initData();
        if (MW.db_dvb_s_get_selected_sat_count(0) == 0) {
            ShowToastInformation(getResources().getString(R.string.no_selected_satellite), 0);
            finishMyself();
            return;
        }
        FillSatelliteItems();
        FillLNBTypeItems();
        FillUnicableTypeItems();
        FillLNBPowerItems();
        FillDiseqcTypeItems();
        RefreshSatelliteInfo(false);
    }

    private void initData() {
        if (MW.db_dvb_s_get_selected_sat_count(0) == 0) {
            ShowToastInformation(getResources().getString(R.string.no_selected_satellite), 0);
            finishMyself();
        } else if (this.bFromManualSearch) {
            this.bFromManualSearch = false;
        } else if (MW.db_dvb_s_get_current_tp_info(0, this.tpInfo) == 0) {
            System.out.println(" tpInfo.sat_index  : " + this.tpInfo.sat_index);
            System.out.println(" tpInfo.tp_index  : " + this.tpInfo.tp_index);
            System.out.println(" tpInfo.sat_pos  : " + this.tpInfo.sat_pos);
            this.currentSatIndex = this.tpInfo.sat_index;
            this.currentSatPos = this.tpInfo.sat_pos;
            if (MW.db_dvb_s_get_tp_count(0, this.currentSatIndex) == 0) {
                this.currentTpIndex = -1;
                ShowToastInformation(getResources().getString(R.string.add_tp), 0);
                return;
            }
            this.currentTpIndex = this.tpInfo.tp_index;
        } else if (MW.db_dvb_s_get_current_sat_info(0, this.satInfo) == 0) {
            this.currentSatIndex = this.satInfo.sat_index;
            this.currentTpIndex = 0;
        } else {
            this.currentSatIndex = 0;
            this.currentTpIndex = 0;
        }
    }

    protected void onStart() {
        super.onStart();
        Log.e("AntennaSetupActivity", " onStart");
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("AntennaSetupActivity", " onNewIntent");
    }

    protected void onPause() {
        super.onPause();
        if (MW.db_dvb_s_get_current_tp_info(0, this.tpInfo) != 0) {
            MW.db_dvb_s_set_current_sat_tp(0, 0, 0);
        } else if (!(this.tpInfo.sat_index == this.currentSatIndex && this.tpInfo.tp_index == this.currentTpIndex)) {
            MW.db_dvb_s_set_current_sat_tp(0, this.currentSatIndex, this.currentTpIndex);
        }
        if (this.fSaveSatTpFlag) {
            this.fSaveSatTpFlag = false;
            MW.db_dvb_s_save_sat_tp(0);
        }
        enableMwMessageCallback(null);
        MW.tuner_unlock_tp(0, 0, false);
        Log.d("AntennaSetupActivity", " onPause");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d("AntennaSetupActivity", " onRestart");
    }

    protected void onResume() {
        Log.d("AntennaSetupActivity", " onResume start");
        super.onResume();
        RefreshAllData();
        SETTINGS.send_led_msg("NENU");
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(2, true);
        if (this.satInfo == null) {
            MW.tuner_unlock_tp(0, 0, false);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalStrength.setProgress(0);
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        } else if (this.satInfo.tp_count > 0) {
            LockCurrentTP();
        } else {
            MW.tuner_unlock_tp(0, 0, false);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalStrength.setProgress(0);
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        }
        Log.d("AntennaSetupActivity", " onResume end");
    }

    protected void onStop() {
        super.onStop();
        Log.d("AntennaSetupActivity", " onStop");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d("AntennaSetupActivity", " onDestroy");
    }
}
