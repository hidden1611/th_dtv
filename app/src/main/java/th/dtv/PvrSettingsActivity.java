package th.dtv;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.storage.StorageManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import th.dtv.StorageDevice.DeviceItem;

public class PvrSettingsActivity extends DtvBaseActivity {
    private int FileSystemIndex;
    private ComboLayout deviceFormat_linearlayout;
    private ComboLayout deviceInfo_linearlayout;
    private LinearLayout deviceInfo_text;
    private ComboLayout deviceName_linearlayout;
    private Dialog dialog;
    private ComboLayout fileSystem_linearlayout;
    private TextView filesystem;
    private TextView filesystem_txt;
    private TextView freespace;
    private TextView freespace_txt;
    private boolean isTimeShift;
    private ArrayList<String> msFileSystemList;
    private ComboLayout pvr_time_linearlayout;
    private int storageDeviceCount;
    private int storageDeviceIndex;
    private StorageDevice storageDeviceInfo;
    private StorageDevicePlugReceiver storageDevicePlugReceiver;
    private ComboLayout timeshift_linearlayout;
    private TextView totalcapacity;
    private TextView totalcapacity_txt;
    private TextView usedspace;

    /* renamed from: th.dtv.PvrSettingsActivity.1 */
    class C00761 implements OnItemClickListener {
        C00761() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
            PvrSettingsActivity.this.storageDeviceIndex = pos;
            PvrSettingsActivity.this.RefreshFileSystemItem();
            PvrSettingsActivity.this.RefreshDeviceInfo();
        }
    }

    /* renamed from: th.dtv.PvrSettingsActivity.2 */
    class C00772 implements OnItemClickListener {
        C00772() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
        }
    }

    /* renamed from: th.dtv.PvrSettingsActivity.3 */
    class C00783 implements OnItemClickListener {
        C00783() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
        }
    }

    /* renamed from: th.dtv.PvrSettingsActivity.4 */
    class C00794 implements OnItemClickListener {
        C00794() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
            Log.e("LEE", "pos = " + pos);
            SETTINGS.setTimeShiftLengthIndex(pos);
        }
    }

    /* renamed from: th.dtv.PvrSettingsActivity.5 */
    class C00805 implements OnItemClickListener {
        C00805() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
            Log.e("LEE", "pos = " + pos);
            SETTINGS.setRecordLengthIndex(pos);
        }
    }

    /* renamed from: th.dtv.PvrSettingsActivity.6 */
    class C00816 implements OnItemClickListener {
        C00816() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            PvrSettingsActivity.this.ShowDeviceInfo();
        }
    }

    /* renamed from: th.dtv.PvrSettingsActivity.7 */
    class C00827 implements OnKeyListener {
        C00827() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    if (PvrSettingsActivity.this.deviceFormat_linearlayout.getInfoTextView().getText().equals("FAT32")) {
                        PvrSettingsActivity.this.FileSystemIndex = 0;
                    } else if (PvrSettingsActivity.this.deviceFormat_linearlayout.getInfoTextView().getText().equals("NTFS")) {
                        PvrSettingsActivity.this.FileSystemIndex = 1;
                    } else {
                        PvrSettingsActivity.this.FileSystemIndex = 0;
                    }
                    PvrSettingsActivity.this.DoDeviceFormatConfirm();
                    return true;
                default:
                    return false;
            }
        }
    }

    public class ConfirmCustomDialog extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        TextView mInfoTextView;
        TextView mTitleTextView;

        /* renamed from: th.dtv.PvrSettingsActivity.ConfirmCustomDialog.1 */

        /* renamed from: th.dtv.PvrSettingsActivity.ConfirmCustomDialog.2 */
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.factory_restore_confirm_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitleTextView = (TextView) findViewById(R.id.title_textview);
            this.mInfoTextView = (TextView) findViewById(R.id.info_textview);
            this.mTitleTextView.setText(R.string.DeviceFormat);
            this.mInfoTextView.setText(R.string.confirm_format_device);
            this.mBtnCancel.requestFocus();
        }

        public ConfirmCustomDialog(Context context, int theme) {
            super(context, theme);
            this.mContext = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class DeviceInfoDia extends Dialog {
        TextView devicename;
        TextView filesystem;
        TextView freespace;
        private Context mContext;
        TextView totalcapacity;
        TextView usedspace;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.deviceinfo);
            this.filesystem = (TextView) findViewById(R.id.filesystem);
            this.freespace = (TextView) findViewById(R.id.freespace);
            this.totalcapacity = (TextView) findViewById(R.id.totalcapacity);
            this.devicename = (TextView) findViewById(R.id.devicename);
            if (PvrSettingsActivity.this.storageDeviceInfo != null) {
                DeviceItem item = PvrSettingsActivity.this.storageDeviceInfo.getDeviceItem(PvrSettingsActivity.this.storageDeviceIndex);
                if (item != null) {
                    if (item.format == null) {
                        this.filesystem.setText(((String) PvrSettingsActivity.this.msFileSystemList.get(0)) + "");
                    } else {
                        this.filesystem.setText(item.format + "");
                    }
                    this.freespace.setText(item.spare + "");
                    this.totalcapacity.setText(item.total + "");
                    if (item.VolumeName.indexOf("[udisk") == 1) {
                        this.devicename.setText(PvrSettingsActivity.this.getResources().getString(R.string.udisk) + " " + (PvrSettingsActivity.this.storageDeviceIndex + 1));
                        return;
                    } else {
                        this.devicename.setText(item.VolumeName);
                        return;
                    }
                }
                this.filesystem.setText("");
                this.freespace.setText("");
                this.totalcapacity.setText("");
                this.devicename.setText("");
            }
        }

        public DeviceInfoDia(Context context, int theme) {
            super(context, theme);
            this.mContext = null;
            this.filesystem = null;
            this.usedspace = null;
            this.freespace = null;
            this.totalcapacity = null;
            this.devicename = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                        dismiss();
                        return true;
                }
            }
            return super.onKeyDown(keyCode, event);
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    dismiss();
                    return true;
                default:
                    return super.onKeyUp(keyCode, event);
            }
        }
    }

    private class StorageDevicePlugReceiver extends BroadcastReceiver {
        private StorageDevicePlugReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
        }
    }

    public PvrSettingsActivity() {
        this.timeshift_linearlayout = null;
        this.pvr_time_linearlayout = null;
        this.deviceName_linearlayout = null;
        this.deviceInfo_linearlayout = null;
        this.fileSystem_linearlayout = null;
        this.deviceFormat_linearlayout = null;
        this.deviceInfo_text = null;
        this.filesystem = null;
        this.usedspace = null;
        this.freespace = null;
        this.totalcapacity = null;
        this.dialog = null;
        this.isTimeShift = true;
        this.msFileSystemList = null;
        this.storageDeviceCount = 0;
        this.storageDeviceIndex = 0;
        this.FileSystemIndex = 0;
        this.filesystem_txt = null;
        this.freespace_txt = null;
        this.totalcapacity_txt = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pvr_settings_activity);
        initData();
        initView();
    }

    private void initData() {
        this.msFileSystemList = new ArrayList();
        this.msFileSystemList.add(getResources().getString(R.string.unknown));
        this.msFileSystemList.add(getResources().getString(R.string.FAT32));
        this.msFileSystemList.add(getResources().getString(R.string.NTFS));
    }

    protected void onResume() {
        super.onResume();
        if (this.storageDeviceInfo == null) {
            this.storageDeviceInfo = new StorageDevice(this);
        }
        this.storageDevicePlugReceiver = new StorageDevicePlugReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.DEVICE_MOUNTED");
        filter.addAction("android.intent.action.DEVICE_REMOVED");
        registerReceiver(this.storageDevicePlugReceiver, filter);
        RefreshDeviceView();
    }

    protected void onPause() {
        super.onPause();
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        this.msFileSystemList = null;
        this.storageDeviceInfo.finish(this);
        if (this.storageDevicePlugReceiver != null) {
            unregisterReceiver(this.storageDevicePlugReceiver);
            this.storageDevicePlugReceiver = null;
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.deviceInfo_text.getVisibility() != 0 && this.timeshift_linearlayout.isFocused()) {
                        this.deviceName_linearlayout.requestFocus();
                        return true;
                    }
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.deviceInfo_text.getVisibility() != 0 && this.deviceName_linearlayout.isFocused()) {
                        this.timeshift_linearlayout.requestFocus();
                        return true;
                    }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void RefreshDeviceView() {
        if (this.storageDeviceInfo != null) {
            this.storageDeviceCount = this.storageDeviceInfo.getDeviceCount();
            ArrayList items;
            if (this.storageDeviceCount > 0) {
                this.deviceName_linearlayout = (ComboLayout) findViewById(R.id.deviceName_linearlayout);
                items = new ArrayList();
                items.clear();
                for (int i = 0; i < this.storageDeviceInfo.getDeviceCount(); i++) {
                    DeviceItem item = this.storageDeviceInfo.getDeviceItem(i);
                    if (item.VolumeName.indexOf("[udisk") == 1) {
                        items.add(getResources().getString(R.string.udisk) + " " + ((this.storageDeviceCount - 1) - i));
                    } else {
                        items.add(item.VolumeName);
                    }
                }
                this.deviceName_linearlayout.initView((int) R.string.DeviceName, items, this.storageDeviceIndex, new C00761());
                RefreshFileSystemItem();
                RefreshDeviceInfo();
                this.deviceInfo_linearlayout.setVisibility(View.GONE);
                this.deviceInfo_text.setVisibility(View.VISIBLE);
                this.fileSystem_linearlayout.setVisibility(View.GONE);
                this.deviceFormat_linearlayout.setVisibility(View.VISIBLE);
                return;
            }
            this.deviceName_linearlayout = (ComboLayout) findViewById(R.id.deviceName_linearlayout);
            items = new ArrayList();
            items.clear();
            items.add(getResources().getString(R.string.None));
            this.deviceName_linearlayout.initView((int) R.string.DeviceName, items, 0, new C00772());
            RefreshFileSystemItem();
            this.deviceInfo_linearlayout.setVisibility(View.GONE);
            this.deviceInfo_text.setVisibility(View.GONE);
            this.fileSystem_linearlayout.setVisibility(View.GONE);
            this.deviceFormat_linearlayout.setVisibility(View.GONE);
        }
    }

    private void RefreshFileSystemItem() {
        if (this.storageDeviceInfo != null) {
            DeviceItem item = this.storageDeviceInfo.getDeviceItem(this.storageDeviceIndex);
            if (item != null) {
                if (item.format != null) {
                    int j = 0;
                    while (j < this.msFileSystemList.size()) {
                        Log.e("LEE", "item.format =" + item.format);
                        Log.e("LEE", "msFileSystemList.get(j)" + ((String) this.msFileSystemList.get(j)));
                        if (item.format.equals(this.msFileSystemList.get(j))) {
                            Log.e("LEE", "j =" + j);
                            if (item.format.equals(this.msFileSystemList.get(0))) {
                                this.FileSystemIndex = 0;
                            } else {
                                this.FileSystemIndex = j - 1;
                            }
                        } else {
                            j++;
                        }
                    }
                } else {
                    this.FileSystemIndex = 0;
                }
            }
            this.fileSystem_linearlayout = (ComboLayout) findViewById(R.id.fileSystem_linearlayout);
            this.fileSystem_linearlayout.initView((int) R.string.FileSystem, (int) R.array.video_type, this.FileSystemIndex, new C00783());
            initDeviceFormatItem();
        }
    }

    private void initView() {
        this.filesystem_txt = (TextView) findViewById(R.id.filesystem_txt);
        this.freespace_txt = (TextView) findViewById(R.id.freespace_txt);
        this.totalcapacity_txt = (TextView) findViewById(R.id.totalcapacity_txt);
        this.filesystem_txt.setText(getResources().getString(R.string.FileSystem) + " :");
        this.freespace_txt.setText(getResources().getString(R.string.FreeSpace) + " :");
        this.totalcapacity_txt.setText(getResources().getString(R.string.TotalCapacity) + " :");
        this.timeshift_linearlayout = (ComboLayout) findViewById(R.id.timeshift_linearlayout);
        this.timeshift_linearlayout.initView((int) R.string.timeshiftLength, (int) R.array.select_timeshifttime_dialog_items, SETTINGS.getTimeShiftLengthIndex(), new C00794());
        this.pvr_time_linearlayout = (ComboLayout) findViewById(R.id.pvr_time_linearlayout);
        this.pvr_time_linearlayout.initView((int) R.string.recordLength, (int) R.array.select_recordtime_dialog_items, SETTINGS.getRecordLengthIndex(), new C00805());
        this.deviceInfo_linearlayout = (ComboLayout) findViewById(R.id.deviceInfo_linearlayout);
        this.deviceInfo_linearlayout.initView((int) R.string.DeviceInfo, 0, 0, new C00816());
        this.deviceInfo_linearlayout.getLeftImageView().setVisibility(View.INVISIBLE);
        this.deviceInfo_linearlayout.getRightImageView().setVisibility(View.INVISIBLE);
        this.deviceInfo_linearlayout.getInfoTextView().setVisibility(View.INVISIBLE);
        initDeviceFormatItem();
        this.deviceInfo_text = (LinearLayout) findViewById(R.id.deviceInfo_text);
        this.filesystem = (TextView) findViewById(R.id.filesystem);
        this.freespace = (TextView) findViewById(R.id.freespace);
        this.totalcapacity = (TextView) findViewById(R.id.totalcapacity);
        RefreshDeviceInfo();
    }

    private void initDeviceFormatItem() {
        this.deviceFormat_linearlayout = (ComboLayout) findViewById(R.id.deviceFormat_linearlayout);
        ArrayList item = new ArrayList();
        item.clear();
        item.add("FAT32");
        item.add("NTFS");
        this.deviceFormat_linearlayout.initView((int) R.string.DeviceFormat, item, this.FileSystemIndex, new C00827());
    }

    private void RefreshDeviceInfo() {
        if (this.storageDeviceInfo != null) {
            DeviceItem item = this.storageDeviceInfo.getDeviceItem(this.storageDeviceIndex);
            if (item != null) {
                if (item.format == null) {
                    this.filesystem.setText(((String) this.msFileSystemList.get(0)) + "");
                } else {
                    this.filesystem.setText(item.format + "");
                }
                this.freespace.setText(item.spare + "");
                this.totalcapacity.setText(item.total + "");
                return;
            }
            this.filesystem.setText("");
            this.freespace.setText("");
            this.totalcapacity.setText("");
        }
    }

    private void DoDeviceFormatConfirm() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        this.dialog = new ConfirmCustomDialog(this, R.style.MyDialog);
        this.dialog.show();
        LayoutParams lp = this.dialog.getWindow().getAttributes();
        lp.x = 0;
        lp.y = -25;
        this.dialog.getWindow().setAttributes(lp);
        this.dialog.getWindow().addFlags(2);
    }

    private void ShowDeviceInfo() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        this.dialog = new DeviceInfoDia(this, R.style.MyDialog);
        this.dialog.show();
        LayoutParams lp = this.dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        this.dialog.getWindow().setAttributes(lp);
        this.dialog.getWindow().addFlags(2);
    }
}
