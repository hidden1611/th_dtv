package th.dtv.myctrl;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import th.dtv.CustomListView;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.mw_data.service;

public class FindDialog extends Dialog {
    private MatchChannelAdapter channelListAdapter;
    private CustomListView listView;
    private int listitem_pos;
    private FindCallBack mCb;
    private Context mContext;
    private int mCurrentIndex;
    private int mServiceType;
    private TextWatcher mTextWatcher;
    private String matchString;
    private EditText searchEt;
    private service serviceInfo;

    /* renamed from: th.dtv.myctrl.FindDialog.1 */
    class C03051 implements OnItemSelectedListener {
        C03051() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            FindDialog.this.listitem_pos = pos;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.myctrl.FindDialog.2 */
    class C03062 implements OnScrollListener {
        C03062() {
        }

        public void onScrollStateChanged(AbsListView arg0, int arg1) {
        }

        public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
        }
    }

    /* renamed from: th.dtv.myctrl.FindDialog.3 */
    class OnClickedEdited implements OnKeyListener {
        OnClickedEdited() {
        }

        public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
            switch (arg1) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    if (FindDialog.this.searchEt.isFocused()) {
                        FindDialog.this.dismiss();
                        return true;
                    }
                    FindDialog.this.searchEt.requestFocus();
                    FindDialog.this.searchEt.selectAll();
                    return true;
                case MW.RET_INVALID_TYPE /*23*/:
                case 66:
                    int index = MW.db_get_find_match_service_index(FindDialog.this.listitem_pos);
                    if (FindDialog.this.mCb != null) {
                        FindDialog.this.mCb.DoCallBackEvent(index);
                    }
                    FindDialog.this.dismiss();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            return false;
        }
    }

    public class MatchChannelAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView channel_name;

            ViewHolder() {
            }
        }

        public MatchChannelAdapter(Context context) {
            this.context = null;
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            Log.e("LEE", "count :" + MW.db_get_find_match_service_count());
            return MW.db_get_find_match_service_count();
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int matchServicePos, View convertView, ViewGroup arg2) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.find_channel_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.channel_name = (TextView) convertView.findViewById(R.id.channellist_name_txt);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            int tempPos = MW.db_get_find_match_service_index(matchServicePos);
            if (tempPos != -1) {
                Log.e("LEE", "tempPos : " + tempPos);
                if (MW.db_get_service_info(FindDialog.this.mServiceType, tempPos, FindDialog.this.serviceInfo) == 0) {
                    SpannableString s = new SpannableString(FindDialog.this.serviceInfo.channel_number_display + "   " + FindDialog.this.serviceInfo.service_name + "");
                    Matcher m = Pattern.compile(FindDialog.this.matchString).matcher(s);
                    while (m.find()) {
                        Log.e("LEE", "1111111111111111");
                        s.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.divider_color)), m.start(), m.end(), 33);
                    }
                    localViewHolder.channel_name.setText(s);
                    return convertView;
                }
            }
            Log.e("LEE", "tempPos : " + tempPos);
            return null;
        }
    }

    private class SearchTextWacher implements TextWatcher {
        private SearchTextWacher() {
        }

        public void afterTextChanged(Editable arg0) {
        }

        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            Log.e("LEE", s.toString());
            FindDialog.this.matchString = s.toString();
            MW.db_find_match_service(FindDialog.this.mServiceType, s.toString(), false);
            FindDialog.this.channelListAdapter.notifyDataSetChanged();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_channel_dia);
        this.searchEt = (EditText) findViewById(R.id.searchEt);
        this.mTextWatcher = new SearchTextWacher();
        this.searchEt.addTextChangedListener(this.mTextWatcher);
        this.listView = (CustomListView) findViewById(R.id.matchlistView);
        this.listView.setOnItemSelectedListener(new C03051());
        this.listView.setCustomScrollListener(new C03062());
//        this.listView.setCustomKeyListener(new OnClickedEdited());
        this.listView.setVisibleItemCount(5);
        this.channelListAdapter = new MatchChannelAdapter(this.mContext);
        this.listView.setAdapter(this.channelListAdapter);
        this.listView.setChoiceMode(1);
    }

    protected void onStop() {
        super.onStop();
        MW.db_free_find_match_service();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
            case DtvBaseActivity.KEYCODE_INFO /*185*/:
                if (this.searchEt.isFocused()) {
                    if (this.mCb != null) {
                        this.mCb.DoCallBackEvent(this.mCurrentIndex);
                    }
                    dismiss();
                    return true;
                }
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    public FindDialog(Context context) {
        super(context);
        this.mContext = null;
        this.searchEt = null;
        this.mTextWatcher = null;
        this.listView = null;
        this.mServiceType = 0;
        this.mCurrentIndex = 0;
        this.serviceInfo = new service();
        this.matchString = null;
        this.mCb = null;
        this.listitem_pos = 0;
    }

    public FindDialog(Context context, int theme, int serviceType, int currentindex, FindCallBack cb) {
        super(context, theme);
        this.mContext = null;
        this.searchEt = null;
        this.mTextWatcher = null;
        this.listView = null;
        this.mServiceType = 0;
        this.mCurrentIndex = 0;
        this.serviceInfo = new service();
        this.matchString = null;
        this.mCb = null;
        this.listitem_pos = 0;
        this.mContext = context;
        this.mServiceType = serviceType;
        this.mCurrentIndex = currentindex;
        this.mCb = cb;
    }
}
