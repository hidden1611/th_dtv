package th.dtv.myctrl;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;

public class ScanDialog extends Dialog {
    private TextView channel_type_opt;
    private ScanCallBack mCb;
    private Context mContext;
    private LayoutInflater mInflater;
    View mLayoutView;
    private LinearLayout nit_search_ll;
    private TextView scan_type_opt;
    private TextView servicetype_opt;

    /* renamed from: th.dtv.myctrl.ScanDialog.1 */
//    class OnClickEdite implements View.OnClickListener {
//        OnClickEdite() {
//        }
//
//        public boolean onKey(View v, int keyCode, KeyEvent event) {
//            if (event.getAction() == 0) {
//                switch (keyCode) {
//                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                    case MW.RET_MEMORY_ERROR /*22*/:
//                        if (ScanDialog.this.channel_type_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.All))) {
//                            ScanDialog.this.channel_type_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.FTAOnly));
//                        } else {
//                            ScanDialog.this.channel_type_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.All));
//                        }
//                        return true;
//                }
//            }
//            return false;
//        }
//
//
//    }
//
//    /* renamed from: th.dtv.myctrl.ScanDialog.2 */
//    class OnClickedEdited2 implements OnKeyListener {
//        OnClickedEdited2() {
//        }
//
//        public boolean onKey(View v, int keyCode, KeyEvent event) {
//            if (event.getAction() == 0) {
//                switch (keyCode) {
//                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                        if (ScanDialog.this.servicetype_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.All))) {
//                            ScanDialog.this.servicetype_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.Radio));
//                            return true;
//                        } else if (ScanDialog.this.servicetype_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.Radio))) {
//                            ScanDialog.this.servicetype_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.TV));
//                            return true;
//                        } else if (!ScanDialog.this.servicetype_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.TV))) {
//                            return true;
//                        } else {
//                            ScanDialog.this.servicetype_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.All));
//                            return true;
//                        }
//                    case MW.RET_MEMORY_ERROR /*22*/:
//                        if (ScanDialog.this.servicetype_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.All))) {
//                            ScanDialog.this.servicetype_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.TV));
//                            return true;
//                        } else if (ScanDialog.this.servicetype_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.TV))) {
//                            ScanDialog.this.servicetype_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.Radio));
//                            return true;
//                        } else if (!ScanDialog.this.servicetype_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.Radio))) {
//                            return true;
//                        } else {
//                            ScanDialog.this.servicetype_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.All));
//                            return true;
//                        }
//                }
//            }
//            return false;
//        }
//
//    }
//
//    /* renamed from: th.dtv.myctrl.ScanDialog.3 */
//    class C03103 implements OnKeyListener {
//        C03103() {
//        }
//
//        public boolean onKey(View v, int keyCode, KeyEvent event) {
//            if (event.getAction() == 0) {
//                switch (keyCode) {
//                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
//                        if (ScanDialog.this.scan_type_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.manual_scan))) {
//                            ScanDialog.this.scan_type_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.auto_scan));
//                            return true;
//                        } else if (!ScanDialog.this.scan_type_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.auto_scan))) {
//                            return true;
//                        } else {
//                            ScanDialog.this.scan_type_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.manual_scan));
//                            return true;
//                        }
//                    case MW.RET_MEMORY_ERROR /*22*/:
//                        if (ScanDialog.this.scan_type_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.manual_scan))) {
//                            ScanDialog.this.scan_type_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.auto_scan));
//                            return true;
//                        } else if (!ScanDialog.this.scan_type_opt.getText().equals(ScanDialog.this.mContext.getResources().getString(R.string.auto_scan))) {
//                            return true;
//                        } else {
//                            ScanDialog.this.scan_type_opt.setText(ScanDialog.this.mContext.getResources().getString(R.string.manual_scan));
//                            return true;
//                        }
//                }
//            }
//            return false;
//        }
//    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.satellite_scan_dialog);
        this.mInflater = LayoutInflater.from(this.mContext);
        this.mLayoutView = this.mInflater.inflate(R.layout.satellite_scan_dialog, null);
        this.nit_search_ll = (LinearLayout) findViewById(R.id.nit_search_ll);
        this.nit_search_ll.setVisibility(View.INVISIBLE);
        this.channel_type_opt = (TextView) findViewById(R.id.channel_type_opt);
        this.servicetype_opt = (TextView) findViewById(R.id.servicetype_opt);
        this.scan_type_opt = (TextView) findViewById(R.id.scan_type_opt);
        if (SETTINGS.get_search_service_type() == 0) {
            this.channel_type_opt.setText(this.mContext.getResources().getString(R.string.All));
        } else {
            this.channel_type_opt.setText(this.mContext.getResources().getString(R.string.FTAOnly));
        }
        if (SETTINGS.get_search_tv_type() == 0) {
            this.servicetype_opt.setText(this.mContext.getResources().getString(R.string.All));
        } else if (SETTINGS.get_search_tv_type() == 1) {
            this.servicetype_opt.setText(this.mContext.getResources().getString(R.string.TV));
        } else if (SETTINGS.get_search_tv_type() == 2) {
            this.servicetype_opt.setText(this.mContext.getResources().getString(R.string.Radio));
        }
//        this.channel_type_opt.setOnClickListener(new OnClickEdite());
//        this.servicetype_opt.setOnClickListener(new OnClickedEdited2());
//        this.scan_type_opt.setOnClickListener(new C03103());
        this.scan_type_opt.setText(this.mContext.getResources().getString(R.string.manual_scan));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != 0) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    return true;
                default:
                    break;
            }
        }
        switch (keyCode) {
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                if (this.scan_type_opt.isFocused()) {
                    this.servicetype_opt.requestFocus();
                    return true;
                }
                break;
            case MW.RET_INVALID_TP_INDEX /*20*/:
                if (this.servicetype_opt.isFocused()) {
                    this.scan_type_opt.requestFocus();
                    return true;
                }
                break;
            case MW.RET_INVALID_TYPE /*23*/:
                int service_type;
                int tv_type = 0;
                int scan_type = 0;
                if (this.channel_type_opt.getText().toString().equals(this.mContext.getResources().getString(R.string.All))) {
                    service_type = 0;
                } else {
                    service_type = 1;
                }
                if (this.servicetype_opt.getText().toString().equals(this.mContext.getResources().getString(R.string.All))) {
                    tv_type = 0;
                } else if (this.servicetype_opt.getText().toString().equals(this.mContext.getResources().getString(R.string.TV))) {
                    tv_type = 1;
                } else if (this.servicetype_opt.getText().toString().equals(this.mContext.getResources().getString(R.string.Radio))) {
                    tv_type = 2;
                }
                if (this.scan_type_opt.getText().toString().equals(this.mContext.getResources().getString(R.string.auto_scan))) {
                    scan_type = 1;
                } else if (this.scan_type_opt.getText().toString().equals(this.mContext.getResources().getString(R.string.manual_scan))) {
                    scan_type = 2;
                }
                SETTINGS.set_search_service_type(service_type);
                SETTINGS.set_search_tv_type(tv_type);
                if (this.mCb != null) {
                    this.mCb.DoCallBackEvent(scan_type);
                    Log.e("LEE", "scan call back");
                }
                dismiss();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public ScanDialog(Context context, int theme, ScanCallBack cb) {
        super(context, theme);
        this.mCb = null;
        this.mInflater = null;
        this.mContext = null;
        this.mLayoutView = null;
        this.mContext = context;
        this.mCb = cb;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }
}
