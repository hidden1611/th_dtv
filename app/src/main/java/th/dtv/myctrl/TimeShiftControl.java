package th.dtv.myctrl;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import th.dtv.MW;
import th.dtv.R;

public class TimeShiftControl extends LinearLayout {
    private RelativeLayout RL_RecordPlay;
    private boolean bShow;
    private Handler localMsgHandler;
    private Context mContext;
    private LayoutParams mDecorLayoutParams;
    private TextView play_bottom_current_time;
    private TextView play_bottom_duration;
    private SeekBar play_bottom_seekbar;
    private TextView play_bottom_videoName;
    private TextView play_bottom_videoNum;
    private TextView play_speed;
    private ImageView play_status_img;
    private WindowManager wm;

    class LOCALMessageHandler extends Handler {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    TimeShiftControl.this.hideTimeshiftInfoView(0);
                default:
            }
        }
    }

    public boolean isTimeshiftInfoBarShow() {
        if (this.RL_RecordPlay.getVisibility() == VISIBLE) {
            return true;
        }
        return false;
    }

    public void showTimeshiftInfoView() {
        this.RL_RecordPlay.setVisibility(VISIBLE);
    }

    public void hideTimeshiftInfoView(int delayMS) {
        System.out.println("hideTimeshiftInfoView === info");
        this.localMsgHandler.removeMessages(0);
        if (delayMS > 0) {
            this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(0), (long) delayMS);
        } else {
            this.RL_RecordPlay.setVisibility(INVISIBLE);
        }
    }

    public TimeShiftControl(Context context) {
        super(context);
        this.mContext = null;
        this.bShow = false;
        this.mContext = context;
        init();
    }

    public TimeShiftControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = null;
        this.bShow = false;
        this.mContext = context;
        init();
    }

    public TimeShiftControl(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = null;
        this.bShow = false;
        this.mContext = context;
        init();
    }

    private void initFloatingWindowLayout() {
//        this.wm = (WindowManager) this.mContext.getSystemService("window");
//        this.mDecorLayoutParams = new LayoutParams();
//        LayoutParams localLayoutParams = this.mDecorLayoutParams;
//        localLayoutParams.gravity = Gravity.CENTER;
//        localLayoutParams.height = WRAP_CONTENT;
//        localLayoutParams.format = -3;
//        localLayoutParams.dimAmount = 0.0f;
//        localLayoutParams.flags = (localLayoutParams.flags | 24) | 2;
//        localLayoutParams.windowAnimations = 17432576;
    }

    private void init() {
//        initFloatingWindowLayout();
//        View localView = ((LayoutInflater) this.mContext.g.inflate(R.layout.timeshift, this);
//        this.play_status_img = (ImageView) localView.findViewById(R.id.play_status_img);
//        this.play_bottom_videoNum = (TextView) localView.findViewById(R.id.play_bottom_videoNum);
//        this.play_bottom_videoName = (TextView) localView.findViewById(R.id.play_bottom_videoName);
//        this.play_bottom_current_time = (TextView) localView.findViewById(R.id.play_bottom_current_time);
//        this.play_bottom_duration = (TextView) localView.findViewById(R.id.play_bottom_duration);
//        this.play_bottom_seekbar = (SeekBar) localView.findViewById(R.id.play_bottom_seekbar);
//        this.play_speed = (TextView) localView.findViewById(R.id.play_speed);
//        this.RL_RecordPlay = (RelativeLayout) localView.findViewById(R.id.RL_RecordPlay);
//        this.play_bottom_current_time.setText("00:00:00");
//        this.play_bottom_duration.setText("00:00:00");
//        this.play_bottom_seekbar.setProgress(0);
//        this.play_status_img.setVisibility(View.INVISIBLE);
//        this.play_speed.setVisibility(View.INVISIBLE);
//        this.localMsgHandler = new LOCALMessageHandler();
    }

    public void show() {
        if (getParent() == null) {
            try {
                this.wm.addView(this, this.mDecorLayoutParams);
                this.bShow = true;
            } catch (Exception localException) {
                localException.printStackTrace();
                this.bShow = false;
            }
        }
    }

    public void dismiss() {
        if (getParent() != null) {
            this.wm.removeView(this);
            this.bShow = false;
        }
    }

    public void setChannelNum(int channelNum) {
        if (this.play_bottom_videoNum != null && channelNum >= 0) {
            this.play_bottom_videoNum.setText(channelNum + " ");
        }
    }

    public void setChannelName(String channelName) {
        if (this.play_bottom_videoName != null && channelName != null) {
            this.play_bottom_videoName.setText(channelName);
        }
    }

    public void setCurrentTime(String currentTime) {
        if (this.play_bottom_current_time != null && currentTime != null) {
            this.play_bottom_current_time.setText(currentTime);
        }
    }

    public void setDuration(String duration) {
        if (this.play_bottom_duration != null && duration != null) {
            this.play_bottom_duration.setText(duration);
        }
    }

    public void setSeekBarProgress(int progress) {
        if (this.play_bottom_seekbar != null && progress >= 0) {
            this.play_bottom_seekbar.setProgress(progress);
        }
    }

    public void setSeekBarSecondProgress(int secondaryProgress) {
        if (this.play_bottom_seekbar != null && secondaryProgress >= 0) {
            this.play_bottom_seekbar.setSecondaryProgress(secondaryProgress);
        }
    }

    public void setSeekBarMax(int max) {
        if (this.play_bottom_seekbar != null && max >= 0) {
            this.play_bottom_seekbar.setMax(max);
        }
    }

    public void showTimeShiftLogo() {
//        if (this.play_status_img != null) {
//            this.play_speed.setVisibility(View.INVISIBLE);
//            this.play_status_img.setImageResource(R.drawable.record);
//            this.play_status_img.setVisibility(View.VISIBLE);
//        }
    }

    public void showPlayStatusAndSpeed(int status, int speed) {
//        if (this.play_speed != null && speed >= 0) {
//            if (status == 1) {
//                this.play_status_img.setImageResource(R.drawable.osd_rewind_hl);
//                this.play_status_img.setVisibility(View.VISIBLE);
//                this.play_speed.setText("X" + speed);
//                this.play_speed.setVisibility(View.VISIBLE);
//            } else if (status == 2) {
//                this.play_status_img.setImageResource(R.drawable.osd_forward_hl);
//                this.play_status_img.setVisibility(View.VISIBLE);
//                this.play_speed.setText("X" + speed);
//                this.play_speed.setVisibility(View.VISIBLE);
//            } else if (status == 3) {
//                this.play_status_img.setImageResource(R.drawable.osd_pause_hl);
//                this.play_speed.setVisibility(View.INVISIBLE);
//            } else if (status == 4) {
//                this.play_status_img.setImageResource(R.drawable.record);
//                this.play_status_img.setVisibility(View.VISIBLE);
//                this.play_speed.setVisibility(View.INVISIBLE);
//            } else {
//                this.play_status_img.setVisibility(View.INVISIBLE);
//                this.play_speed.setVisibility(View.INVISIBLE);
//            }
//        }
    }
}
