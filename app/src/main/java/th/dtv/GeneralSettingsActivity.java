package th.dtv;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import java.util.ArrayList;
import th.dtv.mw_data.dvb_t_area;

public class GeneralSettingsActivity extends DtvBaseActivity {
    private int System_type;
    private String TAG;
    private ComboLayout ant_power;
    private dvb_t_area areaInfo;
    private ComboLayout country_set;
    private ComboLayout lcn_set;
    private ComboLayout mAspectModeLinearLayout;
    private ComboLayout mBannerTimeoutLinearLayout;
    private Context mContext;
    private ComboLayout mFirstAudioLangueLinearLayout;
    private ComboLayout mSecondAudioLangueLinearLayout;
    private ComboLayout mSubtitleDisplayLinearLayout;
    private ComboLayout mSubtitleLangueLinearLayout;
    private ComboLayout mSwitchModeLinearLayout;
    private ComboLayout mTeletextLangueLinearLayout;

    /* renamed from: th.dtv.GeneralSettingsActivity.1 */
    class C00611 implements OnItemClickListener {
        C00611() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (SETTINGS.get_subtitle_default_on()) {
                SETTINGS.set_subtitle_default_on(false);
            } else {
                SETTINGS.set_subtitle_default_on(true);
            }
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.2 */
    class C00622 implements OnItemClickListener {
        C00622() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_banner_timeout(position);
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.3 */
    class C00633 implements OnItemClickListener {
        C00633() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_video_screen_format(position);
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.4 */
    class C00644 implements OnItemClickListener {
        C00644() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_first_audio_language(GeneralSettingsActivity.this.LanguageIndex2Str(position), true);
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.5 */
    class C00655 implements OnItemClickListener {
        C00655() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_second_audio_language(GeneralSettingsActivity.this.LanguageIndex2Str(position), true);
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.6 */
    class C00666 implements OnItemClickListener {
        C00666() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_subtitle_language(GeneralSettingsActivity.this.LanguageIndex2Str(position));
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.7 */
    class C00677 implements OnItemClickListener {
        C00677() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_teletext_language(GeneralSettingsActivity.this.LanguageIndex2Str(position));
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.8 */
    class C00688 implements OnItemClickListener {
        C00688() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_switch_mode(position);
        }
    }

    /* renamed from: th.dtv.GeneralSettingsActivity.9 */
    class C00699 implements OnItemClickListener {
        C00699() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            switch (position) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    SETTINGS.set_lcn_onoff(false);
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    SETTINGS.set_lcn_onoff(true);
                default:
            }
        }
    }

    public GeneralSettingsActivity() {
        this.TAG = "GeneralSettingsActivity";
        this.mContext = null;
        this.System_type = 0;
        this.areaInfo = new dvb_t_area();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.dtv_settings_general);
        this.mContext = this;
        this.System_type = MW.get_system_tuner_config();
        initView();
    }

    private void initView() {
        int i;
        this.country_set = (ComboLayout) findViewById(R.id.Country_Set);
        this.ant_power = (ComboLayout) findViewById(R.id.Ant_Power);
        this.lcn_set = (ComboLayout) findViewById(R.id.LCN_Set);
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getResources().getString(R.string.off));
        items.add(getResources().getString(R.string.on));
        this.mSubtitleDisplayLinearLayout = (ComboLayout) findViewById(R.id.subtitle_display_linearlayout);
        ComboLayout comboLayout = this.mSubtitleDisplayLinearLayout;
        if (SETTINGS.get_subtitle_default_on()) {
            i = 1;
        } else {
            i = 0;
        }
        comboLayout.initView((int) R.string.subtitle_display, items, i, new C00611());
        this.mBannerTimeoutLinearLayout = (ComboLayout) findViewById(R.id.banner_timeout_linearlayout);
        this.mBannerTimeoutLinearLayout.initView((int) R.string.banner_timeout, (int) R.array.banner_timeout_entries, SETTINGS.get_banner_timeout(), new C00622());
        ArrayList aspect_mode = new ArrayList();
        aspect_mode.clear();
        aspect_mode.add(getResources().getString(R.string.Auto));
        aspect_mode.add(getResources().getString(R.string.Full_stretch));
        aspect_mode.add(getResources().getString(R.string.Rate4_3));
        aspect_mode.add(getResources().getString(R.string.Rate16_9));
        this.mAspectModeLinearLayout = (ComboLayout) findViewById(R.id.aspect_mode_linearlayout);
        this.mAspectModeLinearLayout.initView((int) R.string.aspect_mode, aspect_mode, SETTINGS.get_video_screen_format(), new C00633());
        this.mFirstAudioLangueLinearLayout = (ComboLayout) findViewById(R.id.first_audio_language_linearlayout);
        this.mFirstAudioLangueLinearLayout.initView((int) R.string.first_audio_language, (int) R.array.language_entries, LanguageStr2Index(SETTINGS.get_first_audio_language()), new C00644());
        this.mSecondAudioLangueLinearLayout = (ComboLayout) findViewById(R.id.second_audio_language_linearlayout);
        this.mSecondAudioLangueLinearLayout.initView((int) R.string.second_audio_language, (int) R.array.language_entries, LanguageStr2Index(SETTINGS.get_second_audio_language()), new C00655());
        this.mSubtitleLangueLinearLayout = (ComboLayout) findViewById(R.id.subtitle_language_linearlayout);
        this.mSubtitleLangueLinearLayout.initView((int) R.string.subtitle_language, (int) R.array.language_entries, LanguageStr2Index(SETTINGS.get_subtitle_language()), new C00666());
        this.mTeletextLangueLinearLayout = (ComboLayout) findViewById(R.id.teletext_language_linearlayout);
        this.mTeletextLangueLinearLayout.initView((int) R.string.teletext_language, (int) R.array.language_entries, LanguageStr2Index(SETTINGS.get_teletext_language()), new C00677());
        this.mTeletextLangueLinearLayout.setVisibility(View.GONE);
        this.mSwitchModeLinearLayout = (ComboLayout) findViewById(R.id.switch_mode_linearlayout);
        ArrayList switch_mode = new ArrayList();
        switch_mode.clear();
        switch_mode.add(getResources().getString(R.string.blank));
        switch_mode.add(getResources().getString(R.string.freeze));
        this.mSwitchModeLinearLayout.initView((int) R.string.switch_mode, switch_mode, SETTINGS.get_switch_mode(), new C00688());
        if (this.System_type == 1) {
            InitAnt_Power();
            InitCountry();
            InitLCN();
        }
    }

    private void InitLCN() {
        int Status;
        this.lcn_set.setVisibility(View.VISIBLE);
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getString(R.string.off));
        items.add(getString(R.string.on));
        boolean IsOn = SETTINGS.get_lcn_onoff();
        SETTINGS.set_lcn_onoff(IsOn);
        if (IsOn) {
            Status = 1;
        } else {
            Status = 0;
        }
        this.lcn_set.initView((int) R.string.LCN, items, Status, new C00699());
    }

    private void InitCountry() {
        this.country_set.setVisibility(View.VISIBLE);
        int total_count = MW.db_dvb_t_get_area_count();
        ArrayList items = new ArrayList();
        items.clear();
        for (int i = 0; i < total_count; i++) {
            if (MW.db_dvb_t_get_area_info(i, this.areaInfo) == 0) {
                items.add(SETTINGS.get_area_string_by_index(this, this.areaInfo.area_index));
            }
        }
        this.country_set.initView((int) R.string.Country_Set, items, SETTINGS.get_current_area_index(), new OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                SETTINGS.set_current_area_index(position);
            }
        });
    }

    private void InitAnt_Power() {
        this.ant_power.setVisibility(View.VISIBLE);
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getString(R.string.off));
        items.add(getString(R.string.on));
        SETTINGS.set_ant_power_status(SETTINGS.get_ant_power_status());
        this.ant_power.initView((int) R.string.Ant_Power, items, SETTINGS.get_ant_power_status(), new OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                SETTINGS.set_ant_power_status(position);
            }
        });
    }

    public void onStart() {
        Log.d(this.TAG, "onStart() ");
        super.onStart();
    }

    public void onResume() {
        Log.d(this.TAG, "onResume() ");
        super.onResume();
        SETTINGS.send_led_msg("NENU");
    }

    public void onPause() {
        Log.d(this.TAG, "onPause() ");
        super.onPause();
    }

    public void onDestroy() {
        Log.d(this.TAG, "onDestroy() ");
        super.onDestroy();
    }

    private int LanguageStr2Index(String lang) {
        int index = 0;
        String[] arr$ = getResources().getStringArray(R.array.language_no_tranlate_entries);
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$ && !arr$[i$].endsWith(lang)) {
            index++;
            i$++;
        }
        return index;
    }

    private String LanguageIndex2Str(int index) {
        String lang = "eng";
        String[] secStrArray = getResources().getStringArray(R.array.language_no_tranlate_entries);
        if (index < secStrArray.length) {
            return secStrArray[index];
        }
        return lang;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                if (this.mSubtitleDisplayLinearLayout.isFocused()) {
                    if (this.lcn_set.getVisibility() != 8) {
                        this.lcn_set.requestFocus();
                        return true;
                    }
                    this.mSubtitleLangueLinearLayout.requestFocus();
                    return true;
                } else if (this.ant_power.isFocused()) {
                    this.mSubtitleLangueLinearLayout.requestFocus();
                    return true;
                }
                break;
            case MW.RET_INVALID_TP_INDEX /*20*/:
                if (this.mSubtitleLangueLinearLayout.isFocused()) {
                    if (this.ant_power.getVisibility() != 8) {
                        this.ant_power.requestFocus();
                        return true;
                    }
                    this.mSubtitleDisplayLinearLayout.requestFocus();
                    return true;
                }
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
