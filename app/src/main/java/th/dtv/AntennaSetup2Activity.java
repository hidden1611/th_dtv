package th.dtv;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.tuner_signal_status;

public class AntennaSetup2Activity extends DtvBaseActivity {
    private LinearLayout DiseqcPort_item;
    private LinearLayout DiseqcType_item;
    private LinearLayout LNBPower_item;
    private LinearLayout LNBType_item;
    private LinearLayout Onoff22K_item;
    int currentSatIndex;
    int currentSatPos;
    int currentTpIndex;
    boolean fSaveSatTpFlag;
    private int[][] lnbTypeList;
    private AudioManager mAudioManager;
    private Context mContext;
    private Handler mwMsgHandler;
    int pre22KHz_ItemType;
    int preDiseqcPort_ItemType;
    ProgressBar prg_signalQuality;
    ProgressBar prg_signalStrength;
    dvb_s_sat satInfo;
    private ComboLayout spn_22Khz;
    private ComboLayout spn_diseqcPort;
    private ComboLayout spn_diseqcType;
    private ComboLayout spn_lnbPower;
    private ComboLayout spn_lnbType;
    dvb_s_tp tpInfo;
    TextView txv_signalQuality;
    TextView txv_signalStrength;

    /* renamed from: th.dtv.AntennaSetup2Activity.1 */
    class C00061 implements OnItemClickListener {
        C00061() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            AntennaSetup2Activity.this.satInfo.lnb_type = AntennaSetup2Activity.this.lnbTypeList[position][0];
            AntennaSetup2Activity.this.satInfo.lnb_low = AntennaSetup2Activity.this.lnbTypeList[position][1];
            AntennaSetup2Activity.this.satInfo.lnb_high = AntennaSetup2Activity.this.lnbTypeList[position][2];
            AntennaSetup2Activity.this.RefreshLNBType();
            AntennaSetup2Activity.this.fSaveSatTpFlag = true;
        }
    }

    /* renamed from: th.dtv.AntennaSetup2Activity.2 */
    class C00072 implements OnItemClickListener {
        C00072() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            AntennaSetup2Activity.this.satInfo.k22 = position;
            MW.db_dvb_s_set_sat_info(1, AntennaSetup2Activity.this.satInfo);
            AntennaSetup2Activity.this.LockCurrentTP();
            AntennaSetup2Activity.this.fSaveSatTpFlag = true;
        }
    }

    /* renamed from: th.dtv.AntennaSetup2Activity.3 */
    class C00083 implements OnItemClickListener {
        C00083() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (AntennaSetup2Activity.this.satInfo.lnb_power != position) {
                AntennaSetup2Activity.this.satInfo.lnb_power = position;
                MW.db_dvb_s_set_sat_info(1, AntennaSetup2Activity.this.satInfo);
                AntennaSetup2Activity.this.LockCurrentTP();
                AntennaSetup2Activity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetup2Activity.4 */
    class C00094 implements OnItemClickListener {
        C00094() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (AntennaSetup2Activity.this.satInfo.diseqc_type != position) {
                AntennaSetup2Activity.this.satInfo.diseqc_type = position;
                AntennaSetup2Activity.this.satInfo.diseqc_port = 0;
                MW.db_dvb_s_set_sat_info(1, AntennaSetup2Activity.this.satInfo);
                AntennaSetup2Activity.this.LockCurrentTP();
                AntennaSetup2Activity.this.RefreshBackGround();
                AntennaSetup2Activity.this.fSaveSatTpFlag = true;
            }
        }
    }

    /* renamed from: th.dtv.AntennaSetup2Activity.5 */
    class C00105 implements OnItemClickListener {
        C00105() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (AntennaSetup2Activity.this.satInfo.diseqc_port != position) {
                AntennaSetup2Activity.this.satInfo.diseqc_port = position;
                MW.db_dvb_s_set_sat_info(1, AntennaSetup2Activity.this.satInfo);
                AntennaSetup2Activity.this.LockCurrentTP();
                AntennaSetup2Activity.this.fSaveSatTpFlag = true;
            }
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                    tuner_signal_status paramsInfo = (tuner_signal_status) msg.obj;
                    if (paramsInfo != null) {
                        if (paramsInfo.locked) {
                            AntennaSetup2Activity.this.prg_signalStrength.setProgressDrawable(AntennaSetup2Activity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
                            AntennaSetup2Activity.this.prg_signalQuality.setProgressDrawable(AntennaSetup2Activity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
                            SETTINGS.set_green_led(true);
                        } else {
                            AntennaSetup2Activity.this.prg_signalStrength.setProgressDrawable(AntennaSetup2Activity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
                            AntennaSetup2Activity.this.prg_signalQuality.setProgressDrawable(AntennaSetup2Activity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
                            SETTINGS.set_green_led(false);
                        }
                        AntennaSetup2Activity.this.prg_signalStrength.setProgress(paramsInfo.strength);
                        AntennaSetup2Activity.this.prg_signalQuality.setProgress(paramsInfo.quality);
                        if (paramsInfo.error) {
                            AntennaSetup2Activity.this.txv_signalStrength.setText("I2C");
                            AntennaSetup2Activity.this.txv_signalQuality.setText("Error");
                            return;
                        }
                        AntennaSetup2Activity.this.txv_signalStrength.setText(paramsInfo.strength + "%");
                        AntennaSetup2Activity.this.txv_signalQuality.setText(paramsInfo.quality + "%");
                    }
                default:
            }
        }
    }

    public AntennaSetup2Activity() {
        this.lnbTypeList = new int[][]{new int[]{0, 5150, 5150}, new int[]{0, 5750, 5750}, new int[]{0, 5950, 5950}, new int[]{0, 9750, 9750}, new int[]{0, 10000, 10000}, new int[]{0, 10050, 10050}, new int[]{0, 10450, 10450}, new int[]{0, 10600, 10600}, new int[]{0, 10700, 10700}, new int[]{0, 10750, 10750}, new int[]{0, 11250, 11250}, new int[]{0, 11300, 11300}, new int[]{1, 5150, 5750}, new int[]{2, 5750, 5150}, new int[]{3, 9750, 10550}, new int[]{3, 9750, 10600}, new int[]{3, 9750, 10700}, new int[]{3, 9750, 10750}, new int[]{4, 9750, 10600}};
        this.satInfo = new dvb_s_sat();
        this.tpInfo = new dvb_s_tp();
        this.fSaveSatTpFlag = false;
        this.currentSatIndex = -1;
        this.currentTpIndex = -1;
        this.currentSatPos = 0;
        this.pre22KHz_ItemType = -1;
        this.preDiseqcPort_ItemType = -1;
        this.mContext = null;
    }

    private void LockCurrentTP() {
        MW.tuner_dvb_s_lock_tp(1, this.currentSatIndex, this.currentTpIndex, false);
    }

    private void RefreshBackGround() {
        if (this.spn_lnbType.isFocused()) {
            this.LNBType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.LNBType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_22Khz.isFocused()) {
            this.Onoff22K_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.Onoff22K_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_lnbPower.isFocused()) {
            this.LNBPower_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.LNBPower_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_diseqcType.isFocused()) {
            this.DiseqcType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.DiseqcType_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        if (this.spn_diseqcPort.isFocused()) {
            this.DiseqcPort_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
        } else {
            this.DiseqcPort_item.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
        }
        RefreshSatelliteSpecailConfigs();
    }

    private void initLinearLayout() {
        this.LNBType_item = (LinearLayout) findViewById(R.id.LNBType_item);
        this.Onoff22K_item = (LinearLayout) findViewById(R.id.Onoff22K_item);
        this.LNBPower_item = (LinearLayout) findViewById(R.id.LNBPower_item);
        this.DiseqcType_item = (LinearLayout) findViewById(R.id.DiseqcType_item);
        this.DiseqcPort_item = (LinearLayout) findViewById(R.id.DiseqcPort_item);
    }

    private void FillLNBTypeItems() {
        int i;
        ArrayList items = new ArrayList();
        items.clear();
        for (i = 0; i < this.lnbTypeList.length; i++) {
            switch (this.lnbTypeList[i][0]) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    items.add("" + this.lnbTypeList[i][1]);
                    break;
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    items.add("" + this.lnbTypeList[i][1] + " - " + this.lnbTypeList[i][2]);
                    break;
                case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                    items.add("aaa" + " ( " + this.lnbTypeList[i][1] + " - " + this.lnbTypeList[i][2] + " )");
                    break;
                default:
                    break;
            }
        }
        if (this.satInfo != null) {
            int initIndex = 0;
            i = 0;
            while (i < this.lnbTypeList.length) {
                if (this.lnbTypeList[i][0] == this.satInfo.lnb_type && this.lnbTypeList[i][1] == this.satInfo.lnb_low && this.lnbTypeList[i][2] == this.satInfo.lnb_high) {
                    initIndex = i;
                    this.spn_lnbType.initView((int) R.string.LNBType, items, initIndex, new C00061());
                }
                i++;
            }
            this.spn_lnbType.initView((int) R.string.LNBType, items, initIndex, new C00061());
        }
    }

    private void Fill22KHzItems() {
        ArrayList items = new ArrayList();
        items.clear();
        if (this.satInfo.lnb_type != 3) {
            this.pre22KHz_ItemType = 0;
            items.add(getString(R.string.off));
            items.add(getString(R.string.on));
        } else {
            this.pre22KHz_ItemType = 1;
            items.add(getString(R.string.Auto));
            items.add(getString(R.string.Auto));
        }
        if (this.satInfo != null) {
            this.spn_22Khz.initView( R.string.add, items, this.satInfo.k22, new C00072());
        }
    }

    private void RefreshSatelliteSpecailConfigs() {
        if (this.satInfo.lnb_type == 4) {
            this.Onoff22K_item.setVisibility(View.VISIBLE);
            this.LNBPower_item.setVisibility(View.VISIBLE);
            this.DiseqcType_item.setVisibility(View.VISIBLE);
            this.DiseqcPort_item.setVisibility(View.VISIBLE);
            return;
        }
        this.Onoff22K_item.setVisibility(View.VISIBLE);
        this.LNBPower_item.setVisibility(View.VISIBLE);
        this.DiseqcType_item.setVisibility(View.VISIBLE);
        Fill22KHzItems();
        if (this.satInfo.lnb_type == 3 || this.satInfo.lnb_type == 4) {
            this.spn_22Khz.setEnabled(false);
            this.spn_22Khz.setFocusable(false);
            this.Onoff22K_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        } else {
            this.spn_22Khz.setEnabled(true);
            this.spn_22Khz.setFocusable(true);
        }
        Fill22KHzItems();
        RefreshDiseqcTypeConfigs();
    }

    private void RefreshDiseqcTypeConfigs() {
        if (this.satInfo.diseqc_type <= 2) {
            this.DiseqcPort_item.setVisibility(View.VISIBLE);
            FillDiseqcPortItems();
            if (this.satInfo.diseqc_type == 0) {
                this.spn_diseqcPort.setEnabled(false);
                this.spn_diseqcPort.setFocusable(false);
                this.DiseqcPort_item.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                return;
            }
            this.spn_diseqcPort.setEnabled(true);
            this.spn_diseqcPort.setFocusable(true);
            return;
        }
        this.DiseqcPort_item.setVisibility(View.VISIBLE);
    }

    private void FillLNBPowerItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getString(R.string.off));
        items.add(getString(R.string._13_18_V));
        items.add(getString(R.string._13V));
        items.add(getString(R.string._18V));
        if (this.satInfo != null) {
            this.spn_lnbPower.initView((int) R.string.LNBPower, items, this.satInfo.lnb_power, new C00083());
        }
    }

    private void FillDiseqcTypeItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getString(R.string.None));
        items.add("1.0");
        items.add("1.1");
        if (this.satInfo != null) {
            this.spn_diseqcType.initView((int) R.string.DiseqcType, items, this.satInfo.diseqc_type, new C00094());
        }
    }

    private void FillDiseqcPortItems() {
        ArrayList items = new ArrayList();
        items.clear();
        int i;
        if (this.satInfo.diseqc_type == 0) {
            this.preDiseqcPort_ItemType = 0;
            for (i = 0; i <= 3; i++) {
                items.add(getString(R.string.None));
            }
        } else if (this.satInfo.diseqc_type == 1) {
            this.preDiseqcPort_ItemType = 1;
            for (i = 0; i <= 3; i++) {
                items.add(Integer.toString(i + 1));
            }
        } else {
            this.preDiseqcPort_ItemType = 2;
            for (i = 0; i < 16; i++) {
                items.add(Integer.toString(i + 1));
            }
        }
        if (this.satInfo != null) {
            this.spn_diseqcPort.initView((int) R.string.DiseqcPort, items, this.satInfo.diseqc_port, new C00105());
        }
    }

    private void RefreshSatelliteInfo(boolean bResetTpIndex) {
        if (MW.db_dvb_s_get_sat_info(1, this.currentSatIndex, this.satInfo) == 0) {
            RefreshBackGround();
            FillLNBPowerItems();
            FillDiseqcTypeItems();
            FillLNBTypeItems();
            if (bResetTpIndex) {
                this.currentTpIndex = 0;
            }
        }
    }

    private void RefreshLNBType() {
        if (this.satInfo != null) {
            MW.db_dvb_s_set_sat_info(1, this.satInfo);
            LockCurrentTP();
            RefreshBackGround();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    this.mAudioManager.playSoundEffect(0);
                    if (!this.spn_lnbType.hasFocus()) {
                        if (!this.spn_22Khz.hasFocus()) {
                            if (!this.spn_lnbPower.hasFocus()) {
                                if (!this.spn_diseqcType.hasFocus()) {
                                    if (!this.spn_diseqcPort.hasFocus()) {
                                        RefreshBackGround();
                                        break;
                                    }
                                    this.spn_diseqcType.requestFocus();
                                    RefreshBackGround();
                                    return true;
                                }
                                this.spn_lnbPower.requestFocus();
                                RefreshBackGround();
                                return true;
                            }
                            if (this.spn_22Khz.isEnabled()) {
                                this.spn_22Khz.requestFocus();
                            } else {
                                this.LNBType_item.requestFocus();
                            }
                            RefreshBackGround();
                            return true;
                        }
                        this.spn_lnbType.requestFocus();
                        RefreshBackGround();
                        return true;
                    }
                    if (this.DiseqcPort_item.getVisibility() == View.VISIBLE) {
                        if (this.satInfo.diseqc_type != 0) {
                            this.spn_diseqcPort.requestFocus();
                        } else {
                            this.spn_diseqcType.requestFocus();
                        }
                    }
                    RefreshBackGround();
                    return true;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    this.mAudioManager.playSoundEffect(0);
                    if (this.spn_diseqcType.hasFocus()) {
                        if (this.DiseqcPort_item.getVisibility() != View.VISIBLE) {
                            this.spn_lnbType.requestFocus();
                        } else if (this.satInfo.diseqc_type == 0) {
                            this.spn_lnbType.requestFocus();
                        } else {
                            this.spn_diseqcPort.requestFocus();
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_diseqcPort.hasFocus()) {
                        this.spn_lnbType.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_lnbType.hasFocus()) {
                        if (this.Onoff22K_item.getVisibility() == View.VISIBLE) {
                            if (this.satInfo.lnb_type == 3) {
                                this.spn_lnbPower.requestFocus();
                            } else {
                                this.spn_22Khz.requestFocus();
                            }
                        }
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_22Khz.hasFocus()) {
                        this.spn_lnbPower.requestFocus();
                        RefreshBackGround();
                        return true;
                    } else if (this.spn_lnbPower.hasFocus()) {
                        this.spn_diseqcType.requestFocus();
                        RefreshBackGround();
                        return true;
                    }
                    break;
            }
        }
        RefreshBackGround();
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    void ShowToastInformation(String text, int duration_mode) {
        LinearLayout toast_view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
        ((TextView) toast_view.findViewById(R.id.toast_text)).setText(text);
        Toast toast = new Toast(this);
//        if (duration_mode == 0) {
//            toast.setDuration(0);
//        } else if (duration_mode == 1) {
//            toast.setDuration(1);
//        }
        toast.setView(toast_view);
        toast.setGravity(17, 0, -70);
        toast.show();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        this.mContext = this;
        setContentView(R.layout.antenna2_setup);
        getWindow().setSoftInputMode(3);
        this.mAudioManager = (AudioManager) this.mContext.getSystemService(Context.AUDIO_SERVICE);
        initLinearLayout();
        this.spn_lnbType = (ComboLayout) findViewById(R.id.Spinner_LNBType);
        this.spn_22Khz = (ComboLayout) findViewById(R.id.Spinner_22Khz);
        this.spn_lnbPower = (ComboLayout) findViewById(R.id.Spinner_LNBPower);
        this.spn_diseqcType = (ComboLayout) findViewById(R.id.Spinner_DiseqcType);
        this.spn_diseqcPort = (ComboLayout) findViewById(R.id.Spinner_DiseqcPort);
        this.prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
        this.prg_signalStrength.setMax(100);
        this.prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
        this.prg_signalQuality.setMax(100);
        this.prg_signalStrength.setProgress(0);
        this.prg_signalQuality.setProgress(0);
        this.txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
        this.txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);
        this.txv_signalStrength.setText("0%");
        this.txv_signalQuality.setText("0%");
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        this.spn_lnbType.requestFocus();
        Log.e("AntennaSetup2Activity", " onCreate");
    }

    private void RefreshAllData() {
        initData();
        if (MW.db_dvb_s_get_selected_sat_count(1) == 0) {
            ShowToastInformation(getResources().getString(R.string.no_selected_satellite), 0);
            finishMyself();
            return;
        }
        FillLNBTypeItems();
        FillLNBPowerItems();
        FillDiseqcTypeItems();
        RefreshSatelliteInfo(false);
    }

    private void initData() {
        if (MW.db_dvb_s_get_selected_sat_count(1) == 0) {
            ShowToastInformation(getResources().getString(R.string.no_selected_satellite), 0);
            finishMyself();
        } else if (MW.db_dvb_s_get_current_tp_info(1, this.tpInfo) == 0) {
            System.out.println(" tpInfo.sat_index  : " + this.tpInfo.sat_index);
            System.out.println(" tpInfo.tp_index  : " + this.tpInfo.tp_index);
            System.out.println(" tpInfo.sat_pos  : " + this.tpInfo.sat_pos);
            this.currentSatIndex = this.tpInfo.sat_index;
            this.currentSatPos = this.tpInfo.sat_pos;
            if (MW.db_dvb_s_get_tp_count(1, this.currentSatIndex) == 0) {
                this.currentTpIndex = -1;
                ShowToastInformation(getResources().getString(R.string.add_tp), 0);
                return;
            }
            this.currentTpIndex = this.tpInfo.tp_index;
        } else if (MW.db_dvb_s_get_current_sat_info(1, this.satInfo) == 0) {
            this.currentSatIndex = this.satInfo.sat_index;
            this.currentTpIndex = 0;
        } else {
            this.currentSatIndex = 0;
            this.currentTpIndex = 0;
        }
    }

    protected void onStart() {
        super.onStart();
        Log.e("AntennaSetup2Activity", " onStart");
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("AntennaSetup2Activity", " onNewIntent");
    }

    protected void onPause() {
        super.onPause();
        if (MW.db_dvb_s_get_current_tp_info(1, this.tpInfo) != 0) {
            MW.db_dvb_s_set_current_sat_tp(1, 0, 0);
        } else if (!(this.tpInfo.sat_index == this.currentSatIndex && this.tpInfo.tp_index == this.currentTpIndex)) {
            MW.db_dvb_s_set_current_sat_tp(1, this.currentSatIndex, this.currentTpIndex);
        }
        if (this.fSaveSatTpFlag) {
            this.fSaveSatTpFlag = false;
            MW.db_dvb_s_save_sat_tp(1);
        }
        enableMwMessageCallback(null);
        MW.tuner_unlock_tp(1, 0, false);
        Log.d("AntennaSetup2Activity", " onPause");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d("AntennaSetup2Activity", " onRestart");
    }

    protected void onResume() {
        Log.d("AntennaSetup2Activity", " onResume start");
        super.onResume();
        MW.tuner_unlock_tp(0, 0, false);
        RefreshAllData();
        SETTINGS.send_led_msg("NENU");
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(3, true);
        if (this.satInfo == null) {
            MW.tuner_unlock_tp(1, 0, false);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalStrength.setProgress(0);
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        } else if (this.satInfo.tp_count > 0) {
            LockCurrentTP();
        } else {
            MW.tuner_unlock_tp(1, 0, false);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalStrength.setProgress(0);
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        }
        Log.d("AntennaSetup2Activity", " onResume end");
    }

    protected void onStop() {
        super.onStop();
        Log.d("AntennaSetup2Activity", " onStop");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d("AntennaSetup2Activity", " onDestroy");
    }
}
