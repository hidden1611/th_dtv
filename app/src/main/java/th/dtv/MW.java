package th.dtv;

import th.dtv.mw_data.aud_info;
import th.dtv.mw_data.book_event_data;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.dvb_t_area;
import th.dtv.mw_data.dvb_t_tp;
import th.dtv.mw_data.epg_extended_event;
import th.dtv.mw_data.epg_pf_event;
import th.dtv.mw_data.epg_schedule_event;
import th.dtv.mw_data.pvr_record;
import th.dtv.mw_data.region_info;
import th.dtv.mw_data.service;
import th.dtv.mw_data.system_tuner_info;
import th.dtv.mw_data.ttx_sub_page_info;
import th.dtv.mw_data.tuner_signal_status;

public class MW {
    public static final int AUDIO_STREAM_TYPE_AAC = 4;
    public static final int AUDIO_STREAM_TYPE_AAC_LATM = 5;
    public static final int AUDIO_STREAM_TYPE_AC3 = 2;
    public static final int AUDIO_STREAM_TYPE_DTS = 6;
    public static final int AUDIO_STREAM_TYPE_EAC3 = 3;
    public static final int AUDIO_STREAM_TYPE_MPEG1 = 0;
    public static final int AUDIO_STREAM_TYPE_MPEG2 = 1;
    public static final int BOOK_EVENT_CONFLICT_MODE_PERIOD = 1;
    public static final int BOOK_EVENT_CONFLICT_MODE_START_TIME = 0;
    public static final int BOOK_MODE_DAILY = 1;
    public static final int BOOK_MODE_MONTHLY = 3;
    public static final int BOOK_MODE_ONCE = 0;
    public static final int BOOK_MODE_WEEKLY = 2;
    public static final int BOOK_STATUS_ACTION = 0;
    public static final int BOOK_STATUS_NOTIFY = 1;
    public static final int BOOK_TYPE_NONE = 0;
    public static final int BOOK_TYPE_PLAY = 1;
    public static final int BOOK_TYPE_POWER_ON = 4;
    public static final int BOOK_TYPE_RECORD = 2;
    public static final int BOOK_TYPE_STANDBY = 3;
    public static final int DYNAMIC_SERVICE_UPDATE_MODE_OFF = 0;
    public static final int DYNAMIC_SERVICE_UPDATE_MODE_PARAMETERS = 1;
    public static final int DYNAMIC_SERVICE_UPDATE_MODE_SERVICE = 2;
    public static final int EPG_EVENT_PLAYING_STATUS_EXPIRED = 0;
    public static final int EPG_EVENT_PLAYING_STATUS_NEW = 2;
    public static final int EPG_EVENT_PLAYING_STATUS_PLAYING = 1;
    public static final int EVENT_TYPE_BOOK_LIST_UPDATE = 10;
    public static final int EVENT_TYPE_BOOK_STATUS = 9;
    public static final int EVENT_TYPE_DATE_TIME = 4;
    public static final int EVENT_TYPE_EPG_CURRENT_NEXT_UPDATE = 5;
    public static final int EVENT_TYPE_EPG_SCHEDULE_UPDATE = 6;
    public static final int EVENT_TYPE_PLAY_STATUS = 0;
    public static final int EVENT_TYPE_PVR_STATUS = 8;
    public static final int EVENT_TYPE_SEARCH_STATUS = 1;
    public static final int EVENT_TYPE_SERVICE_STATUS_UPDATE = 11;
    public static final int EVENT_TYPE_TELETEXT_SUBTITLE_UPDATE = 7;
    public static final int EVENT_TYPE_TUNER_STATUS = 2;
    public static final int EVENT_TYPE_TUNER_STATUS_2 = 3;
    public static final int FRONT_END_TYPE_DVBS = 1;
    public static final int FRONT_END_TYPE_DVBS_2 = 2;
    public static final int FRONT_END_TYPE_DVBT = 3;
    public static final int FRONT_END_TYPE_DVBT_2 = 4;
    public static final int FRONT_END_TYPE_Undef = 0;
    public static final int HEMISPHERE_NORTH = 1;
    public static final int HEMISPHERE_SOUTH = 0;
    public static final int LNB_DISEQC_PORT_1 = 0;
    public static final int LNB_DISEQC_PORT_10 = 9;
    public static final int LNB_DISEQC_PORT_11 = 10;
    public static final int LNB_DISEQC_PORT_12 = 11;
    public static final int LNB_DISEQC_PORT_13 = 12;
    public static final int LNB_DISEQC_PORT_14 = 13;
    public static final int LNB_DISEQC_PORT_15 = 14;
    public static final int LNB_DISEQC_PORT_16 = 15;
    public static final int LNB_DISEQC_PORT_2 = 1;
    public static final int LNB_DISEQC_PORT_3 = 2;
    public static final int LNB_DISEQC_PORT_4 = 3;
    public static final int LNB_DISEQC_PORT_5 = 4;
    public static final int LNB_DISEQC_PORT_6 = 5;
    public static final int LNB_DISEQC_PORT_7 = 6;
    public static final int LNB_DISEQC_PORT_8 = 7;
    public static final int LNB_DISEQC_PORT_9 = 8;
    public static final int LNB_DISEQC_PORT_COUNT = 16;
    public static final int LNB_DISEQC_TYPE_NONE = 0;
    public static final int LNB_DISEQC_TYPE_V_1_0 = 1;
    public static final int LNB_DISEQC_TYPE_V_1_1 = 2;
    public static final int LNB_MOTOR_TYPE_NONE = 0;
    public static final int LNB_MOTOR_TYPE_V_1_2 = 1;
    public static final int LNB_MOTOR_TYPE_V_1_3 = 2;
    public static final int LNB_POL_H = 1;
    public static final int LNB_POL_V = 0;
    public static final int LNB_SCR_POSTION_A = 0;
    public static final int LNB_SCR_POSTION_B = 1;
    public static final int LNB_TYPE_C_DOUBLE_5150_5750 = 1;
    public static final int LNB_TYPE_C_DOUBLE_5750_5150 = 2;
    public static final int LNB_TYPE_SINGLE = 0;
    public static final int LNB_TYPE_UNICABLE = 4;
    public static final int LNB_TYPE_UNIVERSAL = 3;
    public static final int LOCATION_EAST = 0;
    public static final int LOCATION_WEST = 1;
    public static final int MAIN_TUNER_INDEX = 0;
    public static final int PLAY_STATUS_MOTOR_MOVING = 6;
    public static final int PLAY_STATUS_NO_AUDIO = 4;
    public static final int PLAY_STATUS_NO_CHANNEL = 1;
    public static final int PLAY_STATUS_NO_SIGNAL = 2;
    public static final int PLAY_STATUS_NO_VIDEO = 3;
    public static final int PLAY_STATUS_OK = 0;
    public static final int PLAY_STATUS_SCRAMBLE = 5;
    public static final int PVR_PLAYBACK_STATUS_EXIT = 5;
    public static final int PVR_PLAYBACK_STATUS_FFFB = 8;
    public static final int PVR_PLAYBACK_STATUS_INIT = 4;
    public static final int PVR_PLAYBACK_STATUS_PAUSE = 7;
    public static final int PVR_PLAYBACK_STATUS_PLAY = 6;
    public static final int PVR_PLAYBACK_STATUS_SEARCH_OK = 9;
    public static final int PVR_RECORD_STATUS_END = 1;
    public static final int PVR_RECORD_STATUS_ERROR = 3;
    public static final int PVR_RECORD_STATUS_START = 0;
    public static final int PVR_RECORD_STATUS_UPDATE_TIME = 2;
    public static final int PVR_TIMESHIFT_STATUS_EXIT = 11;
    public static final int PVR_TIMESHIFT_STATUS_FFFB = 14;
    public static final int PVR_TIMESHIFT_STATUS_INIT = 10;
    public static final int PVR_TIMESHIFT_STATUS_PAUSE = 13;
    public static final int PVR_TIMESHIFT_STATUS_PLAY = 12;
    public static final int PVR_TIMESHIFT_STATUS_SEARCH_OK = 15;
    public static final int RADIO_CH = 1;
    public static final int RET_ALLOC_OBJECT_FAILED = 35;
    public static final int RET_ALREADY_RECORDING = 41;
    public static final int RET_BOOK_CONFLICT_EVENT = 15;
    public static final int RET_BOOK_EVENT_INVALID = 14;
    public static final int RET_EVENT_NOT_FOUND = 16;
    public static final int RET_FUNCTION_NOT_SUPPORTTED = 40;
    public static final int RET_GET_BUFFER_FAILED = 37;
    public static final int RET_GET_JAVA_METHOD_FAILED = 39;
    public static final int RET_GET_OBJECT_FAILED = 36;
    public static final int RET_INVALID_AREA = 12;
    public static final int RET_INVALID_AREA_INDEX = 19;
    public static final int RET_INVALID_AREA_INDEX_LIST = 27;
    public static final int RET_INVALID_BOOK_MODE = 33;
    public static final int RET_INVALID_BOOK_TYPE = 32;
    public static final int RET_INVALID_DATA_LENGTH = 25;
    public static final int RET_INVALID_DATE_TIME = 34;
    public static final int RET_INVALID_OBJECT = 30;
    public static final int RET_INVALID_RECORD_INDEX = 21;
    public static final int RET_INVALID_RECORD_INDEX_LIST = 29;
    public static final int RET_INVALID_REGION_POS = 17;
    public static final int RET_INVALID_SAT = 11;
    public static final int RET_INVALID_SAT_INDEX = 18;
    public static final int RET_INVALID_SAT_INDEX_LIST = 26;
    public static final int RET_INVALID_SERVICE = 31;
    public static final int RET_INVALID_TELETEXT_COLOR = 24;
    public static final int RET_INVALID_TP = 13;
    public static final int RET_INVALID_TP_INDEX = 20;
    public static final int RET_INVALID_TP_INDEX_LIST = 28;
    public static final int RET_INVALID_TYPE = 23;
    public static final int RET_MEMORY_ERROR = 22;
    public static final int RET_NO_AUDIO_CHANNEL = 8;
    public static final int RET_NO_AUDIO_DATA = 4;
    public static final int RET_NO_CHANNEL = 1;
    public static final int RET_NO_DATA = 45;
    public static final int RET_NO_SIGNAL = 2;
    public static final int RET_NO_THIS_CHANNEL = 6;
    public static final int RET_NO_THIS_RECORD = 42;
    public static final int RET_NO_VIDEO_CHANNEL = 7;
    public static final int RET_NO_VIDEO_DATA = 3;
    public static final int RET_OK = 0;
    public static final int RET_PLAYBACK_FAILED = 43;
    public static final int RET_REC_ERR_BUSY = 47;
    public static final int RET_REC_ERR_CANNOT_ACCESS_FILE = 50;
    public static final int RET_REC_ERR_CANNOT_CREATE_THREAD = 46;
    public static final int RET_REC_ERR_CANNOT_OPEN_FILE = 48;
    public static final int RET_REC_ERR_CANNOT_WRITE_FILE = 49;
    public static final int RET_REC_ERR_DVR = 52;
    public static final int RET_REC_ERR_FILE_TOO_LARGE = 51;
    public static final int RET_SAT_EXIST = 9;
    public static final int RET_SCRAMBLE_CHANNEL = 5;
    public static final int RET_SERVICE_LOCK = 44;
    public static final int RET_SYSTEM_ERROR = 38;
    public static final int RET_TP_EXIST = 10;
    public static final int SAT_POSITION_EAST = 0;
    public static final int SAT_POSITION_WEST = 1;
    public static final int SEARCH_SERVICE_TYPE_ALL = 0;
    public static final int SEARCH_SERVICE_TYPE_FTA_ONLY = 1;
    public static final int SEARCH_STATUS_SAVE_DATA_END = 8;
    public static final int SEARCH_STATUS_SAVE_DATA_START = 7;
    public static final int SEARCH_STATUS_SEARCH_END = 9;
    public static final int SEARCH_STATUS_SEARCH_START = 0;
    public static final int SEARCH_STATUS_UPDATE_CHANNEL_LIST = 1;
    public static final int SEARCH_STATUS_UPDATE_DVB_S_BLIND_SCAN_PARAMS_INFO = 4;
    public static final int SEARCH_STATUS_UPDATE_DVB_S_BLIND_SCAN_PROGRESS = 3;
    public static final int SEARCH_STATUS_UPDATE_DVB_S_BLIND_SCAN_TUNNING_INFO = 2;
    public static final int SEARCH_STATUS_UPDATE_DVB_S_PARAMS_INFO = 5;
    public static final int SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO = 6;
    public static final int SEARCH_TV_TYPE_ALL = 0;
    public static final int SEARCH_TV_TYPE_RADIO_ONLY = 2;
    public static final int SEARCH_TV_TYPE_TV_ONLY = 1;
    public static final int SEARCH_TYPE_AUTO = 1;
    public static final int SEARCH_TYPE_BLIND = 0;
    public static final int SEARCH_TYPE_MANUAL = 2;
    public static final int SECOND_TUNER_INDEX = 1;
    public static final int SERVICE_SORT_TYPE_A_TO_Z = 1;
    public static final int SERVICE_SORT_TYPE_CAS = 8;
    public static final int SERVICE_SORT_TYPE_SATELLITE = 2;
    public static final int SERVICE_SORT_TYPE_TRANSPONDER = 4;
    public static final int SERVICE_STATUS_NAME_UPDATE = 0;
    public static final int SERVICE_STATUS_PARAMS_UPDATE = 1;
    public static final int SERVICE_STATUS_RESOLUTION_TYPE_UPDATE = 2;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_16_9_RATIO = 18;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO = 19;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_4_3_RATIO = 17;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_HARD_HEARING_16_9_RATIO = 34;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_HARD_HEARING_2_21_1_RATIO = 35;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_HARD_HEARING_4_3_RATIO = 33;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_HARD_HEARING_NO_RATIO = 32;
    public static final int SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO = 16;
    public static final int SUBTITLING_TYPE_EBU_TELETEXT_SUBTITLE = 1;
    public static final int SWITCH_CHANNEL_MODE_BLANK = 0;
    public static final int SWITCH_CHANNEL_MODE_FREEZE = 1;
    public static final int SYSTEM_TUNER_CONFIG_DVB_S = 0;
    public static final int SYSTEM_TUNER_CONFIG_DVB_ST = 3;
    public static final int SYSTEM_TUNER_CONFIG_DVB_ST_S = 4;
    public static final int SYSTEM_TUNER_CONFIG_DVB_S_S = 2;
    public static final int SYSTEM_TUNER_CONFIG_DVB_T = 1;
    public static final int SYSTEM_TYPE_DVB_S = 0;
    public static final int SYSTEM_TYPE_DVB_T = 1;
    public static final int TELETEXT_COLOR_BLUE = 3;
    public static final int TELETEXT_COLOR_GREEN = 1;
    public static final int TELETEXT_COLOR_RED = 0;
    public static final int TELETEXT_COLOR_YELLOW = 2;
    public static final int TELETEXT_SUBTITLE_UPDATE = 0;
    public static final int TELETEXT_SUB_PAGE_UPDATE = 1;
    public static final int TELETEXT_TYPE_ADDITIONAL_INFORMATION_PAGE = 3;
    public static final int TELETEXT_TYPE_INITIAL_TELETEXT_PAGE = 1;
    public static final int TELETEXT_TYPE_PROGRAMME_SCHEDULE_PAGE = 4;
    public static final int TELETEXT_TYPE_TEXLETEXT_SUBTITLE_PAGE = 2;
    public static final int f0xe3564d8 = 5;
    public static final int TUNER_ID_ABS_DA3100 = 200;
    public static final int TUNER_ID_DTMB_AVL6381 = 501;
    public static final int TUNER_ID_DTMB_LGS9XB1 = 500;
    public static final int TUNER_ID_DVBT_AVL6762 = 304;
    public static final int TUNER_ID_DVBT_CXD2837 = 300;
    public static final int TUNER_ID_DVBT_IT9133 = 301;
    public static final int TUNER_ID_DVBT_MN88473 = 303;
    public static final int TUNER_ID_DVBT_SI2168 = 302;
    public static final int TUNER_ID_DVB_S_AVL6211 = 100;
    public static final int TUNER_ID_ISDBT_CXD2838 = 400;
    public static final int TUNER_ID_ISDBT_MXL68X = 401;
    public static final int TUNER_ID_UNDEFINED = 0;
    public static final int TUNER_TYPE_ABS_S = 102;
    public static final int TUNER_TYPE_DTMB = 107;
    public static final int TUNER_TYPE_DVB_C = 106;
    public static final int TUNER_TYPE_DVB_S = 100;
    public static final int TUNER_TYPE_DVB_S2 = 101;
    public static final int TUNER_TYPE_DVB_T = 104;
    public static final int TUNER_TYPE_DVB_T2 = 105;
    public static final int TUNER_TYPE_ISDB_T = 103;
    public static final int TUNER_TYPE_UNDEFINED = 0;
    public static final int TV_CH = 0;
    public static final int UNICABLE_TYPE_DUAL_SAT_4_SCR = 2;
    public static final int UNICABLE_TYPE_DUAL_SAT_8_SCR = 3;
    public static final int UNICABLE_TYPE_SINGLE_SAT_4_SCR = 0;
    public static final int UNICABLE_TYPE_SINGLE_SAT_8_SCR = 1;
    public static final int VIDEO_STREAM_TYPE_H264 = 1;
    public static final int VIDEO_STREAM_TYPE_H265 = 4;
    public static final int VIDEO_STREAM_TYPE_MPEG2 = 0;
    public static final int VIDEO_STREAM_TYPE_MPEG4 = 2;
    public static final int VIDEO_STREAM_TYPE_VC1 = 3;

    public static native int book_delete_event_by_index(int i);

    public static native int book_epg_add_event(int i, int i2, int i3, service th_dtv_mw_data_service, date th_dtv_mw_data_date, book_event_data th_dtv_mw_data_book_event_data);

    public static native int book_epg_delete_event(int i, int i2, service th_dtv_mw_data_service, date th_dtv_mw_data_date);

    public static native int book_get_event_by_index(int i, book_event_data th_dtv_mw_data_book_event_data);

    public static native int book_get_event_count();

    public static native int book_handle_event_by_index(int i);

    public static native void book_lock_event_check();

    public static native int book_manual_add_event(int i, int i2, service th_dtv_mw_data_service, date_time th_dtv_mw_data_date_time, int i3, book_event_data th_dtv_mw_data_book_event_data);

    public static native int book_manual_modify_event(int i, int i2, int i3, service th_dtv_mw_data_service, date_time th_dtv_mw_data_date_time, int i4, book_event_data th_dtv_mw_data_book_event_data);

    public static native int book_set_event_conflict_check_mode(int i);

    public static native void book_unlock_event_check();

    public static native int channel_edit_add_service(int i, int i2, int i3, int i4, int i5, int i6, String str, int i7, int i8, int i9, aud_info[] th_dtv_mw_data_aud_infoArr);

    public static native int channel_edit_cancel_operation();

    public static native boolean channel_edit_check_data_modified();

    public static native boolean channel_edit_check_delete(int i);

    public static native boolean channel_edit_check_lock(int i);

    public static native boolean channel_edit_check_selection(int i);

    public static native boolean channel_edit_check_skip(int i);

    public static native int channel_edit_delete_selection();

    public static native int channel_edit_delete_single(int i);

    public static native void channel_edit_exit();

    public static native int channel_edit_get_favorite(int i);

    public static native boolean channel_edit_has_selection();

    public static native int channel_edit_init();

    public static native int channel_edit_move_selection(int i);

    public static native int channel_edit_save_operation();

    public static native int channel_edit_select_all(boolean z);

    public static native int channel_edit_select_region(boolean z, int i);

    public static native int channel_edit_select_scrambled(boolean z);

    public static native int channel_edit_select_single(boolean z, int i);

    public static native int channel_edit_set_delete_selection(boolean z);

    public static native int channel_edit_set_delete_single(boolean z, int i);

    public static native int channel_edit_set_favorite_selection(int i);

    public static native int channel_edit_set_favorite_single(int i, int i2);

    public static native int channel_edit_set_lock_selection(boolean z);

    public static native int channel_edit_set_lock_single(boolean z, int i);

    public static native int channel_edit_set_service_name(int i, String str);

    public static native int channel_edit_set_service_pids(int i, int i2, int i3, int i4, aud_info[] th_dtv_mw_data_aud_infoArr);

    public static native int channel_edit_set_skip_selection(boolean z);

    public static native int channel_edit_set_skip_single(boolean z, int i);

    public static native int channel_edit_sort(int i);

    public static native boolean db_check_service_in_fav_mode();

    public static native boolean db_delete_all_service();

    public static native int db_dvb_s_add_sat(String str, int i, int i2, int i3);

    public static native int db_dvb_s_add_tp(int i, int i2, int i3, int i4);

    public static native boolean db_dvb_s_check_sat_has_service(int i);

    public static native int db_dvb_s_delete_sat(int[] iArr);

    public static native int db_dvb_s_delete_tp(int i, int[] iArr);

    public static native void db_dvb_s_delete_unselect_service();

    public static native int db_dvb_s_get_current_sat_info(int i, dvb_s_sat th_dtv_mw_data_dvb_s_sat);

    public static native int db_dvb_s_get_current_tp_info(int i, dvb_s_tp th_dtv_mw_data_dvb_s_tp);

    public static native int db_dvb_s_get_sat_count(int i);

    public static native int db_dvb_s_get_sat_info(int i, int i2, dvb_s_sat th_dtv_mw_data_dvb_s_sat);

    public static native int db_dvb_s_get_selected_sat_count(int i);

    public static native int db_dvb_s_get_selected_sat_info(int i, int i2, dvb_s_sat th_dtv_mw_data_dvb_s_sat);

    public static native int db_dvb_s_get_tp_count(int i, int i2);

    public static native int db_dvb_s_get_tp_info(int i, int i2, int i3, dvb_s_tp th_dtv_mw_data_dvb_s_tp);

    public static native boolean db_dvb_s_save_sat_tp(int i);

    public static native int db_dvb_s_set_current_sat_tp(int i, int i2, int i3);

    public static native int db_dvb_s_set_sat_info(int i, dvb_s_sat th_dtv_mw_data_dvb_s_sat);

    public static native int db_dvb_s_set_tp_info(int i, dvb_s_tp th_dtv_mw_data_dvb_s_tp);

    public static native int db_dvb_s_update_sat(int i, String str, int i2, int i3, int i4);

    public static native int db_dvb_s_update_tp(int i, int i2, int i3, int i4, int i5);

    public static native int db_dvb_t_get_area_count();

    public static native int db_dvb_t_get_area_info(int i, dvb_t_area th_dtv_mw_data_dvb_t_area);

    public static native int db_dvb_t_get_current_area_info(dvb_t_area th_dtv_mw_data_dvb_t_area);

    public static native int db_dvb_t_get_current_tp_info(dvb_t_tp th_dtv_mw_data_dvb_t_tp);

    public static native int db_dvb_t_get_tp_count(int i);

    public static native int db_dvb_t_get_tp_info(int i, int i2, dvb_t_tp th_dtv_mw_data_dvb_t_tp);

    public static native boolean db_dvb_t_save_area_tp();

    public static native int db_dvb_t_set_current_area_tp(int i, int i2);

    public static native int db_dvb_t_set_tp_info(dvb_t_tp th_dtv_mw_data_dvb_t_tp);

    public static native int db_dvb_t_update_tp(int i, int i2, int i3, int i4);

    public static native int db_export_user_data(String str);

    public static native int db_find_match_service(int i, String str, boolean z);

    public static native void db_free_find_match_service();

    public static native int db_get_current_service_info(service th_dtv_mw_data_service);

    public static native int db_get_find_match_service_count();

    public static native int db_get_find_match_service_index(int i);

    public static native int db_get_scr_bp_frequency(int i, int i2);

    public static native int db_get_service_available_fav_group_count();

    public static native int db_get_service_available_fav_group_current_pos();

    public static native int db_get_service_available_fav_group_index(int i);

    public static native int db_get_service_count(int i);

    public static native int db_get_service_info(int i, int i2, service th_dtv_mw_data_service);

    public static native int db_get_service_info_by_channel_number(int i, service th_dtv_mw_data_service);

    public static native int db_get_service_region_count();

    public static native int db_get_service_region_current_pos();

    public static native int db_get_service_region_info(int i, region_info th_dtv_mw_data_region_info);

    public static native int db_get_service_total_fav_group_current_index();

    public static native int db_get_total_service_count();

    public static native int db_import_user_data(String str);

    public static native void db_load_default_data();

    public static native boolean db_save_service();

    public static native void db_service_set_in_fav_mode();

    public static native void db_service_set_in_region_mode();

    public static native void db_service_set_in_region_mode_with_service_type_index(int i, int i2);

    public static native boolean db_set_service_available_fav_group_current_pos(int i);

    public static native boolean db_set_service_region_current_pos(int i);

    public static native boolean db_set_service_total_fav_group_current_index(int i);

    public static native boolean diseqc12_disable_limit();

    public static native boolean diseqc12_goto_position(int i);

    public static native boolean diseqc12_goto_reference();

    public static native boolean diseqc12_halt();

    public static native boolean diseqc12_move_east();

    public static native boolean diseqc12_move_east_continues();

    public static native boolean diseqc12_move_west();

    public static native boolean diseqc12_move_west_continues();

    public static native boolean diseqc12_recalculate(int i);

    public static native boolean diseqc12_set_east_limit();

    public static native boolean diseqc12_set_west_limit();

    public static native boolean diseqc12_store_position(int i);

    public static native boolean diseqc13_goto_position(int i);

    public static native void diseqc_motor_enable_auto_moving(boolean z);

    public static native void enter_standby();

    public static native int epg_get_pf_event(service th_dtv_mw_data_service, epg_pf_event th_dtv_mw_data_epg_pf_event, epg_pf_event th_dtv_mw_data_epg_pf_event2);

    public static native int epg_get_schedule_event_by_event_id(int i, int i2, service th_dtv_mw_data_service, date th_dtv_mw_data_date, epg_schedule_event th_dtv_mw_data_epg_schedule_event);

    public static native int epg_get_schedule_extened_event_by_event_id(int i, int i2, service th_dtv_mw_data_service, date th_dtv_mw_data_date, epg_extended_event th_dtv_mw_data_epg_extended_event);

    public static native int epg_lock_get_schedule_event_count(int i, service th_dtv_mw_data_service, date th_dtv_mw_data_date);

    public static native int epg_lock_get_schedule_event_id_by_index(int i, int i2, service th_dtv_mw_data_service, date th_dtv_mw_data_date);

    public static native void epg_require_pf_event();

    public static native void epg_set_schedule_event_day_offset(int i);

    public static native void epg_unlock_schedule_event();

    public static native int get_date(int i, date th_dtv_mw_data_date, date th_dtv_mw_data_date2);

    public static native int get_date_time(date_time th_dtv_mw_data_date_time);

    public static native int get_system_tuner_config();

    public static native int get_system_tuner_info(system_tuner_info th_dtv_mw_data_system_tuner_info);

    public static native int global_set_default_audio_ISO_639_Language_Code(String str, String str2);

    public static native int global_set_dynamic_service_update_mode(int i, int i2);

    public static native void global_set_lcn_option(boolean z);

    public static native int global_set_local_location(int i, int i2, int i3, int i4);

    public static native void global_set_search_nit(boolean z);

    public static native int global_set_search_service_type(int i);

    public static native int global_set_search_tv_type(int i);

    public static native void global_set_second_tuner_loop_lock(boolean z, int i);

    public static native int global_set_switch_channel_mode(int i);

    public static native int global_set_system_ISO_639_Language_Code(String str);

    public static native void global_set_time_zone(int i);

    public static native void global_set_timeshift_start_with_blank(boolean z);

    public static native void pvr_playback_fast_backward(int i);

    public static native void pvr_playback_fast_forward(int i);

    public static native void pvr_playback_pause();

    public static native void pvr_playback_resume();

    public static native void pvr_playback_seek(int i, boolean z);

    public static native int pvr_playback_start(int i);

    public static native void pvr_playback_stop();

    public static native void pvr_playback_switch_audio(int i, int i2);

    public static native int pvr_record_change_record_name(int i, String str);

    public static native int pvr_record_delete_record(int[] iArr);

    public static native void pvr_record_exit_record_list();

    public static native int pvr_record_get_record_count();

    public static native int pvr_record_get_record_info(int i, pvr_record th_dtv_mw_data_pvr_record);

    public static native int pvr_record_init_record_list(String str);

    public static native int pvr_record_set_record_lock(int i, boolean z);

    public static native int pvr_record_start(int i, int i2, int i3, String str, String str2);

    public static native void pvr_record_stop();

    public static native void pvr_timeshift_fast_backward(int i);

    public static native void pvr_timeshift_fast_forward(int i);

    public static native void pvr_timeshift_pause();

    public static native void pvr_timeshift_resume();

    public static native void pvr_timeshift_seek(int i, boolean z);

    public static native int pvr_timeshift_start(int i, int i2, int i3, String str);

    public static native void pvr_timeshift_stop();

    public static native int register_event_cb(Object obj);

    public static native int register_event_type(int i, boolean z);

    public static native int search_dvb_s_start(int i, int[] iArr, int[] iArr2, int i2);

    public static native void search_dvb_s_stop();

    public static native int search_dvb_t_start(int i, int[] iArr, int[] iArr2, int i2);

    public static native void search_dvb_t_stop();

    public static native int set_date_time(date_time th_dtv_mw_data_date_time);

    public static native void specail_set_clear_epg_event_start_with_special_chars(boolean z);

    public static native void specail_set_enable_switch_channel_while_recording(boolean z);

    public static native void specail_set_keep_service_user_data_which_search(boolean z);

    public static native void specail_set_remove_duplicate_service_while_search(boolean z);

    public static native void specail_set_show_no_audio_play_status(boolean z);

    public static native void special_set_default_h264_as_hd_channel(boolean z);

    public static native int special_set_dvb_text_default_char_coding(String str);

    public static native void special_set_table_search_timeout(int i, int i2, int i3, int i4, int i5);

    public static native int subtitle_get_picture_height();

    public static native int subtitle_get_picture_width();

    public static native void subtitle_lock();

    public static native int subtitle_start(int i, boolean z, int i2, int i3, Object obj);

    public static native void subtitle_stop();

    public static native void subtitle_unlock();

    public static native int teletext_get_current_page();

    public static native int teletext_get_sub_page_info(ttx_sub_page_info th_dtv_mw_data_ttx_sub_page_info);

    public static native int teletext_goto_color_link(int i);

    public static native int teletext_goto_first_page();

    public static native int teletext_goto_home();

    public static native int teletext_goto_last_page();

    public static native int teletext_goto_next_page();

    public static native int teletext_goto_next_sub_page();

    public static native int teletext_goto_offset_page(int i);

    public static native int teletext_goto_page(int i);

    public static native int teletext_goto_prev_page();

    public static native int teletext_goto_prev_sub_page();

    public static native void teletext_lock();

    public static native int teletext_start(int i, int i2, int i3, Object obj);

    public static native void teletext_stop();

    public static native void teletext_unlock();

    public static native int ts_player_get_play_status();

    public static native int ts_player_play(int i, int i2, int i3, boolean z);

    public static native int ts_player_play_by_channel_number(int i, boolean z);

    public static native int ts_player_play_current(boolean z, boolean z2);

    public static native int ts_player_play_down(int i, boolean z);

    public static native int ts_player_play_previous(boolean z);

    public static native int ts_player_play_switch_audio(int i);

    public static native int ts_player_play_switch_tv_radio(boolean z);

    public static native int ts_player_play_up(int i, boolean z);

    public static native void ts_player_require_play_status();

    public static native void ts_player_stop_play();

    public static native void ts_player_stop_play_and_tunning();

    public static native int tuner_dvb_s_lock_tp(int i, int i2, int i3, boolean z);

    public static native int tuner_dvb_t_lock_tp(int i, int i2, int i3, boolean z);

    public static native void tuner_dvb_t_set_antenna_power(int i, int i2);

    public static native int tuner_get_tuner_status(int i, tuner_signal_status th_dtv_mw_data_tuner_signal_status);

    public static native void tuner_require_tuner_status(int i);

    public static native void tuner_unlock_tp(int i, int i2, boolean z);
}
