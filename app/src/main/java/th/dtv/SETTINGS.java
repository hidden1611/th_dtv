package th.dtv;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.os.Handler;
import android.provider.Settings.System;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import th.dtv.activity.BissActivity;
import th.dtv.activity.TestActivity;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;
import th.dtv.mw_data.system_tuner_info;

public class SETTINGS {
    public static boolean ENABLE_SWITCH_CHANNEL_WHILE_RECORDING;
    public static boolean bBookRec;
    public static boolean bFromRTC;
    public static boolean bHongKongDTMB;
    public static boolean bPass;
    public static Context mContext;
    public static Handler mHandler;
    public static final Object mInitLock;
    public static String mLedChannelValue;
    private static boolean mLedIsOpen;
    public static Runnable mLedSendRunnable;
    public static SharedPreferences mSp;
    public static String password;
    public static boolean showdialog;
    static Toast toast;

    /* renamed from: th.dtv.SETTINGS.1 */
    static class C00861 implements Runnable {

        /* renamed from: th.dtv.SETTINGS.1.1 */
        class C00851 extends Thread {
            C00851() {
            }

            public void run() {
                SETTINGS.send_led_msg(SETTINGS.mLedChannelValue);
            }
        }

        C00861() {
        }

        public void run() {
            new C00851().start();
        }
    }

    static {
        mLedIsOpen = false;
        ENABLE_SWITCH_CHANNEL_WHILE_RECORDING = false;
        password = "0000";
        showdialog = true;
        bPass = false;
        bFromRTC = false;
        bBookRec = false;
        bHongKongDTMB = false;
        mContext = null;
        mSp = null;
        mInitLock = new Object();
        mLedChannelValue = null;
        mHandler = new Handler();
        mLedSendRunnable = new C00861();
    }

    public static void init(Context context, boolean forceInit) {
        int i = 5;
        synchronized (mInitLock) {
            if (mContext == null || forceInit) {
                String isoLangCode = context.getResources().getConfiguration().locale.getISO3Language();
                mContext = context;
                mSp = mContext.getSharedPreferences("dtv_preferences", 4);
                MW.global_set_system_ISO_639_Language_Code(isoLangCode);
                if (5 != get_data_version_number()) {
                    load_default_configs();
                }
                MW.book_set_event_conflict_check_mode(1);
                MW.global_set_local_location(get_location_value(), get_longitude_value(), get_hemisphere_value(), get_latitude_value());
                set_timezone_value(0);
                MW.global_set_default_audio_ISO_639_Language_Code(get_first_audio_language(), get_second_audio_language());
                MW.global_set_search_service_type(get_search_service_type());
                MW.global_set_search_tv_type(get_search_tv_type());
                MW.global_set_search_nit(get_search_nit());
                MW.global_set_switch_channel_mode(get_switch_mode());
                if (MW.get_system_tuner_config() == 1) {
                    if (get_ant_power_status() != 1) {
                        i = 0;
                    }
                    MW.tuner_dvb_t_set_antenna_power(0, i);
                    MW.global_set_lcn_option(get_lcn_onoff());
                }
                MW.global_set_second_tuner_loop_lock(true, 3000);
                system_tuner_info tunerInfo = new system_tuner_info();
                MW.get_system_tuner_info(tunerInfo);
                Log.e("SETTINGS", "sat tuner 1 : " + tunerInfo.sat_tuner_id_1);
                Log.e("SETTINGS", "sat tuner 2 : " + tunerInfo.sat_tuner_id_2);
                Log.e("SETTINGS", "ter tuner 1 : " + tunerInfo.ter_tuner_id_1);
                Log.e("SETTINGS", "ter tuner 2 : " + tunerInfo.ter_tuner_id_2);
                MW.specail_set_show_no_audio_play_status(false);
                if (tunerInfo.ter_tuner_type_1 == MW.TUNER_TYPE_DTMB) {
                    MW.special_set_dvb_text_default_char_coding("GB2312");
                }
                if (MW.get_system_tuner_config() == 0) {
                    ENABLE_SWITCH_CHANNEL_WHILE_RECORDING = false;
                } else {
                    ENABLE_SWITCH_CHANNEL_WHILE_RECORDING = true;
                }
                TestActivity.DoFunction(context);
                BissActivity.DoFunction(context);
                set_current_area_index(getAreaIndexByPorperSetting(context));
            }
        }
    }

    private static int getAreaIndexByPorperSetting(Context context) {
        return 0;
    }

    public static void load_default_configs() {
        set_password("0000");
        set_subtitle_default_on(false);
        set_timeshift_default_on(true);
        set_search_service_type(0);
        set_search_tv_type(0);
        set_video_screen_format(1);
        set_audio_channel(0);
        set_menu_lock_onff(false);
        set_channel_lock_onoff(true);
        set_banner_timeout(1);
        set_first_audio_language("eng", false);
        set_second_audio_language("eng", false);
        set_subtitle_language("eng");
        set_teletext_language("eng");
        set_timezone_value(0);
        set_location_value(0, false);
        set_hemisphere_value(0, false);
        set_longitude_value(0, false);
        set_latitude_value(0, false);
        set_parental_rating_value(19);
        setTimeShiftLengthIndex(1);
        setCustomTimeShiftLength(60);
        setRecordLengthIndex(5);
        setCustomRecordLength(100);
        set_current_area_index(0);
        if (MW.get_system_tuner_config() == 1) {
            set_ant_power_status(0);
        } else {
            set_ant_power_status(1);
        }
        set_data_version_number();
        set_default_favGroupName();
        showdialog = true;
        if (MW.get_system_tuner_config() == 1) {
            set_lcn_onoff(true);
        }
    }

    public static void set_data_version_number() {
        mSp.edit().putInt("DATA_VERSION_NUMBER", 5).commit();
    }

    public static int get_data_version_number() {
        return mSp.getInt("DATA_VERSION_NUMBER", 0);
    }

    public static void set_password(String s) {
        password = s;
        putSettingsProviderString("PASSWORD", s);
    }

    public static String get_password() {
        return getSettingsProviderString("PASSWORD", getInitPassword());
    }

    public static void set_subtitle_default_on(boolean on) {
        mSp.edit().putBoolean("SUBTITLE_ONOFF", on).commit();
    }

    public static boolean get_subtitle_default_on() {
        return mSp.getBoolean("SUBTITLE_ONOFF", false);
    }

    public static void set_timeshift_default_on(boolean on) {
        mSp.edit().putBoolean("TIMESHIFT_ONOFF", on).commit();
    }

    public static void set_search_nit(boolean on) {
        mSp.edit().putBoolean("NIT_SEARCH", on).commit();
        MW.global_set_search_nit(on);
    }

    public static boolean get_search_nit() {
        return mSp.getBoolean("NIT_SEARCH", false);
    }

    public static void set_search_service_type(int type) {
        mSp.edit().putInt("SEARCH_SERVICE_TYPE", type).commit();
        MW.global_set_search_service_type(type);
    }

    public static int get_search_service_type() {
        return mSp.getInt("SEARCH_SERVICE_TYPE", 0);
    }

    public static void set_search_tv_type(int type) {
        mSp.edit().putInt("SEARCH_TV_TYPE", type).commit();
        MW.global_set_search_tv_type(type);
    }

    public static int get_search_tv_type() {
        return mSp.getInt("SEARCH_TV_TYPE", 0);
    }

    public static void set_video_screen_format(int fmt) {
        mSp.edit().putInt("VIDEO_SCREEN_FORMAT", fmt).commit();
    }

    public static int get_video_screen_format() {
        return mSp.getInt("VIDEO_SCREEN_FORMAT", 1);
    }

    public static void set_screen_mode() {
        set_screen_mode(get_video_screen_format());
    }

    public static void set_screen_mode(int value) {
        Log.d("SETTINGS", "set_screen_mode[:" + value + "]");
        DtvControl mControl = new DtvControl("/sys/class/video/screen_mode");
        if (value == 0) {
            value = 0;
        } else if (value == 1) {
            value = 1;
        } else if (value == 2) {
            value = 2;
        } else if (value == 3) {
            value = 3;
        }
        mControl.setValue(Integer.toString(value));
    }

    public static void set_audio_channel(int channel) {
        mSp.edit().putInt("AUDIO_CHANNEL", channel).commit();
    }

    public static int get_audio_channel() {
        return mSp.getInt("AUDIO_CHANNEL", 0);
    }

    public static void set_menu_lock_onff(boolean on) {
        mSp.edit().putBoolean("MENULOCK_ONOFF", on).commit();
    }

    public static boolean get_menu_lock_onff() {
        return mSp.getBoolean("MENULOCK_ONOFF", false);
    }

    public static void set_channel_lock_onoff(boolean on) {
        mSp.edit().putBoolean("CHANNELLOCK_ONOFF", on).commit();
    }

    public static boolean get_channel_lock_onoff() {
        return mSp.getBoolean("CHANNELLOCK_ONOFF", true);
    }

    public static void set_banner_timeout(int index) {
        mSp.edit().putInt("BANNER_TIMEOUT", index).commit();
    }

    public static int get_banner_timeout() {
        return mSp.getInt("BANNER_TIMEOUT", 0);
    }

    public static int get_banner_time() {
        switch (get_banner_timeout()) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                return 3000;
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                return 5000;
            case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                return 7000;
            case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                return 10000;
            default:
                return 0;
        }
    }

    public static void set_first_audio_language(String lang, boolean bSetMW) {
        mSp.edit().putString("FIRST_AUDIO_LANGUAGE", lang).commit();
        if (bSetMW) {
            MW.global_set_default_audio_ISO_639_Language_Code(get_first_audio_language(), get_second_audio_language());
        }
    }

    public static String get_first_audio_language() {
        return mSp.getString("FIRST_AUDIO_LANGUAGE", "eng");
    }

    public static void set_second_audio_language(String lang, boolean bSetMW) {
        mSp.edit().putString("SECOND_AUDIO_LANGUAGE", lang).commit();
        if (bSetMW) {
            MW.global_set_default_audio_ISO_639_Language_Code(get_first_audio_language(), get_second_audio_language());
        }
    }

    public static String get_second_audio_language() {
        return mSp.getString("SECOND_AUDIO_LANGUAGE", "eng");
    }

    public static void set_subtitle_language(String lang) {
        mSp.edit().putString("SUBTITLE_LANGUAGE", lang).commit();
    }

    public static String get_subtitle_language() {
        return mSp.getString("SUBTITLE_LANGUAGE", "eng");
    }

    public static void set_teletext_language(String lang) {
        mSp.edit().putString("TELETEXT_LANGUAGE", lang).commit();
    }

    public static String get_teletext_language() {
        return mSp.getString("TELETEXT_LANGUAGE", "eng");
    }

    public static void set_timezone_value(int value) {
        int offset = (TimeZone.getTimeZone(TimeZone.getDefault().getID()).getOffset(Calendar.getInstance().getTimeInMillis()) / 1000) / 60;
        value = offset;
        mSp.edit().putInt("TIMEZONE_VALUE", value).commit();
        MW.global_set_time_zone(value);
    }

    public static void set_location_value(int value, boolean bSetMW) {
        mSp.edit().putInt("LOCATION_VALUE", value).commit();
        if (bSetMW) {
            MW.global_set_local_location(get_location_value(), get_longitude_value(), get_hemisphere_value(), get_latitude_value());
        }
    }

    public static int get_location_value() {
        if (mSp == null) {
            Log.e("SETTINGS", "get_location_value : problem happened, msp = null, why...");
        }
        return mSp.getInt("LOCATION_VALUE", 0);
    }

    public static void set_hemisphere_value(int value, boolean bSetMW) {
        mSp.edit().putInt("HEMISPHERE_VALUE", value).commit();
        if (bSetMW) {
            MW.global_set_local_location(get_location_value(), get_longitude_value(), get_hemisphere_value(), get_latitude_value());
        }
    }

    public static int get_hemisphere_value() {
        return mSp.getInt("HEMISPHERE_VALUE", 0);
    }

    public static void set_longitude_value(int value, boolean bSetMW) {
        mSp.edit().putInt("LONGITUDE_VALUE", value).commit();
        if (bSetMW) {
            MW.global_set_local_location(get_location_value(), get_longitude_value(), get_hemisphere_value(), get_latitude_value());
        }
    }

    public static int get_longitude_value() {
        return mSp.getInt("LONGITUDE_VALUE", 0);
    }

    public static void set_latitude_value(int value, boolean bSetMW) {
        mSp.edit().putInt("LATITUDE_VALUE", value).commit();
        if (bSetMW) {
            MW.global_set_local_location(get_location_value(), get_longitude_value(), get_hemisphere_value(), get_latitude_value());
        }
    }

    public static int get_latitude_value() {
        return mSp.getInt("LATITUDE_VALUE", 0);
    }

    public static void set_parental_rating_value(int value) {
        mSp.edit().putInt("PARENTAL_RATING_VALUE", value).commit();
    }

    public static int get_parental_rating_value() {
        return mSp.getInt("PARENTAL_RATING_VALUE", 19);
    }

    public static String getSettingsProviderString(String key, String defaultStr) {
        String mStr = System.getString(mContext.getContentResolver(), key);
        if (mStr == null) {
            return defaultStr;
        }
        return mStr;
    }

    public static boolean putSettingsProviderString(String key, String valueStr) {
        return System.putString(mContext.getContentResolver(), key, valueStr);
    }

    public static boolean putSettingsProviderInt(String key, int value) {
        return System.putInt(mContext.getContentResolver(), key, value);
    }

    public static String getInitPassword() {
        return "0000";
    }

    public static String getSuperPassword() {
        return "3547";
    }

    public static void makeText(Context context, int resId, int duration) {
        LinearLayout toast_view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.toast_main, null);
        TextView toast_tv = (TextView) toast_view.findViewById(R.id.toast_text);
        toast_tv.setText(resId);
        if (toast != null) {
            toast_tv.setText(resId);
        } else {
            toast = new Toast(context);
        }
        toast.setView(toast_view);
        toast.setDuration(duration);
        toast.setGravity(17, 0, -70);
        toast.show();
    }

    public static void setFavGroupListStringArray(String[] values) {
        String regularEx = "#";
        String str = "";
        if (values != null && values.length > 0) {
            for (String value : values) {
                str = (str + value) + regularEx;
            }
            mSp.edit().putString("fav_list_name", str).commit();
        }
    }

    public static String[] getFavGroupListArray() {
        return mSp.getString("fav_list_name", "").split("#");
    }

    private static void set_default_favGroupName() {
        String[] defautStr = new String[10];
        for (int i = 0; i < 10; i++) {
            defautStr[i] = mContext.getResources().getString(R.string.static_string_FAV) + (i + 1);
        }
        setFavGroupListStringArray(defautStr);
    }

    public static void set_video_enable(boolean enable) {
        new DtvControl("/sys/class/video/disable_video").setValue(enable ? "0" : "1");
    }

    public static void set_led_channel(int channel) {
//        if (SystemProperties.getBoolean("hw.vfd", false)) {
//            if (channel < 0) {
//                channel = 0;
//            } else if (channel > 9999) {
//                channel %= 10000;
//            }
//            String channelString = String.valueOf(channel);
//            String[] channelPre = new String[]{"C00", "C0", "C", ""};
//            mLedChannelValue = channelPre[channelString.length() - 1] + channelString;
//            Log.d("SETTINGS", "set_led_channel() = " + mLedChannelValue + " mLedIsOpen " + mLedIsOpen);
//            if (mLedIsOpen) {
//                new DtvControl("/sys/devices/platform/m1-vfd.0/led").setValueForce(mLedChannelValue);
//                return;
//            }
//            send_led_msg(mLedChannelValue);
//            mLedIsOpen = true;
//        }
    }

    public static void send_led_msg(String msg) {
//        if (SystemProperties.getBoolean("hw.vfd", false)) {
//            mLedIsOpen = false;
//            Log.d("SETTINGS", "send_led_msg() = " + msg + " mLedIsOpen " + mLedIsOpen);
//            LocalSocket so = new LocalSocket();
//            try {
//                so.connect(new LocalSocketAddress("led", Namespace.RESERVED));
//                so.setSendBufferSize(msg.length());
//                OutputStream out = so.getOutputStream();
//                out.write(msg.getBytes());
//                out.close();
//                so.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    public static void set_green_led(boolean enable) {
        new DtvControl("/sys/devices/platform/m1-vfd.0/greenled").setValue(enable ? "1" : "0");
    }

    static void setVideoWindow(Context context) {
        int[] iArr = new int[]{0, 0, 1280, 720, 1280, 720};
        iArr = getPosition(context);
        String winStr = iArr[0] + " " + iArr[1] + " " + iArr[2] + " " + iArr[3];
        Log.d("SETTINGS", "setDevVideoWindow() " + winStr);
        new DtvControl("/sys/class/video/axis").setValue(winStr);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int[] getPosition(android.content.Context r37) {
        /*
        r5 = "ubootenv.var.480ioutputx";
        r6 = "ubootenv.var.480ioutputy";
        r7 = "ubootenv.var.480ioutputwidth";
        r8 = "ubootenv.var.480ioutputheight";
        r9 = "ubootenv.var.480poutputx";
        r10 = "ubootenv.var.480poutputy";
        r11 = "ubootenv.var.480poutputwidth";
        r12 = "ubootenv.var.480poutputheight";
        r13 = "ubootenv.var.576ioutputx";
        r14 = "ubootenv.var.576ioutputy";
        r15 = "ubootenv.var.576ioutputwidth";
        r16 = "ubootenv.var.576ioutputheight";
        r17 = "ubootenv.var.576poutputx";
        r18 = "ubootenv.var.576poutputy";
        r19 = "ubootenv.var.576poutputwidth";
        r20 = "ubootenv.var.576poutputheight";
        r21 = "ubootenv.var.720poutputx";
        r22 = "ubootenv.var.720poutputy";
        r23 = "ubootenv.var.720poutputwidth";
        r24 = "ubootenv.var.720poutputheight";
        r25 = "ubootenv.var.1080ioutputx";
        r26 = "ubootenv.var.1080ioutputy";
        r27 = "ubootenv.var.1080ioutputwidth";
        r28 = "ubootenv.var.1080ioutputheight";
        r29 = "ubootenv.var.1080poutputx";
        r30 = "ubootenv.var.1080poutputy";
        r31 = "ubootenv.var.1080poutputwidth";
        r32 = "ubootenv.var.1080poutputheight";
        r3 = "/sys/class/display/mode";
        r2 = 12;
        r0 = new java.lang.String[r2];
        r33 = r0;
        r2 = 0;
        r4 = "480i";
        r33[r2] = r4;
        r2 = 1;
        r4 = "480p";
        r33[r2] = r4;
        r2 = 2;
        r4 = "576i";
        r33[r2] = r4;
        r2 = 3;
        r4 = "576p";
        r33[r2] = r4;
        r2 = 4;
        r4 = "720p";
        r33[r2] = r4;
        r2 = 5;
        r4 = "1080i";
        r33[r2] = r4;
        r2 = 6;
        r4 = "1080p";
        r33[r2] = r4;
        r2 = 7;
        r4 = "720p50hz";
        r33[r2] = r4;
        r2 = 8;
        r4 = "1080i50hz";
        r33[r2] = r4;
        r2 = 9;
        r4 = "1080p50hz";
        r33[r2] = r4;
        r2 = 10;
        r4 = "480cvbs";
        r33[r2] = r4;
        r2 = 11;
        r4 = "576cvbs";
        r33[r2] = r4;
        r2 = "system_write";
        r0 = r37;
        r2 = r0.getSystemService(r2);
        r2 = (android.app.SystemWriteManager) r2;
        r34 = r2.readSysfs(r3);
        r3 = 6;
        r0 = new int[r3];
        r35 = r0;
        r35 = {0, 0, 1280, 720, 1280, 720};
        r4 = 4;
        r3 = 0;
    L_0x0098:
        r0 = r33;
        r0 = r0.length;
        r36 = r0;
        r0 = r36;
        if (r3 >= r0) goto L_0x00b1;
    L_0x00a1:
        r36 = r33[r3];
        r0 = r34;
        r1 = r36;
        r36 = r0.equalsIgnoreCase(r1);
        if (r36 == 0) goto L_0x00ae;
    L_0x00ad:
        r4 = r3;
    L_0x00ae:
        r3 = r3 + 1;
        goto L_0x0098;
    L_0x00b1:
        switch(r4) {
            case 0: goto L_0x00fd;
            case 1: goto L_0x012a;
            case 2: goto L_0x0157;
            case 3: goto L_0x0187;
            case 4: goto L_0x01bd;
            case 5: goto L_0x01f3;
            case 6: goto L_0x0229;
            case 7: goto L_0x01bd;
            case 8: goto L_0x01f3;
            case 9: goto L_0x0229;
            case 10: goto L_0x00fd;
            case 11: goto L_0x0157;
            default: goto L_0x00b4;
        };
    L_0x00b4:
        r3 = 0;
        r4 = 0;
        r0 = r21;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r0 = r22;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 1280; // 0x500 float:1.794E-42 double:6.324E-321;
        r0 = r23;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r0 = r24;
        r2 = r2.getPropertyInt(r0, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 1280; // 0x500 float:1.794E-42 double:6.324E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r35[r2] = r3;
    L_0x00e8:
        r2 = 2;
        r3 = 0;
        r3 = r35[r3];
        r4 = 2;
        r4 = r35[r4];
        r3 = r3 + r4;
        r35[r2] = r3;
        r2 = 3;
        r3 = 1;
        r3 = r35[r3];
        r4 = 3;
        r4 = r35[r4];
        r3 = r3 + r4;
        r35[r2] = r3;
        return r35;
    L_0x00fd:
        r3 = 0;
        r4 = 0;
        r4 = r2.getPropertyInt(r5, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r4 = r2.getPropertyInt(r6, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r4 = r2.getPropertyInt(r7, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 480; // 0x1e0 float:6.73E-43 double:2.37E-321;
        r2 = r2.getPropertyInt(r8, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 480; // 0x1e0 float:6.73E-43 double:2.37E-321;
        r35[r2] = r3;
        goto L_0x00e8;
    L_0x012a:
        r3 = 0;
        r4 = 0;
        r4 = r2.getPropertyInt(r9, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r4 = r2.getPropertyInt(r10, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r4 = r2.getPropertyInt(r11, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 480; // 0x1e0 float:6.73E-43 double:2.37E-321;
        r2 = r2.getPropertyInt(r12, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 480; // 0x1e0 float:6.73E-43 double:2.37E-321;
        r35[r2] = r3;
        goto L_0x00e8;
    L_0x0157:
        r3 = 0;
        r4 = 0;
        r4 = r2.getPropertyInt(r13, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r4 = r2.getPropertyInt(r14, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r4 = r2.getPropertyInt(r15, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 576; // 0x240 float:8.07E-43 double:2.846E-321;
        r0 = r16;
        r2 = r2.getPropertyInt(r0, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 576; // 0x240 float:8.07E-43 double:2.846E-321;
        r35[r2] = r3;
        goto L_0x00e8;
    L_0x0187:
        r3 = 0;
        r4 = 0;
        r0 = r17;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r0 = r18;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r0 = r19;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 576; // 0x240 float:8.07E-43 double:2.846E-321;
        r0 = r20;
        r2 = r2.getPropertyInt(r0, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 576; // 0x240 float:8.07E-43 double:2.846E-321;
        r35[r2] = r3;
        goto L_0x00e8;
    L_0x01bd:
        r3 = 0;
        r4 = 0;
        r0 = r21;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r0 = r22;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 1280; // 0x500 float:1.794E-42 double:6.324E-321;
        r0 = r23;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r0 = r24;
        r2 = r2.getPropertyInt(r0, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 1280; // 0x500 float:1.794E-42 double:6.324E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 720; // 0x2d0 float:1.009E-42 double:3.557E-321;
        r35[r2] = r3;
        goto L_0x00e8;
    L_0x01f3:
        r3 = 0;
        r4 = 0;
        r0 = r25;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r0 = r26;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 1920; // 0x780 float:2.69E-42 double:9.486E-321;
        r0 = r27;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 1080; // 0x438 float:1.513E-42 double:5.336E-321;
        r0 = r28;
        r2 = r2.getPropertyInt(r0, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 1920; // 0x780 float:2.69E-42 double:9.486E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 1080; // 0x438 float:1.513E-42 double:5.336E-321;
        r35[r2] = r3;
        goto L_0x00e8;
    L_0x0229:
        r3 = 0;
        r4 = 0;
        r0 = r29;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 1;
        r4 = 0;
        r0 = r30;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 2;
        r4 = 1920; // 0x780 float:2.69E-42 double:9.486E-321;
        r0 = r31;
        r4 = r2.getPropertyInt(r0, r4);
        r35[r3] = r4;
        r3 = 3;
        r4 = 1080; // 0x438 float:1.513E-42 double:5.336E-321;
        r0 = r32;
        r2 = r2.getPropertyInt(r0, r4);
        r35[r3] = r2;
        r2 = 4;
        r3 = 1920; // 0x780 float:2.69E-42 double:9.486E-321;
        r35[r2] = r3;
        r2 = 5;
        r3 = 1080; // 0x438 float:1.513E-42 double:5.336E-321;
        r35[r2] = r3;
        goto L_0x00e8;
        */
        throw new UnsupportedOperationException("Method not decompiled: th.dtv.SETTINGS.getPosition(android.content.Context):int[]");
    }

    public static void setTimeShiftLengthIndex(int i) {
        mSp.edit().putInt("TIMESHIFTLENGTH", i).commit();
    }

    public static int getTimeShiftLengthIndex() {
        return mSp.getInt("TIMESHIFTLENGTH", 0);
    }

    public static void setCustomTimeShiftLength(int i) {
        mSp.edit().putInt("CUSTOMTIMESHIFTLENGTH", i).commit();
    }

    public static int getCustomTimeShiftLength() {
        return mSp.getInt("CUSTOMTIMESHIFTLENGTH", 60);
    }

    public static int getTimeShiftTime() {
        switch (getTimeShiftLengthIndex()) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                return 60;
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                return MW.TUNER_ID_DVBT_CXD2837;
            case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                return 1800;
            case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                return 3600;
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                return getCustomTimeShiftLength() * 60;
            default:
                return 0;
        }
    }

    public static void setRecordLengthIndex(int i) {
        mSp.edit().putInt("RECORDLENGTH", i).commit();
    }

    public static int getRecordLengthIndex() {
        return mSp.getInt("RECORDLENGTH", 5);
    }

    public static void setCustomRecordLength(int i) {
        mSp.edit().putInt("CUSTOMRECORDLENGTH", i).commit();
    }

    public static int getCustomRecordLength() {
        return mSp.getInt("CUSTOMRECORDLENGTH", 100);
    }

    public static int getRecordTime() {
        switch (getRecordLengthIndex()) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                return 60000;
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                return 600000;
            case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                return 1800000;
            case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                return 3600000;
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                return 60000 * getCustomRecordLength();
            case MW.f0xe3564d8 /*5*/:
                return 0;
            default:
                return 0;
        }
    }

    public static String getDateTimeAdjustSystem(date_time th_dtv_mw_data_date_time, boolean z) {
        String str = "";
        str = "";
        if (th_dtv_mw_data_date_time == null) {
            return "";
        }
        if (getTimeFormat().equals("24")) {
            if (z) {
                str = "HH:mm:ss";
            } else {
                str = "HH:mm";
            }
        } else if (getTimeFormat().equals("12")) {
            if (z) {
                if (mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("CN") || mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("TW")) {
                    str = " a hh:mm:ss ";
                } else {
                    str = "hh:mm:ss a";
                }
            } else if (mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("CN") || mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("TW")) {
                str = "a hh:mm ";
            } else {
                str = "hh:mm a";
            }
        }
        str = new SimpleDateFormat(getDateFormat() + " " + str).format(new Date(th_dtv_mw_data_date_time.year - 1900, th_dtv_mw_data_date_time.month - 1, th_dtv_mw_data_date_time.day, th_dtv_mw_data_date_time.hour, th_dtv_mw_data_date_time.minute, th_dtv_mw_data_date_time.second));
        if (str == null) {
            return "";
        }
        return str;
    }

    public static String getTimeAdjustSystem(date_time th_dtv_mw_data_date_time, boolean z) {
        String str = "";
        String str2 = "";
        if (th_dtv_mw_data_date_time == null) {
            return "";
        }
        if (getTimeFormat().equals("24")) {
            if (z) {
                str = "HH:mm:ss";
            } else {
                str = "HH:mm";
            }
        } else if (getTimeFormat().equals("12")) {
            if (z) {
                if (mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("CN") || mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("TW")) {
                    str = " a hh:mm:ss ";
                } else {
                    str = "hh:mm:ss a";
                }
            } else if (mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("CN") || mContext.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("TW")) {
                str = "a hh:mm ";
            } else {
                str = "hh:mm a";
            }
        }
        str = new SimpleDateFormat(str).format(Long.valueOf(new Date(th_dtv_mw_data_date_time.year - 1900, th_dtv_mw_data_date_time.month - 1, th_dtv_mw_data_date_time.day, th_dtv_mw_data_date_time.hour, th_dtv_mw_data_date_time.minute, th_dtv_mw_data_date_time.second).getTime()));
        if (str == null) {
            return "";
        }
        return str;
    }

    private static String getDateFormat() {
        String string = System.getString(mContext.getContentResolver(), "date_format");
        if (string == null) {
            return "yyyy-MM-dd";
        }
        return string;
    }

    private static String getTimeFormat() {
        String string = System.getString(mContext.getContentResolver(), "time_12_24");
        if (string == null) {
            return "24";
        }
        return string;
    }

    public static String getDateAdjustSystem(date date) {
        String string_date = "";
        if (date == null) {
            return "";
        }
        string_date = new SimpleDateFormat(getDateFormat()).format(new Date(date.year - 1900, date.month - 1, date.day));
        return string_date == null ? "" : string_date;
    }

    public static int get_ant_power_status() {
        return mSp.getInt("ANT_POWER", 0);
    }

    public static void set_ant_power_status(int i) {
        mSp.edit().putInt("ANT_POWER", i).commit();
        if (i == 1) {
            i = 5;
        }
        MW.tuner_dvb_t_set_antenna_power(0, i);
    }

    public static void set_lcn_onoff(boolean z) {
        mSp.edit().putBoolean("LCN", z).commit();
        MW.global_set_lcn_option(z);
    }

    public static boolean get_lcn_onoff() {
        return mSp.getBoolean("LCN", true);
    }

    public static int get_current_area_index() {
        return mSp.getInt("AREA_INDEX", 0);
    }

    public static void set_current_area_index(int i) {
        mSp.edit().putInt("AREA_INDEX", i).commit();
    }

    public static String get_area_string_by_index(Context context, int i) {
        system_tuner_info th_dtv_mw_data_system_tuner_info = new system_tuner_info();
        MW.get_system_tuner_info(th_dtv_mw_data_system_tuner_info);
//        if (SystemProperties.get("net.platform.dtv.module").equals("MXL68X")) {
//            return context.getResources().getStringArray(R.array.isdbt_area_array)[i];
//        }
        return context.getResources().getStringArray(th_dtv_mw_data_system_tuner_info.ter_tuner_type_1 == MW.TUNER_TYPE_DTMB ? R.array.area_dtmb_array : R.array.area_array)[i];
    }

    public static void videoViewSetFormat(Context mContext, VideoView videoView) {
        videoView.getHolder().setFormat(258);
    }

    public static int getPlatformType() {
//        if (((SystemWriteManager) mContext.getSystemService("system_write")).getPropertyString("ro.build.version.release", "0").equals("4.2.2")) {
//            return 0;
//        }
        return 1;
    }

    public static int getWindowWidth() {
        return 1080;
//        return Integer.parseInt(((SystemWriteManager) mContext.getSystemService("system_write")).getPropertyString("const.window.w", "1280"));
    }

    public static int getWindowHeight() {
        return 720;
//        return Integer.parseInt(((SystemWriteManager) mContext.getSystemService("system_write")).getPropertyString("const.window.h", "720"));
    }

    public static void set_RTC_flag(boolean z) {
        mSp.edit().putBoolean("FROM_RTC", z).commit();
    }

    public static boolean get_RTC_flag() {
        return mSp.getBoolean("FROM_RTC", false);
    }

    public static void set_switch_mode(int i) {
        mSp.edit().putInt("SWITCH_MODE", i).commit();
        MW.global_set_switch_channel_mode(get_switch_mode());
    }

    public static int get_switch_mode() {
        return mSp.getInt("SWITCH_MODE", 1);
    }
}
