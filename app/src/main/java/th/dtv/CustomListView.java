package th.dtv;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class CustomListView extends ListView implements OnKeyListener, OnScrollListener {
    private int mCurrentSelectId;
    private int mFirstVisibleItem;
    private OnKeyListener mOnKeyListener;
    private OnScrollListener mOnScrollListener;
    private int mVisibleItemCount;

    public CustomListView(Context context) {
        super(context);
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 0;
        this.mCurrentSelectId = 0;
        this.mOnKeyListener = null;
        this.mOnScrollListener = null;
        setOnKeyListener(this);
        setOnScrollListener(this);
    }

    public CustomListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 0;
        this.mCurrentSelectId = 0;
        this.mOnKeyListener = null;
        this.mOnScrollListener = null;
        setOnKeyListener(this);
        setOnScrollListener(this);
    }

    public CustomListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 0;
        this.mCurrentSelectId = 0;
        this.mOnKeyListener = null;
        this.mOnScrollListener = null;
        setOnKeyListener(this);
        setOnScrollListener(this);
    }

    public void setVisibleItemCount(int count) {
        this.mVisibleItemCount = count;
    }

    public void setCustomKeyListener(OnKeyListener l) {
        this.mOnKeyListener = l;
    }

    public void setCustomScrollListener(OnScrollListener l) {
        this.mOnScrollListener = l;
    }

    public void setCustomSelection(int position) {
        super.setSelection(position);
        this.mCurrentSelectId = position;
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d("CustomListView", "======onSizeChanged() mCurrentSelectId " + this.mCurrentSelectId + " mVisibleItemCount " + this.mVisibleItemCount + " h " + h);
        if (this.mCurrentSelectId != 0 && this.mVisibleItemCount > 0 && h > 0) {
            int y = (this.mCurrentSelectId % this.mVisibleItemCount) * (h / this.mVisibleItemCount);
            Log.d("CustomListView", "======onSizeChanged() " + y);
            setSelectionFromTop(this.mCurrentSelectId, y);
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.mFirstVisibleItem = firstVisibleItem;
        this.mVisibleItemCount = visibleItemCount;
        this.mCurrentSelectId = view.getSelectedItemPosition();
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScrollStateChanged(view, scrollState);
        }
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.mCurrentSelectId <= 0) {
                        this.mCurrentSelectId = getCount() - 1;
                        setSelection(this.mCurrentSelectId);
                        return true;
                    } else if (this.mCurrentSelectId != this.mFirstVisibleItem || this.mCurrentSelectId <= 0) {
                        this.mCurrentSelectId = getSelectedItemPosition();
                        break;
                    } else {
                        setSelection(this.mCurrentSelectId - 1);
                        return true;
                    }
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.mCurrentSelectId >= getCount() - 1) {
                        this.mCurrentSelectId = 0;
                        setSelection(this.mCurrentSelectId);
                        return true;
                    } else if (this.mCurrentSelectId + 1 != this.mFirstVisibleItem + this.mVisibleItemCount || this.mCurrentSelectId >= getCount() - 1) {
                        this.mCurrentSelectId = getSelectedItemPosition();
                        break;
                    } else {
                        setSelection(this.mCurrentSelectId + 1);
                        return true;
                    }
                case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                    if (this.mCurrentSelectId >= getCount() - 1) {
                        this.mCurrentSelectId = 0;
                    } else {
                        this.mCurrentSelectId += this.mVisibleItemCount;
                        if (this.mCurrentSelectId > getCount() - 1) {
                            this.mCurrentSelectId = getCount() - 1;
                        }
                    }
                    setSelection(this.mCurrentSelectId);
                    ((BaseAdapter) getAdapter()).notifyDataSetChanged();
                    return true;
                case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                    if (this.mCurrentSelectId <= 0) {
                        this.mCurrentSelectId = getCount() - 1;
                    } else {
                        this.mCurrentSelectId -= this.mVisibleItemCount;
                        if (this.mCurrentSelectId < 0) {
                            this.mCurrentSelectId = 0;
                        }
                    }
                    setSelection(this.mCurrentSelectId);
                    ((BaseAdapter) getAdapter()).notifyDataSetChanged();
                    return true;
            }
        }
        if (this.mOnKeyListener != null) {
            return this.mOnKeyListener.onKey(v, keyCode, event);
        }
        return false;
    }
}
