package th.dtv;

public class mw_data {

    public static class aud_info {
        public String ISO_639_language_code;
        public int audio_pid;
        public int audio_stream_type;
    }

    public static class book_callback_event_data {
        public int book_mode;
        public int book_type;
        public int duration_seconds;
        public int event_index;
        public String event_name;
        public boolean is_all_region_service_index;
        public int left_time_seconds;
        public int service_index;
        public String service_name;
        public int service_type;
    }

    public static class book_event_data {
        public int book_mode;
        public int book_type;
        public int end_day;
        public int end_hour;
        public int end_minute;
        public int end_month;
        public int end_year;
        public int event_index;
        public String event_name;
        public boolean is_all_region_service_index;
        public int service_index;
        public String service_name;
        public int service_type;
        public int start_day;
        public int start_hour;
        public int start_minute;
        public int start_month;
        public int start_year;
    }

    public static class date {
        public int day;
        public int month;
        public int weekNo;
        public int year;
    }

    public static class date_time {
        public int day;
        public int hour;
        public int minute;
        public int month;
        public int second;
        public int year;
    }

    public static class dvb_s_sat {
        public int diseqc12_position;
        public int diseqc_port;
        public int diseqc_type;
        public int k22;
        public int lnb_high;
        public int lnb_low;
        public int lnb_power;
        public int lnb_type;
        public int motor_type;
        public int sat_degree_dec;
        public int sat_degree_point;
        public int sat_index;
        public String sat_name;
        public int sat_pos;
        public int sat_position;
        public int scr_bandPassFrequency;
        public int scr_number;
        public int scr_position;
        public boolean selected;
        public int tp_count;
        public int unicable_type;
    }

    public static class dvb_s_tp {
        public int frq;
        public int pol;
        public int sat_index;
        public int sat_pos;
        public int sym;
        public int tp_index;
    }

    public static class dvb_t_area {
        public int area_index;
        public String area_name;
        public int tp_count;
    }

    public static class dvb_t_tp {
        public int area_index;
        public int bandwidth;
        public String channel_number;
        public int frq;
        public int tp_index;
    }

    public static class epg_extended_event {
        public String event_text;
        public String[] items_descriptor;
        public String[] items_text;
    }

    public static class epg_pf_event {
        public int end_day;
        public int end_hour;
        public int end_minute;
        public int end_month;
        public int end_year;
        public String event_name;
        public int parental_rating;
        public int play_progress;
        public int start_day;
        public int start_hour;
        public int start_minute;
        public int start_month;
        public int start_year;
    }

    public static class epg_schedule_event {
        public int book_status;
        public int end_day;
        public int end_hour;
        public int end_minute;
        public int end_month;
        public int end_year;
        public int event_id;
        public String event_name;
        public String event_text;
        public boolean has_extended_event_info;
        public int parental_rating;
        public int playing_status;
        public int start_day;
        public int start_hour;
        public int start_minute;
        public int start_month;
        public int start_year;
    }

    public static class pvr_record {
        public int audio_index;
        public aud_info[] audio_info;
        public int current_play_position;
        public int duration;
        public String event_name;
        public long file_size;
        public boolean is_hd;
        public boolean is_locked;
        public int record_day;
        public int record_hour;
        public int record_minute;
        public int record_month;
        public int record_second;
        public int record_year;
        public String service_name;
        public int service_type;
        public sub_info[] subtitle_info;
        public ttx_info[] teletext_info;
        public int video_pid;
        public int video_stream_type;
    }

    public static class region_info {
        public int region_index;
        public String region_name;
        public int system_type;
    }

    public static class search_status_dvbs_blind_scan_params {
        public int frq;
        public boolean locked;
        public int pol;
        public int sat_degree_dec;
        public int sat_degree_point;
        public String sat_name;
        public int sat_position;
        public int search_sat_count;
        public int search_sat_index;
        public int search_sat_pos;
        public int sym;
    }

    public static class search_status_dvbs_blind_scan_progress {
        public boolean locked;
        public int progress;
        public int progress_total;
    }

    public static class search_status_dvbs_params {
        public int frq;
        public int pol;
        public int progress;
        public int progress_total;
        public int sat_degree_dec;
        public int sat_degree_point;
        public String sat_name;
        public int sat_position;
        public int search_sat_count;
        public int search_sat_index;
        public int search_sat_pos;
        public int search_tp_count;
        public int search_tp_pos;
        public int sym;
    }

    public static class search_status_dvbt_params {
        public String area_name;
        public int bandwidth;
        public String channel_number;
        public int dvb_t2_plp_id;
        public int dvb_t_t2_profile;
        public int front_end_type;
        public int frq;
        public int progress;
        public int progress_total;
        public int search_area_count;
        public int search_area_index;
        public int search_area_pos;
        public int search_tp_count;
        public int search_tp_pos;
    }

    public static class search_status_result {
        public int radio_service_count;
        public int tv_service_count;
    }

    public static class search_status_update_channel_list {
        public String[] radio_channels_name;
        public int[] radio_channels_number;
        public String[] tv_channels_name;
        public int[] tv_channels_number;
    }

    public static class service {
        public int audio_index;
        public aud_info[] audio_info;
        public int channel_number_display;
        public int dvb_t2_plp_id;
        public int dvb_t_t2_profile;
        public int favorite;
        public int front_end_type;
        public boolean is_hd;
        public boolean is_locked;
        public boolean is_scrambled;
        public boolean is_skipped;
        public int on_id;
        public int pcr_pid;
        public int region_index;
        public int service_id;
        public int service_index;
        public String service_name;
        public int service_type;
        public sub_info[] subtitle_info;
        public int system_type;
        public ttx_info[] teletext_info;
        public int tp_index;
        public int ts_id;
        public int video_pid;
        public int video_stream_type;
    }

    public static class sub_info {
        public String ISO_639_language_code;
        public int ancillary_page_id;
        public int composition_page_id;
        public int pid;
        public int subtitling_type;
    }

    public static class system_tuner_info {
        public int sat_tuner_id_1;
        public int sat_tuner_id_2;
        public int sat_tuner_type_1;
        public int sat_tuner_type_2;
        public int ter_tuner_id_1;
        public int ter_tuner_id_2;
        public int ter_tuner_type_1;
        public int ter_tuner_type_2;
    }

    public static class ttx_info {
        public String ISO_639_language_code;
        public int pid;
        public int teletext_magazine_number;
        public int teletext_page_number;
        public int teletext_type;
    }

    public static class ttx_sub_page_info {
        public int[] sub_page_nos;
    }

    public static class tuner_signal_status {
        public long ber;
        public boolean error;
        public int front_end_type;
        public boolean locked;
        public int quality;
        public int strength;
    }
}
