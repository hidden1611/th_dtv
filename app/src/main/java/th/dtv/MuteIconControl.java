package th.dtv;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.UserHandle;
import android.util.Log;

public class MuteIconControl {
    private static int mPreVolume;

    static {
        mPreVolume = 0;
    }

    public static void startMuteIconService(Context context) {
        Log.d("MuteIconControl", "==== startMuteIconService ");
        context.startService(new Intent("com.android.dtv.MuteIconService"));
//        if (((AudioManager) context.getSystemService("audio")).isStreamMute(3)) {
//            showMuteIcon(context);
//        } else {
//            hideMuteIcon(context);
//        }
    }

    public static void showMuteIcon(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.dtv.mute.icon.show");
//        context.sendBroadcastAsUser(intent, UserHandle.ALL);
    }

    public static void hideMuteIcon(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.dtv.mute.icon.hide");
//        context.sendBroadcastAsUser(intent, UserHandle.ALL);
    }
}
