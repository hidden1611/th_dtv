package th.dtv;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;

public class AdvancedSettingsActivity extends DtvBaseActivity {
    private String TAG;
    private ComboLayout mChannelLockLinearLayout;
    private EditText mConfirmPasswrodEditText;
    private Context mContext;
    private EditText mCurrentPasswordEditText;
    private ComboLayout mMenuLockLinearLayout;
    private EditText mNewPasswordEditText;
    private ComboLayout mParentalRatingLinearLayout;
    private ComboLayout mSetPasswordLinearLayout;

    /* renamed from: th.dtv.AdvancedSettingsActivity.1 */
    class C00001 implements OnItemClickListener {
        C00001() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (SETTINGS.get_menu_lock_onff()) {
                SETTINGS.set_menu_lock_onff(false);
            } else {
                SETTINGS.set_menu_lock_onff(true);
            }
        }
    }

    /* renamed from: th.dtv.AdvancedSettingsActivity.2 */
    class C00012 implements OnItemClickListener {
        C00012() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (SETTINGS.get_channel_lock_onoff()) {
                SETTINGS.set_channel_lock_onoff(false);
            } else {
                SETTINGS.set_channel_lock_onoff(true);
            }
        }
    }

    /* renamed from: th.dtv.AdvancedSettingsActivity.3 */
    class C00023 implements OnItemClickListener {
        C00023() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            SETTINGS.set_parental_rating_value(position + 4);
        }
    }

    /* renamed from: th.dtv.AdvancedSettingsActivity.4 */
    class C00034 implements OnItemClickListener {
        C00034() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            AdvancedSettingsActivity.this.EditSecurityPassword();
        }
    }

    public class EditCustomDialog extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;

        /* renamed from: th.dtv.AdvancedSettingsActivity.EditCustomDialog.1 */
//        class C00041 implements OnClickListener {
//            C00041() {
//            }
//
//            public void onClick(View v) {
//                EditCustomDialog.this.dismiss();
//            }
//        }
//
//        /* renamed from: th.dtv.AdvancedSettingsActivity.EditCustomDialog.2 */
//        class C00052 implements OnClickListener {
//            C00052() {
//            }
//
//            public void onClick(View v) {
//                String mCurrentPasswordStr = SETTINGS.getSettingsProviderString("PASSWORD", SETTINGS.getInitPassword());
//                String password_old = AdvancedSettingsActivity.this.mCurrentPasswordEditText.getText().toString();
//                String password_new = AdvancedSettingsActivity.this.mNewPasswordEditText.getText().toString();
//                String password_new_again = AdvancedSettingsActivity.this.mConfirmPasswrodEditText.getText().toString();
//                if (password_new.equals("") || password_new_again.equals("") || password_old.equals("")) {
//                    SETTINGS.makeText(AdvancedSettingsActivity.this.mContext, R.string.invalid_password, 0);
//                } else if (password_new.equals(password_new_again) && (password_old.equals(mCurrentPasswordStr) || password_old.equals(SETTINGS.getSuperPassword()))) {
//                    SETTINGS.putSettingsProviderString("PASSWORD", password_new);
//                    EditCustomDialog.this.dismiss();
//                } else {
//                    SETTINGS.makeText(AdvancedSettingsActivity.this.mContext, R.string.invalid_password, 0);
//                }
//            }
//        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.security_edit_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            AdvancedSettingsActivity.this.mCurrentPasswordEditText = (EditText) findViewById(R.id.current_password_edittext);
//            AdvancedSettingsActivity.this.mCurrentPasswordEditText.setBackgroundResource(17301529);
            AdvancedSettingsActivity.this.mCurrentPasswordEditText.setGravity(3);
            AdvancedSettingsActivity.this.mNewPasswordEditText = (EditText) findViewById(R.id.new_password_edittext);
//            AdvancedSettingsActivity.this.mNewPasswordEditText.setBackgroundResource(17301529);
            AdvancedSettingsActivity.this.mNewPasswordEditText.setGravity(3);
            AdvancedSettingsActivity.this.mConfirmPasswrodEditText = (EditText) findViewById(R.id.confirm_password_edittext);
//            AdvancedSettingsActivity.this.mConfirmPasswrodEditText.setBackgroundResource(17301529);
            AdvancedSettingsActivity.this.mConfirmPasswrodEditText.setGravity(3);
//            this.mBtnCancel.setOnClickListener(new C00041());
//            this.mBtnStart.setOnClickListener(new C00052());
        }

        public EditCustomDialog(Context context, int theme) {
            super(context, theme);
            this.mBtnStart = null;
            this.mBtnCancel = null;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            if (keyCode != 82) {
                return super.onKeyUp(keyCode, event);
            }
            dismiss();
            return true;
        }
    }

    public AdvancedSettingsActivity() {
        this.TAG = "AdvancedSettingsActivity";
        this.mContext = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.dtv_settings_advanced);
        this.mContext = this;
        initView();
    }

    private void initView() {
        int i;
        int i2 = 1;
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getResources().getString(R.string.off));
        items.add(getResources().getString(R.string.on));
        this.mMenuLockLinearLayout = (ComboLayout) findViewById(R.id.menu_lock_linearlayout);
        ComboLayout comboLayout = this.mMenuLockLinearLayout;
        if (SETTINGS.get_menu_lock_onff()) {
            i = 1;
        } else {
            i = 0;
        }
        comboLayout.initView((int) R.string.menu_lock, items, i, new C00001());
        this.mChannelLockLinearLayout = (ComboLayout) findViewById(R.id.channel_lock_linearlayout);
        ComboLayout comboLayout2 = this.mChannelLockLinearLayout;
        if (!SETTINGS.get_channel_lock_onoff()) {
            i2 = 0;
        }
        comboLayout2.initView((int) R.string.channel_lock, items, i2, new C00012());
        ArrayList list = new ArrayList();
        for (int i3 = 4; i3 <= 19; i3++) {
            if (i3 == 19) {
                list.add("OFF");
            } else {
                list.add("" + i3);
            }
        }
        this.mParentalRatingLinearLayout = (ComboLayout) findViewById(R.id.parental_rating_linearlayout);
        this.mParentalRatingLinearLayout.initView((int) R.string.parental_rating, list, SETTINGS.get_parental_rating_value() - 4, new C00023());
        this.mSetPasswordLinearLayout = (ComboLayout) findViewById(R.id.set_password_linearlayout);
        this.mSetPasswordLinearLayout.initView((int) R.string.password_set, 0, 0, new C00034());
        this.mSetPasswordLinearLayout.getLeftImageView().setVisibility(View.VISIBLE);
        this.mSetPasswordLinearLayout.getRightImageView().setVisibility(View.VISIBLE);
        this.mSetPasswordLinearLayout.getInfoTextView().setVisibility(View.VISIBLE);
    }

    public void onStart() {
        Log.d(this.TAG, "onStart() ");
        super.onStart();
    }

    public void onResume() {
        Log.d(this.TAG, "onResume() ");
        super.onResume();
        SETTINGS.send_led_msg("NENU");
    }

    public void onPause() {
        Log.d(this.TAG, "onPause() ");
        super.onPause();
    }

    public void onDestroy() {
        Log.d(this.TAG, "onDestroy() ");
        super.onDestroy();
    }

    void EditSecurityPassword() {
        Dialog editDialog = new EditCustomDialog(this.mContext, R.style.MyDialog);
        editDialog.show();
        LayoutParams lp = editDialog.getWindow().getAttributes();
        lp.x = 0;
        lp.y = -25;
        editDialog.getWindow().setAttributes(lp);
        editDialog.getWindow().addFlags(2);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
