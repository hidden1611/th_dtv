package th.dtv;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import java.lang.reflect.Method;

public class BlankActivity extends Activity {
    private Runnable mCheckRunnable;
    private Handler mHandle;
    private boolean mbCanLauncher;
    private boolean mbIsLaunched;

    /* renamed from: th.dtv.BlankActivity.1 */
    class C00241 implements Runnable {
        C00241() {
        }

        public void run() {
            Log.d("BlankActivity", "=====mbCanLauncher " + BlankActivity.this.mbCanLauncher);
            if (BlankActivity.this.mbCanLauncher) {
                BlankActivity.this.launcherDTV();
            }
        }
    }

    public BlankActivity() {
        this.mbCanLauncher = false;
        this.mbIsLaunched = false;
        this.mHandle = new Handler();
        this.mCheckRunnable = new C00241();
    }

    private void launcherDTV() {
        Log.d("BlankActivity", "=====launcherDTV " + this.mbIsLaunched);
        if (!this.mbIsLaunched) {
            this.mbIsLaunched = true;
            Intent intent = new Intent();
            intent.setClass(this, DtvMainActivity.class);
            intent.addFlags(268435456);
            intent.addFlags(67108864);
            intent.putExtra("book_type", getIntent().getIntExtra("book_type", 1));
            intent.putExtra("book_service_type", getIntent().getIntExtra("book_service_type", 0));
            intent.putExtra("book_service_Index", getIntent().getIntExtra("book_service_Index", -1));
            intent.putExtra("book_record_time", getIntent().getIntExtra("book_record_time", 0));
            intent.putExtra("is_all_region", getIntent().getBooleanExtra("is_all_region", false));
            intent.putExtra("LaunchByDirectLink", true);
            startActivity(intent);
            finish();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        Log.d("BlankActivity", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blank_layout);
        this.mbCanLauncher = true;
        forceStopPackage();
        this.mHandle.postDelayed(this.mCheckRunnable, 300);
    }

    protected void onResume() {
        Log.d("BlankActivity", "onResume()");
        super.onResume();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("BlankActivity", " onKeyDown() " + keyCode);
        switch (keyCode) {
            case MW.RET_INVALID_TYPE /*23*/:
                this.mHandle.removeCallbacks(this.mCheckRunnable);
                launcherDTV();
                return true;
            case MW.RET_INVALID_TELETEXT_COLOR /*24*/:
            case MW.RET_INVALID_DATA_LENGTH /*25*/:
            case 164:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    protected void onStop() {
        Log.d("BlankActivity", "onStop()");
        super.onStop();
        this.mHandle.removeCallbacks(this.mCheckRunnable);
    }

    protected void onDestroy() {
        Log.d("BlankActivity", "onDestroy()");
        super.onDestroy();
    }

    private void forceStopPackage() {
        ActivityManager activityManager = (ActivityManager) getSystemService("activity");
        try {
            Method declaredMethod = activityManager.getClass().getDeclaredMethod("forceStopPackage", new Class[]{String.class});
            declaredMethod.setAccessible(true);
            for (RunningTaskInfo runningTaskInfo : activityManager.getRunningTasks(100)) {
                String packageName = runningTaskInfo.baseActivity.getPackageName();
                if (packageName.equals("org.xbmc.xbmc") || packageName.equals("org.xbmc.kodi")) {
                    Log.d("BlankActivity", "forceStopPackage() " + packageName);
                    declaredMethod.invoke(activityManager, new Object[]{packageName});
                }
            }
        } catch (Throwable e) {
            Log.e("BlankActivity", "Can't find forceStopPackage", e);
        }
    }
}
