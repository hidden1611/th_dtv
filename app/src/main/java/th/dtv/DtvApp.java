package th.dtv;

import android.app.Activity;
import android.app.Application;
import android.util.Log;
import java.util.LinkedList;
import java.util.List;

public class DtvApp extends Application {
    private static DtvApp instance;
    private String TAG;
    private List<Activity> activityList;

    private static class SingleInstance {
        private static final DtvApp SingleInstance;

        private SingleInstance() {
        }

        static {
            SingleInstance = new DtvApp();
        }
    }

    public DtvApp() {
        this.TAG = "DtvApp";
        this.activityList = new LinkedList();
    }

    public static DtvApp getInstance() {
        if (instance == null) {
            instance = SingleInstance.SingleInstance;
        }
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        Log.d(this.TAG, "onCreate() ");
        instance = getInstance();
    }

    public void onTerminate() {
        Log.d(this.TAG, "onTerminate() ");
        super.onTerminate();
    }

    public void addActivity(Activity paramActivity) {
        this.activityList.add(paramActivity);
    }

    public void exit() {
        for (Activity finish : this.activityList) {
            finish.finish();
        }
    }
}
