package th.dtv.dvbt2;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import th.dtv.ChannelSearchActivity;
import th.dtv.ComboEditText;
import th.dtv.ComboLayout;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.activity.PswdCB;
import th.dtv.mw_data.dvb_t_area;
import th.dtv.mw_data.dvb_t_tp;
import th.dtv.mw_data.tuner_signal_status;
import th.dtv.myctrl.ScanCallBack;
import th.dtv.myctrl.ScanDialog;

public class ManualSearchActivity extends DtvBaseActivity {
    private ComboLayout Area;
    private ComboLayout Band;
    private ComboEditText Freq;
    ArrayList<Integer> SelAreaList;
    ArrayList<Integer> SelTpList;
    private ComboLayout TpNo;
    private dvb_t_area areaInfo;
    private int currentAreaIndex;
    private int currentTpIndex;
    boolean fSaveAreaTpFlag;
    private Handler mwMsgHandler;
    private ProgressBar prg_signalQuality;
    private ProgressBar prg_signalStrength;
    private int tempFreq;
    private dvb_t_tp tpInfo;
    private TextView txv_signalQuality;
    private TextView txv_signalStrength;

    /* renamed from: th.dtv.dvbt2.ManualSearchActivity.1 */
    class C02991 implements ScanCallBack {
        C02991() {
        }

        public void DoCallBackEvent(int i) {
            Log.e("ManualSearchActivity", "scan_type :" + i);
            Intent intent = new Intent();
            ManualSearchActivity.this.SelAreaList.clear();
            ManualSearchActivity.this.SelAreaList.add(Integer.valueOf(ManualSearchActivity.this.currentAreaIndex));
            ManualSearchActivity.this.SelTpList.clear();
            ManualSearchActivity.this.SelTpList.add(Integer.valueOf(ManualSearchActivity.this.currentTpIndex));
            intent.setFlags(67108864);
            intent.setClass(ManualSearchActivity.this, ChannelSearchActivity.class);
            intent.putExtra("system_type", 1);
            intent.putExtra("search_type", i);
            intent.putIntegerArrayListExtra("search_sat_index", ManualSearchActivity.this.SelAreaList);
            intent.putIntegerArrayListExtra("search_tp_index", ManualSearchActivity.this.SelTpList);
            ManualSearchActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.dvbt2.ManualSearchActivity.2 */
    class C03002 implements OnItemClickListener {
        C03002() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (ManualSearchActivity.this.currentAreaIndex != position) {
                ManualSearchActivity.this.currentAreaIndex = position;
                ManualSearchActivity.this.RefreshView(true);
            }
        }
    }

    /* renamed from: th.dtv.dvbt2.ManualSearchActivity.3 */
    class C03013 implements OnItemClickListener {
        C03013() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (ManualSearchActivity.this.currentTpIndex != position) {
                ManualSearchActivity.this.currentTpIndex = position;
                ManualSearchActivity.this.updateFreqandBand();
                ManualSearchActivity.this.LockCurrentTP();
            }
        }
    }

    /* renamed from: th.dtv.dvbt2.ManualSearchActivity.4 */
    class C03024 implements PswdCB {
        C03024() {
        }

        public void DoCallBackEvent() {
            ArrayList<String> items = new ArrayList();
            items.clear();
            int tp_count = MW.db_dvb_t_get_tp_count(ManualSearchActivity.this.currentAreaIndex);
            for (int i = 0; i < tp_count; i++) {
                if (MW.db_dvb_t_get_tp_info(ManualSearchActivity.this.currentAreaIndex, i, ManualSearchActivity.this.tpInfo) == 0) {
                    items.add("CH - " + ManualSearchActivity.this.tpInfo.channel_number + "  " + ManualSearchActivity.this.tpInfo.frq + " KHz" + "  " + ManualSearchActivity.this.tpInfo.bandwidth + "  MHz");
                }
            }
            if (MW.db_dvb_t_get_tp_info(ManualSearchActivity.this.currentAreaIndex, ManualSearchActivity.this.currentTpIndex, ManualSearchActivity.this.tpInfo) == 0) {
                ManualSearchActivity.this.TpNo.UpdateInfotext(items);
            } else {
                ManualSearchActivity.this.TpNo.UpdateInfotext(items);
            }
        }
    }

    /* renamed from: th.dtv.dvbt2.ManualSearchActivity.5 */
    class C03035 implements OnItemClickListener {
        C03035() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            ManualSearchActivity.this.tpInfo.bandwidth = position + 6;
            Log.e("LEE", "band tpInfo.area_index = " + ManualSearchActivity.this.tpInfo.area_index);
            Log.e("LEE", "band tpInfo.tp_index = " + ManualSearchActivity.this.tpInfo.tp_index);
            Log.e("LEE", "band tpInfo.channel_number = " + ManualSearchActivity.this.tpInfo.channel_number);
            Log.e("LEE", "band tpInfo.frq = " + ManualSearchActivity.this.tpInfo.frq);
            Log.e("LEE", "band tpInfo.bandwidth = " + ManualSearchActivity.this.tpInfo.bandwidth);
            MW.db_dvb_t_set_tp_info(ManualSearchActivity.this.tpInfo);
            ManualSearchActivity.this.LockCurrentTP();
            ManualSearchActivity.this.fSaveAreaTpFlag = true;
        }
    }

    /* renamed from: th.dtv.dvbt2.ManualSearchActivity.6 */
    class C03046 implements TextWatcher {
        C03046() {
        }

        public void afterTextChanged(Editable v) {
            int value;
            if (v.length() > 0) {
                value = Integer.valueOf(ManualSearchActivity.this.Freq.getInfoTextView().getText().toString()).intValue();
            } else {
                value = ManualSearchActivity.this.tpInfo.frq;
            }
            ManualSearchActivity.this.tpInfo.frq = value;
            MW.db_dvb_t_set_tp_info(ManualSearchActivity.this.tpInfo);
            ManualSearchActivity.this.LockCurrentTP();
            ManualSearchActivity.this.fSaveAreaTpFlag = true;
        }

        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    tuner_signal_status paramsInfo = (tuner_signal_status) msg.obj;
                    if (paramsInfo != null) {
                        if (paramsInfo.locked) {
                            ManualSearchActivity.this.prg_signalStrength.setProgressDrawable(ManualSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
                            ManualSearchActivity.this.prg_signalQuality.setProgressDrawable(ManualSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_locked));
                            SETTINGS.set_green_led(true);
                        } else {
                            ManualSearchActivity.this.prg_signalStrength.setProgressDrawable(ManualSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
                            ManualSearchActivity.this.prg_signalQuality.setProgressDrawable(ManualSearchActivity.this.getResources().getDrawable(R.drawable.signal_progbar_unlock));
                            SETTINGS.set_green_led(false);
                        }
                        ManualSearchActivity.this.prg_signalStrength.setProgress(paramsInfo.strength);
                        ManualSearchActivity.this.prg_signalQuality.setProgress(paramsInfo.quality);
                        if (paramsInfo.error) {
                            ManualSearchActivity.this.txv_signalStrength.setText("I2C");
                            ManualSearchActivity.this.txv_signalQuality.setText("Error");
                            return;
                        }
                        ManualSearchActivity.this.txv_signalStrength.setText(paramsInfo.strength + "%");
                        ManualSearchActivity.this.txv_signalQuality.setText(paramsInfo.quality + "%");
                    }
                default:
            }
        }
    }

    public ManualSearchActivity() {
        this.Area = null;
        this.TpNo = null;
        this.Band = null;
        this.Freq = null;
        this.areaInfo = new dvb_t_area();
        this.tpInfo = new dvb_t_tp();
        this.currentAreaIndex = 0;
        this.currentTpIndex = 0;
        this.fSaveAreaTpFlag = false;
        this.SelAreaList = new ArrayList();
        this.SelTpList = new ArrayList();
        this.tempFreq = 0;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.dvb_t_manual_search);
        initData();
        initView();
    }

    protected void onResume() {
        super.onResume();
        RefreshAllData();
        SETTINGS.send_led_msg("NENU");
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(2, true);
        LockCurrentTP();
    }

    protected void onPause() {
        super.onPause();
        if (MW.db_dvb_t_get_current_tp_info(this.tpInfo) != 0) {
            MW.db_dvb_t_set_current_area_tp(0, 0);
        } else if (!(this.tpInfo.area_index == this.currentAreaIndex && this.tpInfo.tp_index == this.currentTpIndex)) {
            MW.db_dvb_t_set_current_area_tp(this.currentAreaIndex, this.currentTpIndex);
        }
        if (this.fSaveAreaTpFlag) {
            this.fSaveAreaTpFlag = false;
            MW.db_dvb_t_save_area_tp();
        }
        enableMwMessageCallback(null);
        MW.tuner_unlock_tp(0, 1, false);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.Band.isFocused()) {
                        this.Freq.getInfoTextView().requestFocus();
                        this.Freq.getInfoTextView().setSelection(this.Freq.getInfoTextView().length());
                        this.Freq.setBackgroundResource(R.drawable.live_channel_list_item_bg_sel);
                        return true;
                    } else if (this.Freq.getInfoTextView().isFocused()) {
                        if (this.Freq.getInfoTextView().length() == 0) {
                            this.Freq.SetInfoTextView("" + this.tempFreq);
                        }
                        this.Freq.getInfoTextView().clearFocus();
                        this.TpNo.requestFocus();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.TpNo.isFocused()) {
                        this.Freq.getInfoTextView().requestFocus();
                        this.Freq.getInfoTextView().setSelection(this.Freq.getInfoTextView().length());
                        this.Freq.setBackgroundResource(R.drawable.live_channel_list_item_bg_sel);
                        return true;
                    } else if (this.Freq.getInfoTextView().isFocused()) {
                        if (this.Freq.getInfoTextView().length() == 0) {
                            this.Freq.SetInfoTextView("" + this.tempFreq);
                        }
                        this.Freq.getInfoTextView().clearFocus();
                        this.Band.requestFocus();
                        return true;
                    }
                    break;
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    DoSearch();
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void DoSearch() {
        Dialog scanActionDig = new ScanDialog(this, R.style.MyDialog, new C02991());
        scanActionDig.show();
        scanActionDig.getWindow().addFlags(2);
    }

    private void FillAreaItem() {
        int total_count = MW.db_dvb_t_get_area_count();
        ArrayList items1 = new ArrayList();
        items1.clear();
        for (int i = 0; i < total_count; i++) {
            if (MW.db_dvb_t_get_area_info(i, this.areaInfo) == 0) {
                items1.add(SETTINGS.get_area_string_by_index(this, this.areaInfo.area_index));
            }
        }
        this.Area.initView((int) R.string.Area, items1, this.currentAreaIndex, new C03002());
    }

    private void FillTpNoItem() {
        ArrayList<String> items1 = new ArrayList();
        items1.clear();
        int tp_count = MW.db_dvb_t_get_tp_count(this.currentAreaIndex);
        for (int i = 0; i < tp_count; i++) {
            if (MW.db_dvb_t_get_tp_info(this.currentAreaIndex, i, this.tpInfo) == 0) {
                items1.add("" + this.tpInfo.channel_number);
            }
        }
        this.TpNo.initView(R.string.TransponderNo, items1, this.currentTpIndex, new C03013(), new C03024());
    }

    private void FillBandItem() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add("6  MHz");
        items.add("7  MHz");
        items.add("8  MHz");
        this.Band.initView((int) R.string.Band, items, this.tpInfo.bandwidth - 6, new C03035());
    }

    private void FillFreqItem() {
        String items = "" + this.tpInfo.frq;
        this.tempFreq = this.tpInfo.frq;
        this.Freq.initView(R.string.Frequency, items, "KHz", new C03046());
    }

    private void RefreshAllData() {
        FillAreaItem();
        RefreshView(false);
    }

    private void RefreshView(boolean bResetTpIndex) {
        if (bResetTpIndex) {
            this.currentTpIndex = 0;
        }
        FillTpNoItem();
        updateFreqandBand();
    }

    private void updateFreqandBand() {
        if (MW.db_dvb_t_get_tp_info(this.currentAreaIndex, this.currentTpIndex, this.tpInfo) == 0) {
            FillBandItem();
            FillFreqItem();
            this.Band.getInfoTextView().setText(this.tpInfo.bandwidth + "  MHz");
            this.Freq.getInfoTextView().setText(this.tpInfo.frq + "");
            return;
        }
        FillBandItem();
        FillFreqItem();
        Log.e("ManualSearchActivity", "db_dvb_t_get_tp_info failed");
    }

    private void initData() {
        if (MW.db_dvb_t_get_current_tp_info(this.tpInfo) == 0) {
            this.currentAreaIndex = this.tpInfo.area_index;
            this.currentTpIndex = this.tpInfo.tp_index;
            System.out.println("tpInfo.area_index: " + this.tpInfo.area_index);
            if (this.currentAreaIndex != SETTINGS.get_current_area_index()) {
                this.currentAreaIndex = SETTINGS.get_current_area_index();
                this.currentTpIndex = 0;
            }
        } else {
            this.currentAreaIndex = SETTINGS.get_current_area_index();
            this.currentTpIndex = 0;
        }
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }

    private void initView() {
        this.Area = (ComboLayout) findViewById(R.id.Area);
        this.TpNo = (ComboLayout) findViewById(R.id.TpNo);
        this.Band = (ComboLayout) findViewById(R.id.Band);
        this.Freq = (ComboEditText) findViewById(R.id.Freq);
        this.prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
        this.prg_signalStrength.setMax(100);
        this.prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
        this.prg_signalQuality.setMax(100);
        this.prg_signalStrength.setProgress(0);
        this.prg_signalQuality.setProgress(0);
        this.txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
        this.txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);
        this.txv_signalStrength.setText("0%");
        this.txv_signalQuality.setText("0%");
    }

    private void LockCurrentTP() {
        MW.tuner_dvb_t_lock_tp(0, this.currentAreaIndex, this.currentTpIndex, false);
    }
}
