package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.book_event_data;
import th.dtv.mw_data.date_time;

public class BookListActivity extends DtvBaseActivity {
    private CustomListView book_list;
    private int book_list_item_pos;
    private BookListAdapter booklistAdapter;
    private TextView current_date_time;
    private date_time dateTime;
    Dialog dialog;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    private Handler mwMsgHandler;
    private book_event_data tmpBookEvent;

    /* renamed from: th.dtv.activity.BookListActivity.1 */
    class C01041 implements OnScrollListener {
        C01041() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            BookListActivity.this.mFirstVisibleItem = firstVisibleItem;
            BookListActivity.this.mVisibleItemCount = visibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.BookListActivity.2 */
    class C01052 implements OnItemSelectedListener {
        C01052() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            BookListActivity.this.book_list_item_pos = position;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.BookListActivity.3 */
    class C01063 implements OnKeyListener {
        C01063() {
        }

        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    if (BookListActivity.this.book_list.getCount() <= 0) {
                        return true;
                    }
                    BookListActivity.this.DoYouWanttoDeleteDialog();
                    return true;
            }
            return false;
        }
    }

    private class BookListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView book_status;
            TextView book_time;
            TextView event_name;
            TextView num;

            ViewHolder() {
            }
        }

        public BookListAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.book_get_event_count();
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.book_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.num = (TextView) convertView.findViewById(R.id.booklist_num);
                localViewHolder.book_time = (TextView) convertView.findViewById(R.id.booklist_time);
                localViewHolder.event_name = (TextView) convertView.findViewById(R.id.booklist_eventname);
                localViewHolder.book_status = (TextView) convertView.findViewById(R.id.booklist_bookstatus);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.book_get_event_by_index(pos, BookListActivity.this.tmpBookEvent) == 0) {
                Log.e("Book", "  " + (pos + 1) + ".  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(BookListActivity.this.tmpBookEvent.start_year), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_month), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_day), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_hour), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_minute), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_year), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_month), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_day), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_hour), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_minute)}) + " ## " + BookListActivity.this.tmpBookEvent.service_name + " ## " + BookListActivity.this.tmpBookEvent.event_name + " ## " + (BookListActivity.this.tmpBookEvent.book_type == 1 ? "Play" : "Record"));
                localViewHolder.num.setText((pos + 1) + "");
                localViewHolder.book_time.setText(String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(BookListActivity.this.tmpBookEvent.start_year), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_month), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_day), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_hour), Integer.valueOf(BookListActivity.this.tmpBookEvent.start_minute), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_year), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_month), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_day), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_hour), Integer.valueOf(BookListActivity.this.tmpBookEvent.end_minute)}));
                localViewHolder.event_name.setText(BookListActivity.this.tmpBookEvent.event_name);
                if (BookListActivity.this.tmpBookEvent.book_type == 1) {
                    localViewHolder.book_status.setText(BookListActivity.this.getString(R.string.Play));
                } else {
                    localViewHolder.book_status.setText(BookListActivity.this.getString(R.string.Record));
                }
            }
            return convertView;
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView txt_Title;
        TextView txt_content;

        /* renamed from: th.dtv.activity.BookListActivity.DeleteDia.1 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.txt_Title = (TextView) findViewById(R.id.deldia_title);
            this.txt_content = (TextView) findViewById(R.id.delete_content);
            this.txt_Title.setText(BookListActivity.this.getResources().getString(R.string.cancle_book));
            this.txt_content.setText(BookListActivity.this.getResources().getString(R.string.cancle_book_tips));
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.txt_Title = null;
            this.txt_content = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                case MW.RET_TP_EXIST /*10*/:
                    BookListActivity.this.booklistAdapter.notifyDataSetChanged();
                default:
            }
        }
    }

    public BookListActivity() {
        this.tmpBookEvent = new book_event_data();
        this.book_list_item_pos = 0;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 0;
        this.dateTime = new date_time();
    }

    private void initMessagehandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }

    private void initView() {
        this.book_list = (CustomListView) findViewById(R.id.booklistview);
        this.booklistAdapter = new BookListAdapter(this);
        this.book_list.setAdapter(this.booklistAdapter);
        this.book_list.setVisibleItemCount(11);
        this.current_date_time = (TextView) findViewById(R.id.current_time);
    }

    private void DoYouWanttoDeleteDialog() {
        this.dialog = new DeleteDia(this, R.style.MyDialog);
        this.dialog.show();
    }

    private void initListener() {
        this.book_list.setCustomScrollListener(new C01041());
        this.book_list.setOnItemSelectedListener(new C01052());
        this.book_list.setCustomKeyListener(new C01063());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.book_list_layout);
        initView();
        initListener();
        initMessagehandle();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.send_led_msg("NENU");
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(4, true);
        MW.register_event_type(10, true);
        if (MW.get_date_time(this.dateTime) == 0) {
            this.current_date_time.setText(String.format("%04d-%02d-%02d %02d:%02d:%02d", new Object[]{Integer.valueOf(this.dateTime.year), Integer.valueOf(this.dateTime.month), Integer.valueOf(this.dateTime.day), Integer.valueOf(this.dateTime.hour), Integer.valueOf(this.dateTime.minute), Integer.valueOf(this.dateTime.second)}));
        }
    }

    protected void onPause() {
        super.onPause();
        enableMwMessageCallback(null);
    }

    protected void onRestart() {
        super.onRestart();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                finish();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                break;
        }
        return super.onKeyUp(keyCode, event);
    }
}
