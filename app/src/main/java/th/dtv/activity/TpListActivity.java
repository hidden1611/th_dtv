package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import th.dtv.ChannelSearchActivity;
import th.dtv.ComboLayout;
import th.dtv.CustomListView;
import th.dtv.DigitsEditText;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.tuner_signal_status;

public class TpListActivity extends DtvBaseActivity {
    ArrayList<Integer> CurrentSatellite;
    private int SateIndex;
    ArrayList<Integer> SelSatList;
    ArrayList<Integer> SelTpList;
    boolean[][] Temp_TpSelStatus;
    boolean[][] TpSelStatus;
    boolean firstTime;
    private int mSateFirstVisibleItem;
    private int mSateVisibleItemCount;
    private int mTpFirstVisibleItem;
    private int mTpVisibleItemCount;
    private Handler mwMsgHandler;
    dvb_s_sat satInfo;
    private SateAdapter sateAdapter;
    private CustomListView sate_list;
    private int sate_list_item_pos;
    int[] temp_tpcount;
    dvb_s_tp tpInfo;
    private TpListAdapter tpListAdapter;
    private CustomListView tp_list;
    private int tp_list_item_pos;
    private ProgressBar tp_quality_prgbar;
    private TextView tp_quality_txt;
    private ProgressBar tp_strength_prgbar;
    private TextView tp_strength_txt;
    int[] ttemp_tpcount;

    /* renamed from: th.dtv.activity.TpListActivity.1 */
    class C02811 implements OnScrollListener {
        C02811() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TpListActivity.this.mTpFirstVisibleItem = firstVisibleItem;
            TpListActivity.this.mTpVisibleItemCount = visibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.TpListActivity.2 */
    class C02822 implements OnItemSelectedListener {
        C02822() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            TpListActivity.this.tp_list_item_pos = position;
            MW.tuner_dvb_s_lock_tp(0, TpListActivity.this.SateIndex, TpListActivity.this.tp_list_item_pos, false);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.TpListActivity.3 */
    class C02833 implements OnScrollListener {
        C02833() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TpListActivity.this.mSateFirstVisibleItem = firstVisibleItem;
            TpListActivity.this.mSateVisibleItemCount = visibleItemCount;
            System.out.println("mSateVisibleItemCount" + TpListActivity.this.mSateVisibleItemCount);
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.TpListActivity.4 */
    class C02844 implements OnItemSelectedListener {
        C02844() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            Log.e("TpListActivity", "satellite list item Position" + position);
            TpListActivity.this.sate_list_item_pos = position;
            if (MW.db_dvb_s_get_selected_sat_info(0, position, TpListActivity.this.satInfo) == 0) {
                TpListActivity.this.SateIndex = TpListActivity.this.satInfo.sat_index;
            }
            TpListActivity.this.refreshTpList();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.TpListActivity.5 */
    class C02855 implements OnKeyListener {
        C02855() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                case MW.RET_INVALID_TP_INDEX /*20*/:
                case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                    System.out.println("sate_list.getcount :" + TpListActivity.this.sate_list.getCount() + "tp_list_item_pos :" + TpListActivity.this.tp_list_item_pos);
                    if (TpListActivity.this.sate_list != null && TpListActivity.this.sate_list.getCount() > 1) {
                        if (TpListActivity.this.tp_list.getSelectedView() != null) {
                            TpListActivity.this.tp_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                        }
                        TpListActivity.this.tp_list_item_pos = 0;
                        TpListActivity.this.tp_list.setCustomSelection(TpListActivity.this.tp_list_item_pos);
                    }
                    int currentPos = TpListActivity.this.sate_list.getSelectedItemPosition();
                    TpListActivity.this.sate_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                case MW.RET_INVALID_TYPE /*23*/:
                    if (TpListActivity.this.tp_list.getCount() <= 0) {
                        return true;
                    }
                    TpListActivity.this.tp_list.requestFocus();
                    TpListActivity.this.tp_list.setSelectionFromTop(TpListActivity.this.tp_list_item_pos, TpListActivity.this.mTpFirstVisibleItem);
                    TpListActivity.this.tpListAdapter.notifyDataSetChanged();
                    TpListActivity.this.tp_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                    if (TpListActivity.this.tp_list.getSelectedItemPosition() < 0) {
                        return true;
                    }
                    TpListActivity.this.sate_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                    TpListActivity.this.tp_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                    return true;
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.TpListActivity.6 */
    class C02866 implements OnKeyListener {
        C02866() {
        }

        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    TpListActivity.this.sate_list.requestFocus();
                    TpListActivity.this.sate_list.setSelectionFromTop(TpListActivity.this.sate_list_item_pos, TpListActivity.this.mSateFirstVisibleItem);
                    TpListActivity.this.sateAdapter.notifyDataSetChanged();
                    TpListActivity.this.sate_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                    if (TpListActivity.this.tp_list.getSelectedItemPosition() < 0) {
                        return true;
                    }
                    TpListActivity.this.tp_list.getSelectedView().setBackgroundResource(R.drawable.list_446_49_sele_expired);
                    return true;
                case MW.RET_INVALID_TYPE /*23*/:
                    TpListActivity.this.RefreshTpSelectImage();
                    return true;
                case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                    TpListActivity.this.EditTp();
                    return true;
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    TpListActivity.this.ScanAction();
                    return true;
                case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                    TpListActivity.this.DeleteTp();
                    return true;
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    TpListActivity.this.AddTp();
                    return true;
            }
            return false;
        }
    }

    public class AddDialog extends Dialog {
        ComboLayout combo_polarization;
        DigitsEditText edittext_frequency;
        DigitsEditText edittext_symbol;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        TextView mFrequency;
        private LayoutInflater mInflater;
        View mLayoutView;
        Switch mNetworkSwitch;
        TextView mPolarity;
        TextView mSymbol;
        TextView mTitle;
        String mode_str;
        int temp_pol;

        /* renamed from: th.dtv.activity.TpListActivity.AddDialog.1 */
        class C02871 implements OnItemClickListener {
            C02871() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                AddDialog.this.temp_pol = position;
            }
        }
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.dvbs_ts_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.edittext_frequency = (DigitsEditText) findViewById(R.id.edittext_frequency);
            this.edittext_symbol = (DigitsEditText) findViewById(R.id.edittext_symbol);
            this.edittext_frequency.setBackgroundResource(17301529);
            this.edittext_frequency.setFilters(new InputFilter[]{new LengthFilter(5)});
            TextView ts_number = (TextView) findViewById(R.id.item_no);
            this.edittext_symbol.setFilters(new InputFilter[]{new LengthFilter(5)});
            this.edittext_symbol.setBackgroundResource(17301529);
            this.combo_polarization = (ComboLayout) findViewById(R.id.combo_polarization);
            this.mTitle.setText(R.string.add);
            ts_number.setText(String.valueOf(MW.db_dvb_s_get_tp_count(0, TpListActivity.this.SateIndex) + 1));
            this.edittext_frequency.setDigitsText("");
            this.edittext_symbol.setDigitsText("");
            ArrayList items = new ArrayList();
            items.clear();
            items.add(TpListActivity.this.getResources().getString(R.string.Vertical));
            items.add(TpListActivity.this.getResources().getString(R.string.Horizontal));
            this.combo_polarization.initView((int) R.string.Polarization, items, this.temp_pol, new C02871());
            this.combo_polarization.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.combo_polarization.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 120) / 1280, -2));
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (!this.edittext_symbol.isFocused()) {
                        this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (!this.edittext_frequency.isFocused()) {
                        this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (!(this.mBtnStart.isFocused() || this.mBtnCancel.isFocused())) {
                        return true;
                    }
            }
            return super.onKeyDown(keyCode, event);
        }

        public AddDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mFrequency = null;
            this.mSymbol = null;
            this.mPolarity = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.mNetworkSwitch = null;
            this.mode_str = "default";
            this.temp_pol = 0;
            this.combo_polarization = null;
            this.edittext_frequency = null;
            this.edittext_symbol = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class EditDialog extends Dialog {
        ComboLayout combo_polarization;
        DigitsEditText edittext_frequency;
        DigitsEditText edittext_symbol;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        TextView mFrequency;
        private LayoutInflater mInflater;
        View mLayoutView;
        Switch mNetworkSwitch;
        TextView mPolarity;
        TextView mSymbol;
        String mode_str;
        int temp_pol;

        /* renamed from: th.dtv.activity.TpListActivity.EditDialog.1 */
        class C02921 implements OnItemClickListener {
            C02921() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                EditDialog.this.temp_pol = position;
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.dvbs_ts_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.edittext_frequency = (DigitsEditText) findViewById(R.id.edittext_frequency);
            this.edittext_symbol = (DigitsEditText) findViewById(R.id.edittext_symbol);
            this.edittext_frequency.setBackgroundResource(17301529);
            this.edittext_frequency.setFilters(new InputFilter[]{new LengthFilter(5)});
            TextView ts_number = (TextView) findViewById(R.id.item_no);
            this.edittext_symbol.setFilters(new InputFilter[]{new LengthFilter(5)});
            this.edittext_symbol.setBackgroundResource(17301529);
            this.combo_polarization = (ComboLayout) findViewById(R.id.combo_polarization);
            ts_number.setText(String.valueOf(TpListActivity.this.tp_list_item_pos + 1));
            this.edittext_frequency.setDigitsText(String.valueOf(TpListActivity.this.tpInfo.frq));
            this.edittext_symbol.setDigitsText(String.valueOf(TpListActivity.this.tpInfo.sym));
            ArrayList items = new ArrayList();
            items.clear();
            items.add(TpListActivity.this.getResources().getString(R.string.Vertical));
            items.add(TpListActivity.this.getResources().getString(R.string.Horizontal));
            this.combo_polarization.initView((int) R.string.Polarization, items, TpListActivity.this.tpInfo.pol, new C02921());
            this.combo_polarization.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.combo_polarization.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 120) / 1280, -2));
            this.edittext_frequency.requestFocus();
            this.edittext_frequency.selectAll();
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (!this.edittext_symbol.isFocused()) {
                        this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (!this.edittext_frequency.isFocused()) {
                        this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_polarization.getInfoTextView().setTextColor(TpListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (!(this.mBtnStart.isFocused() || this.mBtnCancel.isFocused())) {
                        return true;
                    }
            }
            return super.onKeyDown(keyCode, event);
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mFrequency = null;
            this.mSymbol = null;
            this.mPolarity = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mNetworkSwitch = null;
            this.mode_str = "default";
            this.temp_pol = 0;
            this.edittext_frequency = null;
            this.edittext_symbol = null;
            this.combo_polarization = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                default:
            }
        }
    }

    class SateAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView sate_name;
            TextView sate_no;

            ViewHolder() {
            }
        }

        public SateAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_dvb_s_get_selected_sat_count(0);
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.tp_satellite_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.sate_no = (TextView) convertView.findViewById(R.id.selected_sate_no);
                localViewHolder.sate_name = (TextView) convertView.findViewById(R.id.selected_sate_name);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.db_dvb_s_get_selected_sat_info(0, position, TpListActivity.this.satInfo) == 0) {
                localViewHolder.sate_no.setText("" + (position + 1));
                localViewHolder.sate_name.setText(TpListActivity.this.satInfo.sat_name + "(" + TpListActivity.this.satInfo.sat_degree_dec + "." + TpListActivity.this.satInfo.sat_degree_point + (TpListActivity.this.satInfo.sat_position == 0 ? "E" : "W") + ")");
            } else {
                localViewHolder.sate_no.setText("");
                localViewHolder.sate_name.setText("");
            }
            return convertView;
        }
    }

    public class ScanActionDig extends Dialog {
        private LinearLayout channel_type_ll;
        private TextView channel_type_opt;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private LinearLayout nit_search_ll;
        private TextView nit_search_opt;
        private LinearLayout scan_type_ll;
        private TextView scan_type_opt;
        private LinearLayout service_type_ll;
        private TextView servicetype_opt;

        private void RefreshBg() {
            if (this.channel_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.servicetype_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.scan_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.nit_search_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
            }
            RefreshNitSearchBg(this.nit_search_opt.isFocusable());
        }

        private void RefreshNitSearchBg(boolean enabled) {
            if (enabled) {
                this.nit_search_opt.setEnabled(true);
                this.nit_search_opt.setFocusable(true);
                if (this.nit_search_opt.isFocused()) {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                    return;
                } else {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                    return;
                }
            }
            this.nit_search_opt.setEnabled(false);
            this.nit_search_opt.setFocusable(false);
            this.nit_search_ll.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.satellite_scan_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.satellite_scan_dialog, null);
            this.channel_type_ll = (LinearLayout) findViewById(R.id.channel_type_ll);
            this.service_type_ll = (LinearLayout) findViewById(R.id.service_type_ll);
            this.scan_type_ll = (LinearLayout) findViewById(R.id.scan_type_ll);
            this.nit_search_ll = (LinearLayout) findViewById(R.id.nit_search_ll);
            this.channel_type_opt = (TextView) findViewById(R.id.channel_type_opt);
            this.servicetype_opt = (TextView) findViewById(R.id.servicetype_opt);
            this.scan_type_opt = (TextView) findViewById(R.id.scan_type_opt);
            this.nit_search_opt = (TextView) findViewById(R.id.nit_search_opt);
            if (SETTINGS.get_search_nit()) {
                this.nit_search_opt.setText(TpListActivity.this.getResources().getString(R.string.on));
            } else {
                this.nit_search_opt.setText(TpListActivity.this.getResources().getString(R.string.off));
            }
            if (SETTINGS.get_search_service_type() == 0) {
                this.channel_type_opt.setText(TpListActivity.this.getResources().getString(R.string.All));
            } else {
                this.channel_type_opt.setText(TpListActivity.this.getResources().getString(R.string.FTAOnly));
            }
            if (SETTINGS.get_search_tv_type() == 0) {
                this.servicetype_opt.setText(TpListActivity.this.getResources().getString(R.string.All));
            } else if (SETTINGS.get_search_tv_type() == 1) {
                this.servicetype_opt.setText(TpListActivity.this.getResources().getString(R.string.TV));
            } else if (SETTINGS.get_search_tv_type() == 2) {
                this.servicetype_opt.setText(TpListActivity.this.getResources().getString(R.string.Radio));
            }
            if (MW.db_dvb_s_get_tp_count(0, TpListActivity.this.SateIndex) == 0) {
                this.scan_type_opt.setText(TpListActivity.this.getResources().getString(R.string.blind_scan));
                RefreshNitSearchBg(false);
            } else {
                this.scan_type_opt.setText(TpListActivity.this.getResources().getString(R.string.manual_scan));
            }
            this.scan_type_opt.requestFocus();
            RefreshBg();
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            int i2 = 2;
            if (keyEvent.getAction() != 0) {
                switch (i) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (i) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.nit_search_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.scan_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.scan_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.scan_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.channel_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.nit_search_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TYPE /*23*/:
                    boolean z;
                    int i3;
                    int i4;
                    if (this.nit_search_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.on))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (this.channel_type_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.All))) {
                        i3 = 0;
                    } else {
                        i3 = 1;
                    }
                    if (this.servicetype_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.All))) {
                        i4 = 0;
                    } else if (this.servicetype_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.TV))) {
                        i4 = 1;
                    } else if (this.servicetype_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.Radio))) {
                        i4 = 2;
                    } else {
                        i4 = 0;
                    }
                    if (this.scan_type_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.auto_scan))) {
                        i2 = 1;
                    } else if (this.scan_type_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.blind_scan))) {
                        i2 = 0;
                    } else if (!this.scan_type_opt.getText().toString().equals(TpListActivity.this.getResources().getString(R.string.manual_scan))) {
                        i2 = 0;
                    }
                    SETTINGS.set_search_service_type(i3);
                    SETTINGS.set_search_tv_type(i4);
                    SETTINGS.set_search_nit(z);
                    if (MW.db_dvb_s_get_tp_count(0, TpListActivity.this.SateIndex) > 0 || i2 == 0) {
                        TpListActivity.this.getCurrentTpSatData();
                        TpListActivity.this.getSelTpData();
                        Intent intent = new Intent();
                        intent.setFlags(67108864);
                        intent.setClass(TpListActivity.this, ChannelSearchActivity.class);
                        intent.putExtra("system_type", 0);
                        intent.putExtra("search_type", i2);
                        intent.putIntegerArrayListExtra("search_sat_index", TpListActivity.this.CurrentSatellite);
                        intent.putIntegerArrayListExtra("search_tp_index", TpListActivity.this.SelTpList);
                        TpListActivity.this.startActivity(intent);
                    } else {
                        TpListActivity.this.ShowToastInformation(TpListActivity.this.getResources().getString(R.string.add_tp), 1);
                    }
                    dismiss();
                    break;
            }
            return super.onKeyDown(i, keyEvent);
        }

        public ScanActionDig(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    class TpListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView tp_frequency;
            TextView tp_no;
            TextView tp_pol;
            ImageView tp_sel_img;
            TextView tp_symbol;

            ViewHolder() {
            }
        }

        public TpListAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_dvb_s_get_tp_count(0, TpListActivity.this.SateIndex);
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.tp_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.tp_no = (TextView) convertView.findViewById(R.id.tp_no);
                localViewHolder.tp_sel_img = (ImageView) convertView.findViewById(R.id.tp_select_img);
                localViewHolder.tp_frequency = (TextView) convertView.findViewById(R.id.tp_frequency);
                localViewHolder.tp_pol = (TextView) convertView.findViewById(R.id.tp_pol);
                localViewHolder.tp_symbol = (TextView) convertView.findViewById(R.id.tp_symbol);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            System.out.println("SateIndex :::" + TpListActivity.this.SateIndex);
            if (MW.db_dvb_s_get_tp_info(0, TpListActivity.this.SateIndex, position, TpListActivity.this.tpInfo) == 0) {
                if (TpListActivity.this.TpSelStatus != null) {
                    System.out.println("not null :" + position);
                    if (TpListActivity.this.TpSelStatus[TpListActivity.this.SateIndex] != null) {
                        System.out.println("TpSelStatus[ " + TpListActivity.this.SateIndex + "][" + position + "]" + TpListActivity.this.TpSelStatus[TpListActivity.this.SateIndex][position]);
                        if (TpListActivity.this.TpSelStatus[TpListActivity.this.SateIndex][position]) {
                            localViewHolder.tp_sel_img.setVisibility(View.VISIBLE);
                        } else {
                            localViewHolder.tp_sel_img.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        localViewHolder.tp_sel_img.setVisibility(View.INVISIBLE);
                    }
                } else {
                    System.out.println("null :" + position);
                }
                localViewHolder.tp_no.setText(String.valueOf(position + 1));
                localViewHolder.tp_frequency.setText(String.valueOf(TpListActivity.this.tpInfo.frq) + " " + "Mhz");
                if (TpListActivity.this.tpInfo.pol == 0) {
                    localViewHolder.tp_pol.setText("V");
                } else {
                    localViewHolder.tp_pol.setText("H");
                }
                localViewHolder.tp_symbol.setText(String.valueOf(TpListActivity.this.tpInfo.sym) + " " + "Kb/s");
            } else {
                System.out.println("db_dvb_s_get_tp_info failed ");
                localViewHolder.tp_frequency.setText("");
                localViewHolder.tp_pol.setText("");
                localViewHolder.tp_symbol.setText("");
                localViewHolder.tp_sel_img.setVisibility(View.INVISIBLE);
            }
            return convertView;
        }
    }

    public TpListActivity() {
        this.mSateFirstVisibleItem = 0;
        this.mSateVisibleItemCount = 0;
        this.sate_list_item_pos = 0;
        this.satInfo = new dvb_s_sat();
        this.mTpFirstVisibleItem = 0;
        this.mTpVisibleItemCount = 0;
        this.tp_list_item_pos = 0;
        this.tpInfo = new dvb_s_tp();
        this.TpSelStatus = (boolean[][]) null;
        this.Temp_TpSelStatus = (boolean[][]) null;
        this.temp_tpcount = null;
        this.ttemp_tpcount = null;
        this.SelSatList = new ArrayList();
        this.SateIndex = -1;
        this.CurrentSatellite = new ArrayList();
        this.SelTpList = new ArrayList();
        this.firstTime = true;
    }

    private void initProbarView() {
        this.tp_strength_prgbar = (ProgressBar) findViewById(R.id.tp_strength_prgbar);
        this.tp_strength_txt = (TextView) findViewById(R.id.tp_strength_txt);
        this.tp_quality_prgbar = (ProgressBar) findViewById(R.id.tp_quality_prgbar);
        this.tp_quality_txt = (TextView) findViewById(R.id.tp_quality_txt);
        this.tp_strength_prgbar.setMax(100);
        this.tp_strength_prgbar.setProgress(0);
        this.tp_strength_txt.setText("0%");
        this.tp_quality_prgbar.setMax(100);
        this.tp_quality_prgbar.setProgress(0);
        this.tp_quality_txt.setText("0%");
    }

    private void initTpListView() {
        this.tpListAdapter = new TpListAdapter(this);
        this.tp_list.setAdapter(this.tpListAdapter);
        this.tp_list.setCustomScrollListener(new C02811());
        this.tp_list.setOnItemSelectedListener(new C02822());
        this.tp_list.setVisibleItemCount(9);
    }

    private void initSateListView() {
        this.sate_list = (CustomListView) findViewById(R.id.sate_list);
        this.sateAdapter = new SateAdapter(this);
        this.sate_list.setAdapter(this.sateAdapter);
        this.sate_list.setCustomScrollListener(new C02833());
        this.sate_list.setOnItemSelectedListener(new C02844());
        this.sate_list.setVisibleItemCount(10);
        int sat_index = getIntent().getIntExtra("selected_sat_index", 0);
        this.sate_list_item_pos = sat_index;
        if (MW.db_dvb_s_get_selected_sat_info(0, sat_index, this.satInfo) == 0) {
            this.SateIndex = this.satInfo.sat_index;
            this.sate_list.setCustomSelection(this.sate_list_item_pos);
            return;
        }
        this.SateIndex = 0;
        this.sate_list.setCustomSelection(0);
    }

    private void initListener() {
        this.sate_list.setCustomKeyListener(new C02855());
        this.tp_list.setCustomKeyListener(new C02866());
    }

    private void RefreshTpSelectImage() {
        if (MW.db_dvb_s_get_tp_info(0, this.SateIndex, this.tp_list_item_pos, this.tpInfo) == 0) {
            if (this.TpSelStatus == null) {
                System.out.println("Tpsle is null");
            } else if (this.TpSelStatus[this.SateIndex] != null) {
                this.TpSelStatus[this.SateIndex][this.tp_list_item_pos] = !this.TpSelStatus[this.SateIndex][this.tp_list_item_pos];
            }
        }
        MW.db_dvb_s_set_sat_info(0, this.satInfo);
        this.tpListAdapter.notifyDataSetChanged();
    }

    void ShowToastInformation(String text, int duration_mode) {
        LinearLayout toast_view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
        ((TextView) toast_view.findViewById(R.id.toast_text)).setText(text);
        Toast toast = new Toast(this);
        if (duration_mode == 0) {
            toast.setDuration(0);
        } else if (duration_mode == 1) {
            toast.setDuration(1);
        }
        toast.setView(toast_view);
        toast.setGravity(17, 0, -70);
        toast.show();
    }

    private void getCurrentTpSatData() {
        this.CurrentSatellite.clear();
        this.CurrentSatellite.add(Integer.valueOf(this.SateIndex));
    }

    private void getSelTpData() {
        this.SelTpList.clear();
        if (this.TpSelStatus != null) {
            for (int j = 0; j < MW.db_dvb_s_get_tp_count(0, this.SateIndex); j++) {
                if (this.TpSelStatus[this.SateIndex][j]) {
                    System.out.println("TpSelStatus   : " + j);
                    this.SelTpList.add(Integer.valueOf(j));
                }
            }
        }
        if (this.SelTpList.size() == 0) {
            this.SelTpList.add(Integer.valueOf(this.tp_list_item_pos));
        }
    }

    private void ScanAction() {
        Dialog scanActionDig = new ScanActionDig(this, R.style.MyDialog);
        scanActionDig.setContentView(R.layout.satellite_scan_dialog);
        scanActionDig.show();
        scanActionDig.getWindow().addFlags(2);
    }

    private void DeleteTp() {
        if (MW.db_dvb_s_get_sat_count(0) == 0 || MW.db_dvb_s_get_sat_info(0, this.SateIndex, this.satInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_sat), 0);
        } else if (MW.db_dvb_s_get_tp_info(0, this.SateIndex, this.tp_list_item_pos, this.tpInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_tp), 0);
        } else {
            new DeleteDia(this, R.style.MyDialog).show();
        }
    }

    private void EditTp() {
        if (MW.db_dvb_s_get_sat_count(0) == 0 || MW.db_dvb_s_get_sat_info(0, this.SateIndex, this.satInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_sat), 0);
        } else if (MW.db_dvb_s_get_tp_info(0, this.SateIndex, this.tp_list_item_pos, this.tpInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_tp), 0);
        } else {
            Dialog editDialog = new EditDialog(this, R.style.MyDialog);
            editDialog.setContentView(R.layout.dvbs_ts_edit_custom_dia);
            editDialog.show();
            editDialog.getWindow().addFlags(2);
        }
    }

    private void AddTp() {
        Dialog addDialog = new AddDialog(this, R.style.MyDialog);
        addDialog.setContentView(R.layout.dvbs_ts_edit_custom_dia);
        addDialog.show();
        addDialog.getWindow().addFlags(2);
    }

    private void refreshTpList() {
        if (this.firstTime) {
            System.out.println("true");
            int sat_index = getIntent().getIntExtra("selected_sat_index", 0);
            int tp_index = getIntent().getIntExtra("selected_tp_index", 0);
            if (this.sate_list.getSelectedView() != null) {
                this.sate_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
            }
            if (MW.db_dvb_s_get_sat_info(0, sat_index, this.satInfo) != 0) {
                System.out.println("refreshTpList firsttime === falied ");
            } else if (this.satInfo.tp_count > 0 && MW.db_dvb_s_get_tp_info(0, sat_index, tp_index, this.tpInfo) == 0) {
                if (this.sate_list.getSelectedView() != null) {
                    this.sate_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                }
                this.tp_list.requestFocus();
                this.tp_list.setCustomSelection(this.tpInfo.tp_index);
                this.tpListAdapter.notifyDataSetChanged();
            }
            this.firstTime = false;
            return;
        }
        if (this.tp_list.getChildAt(this.tp_list_item_pos) != null) {
            this.tp_list.getChildAt(this.tp_list_item_pos).setBackgroundResource(R.drawable.listview_item_bg_selector);
        }
        this.tp_list_item_pos = 0;
        this.tp_list.setCustomSelection(this.tp_list_item_pos);
        if (this.sate_list != null && this.sate_list.getCount() > 0) {
            MW.db_dvb_s_get_tp_info(0, this.SateIndex, this.tp_list_item_pos, this.tpInfo);
        }
        if (MW.db_dvb_s_get_sat_info(0, this.SateIndex, this.satInfo) == 0) {
            this.tpListAdapter.notifyDataSetChanged();
        } else {
            this.tpListAdapter.notifyDataSetChanged();
        }
    }

    private void initData() {
        int satcount = MW.db_dvb_s_get_sat_count(0);
        if (satcount <= 0) {
            return;
        }
        int i;
        int j;
        int tpcount;
        if (this.TpSelStatus != null) {
            for (i = 0; i < satcount; i++) {
                this.ttemp_tpcount[i] = this.temp_tpcount[i];
            }
            this.Temp_TpSelStatus = new boolean[satcount][];
            for (i = 0; i < satcount; i++) {
                if (this.temp_tpcount[i] > 0) {
                    this.Temp_TpSelStatus[i] = new boolean[this.temp_tpcount[i]];
                    for (j = 0; j < this.temp_tpcount[i]; j++) {
                        this.Temp_TpSelStatus[i][j] = this.TpSelStatus[i][j];
                    }
                } else {
                    this.Temp_TpSelStatus[i] = null;
                }
            }
            this.TpSelStatus = new boolean[satcount][];
            System.out.println("TpSelStatus " + this.TpSelStatus);
            for (i = 0; i < satcount; i++) {
                tpcount = MW.db_dvb_s_get_tp_count(0, i);
                this.temp_tpcount[i] = tpcount;
                if (tpcount > 0) {
                    this.TpSelStatus[i] = new boolean[tpcount];
                    for (j = 0; j < tpcount; j++) {
                        this.TpSelStatus[i][j] = false;
                    }
                } else {
                    this.TpSelStatus[i] = null;
                }
            }
            for (i = 0; i < satcount; i++) {
                if (this.ttemp_tpcount[i] < this.temp_tpcount[i]) {
                    for (j = 0; j < this.ttemp_tpcount[i]; j++) {
                        this.TpSelStatus[i][j] = this.Temp_TpSelStatus[i][j];
                    }
                    this.TpSelStatus[i][this.temp_tpcount[i] - 1] = true;
                }
            }
            return;
        }
        this.TpSelStatus = new boolean[satcount][];
        this.ttemp_tpcount = new int[satcount];
        this.temp_tpcount = new int[satcount];
        System.out.println("TpSelStatus " + this.TpSelStatus);
        for (i = 0; i < satcount; i++) {
            tpcount = MW.db_dvb_s_get_tp_count(0, i);
            this.temp_tpcount[i] = tpcount;
            if (tpcount > 0) {
                this.TpSelStatus[i] = new boolean[tpcount];
                for (j = 0; j < tpcount; j++) {
                    this.TpSelStatus[i][j] = false;
                }
            } else {
                this.TpSelStatus[i] = null;
            }
        }
    }

    private void initView() {
        this.tp_list = (CustomListView) findViewById(R.id.tp_list);
        initSateListView();
        initTpListView();
        initProbarView();
        initListener();
        initMessageHandle();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    ScanAction();
                    return true;
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    if (MW.db_dvb_s_get_sat_count(0) > 0) {
                        AddTp();
                        return true;
                    }
                    ShowToastInformation(getResources().getString(R.string.no_satellite), 0);
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.tplist_activity);
        if (MW.db_dvb_s_get_selected_sat_count(0) == 0) {
            ShowToastInformation(getResources().getString(R.string.no_selected_satellite), 0);
            finishMyself();
            return;
        }
        initView();
    }

    protected void onResume() {
        super.onResume();
        initData();
        if (MW.db_dvb_s_get_sat_info(0, this.SateIndex, this.satInfo) == 0) {
            this.sate_list.setCustomSelection(this.sate_list_item_pos);
        }
        this.tpListAdapter.notifyDataSetChanged();
        SETTINGS.send_led_msg("NENU");
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(2, true);
        if (this.SateIndex >= 0 && this.tp_list.getCount() > 0) {
            MW.tuner_dvb_s_lock_tp(0, this.SateIndex, this.tp_list_item_pos, false);
        }
    }

    protected void onPause() {
        super.onPause();
        enableMwMessageCallback(null);
        if (this.SateIndex >= 0 && this.tp_list.getCount() > 0) {
            MW.tuner_unlock_tp(0, 0, false);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void initMessageHandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }
}
