package th.dtv.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import th.dtv.AntennaSetup2Activity;
import th.dtv.AntennaSetupActivity;
import th.dtv.ChannelSearchActivity;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.PasswordDialog;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.StorageDevice;
import th.dtv.StorageDevice.DeviceItem;
import th.dtv.dvbt2.ManualSearchActivity;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.dvb_t_area;

public class InstallerActivity extends DtvBaseActivity {
    private LinearLayout Ant2_ll;
    private LinearLayout Ant_ll;
    private LinearLayout AutoSearch_ll;
    StorageDeviceDialog DeviceInfoDia;
    private LinearLayout ManualSearch_ll;
    private LinearLayout MultiScan_ll;
    private LinearLayout Sat_ll;
    private int System_type;
    private Button ant2_btn;
    private Button ant_btn;
    private dvb_t_area areaInfo;
    private Button autosearch_btn;
    private int currentAreaIndex;
    Dialog dlg;
    private Button export_btn;
    private LinearLayout export_ll;
    private Button factory_btn;
    private Button import_btn;
    private LinearLayout import_ll;
    private BroadcastReceiver mTipsReceiver;
    private Button manualsearch_btn;
    private Button motor_btn;
    private LinearLayout motor_ll;
    private Button multiscan_btn;
    int passwd;
    private Button sat_btn;
    private int storageDeviceCount;
    private StorageDevice storageDeviceInfo;
    private StorageDevicePlugReceiver storageDevicePlugReceiver;
    private Button tp_btn;
    private LinearLayout tp_ll;

    /* renamed from: th.dtv.activity.InstallerActivity.1 */
    class C01661 implements OnClickListener {
        C01661() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, AntennaSetupActivity.class);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.2 */
    class C01672 implements OnClickListener {
        C01672() {
        }

        public void onClick(View view) {
            int i = 0;
            dvb_s_sat th_dtv_mw_data_dvb_s_sat = new dvb_s_sat();
            if (MW.db_dvb_s_get_current_sat_info(0, th_dtv_mw_data_dvb_s_sat) == 0) {
                i = th_dtv_mw_data_dvb_s_sat.sat_index;
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, SatelliteListActivity.class);
            intent.putExtra("selected_sat_index", i);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.3 */
    class C01683 implements OnClickListener {
        C01683() {
        }

        public void onClick(View view) {
            int i;
            int i2 = 0;
            dvb_s_tp th_dtv_mw_data_dvb_s_tp = new dvb_s_tp();
            dvb_s_sat th_dtv_mw_data_dvb_s_sat = new dvb_s_sat();
            if (MW.db_dvb_s_get_current_tp_info(0, th_dtv_mw_data_dvb_s_tp) == 0) {
                i = 0;
                while (i < MW.db_dvb_s_get_selected_sat_count(0)) {
                    if (MW.db_dvb_s_get_selected_sat_info(0, i, th_dtv_mw_data_dvb_s_sat) == 0 && th_dtv_mw_data_dvb_s_sat.sat_index == th_dtv_mw_data_dvb_s_tp.sat_index) {
                        break;
                    }
                    i++;
                }
                i = 0;
                i2 = th_dtv_mw_data_dvb_s_tp.tp_index;
            } else {
                i = 0;
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, TpListActivity.class);
            intent.putExtra("selected_sat_index", i);
            intent.putExtra("selected_tp_index", i2);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.4 */
    class C01694 implements OnClickListener {
        C01694() {
        }

        public void onClick(View view) {
            int i;
            int i2 = 0;
            dvb_s_tp th_dtv_mw_data_dvb_s_tp = new dvb_s_tp();
            dvb_s_sat th_dtv_mw_data_dvb_s_sat = new dvb_s_sat();
            if (MW.db_dvb_s_get_current_tp_info(0, th_dtv_mw_data_dvb_s_tp) == 0) {
                i = 0;
                while (i < MW.db_dvb_s_get_selected_sat_count(0)) {
                    if (MW.db_dvb_s_get_selected_sat_info(0, i, th_dtv_mw_data_dvb_s_sat) == 0 && th_dtv_mw_data_dvb_s_sat.sat_index == th_dtv_mw_data_dvb_s_tp.sat_index) {
                        break;
                    }
                    i++;
                }
                i = 0;
                i2 = th_dtv_mw_data_dvb_s_tp.tp_index;
            } else {
                i = 0;
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, MotorSettingActivity.class);
            intent.putExtra("selected_sat_index", i);
            intent.putExtra("selected_tp_index", i2);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.5 */
    class C01705 implements OnClickListener {
        C01705() {
        }

        public void onClick(View view) {
            int i = 0;
            dvb_s_tp th_dtv_mw_data_dvb_s_tp = new dvb_s_tp();
            dvb_s_sat th_dtv_mw_data_dvb_s_sat = new dvb_s_sat();
            if (MW.db_dvb_s_get_current_tp_info(0, th_dtv_mw_data_dvb_s_tp) == 0) {
                for (int i2 = 0; i2 < MW.db_dvb_s_get_selected_sat_count(0); i2++) {
                    if (MW.db_dvb_s_get_selected_sat_info(0, i2, th_dtv_mw_data_dvb_s_sat) == 0 && th_dtv_mw_data_dvb_s_sat.sat_index == th_dtv_mw_data_dvb_s_tp.sat_index) {
                        i = i2;
                        break;
                    }
                }
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, SelectedSatelliteListActivity.class);
            intent.putExtra("selected_sat_index", i);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.6 */
    class C01716 implements OnClickListener {
        C01716() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, AntennaSetup2Activity.class);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.7 */
    class C01727 implements OnClickListener {
        C01727() {
        }

        public void onClick(View view) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Intent intent = new Intent();
            arrayList.clear();
            arrayList.add(Integer.valueOf(SETTINGS.get_current_area_index()));
            arrayList2.clear();
            arrayList2.add(Integer.valueOf(0));
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, ChannelSearchActivity.class);
            intent.putExtra("system_type", 1);
            intent.putExtra("search_type", 1);
            intent.putIntegerArrayListExtra("search_sat_index", arrayList);
            intent.putIntegerArrayListExtra("search_tp_index", arrayList2);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.8 */
    class C01738 implements OnClickListener {
        C01738() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(InstallerActivity.this, ManualSearchActivity.class);
            InstallerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.InstallerActivity.9 */
    class C01769 implements OnClickListener {

        /* renamed from: th.dtv.activity.InstallerActivity.9.1 */
        class C01741 implements OnKeyListener {
            C01741() {
            }

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 1 || (keyCode != 4 && keyCode != 82)) {
                    return false;
                }
                InstallerActivity.this.dlg.dismiss();
                return true;
            }
        }

        /* renamed from: th.dtv.activity.InstallerActivity.9.2 */
        class C01752 implements PswdCB {
            C01752() {
            }

            public void DoCallBackEvent() {
                InstallerActivity.this.FactoryRestoreConfrim();
            }
        }

        C01769() {
        }

        public void onClick(View view) {
//            InstallerActivity.this.dlg = new PasswordDialog(InstallerActivity.this, R.style.MyDialog, new C01741(), new C01752());
//            InstallerActivity.this.dlg.show();
//            ((PasswordDialog) InstallerActivity.this.dlg).setHandleUpDown(true);
        }
    }

    public class ConfirmCustomDialog extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        TextView mInfoTextView;
        TextView mTitleTextView;
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.factory_restore_confirm_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitleTextView = (TextView) findViewById(R.id.title_textview);
            this.mInfoTextView = (TextView) findViewById(R.id.info_textview);
            this.mTitleTextView.setText(R.string.dvb_data_reset);
            this.mInfoTextView.setText(R.string.confirm_dvb_data_reset);
            this.mBtnCancel.requestFocus();
        }

        public ConfirmCustomDialog(Context context, int theme) {
            super(context, theme);
            this.mBtnStart = null;
            this.mBtnCancel = null;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class StorageDeviceDialog extends Dialog {
        List<String> items;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        private int mInorOut;
        View mLayoutView;
        private CustomListView mListView;
        private int temp_no;

        /* renamed from: th.dtv.activity.InstallerActivity.StorageDeviceDialog.1 */
        class C01801 implements OnItemSelectedListener {
            C01801() {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                StorageDeviceDialog.this.temp_no = position;
                StorageDeviceDialog.this.mListView.setItemChecked(position, true);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        }

        private void refreshStorageDevice() {
            this.items.clear();
            if (InstallerActivity.this.storageDeviceInfo == null) {
                dismiss();
            } else if (InstallerActivity.this.storageDeviceInfo.getDeviceCount() > 0) {
                for (int i = 0; i < InstallerActivity.this.storageDeviceInfo.getDeviceCount(); i++) {
                    DeviceItem item = InstallerActivity.this.storageDeviceInfo.getDeviceItem(i);
                    if (item.VolumeName != null) {
                        if (item.VolumeName.indexOf("[udisk") == 1) {
                            this.items.add(" " + InstallerActivity.this.getResources().getString(R.string.udisk) + " " + ((InstallerActivity.this.storageDeviceInfo.getDeviceCount() - 1) - i));
                        } else {
                            this.items.add(item.VolumeName + "");
                        }
                    }
                }
                this.mAdapter.notifyDataSetChanged();
            } else {
                dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.storagedevice_sel_dialog);
            ((TextView) findViewById(R.id.TextView_Title)).setText(InstallerActivity.this.getResources().getString(R.string.SelStorageDevice));
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            this.items = new ArrayList();
            this.items.clear();
            for (int i = 0; i < InstallerActivity.this.storageDeviceInfo.getDeviceCount(); i++) {
                DeviceItem item = InstallerActivity.this.storageDeviceInfo.getDeviceItem(i);
                if (item.VolumeName != null) {
                    if (item.VolumeName.indexOf("[udisk") == 1) {
                        this.items.add(" " + InstallerActivity.this.getResources().getString(R.string.udisk) + " " + ((InstallerActivity.this.storageDeviceInfo.getDeviceCount() - 1) - i));
                    } else {
                        this.items.add(item.VolumeName + "");
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(InstallerActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, this.items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setItemChecked(0, true);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemSelectedListener(new C01801());
        }

        protected void onStop() {
            super.onStop();
        }

        public StorageDeviceDialog(Context context, int theme, int InOrOut) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.temp_no = 0;
            this.mInorOut = 1;
            this.mContext = context;
            this.mInorOut = InOrOut;
        }
    }

    private class StorageDevicePlugReceiver extends BroadcastReceiver {
        private StorageDevicePlugReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.intent.action.DEVICE_MOUNTED")) {
                SETTINGS.makeText(InstallerActivity.this, R.string.insert, 0);
//                Log.e("InstallerActivity", "StorageDevice plug in :  " + intent.getExtra("device_path"));
                if (InstallerActivity.this.DeviceInfoDia != null) {
                    InstallerActivity.this.DeviceInfoDia.refreshStorageDevice();
                }
                InstallerActivity.this.RefreshDeviceView();
            } else if (action.equals("android.intent.action.DEVICE_REMOVED")) {
                SETTINGS.makeText(InstallerActivity.this, R.string.out, 0);
//                Log.e("InstallerActivity", "StorageDevice plug out :  " + intent.getExtra("device_path"));
                if (InstallerActivity.this.DeviceInfoDia != null) {
                    InstallerActivity.this.DeviceInfoDia.refreshStorageDevice();
                }
                InstallerActivity.this.RefreshDeviceView();
            }
        }
    }

    public InstallerActivity() {
        this.System_type = 0;
        this.areaInfo = new dvb_t_area();
        this.currentAreaIndex = 0;
        this.dlg = null;
        this.passwd = 0;
        this.storageDeviceCount = 0;
        this.mTipsReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d("InstallerActivity", "mBookReceiver: " + action);
                if (action.equals("data.factory.success")) {
                    InstallerActivity.this.showSuccessTips();
                }
            }
        };
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (i) {
                case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                case MW.RET_TP_EXIST /*10*/:
                case MW.RET_INVALID_SAT /*11*/:
                case MW.RET_INVALID_AREA /*12*/:
                case MW.RET_INVALID_TP /*13*/:
                case MW.RET_BOOK_EVENT_INVALID /*14*/:
                case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                    this.passwd = ((this.passwd * 10) + i) - 7;
                    if (this.passwd == 111111) {
                        Intent intent = new Intent();
                        intent.setFlags(67108864);
                        intent.setClass(this, SmartManagerActivity.class);
                        startActivity(intent);
                        return true;
                    }
                    break;
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.sat_btn.hasFocus()) {
                        this.factory_btn.requestFocus();
                        return true;
                    } else if (this.autosearch_btn.hasFocus()) {
                        this.factory_btn.requestFocus();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.factory_btn.hasFocus() && this.Ant_ll.getVisibility() != 8) {
                        this.sat_btn.requestFocus();
                        return true;
                    } else if (this.factory_btn.hasFocus() && this.autosearch_btn.getVisibility() != 8) {
                        this.autosearch_btn.requestFocus();
                        return true;
                    }
                    break;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.install_activity);
        initView();
    }

    private void initListener() {
        if (this.System_type == 0 || this.System_type == 2) {
            this.ant_btn.setOnClickListener(new C01661());
            this.sat_btn.setOnClickListener(new C01672());
            this.tp_btn.setOnClickListener(new C01683());
            this.motor_btn.setOnClickListener(new C01694());
            this.multiscan_btn.setOnClickListener(new C01705());
            this.ant2_btn.setOnClickListener(new C01716());
        } else {
            InitAnt_Power();
            InitCountry();
            this.autosearch_btn.setOnClickListener(new C01727());
            this.manualsearch_btn.setOnClickListener(new C01738());
        }
        this.factory_btn.setOnClickListener(new C01769());
        this.export_btn.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                InstallerActivity.this.ShowStorageDeviceDialog(1);
            }
        });
        this.import_btn.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                InstallerActivity.this.ShowStorageDeviceDialog(0);
            }
        });
    }

    private void InitCountry() {
    }

    private void InitAnt_Power() {
    }

    public void FactoryRestoreConfrim() {
        Dialog confirmDialog = new ConfirmCustomDialog(this, R.style.MyDialog);
        confirmDialog.show();
        LayoutParams lp = confirmDialog.getWindow().getAttributes();
        lp.x = 0;
        lp.y = -25;
        confirmDialog.getWindow().setAttributes(lp);
        confirmDialog.getWindow().addFlags(2);
    }

    private void SendSuccessBroadCast() {
        sendBroadcast(new Intent("data.factory.success"));
    }

    private void registerBroadCastReceiver() {
        IntentFilter dialog = new IntentFilter();
        dialog.addAction("data.factory.success");
        registerReceiver(this.mTipsReceiver, dialog);
    }

    private void unregisterBroadCastReceiver() {
        unregisterReceiver(this.mTipsReceiver);
    }

    private void showSuccessTips() {
        SETTINGS.makeText(this, R.string.restore_factory_success, 0);
    }

    private void initView() {
        this.ant_btn = (Button) findViewById(R.id.ant_btn);
        this.sat_btn = (Button) findViewById(R.id.sat_btn);
        this.tp_btn = (Button) findViewById(R.id.tp_btn);
        this.motor_btn = (Button) findViewById(R.id.motor_btn);
        this.factory_btn = (Button) findViewById(R.id.factory_btn);
        this.autosearch_btn = (Button) findViewById(R.id.autosearch_btn);
        this.manualsearch_btn = (Button) findViewById(R.id.manualsearch_btn);
        this.multiscan_btn = (Button) findViewById(R.id.multiscan_btn);
        this.Ant_ll = (LinearLayout) findViewById(R.id.Ant_ll);
        this.Sat_ll = (LinearLayout) findViewById(R.id.Sat_ll);
        this.tp_ll = (LinearLayout) findViewById(R.id.tp_ll);
        this.motor_ll = (LinearLayout) findViewById(R.id.motor_ll);
        this.MultiScan_ll = (LinearLayout) findViewById(R.id.MultiScan_ll);
        this.AutoSearch_ll = (LinearLayout) findViewById(R.id.AutoSearch_ll);
        this.ManualSearch_ll = (LinearLayout) findViewById(R.id.ManualSearch_ll);
        this.export_btn = (Button) findViewById(R.id.export_btn);
        this.import_btn = (Button) findViewById(R.id.import_btn);
        this.import_ll = (LinearLayout) findViewById(R.id.import_ll);
        this.export_ll = (LinearLayout) findViewById(R.id.export_ll);
        this.ant2_btn = (Button) findViewById(R.id.antenna_setup2_btn);
        this.Ant2_ll = (LinearLayout) findViewById(R.id.antenna_setup2_ll);
    }

    private void ShowDVB_S_View() {
        this.Ant_ll.setVisibility(View.VISIBLE);
        this.Sat_ll.setVisibility(View.VISIBLE);
        this.tp_ll.setVisibility(View.VISIBLE);
        this.motor_ll.setVisibility(View.VISIBLE);
        this.MultiScan_ll.setVisibility(View.VISIBLE);
        this.AutoSearch_ll.setVisibility(View.GONE);
        this.ManualSearch_ll.setVisibility(View.GONE);
        if (this.System_type == 2) {
            this.Ant2_ll.setVisibility(View.VISIBLE);
        } else {
            this.Ant2_ll.setVisibility(View.GONE);
        }
    }

    private void ShowDVB_T_View() {
        this.Ant_ll.setVisibility(View.GONE);
        this.Ant2_ll.setVisibility(View.GONE);
        this.Sat_ll.setVisibility(View.GONE);
        this.tp_ll.setVisibility(View.GONE);
        this.motor_ll.setVisibility(View.GONE);
        this.MultiScan_ll.setVisibility(View.GONE);
        this.AutoSearch_ll.setVisibility(View.VISIBLE);
        this.ManualSearch_ll.setVisibility(View.VISIBLE);
    }

    protected void onResume() {
        this.passwd = 0;
        SETTINGS.send_led_msg("NENU");
        SETTINGS.set_green_led(false);
        this.System_type = MW.get_system_tuner_config();
        if (this.System_type == 0 || this.System_type == 2) {
            ShowDVB_S_View();
        } else {
            ShowDVB_T_View();
        }
        initListener();
        if (this.storageDeviceInfo == null) {
            this.storageDeviceInfo = new StorageDevice(this);
        }
        this.storageDevicePlugReceiver = new StorageDevicePlugReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.DEVICE_MOUNTED");
        filter.addAction("android.intent.action.DEVICE_REMOVED");
        registerReceiver(this.storageDevicePlugReceiver, filter);
        RefreshDeviceView();
        registerBroadCastReceiver();
        super.onResume();
    }

    protected void onPause() {
        if (this.dlg != null && this.dlg.isShowing()) {
            this.dlg.dismiss();
        }
        if (this.storageDevicePlugReceiver != null) {
            unregisterReceiver(this.storageDevicePlugReceiver);
            this.storageDevicePlugReceiver = null;
        }
        unregisterBroadCastReceiver();
        if (this.DeviceInfoDia != null && this.DeviceInfoDia.isShowing()) {
            this.DeviceInfoDia.dismiss();
        }
        super.onPause();
    }

    protected void onDestroy() {
        this.storageDeviceInfo.finish(this);
        super.onDestroy();
    }

    private void RefreshDeviceView() {
        if (this.storageDeviceInfo != null) {
            this.storageDeviceCount = this.storageDeviceInfo.getDeviceCount();
            if (this.storageDeviceCount > 0) {
                this.import_btn.setFocusable(true);
                this.import_btn.setBackgroundResource(R.drawable.live_channel_list_item_bg);
                this.export_btn.setFocusable(true);
                this.export_btn.setBackgroundResource(R.drawable.live_channel_list_item_bg);
                return;
            }
            this.import_btn.setFocusable(false);
            this.import_btn.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
            this.export_btn.setFocusable(false);
            this.export_btn.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        }
    }

    private void ShowStorageDeviceDialog(int inOrOut) {
        if (this.storageDeviceInfo != null && this.storageDeviceInfo.getDeviceCount() > 0) {
            Log.e("InstallerActivity", "have device : " + this.storageDeviceInfo.getDeviceCount());
            if (this.storageDeviceInfo.getDeviceCount() > 1) {
                this.DeviceInfoDia = new StorageDeviceDialog(this, R.style.MyDialog, inOrOut);
                this.DeviceInfoDia.show();
                LayoutParams lp = this.DeviceInfoDia.getWindow().getAttributes();
                lp.dimAmount = 0.0f;
                this.DeviceInfoDia.getWindow().setAttributes(lp);
                this.DeviceInfoDia.getWindow().addFlags(2);
                return;
            }
            String Path = this.storageDeviceInfo.getDeviceItem(0).Path;
            System.out.println(Path);
            switch (inOrOut) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    DoImportDB(Path);
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    DoExportDB(Path);
                default:
            }
        }
    }

    private void DoImportDB(String Path) {
        int Ret = MW.db_import_user_data(Path);
        if (Ret == 0) {
            SETTINGS.makeText(this, R.string.RestoreUserDataSuccess, 0);
            SETTINGS.init(getApplicationContext(), true);
        } else if (Ret == 45) {
            SETTINGS.makeText(this, R.string.NoUserData, 0);
        } else {
            SETTINGS.makeText(this, R.string.RestoreUserDataFailed, 0);
        }
    }

    private void DoExportDB(String Path) {
        if (MW.db_export_user_data(Path) == 0) {
            SETTINGS.makeText(this, R.string.BackupUserDataSuccess, 0);
        } else {
            SETTINGS.makeText(this, R.string.BackupUserDataFailed, 0);
        }
    }
}
