package th.dtv.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import th.dtv.ComboLayout;
import th.dtv.CustomListView;
import th.dtv.DigitsEditText;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.StorageDevice;
import th.dtv.StorageDevice.DeviceItem;
import th.dtv.util.SmartCfgDataHelper;
import th.dtv.util.SmartDataInfo;

public class TestActivity extends DtvBaseActivity {
    private static SmartCfgDataHelper helper;
    private Thread CheckTask;
    StorageDeviceDialog DeviceInfoDia;
    ArrayList<Boolean> ItemSelStatus;
    ArrayList<Integer> SelServerItem;
    private BroadcastReceiver UpdateStatusReceiver;
    private boolean bCheck;
    private boolean bRun;
    Dialog dialog;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    private TextView select_txt;
    private ServerInfoAdapter serverInfoAdapter;
    private int server_item_pos;
    private CustomListView server_listview;
    private int storageDeviceCount;
    private StorageDevice storageDeviceInfo;

    /* renamed from: th.dtv.activity.TestActivity.1 */
    class C02491 extends BroadcastReceiver {
        C02491() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("TestActivity", "UpdateStatusReceiver: " + action);
            if (action.equals("REFRESH_CONNECT_STATUS")) {
                TestActivity.this.serverInfoAdapter.notifyDataSetChanged();
            }
        }
    }

    /* renamed from: th.dtv.activity.TestActivity.2 */
    class C02502 implements OnScrollListener {
        C02502() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TestActivity.this.mFirstVisibleItem = firstVisibleItem;
            TestActivity.this.mVisibleItemCount = visibleItemCount;
            System.out.println("mVisibleItemCount" + TestActivity.this.mVisibleItemCount);
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.TestActivity.3 */
    class C02513 implements OnItemSelectedListener {
        C02513() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            TestActivity.this.server_item_pos = position;
            if (TestActivity.this.ItemSelStatus == null) {
                return;
            }
            if (((Boolean) TestActivity.this.ItemSelStatus.get(TestActivity.this.server_item_pos)).booleanValue()) {
                TestActivity.this.ShowUnSelectTipsView();
            } else {
                TestActivity.this.ShowSelectTipsView();
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.TestActivity.4 */
    class C02524 implements OnKeyListener {
        C02524() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    TestActivity.this.RefreshSatSelectImage();
                    break;
                case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                    TestActivity.this.EditServer();
                    return true;
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    TestActivity.this.DeleteServer();
                    return true;
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    TestActivity.this.AddServer();
                    return true;
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.TestActivity.5 */
    class C02535 implements Runnable {
        C02535() {
        }

        public void run() {
            while (TestActivity.this.bRun) {
                if (TestActivity.this.bCheck && TestActivity.this.UpdateDbData()) {
                    TestActivity.this.sendBroadcast(new Intent("REFRESH_CONNECT_STATUS"));
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class AddServerDialog extends Dialog {
        private ComboLayout Enable;
        private ComboLayout Protocol;
        private EditText key;
        private EditText label_name;
        private Button mBtnCancel;
        private Button mBtnStart;
        private EditText passwd;
        private EditText server_name;
        private DigitsEditText server_port;
        private SmartDataInfo smartDataInfo;
        int temp_Enabled_position;
        int temp_Protocol_position;
        private TextView title;
        private EditText user_name;

        /* renamed from: th.dtv.activity.TestActivity.AddServerDialog.1 */
        class C02541 implements OnItemClickListener {
            C02541() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                AddServerDialog.this.temp_Protocol_position = position;
                if (position == 0) {
                    AddServerDialog.this.key.setEnabled(false);
                } else {
                    AddServerDialog.this.key.setEnabled(true);
                }
            }
        }

        /* renamed from: th.dtv.activity.TestActivity.AddServerDialog.2 */
        class C02552 implements OnItemClickListener {
            C02552() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                AddServerDialog.this.temp_Enabled_position = position;
            }
        }


        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.edit_server_list);
            this.title = (TextView) findViewById(R.id.title);
            this.label_name = (EditText) findViewById(R.id.label_name);
            this.Protocol = (ComboLayout) findViewById(R.id.Protocol);
            this.server_name = (EditText) findViewById(R.id.server_name);
            this.server_port = (DigitsEditText) findViewById(R.id.server_port);
            this.user_name = (EditText) findViewById(R.id.user_name);
            this.passwd = (EditText) findViewById(R.id.passwd);
            this.Enable = (ComboLayout) findViewById(R.id.Enable);
            this.key = (EditText) findViewById(R.id.key);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.title.setText(TestActivity.this.getResources().getString(R.string.add));
            this.smartDataInfo = new SmartDataInfo();
            this.smartDataInfo.index = TestActivity.helper.getCount();
            this.smartDataInfo.label = TestActivity.this.getResources().getString(R.string.CCcamd) + (this.smartDataInfo.index + 1);
            this.smartDataInfo.servername = "";
            this.smartDataInfo.serverport = 0;
            this.smartDataInfo.username = "";
            this.smartDataInfo.passwd = "";
            this.smartDataInfo.enabled = 1;
            this.smartDataInfo.key = "";
            if (this.smartDataInfo != null) {
                this.label_name.setText(this.smartDataInfo.label);
                this.server_name.setText(this.smartDataInfo.servername);
                this.server_port.setText(Integer.toString(this.smartDataInfo.serverport));
                this.user_name.setText(this.smartDataInfo.username);
                this.passwd.setText(this.smartDataInfo.passwd);
                this.temp_Protocol_position = this.smartDataInfo.protocol;
                ArrayList items = new ArrayList();
                items.clear();
                items.add(TestActivity.this.getResources().getString(R.string.CCcamd));
                items.add(TestActivity.this.getResources().getString(R.string.Newcamd));
                this.Protocol.initView((int) R.string.Protocol, items, this.smartDataInfo.protocol, new C02541());
                this.Protocol.getTitleTextView().setLayoutParams(new LayoutParams(210, -2));
                this.Protocol.getInfoTextView().setLayoutParams(new LayoutParams(160, -2));
                this.temp_Enabled_position = this.smartDataInfo.enabled;
                ArrayList items1 = new ArrayList();
                items1.clear();
                items1.add(TestActivity.this.getResources().getString(R.string.Off));
                items1.add(TestActivity.this.getResources().getString(R.string.On));
                this.Enable.initView((int) R.string.Enabled, items1, this.smartDataInfo.enabled, new C02552());
                this.Enable.getTitleTextView().setLayoutParams(new LayoutParams(210, -2));
                this.Enable.getInfoTextView().setLayoutParams(new LayoutParams(160, -2));
                if (this.smartDataInfo.protocol == 0) {
                    this.key.setEnabled(false);
                } else {
                    this.key.setEnabled(true);
                }
                this.key.setText(this.smartDataInfo.key);
            }
            this.Protocol.requestFocus();
            this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.Enable.isFocused()) {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    } else {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                    }
                    if (!this.key.isFocused()) {
                        this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.mBtnCancel.isFocused() || this.mBtnStart.isFocused()) {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    } else {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                    }
                    if (!this.Protocol.isFocused()) {
                        this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (!(this.mBtnStart.isFocused() || this.mBtnCancel.isFocused())) {
                        return true;
                    }
            }
            return super.onKeyDown(keyCode, event);
        }

        public AddServerDialog(Context context, int theme) {
            super(context, theme);
            this.title = null;
            this.label_name = null;
            this.Protocol = null;
            this.server_name = null;
            this.server_port = null;
            this.user_name = null;
            this.passwd = null;
            this.smartDataInfo = null;
            this.Enable = null;
            this.key = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.temp_Protocol_position = 0;
            this.temp_Enabled_position = 0;
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class EditServerDialog extends Dialog {
        private ComboLayout Enable;
        private ComboLayout Protocol;
        private EditText key;
        private EditText label_name;
        private Button mBtnCancel;
        private Button mBtnStart;
        private EditText passwd;
        private EditText server_name;
        private DigitsEditText server_port;
        private SmartDataInfo smartDataInfo;
        int temp_Enabled_position;
        int temp_Protocol_position;
        private EditText user_name;

        /* renamed from: th.dtv.activity.TestActivity.EditServerDialog.1 */
        class C02601 implements OnItemClickListener {
            C02601() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                EditServerDialog.this.temp_Protocol_position = position;
                if (position == 0) {
                    EditServerDialog.this.key.setEnabled(false);
                } else {
                    EditServerDialog.this.key.setEnabled(true);
                }
            }
        }

        /* renamed from: th.dtv.activity.TestActivity.EditServerDialog.2 */
        class C02612 implements OnItemClickListener {
            C02612() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                EditServerDialog.this.temp_Enabled_position = position;
            }
        }
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.edit_server_list);
            this.label_name = (EditText) findViewById(R.id.label_name);
            this.Protocol = (ComboLayout) findViewById(R.id.Protocol);
            this.server_name = (EditText) findViewById(R.id.server_name);
            this.server_port = (DigitsEditText) findViewById(R.id.server_port);
            this.user_name = (EditText) findViewById(R.id.user_name);
            this.passwd = (EditText) findViewById(R.id.passwd);
            this.Enable = (ComboLayout) findViewById(R.id.Enable);
            this.key = (EditText) findViewById(R.id.key);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.smartDataInfo = TestActivity.helper.getSmartDataInfoByIndex(TestActivity.this.server_item_pos);
            if (this.smartDataInfo != null) {
                this.label_name.setText(this.smartDataInfo.label);
                this.server_name.setText(this.smartDataInfo.servername);
                this.server_port.setText(Integer.toString(this.smartDataInfo.serverport));
                this.user_name.setText(this.smartDataInfo.username);
                this.passwd.setText(this.smartDataInfo.passwd);
                this.temp_Protocol_position = this.smartDataInfo.protocol;
                ArrayList items = new ArrayList();
                items.clear();
                items.add(TestActivity.this.getResources().getString(R.string.CCcamd));
                items.add(TestActivity.this.getResources().getString(R.string.Newcamd));
                this.Protocol.initView((int) R.string.Protocol, items, this.smartDataInfo.protocol, new C02601());
                this.Protocol.getTitleTextView().setLayoutParams(new LayoutParams(210, -2));
                this.Protocol.getInfoTextView().setLayoutParams(new LayoutParams(160, -2));
                this.temp_Enabled_position = this.smartDataInfo.enabled;
                ArrayList items1 = new ArrayList();
                items1.clear();
                items1.add(TestActivity.this.getResources().getString(R.string.Off));
                items1.add(TestActivity.this.getResources().getString(R.string.On));
                this.Enable.initView((int) R.string.Enabled, items1, this.smartDataInfo.enabled, new C02612());
                this.Enable.getTitleTextView().setLayoutParams(new LayoutParams(210, -2));
                this.Enable.getInfoTextView().setLayoutParams(new LayoutParams(160, -2));
                if (this.smartDataInfo.protocol == 0) {
                    this.key.setEnabled(false);
                } else {
                    this.key.setEnabled(true);
                }
                this.key.setText(this.smartDataInfo.key);
            }
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.Enable.isFocused()) {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    } else {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                    }
                    if (!this.key.isFocused()) {
                        this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.mBtnCancel.isFocused() || this.mBtnStart.isFocused()) {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    } else {
                        this.Protocol.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                    }
                    if (!this.Protocol.isFocused()) {
                        this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.Enable.getInfoTextView().setTextColor(TestActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (!(this.mBtnStart.isFocused() || this.mBtnCancel.isFocused())) {
                        return true;
                    }
            }
            return super.onKeyDown(keyCode, event);
        }

        public EditServerDialog(Context context, int theme) {
            super(context, theme);
            this.label_name = null;
            this.Protocol = null;
            this.server_name = null;
            this.server_port = null;
            this.user_name = null;
            this.passwd = null;
            this.smartDataInfo = null;
            this.Enable = null;
            this.key = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.temp_Protocol_position = 0;
            this.temp_Enabled_position = 0;
        }
    }

    private class ServerInfoAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mcontext;
        private SmartDataInfo smartDataInfo;

        class ViewHolder {
            TextView server_label;
            TextView server_no;
            ImageView server_select_img;
            TextView server_status;

            ViewHolder() {
            }
        }

        public ServerInfoAdapter(Context context) {
            this.smartDataInfo = null;
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return TestActivity.helper.getCount();
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.server_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.server_no = (TextView) convertView.findViewById(R.id.server_no);
                localViewHolder.server_select_img = (ImageView) convertView.findViewById(R.id.server_select_img);
                localViewHolder.server_label = (TextView) convertView.findViewById(R.id.server_label);
                localViewHolder.server_status = (TextView) convertView.findViewById(R.id.server_status);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            this.smartDataInfo = TestActivity.helper.getSmartDataInfoByIndex(pos);
            if (this.smartDataInfo != null) {
                localViewHolder.server_no.setText((pos + 1) + "");
                localViewHolder.server_label.setText(this.smartDataInfo.label);
                if (this.smartDataInfo.enabled <= 0) {
                    localViewHolder.server_status.setText(R.string.Disabled);
                } else if (this.smartDataInfo.connectStatus == 0) {
                    localViewHolder.server_status.setText(R.string.unconnected);
                } else {
                    localViewHolder.server_status.setText(R.string.connected);
                }
            } else {
                Log.e("LEE", "smartDataInfo is null");
            }
            if (TestActivity.this.ItemSelStatus != null) {
                if (((Boolean) TestActivity.this.ItemSelStatus.get(pos)).booleanValue()) {
                    localViewHolder.server_select_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.server_select_img.setVisibility(View.INVISIBLE);
                }
            }
            return convertView;
        }
    }

    public class StorageDeviceDialog extends Dialog {
        List<String> items;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        private int mInorOut;
        View mLayoutView;
        private CustomListView mListView;
        private int temp_no;

        /* renamed from: th.dtv.activity.TestActivity.StorageDeviceDialog.1 */
        class C02641 implements OnItemSelectedListener {
            C02641() {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Log.e("TestActivity", "mListView position =" + position);
                StorageDeviceDialog.this.temp_no = position;
                StorageDeviceDialog.this.mListView.setItemChecked(position, true);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        }

        /* renamed from: th.dtv.activity.TestActivity.StorageDeviceDialog.2 */
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.storagedevice_sel_dialog);
            ((TextView) findViewById(R.id.TextView_Title)).setText(TestActivity.this.getResources().getString(R.string.SelStorageDevice));
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            this.items = new ArrayList();
            this.items.clear();
            for (int i = 0; i < TestActivity.this.storageDeviceInfo.getDeviceCount(); i++) {
                DeviceItem item = TestActivity.this.storageDeviceInfo.getDeviceItem(i);
                if (item.VolumeName != null) {
                    if (item.VolumeName.indexOf("[udisk") == 1) {
                        this.items.add(" " + TestActivity.this.getResources().getString(R.string.udisk) + " " + ((TestActivity.this.storageDeviceInfo.getDeviceCount() - 1) - i));
                    } else {
                        this.items.add(item.VolumeName + "");
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(TestActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, this.items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setItemChecked(0, true);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemSelectedListener(new C02641());
        }

        protected void onStop() {
            super.onStop();
        }

        public StorageDeviceDialog(Context context, int theme, int InOrOut) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.temp_no = 0;
            this.mInorOut = 1;
            this.mContext = context;
            this.mInorOut = InOrOut;
        }
    }

    public static native boolean ns_check_status();

    public static native int ns_get_connect_status(int i);

    public static native boolean ns_set_info(SmartDataInfo[] smartDataInfoArr);

    public TestActivity() {
        this.server_listview = null;
        this.serverInfoAdapter = null;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.server_item_pos = 0;
        this.ItemSelStatus = null;
        this.storageDeviceCount = 0;
        this.UpdateStatusReceiver = new C02491();
        this.dialog = null;
        this.SelServerItem = new ArrayList();
        this.CheckTask = null;
        this.bCheck = true;
        this.bRun = true;
    }

    static {
        helper = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        initData();
        initView();
        initTipView();
        if (!ns_check_status()) {
            ((TextView) findViewById(R.id.type_details_type)).setText(getResources().getString(R.string.smartCard) + " &");
        }
    }

    protected void onResume() {
        super.onResume();
        IntentFilter showdialog = new IntentFilter();
        showdialog.addAction("REFRESH_CONNECT_STATUS");
        registerReceiver(this.UpdateStatusReceiver, showdialog);
        if (this.storageDeviceInfo == null) {
            this.storageDeviceInfo = new StorageDevice(this);
        }
        CheckStatusTask_Start();
    }

    protected void onPause() {
        super.onPause();
        CheckStatusTask_Stop();
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        if (this.UpdateStatusReceiver != null) {
            unregisterReceiver(this.UpdateStatusReceiver);
            this.UpdateStatusReceiver = null;
        }
        this.storageDeviceInfo.finish(this);
        if (this.DeviceInfoDia != null && this.DeviceInfoDia.isShowing()) {
            this.DeviceInfoDia.dismiss();
        }
        helper.saveAllSmartDatatoCfgFile();
    }

    private void initTipView() {
        this.select_txt = (TextView) findViewById(R.id.select_txt);
    }

    private void ShowSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.SelectSat));
    }

    private void ShowUnSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.UnSelectSat));
    }

    private void RefreshSatSelectImage() {
        if (this.ItemSelStatus != null) {
            if (((Boolean) this.ItemSelStatus.get(this.server_item_pos)).booleanValue()) {
                this.ItemSelStatus.set(this.server_item_pos, Boolean.valueOf(false));
                ShowSelectTipsView();
            } else {
                this.ItemSelStatus.set(this.server_item_pos, Boolean.valueOf(true));
                ShowUnSelectTipsView();
            }
        }
        this.serverInfoAdapter.notifyDataSetChanged();
    }

    private void initData() {
        SmartDataInfo smartDataInfo = new SmartDataInfo();
        helper = SmartCfgDataHelper.getInstance(this);
        initItemSelStatus();
    }

    private void initItemSelStatus() {
        int count = helper.getCount();
        if (count > 0) {
            this.ItemSelStatus = new ArrayList();
            for (int i = 0; i < count; i++) {
                this.ItemSelStatus.add(i, Boolean.valueOf(false));
            }
            return;
        }
        this.ItemSelStatus = null;
    }

    private void initView() {
        this.server_listview = (CustomListView) findViewById(R.id.server_listview);
        this.serverInfoAdapter = new ServerInfoAdapter(this);
        this.server_listview.setAdapter(this.serverInfoAdapter);
        this.server_listview.setVisibleItemCount(11);
        this.server_listview.setCustomScrollListener(new C02502());
        this.server_listview.setOnItemSelectedListener(new C02513());
        this.server_listview.setCustomKeyListener(new C02524());
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != 0) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    return true;
                default:
                    break;
            }
        }
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                if (helper == null) {
                    return true;
                }
                helper.saveAllSmartDatatoCfgFile();
                ShowStorageDeviceDialog(1);
                return true;
            case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                if (helper == null) {
                    return true;
                }
                ShowStorageDeviceDialog(0);
                return true;
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                AddServer();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void DeleteServer() {
        this.dialog = new DeleteDia(this, R.style.MyDialog);
        this.dialog.show();
        this.dialog.getWindow().addFlags(2);
    }

    private void EditServer() {
        this.dialog = new EditServerDialog(this, R.style.MyDialog);
        this.dialog.show();
        this.dialog.getWindow().addFlags(2);
    }

    private void AddServer() {
        this.dialog = new AddServerDialog(this, R.style.MyDialog);
        this.dialog.show();
        this.dialog.getWindow().addFlags(2);
    }

    private void getSelSatData() {
        this.SelServerItem.clear();
        int i = 0;
        while (i < helper.getCount()) {
            if (this.ItemSelStatus != null && ((Boolean) this.ItemSelStatus.get(i)).booleanValue()) {
                this.SelServerItem.add(Integer.valueOf(i));
                System.out.println("i  ::::: " + i);
            }
            i++;
        }
        if (this.SelServerItem.size() == 0 && this.server_listview != null && this.server_listview.getCount() != -1) {
            this.SelServerItem.add(Integer.valueOf(this.server_item_pos));
        }
    }

    private boolean CheckKey(String key) {
        if (key == null) {
            return false;
        }
        char[] keyArray = key.toLowerCase().toCharArray();
        int i = 0;
        while (i < keyArray.length) {
            if ((keyArray[i] < '0' || keyArray[i] > '9') && (keyArray[i] < 'a' || keyArray[i] > 'f')) {
                return false;
            }
            i++;
        }
        return true;
    }

    private void CheckStatusTask_Start() {
        this.CheckTask = new Thread(new C02535());
        this.CheckTask.start();
    }

    private void CheckStatusTask_Stop() {
        this.bRun = false;
    }

    public static void DoFunction(Context context) {
        helper = SmartCfgDataHelper.getInstance(context);
        if (helper != null) {
            SmartDataInfo[] nsInfoList = null;
            if (helper.getCount() > 0) {
                nsInfoList = helper.getAllSamrtDataInfo();
            }
            ns_set_info(nsInfoList);
            return;
        }
        ns_set_info(null);
    }

    private boolean UpdateDbData() {
        boolean bRet = false;
        if (helper != null) {
            int nsCount = helper.getCount();
            for (int i = 0; i < nsCount; i++) {
                SmartDataInfo smartDataInfo = helper.getSmartDataInfoByIndex(i);
                if (smartDataInfo != null) {
                    int connectStatus = ns_get_connect_status(i);
                    if (connectStatus != smartDataInfo.connectStatus) {
                        smartDataInfo.connectStatus = connectStatus;
                        helper.update(smartDataInfo);
                        bRet = true;
                    }
                }
            }
        }
        return bRet;
    }

    public boolean importSmartDB(String fromDbPath, String toDbPath) throws IOException, FileNotFoundException {
        return SmartCfgDataHelper.getInstance(this).importDB(fromDbPath);
    }

    private void DoImportDB(String Path) {
        try {
            StringBuilder append = new StringBuilder().append(Path).append("/");
            SmartCfgDataHelper smartCfgDataHelper = helper;
            if (new File(append.append(SmartCfgDataHelper.mSmartCfgFile).toString()).exists()) {
                SmartCfgDataHelper smartCfgDataHelper2 = helper;
                StringBuilder append2 = new StringBuilder().append(Path).append("/");
                SmartCfgDataHelper smartCfgDataHelper3 = helper;
                if (!smartCfgDataHelper2.checkCfgFile(append2.append(SmartCfgDataHelper.mSmartCfgFile).toString())) {
                    SETTINGS.makeText(this, R.string.UnsupporttedFile, 0);
                } else if (importSmartDB(Path + "/", "data/data/th.dtv/")) {
                    SETTINGS.makeText(this, R.string.RestoreDataSuccess, 0);
                } else {
                    SETTINGS.makeText(this, R.string.FileNoFound, 0);
                }
                helper.initSmartData();
                DoFunction(this);
                initItemSelStatus();
                if (this.serverInfoAdapter != null) {
                    this.serverInfoAdapter.notifyDataSetChanged();
                    return;
                }
                return;
            }
            SETTINGS.makeText(this, R.string.FileNoFound, 0);
        } catch (FileNotFoundException e) {
            SETTINGS.makeText(this, R.string.FileNoFound, 0);
            e.printStackTrace();
        } catch (IOException e2) {
            SETTINGS.makeText(this, R.string.RestoreDataFailed, 0);
            e2.printStackTrace();
            initItemSelStatus();
            if (this.serverInfoAdapter != null) {
                this.serverInfoAdapter.notifyDataSetChanged();
            }
        }
    }

    public boolean exportSmartDB(String fromDbPath, String toDbPath) throws IOException, FileNotFoundException {
        return SmartCfgDataHelper.getInstance(this).exportDB(toDbPath);
    }

    private void DoExportDB(String Path) {
        if (SmartCfgDataHelper.getInstance(this).getCount() <= 0) {
            SETTINGS.makeText(this, R.string.NoData, 0);
            return;
        }
        try {
            if (exportSmartDB("data/data/th.dtv/", Path + "/")) {
                SETTINGS.makeText(this, R.string.BackupDataSuccess, 0);
            } else {
                SETTINGS.makeText(this, R.string.FileNoFound, 0);
            }
        } catch (FileNotFoundException e) {
            SETTINGS.makeText(this, R.string.FileNoFound, 0);
            e.printStackTrace();
        } catch (IOException e2) {
            SETTINGS.makeText(this, R.string.BackupDataFailed, 0);
            e2.printStackTrace();
        }
    }

    private void ShowStorageDeviceDialog(int inOrOut) {
        if (this.storageDeviceInfo == null) {
            return;
        }
        if (this.storageDeviceInfo.getDeviceCount() > 0) {
            Log.e("TestActivity", "have device : " + this.storageDeviceInfo.getDeviceCount());
            if (this.storageDeviceInfo.getDeviceCount() > 1) {
                this.DeviceInfoDia = new StorageDeviceDialog(this, R.style.MyDialog, inOrOut);
                this.DeviceInfoDia.show();
                WindowManager.LayoutParams lp = this.DeviceInfoDia.getWindow().getAttributes();
                lp.dimAmount = 0.0f;
                this.DeviceInfoDia.getWindow().setAttributes(lp);
                this.DeviceInfoDia.getWindow().addFlags(2);
                return;
            }
            String Path = this.storageDeviceInfo.getDeviceItem(0).Path;
            System.out.println(Path);
            switch (inOrOut) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    DoImportDB(Path);
                    return;
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    DoExportDB(Path);
                    return;
                default:
                    return;
            }
        }
        SETTINGS.makeText(this, R.string.no_storage, 0);
    }
}
