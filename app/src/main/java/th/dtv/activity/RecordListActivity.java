package th.dtv.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import java.util.ArrayList;
import java.util.List;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.PasswordDialog;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.StorageDevice;
import th.dtv.StorageDevice.DeviceItem;
import th.dtv.TeletextSubtitleView;
import th.dtv.mw_data.pvr_record;
import th.dtv.mw_data.ttx_info;
import th.dtv.mw_data.ttx_sub_page_info;

public class RecordListActivity extends DtvBaseActivity {
    boolean[][] PvrSelStatus;
    private RelativeLayout RL_RecordPlay;
    ArrayList<Integer> SelPvrList;
    private boolean bHandlePreNext;
    private boolean bRefresh;
    private boolean bTeletextShowed;
    private boolean bUpdate;
    private Dialog dia_currentDialog;
    Dialog dialog;
    private EditText et_ttxPageNumber;
    private int fbackforward;
    private boolean flag;
    private boolean hasLockedFile;
    private EditText hour;
    private boolean inSubtitleMode;
    private boolean inTeletextMode;
    private boolean initflag;
    private ImageView left_img;
    private LinearLayout ll_recordListUI;
    private Handler localMsgHandler;
    private int mRecordFirstVisibleItem;
    private int mRecordVisibleItemCount;
    Callback mSHCallback;
    private int mStorageFirstVisibleItem;
    private int mStorageVisibleItemCount;
    private int m_hour;
    private int m_minute;
    private int m_second;
    private EditText minute;
    private Handler mwMsgHandler;
    private TextView play_bottom_current_time;
    private TextView play_bottom_duration;
    private SeekBar play_bottom_seekbar;
    private TextView play_bottom_videoName;
    private TextView play_speed;
    private ImageView play_status_img;
    private int playback_status;
    private int[] pos;
    private Dialog pswdDig;
    pvr_record pvrRecord;
    private pvr_record pvrRecordInfo;
    private RecordListAdapter recordListAdapter;
    private int recordListCount;
    private CustomListView record_list;
    private int record_list_item_pos;
    private ImageView right_img;
    private RelativeLayout rl_loading;
    private EditText second;
    private int seektime;
    private int speed;
    private StorageAdapter storageAdapter;
    private int storageDeviceCount;
    private int storageDeviceIndex;
    private StorageDevice storageDeviceInfo;
    private StorageDevicePlugReceiver storageDevicePlugReceiver;
    private CustomListView storage_list;
    private int storage_list_item_pos;
    private int temp_hour;
    private int temp_minute;
    private int temp_second;
    private int temp_whichone;
    private TextView tips_txt;
    private int totaltime;
    private ttx_sub_page_info ttxSubPageInfo;
    private TeletextSubtitleView ttx_sub;
    private VideoView vView;

    /* renamed from: th.dtv.activity.RecordListActivity.1 */
    class C02121 implements TextWatcher {
        C02121() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                RecordListActivity.this.m_hour = Integer.parseInt(RecordListActivity.this.hour.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                RecordListActivity.this.m_hour = RecordListActivity.this.temp_hour;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.2 */
    class C02132 implements TextWatcher {
        C02132() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                RecordListActivity.this.m_minute = Integer.parseInt(RecordListActivity.this.minute.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                RecordListActivity.this.m_minute = RecordListActivity.this.temp_minute;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.3 */
    class C02143 implements TextWatcher {
        C02143() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                RecordListActivity.this.m_second = Integer.parseInt(RecordListActivity.this.second.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                RecordListActivity.this.m_second = RecordListActivity.this.temp_second;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.4 */
    class C02154 implements OnKeyListener {
        C02154() {
        }

        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                        if (RecordListActivity.this.playback_status == 7) {
                            RecordListActivity.this.play_status_img.setVisibility(View.INVISIBLE);
                            MW.pvr_playback_resume();
                            RecordListActivity.this.HidePlayInfoView(5000);
                            break;
                        }
                        break;
                    case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                    case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                    case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                    case MW.RET_TP_EXIST /*10*/:
                    case MW.RET_INVALID_SAT /*11*/:
                    case MW.RET_INVALID_AREA /*12*/:
                    case MW.RET_INVALID_TP /*13*/:
                    case MW.RET_BOOK_EVENT_INVALID /*14*/:
                    case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                        int value = 0;
                        Log.e("LEE", "KeyCode :" + keyCode);
                        if (RecordListActivity.this.hour.isFocused()) {
                            if (RecordListActivity.this.hour.getText().toString().length() > 0) {
                                value = Integer.valueOf(RecordListActivity.this.hour.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100) {
                                value %= 10;
                            }
                            RecordListActivity.this.hour.setText(Integer.toString(value));
                            return true;
                        } else if (RecordListActivity.this.minute.isFocused()) {
                            if (RecordListActivity.this.minute.getText().toString().length() > 0) {
                                value = Integer.valueOf(RecordListActivity.this.minute.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100) {
                                value %= 10;
                            } else if (value >= 60) {
                                value = 0;
                            }
                            RecordListActivity.this.minute.setText(Integer.toString(value));
                            return true;
                        } else if (!RecordListActivity.this.second.isFocused()) {
                            return true;
                        } else {
                            if (RecordListActivity.this.second.getText().toString().length() > 0) {
                                value = Integer.valueOf(RecordListActivity.this.second.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100) {
                                value %= 10;
                            } else if (value >= 60) {
                                value = 0;
                            }
                            RecordListActivity.this.second.setText(Integer.toString(value));
                            return true;
                        }
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                        Log.e("LEE ", "focus : " + RecordListActivity.this.hour.isFocused() + RecordListActivity.this.minute.isFocused() + RecordListActivity.this.second.isFocused());
                        if (RecordListActivity.this.hour.isFocused()) {
                            RecordListActivity.this.second.requestFocus();
                            return true;
                        } else if (RecordListActivity.this.second.isFocused()) {
                            RecordListActivity.this.second.clearFocus();
                            RecordListActivity.this.minute.requestFocus();
                            return true;
                        } else if (RecordListActivity.this.minute.isFocused()) {
                            RecordListActivity.this.minute.clearFocus();
                            if (RecordListActivity.this.hour.isFocusable()) {
                                RecordListActivity.this.hour.requestFocus();
                                return true;
                            }
                            RecordListActivity.this.second.requestFocus();
                            return true;
                        }
                        break;
                    case MW.RET_MEMORY_ERROR /*22*/:
                        if (RecordListActivity.this.second.isFocused()) {
                            RecordListActivity.this.second.clearFocus();
                            if (RecordListActivity.this.hour.isFocusable()) {
                                RecordListActivity.this.hour.requestFocus();
                                return true;
                            }
                            RecordListActivity.this.minute.requestFocus();
                            return true;
                        } else if (RecordListActivity.this.hour.isFocused()) {
                            RecordListActivity.this.hour.clearFocus();
                            RecordListActivity.this.minute.requestFocus();
                            return true;
                        } else if (RecordListActivity.this.minute.isFocused()) {
                            RecordListActivity.this.minute.clearFocus();
                            RecordListActivity.this.second.requestFocus();
                            return true;
                        }
                        break;
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_MEDIA_PLAY_PAUSE /*85*/:
                        int m_seektime = (((RecordListActivity.this.m_hour * 60) * 60) + (RecordListActivity.this.m_minute * 60)) + RecordListActivity.this.m_second;
                        Log.e("LEE", "m_seektime : " + m_seektime);
                        Log.e("LEE", "m_hour : " + RecordListActivity.this.m_hour);
                        Log.e("LEE", "m_minute : " + RecordListActivity.this.m_minute);
                        Log.e("LEE", " m_second : " + RecordListActivity.this.m_second);
                        if (m_seektime < 0 || m_seektime > RecordListActivity.this.totaltime) {
                            RecordListActivity.this.tips_txt.setText(RecordListActivity.this.getResources().getString(R.string.seekTimeTitletips_invalid));
                            RecordListActivity.this.tips_txt.setTextColor(Color.rgb(255, 0, 0));
                            return true;
                        } else if (m_seektime == RecordListActivity.this.seektime) {
                            MW.pvr_playback_resume();
                            RecordListActivity.this.playback_status = 6;
                            RecordListActivity.this.HidePlayInfoView(5000);
                            dialog.dismiss();
                            return true;
                        } else {
                            MW.pvr_playback_seek(m_seektime, false);
                            dialog.dismiss();
                            RecordListActivity.this.HidePlayInfoView(5000);
                            return true;
                        }
                }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.5 */
    class C02165 implements Callback {
        C02165() {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("RecordListActivity", "surfaceChanged");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("RecordListActivity", "surfaceCreated");
            try {
                initSurface(holder);
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("RecordListActivity", "surfaceDestroyed");
        }

        private void initSurface(SurfaceHolder h) {
            Canvas c = null;
            try {
                Log.d("RecordListActivity", "initSurface");
                c = h.lockCanvas();
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            } catch (Throwable th) {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.6 */
    class C02176 implements OnScrollListener {
        C02176() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            RecordListActivity.this.mStorageFirstVisibleItem = firstVisibleItem;
            RecordListActivity.this.mStorageVisibleItemCount = visibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.7 */
    class C02187 implements OnItemSelectedListener {
        C02187() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            Log.e("RecordListActivity", "storage list item Position" + position);
            RecordListActivity.this.storageDeviceIndex = position;
            RecordListActivity.this.storage_list_item_pos = position;
            RecordListActivity.this.refreshRecordList();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.8 */
    class C02198 implements OnScrollListener {
        C02198() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            RecordListActivity.this.mRecordFirstVisibleItem = firstVisibleItem;
            RecordListActivity.this.mRecordVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.RecordListActivity.9 */
    class C02209 implements OnItemSelectedListener {
        C02209() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            Log.e("RecordListActivity", "record list item Position" + position);
            RecordListActivity.this.record_list_item_pos = position;
            if (MW.pvr_record_get_record_info(RecordListActivity.this.record_list_item_pos, RecordListActivity.this.pvrRecord) == 0 && RecordListActivity.this.pvrRecord.is_locked) {
                SETTINGS.bPass = false;
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
            RecordListActivity.this.record_list_item_pos = 0;
        }
    }

    public class AudioInfoDialog extends Dialog {
        private int count;
        private ArrayAdapter mAdapter;
        TextView mAudioTrack;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;
        private int prog_num;

        /* renamed from: th.dtv.activity.RecordListActivity.AudioInfoDialog.1 */
        class C02211 implements OnItemClickListener {
            C02211() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                System.out.println("position : " + position);
                System.out.println("prog_num : " + AudioInfoDialog.this.prog_num);
                RecordListActivity.this.pos[AudioInfoDialog.this.prog_num] = position;
                MW.ts_player_play_switch_audio(position);
                AudioInfoDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.audio_info_dialog);
            if (RecordListActivity.this.pos == null) {
                RecordListActivity.this.pos = new int[this.count];
            }
            RecordListActivity.this.dia_currentDialog = this;
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mAudioTrack = (TextView) findViewById(R.id.info);
            switch (SETTINGS.get_audio_channel()) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    this.mAudioTrack.setText(R.string.stereo);
                    break;
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    this.mAudioTrack.setText(R.string.left);
                    break;
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    this.mAudioTrack.setText(R.string.right);
                    break;
            }
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            if (RecordListActivity.this.pvrRecordInfo.audio_info.length > 0) {
                List<String> languages = new ArrayList();
                languages.clear();
                int i = 0;
                while (i < RecordListActivity.this.pvrRecordInfo.audio_info.length) {
                    String audio_type = RecordListActivity.this.getResources().getStringArray(R.array.audio_type)[RecordListActivity.this.pvrRecordInfo.audio_info[i].audio_stream_type];
                    if (RecordListActivity.this.pvrRecordInfo.audio_info[i].ISO_639_language_code.length() <= 0 || RecordListActivity.this.pvrRecordInfo.audio_info[i].ISO_639_language_code.charAt(0) == '\u0000') {
                        languages.add("Audio " + (i + 1) + " (" + audio_type + ")");
                    } else {
                        languages.add(RecordListActivity.this.pvrRecordInfo.audio_info[i].ISO_639_language_code + " (" + audio_type + ")");
                    }
                    i++;
                }
                this.mAdapter = new ArrayAdapter(RecordListActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, languages);
                this.mListView.setAdapter(this.mAdapter);
                this.mListView.setChoiceMode(1);
                this.mListView.setItemChecked(RecordListActivity.this.pos[this.prog_num], true);
                this.mListView.setCustomSelection(RecordListActivity.this.pos[this.prog_num]);
                this.mListView.setVisibleItemCount(5);
                this.mListView.setOnItemClickListener(new C02211());
            }
        }

        protected void onStop() {
            super.onStop();
            RecordListActivity.this.dia_currentDialog = null;
        }

        public AudioInfoDialog(Context context, int theme, int i, int count) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mAudioTrack = null;
            this.prog_num = 0;
            this.count = 0;
            this.mContext = context;
            System.out.println("count : " + count);
            System.out.println("i : " + i);
            this.prog_num = i;
            this.count = count;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            int channel;
            switch (keyCode) {
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    channel = SETTINGS.get_audio_channel();
                    if (channel == 1) {
                        this.mAudioTrack.setText(R.string.stereo);
                        SETTINGS.set_audio_channel(0);
                        return true;
                    } else if (channel == 2) {
                        this.mAudioTrack.setText(R.string.left);
                        SETTINGS.set_audio_channel(1);
                        return true;
                    } else {
                        this.mAudioTrack.setText(R.string.right);
                        SETTINGS.set_audio_channel(2);
                        return true;
                    }
                case MW.RET_MEMORY_ERROR /*22*/:
                    channel = SETTINGS.get_audio_channel();
                    if (channel == 1) {
                        this.mAudioTrack.setText(R.string.right);
                        SETTINGS.set_audio_channel(2);
                        return true;
                    } else if (channel == 2) {
                        this.mAudioTrack.setText(R.string.stereo);
                        SETTINGS.set_audio_channel(0);
                        return true;
                    } else {
                        this.mAudioTrack.setText(R.string.left);
                        SETTINGS.set_audio_channel(1);
                        return true;
                    }
                case DtvBaseActivity.KEYCODE_AUDIO /*2006*/:
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class EditDialog extends Dialog {
        TextView item_no;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        EditText mFileName;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.pvr_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.item_no = (TextView) findViewById(R.id.item_no);
            this.mFileName = (EditText) findViewById(R.id.edittext_pvr_name);
            this.mTitle.setText(RecordListActivity.this.getResources().getString(R.string.rename));
            if (MW.pvr_record_get_record_info(RecordListActivity.this.record_list_item_pos, RecordListActivity.this.pvrRecord) == 0) {
                this.item_no.setText((RecordListActivity.this.record_list_item_pos + 1) + "");
                this.mFileName.setText(RecordListActivity.this.pvrRecord.service_name);
                this.mFileName.setSelection(0, this.mFileName.length());
            } else {
                this.item_no.setText("");
                this.mFileName.setText("");
            }
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.item_no = null;
            this.mFileName = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    class LOCALMessageHandler extends Handler {
        public LOCALMessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    RecordListActivity.this.HidePlayInfoView(0);
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    if (!RecordListActivity.this.bUpdate) {
                        RecordListActivity.this.rl_loading.setVisibility(View.VISIBLE);
                    }
                default:
            }
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r21) {
            /*
            r20 = this;
            r0 = r21;
            r15 = r0.what;
            r15 = r15 >> 16;
            r16 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
            r5 = r15 & r16;
            r0 = r21;
            r15 = r0.what;
            r16 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
            r9 = r15 & r16;
            switch(r5) {
                case 7: goto L_0x043f;
                case 8: goto L_0x0018;
                default: goto L_0x0017;
            };
        L_0x0017:
            return;
        L_0x0018:
            switch(r9) {
                case 4: goto L_0x001c;
                case 5: goto L_0x037d;
                case 6: goto L_0x00a7;
                case 7: goto L_0x01cd;
                case 8: goto L_0x0239;
                case 9: goto L_0x036b;
                default: goto L_0x001b;
            };
        L_0x001b:
            goto L_0x0017;
        L_0x001c:
            r0 = r21;
            r14 = r0.arg1;
            r11 = 0;
            r12 = 0;
            r13 = 0;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.totaltime = r14;
            r13 = r14 % 60;
            r15 = r14 / 60;
            r12 = r15 % 60;
            r11 = r14 / 3600;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_seekbar;
            r15.setMax(r14);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_duration;
            r16 = "%02d:%02d:%02d";
            r17 = 3;
            r0 = r17;
            r0 = new java.lang.Object[r0];
            r17 = r0;
            r18 = 0;
            r19 = java.lang.Integer.valueOf(r11);
            r17[r18] = r19;
            r18 = 1;
            r19 = java.lang.Integer.valueOf(r12);
            r17[r18] = r19;
            r18 = 2;
            r19 = java.lang.Integer.valueOf(r13);
            r17[r18] = r19;
            r16 = java.lang.String.format(r16, r17);
            r15.setText(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_current_time;
            r16 = "00:00:00";
            r15.setText(r16);
            r15 = "PVR";
            r16 = new java.lang.StringBuilder;
            r16.<init>();
            r17 = "timeshift total time : ";
            r16 = r16.append(r17);
            r0 = r16;
            r16 = r0.append(r14);
            r16 = r16.toString();
            android.util.Log.e(r15, r16);
            r15 = "PVR";
            r16 = "INIT";
            android.util.Log.e(r15, r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 1;
            r15.initflag = r16;
            goto L_0x0017;
        L_0x00a7:
            r0 = r21;
            r4 = r0.arg1;
            r1 = 0;
            r2 = 0;
            r3 = 0;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.seektime = r4;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r3 = r4 % 60;
            r15.m_second = r3;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = r4 / 60;
            r2 = r16 % 60;
            r15.m_minute = r2;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r1 = r4 / 3600;
            r15.m_hour = r1;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_current_time;
            r16 = "%02d:%02d:%02d";
            r17 = 3;
            r0 = r17;
            r0 = new java.lang.Object[r0];
            r17 = r0;
            r18 = 0;
            r19 = java.lang.Integer.valueOf(r1);
            r17[r18] = r19;
            r18 = 1;
            r19 = java.lang.Integer.valueOf(r2);
            r17[r18] = r19;
            r18 = 2;
            r19 = java.lang.Integer.valueOf(r3);
            r17[r18] = r19;
            r16 = java.lang.String.format(r16, r17);
            r15.setText(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_seekbar;
            r15.setProgress(r4);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 4;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_speed;
            r16 = 4;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.fbackforward;
            if (r15 != 0) goto L_0x014d;
        L_0x0132:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 2130837700; // 0x7f0200c4 float:1.7280361E38 double:1.0527737044E-314;
            r15.setImageResource(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 0;
            r15.setVisibility(r16);
        L_0x014d:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.playback_status;
            r16 = 8;
            r0 = r16;
            if (r15 != r0) goto L_0x0164;
        L_0x015b:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 5000; // 0x1388 float:7.006E-42 double:2.4703E-320;
            r15.HidePlayInfoView(r16);
        L_0x0164:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 6;
            r15.playback_status = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.fbackforward = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.speed = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.flag = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.ShowSubtitle();
            r15 = "PVR";
            r16 = new java.lang.StringBuilder;
            r16.<init>();
            r17 = " timeshift update time : ";
            r16 = r16.append(r17);
            r0 = r16;
            r16 = r0.append(r4);
            r17 = "play_status :";
            r16 = r16.append(r17);
            r0 = r20;
            r0 = th.dtv.activity.RecordListActivity.this;
            r17 = r0;
            r17 = r17.playback_status;
            r16 = r16.append(r17);
            r16 = r16.toString();
            android.util.Log.e(r15, r16);
            r15 = "PVR";
            r16 = "PLAY";
            android.util.Log.e(r15, r16);
            r15 = "LEE";
            r16 = "PVR_PLAYBACK_STATUS_PLAY";
            android.util.Log.e(r15, r16);
            goto L_0x0017;
        L_0x01cd:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 7;
            r15.playback_status = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.speed = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.fbackforward = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.HidePlayInfoView(r16);
            r15 = "LEE";
            r16 = "PVR_PLAYBACK_STATUS_PAUSE";
            android.util.Log.e(r15, r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.ShowPlayInfoView();
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_speed;
            r16 = 4;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 2130837675; // 0x7f0200ab float:1.728031E38 double:1.052773692E-314;
            r15.setImageResource(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 0;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.flag = r16;
            r15 = "PVR";
            r16 = "pause";
            android.util.Log.e(r15, r16);
            goto L_0x0017;
        L_0x0239:
            r0 = r21;
            r10 = r0.arg1;
            r6 = 0;
            r7 = 0;
            r8 = 0;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.HideSubtitle();
            r8 = r10 % 60;
            r15 = r10 / 60;
            r7 = r15 % 60;
            r6 = r10 / 3600;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 8;
            r15.playback_status = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_current_time;
            r16 = "%02d:%02d:%02d";
            r17 = 3;
            r0 = r17;
            r0 = new java.lang.Object[r0];
            r17 = r0;
            r18 = 0;
            r19 = java.lang.Integer.valueOf(r6);
            r17[r18] = r19;
            r18 = 1;
            r19 = java.lang.Integer.valueOf(r7);
            r17[r18] = r19;
            r18 = 2;
            r19 = java.lang.Integer.valueOf(r8);
            r17[r18] = r19;
            r16 = java.lang.String.format(r16, r17);
            r15.setText(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_bottom_seekbar;
            r15.setProgress(r10);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.fbackforward;
            r16 = 2;
            r0 = r16;
            if (r15 != r0) goto L_0x0316;
        L_0x02a2:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 2130837680; // 0x7f0200b0 float:1.728032E38 double:1.0527736946E-314;
            r15.setImageResource(r16);
        L_0x02b0:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.flag;
            if (r15 != 0) goto L_0x0350;
        L_0x02ba:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.HidePlayInfoView(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.ShowPlayInfoView();
            r15 = "LEE";
            r16 = "PVR_PLAYBACK_STATUS_FFFB";
            android.util.Log.e(r15, r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_speed;
            r16 = 0;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 0;
            r15.setVisibility(r16);
        L_0x02eb:
            r15 = "PVR";
            r16 = new java.lang.StringBuilder;
            r16.<init>();
            r17 = "SPEED : ";
            r16 = r16.append(r17);
            r0 = r20;
            r0 = th.dtv.activity.RecordListActivity.this;
            r17 = r0;
            r17 = r17.speed;
            r16 = r16.append(r17);
            r16 = r16.toString();
            android.util.Log.e(r15, r16);
            r15 = "PVR";
            r16 = "FFFB";
            android.util.Log.e(r15, r16);
            goto L_0x0017;
        L_0x0316:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.fbackforward;
            r16 = 1;
            r0 = r16;
            if (r15 != r0) goto L_0x0334;
        L_0x0324:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 2130837671; // 0x7f0200a7 float:1.7280303E38 double:1.05277369E-314;
            r15.setImageResource(r16);
            goto L_0x02b0;
        L_0x0334:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_speed;
            r16 = 4;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 4;
            r15.setVisibility(r16);
            goto L_0x02b0;
        L_0x0350:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_speed;
            r16 = 4;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 0;
            r15.setVisibility(r16);
            goto L_0x02eb;
        L_0x036b:
            r15 = "PVR";
            r16 = "SEEK";
            android.util.Log.e(r15, r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 9;
            r15.playback_status = r16;
            goto L_0x0017;
        L_0x037d:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.play_status_img;
            r16 = 4;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.ll_recordListUI;
            r15 = r15.getVisibility();
            r16 = 8;
            r0 = r16;
            if (r15 != r0) goto L_0x03d3;
        L_0x039c:
            th.dtv.MW.pvr_playback_stop();
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.ll_recordListUI;
            r16 = 0;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.Showmanline();
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.record_list;
            r15.requestFocus();
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.record_list;
            r0 = r20;
            r0 = th.dtv.activity.RecordListActivity.this;
            r16 = r0;
            r16 = r16.record_list_item_pos;
            r15.setCustomSelection(r16);
        L_0x03d3:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.dialog;
            if (r15 == 0) goto L_0x03f0;
        L_0x03db:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.dialog;
            r15 = r15.isShowing();
            if (r15 == 0) goto L_0x03f0;
        L_0x03e7:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.dialog;
            r15.dismiss();
        L_0x03f0:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.dia_currentDialog;
            if (r15 == 0) goto L_0x0405;
        L_0x03fa:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.dia_currentDialog;
            r15.dismiss();
        L_0x0405:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.stopTeletextSubtitle();
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.initflag;
            r16 = 1;
            r0 = r16;
            if (r15 != r0) goto L_0x041d;
        L_0x041a:
            th.dtv.MW.pvr_playback_stop();
        L_0x041d:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 0;
            r15.speed = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 5;
            r15.playback_status = r16;
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15.ShowSubtitle();
            r15 = "PVR";
            r16 = "exit";
            android.util.Log.e(r15, r16);
            goto L_0x0017;
        L_0x043f:
            if (r9 != 0) goto L_0x0502;
        L_0x0441:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.inTeletextMode;
            if (r15 != 0) goto L_0x0455;
        L_0x044b:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.inSubtitleMode;
            if (r15 == 0) goto L_0x04a2;
        L_0x0455:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.rl_loading;
            r16 = 8;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.inTeletextMode;
            if (r15 == 0) goto L_0x04a2;
        L_0x046c:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.bRefresh;
            if (r15 == 0) goto L_0x0494;
        L_0x0476:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.et_ttxPageNumber;
            r16 = th.dtv.MW.teletext_get_current_page();
            r16 = java.lang.Integer.toString(r16);
            r15.setText(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.et_ttxPageNumber;
            r15.selectAll();
        L_0x0494:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.ttxSubPageInfo;
            r15 = th.dtv.MW.teletext_get_sub_page_info(r15);
            if (r15 != 0) goto L_0x04a2;
        L_0x04a2:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.inTeletextMode;
            if (r15 == 0) goto L_0x04e5;
        L_0x04ac:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.bTeletextShowed;
            if (r15 != 0) goto L_0x04bf;
        L_0x04b6:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 1;
            r15.bTeletextShowed = r16;
        L_0x04bf:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.et_ttxPageNumber;
            r15 = r15.getVisibility();
            if (r15 == 0) goto L_0x04e5;
        L_0x04cd:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.et_ttxPageNumber;
            r16 = 0;
            r15.setVisibility(r16);
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.et_ttxPageNumber;
            r15.requestFocus();
        L_0x04e5:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.ttx_sub;
            r15.update();
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r16 = 1;
            r15.bUpdate = r16;
            r15 = "RecordListActivity";
            r16 = "EVENT_TYPE_TELETEXT_SUBTITLE_UPDATE ";
            android.util.Log.e(r15, r16);
            goto L_0x0017;
        L_0x0502:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.inTeletextMode;
            if (r15 == 0) goto L_0x0017;
        L_0x050c:
            r15 = 1;
            if (r9 != r15) goto L_0x0017;
        L_0x050f:
            r0 = r20;
            r15 = th.dtv.activity.RecordListActivity.this;
            r15 = r15.ttxSubPageInfo;
            r15 = th.dtv.MW.teletext_get_sub_page_info(r15);
            if (r15 != 0) goto L_0x0017;
        L_0x051d:
            goto L_0x0017;
            */
            throw new UnsupportedOperationException("Method not decompiled: th.dtv.activity.RecordListActivity.MWmessageHandler.handleMessage(android.os.Message):void");
        }
    }

    class RecordListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            ImageView lock_img;
            TextView num;
            TextView pvr_program_name;
            ImageView select_img;
            TextView size;
            TextView time;

            ViewHolder() {
            }
        }

        public RecordListAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            System.out.println("" + RecordListActivity.this.recordListCount);
            return RecordListActivity.this.recordListCount;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.pvr_program_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.num = (TextView) convertView.findViewById(R.id.num);
                localViewHolder.select_img = (ImageView) convertView.findViewById(R.id.select_img);
                localViewHolder.lock_img = (ImageView) convertView.findViewById(R.id.lock_img);
                localViewHolder.pvr_program_name = (TextView) convertView.findViewById(R.id.pvr_program_name);
                localViewHolder.size = (TextView) convertView.findViewById(R.id.size);
                localViewHolder.time = (TextView) convertView.findViewById(R.id.time);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            localViewHolder.num.setText("" + (position + 1));
            if (MW.pvr_record_get_record_info(position, RecordListActivity.this.pvrRecord) == 0) {
                if (RecordListActivity.this.pvrRecord.event_name == null || RecordListActivity.this.pvrRecord.event_name.isEmpty()) {
                    localViewHolder.pvr_program_name.setText(RecordListActivity.this.pvrRecord.service_name);
                } else {
                    localViewHolder.pvr_program_name.setText(RecordListActivity.this.pvrRecord.service_name + ": " + RecordListActivity.this.pvrRecord.event_name);
                }
                if (((RecordListActivity.this.pvrRecord.file_size / 1024) / 1024) / 1024 >= 1) {
                    localViewHolder.size.setText(String.format("%.2f G", new Object[]{Float.valueOf(((((float) RecordListActivity.this.pvrRecord.file_size) / 1024.0f) / 1024.0f) / 1024.0f)}));
                } else if ((RecordListActivity.this.pvrRecord.file_size / 1024) / 1024 >= 1) {
                    localViewHolder.size.setText(String.format("%d MB", new Object[]{Long.valueOf((RecordListActivity.this.pvrRecord.file_size / 1024) / 1024)}));
                } else if (RecordListActivity.this.pvrRecord.file_size / 1024 >= 1) {
                    localViewHolder.size.setText(String.format("%d KB", new Object[]{Long.valueOf(RecordListActivity.this.pvrRecord.file_size / 1024)}));
                } else {
                    localViewHolder.size.setText(String.format("%d B", new Object[]{Long.valueOf(RecordListActivity.this.pvrRecord.file_size / 1024)}));
                }
                localViewHolder.time.setText(String.format("%04d-%02d-%02d", new Object[]{Integer.valueOf(RecordListActivity.this.pvrRecord.record_year), Integer.valueOf(RecordListActivity.this.pvrRecord.record_month), Integer.valueOf(RecordListActivity.this.pvrRecord.record_day)}));
                if (RecordListActivity.this.PvrSelStatus != null) {
                    System.out.println("not null :" + position);
                    if (RecordListActivity.this.PvrSelStatus[RecordListActivity.this.storageDeviceIndex] != null) {
                        System.out.println("PvrSelStatus[ " + RecordListActivity.this.storageDeviceIndex + "][" + position + "]" + RecordListActivity.this.PvrSelStatus[RecordListActivity.this.storageDeviceIndex][position]);
                        if (RecordListActivity.this.PvrSelStatus[RecordListActivity.this.storageDeviceIndex][position]) {
                            localViewHolder.select_img.setVisibility(View.VISIBLE);
                        } else {
                            localViewHolder.select_img.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        localViewHolder.select_img.setVisibility(View.INVISIBLE);
                    }
                } else {
                    System.out.println("null :" + position);
                }
                if (RecordListActivity.this.pvrRecord.is_locked) {
                    localViewHolder.lock_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.lock_img.setVisibility(View.INVISIBLE);
                }
            } else {
                localViewHolder.pvr_program_name.setText("");
                localViewHolder.size.setText("");
                localViewHolder.time.setText("");
                localViewHolder.lock_img.setVisibility(View.INVISIBLE);
            }
            return convertView;
        }
    }

    class StorageAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView stroage_name;

            ViewHolder() {
            }
        }

        public StorageAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return RecordListActivity.this.storageDeviceCount;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            Log.e("RecordListActivity", "storage adapter position : " + position);
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.storage_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.stroage_name = (TextView) convertView.findViewById(R.id.storage_name);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            DeviceItem item = RecordListActivity.this.storageDeviceInfo.getDeviceItem(position);
            if (item == null) {
                localViewHolder.stroage_name.setText("");
            } else if (item.VolumeName.indexOf("[udisk") == 1) {
                localViewHolder.stroage_name.setText(RecordListActivity.this.getResources().getString(R.string.udisk) + " " + ((RecordListActivity.this.storageDeviceCount - 1) - position));
            } else {
                localViewHolder.stroage_name.setText(item.VolumeName);
            }
            return convertView;
        }
    }

    private class StorageDevicePlugReceiver extends BroadcastReceiver {
        private StorageDevicePlugReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.intent.action.DEVICE_MOUNTED")) {
                RecordListActivity.this.refreshStorageDeviceList();
            } else if (action.equals("android.intent.action.DEVICE_REMOVED")) {
                RecordListActivity.this.refreshStorageDeviceList();
            }
        }
    }

    public class TeletextSubtitleInfoDialog extends Dialog {
        boolean bTeletext;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.activity.RecordListActivity.TeletextSubtitleInfoDialog.1 */
        class C02261 implements OnItemClickListener {
            C02261() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (TeletextSubtitleInfoDialog.this.bTeletext) {
                    RecordListActivity.this.startTeletextSubtitle(TeletextSubtitleInfoDialog.this.bTeletext, position, RecordListActivity.this.pvrRecordInfo);
                } else if (RecordListActivity.this.temp_whichone == -1 || RecordListActivity.this.temp_whichone != position) {
                    RecordListActivity.this.temp_whichone = position;
                    if (position == 0) {
                        RecordListActivity.this.stopTeletextSubtitle();
                    } else {
                        RecordListActivity.this.startTeletextSubtitle(TeletextSubtitleInfoDialog.this.bTeletext, position - 1, RecordListActivity.this.pvrRecordInfo);
                    }
                } else {
                    TeletextSubtitleInfoDialog.this.dismiss();
                    return;
                }
                TeletextSubtitleInfoDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            RecordListActivity.this.dia_currentDialog = this;
            TextView txv_Title = (TextView) findViewById(R.id.TextView_Title);
            if (this.bTeletext) {
                txv_Title.setText(R.string.teletext_language);
            } else {
                txv_Title.setText(R.string.subtitle_language);
            }
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            int whichone = 0;
            List<String> items = new ArrayList();
            items.clear();
            int i;
            if (this.bTeletext) {
                for (ttx_info th_dtv_mw_data_ttx_info : RecordListActivity.this.pvrRecordInfo.teletext_info) {
                    items.add(th_dtv_mw_data_ttx_info.ISO_639_language_code);
                    whichone = 0;
                }
            } else {
                items.add(RecordListActivity.this.getString(R.string.off));
                i = 0;
                while (i < RecordListActivity.this.pvrRecordInfo.subtitle_info.length) {
                    if (RecordListActivity.this.pvrRecordInfo.subtitle_info[i].subtitling_type == 2 || RecordListActivity.this.pvrRecordInfo.subtitle_info[i].subtitling_type == 5) {
                        items.add(RecordListActivity.this.pvrRecordInfo.subtitle_info[i].ISO_639_language_code + " (T) ");
                    } else {
                        items.add(RecordListActivity.this.pvrRecordInfo.subtitle_info[i].ISO_639_language_code + " (D) ");
                    }
                    i++;
                }
                i = 0;
                while (i < RecordListActivity.this.pvrRecordInfo.subtitle_info.length) {
                    if (!RecordListActivity.this.pvrRecordInfo.subtitle_info[i].ISO_639_language_code.equals(SETTINGS.get_subtitle_language())) {
                        if (RecordListActivity.this.pvrRecordInfo.subtitle_info[i].subtitling_type != 2 && RecordListActivity.this.pvrRecordInfo.subtitle_info[i].subtitling_type != 5) {
                            whichone = i + 1;
                            break;
                        } else {
                            whichone = 1;
                            i++;
                        }
                    } else if (RecordListActivity.this.pvrRecordInfo.subtitle_info[i].subtitling_type == 2 || RecordListActivity.this.pvrRecordInfo.subtitle_info[i].subtitling_type == 5) {
                        int j = i;
                        while (j < RecordListActivity.this.pvrRecordInfo.subtitle_info.length) {
                            if (RecordListActivity.this.pvrRecordInfo.subtitle_info[j].subtitling_type != 2 && RecordListActivity.this.pvrRecordInfo.subtitle_info[j].subtitling_type != 5) {
                                whichone = j + 1;
                                break;
                            } else {
                                whichone = i + 1;
                                j++;
                            }
                        }
                    } else {
                        whichone = i + 1;
                    }
                }
                if (!SETTINGS.get_subtitle_default_on()) {
                    whichone = 0;
                }
            }
            this.mAdapter = new ArrayAdapter(RecordListActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            if (RecordListActivity.this.temp_whichone == -1) {
                this.mListView.setItemChecked(whichone, true);
                this.mListView.setCustomSelection(whichone);
                RecordListActivity.this.temp_whichone = whichone;
            } else {
                this.mListView.setItemChecked(RecordListActivity.this.temp_whichone, true);
                this.mListView.setCustomSelection(RecordListActivity.this.temp_whichone);
            }
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C02261());
        }

        protected void onStop() {
            super.onStop();
            RecordListActivity.this.dia_currentDialog = null;
        }

        public TeletextSubtitleInfoDialog(Context context, int theme, boolean bTTX) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
            this.bTeletext = bTTX;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    if (!this.bTeletext) {
                        return true;
                    }
                    dismiss();
                    return true;
                case DtvBaseActivity.KEYCODE_SUBTITLE /*2012*/:
                    if (this.bTeletext) {
                        return true;
                    }
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    public RecordListActivity() {
        this.mStorageFirstVisibleItem = 0;
        this.mStorageVisibleItemCount = 0;
        this.mRecordFirstVisibleItem = 0;
        this.mRecordVisibleItemCount = 0;
        this.storageDeviceCount = 0;
        this.storageDeviceIndex = 0;
        this.storage_list_item_pos = 0;
        this.recordListCount = 0;
        this.record_list_item_pos = 0;
        this.bHandlePreNext = false;
        this.pvrRecord = new pvr_record();
        this.playback_status = 3;
        this.speed = 0;
        this.fbackforward = 0;
        this.flag = false;
        this.initflag = false;
        this.bRefresh = true;
        this.pvrRecordInfo = new pvr_record();
        this.ttxSubPageInfo = new ttx_sub_page_info();
        this.PvrSelStatus = (boolean[][]) null;
        this.SelPvrList = new ArrayList();
        this.seektime = 0;
        this.totaltime = 0;
        this.m_hour = 0;
        this.m_minute = 0;
        this.m_second = 0;
        this.temp_hour = 0;
        this.temp_minute = 0;
        this.temp_second = 0;
        this.mSHCallback = new C02165();
        this.hasLockedFile = false;
        this.pswdDig = null;
        this.temp_whichone = -1;
        this.pos = null;
    }

    private void initPvrSelStatus() {
        if (this.storageDeviceInfo != null) {
            this.storageDeviceCount = this.storageDeviceInfo.getDeviceCount();
        } else {
            this.storageDeviceCount = 0;
        }
        int device_count = this.storageDeviceCount;
        if (device_count > 0) {
            this.PvrSelStatus = new boolean[device_count][];
            System.out.println("PvrSelStatus " + this.PvrSelStatus);
            for (int i = 0; i < device_count; i++) {
                DeviceItem item = this.storageDeviceInfo.getDeviceItem(i);
                if (item != null && MW.pvr_record_init_record_list(item.Path) == 0) {
                    int filecount = MW.pvr_record_get_record_count();
                    if (filecount > 0) {
                        this.PvrSelStatus[i] = new boolean[filecount];
                        for (int j = 0; j < filecount; j++) {
                            this.PvrSelStatus[i][j] = false;
                        }
                    } else {
                        this.PvrSelStatus[i] = null;
                    }
                }
            }
        }
    }

    private void getSelPvrFileInfo() {
        this.SelPvrList.clear();
        if (this.PvrSelStatus != null) {
            for (int j = 0; j < this.recordListCount; j++) {
                if (this.PvrSelStatus[this.storageDeviceIndex][j]) {
                    System.out.println("PvrSelStatus file index  : " + j);
                    this.SelPvrList.add(Integer.valueOf(j));
                }
            }
        }
        if (this.SelPvrList.size() == 0) {
            this.SelPvrList.add(Integer.valueOf(this.record_list_item_pos));
        }
    }

    private void RefreshPvrSelectImage() {
        if (this.PvrSelStatus == null) {
            System.out.println("PvrSelStatus is null");
        } else if (this.PvrSelStatus[this.storageDeviceIndex] != null) {
            this.PvrSelStatus[this.storageDeviceIndex][this.record_list_item_pos] = !this.PvrSelStatus[this.storageDeviceIndex][this.record_list_item_pos];
        }
        this.recordListAdapter.notifyDataSetChanged();
    }

    private void initPlayView() {
        this.RL_RecordPlay = (RelativeLayout) findViewById(R.id.RL_RecordPlay);
        this.play_status_img = (ImageView) findViewById(R.id.play_status_img);
        this.play_bottom_videoName = (TextView) findViewById(R.id.play_bottom_videoName);
        this.play_bottom_current_time = (TextView) findViewById(R.id.play_bottom_current_time);
        this.play_bottom_duration = (TextView) findViewById(R.id.play_bottom_duration);
        this.play_bottom_seekbar = (SeekBar) findViewById(R.id.play_bottom_seekbar);
        this.play_speed = (TextView) findViewById(R.id.play_speed);
        this.play_bottom_current_time.setText("00:00:00");
        this.play_bottom_duration.setText("00:00:00");
        this.play_bottom_seekbar.setProgress(0);
        this.play_status_img.setVisibility(View.INVISIBLE);
        this.play_speed.setVisibility(View.INVISIBLE);
        this.fbackforward = 0;
    }

    private void ShowPlayInfoView() {
        this.RL_RecordPlay.setVisibility(View.VISIBLE);
        if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecord) == 0) {
            if (this.pvrRecord.service_name != null) {
                if (this.pvrRecord.service_name.length() > 0) {
                    this.play_bottom_videoName.setText(this.pvrRecord.service_name);
                } else {
                    this.play_bottom_videoName.setText("Unknow");
                }
            }
            if (!SETTINGS.bPass) {
                int duration = this.pvrRecord.duration;
                int total_second = duration % 60;
                int total_minute = (duration / 60) % 60;
                int total_hour = duration / 3600;
                this.play_bottom_seekbar.setMax(duration);
                this.play_bottom_duration.setText(String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf(total_hour), Integer.valueOf(total_minute), Integer.valueOf(total_second)}));
                this.play_status_img.setVisibility(View.INVISIBLE);
                this.play_speed.setVisibility(View.INVISIBLE);
                if (this.bHandlePreNext) {
                    this.bHandlePreNext = false;
                    this.play_bottom_current_time.setText("00:00:00");
                    this.play_bottom_seekbar.setProgress(0);
                }
            }
        }
        Log.e("PVR", "playback_status =" + this.playback_status);
        if (this.playback_status != 7) {
            HidePlayInfoView(5000);
        }
    }

    private void HidePlayInfoView(int delayMS) {
        this.localMsgHandler.removeMessages(0);
        if (delayMS > 0) {
            this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(0), (long) delayMS);
        } else {
            this.RL_RecordPlay.setVisibility(View.INVISIBLE);
        }
    }

    private void HideSubtitle() {
        if (!this.inTeletextMode && this.bUpdate) {
            this.ttx_sub.hide();
            this.ttx_sub.update();
        }
    }

    private void ShowSubtitle() {
        if (!this.inTeletextMode && this.bUpdate) {
            this.ttx_sub.show();
            this.ttx_sub.update();
        }
    }

    private void ShowLoadingIcon() {
        this.bUpdate = false;
        this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(1), 500);
    }

    private void initMessageHandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        this.localMsgHandler = new LOCALMessageHandler(this.looper);
    }

    private void ShowPvrSeekTimeSetDialog() {
        this.temp_second = this.seektime % 60;
        this.temp_minute = (this.seektime / 60) % 60;
        this.temp_hour = this.seektime / 3600;
        View view = LayoutInflater.from(this).inflate(R.layout.seektimeedit, null);
        this.hour = (EditText) view.findViewById(R.id.seektime_hours);
        this.minute = (EditText) view.findViewById(R.id.seektime_minutes);
        this.second = (EditText) view.findViewById(R.id.seektime_seconds);
        this.tips_txt = (TextView) view.findViewById(R.id.tips_txt);
        this.tips_txt.setText(getResources().getString(R.string.seekTimeTitletips));
        this.hour.setText(String.format("%02d", new Object[]{Integer.valueOf(this.m_hour)}));
        this.minute.setText(String.format("%02d", new Object[]{Integer.valueOf(this.m_minute)}));
        this.second.setText(String.format("%02d", new Object[]{Integer.valueOf(this.m_second)}));
        if (this.totaltime / 3600 == 0) {
            this.hour.setFocusable(false);
            this.hour.setBackgroundResource(R.drawable.seektime_eidtback);
        } else {
            this.hour.setFocusable(true);
        }
        if (this.totaltime / 3600 != 0 || (this.totaltime / 60) % 60 != 0) {
            this.minute.setFocusable(true);
        } else if (this.totaltime % 60 != 0) {
            this.minute.setFocusable(false);
            this.minute.setBackgroundResource(R.drawable.seektime_eidtback);
        }
        this.hour.setInputType(0);
        this.minute.setInputType(0);
        this.second.setInputType(0);
        this.hour.addTextChangedListener(new C02121());
        this.minute.addTextChangedListener(new C02132());
        this.second.addTextChangedListener(new C02143());
        Builder builder = new Builder(this);
        builder.setView(view);
        Log.e("LEE", "hour is focused " + this.hour.isFocused());
        builder.setOnKeyListener(new C02154());
        this.dialog = builder.show();
        LayoutParams lp = this.dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        this.dialog.getWindow().setAttributes(lp);
        this.dialog.getWindow().addFlags(2);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.bTeletextShowed && onTeletextKeyDown(keyCode)) {
            return true;
        }
        LayoutParams lp;
        switch (keyCode) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
            case DtvBaseActivity.KEYCODE_MEDIA_STOP /*86*/:
                if (this.pswdDig != null) {
                    this.pswdDig.dismiss();
                    this.pswdDig = null;
                }
                if (keyCode == 4) {
                    if (this.rl_loading.getVisibility() == 0) {
                        if (!this.bTeletextShowed) {
                            refreshTeletextSubtitle();
                        }
                        return true;
                    } else if (this.RL_RecordPlay.getVisibility() != 4 && this.playback_status == 6) {
                        this.RL_RecordPlay.setVisibility(View.INVISIBLE);
                        return true;
                    }
                }
                if (this.dialog != null && this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (this.dia_currentDialog != null) {
                    this.dia_currentDialog.dismiss();
                }
                stopTeletextSubtitle();
                this.temp_whichone = -1;
                if (this.initflag) {
                    MW.pvr_playback_stop();
                }
                if (this.ll_recordListUI.getVisibility() == 8) {
                    MW.pvr_playback_stop();
                    this.ll_recordListUI.setVisibility(View.VISIBLE);
                    Showmanline();
                    this.record_list.requestFocus();
                    this.record_list.setCustomSelection(this.record_list_item_pos);
                    this.play_status_img.setVisibility(View.INVISIBLE);
                    this.playback_status = 5;
                    return true;
                }
                break;
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                sendKeyEvent(25);
                return true;
            case MW.RET_MEMORY_ERROR /*22*/:
                sendKeyEvent(24);
                return true;
            case MW.RET_INVALID_TYPE /*23*/:
            case DtvBaseActivity.KEYCODE_MEDIA_PLAY_PAUSE /*85*/:
                Log.e("PVR", "playback_status : " + this.playback_status);
                if (this.playback_status == 6) {
                    MW.pvr_playback_pause();
                    ShowPvrSeekTimeSetDialog();
                } else if (this.playback_status == 7) {
                    this.play_status_img.setVisibility(View.INVISIBLE);
                    MW.pvr_playback_resume();
                    HidePlayInfoView(5000);
                } else if (this.playback_status == 8) {
                    this.flag = true;
                    Log.e("LEE", "fbackforward OK : " + this.fbackforward);
                    if (this.fbackforward == 1) {
                        MW.pvr_playback_fast_forward(0);
                    } else {
                        MW.pvr_playback_fast_backward(0);
                    }
                    this.play_status_img.setImageResource(R.drawable.record);
                    this.play_speed.setVisibility(View.INVISIBLE);
                    HidePlayInfoView(5000);
                    Log.e("LEE", "hide info bar ");
                }
                return true;
            case DtvBaseActivity.KEYCODE_MEDIA_NEXT /*87*/:
                Log.e("LEE ", "recordListCount := " + this.recordListCount);
                if (this.ll_recordListUI.getVisibility() == 0) {
                    return true;
                }
                if (this.recordListCount == 1) {
                    Log.e("LEE", "no next item");
                    return true;
                }
                this.bHandlePreNext = true;
                if (this.record_list_item_pos < this.recordListCount - 1) {
                    this.record_list_item_pos++;
                } else {
                    this.record_list_item_pos = 0;
                }
                Log.e("LEE", "record_list_item_pos NEXT  := " + this.record_list_item_pos);
                if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecord) == 0) {
                    if (this.pvrRecord.is_locked) {
                        CheckNextPreLockAndShowPasswd("1");
                    } else {
                        if (this.pswdDig != null) {
                            this.pswdDig.dismiss();
                            this.pswdDig = null;
                        }
                        if (MW.pvr_playback_start(this.record_list_item_pos) == 0) {
                            ShowPlayInfoView();
                        } else {
                            Log.e("Lee", " KEYCODE_MEDIA_NEXT pvr_playback_start failed : record_list_item_pos = " + this.record_list_item_pos);
                        }
                        this.temp_whichone = -1;
                        refreshTeletextSubtitle();
                    }
                }
                return true;
            case DtvBaseActivity.KEYCODE_MEDIA_PREVIOUS /*88*/:
                Log.e("LEE ", "recordListCount := " + this.recordListCount);
                if (this.ll_recordListUI.getVisibility() == 0) {
                    return true;
                }
                if (this.recordListCount == 1) {
                    Log.e("LEE", "no previous item");
                    return true;
                }
                this.bHandlePreNext = true;
                if (this.record_list_item_pos > 0) {
                    this.record_list_item_pos--;
                } else {
                    this.record_list_item_pos = this.recordListCount - 1;
                }
                Log.e("LEE", "record_list_item_pos  := " + this.record_list_item_pos);
                if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecord) == 0) {
                    if (this.pvrRecord.is_locked) {
                        CheckNextPreLockAndShowPasswd("1");
                    } else {
                        if (this.pswdDig != null) {
                            this.pswdDig.dismiss();
                            this.pswdDig = null;
                        }
                        if (MW.pvr_playback_start(this.record_list_item_pos) == 0) {
                            ShowPlayInfoView();
                        } else {
                            Log.e("Lee", "KEYCODE_MEDIA_PREVIOUS pvr_playback_start failed : record_list_item_pos = " + this.record_list_item_pos);
                        }
                        this.temp_whichone = -1;
                        refreshTeletextSubtitle();
                    }
                }
                return true;
            case DtvBaseActivity.KEYCODE_MEDIA_REWIND /*89*/:
                Log.e("LEE", " fbackforward  rewind :  " + this.fbackforward);
                if (this.fbackforward == 1) {
                    this.speed = 0;
                    MW.pvr_playback_fast_forward(this.speed);
                    return true;
                }
                this.fbackforward = 2;
                if (this.speed == 0) {
                    this.speed = 2;
                } else {
                    this.speed *= 2;
                }
                if (this.speed > 32) {
                    this.speed = 0;
                } else {
                    this.play_speed.setText("X" + this.speed);
                    this.play_status_img.setImageResource(R.drawable.osd_rewind_hl);
                }
                MW.pvr_playback_fast_backward(this.speed);
                return true;
            case DtvBaseActivity.KEYCODE_MEDIA_FAST_FORWARD /*90*/:
                Log.e("LEE", " fbackforward  forward : " + this.fbackforward);
                if (this.fbackforward == 2) {
                    this.speed = 0;
                    MW.pvr_playback_fast_forward(this.speed);
                    return true;
                }
                this.fbackforward = 1;
                if (this.speed == 0) {
                    this.speed = 2;
                } else {
                    this.speed *= 2;
                }
                if (this.speed > 32) {
                    this.speed = 0;
                } else {
                    this.play_speed.setText("X" + this.speed);
                    this.play_status_img.setImageResource(R.drawable.osd_forward_hl);
                }
                MW.pvr_playback_fast_forward(this.speed);
                return true;
            case DtvBaseActivity.KEYCODE_INFO /*185*/:
                if (this.playback_status == 6) {
                    if (this.RL_RecordPlay.getVisibility() != 0) {
                        ShowPlayInfoView();
                    } else {
                        HidePlayInfoView(0);
                    }
                }
                return true;
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                if (this.ll_recordListUI.getVisibility() == 0) {
                    return true;
                }
                if (this.rl_loading.getVisibility() == 0) {
                    return true;
                }
                if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecordInfo) == 0 && this.pvrRecordInfo.teletext_info.length > 0) {
                    if (this.pvrRecordInfo.teletext_info.length == 1) {
                        startTeletextSubtitle(true, 0, this.pvrRecordInfo);
                    } else {
                        TeletextSubtitleInfoDialog ttxInfoDia = new TeletextSubtitleInfoDialog(this, R.style.MyDialog, true);
                        ttxInfoDia.show();
                        lp = ttxInfoDia.getWindow().getAttributes();
                        lp.dimAmount = 0.0f;
                        ttxInfoDia.getWindow().setAttributes(lp);
                        ttxInfoDia.getWindow().addFlags(2);
                        if (this.inTeletextMode) {
                            stopTeletextSubtitle();
                        }
                    }
                }
                return true;
            case DtvBaseActivity.KEYCODE_AUDIO /*2006*/:
                if (this.ll_recordListUI.getVisibility() == 0) {
                    return true;
                }
                if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecordInfo) == 0) {
                    System.out.println("pvrRecord.audio_index    :  " + this.pvrRecord.audio_index);
                    AudioInfoDialog audioInfoDia = new AudioInfoDialog(this, R.style.MyDialog, this.record_list_item_pos, this.recordListCount);
                    audioInfoDia.show();
                    lp = audioInfoDia.getWindow().getAttributes();
                    lp.dimAmount = 0.0f;
                    audioInfoDia.getWindow().setAttributes(lp);
                    audioInfoDia.getWindow().addFlags(2);
                    if (this.inTeletextMode) {
                        stopTeletextSubtitle();
                    }
                }
                return true;
            case DtvBaseActivity.KEYCODE_SUBTITLE /*2012*/:
                if (this.ll_recordListUI.getVisibility() == 0) {
                    return true;
                }
                if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecordInfo) == 0 && this.pvrRecordInfo.subtitle_info.length > 0) {
                    TeletextSubtitleInfoDialog subInfoDia = new TeletextSubtitleInfoDialog(this, R.style.MyDialog, false);
                    subInfoDia.show();
                    lp = subInfoDia.getWindow().getAttributes();
                    lp.dimAmount = 0.0f;
                    subInfoDia.getWindow().setAttributes(lp);
                    subInfoDia.getWindow().addFlags(2);
                    if (this.inTeletextMode) {
                        stopTeletextSubtitle();
                    }
                }
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void refreshStorageDeviceList() {
        int i = 0;
        initPvrSelStatus();
        if (this.storageDeviceInfo != null) {
            this.storageDeviceCount = this.storageDeviceInfo.getDeviceCount();
            if (this.storageDeviceCount > 0) {
                Log.e("RecordListActivity", "have device count : " + this.storageDeviceCount);
                while (i < this.storageDeviceCount) {
                    DeviceItem deviceItem = this.storageDeviceInfo.getDeviceItem(i);
                    if (deviceItem != null) {
                        Log.e("RecordListActivity", "device path: " + deviceItem.Path + " device format: " + deviceItem.format + "  name: " + deviceItem.VolumeName + " spare : " + deviceItem.spare + " total: " + deviceItem.total);
                    } else {
                        Log.e("RecordListActivity", "get device failed : " + i);
                    }
                    i++;
                }
                Log.e("RecordListActivity", "storageDeviceIndex \uff1a" + this.storageDeviceIndex + "storageDeviceCount : " + this.storageDeviceCount);
            } else {
                Log.e("RecordListActivity", "not device");
            }
        } else {
            this.storageDeviceCount = 0;
        }
        this.storageAdapter.notifyDataSetChanged();
        this.storageDeviceIndex = this.storage_list_item_pos;
        this.storage_list.requestFocus();
        this.storage_list.setCustomSelection(this.storageDeviceIndex);
        if (this.storage_list.getSelectedView() != null) {
            this.storage_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
        }
        refreshRecordList();
    }

    private void refreshRecordList() {
        int i = 0;
        DeviceItem deviceItem = this.storageDeviceInfo.getDeviceItem(this.storageDeviceIndex);
        if (this.record_list.getChildAt(this.record_list_item_pos) != null) {
            this.record_list.getChildAt(this.record_list_item_pos).setBackgroundResource(R.drawable.listview_item_bg_selector);
        }
        this.record_list_item_pos = 0;
        if (deviceItem == null) {
            this.recordListCount = 0;
        } else if (MW.pvr_record_init_record_list(deviceItem.Path) == 0) {
            this.recordListCount = MW.pvr_record_get_record_count();
            if (this.recordListCount > 0) {
                Log.e("RecordListActivity", "have record count : " + this.recordListCount);
                while (i < this.recordListCount) {
                    if (MW.pvr_record_get_record_info(i, this.pvrRecord) == 0) {
                        Log.e("RecordListActivity", "record name : " + this.pvrRecord.service_name + " **  duration : " + this.pvrRecord.duration + " **  file size : " + (this.pvrRecord.file_size / 1048576) + " MB");
                    } else {
                        Log.e("RecordListActivity", "get record failed : " + i);
                    }
                    i++;
                }
            } else {
                Log.e("RecordListActivity", "not record");
            }
        } else {
            Log.e("RecordListActivity", "init record list failed");
        }
        this.recordListAdapter.notifyDataSetChanged();
    }

    private void initVideoView() {
        this.vView = (VideoView) findViewById(R.id.VideoView);
        this.vView.getHolder().addCallback(this.mSHCallback);
        SETTINGS.videoViewSetFormat(this, this.vView);
    }

    private void initView() {
        this.ll_recordListUI = (LinearLayout) findViewById(R.id.LinearLayout_RecordListUI);
        this.left_img = (ImageView) findViewById(R.id.left_img);
        this.right_img = (ImageView) findViewById(R.id.right_img);
        this.storage_list = (CustomListView) findViewById(R.id.storage_list);
        this.record_list = (CustomListView) findViewById(R.id.pvr_program_list);
    }

    private void Hidemanline() {
        this.left_img.setVisibility(View.GONE);
        this.right_img.setVisibility(View.GONE);
    }

    private void Showmanline() {
        this.left_img.setVisibility(View.VISIBLE);
        this.right_img.setVisibility(View.VISIBLE);
    }

    private void initData() {
        this.storageAdapter = new StorageAdapter(this);
        this.storage_list.setAdapter(this.storageAdapter);
        this.storage_list.setVisibleItemCount(10);
        this.storage_list.setCustomScrollListener(new C02176());
        this.storage_list.setOnItemSelectedListener(new C02187());
        this.recordListAdapter = new RecordListAdapter(this);
        this.record_list.setAdapter(this.recordListAdapter);
        this.record_list.setVisibleItemCount(11);
        this.record_list.setCustomScrollListener(new C02198());
        this.record_list.setOnItemSelectedListener(new C02209());
        initMessageHandle();
    }

    private void initListener() {
        this.storage_list.setCustomKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0) {
                    switch (keyCode) {
                        case MW.RET_INVALID_TYPE /*23*/:
                        case DtvBaseActivity.KEYCODE_DEL /*67*/:
                            return true;
                        default:
                            break;
                    }
                }
                switch (keyCode) {
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    case MW.RET_INVALID_TP_INDEX /*20*/:
                    case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                    case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                        if (RecordListActivity.this.storage_list != null && RecordListActivity.this.storage_list.getCount() > 1) {
                            if (RecordListActivity.this.record_list.getSelectedView() != null) {
                                RecordListActivity.this.record_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                            }
                            RecordListActivity.this.record_list_item_pos = 0;
                            RecordListActivity.this.record_list.setCustomSelection(RecordListActivity.this.record_list_item_pos);
                        }
                        int currentPos = RecordListActivity.this.storage_list.getSelectedItemPosition();
                        RecordListActivity.this.storage_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        break;
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    case MW.RET_MEMORY_ERROR /*22*/:
                    case MW.RET_INVALID_TYPE /*23*/:
                        if (RecordListActivity.this.record_list.getCount() <= 0) {
                            return true;
                        }
                        RecordListActivity.this.record_list.requestFocus();
                        RecordListActivity.this.recordListAdapter.notifyDataSetChanged();
                        RecordListActivity.this.record_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                        if (RecordListActivity.this.record_list.getSelectedItemPosition() < 0) {
                            return true;
                        }
                        RecordListActivity.this.storage_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                        RecordListActivity.this.record_list.requestFocus();
                        RecordListActivity.this.record_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                        return true;
                }
                return false;
            }
        });
        this.record_list.setCustomKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (event.getAction() != 0) {
                    switch (keyCode) {
                        case MW.RET_INVALID_TYPE /*23*/:
                        case DtvBaseActivity.KEYCODE_DEL /*67*/:
                            return true;
                        case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                            RecordListActivity.this.PvrFile_Lock();
                            return true;
                        default:
                            break;
                    }
                }
                switch (keyCode) {
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    case MW.RET_MEMORY_ERROR /*22*/:
                        RecordListActivity.this.storage_list.requestFocus();
                        RecordListActivity.this.storageAdapter.notifyDataSetChanged();
                        RecordListActivity.this.storage_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        if (RecordListActivity.this.record_list.getSelectedItemPosition() < 0) {
                            return true;
                        }
                        RecordListActivity.this.record_list.getSelectedView().setBackgroundResource(R.drawable.list_446_49_sele_expired);
                        return true;
                    case MW.RET_INVALID_TYPE /*23*/:
                        if (MW.pvr_record_get_record_info(RecordListActivity.this.record_list_item_pos, RecordListActivity.this.pvrRecord) != 0) {
                            return true;
                        }
                        if (RecordListActivity.this.pvrRecord.is_locked) {
                            if (!SETTINGS.bPass) {
                                RecordListActivity.this.CheckChannelLockAndShowPasswd("3");
                                return true;
                            } else if (MW.pvr_playback_start(RecordListActivity.this.record_list_item_pos) != 0) {
                                return true;
                            } else {
                                RecordListActivity.this.ll_recordListUI.setVisibility(View.GONE);
                                RecordListActivity.this.Hidemanline();
                                RecordListActivity.this.ShowPlayInfoView();
                                RecordListActivity.this.refreshTeletextSubtitle();
                                return true;
                            }
                        } else if (MW.pvr_playback_start(RecordListActivity.this.record_list_item_pos) != 0) {
                            return true;
                        } else {
                            RecordListActivity.this.ll_recordListUI.setVisibility(View.GONE);
                            RecordListActivity.this.Hidemanline();
                            RecordListActivity.this.ShowPlayInfoView();
                            RecordListActivity.this.refreshTeletextSubtitle();
                            return true;
                        }
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        RecordListActivity.this.PvrFile_Rename();
                        return true;
                    case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                        RecordListActivity.this.PvrFile_Delete();
                        return true;
                    case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                        if (RecordListActivity.this.record_list.getVisibility() != 0) {
                            return true;
                        }
                        RecordListActivity.this.RefreshPvrSelectImage();
                        return true;
                }
                return false;
            }
        });
    }

    private void PvrFile_Delete() {
        getSelPvrFileInfo();
        int[] iArr = new int[this.SelPvrList.size()];
        for (int i = 0; i < this.SelPvrList.size(); i++) {
            iArr[i] = ((Integer) this.SelPvrList.get(i)).intValue();
            System.out.println("delete select_pvr_index_list: " + iArr[i]);
            if (MW.pvr_record_get_record_info(iArr[i], this.pvrRecord) == 0) {
                if (this.pvrRecord.is_locked) {
                    this.hasLockedFile = true;
                    break;
                }
                this.hasLockedFile = false;
            }
        }
        Dialog deleteDia;
        LayoutParams attributes;
        if (!this.hasLockedFile) {
            deleteDia = new DeleteDia(this, R.style.MyDialog);
            deleteDia.show();
            attributes = deleteDia.getWindow().getAttributes();
            attributes.dimAmount = 0.0f;
            deleteDia.getWindow().setAttributes(attributes);
            deleteDia.getWindow().addFlags(2);
        } else if (SETTINGS.bPass) {
            deleteDia = new DeleteDia(this, R.style.MyDialog);
            deleteDia.show();
            attributes = deleteDia.getWindow().getAttributes();
            attributes.dimAmount = 0.0f;
            deleteDia.getWindow().setAttributes(attributes);
            deleteDia.getWindow().addFlags(2);
        } else {
            CheckChannelLockAndShowPasswd("5");
        }
    }

    private void PvrFile_Rename() {
        if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecord) != 0) {
            return;
        }
        Dialog editDialog;
        LayoutParams attributes;
        if (SETTINGS.bPass) {
            editDialog = new EditDialog(this, R.style.MyDialog);
            editDialog.setContentView(R.layout.pvr_edit_custom_dia);
            editDialog.show();
            attributes = editDialog.getWindow().getAttributes();
            attributes.dimAmount = 0.0f;
            editDialog.getWindow().setAttributes(attributes);
            editDialog.getWindow().addFlags(2);
        } else if (this.pvrRecord.is_locked) {
            CheckChannelLockAndShowPasswd("4");
        } else {
            editDialog = new EditDialog(this, R.style.MyDialog);
            editDialog.setContentView(R.layout.pvr_edit_custom_dia);
            editDialog.show();
            attributes = editDialog.getWindow().getAttributes();
            attributes.dimAmount = 0.0f;
            editDialog.getWindow().setAttributes(attributes);
            editDialog.getWindow().addFlags(2);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void PvrFile_Lock() {
        /*
        r4 = this;
        r3 = 0;
        r2 = 1;
        r0 = r4.record_list_item_pos;
        r1 = r4.pvrRecord;
        r0 = th.dtv.MW.pvr_record_get_record_info(r0, r1);
        if (r0 != 0) goto L_0x0025;
    L_0x000c:
        r0 = th.dtv.SETTINGS.bPass;
        if (r0 != r2) goto L_0x0036;
    L_0x0010:
        r0 = r4.pvrRecord;
        r0 = r0.is_locked;
        if (r0 != 0) goto L_0x002d;
    L_0x0016:
        r0 = r4.record_list_item_pos;
        r0 = th.dtv.MW.pvr_record_set_record_lock(r0, r2);
        if (r0 != 0) goto L_0x001e;
    L_0x001e:
        th.dtv.SETTINGS.bPass = r3;
    L_0x0020:
        r0 = r4.recordListAdapter;
        r0.notifyDataSetChanged();
    L_0x0025:
        r0 = "LEE";
        r1 = "1111111";
        android.util.Log.e(r0, r1);
        return;
    L_0x002d:
        r0 = r4.record_list_item_pos;
        r0 = th.dtv.MW.pvr_record_set_record_lock(r0, r3);
        if (r0 != 0) goto L_0x001e;
    L_0x0035:
        goto L_0x001e;
    L_0x0036:
        r0 = r4.pvrRecord;
        r0 = r0.is_locked;
        if (r0 != 0) goto L_0x0045;
    L_0x003c:
        r0 = r4.record_list_item_pos;
        r0 = th.dtv.MW.pvr_record_set_record_lock(r0, r2);
        if (r0 != 0) goto L_0x0020;
    L_0x0044:
        goto L_0x0020;
    L_0x0045:
        r0 = "1";
        r4.CheckChannelLockAndShowPasswd(r0);
        goto L_0x0020;
        */
        throw new UnsupportedOperationException("Method not decompiled: th.dtv.activity.RecordListActivity.PvrFile_Lock():void");
    }

    private void CheckChannelLockAndShowPasswd(String str) {
        System.out.println("PVR called pos :" + str);
        if (this.pswdDig != null) {
            this.pswdDig.dismiss();
            this.pswdDig = null;
        }
    }

    private void CheckNextPreLockAndShowPasswd(String str) {
        System.out.println("PVR called pos :" + str);
        if (this.pswdDig != null) {
            this.pswdDig.dismiss();
            this.pswdDig = null;
        }
    }

    private void initttx_subView() {
        this.rl_loading = (RelativeLayout) findViewById(R.id.RelativeLayout_Loading);
        this.ttx_sub = (TeletextSubtitleView) findViewById(R.id.TeletextSubtitleView);
        this.ttx_sub.setLayerType(1, null);
        this.et_ttxPageNumber = (EditText) findViewById(R.id.EditText_TeletextPageNumber);
        this.et_ttxPageNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0) {
                    switch (keyCode) {
                        case MW.RET_INVALID_TYPE /*23*/:
                        case DtvBaseActivity.KEYCODE_DEL /*67*/:
                            return true;
                        default:
                            break;
                    }
                }
                EditText et = (EditText) v;
                int value;
                switch (keyCode) {
                    case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                    case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                    case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                    case MW.RET_TP_EXIST /*10*/:
                    case MW.RET_INVALID_SAT /*11*/:
                    case MW.RET_INVALID_AREA /*12*/:
                    case MW.RET_INVALID_TP /*13*/:
                    case MW.RET_BOOK_EVENT_INVALID /*14*/:
                    case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                        RecordListActivity.this.bRefresh = false;
                        value = 0;
                        if (et.hasSelection()) {
                            value = keyCode - 7;
                        } else {
                            if (et.getText().toString().length() > 0) {
                                value = Integer.valueOf(et.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100000) {
                                value %= 100000;
                            }
                        }
                        et.setText(Integer.toString(value));
                        et.setSelection(et.length());
                        return true;
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                        RecordListActivity.this.bRefresh = true;
                        if (RecordListActivity.this.et_ttxPageNumber != null) {
                            RecordListActivity.this.et_ttxPageNumber.selectAll();
                        }
                        if (MW.teletext_goto_next_page() != 0) {
                            return true;
                        }
                        RecordListActivity.this.ShowLoadingIcon();
                        return true;
                    case MW.RET_INVALID_TP_INDEX /*20*/:
                        RecordListActivity.this.bRefresh = true;
                        if (RecordListActivity.this.et_ttxPageNumber != null) {
                            RecordListActivity.this.et_ttxPageNumber.selectAll();
                        }
                        if (MW.teletext_goto_prev_page() != 0) {
                            return true;
                        }
                        RecordListActivity.this.ShowLoadingIcon();
                        return true;
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        RecordListActivity.this.bRefresh = false;
                        if (et.hasSelection()) {
                            et.setSelection(et.length());
                            return true;
                        }
                        value = 0;
                        if (et.getText().toString().length() > 0) {
                            value = Integer.valueOf(et.getText().toString()).intValue();
                        }
                        et.setText(Integer.toString(value / 10));
                        et.setSelection(et.length());
                        return true;
                    case MW.RET_MEMORY_ERROR /*22*/:
                        RecordListActivity.this.bRefresh = false;
                        if (!et.hasSelection()) {
                            return true;
                        }
                        et.setSelection(et.length());
                        return true;
                    case MW.RET_INVALID_TYPE /*23*/:
                        RecordListActivity.this.bRefresh = true;
                        if (et.hasSelection()) {
                            et.setSelection(et.length());
                            return true;
                        }
                        value = 0;
                        if (et.getText().toString().length() > 0) {
                            value = Integer.valueOf(et.getText().toString()).intValue();
                        }
                        et.selectAll();
                        if (MW.teletext_goto_page(value) != 0) {
                            return true;
                        }
                        RecordListActivity.this.ShowLoadingIcon();
                        return true;
                    case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                    case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                        RecordListActivity.this.bRefresh = true;
                        if (RecordListActivity.this.et_ttxPageNumber != null) {
                            RecordListActivity.this.et_ttxPageNumber.selectAll();
                        }
                        if (keyCode == 92) {
                            if (MW.teletext_goto_offset_page(100) != 0) {
                                return true;
                            }
                            RecordListActivity.this.ShowLoadingIcon();
                            return true;
                        } else if (MW.teletext_goto_offset_page(-100) != 0) {
                            return true;
                        } else {
                            RecordListActivity.this.ShowLoadingIcon();
                            return true;
                        }
                }
                return false;
            }
        });
    }

    void startTeletextSubtitle(boolean bTeletext, int ttxSubIndex, pvr_record pvrRecordInfo) {
        if (bTeletext) {
            this.inTeletextMode = true;
            if (this.rl_loading.getVisibility() != 0) {
                this.rl_loading.setVisibility(View.VISIBLE);
            }
        } else {
            this.inSubtitleMode = true;
        }
        this.ttx_sub.start(bTeletext, ttxSubIndex, pvrRecordInfo);
    }

    void refreshTeletextSubtitle() {
        stopTeletextSubtitle();
        if (this.temp_whichone != 0) {
            if (!SETTINGS.get_subtitle_default_on()) {
                this.temp_whichone = 0;
            } else if (MW.pvr_record_get_record_info(this.record_list_item_pos, this.pvrRecordInfo) == 0 && this.pvrRecordInfo.subtitle_info.length > 0) {
                if (this.temp_whichone == -1) {
                    int whichone = 0;
                    int i = 0;
                    while (i < this.pvrRecordInfo.subtitle_info.length) {
                        if (!this.pvrRecordInfo.subtitle_info[i].ISO_639_language_code.equals(SETTINGS.get_subtitle_language())) {
                            if (this.pvrRecordInfo.subtitle_info[i].subtitling_type != 2 && this.pvrRecordInfo.subtitle_info[i].subtitling_type != 5) {
                                whichone = i + 1;
                                break;
                            } else {
                                whichone = 1;
                                i++;
                            }
                        } else if (this.pvrRecordInfo.subtitle_info[i].subtitling_type == 2 || this.pvrRecordInfo.subtitle_info[i].subtitling_type == 5) {
                            int j = i;
                            while (j < this.pvrRecordInfo.subtitle_info.length) {
                                if (this.pvrRecordInfo.subtitle_info[j].subtitling_type != 2 && this.pvrRecordInfo.subtitle_info[j].subtitling_type != 5) {
                                    whichone = j + 1;
                                    break;
                                } else {
                                    whichone = i + 1;
                                    j++;
                                }
                            }
                        } else {
                            whichone = i + 1;
                        }
                    }
                    startTeletextSubtitle(false, whichone - 1, this.pvrRecordInfo);
                    return;
                }
                startTeletextSubtitle(false, this.temp_whichone - 1, this.pvrRecordInfo);
            }
        }
    }

    void stopTeletextSubtitle() {
        this.inTeletextMode = false;
        this.bTeletextShowed = false;
        this.inSubtitleMode = false;
        this.ttx_sub.stop();
        if (this.rl_loading.getVisibility() != 8) {
            this.rl_loading.setVisibility(View.GONE);
        }
        if (this.et_ttxPageNumber.getVisibility() != 8) {
            this.et_ttxPageNumber.setVisibility(View.GONE);
        }
    }

    private boolean onTeletextKeyDown(int keyCode) {
        switch (keyCode) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                refreshTeletextSubtitle();
                return true;
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                if (MW.teletext_goto_next_page() != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case MW.RET_INVALID_TP_INDEX /*20*/:
                if (MW.teletext_goto_prev_page() != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case MW.RET_INVALID_TYPE /*23*/:
            case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                return true;
            case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                if (MW.teletext_goto_color_link(1) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                if (MW.teletext_goto_color_link(3) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                if (MW.teletext_goto_color_link(2) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_RECALL /*666*/:
                if (MW.teletext_goto_home() != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                if (MW.teletext_goto_color_link(0) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            default:
                return false;
        }
    }

    void ShowToastInformation(String str, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
        ((TextView) linearLayout.findViewById(R.id.toast_text)).setText(str);
        Toast toast = new Toast(this);
        if (i == 0) {
            toast.setDuration(0);
        } else if (i == 1) {
            toast.setDuration(1);
        }
        toast.setView(linearLayout);
        toast.setGravity(17, 0, 0);
        toast.show();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        DtvApp.getInstance().addActivity(this);
        Intent intent = new Intent();
        intent.setAction("com.android.music.musicservicecommand.pause");
        intent.putExtra("command", "stop");
        setContentView(R.layout.record_list_activity);
        initView();
        initVideoView();
        initttx_subView();
        initPlayView();
        initData();
        initListener();
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.bPass = false;
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (this.pswdDig != null) {
            this.pswdDig.dismiss();
            this.pswdDig = null;
        }
        SETTINGS.send_led_msg("PVR ");
        enableMwMessageCallback(this.mwMsgHandler);
        if (this.storageDeviceInfo == null) {
            this.storageDeviceInfo = new StorageDevice(this);
        }
        this.storageDevicePlugReceiver = new StorageDevicePlugReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_MOUNTED");
        intentFilter.addAction("android.intent.action.DEVICE_REMOVED");
        registerReceiver(this.storageDevicePlugReceiver, intentFilter);
        refreshStorageDeviceList();
        this.ll_recordListUI.setVisibility(View.VISIBLE);
        Showmanline();
        this.storage_list.requestFocus();
        SETTINGS.set_screen_mode();
    }

    protected void onPause() {
        super.onPause();
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (this.dia_currentDialog != null) {
            this.dia_currentDialog.dismiss();
        }
        stopTeletextSubtitle();
        if (this.initflag) {
            MW.pvr_playback_stop();
        }
        this.storageDeviceInfo.finish(this);
        this.storageDeviceInfo = null;
        if (this.storageDevicePlugReceiver != null) {
            unregisterReceiver(this.storageDevicePlugReceiver);
            this.storageDevicePlugReceiver = null;
        }
        MW.pvr_record_exit_record_list();
        enableMwMessageCallback(null);
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        super.onDestroy();
    }
}
