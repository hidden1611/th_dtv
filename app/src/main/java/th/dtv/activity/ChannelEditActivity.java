package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import java.util.ArrayList;
import java.util.List;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_t_area;
import th.dtv.mw_data.region_info;
import th.dtv.mw_data.service;
import th.dtv.util.HongKongDTMBHD;

public class ChannelEditActivity extends DtvBaseActivity {
    private dvb_t_area areaInfo;
    private boolean bForceSaveData;
    private TextView channel_type;
    private ChannelEditAdapter cheditAdapter;
    private CustomListView chedit_listview;
    private RelativeLayout chedit_rl;
    private int cheditlist_item_pos;
    private int current_service_index;
    private int current_service_type;
    private Dialog dia_currentDialog;
    private boolean fsecondfuntion;
    private int mFirstVisibleItem;
    Callback mSHCallback;
    private int mVisibleItemCount;
    private Handler mwMsgHandler;
    private LinearLayout pm_help_tips;
    private LinearLayout pm_help_tips_two;
    private TextView pm_sat;
    private region_info regionInfo;
    private dvb_s_sat satInfo;
    private service serviceInfo;

    /* renamed from: th.dtv.activity.ChannelEditActivity.1 */
    class C01091 implements Callback {
        C01091() {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("ChannelEditActivity", "surfaceChanged");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("ChannelEditActivity", "surfaceCreated");
            try {
                initSurface(holder);
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("ChannelEditActivity", "surfaceDestroyed");
        }

        private void initSurface(SurfaceHolder h) {
            Canvas c = null;
            try {
                Log.d("ChannelEditActivity", "initSurface");
                c = h.lockCanvas();
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            } catch (Throwable th) {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /* renamed from: th.dtv.activity.ChannelEditActivity.2 */
    class C01102 implements OnScrollListener {
        C01102() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            ChannelEditActivity.this.mFirstVisibleItem = firstVisibleItem;
            ChannelEditActivity.this.mVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.ChannelEditActivity.3 */
    class C01113 implements OnItemSelectedListener {
        C01113() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            ChannelEditActivity.this.cheditlist_item_pos = pos;
            if (MW.db_get_service_info(ChannelEditActivity.this.current_service_type, pos, ChannelEditActivity.this.serviceInfo) == 0) {
                MW.ts_player_play(ChannelEditActivity.this.current_service_type, pos, 50, true);
                SETTINGS.set_led_channel(ChannelEditActivity.this.serviceInfo.channel_number_display);
                ChannelEditActivity.this.current_service_index = ChannelEditActivity.this.serviceInfo.service_index;
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class ChannelEditAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mcontext;

        class ViewHolder {
            TextView chedit_chname;
            ImageView chedit_fav;
            ImageView chedit_hdsd_img;
            ImageView chedit_lock;
            TextView chedit_no;
            ImageView chedit_scramble_img;
            ImageView chedit_selected;
            ImageView chedit_skip;

            ViewHolder() {
            }
        }

        public ChannelEditAdapter(Context context) {
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_get_service_count(ChannelEditActivity.this.current_service_type);
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.channeledit_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.chedit_no = (TextView) convertView.findViewById(R.id.chedit_no);
                localViewHolder.chedit_scramble_img = (ImageView) convertView.findViewById(R.id.chedit_scramble_img);
                localViewHolder.chedit_hdsd_img = (ImageView) convertView.findViewById(R.id.chedit_hdsd_img);
                localViewHolder.chedit_chname = (TextView) convertView.findViewById(R.id.chedit_chname);
                localViewHolder.chedit_selected = (ImageView) convertView.findViewById(R.id.chedit_selected);
                localViewHolder.chedit_lock = (ImageView) convertView.findViewById(R.id.chedit_lock);
                localViewHolder.chedit_skip = (ImageView) convertView.findViewById(R.id.chedit_skip);
                localViewHolder.chedit_fav = (ImageView) convertView.findViewById(R.id.chedit_fav);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.db_get_service_info(ChannelEditActivity.this.current_service_type, position, ChannelEditActivity.this.serviceInfo) == 0) {
                localViewHolder.chedit_no.setText(Integer.toString(ChannelEditActivity.this.serviceInfo.channel_number_display));
                localViewHolder.chedit_chname.setText(ChannelEditActivity.this.serviceInfo.service_name);
                if (ChannelEditActivity.this.serviceInfo.is_scrambled) {
                    localViewHolder.chedit_scramble_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_scramble_img.setVisibility(View.INVISIBLE);
                }
                if (SETTINGS.bHongKongDTMB) {
                    if (HongKongDTMBHD.isHongKongDTMBHDChannel(ChannelEditActivity.this.serviceInfo.video_pid, ChannelEditActivity.this.serviceInfo.audio_info[0].audio_pid) || ChannelEditActivity.this.serviceInfo.is_hd) {
                        localViewHolder.chedit_hdsd_img.setVisibility(View.VISIBLE);
                    } else {
                        localViewHolder.chedit_hdsd_img.setVisibility(View.INVISIBLE);
                    }
                } else if (ChannelEditActivity.this.serviceInfo.is_hd) {
                    localViewHolder.chedit_hdsd_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_hdsd_img.setVisibility(View.INVISIBLE);
                }
                if (ChannelEditActivity.this.serviceInfo.is_locked) {
                    localViewHolder.chedit_lock.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_lock.setVisibility(View.INVISIBLE);
                }
                if (ChannelEditActivity.this.serviceInfo.is_skipped) {
                    localViewHolder.chedit_skip.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_skip.setVisibility(View.INVISIBLE);
                }
                if (MW.channel_edit_check_selection(position)) {
                    localViewHolder.chedit_selected.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_selected.setVisibility(View.INVISIBLE);
                }
                if (MW.channel_edit_get_favorite(position) != 0) {
                    localViewHolder.chedit_fav.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_fav.setVisibility(View.INVISIBLE);
                }
                if (MW.channel_edit_check_lock(position)) {
                    localViewHolder.chedit_lock.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_lock.setVisibility(View.INVISIBLE);
                }
                if (MW.channel_edit_check_skip(position)) {
                    localViewHolder.chedit_skip.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.chedit_skip.setVisibility(View.INVISIBLE);
                }
            }
            return convertView;
        }
    }

    public class ChannelSortDialog extends Dialog {
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;
        private ImageView operate_img;
        private TextView operate_tips;

        /* renamed from: th.dtv.activity.ChannelEditActivity.ChannelSortDialog.1 */
        class C01121 implements OnItemClickListener {
            C01121() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            ChannelEditActivity.this.dia_currentDialog = this;
            ((TextView) findViewById(R.id.TextView_Title)).setText(R.string.Sort);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.operate_tips = (TextView) findViewById(R.id.operate_tips);
            this.operate_tips.setVisibility(View.VISIBLE);
            this.operate_tips.setText(R.string.sort_tips);
            this.operate_img = (ImageView) findViewById(R.id.operate_img);
            this.operate_img.setVisibility(View.VISIBLE);
            this.operate_img.setImageResource(R.drawable.infomessage_blue);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.multiple_selection_list_item, null);
            List<String> items = new ArrayList();
            items.clear();
            items.add(ChannelEditActivity.this.getString(R.string.AtoZ));
            items.add(ChannelEditActivity.this.getString(R.string.Satellite));
            items.add(ChannelEditActivity.this.getString(R.string.Transponder));
            items.add(ChannelEditActivity.this.getString(R.string.CAS));
            this.mAdapter = new ArrayAdapter(ChannelEditActivity.this, R.layout.multiple_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(2);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(4);
            this.mListView.setItemChecked(0, false);
            this.mListView.setItemChecked(1, false);
            this.mListView.setItemChecked(2, false);
            this.mListView.setItemChecked(3, false);
            this.mListView.setOnItemClickListener(new C01121());
        }

        protected void onStop() {
            super.onStop();
        }

        public ChannelSortDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.operate_tips = null;
            this.operate_img = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    int serviceSortType = 0;
                    if (this.mListView.isItemChecked(0)) {
                        serviceSortType = 0 | 1;
                    }
                    if (this.mListView.isItemChecked(1)) {
                        serviceSortType |= 2;
                    }
                    if (this.mListView.isItemChecked(2)) {
                        serviceSortType |= 4;
                    }
                    if (this.mListView.isItemChecked(3)) {
                        serviceSortType |= 8;
                    }
                    if (MW.channel_edit_sort(serviceSortType) == 0) {
                        ChannelEditActivity.this.RefreshShowChannelEditAll(false);
                    }
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    public class EditDialog extends Dialog {
        TextView item_no;
        Button mBtnCancel;
        Button mBtnStart;
        TextView mChannelName;
        private Context mContext;
        EditText mFileName;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        /* renamed from: th.dtv.activity.ChannelEditActivity.EditDialog.1 */
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.pvr_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.item_no = (TextView) findViewById(R.id.item_no);
            this.mFileName = (EditText) findViewById(R.id.edittext_pvr_name);
            this.mChannelName = (TextView) findViewById(R.id.filename);
            this.mTitle.setText(ChannelEditActivity.this.getResources().getString(R.string.rename));
            this.mChannelName.setText(R.string.channel_name);
            if (MW.db_get_service_info(ChannelEditActivity.this.current_service_type, ChannelEditActivity.this.cheditlist_item_pos, ChannelEditActivity.this.serviceInfo) == 0) {
                this.item_no.setText((ChannelEditActivity.this.cheditlist_item_pos + 1) + "");
                this.mFileName.setText(ChannelEditActivity.this.serviceInfo.service_name);
                this.mFileName.setSelection(this.mFileName.length());
            } else {
                this.item_no.setText("");
                this.mFileName.setText("");
            }
            this.mFileName.requestFocus();
            this.mFileName.selectAll();
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.item_no = null;
            this.mFileName = null;
            this.mChannelName = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class FavDialog extends Dialog {
        String[] favgroup;
        int favtype;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;
        private ImageView operate_img;
        private TextView operate_tips;

        /* renamed from: th.dtv.activity.ChannelEditActivity.FavDialog.1 */
        class C01151 implements OnItemClickListener {
            C01151() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            int i;
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            ChannelEditActivity.this.dia_currentDialog = this;
            ((TextView) findViewById(R.id.TextView_Title)).setText(R.string.FavType);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.operate_tips = (TextView) findViewById(R.id.operate_tips);
            this.operate_tips.setVisibility(View.VISIBLE);
            this.operate_tips.setText(R.string.fav_tips);
            this.operate_img = (ImageView) findViewById(R.id.operate_img);
            this.operate_img.setVisibility(View.VISIBLE);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.multiple_selection_list_item, null);
            if (MW.channel_edit_has_selection()) {
                this.favtype = 0;
            } else {
                this.favtype = MW.channel_edit_get_favorite(ChannelEditActivity.this.cheditlist_item_pos);
            }
            List<String> items = new ArrayList();
            items.clear();
            this.favgroup = SETTINGS.getFavGroupListArray();
            if (this.favgroup == null || this.favgroup.length <= 0) {
                System.out.println("fav group is null");
            } else {
                for (i = 0; i < this.favgroup.length; i++) {
                    System.out.println("fav group :" + this.favgroup[i]);
                    items.add(this.favgroup[i]);
                }
            }
            this.mAdapter = new ArrayAdapter(ChannelEditActivity.this, R.layout.multiple_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(2);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(5);
            if (this.favgroup != null) {
                for (i = 0; i < this.favgroup.length; i++) {
                    if (((this.favtype >> i) & 1) == 1) {
                        this.mListView.setItemChecked(i, true);
                    } else {
                        this.mListView.setItemChecked(i, false);
                    }
                }
            }
            this.mListView.setOnItemClickListener(new C01151());
        }

        protected void onStop() {
            super.onStop();
        }

        public FavDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.favtype = 0;
            this.favgroup = null;
            this.operate_tips = null;
            this.operate_img = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    this.favtype = 0;
                    for (int i = 0; i < this.favgroup.length; i++) {
                        if (this.mListView.isItemChecked(i)) {
                            this.favtype |= 1 << i;
                        }
                    }
                    if (MW.channel_edit_has_selection()) {
                        MW.channel_edit_set_favorite_selection(this.favtype);
                    } else {
                        MW.channel_edit_set_favorite_single(this.favtype, ChannelEditActivity.this.cheditlist_item_pos);
                    }
                    MW.channel_edit_select_all(false);
                    ChannelEditActivity.this.cheditAdapter.notifyDataSetChanged();
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.RET_INVALID_SAT /*11*/:
                    int bCurrentServiceChanged = msg.arg1;
                    switch (sub_event_type) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            Log.e("ChannelEditActivity", "service name update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            ChannelEditActivity.this.RefreshShowChannelEditAll(false);
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            Log.e("ChannelEditActivity", "service params update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            if (bCurrentServiceChanged == 1) {
                                MW.ts_player_play_current(true, false);
                            }
                        default:
                    }
                default:
            }
        }
    }

    public class RegionSelectDialog extends Dialog {
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.activity.ChannelEditActivity.RegionSelectDialog.1 */
        class C01161 implements OnItemClickListener {
            C01161() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (MW.db_set_service_region_current_pos(position)) {
                    MW.ts_player_play_current(true, false);
                    ChannelEditActivity.this.RefreshShowChannelEditAll(true);
                }
                RegionSelectDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            ChannelEditActivity.this.dia_currentDialog = this;
            ((TextView) findViewById(R.id.TextView_Title)).setText(R.string.Satellite);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            int serviceRegionCount = MW.db_get_service_region_count();
            List<String> items = new ArrayList();
            items.clear();
            items.add(ChannelEditActivity.this.getString(R.string.All));
            for (int i = 0; i < serviceRegionCount; i++) {
                if (MW.db_get_service_region_info(i + 1, ChannelEditActivity.this.regionInfo) == 0) {
                    if (MW.get_system_tuner_config() == 1) {
                        items.add(SETTINGS.get_area_string_by_index(ChannelEditActivity.this, ChannelEditActivity.this.regionInfo.region_index));
                    } else {
                        items.add(ChannelEditActivity.this.regionInfo.region_name);
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(ChannelEditActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setCustomSelection(MW.db_get_service_region_current_pos());
            this.mListView.setItemChecked(MW.db_get_service_region_current_pos(), true);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C01161());
        }

        protected void onStop() {
            super.onStop();
            ChannelEditActivity.this.dia_currentDialog = null;
        }

        public RegionSelectDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_SAT /*55*/:
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    public class SaveDia extends Dialog {
        TextView deldia_title;
        TextView delete_content;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        int mCurrentPos;
        int mEvent;
        private LayoutInflater mInflater;
        View mLayoutView;

        /* renamed from: th.dtv.activity.ChannelEditActivity.SaveDia.1 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.delete_content = (TextView) findViewById(R.id.delete_content);
            if (this.mEvent == 4) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else if (this.mEvent == 6) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else if (this.mEvent == 5) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else if (this.mEvent == 7) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else if (this.mEvent == 8) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else {
                this.mBtnCancel.requestFocus();
            }
        }

        public SaveDia(Context context, int theme, int event, int pos) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.deldia_title = null;
            this.delete_content = null;
            this.mEvent = -1;
            this.mCurrentPos = -1;
            this.mContext = context;
            this.mEvent = event;
            this.mCurrentPos = pos;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class TipsDia extends Dialog {
        TextView TextView_Title;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private int mEvent;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.activity.ChannelEditActivity.TipsDia.1 */
        class C01191 implements OnItemClickListener {
            C01191() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (TipsDia.this.mEvent == 0) {
                    if (position == 0) {
                        MW.channel_edit_set_lock_selection(true);
                    } else {
                        MW.channel_edit_set_lock_selection(false);
                    }
                } else if (TipsDia.this.mEvent == 2) {
                    if (position == 0) {
                        MW.channel_edit_set_skip_selection(true);
                    } else {
                        MW.channel_edit_set_skip_selection(false);
                    }
                } else if (TipsDia.this.mEvent == 1) {
                    if (position == 0) {
                        MW.channel_edit_set_delete_selection(true);
                    } else {
                        MW.channel_edit_set_delete_selection(false);
                    }
                }
                MW.channel_edit_select_all(false);
                ChannelEditActivity.this.cheditAdapter.notifyDataSetChanged();
                TipsDia.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            this.TextView_Title = (TextView) findViewById(R.id.TextView_Title);
            this.TextView_Title.setText(R.string.Change_all);
            List<String> items = new ArrayList();
            items.clear();
            if (this.mEvent == 0) {
                items.add(ChannelEditActivity.this.getResources().getString(R.string.Lock));
                items.add(ChannelEditActivity.this.getResources().getString(R.string.unLock));
            } else if (this.mEvent == 2) {
                items.add(ChannelEditActivity.this.getResources().getString(R.string.Skip));
                items.add(ChannelEditActivity.this.getResources().getString(R.string.unskip));
            } else if (this.mEvent == 1) {
                items.add(ChannelEditActivity.this.getResources().getString(R.string.Delete));
                items.add(ChannelEditActivity.this.getResources().getString(R.string.no_delete));
            } else {
                return;
            }
            this.mAdapter = new ArrayAdapter(ChannelEditActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setItemChecked(0, true);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C01191());
        }

        public TipsDia(Context context, int theme, int event) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.TextView_Title = null;
            this.mEvent = -1;
            this.mContext = context;
            this.mEvent = event;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            return super.onKeyDown(keyCode, event);
        }
    }

    private class listOnKeyListener implements OnKeyListener {
        private listOnKeyListener() {
        }

        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                        ChannelEditActivity.this.DoSelectionAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        if (ChannelEditActivity.this.fsecondfuntion) {
                            ChannelEditActivity.this.DoRenameAction();
                            return true;
                        }
                        ChannelEditActivity.this.DoLockAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                        if (ChannelEditActivity.this.fsecondfuntion) {
                            ChannelEditActivity.this.DoSortAction();
                            return true;
                        }
                        ChannelEditActivity.this.RefreshHelpTips();
                        return true;
                    case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                        if (ChannelEditActivity.this.fsecondfuntion) {
                            ChannelEditActivity.this.DoDeleteAction();
                            return true;
                        }
                        ChannelEditActivity.this.DoSkipAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                        if (ChannelEditActivity.this.fsecondfuntion) {
                            ChannelEditActivity.this.DoMoveAction();
                            return true;
                        }
                        ChannelEditActivity.this.DoFavAction();
                        return true;
                }
            }
            return false;
        }
    }

    public ChannelEditActivity() {
        this.pm_sat = null;
        this.pm_help_tips = null;
        this.pm_help_tips_two = null;
        this.fsecondfuntion = false;
        this.chedit_listview = null;
        this.cheditAdapter = null;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.cheditlist_item_pos = 0;
        this.serviceInfo = new service();
        this.current_service_type = 0;
        this.current_service_index = 0;
        this.regionInfo = new region_info();
        this.satInfo = new dvb_s_sat();
        this.areaInfo = new dvb_t_area();
        this.dia_currentDialog = null;
        this.chedit_rl = null;
        this.channel_type = null;
        this.bForceSaveData = true;
        this.mSHCallback = new C01091();
    }

    private void setEpgBackgroundAlpha(int transparency) {
        this.chedit_rl = (RelativeLayout) findViewById(R.id.chedit_rl);
        this.chedit_rl.getBackground().setAlpha(transparency);
    }

    private void initVideoView() {
        VideoView videoView = (VideoView) findViewById(R.id.chedit_video);
        videoView.setFocusable(false);
        videoView.setClickable(false);
        videoView.getHolder().addCallback(this.mSHCallback);
        SETTINGS.videoViewSetFormat(this, videoView);
    }

    private void initSatellite() {
        this.pm_sat = (TextView) findViewById(R.id.pm_sat);
        RefreshRegionText();
    }

    private void initChannelListView() {
        this.chedit_listview = (CustomListView) findViewById(R.id.chedit_listview);
        this.cheditAdapter = new ChannelEditAdapter(this);
        this.chedit_listview.setAdapter(this.cheditAdapter);
        this.chedit_listview.setChoiceMode(1);
        this.chedit_listview.setCustomSelection(this.mFirstVisibleItem);
        this.chedit_listview.setCustomScrollListener(new C01102());
        this.chedit_listview.setOnItemSelectedListener(new C01113());
        this.chedit_listview.setCustomKeyListener(new listOnKeyListener());
        this.chedit_listview.setVisibleItemCount(10);
        this.channel_type = (TextView) findViewById(R.id.channeltype_txt);
        RefreshShowChannelEditAll(true);
    }

    private void initHelpTipsView() {
        this.pm_help_tips = (LinearLayout) findViewById(R.id.pm_help_tips);
        this.pm_help_tips_two = (LinearLayout) findViewById(R.id.pm_help_tips_two);
        if (MW.get_system_tuner_config() != 1) {
        }
    }

    private void initView() {
        initVideoView();
        initSatellite();
        initHelpTipsView();
        initChannelListView();
        setEpgBackgroundAlpha(220);
    }

    private void initData() {
        MW.db_service_set_in_region_mode();
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
        this.mFirstVisibleItem = this.current_service_index;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        Intent stopMusicIntent = new Intent();
        stopMusicIntent.setAction("com.android.music.musicservicecommand.pause");
        stopMusicIntent.putExtra("command", "stop");
//        sendBroadcastAsUser(stopMusicIntent, UserHandle.ALL);
        setContentView(R.layout.channeledit_activity);
        initMessagehandle();
        initData();
        initView();
    }

    protected void onResume() {
        enableMwMessageCallback(this.mwMsgHandler);
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
        if (MW.ts_player_play_current(true, false) == 0) {
        }
        if (MW.channel_edit_init() == 0) {
            SETTINGS.set_screen_mode();
            super.onResume();
        }
    }

    protected void onPause() {
        if (this.dia_currentDialog != null) {
            this.dia_currentDialog.dismiss();
        }
        if (this.bForceSaveData) {
            if (MW.channel_edit_save_operation() == 0) {
                this.bForceSaveData = false;
            } else {
                this.bForceSaveData = false;
            }
        }
        MW.channel_edit_exit();
        MW.ts_player_stop_play();
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void RefreshRegionText() {
        if (MW.db_get_service_region_current_pos() == 0) {
            this.pm_sat.setText(R.string.All);
        } else if (MW.get_system_tuner_config() == 0 || MW.get_system_tuner_config() == 2) {
            if (MW.db_dvb_s_get_current_sat_info(0, this.satInfo) == 0) {
                this.pm_sat.setText(this.satInfo.sat_name);
            }
        } else if (MW.db_dvb_t_get_current_area_info(this.areaInfo) == 0) {
            this.pm_sat.setText(SETTINGS.get_area_string_by_index(this, this.areaInfo.area_index));
        }
    }

    private void RefreshShowChannelEditAll(boolean bchedit_init) {
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            SETTINGS.set_led_channel(this.serviceInfo.channel_number_display);
            RefreshRegionText();
            if (this.current_service_type == 0) {
                this.channel_type.setText(R.string.TvChannelList);
            } else {
                this.channel_type.setText(R.string.RadioChannelList);
            }
            this.mFirstVisibleItem = this.current_service_index;
            this.chedit_listview.setFocusableInTouchMode(true);
            this.chedit_listview.requestFocus();
            this.chedit_listview.requestFocusFromTouch();
            this.chedit_listview.setCustomSelection(this.mFirstVisibleItem);
            if (bchedit_init) {
                MW.channel_edit_init();
            }
            this.cheditAdapter.notifyDataSetChanged();
        }
    }

    private void SwitchRegions(int keyCode) {
        int position = MW.db_get_service_region_current_pos();
        int serviceRegionCount = MW.db_get_service_region_count();
        switch (keyCode) {
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                position--;
                if (position < 0) {
                    position = serviceRegionCount;
                    break;
                }
                break;
            case MW.RET_MEMORY_ERROR /*22*/:
                position++;
                if (position > serviceRegionCount) {
                    position = 0;
                    break;
                }
                break;
        }
        if (serviceRegionCount > 0 && MW.db_set_service_region_current_pos(position)) {
            MW.ts_player_play_current(true, false);
            RefreshShowChannelEditAll(true);
        }
    }

    private void SelectSatAction() {
        RegionSelectDialog dia = new RegionSelectDialog(this, R.style.MyDialog);
        dia.show();
        LayoutParams lp = dia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dia.getWindow().setAttributes(lp);
        dia.getWindow().addFlags(2);
    }

    private void RefreshHelpTips() {
        if (this.pm_help_tips.getVisibility() == 0) {
            this.fsecondfuntion = true;
            this.pm_help_tips.setVisibility(View.GONE);
            this.pm_help_tips_two.setVisibility(View.VISIBLE);
            return;
        }
        this.fsecondfuntion = false;
        this.pm_help_tips_two.setVisibility(View.GONE);
        this.pm_help_tips.setVisibility(View.VISIBLE);
    }

    private void DoSkipAction() {
        int skipItem = this.cheditlist_item_pos;
        if (MW.channel_edit_has_selection()) {
            ShowTipDialog(2);
        } else if (MW.channel_edit_check_skip(skipItem)) {
            MW.channel_edit_set_skip_single(false, skipItem);
        } else {
            MW.channel_edit_set_skip_single(true, skipItem);
        }
        this.cheditAdapter.notifyDataSetChanged();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void DoMoveAction() {
        /*
        r5 = this;
        r4 = 0;
        r0 = r5.cheditlist_item_pos;
        r1 = java.lang.System.out;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "berore move action : serviceInfo.service_index :";
        r2 = r2.append(r3);
        r3 = r5.serviceInfo;
        r3 = r3.service_index;
        r2 = r2.append(r3);
        r2 = r2.toString();
        r1.println(r2);
        r1 = java.lang.System.out;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "DoMoveAction  des_pos :";
        r2 = r2.append(r3);
        r2 = r2.append(r0);
        r2 = r2.toString();
        r1.println(r2);
        r1 = th.dtv.MW.channel_edit_has_selection();
        if (r1 == 0) goto L_0x0087;
    L_0x003d:
        r1 = th.dtv.MW.channel_edit_move_selection(r0);
        if (r1 != 0) goto L_0x0043;
    L_0x0043:
        th.dtv.MW.channel_edit_select_all(r4);
        r1 = r5.serviceInfo;
        r1 = th.dtv.MW.db_get_current_service_info(r1);
        if (r1 != 0) goto L_0x0065;
    L_0x004e:
        r1 = r5.serviceInfo;
        r1 = r1.service_type;
        r5.current_service_type = r1;
        r1 = r5.serviceInfo;
        r1 = r1.service_index;
        r5.current_service_index = r1;
        r1 = r5.current_service_index;
        r5.mFirstVisibleItem = r1;
        r1 = r5.chedit_listview;
        r2 = r5.mFirstVisibleItem;
        r1.setCustomSelection(r2);
    L_0x0065:
        r1 = java.lang.System.out;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "after move action : serviceInfo.service_index :";
        r2 = r2.append(r3);
        r3 = r5.serviceInfo;
        r3 = r3.service_index;
        r2 = r2.append(r3);
        r2 = r2.toString();
        r1.println(r2);
        r1 = r5.cheditAdapter;
        r1.notifyDataSetChanged();
        return;
    L_0x0087:
        r1 = 2131165360; // 0x7f0700b0 float:1.7944935E38 double:1.05293559E-314;
        th.dtv.SETTINGS.makeText(r5, r1, r4);
        goto L_0x0043;
        */
        throw new UnsupportedOperationException("Method not decompiled: th.dtv.activity.ChannelEditActivity.DoMoveAction():void");
    }

    private void DoSortAction() {
        ChannelSortDialog dia = new ChannelSortDialog(this, R.style.MyDialog);
        dia.show();
        LayoutParams lp = dia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dia.getWindow().setAttributes(lp);
        dia.getWindow().addFlags(2);
    }

    private void ShowDeleteDialog() {
        SaveDia deletedia = new SaveDia(this, R.style.MyDialog, 1, this.cheditlist_item_pos);
        deletedia.show();
        LayoutParams lp = deletedia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        deletedia.getWindow().setAttributes(lp);
        deletedia.getWindow().addFlags(2);
    }

    private void DoDeleteAction() {
        ShowDeleteDialog();
    }

    private void DoRenameAction() {
        Dialog editDialog = new EditDialog(this, R.style.MyDialog);
        editDialog.setContentView(R.layout.pvr_edit_custom_dia);
        editDialog.show();
        LayoutParams lp = editDialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        editDialog.getWindow().setAttributes(lp);
        editDialog.getWindow().addFlags(2);
    }

    private void DoLockAction() {
        int lockItem = this.cheditlist_item_pos;
        if (MW.channel_edit_has_selection()) {
            ShowTipDialog(0);
        } else if (MW.channel_edit_check_lock(lockItem)) {
            MW.channel_edit_set_lock_single(false, lockItem);
        } else {
            MW.channel_edit_set_lock_single(true, lockItem);
        }
        this.cheditAdapter.notifyDataSetChanged();
    }

    private void DoSelectionAction() {
        int selectItem = this.cheditlist_item_pos;
        if (MW.channel_edit_check_selection(selectItem)) {
            MW.channel_edit_select_single(false, selectItem);
        } else {
            MW.channel_edit_select_single(true, selectItem);
        }
        this.cheditAdapter.notifyDataSetChanged();
    }

    private void ShowSaveDialog(int event) {
        if (MW.channel_edit_check_data_modified()) {
            Dialog saveDataDialog = new SaveDia(this, R.style.MyDialog, event, this.cheditlist_item_pos);
            saveDataDialog.show();
            saveDataDialog.getWindow().addFlags(2);
            return;
        }
        switch (event) {
            case MW.f0xe3564d8 /*5*/:
                RefreshTvRadio();
            case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
                if (MW.db_get_total_service_count() > 0) {
                    SelectSatAction();
                }
            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                SwitchRegions(21);
            case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                SwitchRegions(22);
            default:
                finish();
        }
    }

    private void ShowTipDialog(int Event) {
        Dialog tipsDialog = new TipsDia(this, R.style.MyDialog, Event);
        tipsDialog.show();
        tipsDialog.getWindow().addFlags(2);
    }

    private void DoFavAction() {
        FavDialog favDig = new FavDialog(this, R.style.MyDialog);
        favDig.show();
        favDig.getWindow().addFlags(2);
    }

    private void RefreshTvRadio() {
        switch (MW.ts_player_play_switch_tv_radio(true)) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                RefreshShowChannelEditAll(true);
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                SETTINGS.makeText(this, R.string.NO_CHANNEL, 0);
            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                SETTINGS.makeText(this, R.string.NO_VIDEO_CHANNEL, 0);
            case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                SETTINGS.makeText(this, R.string.NO_AUDIO_CHANNEL, 0);
            default:
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    if (!this.fsecondfuntion) {
                        ShowSaveDialog(4);
                        break;
                    }
                    RefreshHelpTips();
                    return true;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    ShowSaveDialog(7);
                    return true;
                case MW.RET_MEMORY_ERROR /*22*/:
                    ShowSaveDialog(8);
                    return true;
                case DtvBaseActivity.KEYCODE_SAT /*55*/:
                    return true;
                case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                    ShowSaveDialog(5);
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                if (!this.fsecondfuntion) {
                    ShowSaveDialog(4);
                    break;
                }
                RefreshHelpTips();
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void initMessagehandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }
}
