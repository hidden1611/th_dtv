package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.PasswordDialog;
import th.dtv.R;
import th.dtv.SETTINGS;

public class PMActivity extends DtvBaseActivity {
    private Button channeledit_btn;
    private Button channelpideidt_btn;
    private Button delete_all_channel_btn;
    private Dialog dlg;
    private Button fav_list_btn;

    /* renamed from: th.dtv.activity.PMActivity.1 */
    class C01991 implements OnClickListener {

        /* renamed from: th.dtv.activity.PMActivity.1.1 */
        class C01971 implements OnKeyListener {
            C01971() {
            }

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 1 || (keyCode != 4 && keyCode != 82)) {
                    return false;
                }
                PMActivity.this.dlg.dismiss();
                return true;
            }
        }

        /* renamed from: th.dtv.activity.PMActivity.1.2 */
        class C01982 implements PswdCB {
            C01982() {
            }

            public void DoCallBackEvent() {
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.setClass(PMActivity.this, ChannelEditActivity.class);
                PMActivity.this.startActivity(intent);
            }
        }

        C01991() {
        }

        public void onClick(View view) {
            if (SETTINGS.get_menu_lock_onff()) {
//                PMActivity.this.dlg = new PasswordDialog(PMActivity.this, R.style.MyDialog, new C01971(), new C01982());
//                PMActivity.this.dlg.show();
//                ((PasswordDialog) PMActivity.this.dlg).setHandleUpDown(true);
                return;
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(PMActivity.this, ChannelEditActivity.class);
            PMActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.PMActivity.2 */
    class C02022 implements OnClickListener {

        /* renamed from: th.dtv.activity.PMActivity.2.1 */
        class C02001 implements OnKeyListener {
            C02001() {
            }

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 1 || (keyCode != 4 && keyCode != 82)) {
                    return false;
                }
                PMActivity.this.dlg.dismiss();
                return true;
            }
        }

        /* renamed from: th.dtv.activity.PMActivity.2.2 */
        class C02012 implements PswdCB {
            C02012() {
            }

            public void DoCallBackEvent() {
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.setClass(PMActivity.this, ChannelPidEditActivity.class);
                PMActivity.this.startActivity(intent);
            }
        }

        C02022() {
        }

        public void onClick(View view) {
            if (SETTINGS.get_menu_lock_onff()) {
//                PMActivity.this.dlg = new PasswordDialog(PMActivity.this, R.style.MyDialog, new C02001(), new C02012());
//                PMActivity.this.dlg.show();
//                ((PasswordDialog) PMActivity.this.dlg).setHandleUpDown(true);
                return;
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(PMActivity.this, ChannelPidEditActivity.class);
            PMActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.PMActivity.3 */
    class C02053 implements OnClickListener {

        /* renamed from: th.dtv.activity.PMActivity.3.1 */
        class C02031 implements OnKeyListener {
            C02031() {
            }

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 1 || (keyCode != 4 && keyCode != 82)) {
                    return false;
                }
                PMActivity.this.dlg.dismiss();
                return true;
            }
        }

        /* renamed from: th.dtv.activity.PMActivity.3.2 */
        class C02042 implements PswdCB {
            C02042() {
            }

            public void DoCallBackEvent() {
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.setClass(PMActivity.this, FavListEdit.class);
                PMActivity.this.startActivity(intent);
            }
        }

        C02053() {
        }

        public void onClick(View view) {
            if (SETTINGS.get_menu_lock_onff()) {
//                PMActivity.this.dlg = new PasswordDialog(PMActivity.this, R.style.MyDialog, new C02031(), new C02042());
//                PMActivity.this.dlg.show();
//                ((PasswordDialog) PMActivity.this.dlg).setHandleUpDown(true);
                return;
            }
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(PMActivity.this, FavListEdit.class);
            PMActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.PMActivity.4 */
    class C02084 implements OnClickListener {

        /* renamed from: th.dtv.activity.PMActivity.4.1 */
        class C02061 implements OnKeyListener {
            C02061() {
            }

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 1 || (keyCode != 4 && keyCode != 82)) {
                    return false;
                }
                PMActivity.this.dlg.dismiss();
                return true;
            }
        }

        /* renamed from: th.dtv.activity.PMActivity.4.2 */
        class C02072 implements PswdCB {
            C02072() {
            }

            public void DoCallBackEvent() {
                PMActivity.this.DelAllChannelConfrim();
            }
        }

        C02084() {
        }

        public void onClick(View view) {
            if (SETTINGS.get_menu_lock_onff()) {
                return;
            }
            PMActivity.this.DelAllChannelConfrim();
        }
    }

    public class ConfirmCustomDialog extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        TextView mInfoTextView;
        TextView mTitleTextView;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.factory_restore_confirm_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitleTextView = (TextView) findViewById(R.id.title_textview);
            this.mInfoTextView = (TextView) findViewById(R.id.info_textview);
            this.mTitleTextView.setText(R.string.delete_allservice);
            this.mInfoTextView.setText(R.string.confirm_del_all_service);
            this.mBtnCancel.requestFocus();
        }

        public ConfirmCustomDialog(Context context, int theme) {
            super(context, theme);
            this.mBtnStart = null;
            this.mBtnCancel = null;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public PMActivity() {
        this.dlg = null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.channeledit_btn.hasFocus()) {
                        this.delete_all_channel_btn.requestFocus();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.delete_all_channel_btn.hasFocus()) {
                        this.channeledit_btn.requestFocus();
                        return true;
                    }
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.programmanage_activity);
        initView();
        initListener();
    }

    private void initListener() {
        this.channeledit_btn.setOnClickListener(new C01991());
        this.channelpideidt_btn.setOnClickListener(new C02022());
        this.fav_list_btn.setOnClickListener(new C02053());
        this.delete_all_channel_btn.setOnClickListener(new C02084());
    }

    public void DelAllChannelConfrim() {
        Dialog confirmDialog = new ConfirmCustomDialog(this, R.style.MyDialog);
        confirmDialog.show();
        LayoutParams lp = confirmDialog.getWindow().getAttributes();
        lp.x = 0;
        lp.y = -25;
        confirmDialog.getWindow().setAttributes(lp);
        confirmDialog.getWindow().addFlags(2);
    }

    private void initView() {
        this.channeledit_btn = (Button) findViewById(R.id.channeledit_btn);
        this.channelpideidt_btn = (Button) findViewById(R.id.channelpideidt_btn);
        this.fav_list_btn = (Button) findViewById(R.id.fav_list_btn);
        this.delete_all_channel_btn = (Button) findViewById(R.id.delete_allservice_btn);
        ((LinearLayout) findViewById(R.id.chedit_pid_ll)).setVisibility(View.GONE);
    }

    protected void onResume() {
        SETTINGS.send_led_msg("NENU");
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onRestart() {
        super.onRestart();
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        super.onDestroy();
    }
}
