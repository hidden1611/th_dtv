package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import th.dtv.ComboEditText;
import th.dtv.ComboLayout;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.aud_info;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.dvb_t_area;
import th.dtv.mw_data.dvb_t_tp;
import th.dtv.mw_data.region_info;
import th.dtv.mw_data.service;
import th.dtv.util.HongKongDTMBHD;

public class ChannelPidEditActivity extends DtvBaseActivity {
    private int System_type;
    private dvb_t_area areaInfo;
    private TextView channel_type;
    Dialog current_dig;
    private int current_service_index;
    private int current_service_type;
    private int mFirstVisibleItem;
    Callback mSHCallback;
    private int mVisibleItemCount;
    private Handler mwMsgHandler;
    private PidAdapter pidAdapter;
    private CustomListView pidedit_listview;
    private RelativeLayout pidedit_rl;
    private int pidlist_item_pos;
    private region_info regionInfo;
    private dvb_s_sat satInfo;
    private service serviceInfo;
    private dvb_t_tp t_tpInfo;
    private dvb_s_tp tpInfo;
    private TextView videoPid_title;

    /* renamed from: th.dtv.activity.ChannelPidEditActivity.1 */
    class C01201 implements Callback {
        C01201() {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("ChannelPidEditActivity", "surfaceChanged");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("ChannelPidEditActivity", "surfaceCreated");
            try {
                initSurface(holder);
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("ChannelPidEditActivity", "surfaceDestroyed");
        }

        private void initSurface(SurfaceHolder h) {
            Canvas c = null;
            try {
                Log.d("ChannelPidEditActivity", "initSurface");
                c = h.lockCanvas();
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            } catch (Throwable th) {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /* renamed from: th.dtv.activity.ChannelPidEditActivity.2 */
    class C01212 implements OnScrollListener {
        C01212() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            ChannelPidEditActivity.this.mFirstVisibleItem = firstVisibleItem;
            ChannelPidEditActivity.this.mVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.ChannelPidEditActivity.3 */
    class C01223 implements OnItemSelectedListener {
        C01223() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            ChannelPidEditActivity.this.pidlist_item_pos = pos;
            if (MW.db_get_service_info(ChannelPidEditActivity.this.current_service_type, pos, ChannelPidEditActivity.this.serviceInfo) == 0) {
                MW.ts_player_play(ChannelPidEditActivity.this.current_service_type, pos, 50, true);
                SETTINGS.set_led_channel(ChannelPidEditActivity.this.serviceInfo.channel_number_display);
                ChannelPidEditActivity.this.current_service_index = ChannelPidEditActivity.this.serviceInfo.service_index;
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public class AddDialog extends Dialog {
        private int AUDIO_COUNT;
        private aud_info[] audioInfoList;
        private ComboEditText audio_pid_et;
        private int audio_type;
        private int currentPos;
        int iplpid;
        int itprofile;
        int ituner_type;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private ComboEditText pcr_pid_et;
        private ComboLayout pid_audio_type;
        private EditText pid_chname;
        private ComboLayout pid_satellite;
        private TextView pid_satellite1;
        private ComboLayout pid_tp;
        private ComboLayout pid_video_type;
        private LinearLayout plp_ll;
        private ComboEditText plp_pid_et;
        private ComboLayout profile;
        private int[] satIndexList;
        private LinearLayout satellite_ll;
        private int satellite_pos;
        private int tp_pos;
        private ComboLayout tuner_type;
        private ComboEditText video_pid_et;
        private int video_type;
        private LinearLayout videopid_ll;

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.1 */
        class C01231 implements TextWatcher {
            C01231() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.2 */
        class C01242 implements OnFocusChangeListener {
            C01242() {
            }

            public void onFocusChange(View arg0, boolean arg1) {
                AddDialog.this.plp_pid_et.getInfoTextView().requestFocus();
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.3 */
        class C01253 implements TextWatcher {
            C01253() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.4 */
        class C01264 implements OnFocusChangeListener {
            C01264() {
            }

            public void onFocusChange(View arg0, boolean arg1) {
                AddDialog.this.video_pid_et.getInfoTextView().requestFocus();
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.5 */
        class C01275 implements TextWatcher {
            C01275() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.6 */
        class C01286 implements OnFocusChangeListener {
            C01286() {
            }

            public void onFocusChange(View arg0, boolean arg1) {
                AddDialog.this.audio_pid_et.getInfoTextView().requestFocus();
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.7 */
        class C01297 implements TextWatcher {
            C01297() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.8 */
        class C01308 implements OnFocusChangeListener {
            C01308() {
            }

            public void onFocusChange(View arg0, boolean arg1) {
                AddDialog.this.pcr_pid_et.getInfoTextView().requestFocus();
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.AddDialog.9 */
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.pidedit_add_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.pid_satellite1 = (TextView) findViewById(R.id.pid_satellite1);
            this.satellite_ll = (LinearLayout) findViewById(R.id.satellite_ll);
            this.tuner_type = (ComboLayout) findViewById(R.id.tuner_type);
            this.profile = (ComboLayout) findViewById(R.id.profile);
            this.plp_ll = (LinearLayout) findViewById(R.id.plp_ll);
            this.plp_pid_et = (ComboEditText) findViewById(R.id.plp_pid_et);
            this.pid_satellite = (ComboLayout) findViewById(R.id.pid_satellite);
            this.pid_tp = (ComboLayout) findViewById(R.id.pid_tp);
            this.pid_chname = (EditText) findViewById(R.id.pid_chname);
            this.pid_video_type = (ComboLayout) findViewById(R.id.pid_video_type);
            this.pid_audio_type = (ComboLayout) findViewById(R.id.pid_audio_type);
            this.video_pid_et = (ComboEditText) findViewById(R.id.video_pid_et);
            this.audio_pid_et = (ComboEditText) findViewById(R.id.audio_pid_et);
            this.pcr_pid_et = (ComboEditText) findViewById(R.id.pcr_pid_et);
            this.videopid_ll = (LinearLayout) findViewById(R.id.videopid_ll);
            if (ChannelPidEditActivity.this.current_service_type == 1) {
                this.pid_video_type.setVisibility(View.GONE);
                this.videopid_ll.setVisibility(View.GONE);
            } else {
                this.pid_video_type.setVisibility(View.VISIBLE);
                this.videopid_ll.setVisibility(View.VISIBLE);
            }
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                if (MW.db_dvb_s_get_current_tp_info(0, ChannelPidEditActivity.this.tpInfo) == 0) {
                    this.satellite_pos = ChannelPidEditActivity.this.tpInfo.sat_index;
                    this.tp_pos = ChannelPidEditActivity.this.tpInfo.tp_index;
                    this.currentPos = ChannelPidEditActivity.this.tpInfo.sat_pos;
                } else {
                    this.satellite_pos = 0;
                    this.tp_pos = 0;
                }
            } else if (MW.db_dvb_t_get_current_tp_info(ChannelPidEditActivity.this.t_tpInfo) == 0) {
                this.satellite_pos = ChannelPidEditActivity.this.t_tpInfo.area_index;
                this.tp_pos = ChannelPidEditActivity.this.t_tpInfo.tp_index;
                this.currentPos = this.satellite_pos;
            } else {
                this.satellite_pos = 0;
                this.tp_pos = 0;
                this.currentPos = this.satellite_pos;
            }
            FillSatelliteItem();
            RefreshSatelliteInfo(false);
            if (ChannelPidEditActivity.this.System_type == 1) {
                FillTunerTypeItems();
                FillProfileTypeItems(this.ituner_type);
                this.plp_ll.setVisibility(View.VISIBLE);
                this.plp_pid_et.initView(R.string.pid_plp, "255", "", new C01231());
                this.plp_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
                this.plp_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
                this.plp_pid_et.setOnFocusChangeListener(new C01242());
                if (this.ituner_type == 0) {
                    this.plp_ll.setEnabled(false);
                    this.plp_pid_et.setFocusable(false);
                    this.plp_pid_et.getInfoTextView().setFocusable(false);
                    this.plp_pid_et.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                } else {
                    this.plp_ll.setEnabled(true);
                    this.plp_pid_et.setFocusable(true);
                    this.plp_pid_et.getInfoTextView().setFocusable(true);
                    this.plp_pid_et.setBackgroundResource(R.drawable.setlect_white_focus);
                }
            }
            FillVideoTypeItems(0);
            FillAudioTypeItems(0);
            this.pid_chname.setText("New Channel");
            this.pid_chname.setSelection(this.pid_chname.length());
            this.video_pid_et.initView(R.string.VideoPid, "8191", "", new C01253());
            this.video_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.video_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.video_pid_et.setOnFocusChangeListener(new C01264());
            this.audio_pid_et.initView(R.string.AudioPid, "8191", "", new C01275());
            this.audio_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.audio_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.audio_pid_et.setOnFocusChangeListener(new C01286());
            this.pcr_pid_et.initView(R.string.PcrPid, "8191", "", new C01297());
            this.pcr_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pcr_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.pcr_pid_et.setOnFocusChangeListener(new C01308());
        }

        private void FillSatelliteItem() {
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                int satCount = MW.db_dvb_s_get_selected_sat_count(0);
                ArrayList items = new ArrayList();
                items.clear();
                this.satIndexList = new int[satCount];
                for (int i = 0; i < satCount; i++) {
                    if (MW.db_dvb_s_get_selected_sat_info(0, i, ChannelPidEditActivity.this.satInfo) == 0) {
                        items.add(String.format("%s", new Object[]{ChannelPidEditActivity.this.satInfo.sat_name}));
                        this.satIndexList[i] = ChannelPidEditActivity.this.satInfo.sat_index;
                    }
                }
                this.pid_satellite.initView((int) R.string.Satellite, items, this.currentPos, new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                        AddDialog.this.satellite_pos = AddDialog.this.satIndexList[position];
                        if (AddDialog.this.currentPos != position) {
                            AddDialog.this.currentPos = position;
                            AddDialog.this.RefreshSatelliteInfo(true);
                        }
                    }
                });
            } else {
                this.pid_satellite.setVisibility(View.GONE);
                this.satellite_ll.setVisibility(View.VISIBLE);
                if (MW.db_dvb_t_get_area_info(this.satellite_pos, ChannelPidEditActivity.this.areaInfo) == 0) {
                    this.pid_satellite1.setText(SETTINGS.get_area_string_by_index(ChannelPidEditActivity.this, ChannelPidEditActivity.this.areaInfo.area_index));
                } else {
                    this.pid_satellite1.setText("unknow");
                }
            }
            this.pid_satellite.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pid_satellite.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void RefreshSatelliteInfo(boolean bResetTpIndex) {
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                if (MW.db_dvb_s_get_sat_info(0, this.satellite_pos, ChannelPidEditActivity.this.satInfo) == 0) {
                    if (bResetTpIndex) {
                        this.tp_pos = 0;
                    }
                    FillTpItems();
                }
            } else if (MW.db_dvb_t_get_area_info(this.satellite_pos, ChannelPidEditActivity.this.areaInfo) == 0) {
                if (bResetTpIndex) {
                    this.tp_pos = 0;
                }
                FillTpItems();
            }
        }

        private void FillTpItems() {
            ArrayList items1;
            int i;
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                if (ChannelPidEditActivity.this.satInfo != null) {
                    items1 = new ArrayList();
                    items1.clear();
                    for (i = 0; i < ChannelPidEditActivity.this.satInfo.tp_count; i++) {
                        if (MW.db_dvb_s_get_tp_info(0, ChannelPidEditActivity.this.satInfo.sat_index, i, ChannelPidEditActivity.this.tpInfo) == 0) {
                            String str = "%d / %d    %05d   %s   %05d";
                            Object[] objArr = new Object[5];
                            objArr[0] = Integer.valueOf(i + 1);
                            objArr[1] = Integer.valueOf(ChannelPidEditActivity.this.satInfo.tp_count);
                            objArr[2] = Integer.valueOf(ChannelPidEditActivity.this.tpInfo.frq);
                            objArr[3] = ChannelPidEditActivity.this.tpInfo.pol == 0 ? "V" : "H";
                            objArr[4] = Integer.valueOf(ChannelPidEditActivity.this.tpInfo.sym);
                            items1.add(String.format(str, objArr));
                        }
                    }
                    this.pid_tp.initView((int) R.string.Transponder, items1, this.tp_pos, new OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                            AddDialog.this.tp_pos = position;
                        }
                    });
                }
            } else if (ChannelPidEditActivity.this.areaInfo != null) {
                items1 = new ArrayList();
                items1.clear();
                for (i = 0; i < ChannelPidEditActivity.this.areaInfo.tp_count; i++) {
                    if (MW.db_dvb_t_get_tp_info(ChannelPidEditActivity.this.areaInfo.area_index, i, ChannelPidEditActivity.this.t_tpInfo) == 0) {
                        items1.add(String.format("CH - %s  %d KHz  %d MHz", new Object[]{ChannelPidEditActivity.this.t_tpInfo.channel_number, Integer.valueOf(ChannelPidEditActivity.this.t_tpInfo.frq), Integer.valueOf(ChannelPidEditActivity.this.t_tpInfo.bandwidth)}));
                    }
                }
                this.pid_tp.initView((int) R.string.Transponder, items1, this.tp_pos, new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                        AddDialog.this.tp_pos = position;
                    }
                });
            }
            this.pid_tp.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pid_tp.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void FillVideoTypeItems(int type) {
            ArrayList items = new ArrayList();
            items.clear();
            for (int i = 0; i < 3; i++) {
                items.add(ChannelPidEditActivity.this.getResources().getStringArray(R.array.video_type)[i]);
            }
            this.pid_video_type.initView((int) R.string.VideoType, items, type, new OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    AddDialog.this.video_type = position;
                }
            });
            this.pid_video_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pid_video_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void FillAudioTypeItems(int type) {
            this.pid_audio_type.initView((int) R.string.AudioType, (int) R.array.audio_type, type, new OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    AddDialog.this.audio_type = position;
                }
            });
            this.pid_audio_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pid_audio_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void FillTunerTypeItems() {
            this.tuner_type.setVisibility(View.VISIBLE);
            ArrayList items = new ArrayList();
            items.clear();
            items.add("DVB_T");
            items.add("DVB_T2");
            this.tuner_type.initView((int) R.string.TunerType, items, this.ituner_type, new OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    AddDialog.this.ituner_type = position;
                    AddDialog.this.FillProfileTypeItems(AddDialog.this.ituner_type);
                    if (AddDialog.this.ituner_type == 0) {
                        AddDialog.this.plp_ll.setEnabled(false);
                        AddDialog.this.plp_pid_et.getInfoTextView().setFocusable(false);
                        AddDialog.this.plp_pid_et.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                        return;
                    }
                    AddDialog.this.plp_ll.setEnabled(true);
                    AddDialog.this.plp_pid_et.getInfoTextView().setFocusable(true);
                    AddDialog.this.plp_pid_et.setBackgroundResource(R.drawable.setlect_white_focus);
                }
            });
            this.tuner_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.tuner_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void FillProfileTypeItems(int tuner_type) {
            this.profile.setVisibility(View.VISIBLE);
            ArrayList items = new ArrayList();
            items.clear();
            if (tuner_type == 0) {
                items.add("HP");
                items.add("LP");
            } else if (tuner_type == 1) {
                items.add("Base");
                items.add("Lite");
            }
            this.profile.initView((int) R.string.Profile, items, 0, new OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    AddDialog.this.itprofile = position;
                }
            });
            this.profile.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.profile.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        public AddDialog(Context context, int theme) {
            super(context, theme);
            this.mContext = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.satellite_pos = 0;
            this.tp_pos = 0;
            this.video_type = 0;
            this.audio_type = 0;
            this.AUDIO_COUNT = 1;
            this.audioInfoList = new aud_info[this.AUDIO_COUNT];
            this.currentPos = 0;
            this.pid_satellite1 = null;
            this.ituner_type = 0;
            this.itprofile = 0;
            this.iplpid = 0;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (ChannelPidEditActivity.this.System_type == 1 && this.pid_tp.isFocused()) {
                        this.mBtnStart.requestFocus();
                        return true;
                    }
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (ChannelPidEditActivity.this.System_type == 1) {
                        if (this.mBtnCancel.isFocused() || this.mBtnStart.isFocused()) {
                            this.pid_tp.requestFocus();
                            return true;
                        } else if (this.pcr_pid_et.isFocused() || this.pcr_pid_et.getInfoTextView().isFocused()) {
                            this.mBtnStart.requestFocus();
                            return true;
                        }
                    }
                    break;
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    public class EditDialog extends Dialog {
        private int AUDIO_COUNT;
        private aud_info[] audioInfoList;
        private ComboEditText audio_pid_et;
        private int audio_type;
        int iplpid;
        int itprofile;
        int ituner_type;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private ComboEditText pcr_pid_et;
        private ComboLayout pid_audio_type;
        private TextView pid_chname;
        private TextView pid_satellite;
        private TextView pid_tp;
        private ComboLayout pid_video_type;
        private LinearLayout plp_ll;
        private ComboEditText plp_pid_et;
        private ComboLayout profile;
        private int satellite_pos;
        private int tp_pos;
        private ComboLayout tuner_type;
        private ComboEditText video_pid_et;
        private int video_type;
        private LinearLayout videopid_ll;

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.1 */

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.3 */
        class C01343 implements OnItemClickListener {
            C01343() {
            }

            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                EditDialog.this.video_type = position;
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.4 */
        class C01354 implements OnItemClickListener {
            C01354() {
            }

            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                EditDialog.this.audio_type = position;
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.5 */
        class C01365 implements TextWatcher {
            C01365() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.6 */
        class C01376 implements OnFocusChangeListener {
            C01376() {
            }

            public void onFocusChange(View arg0, boolean arg1) {
                EditDialog.this.video_pid_et.getInfoTextView().requestFocus();
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.7 */
        class C01387 implements TextWatcher {
            C01387() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.8 */
        class C01398 implements OnFocusChangeListener {
            C01398() {
            }

            public void onFocusChange(View arg0, boolean arg1) {
                EditDialog.this.audio_pid_et.getInfoTextView().requestFocus();
            }
        }

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.EditDialog.9 */
        class C01409 implements TextWatcher {
            C01409() {
            }

            public void afterTextChanged(Editable v) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.pidedit_edit_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.tuner_type = (ComboLayout) findViewById(R.id.tuner_type);
            this.profile = (ComboLayout) findViewById(R.id.profile);
            this.plp_ll = (LinearLayout) findViewById(R.id.plp_ll);
            this.plp_pid_et = (ComboEditText) findViewById(R.id.plp_pid_et);
            this.pid_satellite = (TextView) findViewById(R.id.pid_satellite);
            this.pid_tp = (TextView) findViewById(R.id.pid_tp);
            this.pid_chname = (TextView) findViewById(R.id.pid_chname);
            this.pid_video_type = (ComboLayout) findViewById(R.id.pid_video_type);
            this.pid_audio_type = (ComboLayout) findViewById(R.id.pid_audio_type);
            this.video_pid_et = (ComboEditText) findViewById(R.id.video_pid_et);
            this.audio_pid_et = (ComboEditText) findViewById(R.id.audio_pid_et);
            this.pcr_pid_et = (ComboEditText) findViewById(R.id.pcr_pid_et);
            this.videopid_ll = (LinearLayout) findViewById(R.id.videopid_ll);
            if (ChannelPidEditActivity.this.current_service_type == 1) {
                this.pid_video_type.setVisibility(View.GONE);
                this.videopid_ll.setVisibility(View.GONE);
            } else {
                this.pid_video_type.setVisibility(View.VISIBLE);
                this.videopid_ll.setVisibility(View.VISIBLE);
            }
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                if (MW.db_dvb_s_get_current_tp_info(0, ChannelPidEditActivity.this.tpInfo) == 0) {
                    this.satellite_pos = ChannelPidEditActivity.this.tpInfo.sat_index;
                    this.tp_pos = ChannelPidEditActivity.this.tpInfo.tp_index;
                } else {
                    this.satellite_pos = 0;
                    this.tp_pos = 0;
                }
            } else if (MW.db_dvb_t_get_current_tp_info(ChannelPidEditActivity.this.t_tpInfo) == 0) {
                this.satellite_pos = ChannelPidEditActivity.this.t_tpInfo.area_index;
                this.tp_pos = ChannelPidEditActivity.this.t_tpInfo.tp_index;
            } else {
                this.satellite_pos = 0;
                this.tp_pos = 0;
            }
            FillSatelliteItem();
            FillTpItems();
            if (MW.db_get_service_info(ChannelPidEditActivity.this.current_service_type, ChannelPidEditActivity.this.pidlist_item_pos, ChannelPidEditActivity.this.serviceInfo) == 0) {
                this.video_type = ChannelPidEditActivity.this.serviceInfo.video_stream_type;
                FillVideoTypeItems(this.video_type);
                this.audio_type = ChannelPidEditActivity.this.serviceInfo.audio_info[ChannelPidEditActivity.this.serviceInfo.audio_index].audio_stream_type;
                FillAudioTypeItems(this.audio_type);
                this.pid_chname.setText(ChannelPidEditActivity.this.serviceInfo.service_name);
                FillVideoPidItems(ChannelPidEditActivity.this.serviceInfo.video_pid);
                FillAudioPidItems(ChannelPidEditActivity.this.serviceInfo.audio_info[ChannelPidEditActivity.this.serviceInfo.audio_index].audio_pid);
                FillPCRPidItems(ChannelPidEditActivity.this.serviceInfo.pcr_pid);
                this.AUDIO_COUNT = ChannelPidEditActivity.this.serviceInfo.audio_info.length;
                this.audioInfoList = new aud_info[this.AUDIO_COUNT];
                if (ChannelPidEditActivity.this.System_type == 1) {
                    int tuner_type = 0;
                    if (ChannelPidEditActivity.this.serviceInfo.front_end_type == 3) {
                        tuner_type = 0;
                    } else if (ChannelPidEditActivity.this.serviceInfo.front_end_type == 4) {
                        tuner_type = 1;
                    }
                    FillTunerTypeItems(tuner_type);
                    FillProfileTypeItems(tuner_type, ChannelPidEditActivity.this.serviceInfo.dvb_t_t2_profile);
                    if (tuner_type == 1) {
                        FillPLPItems(ChannelPidEditActivity.this.serviceInfo.dvb_t2_plp_id);
                    }
                }
            } else {
                FillVideoTypeItems(0);
                FillAudioTypeItems(0);
                this.pid_chname.setText("Unknow");
                FillVideoPidItems(0);
                FillAudioPidItems(0);
                FillPCRPidItems(0);
                if (ChannelPidEditActivity.this.System_type == 1) {
                    FillTunerTypeItems(0);
                    FillProfileTypeItems(0, 0);
                    FillPLPItems(0);
                }
            }
        }

        private void FillSatelliteItem() {
            new ArrayList().clear();
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                if (MW.db_dvb_s_get_sat_info(0, this.satellite_pos, ChannelPidEditActivity.this.satInfo) == 0) {
                    this.pid_satellite.setText(ChannelPidEditActivity.this.satInfo.sat_name);
                } else {
                    this.pid_satellite.setText("unknow");
                }
            } else if (MW.db_dvb_t_get_area_info(this.satellite_pos, ChannelPidEditActivity.this.areaInfo) == 0) {
                this.pid_satellite.setText(SETTINGS.get_area_string_by_index(ChannelPidEditActivity.this, ChannelPidEditActivity.this.areaInfo.area_index));
            } else {
                this.pid_satellite.setText("unknow");
            }
        }

        private void FillTpItems() {
            if (ChannelPidEditActivity.this.System_type == 0 || ChannelPidEditActivity.this.System_type == 2) {
                if (MW.db_dvb_s_get_sat_info(0, this.satellite_pos, ChannelPidEditActivity.this.satInfo) != 0) {
                    this.pid_tp.setText("unknow");
                } else if (ChannelPidEditActivity.this.satInfo != null) {
                    new ArrayList().clear();
                    if (MW.db_dvb_s_get_tp_info(0, ChannelPidEditActivity.this.satInfo.sat_index, this.tp_pos, ChannelPidEditActivity.this.tpInfo) == 0) {
                        String str;
                        TextView textView = this.pid_tp;
                        String str2 = "%d / %d    %05d   %s   %05d";
                        Object[] objArr = new Object[5];
                        objArr[0] = Integer.valueOf(this.tp_pos + 1);
                        objArr[1] = Integer.valueOf(ChannelPidEditActivity.this.satInfo.tp_count);
                        objArr[2] = Integer.valueOf(ChannelPidEditActivity.this.tpInfo.frq);
                        if (ChannelPidEditActivity.this.tpInfo.pol == 0) {
                            str = "V";
                        } else {
                            str = "H";
                        }
                        objArr[3] = str;
                        objArr[4] = Integer.valueOf(ChannelPidEditActivity.this.tpInfo.sym);
                        textView.setText(String.format(str2, objArr));
                        return;
                    }
                    this.pid_tp.setText("unknow");
                }
            } else if (MW.db_dvb_t_get_area_info(this.satellite_pos, ChannelPidEditActivity.this.areaInfo) != 0) {
                this.pid_tp.setText("unknow");
            } else if (ChannelPidEditActivity.this.areaInfo != null) {
                new ArrayList().clear();
                if (MW.db_dvb_t_get_tp_info(ChannelPidEditActivity.this.areaInfo.area_index, this.tp_pos, ChannelPidEditActivity.this.t_tpInfo) == 0) {
                    this.pid_tp.setText(String.format("CH - %s  %d KHz  %d MHz", new Object[]{ChannelPidEditActivity.this.t_tpInfo.channel_number, Integer.valueOf(ChannelPidEditActivity.this.t_tpInfo.frq), Integer.valueOf(ChannelPidEditActivity.this.t_tpInfo.bandwidth)}));
                    return;
                }
                this.pid_tp.setText("unknow");
            }
        }

        private void FillVideoTypeItems(int type) {
            ArrayList items = new ArrayList();
            items.clear();
            if (type == 3) {
                items.add(ChannelPidEditActivity.this.getResources().getStringArray(R.array.video_type)[3]);
            } else {
                for (int i = 0; i < 3; i++) {
                    items.add(ChannelPidEditActivity.this.getResources().getStringArray(R.array.video_type)[i]);
                }
            }
            this.pid_video_type.initView((int) R.string.VideoType, items, type, new C01343());
            this.pid_video_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pid_video_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void FillAudioTypeItems(int type) {
            this.pid_audio_type.initView((int) R.string.AudioType, (int) R.array.audio_type, type, new C01354());
            this.pid_audio_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pid_audio_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void FillVideoPidItems(int value) {
            this.video_pid_et.initView(R.string.VideoPid, "" + value, "", new C01365());
            this.video_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.video_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.video_pid_et.setOnFocusChangeListener(new C01376());
        }

        private void FillAudioPidItems(int value) {
            this.audio_pid_et.initView(R.string.AudioPid, "" + value, "", new C01387());
            this.audio_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.audio_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.audio_pid_et.setOnFocusChangeListener(new C01398());
        }

        private void FillPCRPidItems(int value) {
            this.pcr_pid_et.initView(R.string.PcrPid, "" + value, "", new C01409());
            this.pcr_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.pcr_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.pcr_pid_et.setOnFocusChangeListener(new OnFocusChangeListener() {
                public void onFocusChange(View arg0, boolean arg1) {
                    EditDialog.this.pcr_pid_et.getInfoTextView().requestFocus();
                }
            });
        }

        private void FillTunerTypeItems(int Tunertype) {
            this.tuner_type.setVisibility(View.VISIBLE);
            this.tuner_type.setFocusable(false);
            ArrayList items = new ArrayList();
            items.clear();
            items.add("DVB_T");
            items.add("DVB_T2");
            this.ituner_type = Tunertype;
            this.tuner_type.initView((int) R.string.TunerType, items, this.ituner_type, new OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    EditDialog.this.ituner_type = position;
                    EditDialog.this.FillProfileTypeItems(EditDialog.this.ituner_type, 0);
                    if (position == 0) {
                        EditDialog.this.plp_ll.setEnabled(false);
                        EditDialog.this.plp_pid_et.getInfoTextView().setFocusable(false);
                        EditDialog.this.plp_pid_et.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                        return;
                    }
                    EditDialog.this.plp_ll.setEnabled(false);
                    EditDialog.this.plp_pid_et.getInfoTextView().setFocusable(false);
                    EditDialog.this.plp_pid_et.setBackgroundResource(R.drawable.setlect_white_focus);
                }
            });
            this.tuner_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.tuner_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.tuner_type.getLeftImageView().setVisibility(View.INVISIBLE);
            this.tuner_type.getRightImageView().setVisibility(View.INVISIBLE);
        }

        private void FillProfileTypeItems(int tuner_type, int Profile) {
            this.profile.setVisibility(View.VISIBLE);
            this.profile.setFocusable(false);
            ArrayList items = new ArrayList();
            items.clear();
            if (tuner_type == 0) {
                items.add("HP");
                items.add("LP");
            } else if (tuner_type == 1) {
                items.add("Base");
                items.add("Lite");
            }
            this.itprofile = Profile;
            this.profile.initView((int) R.string.Profile, items, this.itprofile, new OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    EditDialog.this.itprofile = position;
                }
            });
            this.profile.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.profile.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.profile.getLeftImageView().setVisibility(View.INVISIBLE);
            this.profile.getRightImageView().setVisibility(View.INVISIBLE);
        }

        private void FillPLPItems(int plpid) {
            this.plp_ll.setVisibility(View.VISIBLE);
            this.plp_ll.setFocusable(false);
            this.plp_pid_et.setFocusable(false);
            this.plp_pid_et.getInfoTextView().setFocusable(false);
            this.plp_pid_et.initView(R.string.pid_plp, "" + plpid, "", new TextWatcher() {
                public void afterTextChanged(Editable v) {
                }

                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }

                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }
            });
            this.plp_pid_et.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.plp_pid_et.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.plp_pid_et.setOnFocusChangeListener(new OnFocusChangeListener() {
                public void onFocusChange(View arg0, boolean arg1) {
                    EditDialog.this.plp_pid_et.getInfoTextView().requestFocus();
                }
            });
            if (this.ituner_type == 0) {
                this.plp_ll.setEnabled(false);
                this.plp_pid_et.setFocusable(false);
                this.plp_pid_et.getInfoTextView().setFocusable(false);
                return;
            }
            this.plp_ll.setEnabled(false);
            this.plp_pid_et.setFocusable(false);
            this.plp_pid_et.getInfoTextView().setFocusable(false);
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mContext = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.satellite_pos = 0;
            this.tp_pos = 0;
            this.video_type = 0;
            this.audio_type = 0;
            this.AUDIO_COUNT = 0;
            this.ituner_type = 0;
            this.itprofile = 0;
            this.iplpid = 0;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.pid_audio_type.isFocused()) {
                        if (this.pid_video_type.getVisibility() != 0) {
                            this.mBtnStart.requestFocus();
                            return true;
                        }
                        this.pid_video_type.requestFocus();
                        return true;
                    } else if (this.pid_video_type.isFocused()) {
                        this.mBtnStart.requestFocus();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.mBtnCancel.isFocused() || this.mBtnStart.isFocused()) {
                        if (this.pid_video_type.getVisibility() != 0) {
                            this.pid_audio_type.requestFocus();
                            return true;
                        }
                        this.pid_video_type.requestFocus();
                        return true;
                    } else if (this.pcr_pid_et.getInfoTextView().isFocused()) {
                        this.mBtnStart.requestFocus();
                        return true;
                    }
                    break;
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.RET_INVALID_SAT /*11*/:
                    int bCurrentServiceChanged = msg.arg1;
                    switch (sub_event_type) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            Log.e("ChannelPidEditActivity", "service name update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            ChannelPidEditActivity.this.RefreshShowPidEditAll(false);
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            Log.e("ChannelPidEditActivity", "service params update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            if (bCurrentServiceChanged == 1) {
                                MW.ts_player_play_current(true, false);
                            }
                        default:
                    }
                default:
            }
        }
    }

    private class PidAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mcontext;

        class ViewHolder {
            TextView audiopid;
            TextView pcrpid;
            ImageView pid__hdsd_img;
            TextView pid_chname;
            TextView pid_no;
            ImageView pid_scramble_img;
            TextView videopid;

            ViewHolder() {
            }
        }

        public PidAdapter(Context context) {
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_get_service_count(ChannelPidEditActivity.this.current_service_type);
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.pidedit_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.pid_no = (TextView) convertView.findViewById(R.id.pid_no);
                localViewHolder.pid_scramble_img = (ImageView) convertView.findViewById(R.id.chedit_scramble_img);
                localViewHolder.pid__hdsd_img = (ImageView) convertView.findViewById(R.id.chedit_hdsd_img);
                localViewHolder.pid_chname = (TextView) convertView.findViewById(R.id.pid_chname);
                localViewHolder.videopid = (TextView) convertView.findViewById(R.id.pid_videopid);
                localViewHolder.audiopid = (TextView) convertView.findViewById(R.id.pid_audiopid);
                localViewHolder.pcrpid = (TextView) convertView.findViewById(R.id.pid_pcrpid);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.db_get_service_info(ChannelPidEditActivity.this.current_service_type, position, ChannelPidEditActivity.this.serviceInfo) == 0) {
                localViewHolder.pid_no.setText(Integer.toString(ChannelPidEditActivity.this.serviceInfo.channel_number_display));
                if (ChannelPidEditActivity.this.serviceInfo.is_scrambled) {
                    localViewHolder.pid_scramble_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.pid_scramble_img.setVisibility(View.INVISIBLE);
                }
                if (SETTINGS.bHongKongDTMB) {
                    if (HongKongDTMBHD.isHongKongDTMBHDChannel(ChannelPidEditActivity.this.serviceInfo.video_pid, ChannelPidEditActivity.this.serviceInfo.audio_info[0].audio_pid) || ChannelPidEditActivity.this.serviceInfo.is_hd) {
                        localViewHolder.pid__hdsd_img.setVisibility(View.VISIBLE);
                    } else {
                        localViewHolder.pid__hdsd_img.setVisibility(View.INVISIBLE);
                    }
                } else if (ChannelPidEditActivity.this.serviceInfo.is_hd) {
                    localViewHolder.pid__hdsd_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.pid__hdsd_img.setVisibility(View.INVISIBLE);
                }
                localViewHolder.pid_chname.setText(ChannelPidEditActivity.this.serviceInfo.service_name);
                localViewHolder.videopid.setText(Integer.toString(ChannelPidEditActivity.this.serviceInfo.video_pid));
                localViewHolder.audiopid.setText(Integer.toString(ChannelPidEditActivity.this.serviceInfo.audio_info[ChannelPidEditActivity.this.serviceInfo.audio_index].audio_pid));
                localViewHolder.pcrpid.setText(Integer.toString(ChannelPidEditActivity.this.serviceInfo.pcr_pid));
            }
            if (ChannelPidEditActivity.this.current_service_type == 1) {
                localViewHolder.videopid.setVisibility(View.INVISIBLE);
            } else {
                localViewHolder.videopid.setVisibility(View.VISIBLE);
            }
            return convertView;
        }
    }

    public class RegionSelectDialog extends Dialog {
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.RegionSelectDialog.1 */
        class C01411 implements OnItemClickListener {
            C01411() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (MW.db_set_service_region_current_pos(position)) {
                    MW.ts_player_play_current(true, false);
                    ChannelPidEditActivity.this.RefreshShowPidEditAll(true);
                }
                RegionSelectDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            ChannelPidEditActivity.this.current_dig = this;
            ((TextView) findViewById(R.id.TextView_Title)).setText(R.string.Satellite);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            int serviceRegionCount = MW.db_get_service_region_count();
            List<String> items = new ArrayList();
            items.clear();
            items.add(ChannelPidEditActivity.this.getString(R.string.All));
            for (int i = 0; i < serviceRegionCount; i++) {
                if (MW.db_get_service_region_info(i + 1, ChannelPidEditActivity.this.regionInfo) == 0) {
                    if (MW.get_system_tuner_config() == 1) {
                        items.add(SETTINGS.get_area_string_by_index(ChannelPidEditActivity.this, ChannelPidEditActivity.this.regionInfo.region_index));
                    } else {
                        items.add(ChannelPidEditActivity.this.regionInfo.region_name);
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(ChannelPidEditActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setCustomSelection(MW.db_get_service_region_current_pos());
            this.mListView.setItemChecked(MW.db_get_service_region_current_pos(), true);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C01411());
        }

        protected void onStop() {
            super.onStop();
            ChannelPidEditActivity.this.current_dig = null;
        }

        public RegionSelectDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_SAT /*55*/:
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    public class SaveDia extends Dialog {
        TextView deldia_title;
        TextView delete_content;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        int mEvent;
        private LayoutInflater mInflater;
        View mLayoutView;

        /* renamed from: th.dtv.activity.ChannelPidEditActivity.SaveDia.1 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.delete_content = (TextView) findViewById(R.id.delete_content);
            if (this.mEvent == 4) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else if (this.mEvent == 6) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else if (this.mEvent == 5) {
                this.deldia_title.setText(R.string.save);
                this.delete_content.setText(R.string.chedit_dywtsd);
                this.mBtnStart.requestFocus();
            } else {
                this.mBtnStart.requestFocus();
            }
        }

        public SaveDia(Context context, int theme, int event) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.deldia_title = null;
            this.delete_content = null;
            this.mEvent = -1;
            this.mContext = context;
            this.mEvent = event;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    private class listOnKeyListener implements OnKeyListener {
        private listOnKeyListener() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        ChannelPidEditActivity.this.DoEditAction();
                        return true;
                }
            }
            return false;
        }
    }

    public ChannelPidEditActivity() {
        this.pidedit_listview = null;
        this.pidAdapter = null;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.pidlist_item_pos = 0;
        this.channel_type = null;
        this.serviceInfo = new service();
        this.current_service_type = 0;
        this.current_service_index = 0;
        this.regionInfo = new region_info();
        this.satInfo = new dvb_s_sat();
        this.tpInfo = new dvb_s_tp();
        this.current_dig = null;
        this.pidedit_rl = null;
        this.videoPid_title = null;
        this.System_type = 0;
        this.t_tpInfo = new dvb_t_tp();
        this.areaInfo = new dvb_t_area();
        this.mSHCallback = new C01201();
    }

    private void setBackgroundAlpha(int transparency) {
        this.pidedit_rl = (RelativeLayout) findViewById(R.id.pidedit_rl);
        this.pidedit_rl.getBackground().setAlpha(transparency);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        Intent stopMusicIntent = new Intent();
        stopMusicIntent.setAction("com.android.music.musicservicecommand.pause");
        stopMusicIntent.putExtra("command", "stop");
//        sendBroadcastAsUser(stopMusicIntent, UserHandle.ALL);
        setContentView(R.layout.pidedit_activity);
        initMessagehandle();
        this.videoPid_title = (TextView) findViewById(R.id.videoPid_title);
        initData();
        initView();
    }

    protected void onResume() {
        super.onResume();
        enableMwMessageCallback(this.mwMsgHandler);
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
        if (MW.ts_player_play_current(true, false) == 0) {
        }
        if (MW.channel_edit_init() == 0) {
            SETTINGS.set_screen_mode();
        }
    }

    protected void onPause() {
        if (this.current_dig != null) {
            this.current_dig.dismiss();
        }
        MW.channel_edit_exit();
        MW.ts_player_stop_play();
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void initData() {
        this.System_type = MW.get_system_tuner_config();
        if (this.System_type == 0 || this.System_type == 2) {
            if (MW.db_dvb_s_get_sat_count(0) == 0) {
                SETTINGS.makeText(this, R.string.no_satellite, 0);
                finishMyself();
                return;
            } else if (MW.db_dvb_s_get_current_tp_info(0, this.tpInfo) == 0) {
                System.out.println(" tpInfo.sat_index  : " + this.tpInfo.sat_index);
                System.out.println(" tpInfo.tp_index  : " + this.tpInfo.tp_index);
                if (MW.db_dvb_s_get_tp_count(0, this.tpInfo.sat_index) == 0) {
                    SETTINGS.makeText(this, R.string.add_tp, 0);
                    finishMyself();
                    return;
                }
            } else {
                SETTINGS.makeText(this, R.string.add_tp, 0);
                finishMyself();
                return;
            }
        } else if (MW.db_dvb_t_get_area_count() == 0) {
            finishMyself();
            return;
        } else if (MW.db_dvb_t_get_current_tp_info(this.t_tpInfo) == 0) {
            System.out.println(" tpInfo.sat_index  : " + this.t_tpInfo.area_index);
            System.out.println(" tpInfo.tp_index  : " + this.t_tpInfo.tp_index);
            if (MW.db_dvb_t_get_tp_count(this.t_tpInfo.area_index) == 0) {
                finishMyself();
                return;
            }
        } else {
            SETTINGS.makeText(this, R.string.add_tp, 0);
            finishMyself();
            return;
        }
        MW.db_service_set_in_region_mode();
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
        this.mFirstVisibleItem = this.current_service_index;
    }

    private void initView() {
        this.pidedit_listview = (CustomListView) findViewById(R.id.pidedit_listview);
        this.pidAdapter = new PidAdapter(this);
        this.pidedit_listview.setAdapter(this.pidAdapter);
        this.pidedit_listview.setChoiceMode(1);
        this.pidedit_listview.setCustomSelection(this.mFirstVisibleItem);
        this.pidedit_listview.setCustomScrollListener(new C01212());
        this.pidedit_listview.setOnItemSelectedListener(new C01223());
        this.pidedit_listview.setCustomKeyListener(new listOnKeyListener());
        this.pidedit_listview.setVisibleItemCount(11);
        this.channel_type = (TextView) findViewById(R.id.channeltype_txt);
        RefreshShowPidEditAll(true);
        setBackgroundAlpha(220);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                ShowSaveDialog(4);
                break;
            case DtvBaseActivity.KEYCODE_SAT /*55*/:
                ShowSaveDialog(6);
                return true;
            case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                ShowSaveDialog(5);
                return true;
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                DoAddAction();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                ShowSaveDialog(4);
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void DoAddAction() {
        this.current_dig = new AddDialog(this, R.style.MyDialog);
        this.current_dig.show();
        this.current_dig.getWindow().addFlags(2);
    }

    private void DoEditAction() {
        this.current_dig = new EditDialog(this, R.style.MyDialog);
        this.current_dig.show();
        this.current_dig.getWindow().addFlags(2);
    }

    private void ShowSaveDialog(int event) {
        if (MW.channel_edit_check_data_modified()) {
            Dialog saveDataDialog = new SaveDia(this, R.style.MyDialog, event);
            saveDataDialog.show();
            saveDataDialog.getWindow().addFlags(2);
            return;
        }
        switch (event) {
            case MW.f0xe3564d8 /*5*/:
                RefreshTvRadio();
            case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
                if (MW.db_get_total_service_count() > 0) {
                    SelectSatAction();
                }
            default:
                finish();
        }
    }

    private void RefreshTvRadio() {
        switch (MW.ts_player_play_switch_tv_radio(true)) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                RefreshShowPidEditAll(true);
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                SETTINGS.makeText(this, R.string.NO_CHANNEL, 0);
            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                SETTINGS.makeText(this, R.string.NO_VIDEO_CHANNEL, 0);
            case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                SETTINGS.makeText(this, R.string.NO_AUDIO_CHANNEL, 0);
            default:
        }
    }

    private void SelectSatAction() {
        RegionSelectDialog dia = new RegionSelectDialog(this, R.style.MyDialog);
        dia.show();
        WindowManager.LayoutParams lp = dia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dia.getWindow().setAttributes(lp);
        dia.getWindow().addFlags(2);
    }

    private void RefreshShowPidEditAll(boolean bchedit_init) {
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            SETTINGS.set_led_channel(this.serviceInfo.channel_number_display);
            if (this.current_service_type == 0) {
                this.channel_type.setText(R.string.TvChannelList);
                this.videoPid_title.setVisibility(View.VISIBLE);
            } else {
                this.channel_type.setText(R.string.RadioChannelList);
                this.videoPid_title.setVisibility(View.INVISIBLE);
            }
            this.mFirstVisibleItem = this.current_service_index;
            this.pidedit_listview.setFocusableInTouchMode(true);
            this.pidedit_listview.requestFocus();
            this.pidedit_listview.requestFocusFromTouch();
            this.pidedit_listview.setCustomSelection(this.mFirstVisibleItem);
            if (bchedit_init) {
                MW.channel_edit_init();
            }
            this.pidAdapter.notifyDataSetChanged();
        }
    }

    private void initMessagehandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }
}
