package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import th.dtv.CustomListView;
import th.dtv.DigitsEditText;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.StorageDevice;
import th.dtv.StorageDevice.DeviceItem;
import th.dtv.util.BissDataHelper;
import th.dtv.util.BissDataInfo;
import th.dtv.util.DtvDbHelper;

public class BissActivity extends DtvBaseActivity {
    private static BissDataHelper helper;
    StorageDeviceDialog DeviceInfoDia;
    ArrayList<Boolean> ItemSelStatus;
    ArrayList<Integer> SelBissItem;
    private BissAdapter bissAdapter;
    private int biss_item_pos;
    private CustomListView biss_list;
    Dialog dialog;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    private TextView select_txt;
    private int storageDeviceCount;
    private StorageDevice storageDeviceInfo;

    /* renamed from: th.dtv.activity.BissActivity.1 */
    class C00941 implements OnScrollListener {
        C00941() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            BissActivity.this.mFirstVisibleItem = firstVisibleItem;
            BissActivity.this.mVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.BissActivity.2 */
    class C00952 implements OnItemSelectedListener {
        C00952() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            BissActivity.this.biss_item_pos = pos;
            if (BissActivity.this.ItemSelStatus == null) {
                return;
            }
            if (((Boolean) BissActivity.this.ItemSelStatus.get(BissActivity.this.biss_item_pos)).booleanValue()) {
                BissActivity.this.ShowUnSelectTipsView();
            } else {
                BissActivity.this.ShowSelectTipsView();
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public class AddDialog extends Dialog {
        private BissDataInfo bissDataInfo;
        EditText key_et;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        DigitsEditText mFreq;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mNum;
        TextView mTitle;
        EditText ssid_et;

        /* renamed from: th.dtv.activity.BissActivity.AddDialog.1 */

        /* renamed from: th.dtv.activity.BissActivity.AddDialog.2 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.biss_add_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.mNum = (TextView) findViewById(R.id.item_no);
            this.mFreq = (DigitsEditText) findViewById(R.id.edittext_frequency);
            this.ssid_et = (EditText) findViewById(R.id.edittext_ssid);
            this.key_et = (EditText) findViewById(R.id.edittext_key);
            this.bissDataInfo = new BissDataInfo();
            this.bissDataInfo.index = BissActivity.helper.getCount();
            this.bissDataInfo.freq = 0;
            this.bissDataInfo.ssid = "";
            this.bissDataInfo.key = "";
            if (this.bissDataInfo != null) {
                this.mNum.setText("" + (this.bissDataInfo.index + 1));
                this.mFreq.setText("" + this.bissDataInfo.freq);
                this.ssid_et.setText(this.bissDataInfo.ssid);
                this.key_et.setText(this.bissDataInfo.key);
            }
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            return super.onKeyDown(keyCode, event);
        }

        public AddDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.mNum = null;
            this.mFreq = null;
            this.ssid_et = null;
            this.key_et = null;
            this.bissDataInfo = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    private class BissAdapter extends BaseAdapter {
        private BissDataInfo bissDataInfo;
        private LayoutInflater mInflater;
        private Context mcontext;

        class ViewHolder {
            TextView biss_freq;
            ImageView biss_img;
            TextView biss_key;
            TextView biss_no;
            TextView biss_ssid;

            ViewHolder() {
            }
        }

        public BissAdapter(Context context) {
            this.bissDataInfo = null;
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return BissActivity.helper.getCount();
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.biss_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.biss_no = (TextView) convertView.findViewById(R.id.biss_no);
                localViewHolder.biss_freq = (TextView) convertView.findViewById(R.id.biss_freq);
                localViewHolder.biss_ssid = (TextView) convertView.findViewById(R.id.biss_ssid);
                localViewHolder.biss_key = (TextView) convertView.findViewById(R.id.biss_key);
                localViewHolder.biss_img = (ImageView) convertView.findViewById(R.id.biss_select_img);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            this.bissDataInfo = BissActivity.helper.getBissDataInfoByIndex(position);
            if (this.bissDataInfo != null) {
                localViewHolder.biss_no.setText((position + 1) + "");
                localViewHolder.biss_freq.setText("" + this.bissDataInfo.freq);
                localViewHolder.biss_ssid.setText(this.bissDataInfo.ssid);
                localViewHolder.biss_key.setText(this.bissDataInfo.key);
            } else {
                Log.e("LEE", "bissDataInfo is null");
            }
            if (BissActivity.this.ItemSelStatus != null) {
                if (((Boolean) BissActivity.this.ItemSelStatus.get(position)).booleanValue()) {
                    localViewHolder.biss_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.biss_img.setVisibility(View.INVISIBLE);
                }
            }
            return convertView;
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;

        /* renamed from: th.dtv.activity.BissActivity.DeleteDia.1 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class EditDialog extends Dialog {
        private BissDataInfo bissDataInfo;
        EditText key_et;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        DigitsEditText mFreq;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mNum;
        TextView mTitle;
        EditText ssid_et;

        /* renamed from: th.dtv.activity.BissActivity.EditDialog.1 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.biss_add_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.mNum = (TextView) findViewById(R.id.item_no);
            this.mFreq = (DigitsEditText) findViewById(R.id.edittext_frequency);
            this.ssid_et = (EditText) findViewById(R.id.edittext_ssid);
            this.key_et = (EditText) findViewById(R.id.edittext_key);
            this.mTitle.setText(BissActivity.this.getResources().getString(R.string.edit));
            this.bissDataInfo = BissActivity.helper.getBissDataInfoByIndex(BissActivity.this.biss_item_pos);
            if (this.bissDataInfo != null) {
                this.mNum.setText("" + (this.bissDataInfo.index + 1));
                this.mFreq.setText("" + this.bissDataInfo.freq);
                this.ssid_et.setText(this.bissDataInfo.ssid);
                this.key_et.setText(this.bissDataInfo.key);
            }
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            return super.onKeyDown(keyCode, event);
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.mNum = null;
            this.mFreq = null;
            this.ssid_et = null;
            this.key_et = null;
            this.bissDataInfo = null;
            this.mContext = context;
        }
    }

    public class StorageDeviceDialog extends Dialog {
        List<String> items;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        private int mInorOut;
        View mLayoutView;
        private CustomListView mListView;
        private int temp_no;

        /* renamed from: th.dtv.activity.BissActivity.StorageDeviceDialog.1 */
        class C01021 implements OnItemSelectedListener {
            C01021() {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Log.e("BissActivity", "mListView position =" + position);
                StorageDeviceDialog.this.temp_no = position;
                StorageDeviceDialog.this.mListView.setItemChecked(position, true);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        }

        /* renamed from: th.dtv.activity.BissActivity.StorageDeviceDialog.2 */

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.storagedevice_sel_dialog);
            ((TextView) findViewById(R.id.TextView_Title)).setText(BissActivity.this.getResources().getString(R.string.SelStorageDevice));
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            this.items = new ArrayList();
            this.items.clear();
            for (int i = 0; i < BissActivity.this.storageDeviceInfo.getDeviceCount(); i++) {
                DeviceItem item = BissActivity.this.storageDeviceInfo.getDeviceItem(i);
                if (item.VolumeName != null) {
                    if (item.VolumeName.indexOf("[udisk") == 1) {
                        this.items.add(" " + BissActivity.this.getResources().getString(R.string.udisk) + " " + ((BissActivity.this.storageDeviceInfo.getDeviceCount() - 1) - i));
                    } else {
                        this.items.add(item.VolumeName + "");
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(BissActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, this.items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setItemChecked(0, true);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemSelectedListener(new C01021());
        }

        protected void onStop() {
            super.onStop();
        }

        public StorageDeviceDialog(Context context, int theme, int InOrOut) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.temp_no = 0;
            this.mInorOut = 1;
            this.mContext = context;
            this.mInorOut = InOrOut;
        }
    }

    private class listOnKeyListener implements OnKeyListener {
        private listOnKeyListener() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                        BissActivity.this.RefreshSatSelectImage();
                        break;
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        BissActivity.this.DoEditAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        BissActivity.this.DoDeleteAction();
                        return true;
                }
            }
            return false;
        }
    }

    public static native boolean biss_set_info(BissDataInfo[] bissDataInfoArr);

    public BissActivity() {
        this.bissAdapter = null;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.biss_item_pos = 0;
        this.ItemSelStatus = null;
        this.storageDeviceCount = 0;
        this.SelBissItem = new ArrayList();
        this.dialog = null;
    }

    static {
        helper = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biss_activity);
        initData();
        initView();
        initTipView();
    }

    private void initData() {
        helper = BissDataHelper.getInstance(this);
        BissDataInfo[] testInfo = helper.getAllBissDataInfo();
        if (testInfo != null) {
            for (int i = 0; i < testInfo.length; i++) {
                Log.e("LEE", "testInfo[" + i + "].index" + testInfo[i].index);
                Log.e("LEE", "testInfo[" + i + "].freq" + testInfo[i].freq);
                Log.e("LEE", "testInfo[" + i + "].ssid" + testInfo[i].ssid);
                Log.e("LEE", "testInfo[" + i + "].key" + testInfo[i].key);
                Log.e("LEE", "---------------------------------------------------------");
            }
        }
        initItemSelStatus();
    }

    private void initItemSelStatus() {
        int count = helper.getCount();
        if (count > 0) {
            this.ItemSelStatus = new ArrayList();
            for (int i = 0; i < count; i++) {
                this.ItemSelStatus.add(i, Boolean.valueOf(false));
            }
            return;
        }
        this.ItemSelStatus = null;
    }

    private void initView() {
        this.biss_list = (CustomListView) findViewById(R.id.biss_listview);
        this.bissAdapter = new BissAdapter(this);
        this.biss_list.setAdapter(this.bissAdapter);
        this.biss_list.setChoiceMode(1);
        this.biss_list.setCustomSelection(this.mFirstVisibleItem);
        this.biss_list.setCustomScrollListener(new C00941());
        this.biss_list.setOnItemSelectedListener(new C00952());
        this.biss_list.setCustomKeyListener(new listOnKeyListener());
        this.biss_list.setVisibleItemCount(11);
    }

    private void initTipView() {
        this.select_txt = (TextView) findViewById(R.id.select_txt);
    }

    private void ShowSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.SelectSat));
    }

    private void ShowUnSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.UnSelectSat));
    }

    private void RefreshSatSelectImage() {
        if (this.ItemSelStatus != null) {
            if (((Boolean) this.ItemSelStatus.get(this.biss_item_pos)).booleanValue()) {
                this.ItemSelStatus.set(this.biss_item_pos, Boolean.valueOf(false));
                ShowSelectTipsView();
            } else {
                this.ItemSelStatus.set(this.biss_item_pos, Boolean.valueOf(true));
                ShowUnSelectTipsView();
            }
        }
        this.biss_list.setSelectionFromTop(this.biss_item_pos, this.mFirstVisibleItem);
        this.bissAdapter.notifyDataSetChanged();
    }

    private void getSelItemData() {
        this.SelBissItem.clear();
        int i = 0;
        while (i < helper.getCount()) {
            if (this.ItemSelStatus != null && ((Boolean) this.ItemSelStatus.get(i)).booleanValue()) {
                this.SelBissItem.add(Integer.valueOf(i));
                System.out.println("i  ::::: " + i);
            }
            i++;
        }
        if (this.SelBissItem.size() == 0 && this.biss_list != null && this.biss_list.getCount() != -1) {
            this.SelBissItem.add(Integer.valueOf(this.biss_item_pos));
        }
    }

    private void DoDeleteAction() {
        this.dialog = new DeleteDia(this, R.style.MyDialog);
        this.dialog.show();
        this.dialog.getWindow().addFlags(2);
    }

    private void DoEditAction() {
        this.dialog = new EditDialog(this, R.style.MyDialog);
        this.dialog.show();
        this.dialog.getWindow().addFlags(2);
    }

    private void DoAddFunction() {
        this.dialog = new AddDialog(this, R.style.MyDialog);
        this.dialog.show();
        this.dialog.getWindow().addFlags(2);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                ShowStorageDeviceDialog(1);
                return true;
            case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                ShowStorageDeviceDialog(0);
                return true;
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                DoAddFunction();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    protected void onResume() {
        super.onResume();
        if (this.storageDeviceInfo == null) {
            this.storageDeviceInfo = new StorageDevice(this);
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        this.storageDeviceInfo.finish(this);
        if (this.DeviceInfoDia != null && this.DeviceInfoDia.isShowing()) {
            this.DeviceInfoDia.dismiss();
        }
        helper.CloseDB();
        helper = null;
    }

    private boolean CheckKey(String key) {
        if (key == null) {
            return false;
        }
        char[] keyArray = key.toLowerCase().toCharArray();
        int i = 0;
        while (i < keyArray.length) {
            if ((keyArray[i] < '0' || keyArray[i] > '9') && (keyArray[i] < 'a' || keyArray[i] > 'f')) {
                return false;
            }
            i++;
        }
        return true;
    }

    public static void DoFunction(Context context) {
        helper = BissDataHelper.getInstance(context);
        if (helper != null) {
            BissDataInfo[] bsInfoList = null;
            if (helper.getCount() > 0) {
                bsInfoList = helper.getAllBissDataInfo();
            }
            biss_set_info(bsInfoList);
            return;
        }
        biss_set_info(null);
    }

    public boolean importSmartDB(String fromDbPath, String toDbPath) throws IOException, FileNotFoundException {
        return DtvDbHelper.getInstance(this).importDB(fromDbPath, toDbPath);
    }

    private void DoImportDB(String Path) {
        try {
            if (importSmartDB(Path + "/", "data/data/th.dtv/databases/")) {
                SETTINGS.makeText(this, R.string.RestoreDataSuccess, 0);
                DoFunction(this);
                helper = BissDataHelper.getInstance(this);
                initItemSelStatus();
                if (this.bissAdapter != null) {
                    this.bissAdapter.notifyDataSetChanged();
                    return;
                }
                return;
            }
            SETTINGS.makeText(this, R.string.FileNoFound, 0);
        } catch (FileNotFoundException e) {
            SETTINGS.makeText(this, R.string.FileNoFound, 0);
            e.printStackTrace();
        } catch (IOException e2) {
            SETTINGS.makeText(this, R.string.RestoreDataFailed, 0);
            e2.printStackTrace();
            helper = BissDataHelper.getInstance(this);
            initItemSelStatus();
            if (this.bissAdapter != null) {
                this.bissAdapter.notifyDataSetChanged();
            }
        }
    }

    public boolean exportSmartDB(String fromDbPath, String toDbPath) throws IOException, FileNotFoundException {
        return DtvDbHelper.getInstance(this).exportDB(fromDbPath, toDbPath);
    }

    private void DoExportDB(String Path) {
        if (BissDataHelper.getInstance(this).getCount() <= 0) {
            SETTINGS.makeText(this, R.string.NoData, 0);
            return;
        }
        try {
            if (exportSmartDB("data/data/th.dtv/databases/", Path + "/")) {
                SETTINGS.makeText(this, R.string.BackupDataSuccess, 0);
            } else {
                SETTINGS.makeText(this, R.string.FileNoFound, 0);
            }
        } catch (FileNotFoundException e) {
            SETTINGS.makeText(this, R.string.FileNoFound, 0);
            e.printStackTrace();
        } catch (IOException e2) {
            SETTINGS.makeText(this, R.string.BackupDataFailed, 0);
            e2.printStackTrace();
        }
    }

    private void ShowStorageDeviceDialog(int inOrOut) {
        if (this.storageDeviceInfo == null) {
            return;
        }
        if (this.storageDeviceInfo.getDeviceCount() > 0) {
            Log.e("BissActivity", "have device : " + this.storageDeviceInfo.getDeviceCount());
            if (this.storageDeviceInfo.getDeviceCount() > 1) {
                this.DeviceInfoDia = new StorageDeviceDialog(this, R.style.MyDialog, inOrOut);
                this.DeviceInfoDia.show();
                LayoutParams lp = this.DeviceInfoDia.getWindow().getAttributes();
                lp.dimAmount = 0.0f;
                this.DeviceInfoDia.getWindow().setAttributes(lp);
                this.DeviceInfoDia.getWindow().addFlags(2);
                return;
            }
            String Path = this.storageDeviceInfo.getDeviceItem(0).Path;
            System.out.println(Path);
            switch (inOrOut) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    DoImportDB(Path);
                    return;
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    DoExportDB(Path);
                    return;
                default:
                    return;
            }
        }
        SETTINGS.makeText(this, R.string.no_storage, 0);
    }
}
