package th.dtv.activity;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.VideoView;
import java.util.ArrayList;
import java.util.List;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.PasswordDialog;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.book_event_data;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;
import th.dtv.mw_data.epg_extended_event;
import th.dtv.mw_data.epg_schedule_event;
import th.dtv.mw_data.service;
import th.dtv.util.HongKongDTMBHD;

public class EPGActivity extends DtvBaseActivity {
    private boolean bNeedShow;
    private CustomListView channel_list;
    private TextView current_date_time;
    private int current_service_index;
    private int current_service_type;
    private date date;
    private date datePre;
    private date_time dateTime;
    private date dateTmp;
    private LinearLayout detail_ll;
    private Dialog dia_currentDialog;
    Dialog dialog;
    private boolean disableUpdate;
    private EPGChannelListAdapter epgChannelListAdapter;
    private epg_extended_event epgExtendedEvent;
    private EpgInfoAdapter epgInfo_Adapter;
    private int epgInfo_item_pos;
    private CustomListView epgInfo_list;
    private int epgScheduleDayoffset;
    private epg_schedule_event epgScheduleEvent;
    private int epgScheduleEventCount;
    private int[] epgScheduleEventIds;
    private LinearLayout epg_bg;
    private TextView epg_event_text;
    private TextView epgchannel_type;
    private boolean fisttime_enter;
    private LinearLayout ll_tvradio_list;
    private Handler localMsgHandler;
    private int mEpgFirstVisibleItem;
    private int mEpgVisibleItemCount;
    private int mFirstVisibleItem;
    Callback mSHCallback;
    private int mVisibleItemCount;
    private boolean mbLauncherFromDtv;
    private Handler mwMsgHandler;
    private LinearLayout noepg_ll;
    private TextView play_status;
    private TextView pos0;
    private TextView pos1;
    private TextView pos2;
    private TextView pos3;
    private TextView pos4;
    private TextView pos5;
    private TextView pos6;
    private LinearLayout progress_ll;
    private Dialog pswdDig;
    private service serviceInfo;
    TextView[] text;
    private book_event_data tmpBookEvent;
    VideoView videoView;
    int[] weekStringID;
    private RadioGroup weekdays;

    /* renamed from: th.dtv.activity.EPGActivity.1 */
    class C01441 implements OnScrollListener {
        C01441() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            EPGActivity.this.mEpgFirstVisibleItem = firstVisibleItem;
            EPGActivity.this.mEpgVisibleItemCount = visibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.2 */
    class C01452 implements OnItemSelectedListener {
        C01452() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            EPGActivity.this.epgInfo_item_pos = position;
            EPGActivity.this.UpdateDetailView(EPGActivity.this.epgInfo_item_pos);
            if (EPGActivity.this.epgScheduleEventIds != null) {
                EPGActivity.this.UpdateScheduleExtendedEpgInfo(EPGActivity.this.epgScheduleEventIds[position]);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.3 */
    class C01463 implements OnKeyListener {
        C01463() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                        if (((RadioButton) EPGActivity.this.weekdays.getChildAt(0)).isChecked()) {
                            EPGActivity.this.channel_list.requestFocus();
//                            EPGActivity.this.channel_list.setSelectionFromTop(EPGActivity.this.current_service_index, EPGActivity.this.mFirstVisibleItem);
                            EPGActivity.this.epgChannelListAdapter.notifyDataSetChanged();
                            if (EPGActivity.this.channel_list.getSelectedView() != null) {
                                EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                            }
                            if (EPGActivity.this.epgInfo_list.getSelectedItemPosition() >= 0 && EPGActivity.this.epgInfo_list.getSelectedView() != null) {
                                EPGActivity.this.epgInfo_list.getSelectedView().setBackgroundResource(R.drawable.list_446_49_sele_expired);
                            }
                            return true;
                        }
                        EPGActivity.this.UpdateScheduleEpgInfo_PrevDay();
                        if (EPGActivity.this.epgInfo_list.getCount() != 0) {
                            if (EPGActivity.this.channel_list.getSelectedView() != null) {
                                EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                            }
                            EPGActivity.this.epgInfo_list.requestFocus();
                            EPGActivity.this.epgInfo_list.setCustomSelection(EPGActivity.this.epgInfo_item_pos);
                        } else if (EPGActivity.this.channel_list.getSelectedView() != null) {
                            EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        }
                        return true;
                    case MW.RET_MEMORY_ERROR /*22*/:
                        EPGActivity.this.UpdateScheduleEpgInfo_NextDay();
                        if (EPGActivity.this.epgInfo_list.getCount() != 0) {
                            if (EPGActivity.this.channel_list.getSelectedView() != null) {
                                EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                            }
                            EPGActivity.this.epgInfo_list.requestFocus();
                            EPGActivity.this.epgInfo_list.setCustomSelection(EPGActivity.this.epgInfo_item_pos);
                        } else if (EPGActivity.this.channel_list.getSelectedView() != null) {
                            EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        }
                        return true;
                    case MW.RET_INVALID_TYPE /*23*/:
                        EPGActivity.this.bookEvent();
                        return true;
                    case DtvBaseActivity.KEYCODE_INFO /*185*/:
                        if (EPGActivity.this.epgScheduleEventIds != null && MW.epg_get_schedule_extened_event_by_event_id(EPGActivity.this.epgScheduleDayoffset, EPGActivity.this.epgScheduleEventIds[EPGActivity.this.epgInfo_item_pos], EPGActivity.this.serviceInfo, EPGActivity.this.date, EPGActivity.this.epgExtendedEvent) == 0 && MW.epg_get_schedule_event_by_event_id(EPGActivity.this.epgScheduleDayoffset, EPGActivity.this.epgScheduleEventIds[EPGActivity.this.epgInfo_item_pos], EPGActivity.this.serviceInfo, EPGActivity.this.date, EPGActivity.this.epgScheduleEvent) == 0 && ((EPGActivity.this.epgExtendedEvent.event_text != null && EPGActivity.this.epgExtendedEvent.event_text.length() > 0) || EPGActivity.this.epgScheduleEvent.has_extended_event_info)) {
                            if (EPGActivity.this.epgScheduleEvent.event_text == null || EPGActivity.this.epgScheduleEvent.event_text.length() <= 0) {
                                EPGActivity.this.showDialog(EPGActivity.this.epgExtendedEvent.event_text);
                            } else {
                                EPGActivity.this.showDialog(EPGActivity.this.epgScheduleEvent.event_text + EPGActivity.this.epgExtendedEvent.event_text);
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            }
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    return true;
                default:
                    return false;
            }
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.4 */
    class C01474 implements OnCheckedChangeListener {
        C01474() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.5 */
    class C01485 implements OnScrollListener {
        C01485() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            EPGActivity.this.mFirstVisibleItem = firstVisibleItem;
            EPGActivity.this.mVisibleItemCount = visibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.6 */
    class C01496 implements OnItemSelectedListener {
        C01496() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            if (MW.db_get_service_info(EPGActivity.this.current_service_type, position, EPGActivity.this.serviceInfo) == 0) {
                if (EPGActivity.this.fisttime_enter) {
                    EPGActivity.this.fisttime_enter = false;
                } else {
                    int Ret = MW.ts_player_play(EPGActivity.this.current_service_type, position, 50, false);
                    if (Ret == 0) {
                        EPGActivity.this.CheckParentLockAndShowPasswd("2");
                    } else if (Ret == 44) {
                        EPGActivity.this.CheckChannelLockAndShowPasswd("2");
                    }
                }
                EPGActivity.this.current_service_index = EPGActivity.this.serviceInfo.service_index;
                EPGActivity.this.bNeedShow = true;
                EPGActivity.this.UpdateScheduleEpgInfo(true);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.7 */
    class C01507 implements OnKeyListener {
        C01507() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                case MW.RET_INVALID_TP_INDEX /*20*/:
                case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                    EPGActivity.this.epgInfo_item_pos = 0;
                    EPGActivity.this.epgInfo_list.setSelection(EPGActivity.this.epgInfo_item_pos);
                    int currentPos = EPGActivity.this.channel_list.getSelectedItemPosition();
                    if (EPGActivity.this.channel_list.getSelectedView() != null) {
                        EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        break;
                    }
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    if (MW.db_get_current_service_info(EPGActivity.this.serviceInfo) == 0) {
                        EPGActivity.this.epgInfo_list.requestFocus();
//                        EPGActivity.this.epgInfo_list.setSelectionFromTop(EPGActivity.this.epgInfo_item_pos, EPGActivity.this.mEpgFirstVisibleItem);
                        if (EPGActivity.this.epgInfo_list.getSelectedItemPosition() < 0) {
                            EPGActivity.this.UpdateScheduleEpgInfo_PrevDay();
                            if (EPGActivity.this.epgInfo_list.getCount() != 0) {
                                if (EPGActivity.this.channel_list.getSelectedView() != null) {
                                    EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                                }
                                EPGActivity.this.epgInfo_list.requestFocus();
//                                EPGActivity.this.epgInfo_list.setSelectionFromTop(EPGActivity.this.epgInfo_item_pos, EPGActivity.this.mEpgFirstVisibleItem);
                            } else if (EPGActivity.this.channel_list.getSelectedView() != null) {
                                EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                            }
                        } else if (EPGActivity.this.channel_list.getSelectedView() != null) {
                            EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                        }
                    }
                    EPGActivity.this.epgInfo_Adapter.notifyDataSetChanged();
                    return true;
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (MW.db_get_current_service_info(EPGActivity.this.serviceInfo) != 0) {
                        return true;
                    }
                    EPGActivity.this.epgInfo_list.requestFocus();
//                    EPGActivity.this.epgInfo_list.setSelectionFromTop(EPGActivity.this.epgInfo_item_pos, EPGActivity.this.mEpgFirstVisibleItem);
                    EPGActivity.this.epgInfo_Adapter.notifyDataSetChanged();
                    if (EPGActivity.this.epgInfo_list.getSelectedItemPosition() < 0) {
                        EPGActivity.this.UpdateScheduleEpgInfo_NextDay();
                        if (EPGActivity.this.epgInfo_list.getCount() != 0) {
                            if (EPGActivity.this.channel_list.getSelectedView() != null) {
                                EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                            }
                            EPGActivity.this.epgInfo_list.requestFocus();
//                            EPGActivity.this.epgInfo_list.setSelectionFromTop(EPGActivity.this.epgInfo_item_pos, EPGActivity.this.mEpgFirstVisibleItem);
                            return true;
                        } else if (EPGActivity.this.channel_list.getSelectedView() == null) {
                            return true;
                        } else {
                            EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                            return true;
                        }
                    } else if (EPGActivity.this.channel_list.getSelectedView() == null) {
                        return true;
                    } else {
                        EPGActivity.this.channel_list.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                        return true;
                    }
                case MW.RET_INVALID_TYPE /*23*/:
                    EPGActivity.this.gotoPlayWindow();
                    return true;
                case DtvBaseActivity.KEYCODE_INFO /*185*/:
                    if (EPGActivity.this.epgScheduleEventIds == null || MW.epg_get_schedule_extened_event_by_event_id(EPGActivity.this.epgScheduleDayoffset, EPGActivity.this.epgScheduleEventIds[EPGActivity.this.epgInfo_item_pos], EPGActivity.this.serviceInfo, EPGActivity.this.date, EPGActivity.this.epgExtendedEvent) != 0 || MW.epg_get_schedule_event_by_event_id(EPGActivity.this.epgScheduleDayoffset, EPGActivity.this.epgScheduleEventIds[EPGActivity.this.epgInfo_item_pos], EPGActivity.this.serviceInfo, EPGActivity.this.date, EPGActivity.this.epgScheduleEvent) != 0) {
                        return true;
                    }
                    if ((EPGActivity.this.epgExtendedEvent.event_text == null || EPGActivity.this.epgExtendedEvent.event_text.length() <= 0) && !EPGActivity.this.epgScheduleEvent.has_extended_event_info) {
                        return true;
                    }
                    if (EPGActivity.this.epgScheduleEvent.event_text == null || EPGActivity.this.epgScheduleEvent.event_text.length() <= 0) {
                        EPGActivity.this.showDialog(EPGActivity.this.epgExtendedEvent.event_text);
                        return true;
                    }
                    EPGActivity.this.showDialog(EPGActivity.this.epgScheduleEvent.event_text + EPGActivity.this.epgExtendedEvent.event_text);
                    return true;
                case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                    switch (MW.ts_player_play_switch_tv_radio(false)) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            EPGActivity.this.RefreshShowEpgChannelList();
                            EPGActivity.this.epgInfo_item_pos = 0;
                            EPGActivity.this.epgInfo_list.setCustomSelection(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.RefreshEpgInfoItemBack(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.CheckParentLockAndShowPasswd("3");
                            return true;
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            SETTINGS.makeText(EPGActivity.this, R.string.NO_CHANNEL, 0);
                            return true;
                        case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                            SETTINGS.makeText(EPGActivity.this, R.string.NO_VIDEO_CHANNEL, 0);
                            return true;
                        case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                            SETTINGS.makeText(EPGActivity.this, R.string.NO_AUDIO_CHANNEL, 0);
                            return true;
                        case MW.RET_SERVICE_LOCK /*44*/:
                            EPGActivity.this.RefreshShowEpgChannelList();
                            EPGActivity.this.epgInfo_item_pos = 0;
                            EPGActivity.this.epgInfo_list.setCustomSelection(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.RefreshEpgInfoItemBack(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.CheckChannelLockAndShowPasswd("3");
                            return true;
                        default:
                            return true;
                    }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.8 */
    class C01518 implements Callback {
        C01518() {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("EPGActivity", "surfaceChanged");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("EPGActivity", "surfaceCreated");
            try {
                initSurface(holder);
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("EPGActivity", "surfaceDestroyed");
        }

        private void initSurface(SurfaceHolder h) {
            Canvas c = null;
            try {
                Log.d("EPGActivity", "initSurface");
                c = h.lockCanvas();
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            } catch (Throwable th) {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /* renamed from: th.dtv.activity.EPGActivity.9 */
    class C01529 implements OnKeyListener {
        C01529() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case DtvBaseActivity.KEYCODE_MENU /*82*/:
                        if (EPGActivity.this.pswdDig != null) {
                            EPGActivity.this.pswdDig.dismiss();
                        }
                        if (!EPGActivity.this.mbLauncherFromDtv) {
                            SETTINGS.set_green_led(false);
                            SETTINGS.send_led_msg("NENU");
                        }
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName("com.android.DTVLauncher", "com.android.DTVLauncher.DTVLauncher"));
                        intent.addFlags(268435456);
                        intent.addFlags(67108864);
                        EPGActivity.this.startActivity(intent);
                        return true;
                    case DtvBaseActivity.KEYCODE_EPG /*2013*/:
                        System.out.println("=====onKey epg epg ");
                        if (EPGActivity.this.pswdDig != null) {
                            EPGActivity.this.pswdDig.dismiss();
                        }
                        if (!EPGActivity.this.mbLauncherFromDtv) {
                            SETTINGS.set_green_led(false);
                            SETTINGS.send_led_msg("NENU");
                        }
                        EPGActivity.this.finishMyself();
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    if (EPGActivity.this.pswdDig != null) {
                        EPGActivity.this.pswdDig.dismiss();
                    }
                    if (!EPGActivity.this.mbLauncherFromDtv) {
                        SETTINGS.set_green_led(false);
                        SETTINGS.send_led_msg("NENU");
                    }
                    EPGActivity.this.finishMyself();
                    return true;
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                case MW.RET_INVALID_TP_INDEX /*20*/:
                case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                    if (EPGActivity.this.pswdDig != null) {
                        EPGActivity.this.pswdDig.dismiss();
                    }
                    EPGActivity.this.channel_list.onKey(v, keyCode, event);
                    return true;
                case MW.RET_INVALID_TYPE /*23*/:
                    if (EPGActivity.this.pswdDig != null) {
                        EPGActivity.this.pswdDig.dismiss();
                    }
                    EPGActivity.this.gotoPlayWindow();
                    return true;
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    if (EPGActivity.this.pswdDig != null) {
                        EPGActivity.this.pswdDig.dismiss();
                    }
                    EPGActivity.this.DumpBookListInfo();
                    return true;
                case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                    switch (MW.ts_player_play_switch_tv_radio(false)) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            if (EPGActivity.this.pswdDig != null) {
                                EPGActivity.this.pswdDig.dismiss();
                            }
                            EPGActivity.this.RefreshShowEpgChannelList();
                            EPGActivity.this.epgInfo_item_pos = 0;
                            EPGActivity.this.epgInfo_list.setCustomSelection(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.RefreshEpgInfoItemBack(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.CheckParentLockAndShowPasswd("4");
                            return true;
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            SETTINGS.makeText(EPGActivity.this, R.string.NO_CHANNEL, 0);
                            return true;
                        case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                            SETTINGS.makeText(EPGActivity.this, R.string.NO_VIDEO_CHANNEL, 0);
                            return true;
                        case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                            SETTINGS.makeText(EPGActivity.this, R.string.NO_AUDIO_CHANNEL, 0);
                            return true;
                        case MW.RET_SERVICE_LOCK /*44*/:
                            if (EPGActivity.this.pswdDig != null) {
                                EPGActivity.this.pswdDig.dismiss();
                            }
                            EPGActivity.this.RefreshShowEpgChannelList();
                            EPGActivity.this.epgInfo_item_pos = 0;
                            EPGActivity.this.epgInfo_list.setCustomSelection(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.RefreshEpgInfoItemBack(EPGActivity.this.epgInfo_item_pos);
                            EPGActivity.this.CheckChannelLockAndShowPasswd("4");
                            return true;
                        default:
                            return true;
                    }
            }
            return false;
        }
    }

    public class BookTypeSelectDialog extends Dialog {
        private ArrayAdapter mAdapter;
        private int mBookType;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;
        private int temp_BookType;

        /* renamed from: th.dtv.activity.EPGActivity.BookTypeSelectDialog.1 */
        class C01531 implements OnItemSelectedListener {
            C01531() {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                BookTypeSelectDialog.this.mListView.setItemChecked(position, true);
                if (BookTypeSelectDialog.this.mBookType == 0) {
                    if (position == 0) {
                        BookTypeSelectDialog.this.temp_BookType = 1;
                    } else if (position == 1) {
                        BookTypeSelectDialog.this.temp_BookType = 2;
                    } else {
                        BookTypeSelectDialog.this.temp_BookType = 0;
                    }
                } else if (position == 0) {
                    BookTypeSelectDialog.this.temp_BookType = 0;
                } else if (position == 1) {
                    BookTypeSelectDialog.this.temp_BookType = 1;
                } else if (position == 2) {
                    BookTypeSelectDialog.this.temp_BookType = 2;
                } else {
                    BookTypeSelectDialog.this.temp_BookType = 0;
                }
                Log.e("BOOK", "temp_BookType : " + BookTypeSelectDialog.this.temp_BookType);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        }

        /* renamed from: th.dtv.activity.EPGActivity.BookTypeSelectDialog.2 */
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.storagedevice_sel_dialog);
            EPGActivity.this.dia_currentDialog = this;
            ((TextView) findViewById(R.id.TextView_Title)).setText(EPGActivity.this.getResources().getString(R.string.SelBookType));
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            List<String> items = new ArrayList();
            items.clear();
            if (this.mBookType == 0) {
                items.add(EPGActivity.this.getString(R.string.Play));
                items.add(EPGActivity.this.getString(R.string.Record));
            } else {
                items.add(EPGActivity.this.getString(R.string.None));
                items.add(EPGActivity.this.getString(R.string.Play));
                items.add(EPGActivity.this.getString(R.string.Record));
            }
            this.mAdapter = new ArrayAdapter(EPGActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setItemChecked(0, true);
            this.mListView.setCustomSelection(this.mBookType);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemSelectedListener(new C01531());
        }

        protected void onStop() {
            super.onStop();
            EPGActivity.this.dia_currentDialog = null;
        }

        public BookTypeSelectDialog(Context context, int theme, int book_type) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBookType = 0;
            this.temp_BookType = 0;
            this.mContext = context;
            this.mBookType = book_type;
        }
    }

    public class DeleteDia extends Dialog {
        TextView channel_name;
        TextView deldia_title;
        TextView delete_content;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private int mbookType;
        private String mtitle;
        TextView program_name;
        TextView time;
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bookconflic_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnStart.setText(EPGActivity.this.getResources().getString(R.string.replace));
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.delete_content = (TextView) findViewById(R.id.delete_content);
            this.time = (TextView) findViewById(R.id.time);
            this.channel_name = (TextView) findViewById(R.id.channel_name);
            this.program_name = (TextView) findViewById(R.id.program_name);
            this.deldia_title.setText(this.mtitle);
            if (EPGActivity.this.tmpBookEvent != null) {
                System.out.println("  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(EPGActivity.this.tmpBookEvent.start_year), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_month), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_day), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_hour), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_minute), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_year), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_month), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_day), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_hour), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_minute)}) + " ## " + EPGActivity.this.tmpBookEvent.service_name + " ## " + EPGActivity.this.tmpBookEvent.event_name + " ## " + (EPGActivity.this.tmpBookEvent.book_type == 1 ? "Play" : "Record"));
                this.delete_content.setText(R.string.dywtreplace);
                this.time.setText(EPGActivity.this.getResources().getString(R.string.book_time) + " \uff1a " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(EPGActivity.this.tmpBookEvent.start_year), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_month), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_day), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_hour), Integer.valueOf(EPGActivity.this.tmpBookEvent.start_minute), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_year), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_month), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_day), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_hour), Integer.valueOf(EPGActivity.this.tmpBookEvent.end_minute)}));
                this.channel_name.setText(EPGActivity.this.getResources().getString(R.string.channel_name) + " \uff1a " + EPGActivity.this.tmpBookEvent.service_name);
                this.program_name.setText(EPGActivity.this.getResources().getString(R.string.Program_name) + " \uff1a " + EPGActivity.this.tmpBookEvent.event_name);
            }
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme, String title, int bookType) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mtitle = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.deldia_title = null;
            this.delete_content = null;
            this.time = null;
            this.channel_name = null;
            this.program_name = null;
            this.mContext = context;
            this.mtitle = title;
            this.mbookType = bookType;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class DetailDia extends Dialog {
        String content;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mcontent;
        TextView txtTitle;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.epg_detail_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.epg_detail_dialog, null);
            this.txtTitle = (TextView) findViewById(R.id.deldia_title);
            this.mcontent = (TextView) findViewById(R.id.delete_content);
            this.txtTitle.setText(EPGActivity.this.getResources().getString(R.string.Detail));
            this.mcontent.setGravity(3);
            this.mcontent.setSingleLine(false);
            this.mcontent.setText(this.content + "");
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case DtvBaseActivity.KEYCODE_INFO /*185*/:
                        dismiss();
                        return true;
                }
            }
            return super.onKeyDown(keyCode, event);
        }

        public DetailDia(Context context, int theme, String string) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.txtTitle = null;
            this.mcontent = null;
            this.content = null;
            this.mContext = context;
            this.content = string;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    private class EPGChannelListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView channel_name;
            TextView channel_num;
            ImageView channel_quality_icon;
            ImageView fav_icon;

            ViewHolder() {
            }
        }

        public EPGChannelListAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_get_service_count(EPGActivity.this.current_service_type);
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.epg_channel_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.channel_num = (TextView) convertView.findViewById(R.id.channellist_num_txt);
                localViewHolder.channel_name = (TextView) convertView.findViewById(R.id.channellist_name_txt);
                localViewHolder.channel_quality_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_sharp_img);
                localViewHolder.fav_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_fav_img);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.db_get_service_info(EPGActivity.this.current_service_type, pos, EPGActivity.this.serviceInfo) == 0) {
                localViewHolder.channel_num.setText(Integer.toString(EPGActivity.this.serviceInfo.channel_number_display));
                localViewHolder.channel_name.setText(EPGActivity.this.serviceInfo.service_name);
                if (SETTINGS.bHongKongDTMB) {
                    if (HongKongDTMBHD.isHongKongDTMBHDChannel(EPGActivity.this.serviceInfo.video_pid, EPGActivity.this.serviceInfo.audio_info[0].audio_pid) || EPGActivity.this.serviceInfo.is_hd) {
                        localViewHolder.channel_quality_icon.setImageResource(R.drawable.imagehd1);
                    } else {
                        localViewHolder.channel_quality_icon.setImageResource(0);
                    }
                } else if (EPGActivity.this.serviceInfo.is_hd) {
                    localViewHolder.channel_quality_icon.setImageResource(R.drawable.imagehd1);
                } else {
                    localViewHolder.channel_quality_icon.setImageResource(0);
                }
                if (EPGActivity.this.serviceInfo.is_scrambled) {
                    localViewHolder.fav_icon.setImageResource(R.drawable.coin_us_dollar);
                } else {
                    localViewHolder.fav_icon.setImageResource(0);
                }
            }
            return convertView;
        }
    }

    private class EpgInfoAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mcontext;

        class ViewHolder {
            TextView epg_book;
            TextView epg_detail;
            TextView epg_name;
            TextView epg_time;

            ViewHolder() {
            }
        }

        public EpgInfoAdapter(Context context) {
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return EPGActivity.this.epgScheduleEventCount;
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            System.out.println("epg info pos = " + pos + "convertView = " + convertView);
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.epg_column_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.epg_time = (TextView) convertView.findViewById(R.id.tv_back_time);
                localViewHolder.epg_name = (TextView) convertView.findViewById(R.id.tv_back_name);
                localViewHolder.epg_book = (TextView) convertView.findViewById(R.id.epg_book_log);
                localViewHolder.epg_detail = (TextView) convertView.findViewById(R.id.epg_detail_log);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.db_get_current_service_info(EPGActivity.this.serviceInfo) == 0) {
                if (MW.epg_get_schedule_event_by_event_id(EPGActivity.this.epgScheduleDayoffset, EPGActivity.this.epgScheduleEventIds[pos], EPGActivity.this.serviceInfo, EPGActivity.this.date, EPGActivity.this.epgScheduleEvent) == 0) {
                    localViewHolder.epg_time.setText(String.format("%02d:%02d - %02d:%02d", new Object[]{Integer.valueOf(EPGActivity.this.epgScheduleEvent.start_hour), Integer.valueOf(EPGActivity.this.epgScheduleEvent.start_minute), Integer.valueOf(EPGActivity.this.epgScheduleEvent.end_hour), Integer.valueOf(EPGActivity.this.epgScheduleEvent.end_minute)}));
                    if (EPGActivity.this.epgScheduleEvent.event_name.length() > 0) {
                        localViewHolder.epg_name.setText(EPGActivity.this.epgScheduleEvent.event_name);
                    } else {
                        localViewHolder.epg_name.setText(R.string.no_available_event);
                    }
                    if ((EPGActivity.this.epgScheduleEvent.event_text == null || EPGActivity.this.epgScheduleEvent.event_text.length() <= 0) && !EPGActivity.this.epgScheduleEvent.has_extended_event_info) {
                        localViewHolder.epg_detail.setVisibility(View.INVISIBLE);
                    } else {
                        localViewHolder.epg_detail.setVisibility(View.VISIBLE);
                    }
                    if (EPGActivity.this.epgScheduleEvent.playing_status == 2) {
                        convertView.setBackgroundResource(R.drawable.listview_item_bg_selector);
                        if (EPGActivity.this.epgScheduleEvent.book_status == 1) {
                            localViewHolder.epg_book.setVisibility(View.VISIBLE);
                            localViewHolder.epg_book.setBackgroundResource(R.drawable.huikan);
                            localViewHolder.epg_book.setText(R.string.static_string_P);
                        } else if (EPGActivity.this.epgScheduleEvent.book_status == 2) {
                            System.out.println("MW.BOOK_TYPE_RECORD");
                            localViewHolder.epg_book.setVisibility(View.VISIBLE);
                            localViewHolder.epg_book.setText(R.string.static_string_R);
                            localViewHolder.epg_book.setBackgroundResource(R.drawable.huikan);
                        } else {
                            localViewHolder.epg_book.setVisibility(View.INVISIBLE);
                        }
                    } else if (EPGActivity.this.epgScheduleEvent.playing_status == 1) {
                        convertView.setBackgroundResource(R.drawable.listview_item_bg_selector);
                        localViewHolder.epg_book.setVisibility(View.VISIBLE);
                        localViewHolder.epg_book.setText(null);
                        localViewHolder.epg_book.setBackgroundResource(R.drawable.epg_playing);
                    } else {
                        convertView.setBackgroundResource(R.drawable.epginfo_item_bg_expired_selector);
                        localViewHolder.epg_book.setVisibility(View.INVISIBLE);
                    }
                    if (pos == EPGActivity.this.epgInfo_item_pos && EPGActivity.this.channel_list.isFocused()) {
                        convertView.setBackgroundResource(R.drawable.list_446_49_sele_expired);
                    }
                } else {
                    System.out.println("epg info pos = " + pos + "  failed");
                    localViewHolder.epg_time.setText("");
                    localViewHolder.epg_name.setText("");
                    localViewHolder.epg_detail.setVisibility(View.INVISIBLE);
                }
            }
            return convertView;
        }
    }

    class LOCALMessageHandler extends Handler {
        public LOCALMessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    EPGActivity.this.HideProgressBar();
                    if (EPGActivity.this.epgScheduleEventCount == 0) {
                        Log.i("EPGActivity", "$$$$$$$$$$$$$$epgScheduleEventCount == 0");
                        EPGActivity.this.ShowNoEPGInfo();
                    }
                default:
            }
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
                    EPGActivity.this.bNeedShow = true;
                    EPGActivity.this.UpdateScheduleEpgInfo(true);
                case MW.RET_INVALID_SAT /*11*/:
                    int bCurrentServiceChanged = msg.arg1;
                    switch (sub_event_type) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            Log.e("EPGActivity", "service name update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            EPGActivity.this.RefreshShowEpgChannelList();
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            Log.e("EPGActivity", "service params update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            if (bCurrentServiceChanged != 1) {
                                return;
                            }
                            if (SETTINGS.bPass) {
                                MW.ts_player_play_current(true, false);
                                return;
                            }
                            int Ret = MW.ts_player_play_current(false, false);
                            if (Ret == 0) {
                                EPGActivity.this.CheckParentLockAndShowPasswd("5");
                            } else if (Ret == 44) {
                                EPGActivity.this.CheckChannelLockAndShowPasswd("21");
                            }
                        default:
                    }
                default:
            }
        }
    }

    public EPGActivity() {
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.mEpgFirstVisibleItem = 0;
        this.mEpgVisibleItemCount = 8;
        this.epgInfo_item_pos = 0;
        this.text = new TextView[]{this.pos0, this.pos1, this.pos2, this.pos3, this.pos4, this.pos5, this.pos6};
        this.weekStringID = new int[]{R.string.monday, R.string.tuesday, R.string.wednesday, R.string.thursday, R.string.friday, R.string.saturday, R.string.sunday};
        this.serviceInfo = new service();
        this.epgExtendedEvent = new epg_extended_event();
        this.epgScheduleEvent = new epg_schedule_event();
        this.tmpBookEvent = new book_event_data();
        this.epgScheduleDayoffset = 0;
        this.epgScheduleEventCount = 0;
        this.disableUpdate = false;
        this.dateTime = new date_time();
        this.date = new date();
        this.dateTmp = new date();
        this.datePre = new date();
        this.mbLauncherFromDtv = false;
        this.fisttime_enter = true;
        this.bNeedShow = true;
        this.videoView = null;
        this.mSHCallback = new C01518();
        this.pswdDig = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        this.mbLauncherFromDtv = getIntent().getBooleanExtra("LauncherFormDtv", false);
        System.out.println("lanucherformdtv :" + this.mbLauncherFromDtv);
        Intent stopMusicIntent = new Intent();
        stopMusicIntent.setAction("com.android.music.musicservicecommand.pause");
        stopMusicIntent.putExtra("command", "stop");
//        sendBroadcastAsUser(stopMusicIntent, UserHandle.ALL);
        setContentView(R.layout.epg_view);
        initMessagehandle();
        initNoEPGInfo();
        initView();
        initData();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        MW.ts_player_stop_play();
        if (!this.mbLauncherFromDtv) {
            this.localMsgHandler.removeMessages(0);
            this.progress_ll.setVisibility(View.INVISIBLE);
            this.videoView.setVisibility(View.INVISIBLE);
        }
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.bPass = false;
        this.videoView.setVisibility(View.VISIBLE);
        SETTINGS.set_screen_mode();
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
        if (this.dia_currentDialog != null) {
            this.dia_currentDialog.dismiss();
        }
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(4, true);
        MW.register_event_type(6, true);
        MW.epg_set_schedule_event_day_offset(this.epgScheduleDayoffset);
        if (MW.get_date_time(this.dateTime) == 0) {
            this.current_date_time.setText(SETTINGS.getDateTimeAdjustSystem(this.dateTime, true));
        }
        System.out.println("SETTINGS.bPass" + SETTINGS.bPass);
        if (SETTINGS.bPass) {
            MW.ts_player_play_current(true, false);
        } else {
            int Ret = MW.ts_player_play_current(false, false);
            if (Ret == 0) {
                CheckParentLockAndShowPasswd("1");
            } else if (Ret == 44) {
                CheckChannelLockAndShowPasswd("5");
            }
        }
        if (this.epgInfo_Adapter != null) {
            this.epgInfo_Adapter.notifyDataSetChanged();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                if (!this.mbLauncherFromDtv) {
                    SETTINGS.set_green_led(false);
                    SETTINGS.send_led_msg("NENU");
                }
                finishMyself();
                return true;
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                DumpBookListInfo();
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
            case DtvBaseActivity.KEYCODE_EPG /*2013*/:
                if (!this.mbLauncherFromDtv) {
                    SETTINGS.set_green_led(false);
                    SETTINGS.send_led_msg("NENU");
                }
                finishMyself();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void gotoPlayWindow() {
        finishMyself();
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("th.dtv", "th.dtv.DtvMainActivity"));
        intent.addFlags(268435456);
        intent.addFlags(67108864);
        startActivity(intent);
    }

    private void initData() {
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
    }

    private void initEpgInfoProgress() {
    }

    private void UpdateDetailView(int pos) {
        if (this.epgScheduleEventIds == null) {
            this.detail_ll.setVisibility(View.INVISIBLE);
        } else if (MW.epg_get_schedule_event_by_event_id(this.epgScheduleDayoffset, this.epgScheduleEventIds[pos], this.serviceInfo, this.date, this.epgScheduleEvent) == 0) {
            this.epg_event_text.setText(this.epgScheduleEvent.event_text);
            if (this.epgScheduleEvent.has_extended_event_info) {
                this.detail_ll.setVisibility(View.VISIBLE);
            } else {
                this.detail_ll.setVisibility(View.INVISIBLE);
            }
        } else {
            this.epg_event_text.setText("");
            this.detail_ll.setVisibility(View.INVISIBLE);
        }
    }

    private void initProgressBar() {
        this.progress_ll = (LinearLayout) findViewById(R.id.progress_ll);
    }

    private void ShowProgressBar(int delayinS) {
        this.localMsgHandler.removeMessages(0);
        if (delayinS > 0) {
            this.progress_ll.setVisibility(View.VISIBLE);
            this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(0), (long) (delayinS * 1000));
        }
    }

    private void HideProgressBar() {
        this.progress_ll.setVisibility(View.INVISIBLE);
    }

    private void initNoEPGInfo() {
        this.noepg_ll = (LinearLayout) findViewById(R.id.noepg_ll);
    }

    private void ShowNoEPGInfo() {
        Log.i("EPGActivity", "###################ShowNoEPGInfo###############");
        this.noepg_ll.setVisibility(View.VISIBLE);
    }

    private void HideNoEPGInfo() {
        Log.i("EPGActivity", "@@@@@@@@@@@@@@@@@@@@@HideNoEPGInfo###############");
        this.noepg_ll.setVisibility(View.INVISIBLE);
    }

    private void initView() {
        this.detail_ll = (LinearLayout) findViewById(R.id.detail_ll);
        initProgressBar();
        initRadioGroupView();
        initVideoView();
        initCurrentProgramView();
        initEpgInfoList();
        initChannelList();
        initPlayStatusView();
        initEpgInfoProgress();
        UpdateDetailView(this.epgInfo_item_pos);
        setEpgBackgroundAlpha(240);
    }

    private void setEpgBackgroundAlpha(int transparency) {
        this.ll_tvradio_list = (LinearLayout) findViewById(R.id.ll_tvradio_list);
        this.epg_bg = (LinearLayout) findViewById(R.id.epg_bg);
        this.epg_bg.getBackground().setAlpha(transparency);
        this.ll_tvradio_list.getBackground().setAlpha(50);
        this.epgInfo_list.getBackground().setAlpha(50);
    }

    private void initCurrentProgramView() {
        this.current_date_time = (TextView) findViewById(R.id.current_time);
        this.epg_event_text = (TextView) findViewById(R.id.epg_info_content);
    }

    private void showDialog(String detail_txt) {
        new DetailDia(this, R.style.MyDialog, detail_txt).show();
    }

    private void RefreshEpgInfoItemBack(int currentPos) {
    }

    private void ShowConflictEventDialog(Context context, String Title, int bookType) {
        this.dialog = new DeleteDia(context, R.style.MyDialog, Title, bookType);
        this.dialog.show();
    }

    private void Add_booktype_event(int temp_BookType) {
        boolean need_add = true;
        MW.book_lock_event_check();
        while (need_add) {
            Log.e("BOOK", "temp_BookType : " + temp_BookType);
            int ret = MW.book_epg_add_event(this.epgScheduleDayoffset, this.epgScheduleEventIds[this.epgInfo_item_pos], temp_BookType, this.serviceInfo, this.date, this.tmpBookEvent);
            Log.e("BOOK", "ret : " + ret);
            switch (ret) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    System.out.println("book add : ok");
                    this.epgInfo_Adapter.notifyDataSetChanged();
                    need_add = false;
                    break;
                case MW.RET_BOOK_EVENT_INVALID /*14*/:
                    System.out.println("book add : event invalid");
                    need_add = false;
                    SETTINGS.makeText(this, R.string.event_invalid, 0);
                    break;
                case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                    System.out.println("book add : conflict event");
                    need_add = false;
                    ShowConflictEventDialog(this, getResources().getString(R.string.conflictevent), temp_BookType);
                    break;
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                    System.out.println("book add : event not found");
                    need_add = false;
                    SETTINGS.makeText(this, R.string.event_not_found, 0);
                    break;
                case MW.RET_INVALID_OBJECT /*30*/:
                    System.out.println("book add : invalid object");
                    need_add = false;
                    SETTINGS.makeText(this, R.string.invalid_object, 0);
                    break;
                case MW.RET_INVALID_SERVICE /*31*/:
                    System.out.println("book add : invalid service");
                    need_add = false;
                    break;
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_HARD_HEARING_NO_RATIO /*32*/:
                    System.out.println("book add : invalid boot type");
                    need_add = false;
                    break;
                default:
                    System.out.println("book add : unknown error : " + ret);
                    need_add = false;
                    SETTINGS.makeText(this, R.string.event_unknown_error, 0);
                    break;
            }
        }
        MW.book_unlock_event_check();
    }

    private void Change_booktype_event(int temp_BookType) {
        switch (MW.book_epg_delete_event(this.epgScheduleDayoffset, this.epgScheduleEventIds[this.epgInfo_item_pos], this.serviceInfo, this.date)) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                System.out.println("book delete : ok");
                this.epgInfo_Adapter.notifyDataSetChanged();
                break;
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                System.out.println("book delete : event not found");
                SETTINGS.makeText(this, R.string.event_not_found, 0);
                break;
            case MW.RET_INVALID_OBJECT /*30*/:
                System.out.println("book delete : invalid object");
                SETTINGS.makeText(this, R.string.invalid_object, 0);
                break;
        }
        if (temp_BookType != 0) {
            Add_booktype_event(temp_BookType);
        }
    }

    private void BooktypeSelectPopupDialog(int book_type) {
        BookTypeSelectDialog bookTypeSelectDialog = new BookTypeSelectDialog(this, R.style.MyDialog, book_type);
        bookTypeSelectDialog.show();
        LayoutParams lp = bookTypeSelectDialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        bookTypeSelectDialog.getWindow().setAttributes(lp);
        bookTypeSelectDialog.getWindow().addFlags(2);
    }

    private void bookEvent() {
        if (this.epgInfo_item_pos >= 0 && this.epgScheduleEventIds != null && MW.epg_get_schedule_event_by_event_id(this.epgScheduleDayoffset, this.epgScheduleEventIds[this.epgInfo_item_pos], this.serviceInfo, this.date, this.epgScheduleEvent) == 0) {
            if (this.epgScheduleEvent.playing_status == 1) {
                gotoPlayWindow();
            } else if (SETTINGS.bHongKongDTMB) {
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.setClass(this, TimerListActivity.class);
                intent.putExtra("bStartSchedule", true);
                intent.putExtra("startBookYear", this.epgScheduleEvent.start_year);
                intent.putExtra("startBookMonth", this.epgScheduleEvent.start_month);
                intent.putExtra("startBookDay", this.epgScheduleEvent.start_day);
                intent.putExtra("startBookHour", this.epgScheduleEvent.start_hour);
                intent.putExtra("startBookMinute", this.epgScheduleEvent.start_minute);
                intent.putExtra("endBookHour", this.epgScheduleEvent.end_hour);
                intent.putExtra("endBookMinute", this.epgScheduleEvent.end_minute);
                startActivity(intent);
            } else {
                BooktypeSelectPopupDialog(this.epgScheduleEvent.book_status);
            }
        }
    }

    private void initEpgInfoList() {
        this.epgInfo_list = (CustomListView) findViewById(R.id.tv_back_videos);
        this.epgInfo_Adapter = new EpgInfoAdapter(this);
        this.epgInfo_list.setAdapter(this.epgInfo_Adapter);
        this.epgInfo_list.setVisibleItemCount(9);
        this.epgInfo_list.setCustomScrollListener(new C01441());
        this.epgInfo_list.setOnItemSelectedListener(new C01452());
        this.epgInfo_list.setCustomKeyListener(new C01463());
    }

    private void RefreshRadioGroupView() {
        int i;
        if (MW.get_date(0, null, this.date) == 0) {
            i = this.date.weekNo;
        } else {
            i = 0;
        }
        int i2 = i;
        for (int i3 = 0; i3 < 7; i3++) {
            ((RadioButton) this.weekdays.getChildAt(i3)).setBackgroundResource(R.drawable.tv_back_friday_selector);
            if (i2 >= 7) {
                i2 = 0;
            }
            if (MW.get_date(i3, this.date, this.dateTmp) == 0) {
                ((RadioButton) this.weekdays.getChildAt(i3)).setText(SETTINGS.getDateAdjustSystem(this.dateTmp));
                this.text[i3].setText(getResources().getString(this.weekStringID[this.dateTmp.weekNo]));
            }
        }
        ((RadioButton) this.weekdays.getChildAt(this.epgScheduleDayoffset)).setChecked(true);
    }

    private void initRadioGroupView() {
        int i;
        this.weekdays = (RadioGroup) findViewById(R.id.tv_back_weekdays);
        this.pos0 = (TextView) findViewById(R.id.pos0);
        this.pos1 = (TextView) findViewById(R.id.pos1);
        this.pos2 = (TextView) findViewById(R.id.pos2);
        this.pos3 = (TextView) findViewById(R.id.pos3);
        this.pos4 = (TextView) findViewById(R.id.pos4);
        this.pos5 = (TextView) findViewById(R.id.pos5);
        this.pos6 = (TextView) findViewById(R.id.pos6);
        if (MW.get_date(0, null, this.date) == 0) {
            i = this.date.weekNo;
        } else {
            i = 0;
        }
        this.text[0] = this.pos0;
        this.text[1] = this.pos1;
        this.text[2] = this.pos2;
        this.text[3] = this.pos3;
        this.text[4] = this.pos4;
        this.text[5] = this.pos5;
        this.text[6] = this.pos6;
        int i2 = i;
        for (int i3 = 0; i3 < 7; i3++) {
            ((RadioButton) this.weekdays.getChildAt(i3)).setBackgroundResource(R.drawable.tv_back_friday_selector);
            if (i2 >= 7) {
                i2 = 0;
            }
            if (MW.get_date(i3, this.date, this.dateTmp) == 0) {
                ((RadioButton) this.weekdays.getChildAt(i3)).setText(SETTINGS.getDateAdjustSystem(this.dateTmp));
                this.text[i3].setText(getResources().getString(this.weekStringID[this.dateTmp.weekNo]));
            }
        }
        ((RadioButton) this.weekdays.getChildAt(this.epgScheduleDayoffset)).setChecked(true);
        this.weekdays.setOnCheckedChangeListener(new C01474());
    }

    private void initChannelList() {
        this.channel_list = (CustomListView) findViewById(R.id.tv_back_channles);
        this.channel_list.requestFocus();
        this.channel_list.setCustomScrollListener(new C01485());
        this.channel_list.setOnItemSelectedListener(new C01496());
        this.channel_list.setCustomKeyListener(new C01507());
        this.epgChannelListAdapter = new EPGChannelListAdapter(this);
        this.channel_list.setAdapter(this.epgChannelListAdapter);
        this.channel_list.setVisibleItemCount(11);
        this.channel_list.setChoiceMode(1);
        this.epgchannel_type = (TextView) findViewById(R.id.channeltype_txt);
        RefreshShowEpgChannelList();
    }

    private void RefreshShowEpgChannelList() {
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            if (this.current_service_type == 0) {
                this.epgchannel_type.setText(R.string.TvChannelList);
            } else {
                this.epgchannel_type.setText(R.string.RadioChannelList);
            }
            this.mFirstVisibleItem = this.current_service_index;
            this.channel_list.setFocusableInTouchMode(true);
            this.channel_list.requestFocus();
            this.channel_list.requestFocusFromTouch();
            this.channel_list.setSelection(this.mFirstVisibleItem);
            this.epgChannelListAdapter.notifyDataSetChanged();
            this.bNeedShow = true;
            UpdateScheduleEpgInfo(true);
        }
    }

    private void DumpBookListInfo() {
        Log.e("Book", "DumpBookListInfo");
        int book_get_event_count = MW.book_get_event_count();
        Log.e("Book", "book event count = " + book_get_event_count);
        for (int i = 0; i < book_get_event_count; i++) {
            if (MW.book_get_event_by_index(i, this.tmpBookEvent) == 0) {
                Log.e("Book", "  " + (i + 1) + ".  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(this.tmpBookEvent.start_year), Integer.valueOf(this.tmpBookEvent.start_month), Integer.valueOf(this.tmpBookEvent.start_day), Integer.valueOf(this.tmpBookEvent.start_hour), Integer.valueOf(this.tmpBookEvent.start_minute), Integer.valueOf(this.tmpBookEvent.end_year), Integer.valueOf(this.tmpBookEvent.end_month), Integer.valueOf(this.tmpBookEvent.end_day), Integer.valueOf(this.tmpBookEvent.end_hour), Integer.valueOf(this.tmpBookEvent.end_minute)}) + " ## " + this.tmpBookEvent.service_name + " ## " + this.tmpBookEvent.event_name + " ## " + (this.tmpBookEvent.book_type == 1 ? "Play" : "Record"));
            }
        }
        Intent intent = new Intent();
        intent.setFlags(67108864);
        intent.setClass(this, TimerListActivity.class);
        startActivity(intent);
    }

    private void UpdateScheduleExtendedEpgInfo(int event_id) {
    }

    private void UpdateScheduleEpgInfo(boolean z) {
        this.epg_event_text.setText("");
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            int i;
            if (z) {
                SETTINGS.set_led_channel(this.serviceInfo.channel_number_display);
            }
            this.epgScheduleEventCount = MW.epg_lock_get_schedule_event_count(this.epgScheduleDayoffset, this.serviceInfo, this.date);
            if (this.epgScheduleEventCount > 0) {
                this.epgScheduleEventIds = new int[this.epgScheduleEventCount];
                for (i = 0; i < this.epgScheduleEventCount; i++) {
                    this.epgScheduleEventIds[i] = MW.epg_lock_get_schedule_event_id_by_index(this.epgScheduleDayoffset, i, this.serviceInfo, this.date);
                }
                if (z || this.epgInfo_item_pos >= this.epgScheduleEventCount) {
                    this.epgInfo_item_pos = 0;
                    this.epgInfo_list.setCustomSelection(this.epgInfo_item_pos);
                }
                UpdateScheduleExtendedEpgInfo(this.epgScheduleEventIds[this.epgInfo_item_pos]);
                UpdateDetailView(this.epgInfo_item_pos);
                HideProgressBar();
                HideNoEPGInfo();
                Log.i("EPGActivity", "********************HideNoEPGInfo^^^^^^^^^^^");
            } else {
                this.epgScheduleEventIds = null;
                this.detail_ll.setVisibility(View.INVISIBLE);
                if (this.bNeedShow) {
                    this.bNeedShow = false;
                    HideNoEPGInfo();
                    ShowProgressBar(30);
                }
            }
            MW.epg_unlock_schedule_event();
            for (i = 0; i < this.epgScheduleEventCount; i++) {
                if (MW.epg_get_schedule_event_by_event_id(this.epgScheduleDayoffset, this.epgScheduleEventIds[i], this.serviceInfo, this.date, this.epgScheduleEvent) == 0) {
                    Log.e("EPG", "  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(this.epgScheduleEvent.start_year), Integer.valueOf(this.epgScheduleEvent.start_month), Integer.valueOf(this.epgScheduleEvent.start_day), Integer.valueOf(this.epgScheduleEvent.start_hour), Integer.valueOf(this.epgScheduleEvent.start_minute), Integer.valueOf(this.epgScheduleEvent.end_year), Integer.valueOf(this.epgScheduleEvent.end_month), Integer.valueOf(this.epgScheduleEvent.end_day), Integer.valueOf(this.epgScheduleEvent.end_hour), Integer.valueOf(this.epgScheduleEvent.end_minute)}) + this.epgScheduleEvent.event_name + " ## " + (this.epgScheduleEvent.event_text.length() > 0 ? "text" : "") + " ## " + (this.epgScheduleEvent.has_extended_event_info ? "detail" : ""));
                    if (this.epgScheduleEvent.playing_status == 2) {
                        Log.e("EPG", "  " + String.format("playing status : new, bookable", new Object[0]));
                    } else if (this.epgScheduleEvent.playing_status == 1) {
                        Log.e("EPG", "  " + String.format("playing status : playing", new Object[0]));
                    } else {
                        Log.e("EPG", "  " + String.format("playing status : expired", new Object[0]));
                    }
                    Log.e("EPG", "  " + String.format("event text : ", new Object[0]) + this.epgScheduleEvent.event_text);
                }
            }
            this.epgInfo_Adapter.notifyDataSetChanged();
        }
    }

    private void UpdateScheduleEpgInfo_NextDay() {
//        this.epgInfo_list.setSelectionFromTop(this.epgInfo_item_pos, this.mEpgFirstVisibleItem);
//        this.epgScheduleDayoffset++;
//        if (this.epgScheduleDayoffset >= 7) {
//            this.epgScheduleDayoffset = 0;
//        }
//        ((RadioButton) this.weekdays.getChildAt(this.epgScheduleDayoffset)).setChecked(true);
//        MW.epg_set_schedule_event_day_offset(this.epgScheduleDayoffset);
//        this.bNeedShow = true;
//        UpdateScheduleEpgInfo(true);
    }

    private void UpdateScheduleEpgInfo_PrevDay() {
        this.epgInfo_list.setCustomSelection(this.epgInfo_item_pos);
        this.epgScheduleDayoffset--;
        if (this.epgScheduleDayoffset < 0) {
            this.epgScheduleDayoffset = 6;
        }
        ((RadioButton) this.weekdays.getChildAt(this.epgScheduleDayoffset)).setChecked(true);
        MW.epg_set_schedule_event_day_offset(this.epgScheduleDayoffset);
        this.bNeedShow = true;
        UpdateScheduleEpgInfo(true);
    }

    private void initVideoView() {
        this.videoView = (VideoView) findViewById(R.id.tv_back_video);
        this.videoView.setFocusable(false);
        this.videoView.setClickable(false);
        this.videoView.getHolder().addCallback(this.mSHCallback);
        SETTINGS.videoViewSetFormat(this, this.videoView);
    }

    private void initPlayStatusView() {
        this.play_status = (TextView) findViewById(R.id.tv_status);
        this.play_status.setVisibility(View.INVISIBLE);
    }

    private void initMessagehandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        this.localMsgHandler = new LOCALMessageHandler(this.looper);
    }

    private void CheckParentLockAndShowPasswd(String str) {
        Log.e("LEE", "ParentLock called: " + str);
        MW.get_date(0, null, this.date);
        if (MW.epg_lock_get_schedule_event_count(0, this.serviceInfo, this.date) > 0) {
            int epg_lock_get_schedule_event_id_by_index = MW.epg_lock_get_schedule_event_id_by_index(0, 0, this.serviceInfo, this.date);
            MW.epg_unlock_schedule_event();
            if (MW.epg_get_schedule_event_by_event_id(0, epg_lock_get_schedule_event_id_by_index, this.serviceInfo, this.date, this.epgScheduleEvent) == 0) {
                System.out.println("Parent rate :" + this.epgScheduleEvent.parental_rating);
                System.out.println("Setting rate :" + SETTINGS.get_parental_rating_value());
                if (this.epgScheduleEvent.parental_rating + 3 > SETTINGS.get_parental_rating_value() && SETTINGS.get_parental_rating_value() != 19) {
                    MW.ts_player_stop_play();
                    CheckChannelLockAndShowPasswd("1");
                    return;
                }
                return;
            }
            return;
        }
        MW.epg_unlock_schedule_event();
    }

    private void CheckChannelLockAndShowPasswd(String str) {
//        System.out.println("EPG called pos :" + str);
//        if (SETTINGS.get_channel_lock_onoff()) {
//            if (this.pswdDig != null) {
//                this.pswdDig.dismiss();
//                this.pswdDig = null;
//            }
//            this.pswdDig = new PasswordDialog(this, R.style.MyDialog, new C01529(), new PswdCB() {
//                public void DoCallBackEvent() {
//                    MW.ts_player_play_current(true, false);
//                    SETTINGS.bPass = true;
//                }
//            });
//            this.pswdDig.show();
//            ((PasswordDialog) this.pswdDig).setHandleUpDown(true);
//            SETTINGS.bPass = false;
//            return;
//        }
//        MW.ts_player_play_current(true, false);
    }
}
