package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import th.dtv.ComboLayout;
import th.dtv.CustomListView;
import th.dtv.DigitsEditText;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.dvb_s_sat;

public class SatelliteListActivity extends DtvBaseActivity {
    ArrayList<Boolean> SatSelStatus;
    ArrayList<Integer> SelSatList;
    ArrayList<Integer> SelTpList;
    boolean fSaveSatTpFlag;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    dvb_s_sat satInfo;
    private int satInfo_item_pos;
    private SateInfoAdapter sateInfoAdapter;
    private CustomListView sate_listView;
    private TextView select_txt;

    /* renamed from: th.dtv.activity.SatelliteListActivity.1 */
    class C02271 implements OnScrollListener {
        C02271() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            SatelliteListActivity.this.mFirstVisibleItem = firstVisibleItem;
            SatelliteListActivity.this.mVisibleItemCount = visibleItemCount;
            System.out.println("mVisibleItemCount" + SatelliteListActivity.this.mVisibleItemCount);
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.SatelliteListActivity.2 */
    class C02282 implements OnItemSelectedListener {
        C02282() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            SatelliteListActivity.this.satInfo_item_pos = position;
            if (MW.db_dvb_s_get_sat_info(0, position, SatelliteListActivity.this.satInfo) != 0) {
                return;
            }
            if (SatelliteListActivity.this.satInfo.selected) {
                SatelliteListActivity.this.ShowUnSelectTipsView();
            } else {
                SatelliteListActivity.this.ShowSelectTipsView();
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.SatelliteListActivity.3 */
    class C02293 implements OnKeyListener {
        C02293() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    SatelliteListActivity.this.DoUnselectedSatellite(SatelliteListActivity.this.satInfo_item_pos);
                    return true;
                case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                    SatelliteListActivity.this.EditSatellite();
                    return true;
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    return true;
                case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                    SatelliteListActivity.this.DeleteSatellite();
                    return true;
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    SatelliteListActivity.this.AddSatellite();
                    return true;
            }
            return false;
        }
    }

    public class AddDialog extends Dialog {
        ComboLayout combo_direction;
        DigitsEditText edittext_angle0;
        DigitsEditText edittext_angle1;
        EditText edittext_satname;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        TextView mFrequency;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mPolarity;
        TextView mSymbol;
        TextView mTitle;
        String mode_str;
        dvb_s_sat satInfo_temp;
        int temp_sat_position;

        /* renamed from: th.dtv.activity.SatelliteListActivity.AddDialog.1 */
        class C02301 implements OnItemClickListener {
            C02301() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                AddDialog.this.temp_sat_position = position;
            }
        }
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.dvbs_sat_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.edittext_satname = (EditText) findViewById(R.id.edittext_sat_name);
            this.combo_direction = (ComboLayout) findViewById(R.id.combo_direction);
            this.edittext_angle0 = (DigitsEditText) findViewById(R.id.angle0);
            this.edittext_angle0.setBackgroundResource(17301529);
            this.edittext_angle0.setFilters(new InputFilter[]{new LengthFilter(3)});
            this.edittext_angle1 = (DigitsEditText) findViewById(R.id.angle1);
            this.edittext_angle1.setFilters(new InputFilter[]{new LengthFilter(1)});
            this.edittext_angle1.setBackgroundResource(17301529);
            this.mTitle.setText(R.string.add);
            ((TextView) findViewById(R.id.item_no)).setText(String.valueOf(MW.db_dvb_s_get_sat_count(0) + 1));
            this.edittext_satname.setText(SatelliteListActivity.this.getResources().getString(R.string.new_sat) + String.valueOf(MW.db_dvb_s_get_sat_count(0) + 1));
            this.edittext_satname.setSelection(0, this.edittext_satname.length());
            this.edittext_angle0.setDigitsText("");
            this.edittext_angle1.setDigitsText("");
            ArrayList items = new ArrayList();
            items.clear();
            items.add(SatelliteListActivity.this.getResources().getString(R.string.East));
            items.add(SatelliteListActivity.this.getResources().getString(R.string.West));
            this.combo_direction.initView((int) R.string.Position, items, 0, new C02301());
            this.combo_direction.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.combo_direction.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 120) / 1280, -2));
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (!this.edittext_angle0.isFocused() && !this.edittext_angle1.isFocused()) {
                        this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (!this.edittext_satname.isFocused()) {
                        this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (!(this.mBtnStart.isFocused() || this.mBtnCancel.isFocused() || ((this.edittext_angle1.isFocused() && keyCode == 21) || (this.edittext_angle0.isFocused() && keyCode == 22)))) {
                        return true;
                    }
            }
            return super.onKeyDown(keyCode, event);
        }

        public AddDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mFrequency = null;
            this.mSymbol = null;
            this.mTitle = null;
            this.mPolarity = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mode_str = "default";
            this.satInfo_temp = new dvb_s_sat();
            this.temp_sat_position = 0;
            this.combo_direction = null;
            this.edittext_satname = null;
            this.edittext_angle0 = null;
            this.edittext_angle1 = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class DYWTUNSELECT extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        TextView mContent;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.deldia_title);
            this.mTitle.setText("");
            this.mContent = (TextView) findViewById(R.id.delete_content);
            this.mContent.setText(SatelliteListActivity.this.getResources().getString(R.string.dywtunselect));
            this.mBtnCancel.requestFocus();
        }

        public DYWTUNSELECT(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.mContent = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class EditDialog extends Dialog {
        ComboLayout combo_direction;
        DigitsEditText edittext_angle0;
        DigitsEditText edittext_angle1;
        EditText edittext_satname;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        TextView mFrequency;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mPolarity;
        TextView mSymbol;
        String mode_str;
        int temp_sat_position;

        /* renamed from: th.dtv.activity.SatelliteListActivity.EditDialog.1 */
        class C02371 implements OnItemClickListener {
            C02371() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                EditDialog.this.temp_sat_position = position;
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.dvbs_sat_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.edittext_satname = (EditText) findViewById(R.id.edittext_sat_name);
            this.combo_direction = (ComboLayout) findViewById(R.id.combo_direction);
            this.edittext_angle0 = (DigitsEditText) findViewById(R.id.angle0);
            this.edittext_angle0.setBackgroundResource(17301529);
            this.edittext_angle0.setFilters(new InputFilter[]{new LengthFilter(3)});
            this.edittext_angle1 = (DigitsEditText) findViewById(R.id.angle1);
            this.edittext_angle1.setFilters(new InputFilter[]{new LengthFilter(1)});
            this.edittext_angle1.setBackgroundResource(17301529);
            ((TextView) findViewById(R.id.item_no)).setText(String.valueOf(SatelliteListActivity.this.satInfo_item_pos + 1));
            this.edittext_satname.setText(SatelliteListActivity.this.satInfo.sat_name);
            this.edittext_satname.setSelection(0, SatelliteListActivity.this.satInfo.sat_name.length());
            this.edittext_angle0.setDigitsText(String.valueOf(SatelliteListActivity.this.satInfo.sat_degree_dec));
            this.edittext_angle1.setDigitsText(String.valueOf(SatelliteListActivity.this.satInfo.sat_degree_point));
            ArrayList items = new ArrayList();
            items.clear();
            items.add(SatelliteListActivity.this.getResources().getString(R.string.East));
            items.add(SatelliteListActivity.this.getResources().getString(R.string.West));
            this.combo_direction.initView((int) R.string.Position, items, SatelliteListActivity.this.satInfo.sat_position, new C02371());
            this.combo_direction.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.combo_direction.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 120) / 1280, -2));
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (!this.edittext_angle0.isFocused() && !this.edittext_angle1.isFocused()) {
                        this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (!this.edittext_satname.isFocused()) {
                        this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.white));
                        break;
                    }
                    this.combo_direction.getInfoTextView().setTextColor(SatelliteListActivity.this.getResources().getColor(R.color.yellow));
                    break;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case MW.RET_MEMORY_ERROR /*22*/:
                    if (!(this.mBtnStart.isFocused() || this.mBtnCancel.isFocused() || ((this.edittext_angle1.isFocused() && keyCode == 21) || (this.edittext_angle0.isFocused() && keyCode == 22)))) {
                        return true;
                    }
            }
            return super.onKeyDown(keyCode, event);
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mFrequency = null;
            this.mSymbol = null;
            this.mPolarity = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mode_str = "default";
            this.edittext_satname = null;
            this.temp_sat_position = 0;
            this.combo_direction = null;
            this.edittext_angle0 = null;
            this.edittext_angle1 = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    private class SateInfoAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mcontext;

        class ViewHolder {
            TextView sate_longitude;
            TextView sate_name;
            TextView sate_no;
            ImageView sate_select_img;

            ViewHolder() {
            }
        }

        public SateInfoAdapter(Context context) {
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_dvb_s_get_sat_count(0);
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            System.out.println("SateInfoAdapter pos = " + pos);
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.satellite_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.sate_no = (TextView) convertView.findViewById(R.id.sate_no);
                localViewHolder.sate_select_img = (ImageView) convertView.findViewById(R.id.sate_select_img);
                localViewHolder.sate_name = (TextView) convertView.findViewById(R.id.sate_name);
                localViewHolder.sate_longitude = (TextView) convertView.findViewById(R.id.sate_longitude);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            localViewHolder.sate_no.setText(String.format("%03d", new Object[]{Integer.valueOf(pos + 1)}));
            if (MW.db_dvb_s_get_sat_info(0, pos, SatelliteListActivity.this.satInfo) == 0) {
                String str;
                if (SatelliteListActivity.this.satInfo.selected) {
                    localViewHolder.sate_select_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.sate_select_img.setVisibility(View.INVISIBLE);
                }
                localViewHolder.sate_name.setText(SatelliteListActivity.this.satInfo.sat_name);
                TextView textView = localViewHolder.sate_longitude;
                String str2 = "%d.%d %s";
                Object[] objArr = new Object[3];
                objArr[0] = Integer.valueOf(SatelliteListActivity.this.satInfo.sat_degree_dec);
                objArr[1] = Integer.valueOf(SatelliteListActivity.this.satInfo.sat_degree_point);
                if (SatelliteListActivity.this.satInfo.sat_position == 0) {
                    str = "E";
                } else {
                    str = "W";
                }
                objArr[2] = str;
                textView.setText(String.format(str2, objArr));
            } else {
                System.out.println("db_dvb_s_get_sat_info  failed");
                localViewHolder.sate_name.setText("");
                localViewHolder.sate_longitude.setText("");
                localViewHolder.sate_select_img.setVisibility(View.INVISIBLE);
            }
            return convertView;
        }
    }

    public SatelliteListActivity() {
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.satInfo = new dvb_s_sat();
        this.satInfo_item_pos = 0;
        this.SatSelStatus = null;
        this.fSaveSatTpFlag = false;
        this.SelSatList = new ArrayList();
        this.SelTpList = new ArrayList();
    }

    private void initSateListView() {
        this.sate_listView = (CustomListView) findViewById(R.id.satellite_listview);
        this.sateInfoAdapter = new SateInfoAdapter(this);
        this.sate_listView.setAdapter(this.sateInfoAdapter);
        this.sate_listView.setVisibleItemCount(11);
        this.sate_listView.setCustomScrollListener(new C02271());
        this.sate_listView.setOnItemSelectedListener(new C02282());
        this.sate_listView.setCustomKeyListener(new C02293());
        if (MW.db_dvb_s_get_sat_info(0, getIntent().getIntExtra("selected_sat_index", 0), this.satInfo) == 0) {
            this.sate_listView.setCustomSelection(this.satInfo.sat_index);
        }
    }

    private void RefreshSatSelectImage() {
        if (MW.db_dvb_s_get_sat_info(0, this.satInfo_item_pos, this.satInfo) == 0) {
            this.satInfo.selected = !this.satInfo.selected;
        }
        if (MW.db_dvb_s_set_sat_info(0, this.satInfo) == 0) {
            this.fSaveSatTpFlag = true;
        }
        if (this.SatSelStatus != null) {
            if (((Boolean) this.SatSelStatus.get(this.satInfo_item_pos)).booleanValue()) {
                this.SatSelStatus.set(this.satInfo_item_pos, Boolean.valueOf(false));
                ShowSelectTipsView();
            } else {
                this.SatSelStatus.set(this.satInfo_item_pos, Boolean.valueOf(true));
                ShowUnSelectTipsView();
            }
        }
        this.sateInfoAdapter.notifyDataSetChanged();
    }

    void ShowToastInformation(String text, int duration_mode) {
        LinearLayout toast_view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
        ((TextView) toast_view.findViewById(R.id.toast_text)).setText(text);
        Toast toast = new Toast(this);
        if (duration_mode == 0) {
            toast.setDuration(0);
        } else if (duration_mode == 1) {
            toast.setDuration(1);
        }
        toast.setView(toast_view);
        toast.setGravity(17, 0, -70);
        toast.show();
    }

    private void AddSatellite() {
        Dialog addDialog = new AddDialog(this, R.style.MyDialog);
        addDialog.setContentView(R.layout.dvbs_sat_edit_custom_dia);
        addDialog.show();
        addDialog.getWindow().addFlags(2);
    }

    private void EditSatellite() {
        if (MW.db_dvb_s_get_sat_count(0) == 0 || MW.db_dvb_s_get_sat_info(0, this.satInfo_item_pos, this.satInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_sat), 0);
            return;
        }
        Dialog editDialog = new EditDialog(this, R.style.MyDialog);
        editDialog.setContentView(R.layout.dvbs_sat_edit_custom_dia);
        editDialog.show();
        editDialog.getWindow().addFlags(2);
    }

    private void DeleteSatellite() {
        if (MW.db_dvb_s_get_sat_count(0) == 0 || MW.db_dvb_s_get_sat_info(0, this.satInfo_item_pos, this.satInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_sat), 0);
        } else {
            new DeleteDia(this, R.style.MyDialog).show();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.satellite_list_activity);
        initData();
        initSateListView();
        initTipView();
    }

    private void initTipView() {
        this.select_txt = (TextView) findViewById(R.id.select_txt);
    }

    private void ShowSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.SelectSat));
    }

    private void ShowUnSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.UnSelectSat));
    }

    private void initData() {
        int count = MW.db_dvb_s_get_sat_count(0);
        if (count > 0) {
            this.SatSelStatus = new ArrayList();
            for (int i = 0; i < count; i++) {
                this.SatSelStatus.add(i, Boolean.valueOf(false));
            }
            return;
        }
        this.SatSelStatus = null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != 0) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    return true;
                default:
                    break;
            }
        }
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                AddSatellite();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.send_led_msg("NENU");
        if (MW.db_dvb_s_get_selected_sat_count(0) <= 0) {
            SETTINGS.makeText(this, R.string.no_selected_satellite, 0);
        }
    }

    protected void onPause() {
        if (MW.db_dvb_s_get_current_sat_info(0, this.satInfo) == 0) {
            System.out.println("Current satellite info :" + this.satInfo.sat_index);
            if (!this.satInfo.selected) {
                if (MW.db_dvb_s_get_selected_sat_count(0) <= 0) {
                    MW.db_dvb_s_set_current_sat_tp(0, 0, 0);
                } else if (MW.db_dvb_s_get_selected_sat_info(0, 0, this.satInfo) == 0) {
                    MW.db_dvb_s_set_current_sat_tp(0, this.satInfo.sat_index, 0);
                }
            }
        }
        if (this.fSaveSatTpFlag) {
            this.fSaveSatTpFlag = false;
            MW.db_dvb_s_save_sat_tp(0);
        }
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void DoUnselectedSatellite(int pos) {
        if (MW.db_dvb_s_get_sat_count(0) == 0 || MW.db_dvb_s_get_sat_info(0, this.satInfo_item_pos, this.satInfo) != 0) {
            ShowToastInformation(getResources().getString(R.string.tip_select_sat), 0);
        } else if (MW.db_dvb_s_check_sat_has_service(pos)) {
            new DYWTUNSELECT(this, R.style.MyDialog).show();
        } else {
            RefreshSatSelectImage();
        }
    }
}
