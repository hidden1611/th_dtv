package th.dtv.activity;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import th.dtv.ComboLayout;
import th.dtv.CustomListView;
import th.dtv.DigitsEditText;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.book_event_data;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;
import th.dtv.mw_data.service;
import th.dtv.util.CheckTimeValidate;

public class TimerListActivity extends DtvBaseActivity {
    private TextView current_date_time;
    Dialog current_dig;
    private int current_service_index;
    private int current_service_type;
    private date_time dateTime;
    private int mBookEndHour;
    private int mBookEndMinute;
    private int mBookStDay;
    private int mBookStHour;
    private int mBookStMinute;
    private int mBookStMonth;
    private int mBookStYear;
    private int mFirstVisibleItem;
    private book_event_data mTmpBookEvent;
    private int mVisibleItemCount;
    private boolean mbStartSchedule;
    private Handler mwMsgHandler;
    private service serviceInfo;
    private TimerListAdapter timerListAdapter;
    private CustomListView timer_listview;
    private int timerlist_item_pos;

    /* renamed from: th.dtv.activity.TimerListActivity.1 */
    class C02661 implements OnScrollListener {
        C02661() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TimerListActivity.this.mFirstVisibleItem = firstVisibleItem;
            TimerListActivity.this.mVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.TimerListActivity.2 */
    class C02672 implements OnItemSelectedListener {
        C02672() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            TimerListActivity.this.timerlist_item_pos = pos;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public class AddDialog extends Dialog {
        private ComboLayout Combo_chname;
        private ComboLayout Combo_chntype;
        private ComboLayout Combo_date;
        private ComboLayout Combo_duration;
        private ComboLayout Combo_endtime;
        private ComboLayout Combo_mode;
        private ComboLayout Combo_startTime;
        private ComboLayout Combo_type;
        private int Day;
        private FrameLayout Frame_duration;
        private LinearLayout Linear_Vdate;
        private LinearLayout Linear_endtime;
        private LinearLayout Linear_startTime;
        private int Month;
        private int Year;
        private boolean bEdit;
        private int channel_index;
        private int channel_type;
        private int duration_hour;
        private int duration_min;
        private EditText ed_hour;
        private EditText ed_minute;
        private int edday;
        private int edhour;
        private int edminute;
        private int edmonth;
        private int edyear;
        private DigitsEditText et_hour;
        private DigitsEditText et_min;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private date mDate;
        private date_time mDateTime;
        private OnDateSetListener mOnDateSetListener;
        private OnTimeSetListener mOnTimeSetListener;
        private OnTimeSetListener mOnTimeSetListener1;
        private TextView mTitletext;
        private int mday;
        private int mhour;
        private int mminute;
        private int mmonth;
        private int myear;
        private service serviceInfo;
        private EditText st_day;
        private EditText st_hour;
        private EditText st_minute;
        private EditText st_month;
        private EditText st_year;
        private int start_hour;
        private int start_min;
        private int timer_mode;
        private int timer_type;
        private book_event_data tmpBookEvent;
        private String to_edit_time;

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.3 */
        class C02703 implements OnItemClickListener {
            C02703() {
            }

            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                AddDialog.this.timer_type = position + 1;
                AddDialog.this.RefreshItems(AddDialog.this.timer_type);
            }
        }

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.4 */
        class C02714 implements OnItemClickListener {
            C02714() {
            }

            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                AddDialog.this.timer_mode = position;
            }
        }

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.5 */
        class C02725 implements OnItemClickListener {
            C02725() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
            }
        }

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.6 */
        class C02736 implements OnItemClickListener {
            C02736() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                AddDialog.this.et_hour.requestFocus();
                AddDialog.this.et_hour.setSelection(AddDialog.this.et_hour.getText().length());
                AddDialog.this.Combo_duration.setBackgroundResource(R.drawable.live_channel_list_item_bg_sel);
            }
        }

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.7 */
        class C02747 implements OnItemClickListener {
            C02747() {
            }

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                new TimePickerDialog(AddDialog.this.mContext, 3, AddDialog.this.mOnTimeSetListener, AddDialog.this.start_hour, AddDialog.this.start_min, DateFormat.is24HourFormat(AddDialog.this.mContext)).show();
            }
        }

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.8 */
        class C02758 implements OnItemClickListener {
            C02758() {
            }

            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                if (AddDialog.this.channel_type != position) {
                    AddDialog.this.channel_type = position;
                    AddDialog.this.channel_index = 0;
                    AddDialog.this.initChannelItem(AddDialog.this.channel_type, AddDialog.this.channel_index);
                }
            }
        }

        /* renamed from: th.dtv.activity.TimerListActivity.AddDialog.9 */
        class C02769 implements OnItemClickListener {
            C02769() {
            }

            public void onItemClick(AdapterView<?> adapterView, View v, int positon, long id) {
                AddDialog.this.channel_index = positon;
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.timer_add_dialog);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitletext = (TextView) findViewById(R.id.title);
            this.Combo_date = (ComboLayout) findViewById(R.id.Combo_date);
            this.Combo_chname = (ComboLayout) findViewById(R.id.Combo_chname);
            this.Combo_chntype = (ComboLayout) findViewById(R.id.Combo_chntype);
            this.Combo_startTime = (ComboLayout) findViewById(R.id.Combo_startTime);
            this.Combo_duration = (ComboLayout) findViewById(R.id.Combo_duration);
            this.Combo_endtime = (ComboLayout) findViewById(R.id.Combo_endtime);
            this.Combo_mode = (ComboLayout) findViewById(R.id.Combo_mode);
            this.Combo_type = (ComboLayout) findViewById(R.id.Combo_type);
            this.Linear_startTime = (LinearLayout) findViewById(R.id.Linear_startTime);
            this.Linear_endtime = (LinearLayout) findViewById(R.id.Linear_endtime);
            this.Linear_Vdate = (LinearLayout) findViewById(R.id.Linear_Vdate);
            this.Frame_duration = (FrameLayout) findViewById(R.id.duration_fl);
            this.et_hour = (DigitsEditText) findViewById(R.id.duration_hour);
            this.et_min = (DigitsEditText) findViewById(R.id.duration_min);
            if (SETTINGS.bHongKongDTMB) {
                this.Combo_date.setVisibility(View.GONE);
                this.Combo_startTime.setVisibility(View.GONE);
                this.Combo_endtime.setVisibility(View.GONE);
                this.Frame_duration.setVisibility(View.GONE);
                this.Linear_endtime.setEnabled(false);
                this.Linear_endtime.setFocusable(false);
            } else {
                this.Linear_startTime.setVisibility(View.GONE);
                this.Linear_endtime.setVisibility(View.GONE);
                this.Linear_Vdate.setVisibility(View.GONE);
                this.Combo_endtime.setEnabled(false);
                this.Combo_endtime.setFocusable(false);
                this.Combo_endtime.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
            }
            if (MW.get_date_time(this.mDateTime) == 0) {
            }
            if (MW.get_date(0, null, this.mDate) == 0) {
            }
            if (this.bEdit) {
                if (MW.book_get_event_by_index(TimerListActivity.this.timerlist_item_pos, this.tmpBookEvent) == 0) {
                    if (SETTINGS.bHongKongDTMB) {
                        this.myear = this.tmpBookEvent.start_year;
                        this.mmonth = this.tmpBookEvent.start_month;
                        this.mday = this.tmpBookEvent.start_day;
                        this.mhour = this.tmpBookEvent.start_hour;
                        this.mminute = this.tmpBookEvent.start_minute;
                        this.edyear = this.tmpBookEvent.end_year;
                        this.edmonth = this.tmpBookEvent.end_month;
                        this.edday = this.tmpBookEvent.end_day;
                        this.edhour = this.tmpBookEvent.end_hour;
                        this.edminute = this.tmpBookEvent.end_minute;
                    } else {
                        this.Year = this.tmpBookEvent.start_year;
                        this.Month = this.tmpBookEvent.start_month;
                        this.Day = this.tmpBookEvent.start_day;
                        this.start_hour = this.tmpBookEvent.start_hour;
                        this.start_min = this.tmpBookEvent.start_minute;
                    }
                    this.channel_type = this.tmpBookEvent.service_type;
                    this.channel_index = this.tmpBookEvent.service_index;
                    this.timer_type = this.tmpBookEvent.book_type;
                    this.timer_mode = this.tmpBookEvent.book_mode;
                    if (!SETTINGS.bHongKongDTMB) {
                        this.duration_hour = this.tmpBookEvent.end_hour - this.tmpBookEvent.start_hour;
                        if (this.duration_hour < 0) {
                            this.duration_hour += 24;
                        }
                        this.duration_min = this.tmpBookEvent.end_minute - this.tmpBookEvent.start_minute;
                        if (this.duration_min < 0) {
                            this.duration_min += 60;
                        }
                        this.mDate.year = this.Year;
                        this.mDate.month = this.Month;
                        this.mDate.day = this.Day;
                        this.mDateTime.hour = this.start_hour;
                        this.mDateTime.minute = this.start_min;
                        Log.e("TIMER", "start_hour:start_min" + this.start_hour + ":" + this.start_min);
                    }
                }
                this.mTitletext.setText(TimerListActivity.this.getResources().getString(R.string.edit));
            } else {
                this.channel_type = TimerListActivity.this.current_service_type;
                this.channel_index = TimerListActivity.this.current_service_index;
                this.timer_type = 1;
                this.timer_mode = 0;
                if (SETTINGS.bHongKongDTMB) {
                    this.myear = this.mDate.year;
                    this.mmonth = this.mDate.month;
                    this.mday = this.mDate.day;
                    int i = this.mDateTime.hour;
                    this.mhour = i;
                    this.edhour = i;
                    i = this.mDateTime.minute + 1;
                    this.start_min = i;
                    this.mminute = i;
                    this.edminute = i;
                } else {
                    this.Year = this.mDate.year;
                    this.Month = this.mDate.month;
                    this.Day = this.mDate.day;
                    this.start_hour = this.mDateTime.hour;
                    this.start_min = this.mDateTime.minute + 1;
                    this.mDateTime.minute = this.start_min;
                }
                this.duration_hour = 0;
                this.duration_min = 0;
            }
            initAddView();
        }

        public void dismiss() {
            if (TimerListActivity.this.mbStartSchedule) {
                TimerListActivity.this.mbStartSchedule = false;
            }
            super.dismiss();
        }

        private void initStartTime() {
            this.to_edit_time = SETTINGS.getDateTimeAdjustSystem(TimerListActivity.this.dateTime, true);
            System.out.println("Setting :" + this.to_edit_time);
            this.st_year = (EditText) this.Linear_Vdate.findViewById(R.id.st_year);
            if (TimerListActivity.this.mbStartSchedule) {
                this.st_year.setText("" + TimerListActivity.this.mBookStYear);
            } else {
                this.st_year.setText("" + this.myear);
            }
            this.st_year.setSelection(this.st_year.getText().length());
            this.st_month = (EditText) this.Linear_Vdate.findViewById(R.id.st_month);
            if (TimerListActivity.this.mbStartSchedule) {
                this.st_month.setText("" + TimerListActivity.this.mBookStMonth);
            } else {
                this.st_month.setText("" + this.mmonth);
            }
            this.st_month.setSelection(this.st_month.getText().length());
            this.st_day = (EditText) this.Linear_Vdate.findViewById(R.id.st_day);
            if (TimerListActivity.this.mbStartSchedule) {
                this.st_day.setText("" + TimerListActivity.this.mBookStDay);
            } else {
                this.st_day.setText("" + this.mday);
            }
            this.st_day.setSelection(this.st_day.getText().length());
            if (SETTINGS.bHongKongDTMB) {
                this.st_hour = (EditText) this.Linear_startTime.findViewById(R.id.hour);
            } else {
                this.st_hour = (EditText) this.Combo_startTime.findViewById(R.id.hour);
            }
            if (TimerListActivity.this.mbStartSchedule) {
                if (TimerListActivity.this.mBookStHour < 10) {
                    this.st_hour.setText("0" + TimerListActivity.this.mBookStHour);
                } else {
                    this.st_hour.setText("" + TimerListActivity.this.mBookStHour);
                }
            } else if (this.mhour < 10) {
                this.st_hour.setText("0" + this.mhour);
            } else {
                this.st_hour.setText("" + this.mhour);
            }
            this.st_hour.setSelection(this.st_hour.getText().length());
            if (SETTINGS.bHongKongDTMB) {
                this.st_minute = (EditText) this.Linear_startTime.findViewById(R.id.minute);
            } else {
                this.st_minute = (EditText) this.Combo_startTime.findViewById(R.id.minute);
            }
            if (TimerListActivity.this.mbStartSchedule) {
                if (TimerListActivity.this.mBookStMinute < 10) {
                    this.st_minute.setText("0" + TimerListActivity.this.mBookStMinute);
                } else {
                    this.st_minute.setText("" + TimerListActivity.this.mBookStMinute);
                }
            } else if (this.mminute < 10) {
                this.st_minute.setText("0" + this.mminute);
            } else {
                this.st_minute.setText("" + this.mminute);
            }
            this.st_minute.setSelection(this.st_minute.getText().length());
        }

        private void initEndTimes() {
            this.to_edit_time = SETTINGS.getDateTimeAdjustSystem(TimerListActivity.this.dateTime, true);
            this.ed_hour = (EditText) this.Linear_endtime.findViewById(R.id.ed_hour);
            if (TimerListActivity.this.mbStartSchedule) {
                if (TimerListActivity.this.mBookEndHour < 10) {
                    this.ed_hour.setText("0" + TimerListActivity.this.mBookEndHour);
                } else {
                    this.ed_hour.setText("" + TimerListActivity.this.mBookEndHour);
                }
            } else if (this.edhour < 10) {
                this.ed_hour.setText("0" + this.edhour);
            } else {
                this.ed_hour.setText("" + this.edhour);
            }
            this.ed_hour.setSelection(this.ed_hour.getText().length());
            this.ed_minute = (EditText) this.Linear_endtime.findViewById(R.id.ed_minute);
            if (TimerListActivity.this.mbStartSchedule) {
                if (TimerListActivity.this.mBookEndMinute < 10) {
                    this.ed_minute.setText("0" + TimerListActivity.this.mBookEndMinute);
                } else {
                    this.ed_minute.setText("" + TimerListActivity.this.mBookEndMinute);
                }
            } else if (this.edminute < 10) {
                this.ed_minute.setText("0" + this.edminute);
            } else {
                this.ed_minute.setText("" + this.edminute);
            }
            this.ed_minute.setSelection(this.ed_minute.getText().length());
        }

        private void getStartTime() {
            this.myear = Integer.parseInt(this.st_year.getText().toString());
            this.mmonth = Integer.parseInt(this.st_month.getText().toString());
            this.mday = Integer.parseInt(this.st_day.getText().toString());
            this.mhour = Integer.parseInt(this.st_hour.getText().toString());
            this.mminute = Integer.parseInt(this.st_minute.getText().toString());
        }

        private void getEndTimes() {
            this.edhour = Integer.parseInt(this.ed_hour.getText().toString());
            this.edminute = Integer.parseInt(this.ed_minute.getText().toString());
        }

        private void initAddView() {
            initTypeItem();
            initModeItem();
            initChannelType();
            initChannelItem(this.channel_type, this.channel_index);
            initDateItem();
            initStartTimeItem();
            initDurationItem();
            initEndTimeItem();
            if (SETTINGS.bHongKongDTMB) {
                initStartTime();
                initEndTimes();
            }
        }

        private void initTypeItem() {
            Log.e("TIMER", "timer_type ::" + this.timer_type);
            this.Combo_type.initView((int) R.string.Type, (int) R.array.timer_type, this.timer_type - 1, new C02703());
            this.Combo_type.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.Combo_type.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            RefreshItems(this.timer_type);
        }

        private void RefreshItems(int type) {
            switch (type) {
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    this.Combo_chname.setEnabled(true);
                    this.Combo_chname.setFocusable(true);
                    this.Combo_chname.setBackgroundResource(R.drawable.setlect_white_focus);
                    this.Combo_chntype.setEnabled(true);
                    this.Combo_chntype.setFocusable(true);
                    this.Combo_chntype.setBackgroundResource(R.drawable.setlect_white_focus);
                    this.et_hour.setEnabled(false);
                    this.et_min.setEnabled(false);
                    this.Combo_duration.setEnabled(false);
                    this.Combo_duration.setFocusable(false);
                    this.Combo_duration.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    this.Combo_chname.setEnabled(true);
                    this.Combo_chname.setFocusable(true);
                    this.Combo_chname.setBackgroundResource(R.drawable.setlect_white_focus);
                    this.Combo_chntype.setEnabled(true);
                    this.Combo_chntype.setFocusable(true);
                    this.Combo_chntype.setBackgroundResource(R.drawable.setlect_white_focus);
                    this.et_hour.setEnabled(true);
                    this.et_min.setEnabled(true);
                    this.Combo_duration.setEnabled(true);
                    this.Combo_duration.setFocusable(true);
                    this.Combo_duration.setBackgroundResource(R.drawable.setlect_white_focus);
                case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    this.Combo_chname.setEnabled(false);
                    this.Combo_chname.setFocusable(false);
                    this.Combo_chname.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                    this.Combo_chntype.setEnabled(false);
                    this.Combo_chntype.setFocusable(false);
                    this.Combo_chntype.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                    this.et_hour.setEnabled(false);
                    this.et_min.setEnabled(false);
                    this.Combo_duration.setEnabled(false);
                    this.Combo_duration.setFocusable(false);
                    this.Combo_duration.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                default:
            }
        }

        private void initModeItem() {
            this.Combo_mode.initView((int) R.string.Mode, (int) R.array.timer_mode, this.timer_mode, new C02714());
            this.Combo_mode.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.Combo_mode.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void initEndTimeItem() {
            if (!SETTINGS.bHongKongDTMB) {
                String time = DateFormat.getTimeFormat(this.mContext).format(Calendar.getInstance().getTime());
                this.Combo_endtime.initView((int) R.string.End_Time, 0, 0, new C02725());
                this.Combo_endtime.getLeftImageView().setVisibility(View.INVISIBLE);
                this.Combo_endtime.getRightImageView().setVisibility(View.INVISIBLE);
                this.Combo_endtime.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
                this.Combo_endtime.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
                this.Combo_endtime.getInfoTextView().setText(time);
                this.Combo_endtime.setVisibility(View.GONE);
            }
        }

        private void initDurationItem() {
            this.Combo_duration.initView((int) R.string.Duration, 0, 0, new C02736());
            this.Combo_duration.getLeftImageView().setVisibility(View.INVISIBLE);
            this.Combo_duration.getRightImageView().setVisibility(View.INVISIBLE);
            this.Combo_duration.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.Combo_duration.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.et_hour.setDigitsText(String.format("%d", new Object[]{Integer.valueOf(this.duration_hour)}));
            this.et_min.setDigitsText(String.format("%d", new Object[]{Integer.valueOf(this.duration_min)}));
        }

        private void initStartTimeItem() {
            if (!SETTINGS.bHongKongDTMB) {
                this.Combo_startTime.initView((int) R.string.Start_Time, 0, 0, new C02747());
                this.Combo_startTime.getLeftImageView().setVisibility(View.INVISIBLE);
                this.Combo_startTime.getRightImageView().setVisibility(View.INVISIBLE);
                this.Combo_startTime.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
                this.Combo_startTime.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
                this.Combo_startTime.getInfoTextView().setText(SETTINGS.getTimeAdjustSystem(this.mDateTime, false));
            }
        }

        private void initChannelType() {
            ArrayList items = new ArrayList();
            items.clear();
            if (MW.db_get_service_count(0) > 0 && MW.db_get_service_count(1) <= 0) {
                items.add(TimerListActivity.this.getResources().getString(R.string.TV));
            } else if (MW.db_get_service_count(0) < 0 && MW.db_get_service_count(1) > 0) {
                items.add(TimerListActivity.this.getResources().getString(R.string.Radio));
            } else if (MW.db_get_service_count(0) > 0 && MW.db_get_service_count(1) > 0) {
                items.add(TimerListActivity.this.getResources().getString(R.string.TV));
                items.add(TimerListActivity.this.getResources().getString(R.string.Radio));
            }
            this.Combo_chntype.initView((int) R.string.channel_type, items, this.channel_type, new C02758());
            this.Combo_chntype.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.Combo_chntype.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void initChannelItem(int service_type, int service_index) {
            ArrayList items = new ArrayList();
            items.clear();
            for (int i = 0; i < MW.db_get_service_count(service_type); i++) {
                if (MW.db_get_service_info(service_type, i, this.serviceInfo) == 0) {
                    items.add(this.serviceInfo.channel_number_display + "   " + this.serviceInfo.service_name + "");
                }
            }
            this.Combo_chname.initView((int) R.string.channel_name, items, service_index, new C02769());
            this.Combo_chname.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.Combo_chname.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
        }

        private void initDateItem() {
            this.Combo_date.initView((int) R.string.Date, 0, 0, new OnItemClickListener() {
                public void onItemClick(AdapterView parent, View v, int position, long id) {
                    new DatePickerDialog(AddDialog.this.mContext, 3, AddDialog.this.mOnDateSetListener, AddDialog.this.Year, AddDialog.this.Month - 1, AddDialog.this.Day).show();
                }
            });
            this.Combo_date.getLeftImageView().setVisibility(View.INVISIBLE);
            this.Combo_date.getRightImageView().setVisibility(View.INVISIBLE);
            this.Combo_date.getTitleTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * 160) / 1280, -2));
            this.Combo_date.getInfoTextView().setLayoutParams(new LayoutParams((SETTINGS.getWindowWidth() * MW.TUNER_ID_DVBT_CXD2837) / 1280, -2));
            this.Combo_date.getInfoTextView().setText(SETTINGS.getDateAdjustSystem(this.mDate));
        }

        public AddDialog(Context context, int theme, boolean bedit) {
            super(context, theme);
            this.mContext = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitletext = null;
            this.bEdit = false;
            this.channel_type = 0;
            this.channel_index = 0;
            this.timer_type = 1;
            this.timer_mode = 0;
            this.Year = 0;
            this.Month = 0;
            this.Day = 0;
            this.start_min = 0;
            this.start_hour = 0;
            this.duration_min = 0;
            this.duration_hour = 0;
            this.mDateTime = new date_time();
            this.mDate = new date();
            this.serviceInfo = new service();
            this.tmpBookEvent = new book_event_data();
            this.mOnDateSetListener = new OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    AddDialog.this.mDate.year = year;
                    AddDialog.this.mDate.month = month + 1;
                    AddDialog.this.mDate.day = day;
                    AddDialog.this.Combo_date.getInfoTextView().setText(SETTINGS.getDateAdjustSystem(AddDialog.this.mDate));
                    AddDialog.this.Year = AddDialog.this.mDate.year;
                    AddDialog.this.Month = AddDialog.this.mDate.month;
                    AddDialog.this.Day = AddDialog.this.mDate.day;
                }
            };
            this.mOnTimeSetListener = new OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    AddDialog.this.start_hour = hourOfDay;
                    AddDialog.this.start_min = minute;
                    AddDialog.this.mDateTime.hour = hourOfDay;
                    AddDialog.this.mDateTime.minute = minute;
                    AddDialog.this.mDateTime.second = 0;
                    if (!SETTINGS.bHongKongDTMB) {
                        AddDialog.this.Combo_startTime.getInfoTextView().setText(SETTINGS.getTimeAdjustSystem(AddDialog.this.mDateTime, false));
                    }
                }
            };
            this.mOnTimeSetListener1 = new OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    AddDialog.this.duration_hour = hourOfDay;
                    AddDialog.this.duration_min = minute;
                    AddDialog.this.Combo_duration.getInfoTextView().setText(String.format("%02d:%02d", new Object[]{Integer.valueOf(AddDialog.this.duration_hour), Integer.valueOf(AddDialog.this.duration_min)}));
                }
            };
            this.mContext = context;
            this.bEdit = bedit;
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            switch (i) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.mBtnCancel.isFocused() || this.mBtnStart.isFocused()) {
                        if (SETTINGS.bHongKongDTMB) {
                            this.Linear_endtime.requestFocus();
                            return true;
                        } else if (this.Combo_duration.isEnabled()) {
                            this.Combo_duration.setBackgroundResource(R.drawable.live_channel_list_item_bg_sel);
                            this.et_hour.requestFocus();
                            return true;
                        } else {
                            this.Combo_startTime.requestFocus();
                            return true;
                        }
                    } else if (this.et_hour.isFocused() || this.et_min.isFocused()) {
                        if (this.et_hour.getText().toString().equals("")) {
                            this.et_hour.setDigitsText(String.format("%d", new Object[]{Integer.valueOf(0)}));
                        }
                        if (this.et_min.getText().toString().equals("")) {
                            this.et_min.setDigitsText(String.format("%d", new Object[]{Integer.valueOf(0)}));
                        }
                        this.Combo_duration.setBackgroundResource(R.drawable.setlect_white_focus);
                        if (SETTINGS.bHongKongDTMB) {
                            return true;
                        }
                        this.Combo_startTime.requestFocus();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.Combo_startTime.isFocused()) {
                        if (this.Combo_duration.isEnabled()) {
                            this.Combo_duration.setBackgroundResource(R.drawable.live_channel_list_item_bg_sel);
                            this.et_hour.requestFocus();
                            return true;
                        }
                        this.mBtnStart.requestFocus();
                        return true;
                    } else if (this.Combo_duration.isFocused()) {
                        this.mBtnStart.requestFocus();
                        return true;
                    } else if (this.et_hour.isFocused() || this.et_min.isFocused()) {
                        if (this.et_hour.getText().toString().equals("")) {
                            this.et_hour.setDigitsText(String.format("%d", new Object[]{Integer.valueOf(0)}));
                        }
                        if (this.et_min.getText().toString().equals("")) {
                            this.et_min.setDigitsText(String.format("%d", new Object[]{Integer.valueOf(0)}));
                        }
                        this.mBtnStart.requestFocus();
                        this.Combo_duration.setBackgroundResource(R.drawable.setlect_white_focus);
                        return true;
                    }
            }
            return super.onKeyDown(i, keyEvent);
        }
    }

    public class DeleteConflictDialog extends Dialog {
        TextView channel_name;
        TextView deldia_title;
        TextView delete_content;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private date_time mDateTime;
        private LayoutInflater mInflater;
        View mLayoutView;
        private service mServiceInfo;
        private int mTimerMode;
        private int mTimerType;
        private book_event_data mTmpBookEvent;
        private boolean mbEdit;
        private int mi;
        private String mtitle;
        TextView program_name;
        TextView time;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bookconflic_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnStart.setText(TimerListActivity.this.getResources().getString(R.string.replace));
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.delete_content = (TextView) findViewById(R.id.delete_content);
            this.time = (TextView) findViewById(R.id.time);
            this.channel_name = (TextView) findViewById(R.id.channel_name);
            this.program_name = (TextView) findViewById(R.id.program_name);
            this.deldia_title.setText(this.mtitle);
            if (this.mTmpBookEvent != null) {
                System.out.println("  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(this.mTmpBookEvent.start_year), Integer.valueOf(this.mTmpBookEvent.start_month), Integer.valueOf(this.mTmpBookEvent.start_day), Integer.valueOf(this.mTmpBookEvent.start_hour), Integer.valueOf(this.mTmpBookEvent.start_minute), Integer.valueOf(this.mTmpBookEvent.end_year), Integer.valueOf(this.mTmpBookEvent.end_month), Integer.valueOf(this.mTmpBookEvent.end_day), Integer.valueOf(this.mTmpBookEvent.end_hour), Integer.valueOf(this.mTmpBookEvent.end_minute)}) + " ## " + this.mTmpBookEvent.service_name + " ## " + this.mTmpBookEvent.event_name + " ## " + (this.mTmpBookEvent.book_type == 1 ? "Play" : "Record"));
                this.delete_content.setText(R.string.dywtreplace);
                if (this.mTmpBookEvent.book_type == 2) {
                    this.channel_name.setVisibility(View.VISIBLE);
                    this.channel_name.setText(TimerListActivity.this.getResources().getString(R.string.channel_name) + " : " + this.mTmpBookEvent.service_name);
                    this.time.setText(TimerListActivity.this.getResources().getString(R.string.book_time) + " : " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(this.mTmpBookEvent.start_year), Integer.valueOf(this.mTmpBookEvent.start_month), Integer.valueOf(this.mTmpBookEvent.start_day), Integer.valueOf(this.mTmpBookEvent.start_hour), Integer.valueOf(this.mTmpBookEvent.start_minute), Integer.valueOf(this.mTmpBookEvent.end_year), Integer.valueOf(this.mTmpBookEvent.end_month), Integer.valueOf(this.mTmpBookEvent.end_day), Integer.valueOf(this.mTmpBookEvent.end_hour), Integer.valueOf(this.mTmpBookEvent.end_minute)}));
                } else if (this.mTmpBookEvent.book_type == 1) {
                    this.channel_name.setVisibility(View.VISIBLE);
                    this.channel_name.setText(TimerListActivity.this.getResources().getString(R.string.channel_name) + " : " + this.mTmpBookEvent.service_name);
                    this.time.setText(TimerListActivity.this.getResources().getString(R.string.book_time) + " : " + String.format("%04d-%02d-%02d %02d:%02d", new Object[]{Integer.valueOf(this.mTmpBookEvent.start_year), Integer.valueOf(this.mTmpBookEvent.start_month), Integer.valueOf(this.mTmpBookEvent.start_day), Integer.valueOf(this.mTmpBookEvent.start_hour), Integer.valueOf(this.mTmpBookEvent.start_minute)}));
                } else {
                    this.channel_name.setVisibility(View.GONE);
                    this.time.setText(TimerListActivity.this.getResources().getString(R.string.book_time) + " : " + String.format("%04d-%02d-%02d %02d:%02d", new Object[]{Integer.valueOf(this.mTmpBookEvent.start_year), Integer.valueOf(this.mTmpBookEvent.start_month), Integer.valueOf(this.mTmpBookEvent.start_day), Integer.valueOf(this.mTmpBookEvent.start_hour), Integer.valueOf(this.mTmpBookEvent.start_minute)}));
                }
                this.program_name.setText(TimerListActivity.this.getResources().getString(R.string.Type) + " : " + TimerListActivity.this.getResources().getStringArray(R.array.timer_type)[this.mTmpBookEvent.book_type - 1]);
            }
            this.mBtnCancel.requestFocus();
        }

        public DeleteConflictDialog(Context context, int theme, String title, boolean bEdit, int timer_type, int timer_mode, service serviceInfo, date_time dateTime, int i, book_event_data tmpBookEvent) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mtitle = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.deldia_title = null;
            this.delete_content = null;
            this.time = null;
            this.channel_name = null;
            this.program_name = null;
            this.mTimerType = 0;
            this.mTimerMode = 0;
            this.mServiceInfo = new service();
            this.mDateTime = new date_time();
            this.mi = 0;
            this.mTmpBookEvent = new book_event_data();
            this.mbEdit = false;
            this.mContext = context;
            this.mtitle = title;
            this.mTimerType = timer_type;
            this.mTimerMode = timer_mode;
            this.mServiceInfo = serviceInfo;
            this.mDateTime = dateTime;
            this.mi = i;
            this.mTmpBookEvent = tmpBookEvent;
            this.mbEdit = bEdit;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView txt_Title;
        TextView txt_content;
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.txt_Title = (TextView) findViewById(R.id.deldia_title);
            this.txt_content = (TextView) findViewById(R.id.delete_content);
            this.txt_Title.setText(TimerListActivity.this.getResources().getString(R.string.cancle_book));
            this.txt_content.setText(TimerListActivity.this.getResources().getString(R.string.cancle_book_tips));
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.txt_Title = null;
            this.txt_content = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                case MW.RET_TP_EXIST /*10*/:
                    TimerListActivity.this.timerListAdapter.notifyDataSetChanged();
                default:
            }
        }
    }

    private class TimerListAdapter extends BaseAdapter {
        private date_time datetime;
        private date_time enddatetime;
        private LayoutInflater mInflater;
        private Context mcontext;
        private book_event_data tmpBookEvent;

        class ViewHolder {
            TextView timer_chname;
            TextView timer_datetime;
            TextView timer_mode;
            TextView timer_no;
            TextView timer_type;

            ViewHolder() {
            }
        }

        public TimerListAdapter(Context context) {
            this.datetime = new date_time();
            this.enddatetime = new date_time();
            this.tmpBookEvent = new book_event_data();
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.book_get_event_count();
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.timer_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.timer_no = (TextView) convertView.findViewById(R.id.timer_no);
                localViewHolder.timer_chname = (TextView) convertView.findViewById(R.id.timer_chname);
                localViewHolder.timer_datetime = (TextView) convertView.findViewById(R.id.timer_datetime);
                localViewHolder.timer_mode = (TextView) convertView.findViewById(R.id.timer_mode);
                localViewHolder.timer_type = (TextView) convertView.findViewById(R.id.timer_type);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.book_get_event_by_index(position, this.tmpBookEvent) == 0) {
                Log.e("Book", "  " + (position + 1) + ".  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", new Object[]{Integer.valueOf(this.tmpBookEvent.start_year), Integer.valueOf(this.tmpBookEvent.start_month), Integer.valueOf(this.tmpBookEvent.start_day), Integer.valueOf(this.tmpBookEvent.start_hour), Integer.valueOf(this.tmpBookEvent.start_minute), Integer.valueOf(this.tmpBookEvent.end_year), Integer.valueOf(this.tmpBookEvent.end_month), Integer.valueOf(this.tmpBookEvent.end_day), Integer.valueOf(this.tmpBookEvent.end_hour), Integer.valueOf(this.tmpBookEvent.end_minute)}) + " ## " + this.tmpBookEvent.service_name + " ## " + this.tmpBookEvent.event_name + " ## " + this.tmpBookEvent.book_type);
                if (this.tmpBookEvent.book_type == 1) {
                    localViewHolder.timer_no.setText((position + 1) + "");
                    localViewHolder.timer_chname.setText(this.tmpBookEvent.service_name);
                    localViewHolder.timer_type.setText(TimerListActivity.this.getString(R.string.Play));
                    localViewHolder.timer_mode.setText(TimerListActivity.this.getResources().getStringArray(R.array.timer_mode)[this.tmpBookEvent.book_mode]);
                    localViewHolder.timer_datetime.setTextSize(20.0f);
                    this.datetime.year = this.tmpBookEvent.start_year;
                    this.datetime.month = this.tmpBookEvent.start_month;
                    this.datetime.day = this.tmpBookEvent.start_day;
                    this.datetime.hour = this.tmpBookEvent.start_hour;
                    this.datetime.minute = this.tmpBookEvent.start_minute;
                    this.datetime.second = 0;
                    localViewHolder.timer_datetime.setText(SETTINGS.getDateTimeAdjustSystem(this.datetime, false));
                } else if (this.tmpBookEvent.book_type == 2) {
                    localViewHolder.timer_no.setText((position + 1) + "");
                    localViewHolder.timer_chname.setText(this.tmpBookEvent.service_name);
                    localViewHolder.timer_mode.setText(TimerListActivity.this.getResources().getStringArray(R.array.timer_mode)[this.tmpBookEvent.book_mode]);
                    localViewHolder.timer_type.setText(TimerListActivity.this.getString(R.string.Record));
                    localViewHolder.timer_datetime.setTextSize(20.0f);
                    this.datetime.year = this.tmpBookEvent.start_year;
                    this.datetime.month = this.tmpBookEvent.start_month;
                    this.datetime.day = this.tmpBookEvent.start_day;
                    this.datetime.hour = this.tmpBookEvent.start_hour;
                    this.datetime.minute = this.tmpBookEvent.start_minute;
                    this.datetime.second = 0;
                    this.enddatetime.year = this.tmpBookEvent.end_year;
                    this.enddatetime.month = this.tmpBookEvent.end_month;
                    this.enddatetime.day = this.tmpBookEvent.end_day;
                    this.enddatetime.hour = this.tmpBookEvent.end_hour;
                    this.enddatetime.minute = this.tmpBookEvent.end_minute;
                    this.enddatetime.second = 0;
                    localViewHolder.timer_datetime.setText(SETTINGS.getDateTimeAdjustSystem(this.datetime, false) + "\n" + "-" + SETTINGS.getDateTimeAdjustSystem(this.enddatetime, false));
                } else if (this.tmpBookEvent.book_type == 3) {
                    localViewHolder.timer_no.setText((position + 1) + "");
                    localViewHolder.timer_chname.setText("----");
                    localViewHolder.timer_mode.setText(TimerListActivity.this.getResources().getStringArray(R.array.timer_mode)[this.tmpBookEvent.book_mode]);
                    localViewHolder.timer_type.setText(TimerListActivity.this.getResources().getStringArray(R.array.timer_type)[2]);
                    localViewHolder.timer_datetime.setTextSize(20.0f);
                    this.datetime.year = this.tmpBookEvent.start_year;
                    this.datetime.month = this.tmpBookEvent.start_month;
                    this.datetime.day = this.tmpBookEvent.start_day;
                    this.datetime.hour = this.tmpBookEvent.start_hour;
                    this.datetime.minute = this.tmpBookEvent.start_minute;
                    this.datetime.second = 0;
                    localViewHolder.timer_datetime.setText(SETTINGS.getDateTimeAdjustSystem(this.datetime, false));
                } else if (this.tmpBookEvent.book_type == 4) {
                    localViewHolder.timer_no.setText((position + 1) + "");
                    localViewHolder.timer_chname.setText("----");
                    localViewHolder.timer_mode.setText(TimerListActivity.this.getResources().getStringArray(R.array.timer_mode)[this.tmpBookEvent.book_mode]);
                    localViewHolder.timer_type.setText(TimerListActivity.this.getResources().getStringArray(R.array.timer_type)[3]);
                    localViewHolder.timer_datetime.setTextSize(20.0f);
                    this.datetime.year = this.tmpBookEvent.start_year;
                    this.datetime.month = this.tmpBookEvent.start_month;
                    this.datetime.day = this.tmpBookEvent.start_day;
                    this.datetime.hour = this.tmpBookEvent.start_hour;
                    this.datetime.minute = this.tmpBookEvent.start_minute;
                    this.datetime.second = 0;
                    localViewHolder.timer_datetime.setText(SETTINGS.getDateTimeAdjustSystem(this.datetime, false));
                }
            }
            return convertView;
        }
    }

    private class listOnKeyListener implements OnKeyListener {
        private listOnKeyListener() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        TimerListActivity.this.DoEditAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                        TimerListActivity.this.DoDeleteAction();
                        return true;
                }
            }
            return false;
        }
    }

    public TimerListActivity() {
        this.timer_listview = null;
        this.timerListAdapter = null;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.timerlist_item_pos = 0;
        this.mTmpBookEvent = new book_event_data();
        this.current_dig = null;
        this.serviceInfo = new service();
        this.current_service_type = 0;
        this.current_service_index = 0;
        this.dateTime = new date_time();
        this.mbStartSchedule = false;
    }

    private void initMessagehandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.timerlist_activity);
        initData();
        initView();
        initMessagehandle();
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.send_led_msg("NENU");
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(4, true);
        MW.register_event_type(10, true);
        if (MW.get_date_time(this.dateTime) == 0) {
            this.current_date_time.setText(SETTINGS.getDateTimeAdjustSystem(this.dateTime, true));
        }
        this.mbStartSchedule = getIntent().getBooleanExtra("bStartSchedule", false);
        if (this.mbStartSchedule) {
            this.mBookStYear = getIntent().getIntExtra("startBookYear", this.dateTime.year);
            this.mBookStMonth = getIntent().getIntExtra("startBookMonth", this.dateTime.month);
            this.mBookStDay = getIntent().getIntExtra("startBookDay", this.dateTime.day);
            this.mBookStHour = getIntent().getIntExtra("startBookHour", this.dateTime.hour);
            this.mBookStMinute = getIntent().getIntExtra("startBookMinute", this.dateTime.minute);
            this.mBookEndHour = getIntent().getIntExtra("endBookHour", this.dateTime.hour);
            this.mBookEndMinute = getIntent().getIntExtra("endBookMinute", this.dateTime.minute);
            DoAddAction();
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.current_dig != null) {
            this.current_dig.dismiss();
        }
        enableMwMessageCallback(null);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void initData() {
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
        }
    }

    private void initView() {
        this.current_date_time = (TextView) findViewById(R.id.current_time);
        this.timer_listview = (CustomListView) findViewById(R.id.timer_listview);
        this.timerListAdapter = new TimerListAdapter(this);
        this.timer_listview.setAdapter(this.timerListAdapter);
        this.timer_listview.setChoiceMode(1);
        this.timer_listview.setCustomSelection(this.mFirstVisibleItem);
        this.timer_listview.setCustomScrollListener(new C02661());
        this.timer_listview.setOnItemSelectedListener(new C02672());
        this.timer_listview.setCustomKeyListener(new listOnKeyListener());
        this.timer_listview.setVisibleItemCount(11);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    finish();
                    break;
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    DoAddAction();
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void DoAddAction() {
        this.current_dig = new AddDialog(this, R.style.MyDialog, false);
        this.current_dig.show();
        this.current_dig.getWindow().addFlags(2);
    }

    private void DoEditAction() {
        this.current_dig = new AddDialog(this, R.style.MyDialog, true);
        this.current_dig.show();
        this.current_dig.getWindow().addFlags(2);
    }

    private void DoDeleteAction() {
        this.current_dig = new DeleteDia(this, R.style.MyDialog);
        this.current_dig.show();
        this.current_dig.getWindow().addFlags(2);
    }

    private void ShowConflictEventDialog(Context mContext, String Title, boolean bEdit, int timer_type, int timer_mode, service serviceInfo, date_time mDateTime, int i, book_event_data tmpBookEvent) {
        this.current_dig = new DeleteConflictDialog(mContext, R.style.MyDialog, Title, bEdit, timer_type, timer_mode, serviceInfo, mDateTime, i, tmpBookEvent);
        this.current_dig.show();
        this.current_dig.getWindow().addFlags(2);
    }

    private void Add_booktype_event(boolean bEdit, int timer_type, int timer_mode, service serviceInfo, date_time mDateTime, int i, book_event_data tmpBookEvent) {
        int Ret;
        if (bEdit) {
            Ret = MW.book_manual_modify_event(tmpBookEvent.event_index, timer_type, timer_mode, serviceInfo, mDateTime, 0, tmpBookEvent);
            Log.e("TIMER", "modify----->Ret: " + Ret);
            if (Ret == 0) {
                this.timerListAdapter.notifyDataSetChanged();
                return;
            } else if (Ret == 14) {
                SETTINGS.makeText(this, R.string.invalid_value, 0);
                return;
            } else if (Ret == 15) {
                ShowConflictEventDialog(this, getResources().getString(R.string.conflictevent), bEdit, timer_type, timer_mode, serviceInfo, mDateTime, i, tmpBookEvent);
                return;
            } else {
                Log.e("TIMER", "failed----->Ret: " + Ret);
                return;
            }
        }
        Ret = MW.book_manual_add_event(timer_type, timer_mode, serviceInfo, mDateTime, 0, tmpBookEvent);
        Log.e("TIMER", "add----->Ret: " + Ret);
        if (Ret == 0) {
            this.timerListAdapter.notifyDataSetChanged();
        } else if (Ret == 14) {
            SETTINGS.makeText(this, R.string.invalid_value, 0);
            return;
        } else if (Ret == 15) {
            ShowConflictEventDialog(this, getResources().getString(R.string.conflictevent), bEdit, timer_type, timer_mode, serviceInfo, mDateTime, i, tmpBookEvent);
        } else {
            Log.e("TIMER", "failed----->Ret: " + Ret);
        }
        Log.e("TIMER", "MW.book_manual_add_event ----end");
    }
}
