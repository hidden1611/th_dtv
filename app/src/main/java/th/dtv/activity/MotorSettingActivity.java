package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import th.dtv.ChannelSearchActivity;
import th.dtv.ComboLayout;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.tuner_signal_status;

public class MotorSettingActivity extends DtvBaseActivity {
    private ComboLayout Combo_ContinuingMove;
    private ComboLayout Combo_GOTO;
    private ComboLayout Combo_Latitude;
    private ComboLayout Combo_Limit;
    private ComboLayout Combo_Longitude;
    private ComboLayout Combo_MotorType;
    private ComboLayout Combo_MoveStep;
    private ComboLayout Combo_PosCommand;
    private ComboLayout Combo_Satellite;
    private ComboLayout Combo_Transponder;
    private ComboLayout Combo_hemisphere;
    private ComboLayout Combo_location;
    ArrayList<Integer> SelSatList;
    ArrayList<Integer> SelTpList;
    int currentSatIndex;
    int currentSatPos;
    int currentTpIndex;
    private boolean fSaveSatTpFlag;
    private int gotoCommand;
    private LinearLayout help_tips;
    private long limitTime;
    private Context mContext;
    CountDownTimer mCountDowntimer;
    Dialog movingDialog;
    private Handler mwMsgHandler;
    private ProgressBar prg_signalQuality;
    private ProgressBar prg_signalStrength;
    private int[] satIndexList;
    dvb_s_sat satInfo;
    private TextView tip_ok;
    dvb_s_tp tpInfo;
    private TextView txv_signalQuality;
    private TextView txv_signalStrength;

    /* renamed from: th.dtv.activity.MotorSettingActivity.13 */
    class AnonymousClass13 extends CountDownTimer {
        AnonymousClass13(long x0, long x1) {
            super(x0, x1);
        }

        public void onTick(long millisUntilFinished) {
            long lefttime = millisUntilFinished / 1000;
            Log.d("MotorSettingActivity", "seconds remaining :" + lefttime);
            if (MotorSettingActivity.this.limitTime > 100) {
                ((MotorMoveDialog) MotorSettingActivity.this.movingDialog).setLeftTimeText(lefttime);
            }
        }

        public void onFinish() {
            if (MotorSettingActivity.this.movingDialog != null) {
                MotorSettingActivity.this.movingDialog.dismiss();
                MotorSettingActivity.this.movingDialog = null;
                if (MotorSettingActivity.this.satInfo.motor_type == 1) {
                    MW.diseqc12_halt();
                }
            }
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.14 */
    class AnonymousClass14 extends CountDownTimer {
        AnonymousClass14(long x0, long x1) {
            super(x0, x1);
        }

        public void onTick(long arg0) {
        }

        public void onFinish() {
            if (MotorSettingActivity.this.movingDialog != null) {
                MotorSettingActivity.this.movingDialog.dismiss();
                MotorSettingActivity.this.movingDialog = null;
            }
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.1 */
    class C01821 implements OnItemClickListener {
        C01821() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (MotorSettingActivity.this.fSaveSatTpFlag) {
                MotorSettingActivity.this.fSaveSatTpFlag = false;
                MW.db_dvb_s_save_sat_tp(0);
            }
            MotorSettingActivity.this.currentSatPos = position;
            MotorSettingActivity.this.currentSatIndex = MotorSettingActivity.this.satIndexList[position];
            MotorSettingActivity.this.RefreshSatelliteInfo(true);
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.2 */
    class C01832 implements OnItemClickListener {
        C01832() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            MotorSettingActivity.this.currentTpIndex = position;
            System.out.println("currentTpIndex" + position);
            MotorSettingActivity.this.RefreshTransponderInfo();
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.3 */
    class C01843 implements OnItemClickListener {
        C01843() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (MotorSettingActivity.this.satInfo.motor_type != position) {
                MotorSettingActivity.this.satInfo.motor_type = position;
                MotorSettingActivity.this.fSaveSatTpFlag = true;
                MW.db_dvb_s_set_sat_info(0, MotorSettingActivity.this.satInfo);
                if (MotorSettingActivity.this.satInfo.motor_type == 0) {
                    MW.diseqc12_halt();
                }
                MotorSettingActivity.this.RefreshMotorTypeItems();
            }
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.4 */
    class C01854 implements OnKeyListener {
        C01854() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    int action = MotorSettingActivity.this.Combo_MoveStep.getHighlightIndex();
                    System.out.println("Moving dish ... action : " + action);
                    switch (action) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            MotorSettingActivity.this.showMotorMovingDialog(5);
                            MW.diseqc12_move_west();
                            break;
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            MotorSettingActivity.this.showMotorMovingDialog(5);
                            MW.diseqc12_move_east();
                            break;
                        default:
                            break;
                    }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.5 */
    class C01865 implements OnKeyListener {
        C01865() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    int action = MotorSettingActivity.this.Combo_ContinuingMove.getHighlightIndex();
                    System.out.println("Continuing Moving dish ... action : " + action);
                    switch (action) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            MotorSettingActivity.this.showMotorMovingDialog(900);
                            MW.diseqc12_move_west_continues();
                            break;
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            MotorSettingActivity.this.showMotorMovingDialog(900);
                            MW.diseqc12_move_east_continues();
                            break;
                        default:
                            break;
                    }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.6 */
    class C01876 implements OnKeyListener {
        C01876() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    int action = MotorSettingActivity.this.Combo_Limit.getHighlightIndex();
                    System.out.println("Limit ... action : " + action);
                    switch (action) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            MotorSettingActivity.this.showSavePositionDialog(5);
                            MW.diseqc12_disable_limit();
                            break;
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            MotorSettingActivity.this.showSavePositionDialog(5);
                            MW.diseqc12_set_west_limit();
                            break;
                        case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                            MotorSettingActivity.this.showSavePositionDialog(5);
                            MW.diseqc12_set_east_limit();
                            break;
                        default:
                            break;
                    }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.7 */
    class C01887 implements OnKeyListener {
        C01887() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    int action = MotorSettingActivity.this.Combo_PosCommand.getHighlightIndex();
                    System.out.println("PosCommand ... action : " + action);
                    if (MotorSettingActivity.this.satInfo.motor_type != 1) {
                        if (MotorSettingActivity.this.satInfo.motor_type == 2) {
                            switch (action) {
                                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                                    MotorSettingActivity.this.showMotorMovingDialog(450);
                                    MW.diseqc12_goto_reference();
                                    break;
                                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                                    MotorSettingActivity.this.showMotorMovingDialog(450);
                                    MW.diseqc13_goto_position(MotorSettingActivity.this.satInfo.sat_index);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    if (action == 0) {
                        MotorSettingActivity.this.gotoCommand = 0;
                    } else {
                        MotorSettingActivity.this.gotoCommand = -1;
                    }
                    switch (action) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            MotorSettingActivity.this.showMotorMovingDialog(450);
                            MW.diseqc12_goto_reference();
                            break;
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            if (MotorSettingActivity.this.satInfo.diseqc12_position != 0) {
                                MotorSettingActivity.this.showSavePositionDialog(5);
                                MW.diseqc12_recalculate(MotorSettingActivity.this.satInfo.diseqc12_position);
                                break;
                            }
                            SETTINGS.makeText(MotorSettingActivity.this, R.string.please_select_correct_position, 0);
                            break;
                        case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                            if (MotorSettingActivity.this.satInfo.diseqc12_position != 0) {
                                MotorSettingActivity.this.showSavePositionDialog(5);
                                MW.diseqc12_store_position(MotorSettingActivity.this.satInfo.diseqc12_position);
                                break;
                            }
                            SETTINGS.makeText(MotorSettingActivity.this, R.string.please_select_correct_position, 0);
                            break;
                        default:
                            break;
                    }
                    break;
            }
            return false;
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.8 */
    class C01898 implements OnItemClickListener {
        C01898() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            MotorSettingActivity.this.satInfo.diseqc12_position = position;
            MotorSettingActivity.this.fSaveSatTpFlag = true;
            MW.db_dvb_s_set_sat_info(0, MotorSettingActivity.this.satInfo);
        }
    }

    /* renamed from: th.dtv.activity.MotorSettingActivity.9 */
    class C01909 implements OnItemClickListener {
        C01909() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            SETTINGS.set_location_value(position, true);
        }
    }

    private class LongitudeEditDialog extends Dialog {
        private Button mBtnCancel;
        private RelativeLayout mBtnRelativeLayout;
        private Button mBtnStart;
        private LinearLayout mContextLinearLayout;
        private EditText mEditText0;
        private EditText mEditText1;
        private TextView mInfoTextView;
        private boolean mIsLongitude;
        private LinearLayout mTitleLinearLayout;
        private TextView mTitleTextView;

        protected void onCreate(Bundle savedInstanceState) {
            int i = R.string.longitude;
            super.onCreate(savedInstanceState);
            setContentView(R.layout.longitude_edit_dialog);
            getWindow().setSoftInputMode(3);
            this.mTitleLinearLayout = (LinearLayout) findViewById(R.id.dialog_title_layout);
            this.mTitleTextView = (TextView) findViewById(R.id.dialog_text_title);
            this.mTitleTextView.setText(this.mIsLongitude ? R.string.longitude : R.string.latitude);
            this.mInfoTextView = (TextView) findViewById(R.id.dialog_text_info);
            TextView textView = this.mInfoTextView;
            if (!this.mIsLongitude) {
                i = R.string.latitude;
            }
            textView.setText(i);
            this.mContextLinearLayout = (LinearLayout) findViewById(R.id.dialog_context_layout);
            this.mBtnRelativeLayout = (RelativeLayout) findViewById(R.id.dialog_button_layout);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mEditText0 = (EditText) findViewById(R.id.longitude_edittext0);
            this.mEditText0.setBackgroundResource(17301529);
            this.mEditText0.setGravity(3);
            this.mEditText0.requestFocus();
            this.mEditText1 = (EditText) findViewById(R.id.longitude_edittext1);
            this.mEditText1.setBackgroundResource(17301529);
            this.mEditText1.setGravity(3);
            String[] valueStr = this.mIsLongitude ? MotorSettingActivity.this.LongitudeValue2Str(SETTINGS.get_longitude_value()).split("\\.") : MotorSettingActivity.this.LatitudeValue2Str(SETTINGS.get_latitude_value()).split("\\.");
            this.mEditText0.setText(valueStr[0]);
            this.mEditText0.setSelection(0, valueStr[0].length());
            this.mEditText1.setText(valueStr[1]);
            this.mEditText1.setSelection(0, valueStr[1].length());
            LayoutParams linearParams = (LayoutParams) this.mContextLinearLayout.getLayoutParams();
            linearParams.height = (SETTINGS.getWindowHeight() / 720) * 120;
            this.mContextLinearLayout.setLayoutParams(linearParams);
        }

        public LongitudeEditDialog(Context context, int theme, boolean isLongitude) {
            super(context, theme);
            this.mIsLongitude = true;
            this.mTitleLinearLayout = null;
            this.mContextLinearLayout = null;
            this.mBtnRelativeLayout = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mEditText0 = null;
            this.mEditText1 = null;
            this.mTitleTextView = null;
            this.mInfoTextView = null;
            MotorSettingActivity.this.mContext = context;
            this.mIsLongitude = isLongitude;
        }

        private boolean setValue() {
            if (this.mEditText0.getText().toString().length() <= 0 || this.mEditText1.getText().toString().length() <= 0) {
                SETTINGS.makeText(MotorSettingActivity.this.mContext, R.string.seekTimeTitletips_invalid, 0);
                return false;
            }
            int second = Integer.valueOf(this.mEditText1.getText().toString()).intValue();
            if (second >= 60) {
                SETTINGS.makeText(MotorSettingActivity.this.mContext, R.string.seekTimeTitletips_invalid, 0);
                return false;
            }
            int value = (Integer.valueOf(this.mEditText0.getText().toString()).intValue() * 100) + second;
            if ((!this.mIsLongitude || value <= 18000) && (this.mIsLongitude || value <= 9000)) {
                if (this.mIsLongitude) {
                    SETTINGS.set_longitude_value(value, true);
                    MotorSettingActivity.this.Combo_Longitude.getInfoTextView().setText(MotorSettingActivity.this.LongitudeValue2Str(value));
                } else {
                    SETTINGS.set_latitude_value(value, true);
                    MotorSettingActivity.this.Combo_Latitude.getInfoTextView().setText(MotorSettingActivity.this.LatitudeValue2Str(value));
                }
                return true;
            }
            SETTINGS.makeText(MotorSettingActivity.this.mContext, R.string.seekTimeTitletips_invalid, 0);
            return false;
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                default:
            }
        }
    }

    public class MotorMoveDialog extends Dialog {
        TextView mContent;
        private Context mContext;
        int mEvent;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.motor_info_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mTitle = (TextView) findViewById(R.id.deldia_title);
            this.mTitle.setText(MotorSettingActivity.this.getResources().getString(R.string.message));
            this.mContent = (TextView) findViewById(R.id.delete_content);
            if (this.mEvent == 0) {
                this.mContent.setText(MotorSettingActivity.this.getResources().getString(R.string.moving) + MotorSettingActivity.this.getResources().getString(R.string.ellipsis));
            } else if (this.mEvent == 1) {
                this.mContent.setText(MotorSettingActivity.this.getResources().getString(R.string.done));
            }
        }

        public MotorMoveDialog(Context context, int theme, int event) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mTitle = null;
            this.mContent = null;
            this.mEvent = 0;
            this.mContext = context;
            this.mEvent = event;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                case MW.RET_INVALID_TYPE /*23*/:
                    MotorSettingActivity.this.hideMotorMovingDialog();
                    break;
            }
            return true;
        }

        public void setLeftTimeText(long lefttime) {
            this.mContent.setText(MotorSettingActivity.this.getResources().getString(R.string.moving) + MotorSettingActivity.this.getResources().getString(R.string.ellipsis) + "    " + lefttime + " " + MotorSettingActivity.this.getResources().getString(R.string.second));
        }
    }

    public class ScanActionDig extends Dialog {
        private LinearLayout channel_type_ll;
        private TextView channel_type_opt;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private LinearLayout nit_search_ll;
        private TextView nit_search_opt;
        private LinearLayout scan_type_ll;
        private TextView scan_type_opt;
        private LinearLayout service_type_ll;
        private TextView servicetype_opt;

        private void RefreshBg() {
            if (this.channel_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.servicetype_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.scan_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.nit_search_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
            }
            RefreshNitSearchBg(this.nit_search_opt.isFocusable());
        }

        private void RefreshNitSearchBg(boolean enabled) {
            if (enabled) {
                this.nit_search_opt.setEnabled(true);
                this.nit_search_opt.setFocusable(true);
                if (this.nit_search_opt.isFocused()) {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                    return;
                } else {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                    return;
                }
            }
            this.nit_search_opt.setEnabled(false);
            this.nit_search_opt.setFocusable(false);
            this.nit_search_ll.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.satellite_scan_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.satellite_scan_dialog, null);
            this.channel_type_ll = (LinearLayout) findViewById(R.id.channel_type_ll);
            this.service_type_ll = (LinearLayout) findViewById(R.id.service_type_ll);
            this.scan_type_ll = (LinearLayout) findViewById(R.id.scan_type_ll);
            this.nit_search_ll = (LinearLayout) findViewById(R.id.nit_search_ll);
            this.channel_type_opt = (TextView) findViewById(R.id.channel_type_opt);
            this.servicetype_opt = (TextView) findViewById(R.id.servicetype_opt);
            this.scan_type_opt = (TextView) findViewById(R.id.scan_type_opt);
            this.nit_search_opt = (TextView) findViewById(R.id.nit_search_opt);
            if (SETTINGS.get_search_nit()) {
                this.nit_search_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.on));
            } else {
                this.nit_search_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.off));
            }
            if (SETTINGS.get_search_service_type() == 0) {
                this.channel_type_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.All));
            } else {
                this.channel_type_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.FTAOnly));
            }
            if (SETTINGS.get_search_tv_type() == 0) {
                this.servicetype_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.All));
            } else if (SETTINGS.get_search_tv_type() == 1) {
                this.servicetype_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.TV));
            } else if (SETTINGS.get_search_tv_type() == 2) {
                this.servicetype_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.Radio));
            }
            if (MW.db_dvb_s_get_tp_count(0, MotorSettingActivity.this.currentSatIndex) == 0) {
                this.scan_type_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.blind_scan));
                RefreshNitSearchBg(false);
            } else {
                this.scan_type_opt.setText(MotorSettingActivity.this.getResources().getString(R.string.manual_scan));
            }
            this.scan_type_opt.requestFocus();
            RefreshBg();
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            int i2 = 1;
            if (keyEvent.getAction() != 0) {
                switch (i) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (i) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.nit_search_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.scan_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.scan_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.scan_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.channel_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.nit_search_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TYPE /*23*/:
                    boolean z;
                    int i3;
                    int i4;
                    if (this.nit_search_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.on))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (this.channel_type_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.All))) {
                        i3 = 0;
                    } else {
                        i3 = 1;
                    }
                    if (this.servicetype_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.All))) {
                        i4 = 0;
                    } else if (this.servicetype_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.TV))) {
                        i4 = 1;
                    } else if (this.servicetype_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.Radio))) {
                        i4 = 2;
                    } else {
                        i4 = 0;
                    }
                    if (!this.scan_type_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.auto_scan))) {
                        if (this.scan_type_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.blind_scan))) {
                            i2 = 0;
                        } else if (this.scan_type_opt.getText().toString().equals(MotorSettingActivity.this.getResources().getString(R.string.manual_scan))) {
                            i2 = 2;
                        } else {
                            i2 = 0;
                        }
                    }
                    SETTINGS.set_search_service_type(i3);
                    SETTINGS.set_search_tv_type(i4);
                    SETTINGS.set_search_nit(z);
                    Intent intent;
                    if (MW.db_dvb_s_get_tp_count(0, MotorSettingActivity.this.currentSatIndex) > 0 || i2 == 0) {
                        intent = new Intent();
                        intent.setFlags(67108864);
                        intent.setClass(MotorSettingActivity.this, ChannelSearchActivity.class);
                        intent.putExtra("system_type", 0);
                        intent.putExtra("search_type", i2);
                        MotorSettingActivity.this.getSelSatData();
                        intent.putIntegerArrayListExtra("search_sat_index", MotorSettingActivity.this.SelSatList);
                        MotorSettingActivity.this.getSelTpData();
                        intent.putIntegerArrayListExtra("search_tp_index", MotorSettingActivity.this.SelTpList);
                        MotorSettingActivity.this.startActivity(intent);
                    } else {
                        SETTINGS.makeText(MotorSettingActivity.this, R.string.add_tp, 0);
                        intent = new Intent();
                        intent.setFlags(67108864);
                        intent.putExtra("selected_sat_index", MotorSettingActivity.this.currentSatIndex);
                        intent.putExtra("selected_tp_index", 0);
                        intent.setClass(MotorSettingActivity.this, TpListActivity.class);
                        MotorSettingActivity.this.startActivity(intent);
                        dismiss();
                    }
                    dismiss();
                    break;
            }
            return super.onKeyDown(i, keyEvent);
        }

        public ScanActionDig(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public MotorSettingActivity() {
        this.mContext = null;
        this.satInfo = new dvb_s_sat();
        this.tpInfo = new dvb_s_tp();
        this.currentSatIndex = -1;
        this.currentTpIndex = -1;
        this.currentSatPos = 0;
        this.fSaveSatTpFlag = false;
        this.gotoCommand = -1;
        this.SelSatList = new ArrayList();
        this.SelTpList = new ArrayList();
        this.movingDialog = null;
        this.mCountDowntimer = null;
        this.limitTime = 600;
    }

    private void initTPInfoView() {
        this.prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
        this.prg_signalStrength.setMax(100);
        this.prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
        this.prg_signalQuality.setMax(100);
        this.prg_signalStrength.setProgress(0);
        this.prg_signalQuality.setProgress(0);
        this.txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
        this.txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);
        this.txv_signalStrength.setText("0%");
        this.txv_signalQuality.setText("0%");
    }

    private void LockCurrentTP() {
        MW.tuner_dvb_s_lock_tp(0, this.currentSatIndex, this.currentTpIndex, false);
    }

    private void initComboView() {
        this.Combo_Satellite = (ComboLayout) findViewById(R.id.Combo_Satellite);
        this.Combo_Transponder = (ComboLayout) findViewById(R.id.Combo_Transponder);
        this.Combo_MotorType = (ComboLayout) findViewById(R.id.Combo_MotorType);
        this.Combo_PosCommand = (ComboLayout) findViewById(R.id.Combo_PosCommand);
        this.Combo_MoveStep = (ComboLayout) findViewById(R.id.Combo_MoveStep);
        this.Combo_ContinuingMove = (ComboLayout) findViewById(R.id.Combo_ContinuingMove);
        this.Combo_Limit = (ComboLayout) findViewById(R.id.Combo_Limit);
        this.Combo_GOTO = (ComboLayout) findViewById(R.id.Combo_GOTO);
        this.Combo_location = (ComboLayout) findViewById(R.id.Combo_location);
        this.Combo_Longitude = (ComboLayout) findViewById(R.id.Combo_Longitude);
        this.Combo_hemisphere = (ComboLayout) findViewById(R.id.Combo_hemisphere);
        this.Combo_Latitude = (ComboLayout) findViewById(R.id.Combo_Latitude);
        this.tip_ok = (TextView) findViewById(R.id.tip_ok);
        this.help_tips = (LinearLayout) findViewById(R.id.help_tips);
    }

    private void initData() {
        if (MW.db_dvb_s_get_selected_sat_count(0) == 0) {
            SETTINGS.makeText(this, R.string.no_selected_satellite, 0);
            finishMyself();
        } else if (MW.db_dvb_s_get_current_tp_info(0, this.tpInfo) == 0) {
            System.out.println(" tpInfo.sat_index  : " + this.tpInfo.sat_index);
            System.out.println(" tpInfo.tp_index  : " + this.tpInfo.tp_index);
            this.currentSatIndex = this.tpInfo.sat_index;
            this.currentSatPos = this.tpInfo.sat_pos;
            if (MW.db_dvb_s_get_tp_count(0, this.currentSatIndex) == 0) {
                this.currentTpIndex = -1;
                SETTINGS.makeText(this, R.string.add_tp, 0);
                finishMyself();
                return;
            }
            this.currentTpIndex = this.tpInfo.tp_index;
        } else if (MW.db_dvb_s_get_current_sat_info(0, this.satInfo) == 0) {
            this.currentSatIndex = this.satInfo.sat_index;
            this.currentTpIndex = 0;
        } else {
            this.currentSatIndex = 0;
            this.currentTpIndex = 0;
        }
    }

    private void initView() {
        initComboView();
        initTPInfoView();
    }

    private void RefreshAllData() {
        if (MW.db_dvb_s_get_selected_sat_count(0) == 0) {
            SETTINGS.makeText(this, R.string.no_selected_satellite, 0);
            finishMyself();
            return;
        }
        FillSatelliteItems();
        RefreshSatelliteInfo(false);
        this.Combo_Satellite.requestFocus();
        refreshTips();
    }

    private void FillSatelliteItems() {
        int satCount = MW.db_dvb_s_get_selected_sat_count(0);
        ArrayList items = new ArrayList();
        items.clear();
        this.satIndexList = new int[satCount];
        for (int i = 0; i < satCount; i++) {
            if (MW.db_dvb_s_get_selected_sat_info(0, i, this.satInfo) == 0) {
                String str = "%03d   %03d.%d    %s    %s";
                Object[] objArr = new Object[5];
                objArr[0] = Integer.valueOf(i + 1);
                objArr[1] = Integer.valueOf(this.satInfo.sat_degree_dec);
                objArr[2] = Integer.valueOf(this.satInfo.sat_degree_point);
                objArr[3] = this.satInfo.sat_position == 0 ? "E" : "W";
                objArr[4] = this.satInfo.sat_name;
                items.add(String.format(str, objArr));
                this.satIndexList[i] = this.satInfo.sat_index;
            }
        }
        this.Combo_Satellite.initView((int) R.string.Satellite, items, this.currentSatPos, new C01821());
    }

    private void RefreshSatelliteInfo(boolean bResetTpIndex) {
        if (MW.db_dvb_s_get_sat_info(0, this.currentSatIndex, this.satInfo) == 0) {
            if (bResetTpIndex) {
                this.currentTpIndex = 0;
            }
            FillTransponderItems();
            RefreshTransponderInfo();
            FillMotorTypeItems();
            RefreshMotorTypeItems();
            FillPosCommandItems();
            System.out.println("Current Motor type : " + this.satInfo.motor_type);
            if (this.satInfo.motor_type == 1) {
                if (this.satInfo.diseqc12_position != 0) {
                    showMotorMovingDialog(450);
                    MW.diseqc12_goto_position(this.satInfo.diseqc12_position);
                }
            } else if (this.satInfo.motor_type == 2) {
                showMotorMovingDialog(450);
                MW.diseqc13_goto_position(this.satInfo.sat_index);
            }
        }
    }

    private void FillTransponderItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            for (int i = 0; i < this.satInfo.tp_count; i++) {
                if (MW.db_dvb_s_get_tp_info(0, this.currentSatIndex, i, this.tpInfo) == 0) {
                    String str = "%d / %d    %05d   %s   %05d";
                    Object[] objArr = new Object[5];
                    objArr[0] = Integer.valueOf(i + 1);
                    objArr[1] = Integer.valueOf(this.satInfo.tp_count);
                    objArr[2] = Integer.valueOf(this.tpInfo.frq);
                    objArr[3] = this.tpInfo.pol == 0 ? "V" : "H";
                    objArr[4] = Integer.valueOf(this.tpInfo.sym);
                    items.add(String.format(str, objArr));
                }
            }
            if (this.satInfo.tp_count == 0) {
                Log.e("LEE", " ======0");
                this.Combo_Transponder.setEnabled(false);
                this.Combo_Transponder.setFocusable(false);
                this.Combo_Transponder.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                MW.tuner_unlock_tp(0, 0, false);
            } else {
                Log.e("LEE", " !==0");
                this.Combo_Transponder.setEnabled(true);
                this.Combo_Transponder.setFocusable(true);
                this.Combo_Transponder.setBackgroundResource(R.drawable.setlect_white_focus);
                LockCurrentTP();
            }
            if (this.currentTpIndex == -1) {
                this.currentTpIndex = 0;
            }
            this.Combo_Transponder.initView((int) R.string.Transponder, items, this.currentTpIndex, new C01832());
        }
    }

    private void RefreshTransponderInfo() {
        if (MW.db_dvb_s_get_tp_info(0, this.currentSatIndex, this.currentTpIndex, this.tpInfo) == 0) {
            LockCurrentTP();
        }
    }

    private void FillMotorTypeItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            items.add(getResources().getString(R.string.disable));
            items.add("DiSEqc 1.2");
            items.add("USALS");
            this.Combo_MotorType.initView((int) R.string.motor_type, items, this.satInfo.motor_type, new C01843());
        }
    }

    private void RefreshMotorTypeItems() {
        FillMotorStepItems();
        FillMotorContinuingItems();
        FillMotorGoto();
        FillMotorLocationItems();
        FillMotorLongitudeItems();
        FillMotorHemisphereItems();
        FillMotorLatitudeItems();
        FillLimitItems();
        FillPosCommandItems();
        if (this.satInfo.motor_type == 1) {
            this.Combo_MoveStep.setVisibility(View.VISIBLE);
            this.Combo_ContinuingMove.setVisibility(View.VISIBLE);
            this.Combo_Limit.setVisibility(View.VISIBLE);
            this.Combo_PosCommand.setVisibility(View.VISIBLE);
            this.Combo_GOTO.setVisibility(View.VISIBLE);
            this.Combo_Longitude.setVisibility(View.GONE);
            this.Combo_Latitude.setVisibility(View.GONE);
            this.Combo_location.setVisibility(View.GONE);
            this.Combo_hemisphere.setVisibility(View.GONE);
            return;
        }
        if (this.satInfo.motor_type == 2) {
            this.Combo_Longitude.setVisibility(View.VISIBLE);
            this.Combo_Latitude.setVisibility(View.VISIBLE);
            this.Combo_location.setVisibility(View.VISIBLE);
            this.Combo_hemisphere.setVisibility(View.VISIBLE);
            this.Combo_PosCommand.setVisibility(View.VISIBLE);
        } else {
            this.Combo_Longitude.setVisibility(View.GONE);
            this.Combo_Latitude.setVisibility(View.GONE);
            this.Combo_location.setVisibility(View.GONE);
            this.Combo_hemisphere.setVisibility(View.GONE);
            this.Combo_PosCommand.setVisibility(View.GONE);
        }
        this.Combo_MoveStep.setVisibility(View.GONE);
        this.Combo_ContinuingMove.setVisibility(View.GONE);
        this.Combo_Limit.setVisibility(View.GONE);
        this.Combo_GOTO.setVisibility(View.GONE);
    }

    private void FillMotorStepItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            items.add(getResources().getString(R.string.West));
            items.add(getResources().getString(R.string.East));
            this.Combo_MoveStep.initView((int) R.string.motor_step, items, 0, new C01854());
        }
    }

    private void FillMotorContinuingItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            items.add(getResources().getString(R.string.West));
            items.add(getResources().getString(R.string.East));
            this.Combo_ContinuingMove.initView((int) R.string.motor_continuing, items, 0, new C01865());
        }
    }

    private void FillLimitItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            items.add(getResources().getString(R.string.disable));
            items.add(getResources().getString(R.string.west_limit));
            items.add(getResources().getString(R.string.east_limit));
            this.Combo_Limit.initView((int) R.string.set_limit, items, 0, new C01876());
        }
    }

    private void FillPosCommandItems() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            if (this.satInfo.motor_type == 1) {
                items.add(getResources().getString(R.string.goto_refrence));
                items.add(getResources().getString(R.string.Recalculate));
                items.add(getResources().getString(R.string.Store_position));
            } else {
                items.add(getResources().getString(R.string.goto_refrence));
                items.add(getResources().getString(R.string.goto_position));
            }
            this.Combo_PosCommand.initView((int) R.string.GotoCommand, items, 0, new C01887());
        }
    }

    private void FillMotorGoto() {
        if (this.satInfo != null) {
            ArrayList items = new ArrayList();
            items.clear();
            items.add("0");
            for (int i = 1; i <= 64; i++) {
                items.add(i + "");
            }
            this.Combo_GOTO.initView((int) R.string.Position, items, this.satInfo.diseqc12_position, new C01898());
        }
    }

    private void FillMotorLocationItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getResources().getString(R.string.East));
        items.add(getResources().getString(R.string.West));
        this.Combo_location.initView((int) R.string.location, items, SETTINGS.get_location_value(), new C01909());
    }

    private void FillMotorLongitudeItems() {
        this.Combo_Longitude.initView((int) R.string.longitude, 0, 0, new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                new LongitudeEditDialog(MotorSettingActivity.this, R.style.MyDialog, true).show();
            }
        });
        this.Combo_Longitude.getLeftImageView().setVisibility(View.INVISIBLE);
        this.Combo_Longitude.getRightImageView().setVisibility(View.INVISIBLE);
        this.Combo_Longitude.getInfoTextView().setText(LongitudeValue2Str(SETTINGS.get_longitude_value()));
    }

    private void FillMotorHemisphereItems() {
        ArrayList items = new ArrayList();
        items.clear();
        items.add(getResources().getString(R.string.South));
        items.add(getResources().getString(R.string.North));
        this.Combo_hemisphere.initView((int) R.string.hemisphere, items, SETTINGS.get_hemisphere_value(), new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                SETTINGS.set_hemisphere_value(position, true);
            }
        });
    }

    private void FillMotorLatitudeItems() {
        this.Combo_Latitude.initView((int) R.string.latitude, 0, 0, new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                new LongitudeEditDialog(MotorSettingActivity.this, R.style.MyDialog, false).show();
            }
        });
        this.Combo_Latitude.getLeftImageView().setVisibility(View.INVISIBLE);
        this.Combo_Latitude.getRightImageView().setVisibility(View.INVISIBLE);
        this.Combo_Latitude.getInfoTextView().setText(LatitudeValue2Str(SETTINGS.get_latitude_value()));
    }

    private String LongitudeValue2Str(int value) {
        String str = "0.00";
        if (value <= 0 || value > 18000) {
            return str;
        }
        return "" + (value / 100) + "." + (value % 100 < 10 ? "0" + (value % 100) : Integer.valueOf(value % 100));
    }

    private String LatitudeValue2Str(int value) {
        String str = "0.00";
        if (value <= 0 || value > 9000) {
            return str;
        }
        return "" + (value / 100) + "." + (value % 100 < 10 ? "0" + (value % 100) : Integer.valueOf(value % 100));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.motorsetting_activity);
        this.mContext = this;
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        initData();
        initView();
    }

    protected void onResume() {
        super.onResume();
        MW.diseqc_motor_enable_auto_moving(false);
        SETTINGS.send_led_msg("NENU");
        RefreshAllData();
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(2, true);
        if (this.satInfo == null) {
            MW.tuner_unlock_tp(0, 0, false);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalStrength.setProgress(0);
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        } else if (this.satInfo.tp_count > 0) {
            LockCurrentTP();
        } else {
            MW.tuner_unlock_tp(0, 0, false);
            this.prg_signalQuality.setProgress(0);
            this.prg_signalStrength.setProgress(0);
            this.txv_signalStrength.setText("0%");
            this.txv_signalQuality.setText("0%");
        }
    }

    protected void onPause() {
        super.onPause();
        if (MW.db_dvb_s_get_current_tp_info(0, this.tpInfo) != 0) {
            MW.db_dvb_s_set_current_sat_tp(0, 0, 0);
        } else if (!(this.tpInfo.sat_index == this.currentSatIndex && this.tpInfo.tp_index == this.currentTpIndex)) {
            MW.db_dvb_s_set_current_sat_tp(0, this.currentSatIndex, this.currentTpIndex);
        }
        MW.diseqc_motor_enable_auto_moving(true);
        if (this.fSaveSatTpFlag) {
            this.fSaveSatTpFlag = false;
            MW.db_dvb_s_save_sat_tp(0);
        }
        hideMotorMovingDialog();
        enableMwMessageCallback(null);
        MW.tuner_unlock_tp(0, 0, false);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                if (this.Combo_Satellite.isFocused()) {
                    if (this.Combo_PosCommand.getVisibility() == 0) {
                        this.Combo_PosCommand.requestFocus();
                    } else {
                        this.Combo_MotorType.requestFocus();
                    }
                    refreshTips();
                    return true;
                }
                if (this.Combo_PosCommand.isFocused()) {
                    if (this.Combo_GOTO.getVisibility() == 0) {
                        this.Combo_GOTO.requestFocus();
                        refreshTips();
                        return true;
                    } else if (this.Combo_Latitude.getVisibility() == 0) {
                        this.Combo_Latitude.requestFocus();
                        refreshTips();
                        return true;
                    }
                }
                if (this.Combo_GOTO.isFocused()) {
                    this.Combo_Limit.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_MoveStep.isFocused()) {
                    this.Combo_MotorType.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_Latitude.isFocused()) {
                    this.Combo_hemisphere.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_hemisphere.isFocused()) {
                    this.Combo_Longitude.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_Longitude.isFocused()) {
                    this.Combo_location.requestFocus();
                    refreshTips();
                    return true;
                }
                break;
            case MW.RET_INVALID_TP_INDEX /*20*/:
                if (this.Combo_PosCommand.isFocused()) {
                    this.Combo_Satellite.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_PosCommand.getVisibility() != 0 && this.Combo_MotorType.isFocused()) {
                    this.Combo_Satellite.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_MotorType.isFocused()) {
                    if (this.Combo_MoveStep.getVisibility() == 0) {
                        this.Combo_MoveStep.requestFocus();
                    } else if (this.Combo_location.getVisibility() == 0) {
                        this.Combo_location.requestFocus();
                    }
                    refreshTips();
                    return true;
                } else if (this.Combo_Limit.isFocused() && this.Combo_GOTO.getVisibility() == 0) {
                    this.Combo_GOTO.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_GOTO.isFocused()) {
                    this.Combo_PosCommand.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_location.isFocused()) {
                    this.Combo_Longitude.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_Longitude.isFocused()) {
                    this.Combo_hemisphere.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_hemisphere.isFocused()) {
                    this.Combo_Latitude.requestFocus();
                    refreshTips();
                    return true;
                } else if (this.Combo_Latitude.isFocused()) {
                    this.Combo_PosCommand.requestFocus();
                    refreshTips();
                    return true;
                }
                break;
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                ScanAction();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void getSelSatData() {
        this.SelSatList.clear();
        this.SelSatList.add(Integer.valueOf(this.currentSatIndex));
    }

    private void getSelTpData() {
        this.SelTpList.clear();
        this.SelTpList.add(Integer.valueOf(this.currentTpIndex));
    }

    private void ScanAction() {
        Dialog scanActionDig = new ScanActionDig(this, R.style.MyDialog);
        scanActionDig.setContentView(R.layout.satellite_scan_dialog);
        scanActionDig.show();
        scanActionDig.getWindow().addFlags(2);
    }

    private void refreshTips() {
        if (this.Combo_Satellite.isFocused() || this.Combo_Transponder.isFocused() || this.Combo_MotorType.isFocused() || this.Combo_location.isFocused() || this.Combo_hemisphere.isFocused() || this.Combo_GOTO.isFocused()) {
            this.help_tips.setVisibility(View.VISIBLE);
            this.tip_ok.setText(getResources().getString(R.string.SelectSat));
        } else if (this.Combo_Longitude.isFocused() || this.Combo_Latitude.isFocused()) {
            this.help_tips.setVisibility(View.VISIBLE);
            this.tip_ok.setText(getResources().getString(R.string.edit));
        } else {
            this.help_tips.setVisibility(View.VISIBLE);
            this.tip_ok.setText(getResources().getString(R.string.start));
        }
    }

    private void showMotorMovingDialog(long j) {
        this.limitTime = j;
        if (this.movingDialog == null) {
            this.movingDialog = new MotorMoveDialog(this, R.style.MyDialog, 0);
            this.mCountDowntimer = new AnonymousClass13(j * 100, 100);
            this.mCountDowntimer.start();
            this.movingDialog.show();
            this.movingDialog.getWindow().addFlags(2);
            return;
        }
        System.out.println("moving dialog is not null");
    }

    private void showSavePositionDialog(long showtime_s) {
        if (this.movingDialog == null) {
            this.movingDialog = new MotorMoveDialog(this, R.style.MyDialog, 1);
            this.mCountDowntimer = new AnonymousClass14(showtime_s * 100, 100);
            this.mCountDowntimer.start();
        }
        this.movingDialog.show();
        this.movingDialog.getWindow().addFlags(2);
    }

    private void hideMotorMovingDialog() {
        if (this.mCountDowntimer != null) {
            this.mCountDowntimer.cancel();
        }
        if (this.movingDialog != null) {
            MW.diseqc12_halt();
            this.movingDialog.dismiss();
            this.movingDialog = null;
        }
    }
}
