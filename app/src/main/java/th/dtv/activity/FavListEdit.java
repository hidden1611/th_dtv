package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.service;
import th.dtv.util.HongKongDTMBHD;

public class FavListEdit extends DtvBaseActivity {
    private TextView channel_type;
    private int current_service_index;
    private int current_service_type;
    private FavChannelAdapter favChannelAdapter;
    private CustomListView favChannelList;
    private int favChannel_list_item_pos;
    private CustomListView favGroupList;
    RelativeLayout favedit_rl;
    private FavgroupAdapter favgroupAdapter;
    private int favgroup_list_item_pos;
    String[] favgroupname;
    LinearLayout favlist_help_tips;
    LinearLayout favlist_help_tips_two;
    private boolean fchangegroup;
    private int mFavChannelFirstVisibleItem;
    private int mFavChannelVisibleItemCount;
    private int mFavgroupFirstVisibleItem;
    private int mFavgroupVisibleItemCount;
    Callback mSHCallback;
    private Handler mwMsgHandler;
    private Dialog pswdDig;
    private service serviceInfo;
    private service serviceInfo_temp;

    /* renamed from: th.dtv.activity.FavListEdit.1 */
    class C01571 implements Callback {
        C01571() {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("FavListEdit", "surfaceChanged");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("FavListEdit", "surfaceCreated");
            try {
                initSurface(holder);
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("FavListEdit", "surfaceDestroyed");
        }

        private void initSurface(SurfaceHolder h) {
            Canvas c = null;
            try {
                Log.d("FavListEdit", "initSurface");
                c = h.lockCanvas();
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            } catch (Throwable th) {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /* renamed from: th.dtv.activity.FavListEdit.2 */
    class C01582 implements OnScrollListener {
        C01582() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            FavListEdit.this.mFavgroupFirstVisibleItem = firstVisibleItem;
            FavListEdit.this.mFavgroupVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.FavListEdit.3 */
    class C01593 implements OnItemSelectedListener {
        C01593() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            FavListEdit.this.favgroup_list_item_pos = pos;
            FavListEdit.this.RefreshFavChannel(pos);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.FavListEdit.4 */
    class C01604 implements OnScrollListener {
        C01604() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            FavListEdit.this.mFavChannelFirstVisibleItem = firstVisibleItem;
            FavListEdit.this.mFavChannelVisibleItemCount = visibleItemCount;
        }
    }

    /* renamed from: th.dtv.activity.FavListEdit.5 */
    class C01615 implements OnItemSelectedListener {
        C01615() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            FavListEdit.this.favChannel_list_item_pos = pos;
            System.out.println("favChannel_list_item_pos" + FavListEdit.this.favChannel_list_item_pos);
            if (MW.db_get_service_info(FavListEdit.this.current_service_type, pos, FavListEdit.this.serviceInfo) == 0) {
                SETTINGS.set_led_channel(FavListEdit.this.serviceInfo.channel_number_display);
                int Ret = MW.ts_player_play(FavListEdit.this.current_service_type, pos, 50, false);
                FavListEdit.this.current_service_index = FavListEdit.this.serviceInfo.service_index;
                System.out.println("Fav Ret  = " + Ret);
                if (Ret == 44) {
                    MW.ts_player_play_current(true, false);
                }
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public class DeleteDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        TextView mContent;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.deldia_title);
            this.mTitle.setText(R.string.remove);
            this.mContent = (TextView) findViewById(R.id.delete_content);
            this.mContent.setText(R.string.dywtrtfc);
            this.mBtnCancel.requestFocus();
        }

        public DeleteDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContent = null;
            this.mTitle = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public class EditDialog extends Dialog {
        TextView item_no;
        Button mBtnCancel;
        Button mBtnStart;
        TextView mChannelName;
        private Context mContext;
        EditText mFileName;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.pvr_edit_custom_dia);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.title);
            this.item_no = (TextView) findViewById(R.id.item_no);
            this.mFileName = (EditText) findViewById(R.id.edittext_pvr_name);
            this.mChannelName = (TextView) findViewById(R.id.filename);
            this.mTitle.setText(FavListEdit.this.getResources().getString(R.string.rename));
            this.mChannelName.setText(R.string.groupname);
            if (FavListEdit.this.favgroupname != null) {
                this.item_no.setText((FavListEdit.this.favgroup_list_item_pos + 1) + "");
                this.mFileName.setText(FavListEdit.this.favgroupname[FavListEdit.this.favgroup_list_item_pos]);
                this.mFileName.setSelection(this.mFileName.length());
            }
            this.mFileName.requestFocus();
            this.mFileName.selectAll();
        }

        public EditDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.item_no = null;
            this.mFileName = null;
            this.mChannelName = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    private class FavChannelAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            ImageView fav_selected;
            TextView favch_chname;
            ImageView favch_hdsd_img;
            TextView favch_no;
            ImageView favch_scramble_img;

            ViewHolder() {
            }
        }

        public FavChannelAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            System.out.println("current group :" + MW.db_get_service_total_fav_group_current_index());
            return MW.db_get_service_count(FavListEdit.this.current_service_type);
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.favchannel_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.fav_selected = (ImageView) convertView.findViewById(R.id.fav_selected);
                localViewHolder.favch_no = (TextView) convertView.findViewById(R.id.favch_no);
                localViewHolder.favch_hdsd_img = (ImageView) convertView.findViewById(R.id.favch_hdsd_img);
                localViewHolder.favch_scramble_img = (ImageView) convertView.findViewById(R.id.favch_scramble_img);
                localViewHolder.favch_chname = (TextView) convertView.findViewById(R.id.favch_chname);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (MW.db_get_service_info(FavListEdit.this.current_service_type, pos, FavListEdit.this.serviceInfo) == 0) {
                localViewHolder.favch_no.setText("" + (pos + 1));
                localViewHolder.favch_chname.setText(FavListEdit.this.serviceInfo.service_name);
                System.out.println("fav channel :" + FavListEdit.this.serviceInfo.service_name);
                if (FavListEdit.this.serviceInfo.is_scrambled) {
                    localViewHolder.favch_scramble_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.favch_scramble_img.setVisibility(View.INVISIBLE);
                }
                if (SETTINGS.bHongKongDTMB) {
                    if (HongKongDTMBHD.isHongKongDTMBHDChannel(FavListEdit.this.serviceInfo.video_pid, FavListEdit.this.serviceInfo.audio_info[0].audio_pid) || FavListEdit.this.serviceInfo.is_hd) {
                        localViewHolder.favch_hdsd_img.setVisibility(View.VISIBLE);
                    } else {
                        localViewHolder.favch_hdsd_img.setVisibility(View.INVISIBLE);
                    }
                } else if (FavListEdit.this.serviceInfo.is_hd) {
                    localViewHolder.favch_hdsd_img.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.favch_hdsd_img.setVisibility(View.INVISIBLE);
                }
                if (MW.channel_edit_check_selection(pos)) {
                    localViewHolder.fav_selected.setVisibility(View.VISIBLE);
                } else {
                    localViewHolder.fav_selected.setVisibility(View.INVISIBLE);
                }
            }
            if (pos == FavListEdit.this.current_service_index && FavListEdit.this.favGroupList.isFocused()) {
                convertView.setBackgroundResource(R.drawable.list_446_49_sele_expired);
            } else {
                convertView.setBackgroundResource(R.drawable.listview_item_bg_selector);
            }
            return convertView;
        }
    }

    private class FavgroupAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView favgroup_name;
            TextView favgroup_no;

            ViewHolder() {
            }
        }

        public FavgroupAdapter(Context context) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return FavListEdit.this.favgroupname.length;
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.favgroup_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.favgroup_name = (TextView) convertView.findViewById(R.id.favgroup_name);
                localViewHolder.favgroup_no = (TextView) convertView.findViewById(R.id.favgroup_no);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            localViewHolder.favgroup_no.setText("" + (pos + 1));
            localViewHolder.favgroup_name.setText(FavListEdit.this.favgroupname[pos]);
            return convertView;
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int sub_event_type = msg.what & 65535;
            switch ((msg.what >> 16) & 65535) {
                case MW.RET_INVALID_SAT /*11*/:
                    int bCurrentServiceChanged = msg.arg1;
                    switch (sub_event_type) {
                        case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                            Log.e("FavListEdit", "service name update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            FavListEdit.this.RefreshFavChannel(FavListEdit.this.favgroup_list_item_pos);
                        case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                            Log.e("FavListEdit", "service params update : " + (bCurrentServiceChanged == 1 ? "current service changed" : "current service not changed"));
                            if (bCurrentServiceChanged == 1) {
                                MW.ts_player_play_current(true, false);
                            }
                        default:
                    }
                default:
            }
        }
    }

    private class channellistOnKeyListener implements OnKeyListener {
        private channellistOnKeyListener() {
        }

        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    case MW.RET_MEMORY_ERROR /*22*/:
                        FavListEdit.this.favGroupList.requestFocus();
//                        FavListEdit.this.favGroupList.setSelectionFromTop(FavListEdit.this.favgroup_list_item_pos, FavListEdit.this.mFavgroupFirstVisibleItem);
                        FavListEdit.this.favgroupAdapter.notifyDataSetChanged();
                        FavListEdit.this.favGroupList.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        if (FavListEdit.this.favChannelList.getSelectedItemPosition() >= 0) {
                            FavListEdit.this.favChannelList.getSelectedView().setBackgroundResource(R.drawable.list_446_49_sele_expired);
                        }
                        FavListEdit.this.RefreshHelpTips();
                        return true;
                    case MW.RET_INVALID_TYPE /*23*/:
                        FavListEdit.this.DoSelectionAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                        FavListEdit.this.CancelFavStatus();
                        return true;
                }
            }
            return false;
        }
    }

    private class grouplistOnKeyListener implements OnKeyListener {
        private grouplistOnKeyListener() {
        }

        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    case MW.RET_INVALID_TP_INDEX /*20*/:
                    case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                    case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                        if (FavListEdit.this.favGroupList != null && FavListEdit.this.favGroupList.getCount() > 1) {
                            if (FavListEdit.this.favChannelList.getSelectedView() != null) {
                                FavListEdit.this.favChannelList.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                            }
                            FavListEdit.this.favChannel_list_item_pos = 0;
                            FavListEdit.this.favChannelList.setCustomSelection(FavListEdit.this.favChannel_list_item_pos);
                        }
                        int currentPos = FavListEdit.this.favGroupList.getSelectedItemPosition();
                        FavListEdit.this.favGroupList.getSelectedView().setBackgroundResource(R.drawable.live_channel_list_item_bg);
                        FavListEdit.this.fchangegroup = true;
                        break;
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    case MW.RET_MEMORY_ERROR /*22*/:
                    case MW.RET_INVALID_TYPE /*23*/:
                        if (FavListEdit.this.favChannelList.getCount() <= 0) {
                            return true;
                        }
                        FavListEdit.this.favChannelList.requestFocus();
//                        FavListEdit.this.favChannelList.setSelectionFromTop(FavListEdit.this.favChannel_list_item_pos, FavListEdit.this.mFavChannelFirstVisibleItem);
                        FavListEdit.this.favChannelAdapter.notifyDataSetChanged();
                        FavListEdit.this.favChannelList.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                        if (FavListEdit.this.favChannelList.getSelectedItemPosition() >= 0) {
                            FavListEdit.this.favGroupList.getSelectedView().setBackgroundResource(R.drawable.epg_channel_list_item_selected);
                            FavListEdit.this.favChannelList.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
                        }
                        FavListEdit.this.RefreshHelpTips();
                        return true;
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        FavListEdit.this.DoRenameAction();
                        return true;
                }
            }
            return false;
        }
    }

    public FavListEdit() {
        this.favGroupList = null;
        this.favgroupAdapter = null;
        this.mFavgroupFirstVisibleItem = 0;
        this.mFavgroupVisibleItemCount = 0;
        this.favgroup_list_item_pos = 0;
        this.favChannelList = null;
        this.favChannelAdapter = null;
        this.mFavChannelFirstVisibleItem = 0;
        this.mFavChannelVisibleItemCount = 0;
        this.favChannel_list_item_pos = 0;
        this.favgroupname = null;
        this.serviceInfo = new service();
        this.serviceInfo_temp = new service();
        this.current_service_type = 0;
        this.current_service_index = 0;
        this.favlist_help_tips = null;
        this.favlist_help_tips_two = null;
        this.favedit_rl = null;
        this.channel_type = null;
        this.fchangegroup = false;
        this.mSHCallback = new C01571();
        this.pswdDig = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        Intent stopMusicIntent = new Intent();
        stopMusicIntent.setAction("com.android.music.musicservicecommand.pause");
        stopMusicIntent.putExtra("command", "stop");
//        sendBroadcastAsUser(stopMusicIntent, UserHandle.ALL);
        setContentView(R.layout.favlistedit_avtivity);
        initMessagehandle();
    }

    protected void onResume() {
        enableMwMessageCallback(this.mwMsgHandler);
        initData();
        initView();
        super.onResume();
    }

    protected void onPause() {
        if (MW.db_get_service_available_fav_group_count() == 0 || MW.db_get_service_count(this.current_service_type) == 0) {
            MW.db_service_set_in_region_mode_with_service_type_index(this.serviceInfo_temp.service_type, this.serviceInfo_temp.service_index);
        }
        MW.channel_edit_exit();
        MW.ts_player_stop_play();
        super.onPause();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == 0) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                    RefreshTvRadio();
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void RefreshTvRadio() {
        switch (MW.ts_player_play_switch_tv_radio(false)) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                RefreshFavChannel(this.favgroup_list_item_pos);
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                SETTINGS.makeText(this, R.string.NO_CHANNEL, 0);
            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                SETTINGS.makeText(this, R.string.NO_VIDEO_CHANNEL, 0);
            case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                SETTINGS.makeText(this, R.string.NO_AUDIO_CHANNEL, 0);
            case MW.RET_SERVICE_LOCK /*44*/:
                RefreshFavChannel(this.favgroup_list_item_pos);
                MW.ts_player_play_current(true, false);
            default:
        }
    }

    private void initData() {
        if (MW.db_get_current_service_info(this.serviceInfo_temp) == 0) {
            this.favgroupname = SETTINGS.getFavGroupListArray();
            MW.db_service_set_in_fav_mode();
            this.mFavgroupFirstVisibleItem = MW.db_get_service_total_fav_group_current_index();
        } else {
            this.favgroupname = SETTINGS.getFavGroupListArray();
            MW.db_service_set_in_fav_mode();
            this.mFavgroupFirstVisibleItem = MW.db_get_service_total_fav_group_current_index();
        }
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            this.mFavChannelFirstVisibleItem = this.current_service_index;
        }
        if (MW.channel_edit_init() == 0) {
            SETTINGS.set_screen_mode();
        }
    }

    private void initView() {
        initVideoView();
        initFavGroupList();
        initFavChannelList();
        this.channel_type = (TextView) findViewById(R.id.channeltype_txt);
        initHelpTips();
        setEpgBackgroundAlpha(220);
    }

    private void initHelpTips() {
        this.favlist_help_tips = (LinearLayout) findViewById(R.id.fav_help_tips);
        this.favlist_help_tips_two = (LinearLayout) findViewById(R.id.fav_help_tips_two);
    }

    private void RefreshHelpTips() {
        if (this.favlist_help_tips.getVisibility() == 0) {
            this.favlist_help_tips.setVisibility(View.GONE);
            this.favlist_help_tips_two.setVisibility(View.VISIBLE);
            return;
        }
        this.favlist_help_tips_two.setVisibility(View.GONE);
        this.favlist_help_tips.setVisibility(View.VISIBLE);
    }

    private void setEpgBackgroundAlpha(int transparency) {
        this.favedit_rl = (RelativeLayout) findViewById(R.id.favedit_rl);
        this.favedit_rl.getBackground().setAlpha(transparency);
    }

    private void initVideoView() {
        VideoView videoView = (VideoView) findViewById(R.id.favlist_video);
        videoView.setFocusable(false);
        videoView.setClickable(false);
        videoView.getHolder().addCallback(this.mSHCallback);
        SETTINGS.videoViewSetFormat(this, videoView);
    }

    private void RefreshFavChannel(int pos) {
        if (this.favChannelList.getChildAt(this.favChannel_list_item_pos) != null) {
            this.favChannelList.getChildAt(this.favChannel_list_item_pos).setBackgroundResource(R.drawable.listview_item_bg_selector);
        }
        if (this.fchangegroup) {
            System.out.println("change group");
            this.fchangegroup = false;
            this.favChannel_list_item_pos = 0;
            this.favChannelList.setCustomSelection(this.favChannel_list_item_pos);
            if (MW.db_get_service_count(this.current_service_type) > 0 && MW.db_get_service_info(this.current_service_type, this.favChannel_list_item_pos, this.serviceInfo) == 0) {
                int Ret = MW.ts_player_play(this.current_service_type, this.favChannel_list_item_pos, 50, false);
                SETTINGS.set_led_channel(this.serviceInfo.channel_number_display);
                this.current_service_index = this.serviceInfo.service_index;
                System.out.println("Fav Ret  = " + Ret);
                if (Ret == 44) {
                    MW.ts_player_play_current(true, false);
                }
            }
        }
        if (this.favGroupList != null && this.favGroupList.getCount() > 0) {
            MW.db_set_service_total_fav_group_current_index(pos);
            MW.channel_edit_init();
        }
        if (MW.db_get_service_count(this.current_service_type) == 0) {
            MW.ts_player_stop_play();
            this.favChannel_list_item_pos = 0;
            this.current_service_index = 0;
        }
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            if (this.current_service_type == 0) {
                this.channel_type.setText(R.string.TvChannelList);
            } else {
                this.channel_type.setText(R.string.RadioChannelList);
            }
        }
        this.favChannelAdapter.notifyDataSetChanged();
    }

    private void initFavGroupList() {
        this.favGroupList = (CustomListView) findViewById(R.id.favgroup_listview);
        this.favgroupAdapter = new FavgroupAdapter(this);
        this.favGroupList.setAdapter(this.favgroupAdapter);
        this.favGroupList.setChoiceMode(1);
        this.favGroupList.setCustomScrollListener(new C01582());
        this.favGroupList.setOnItemSelectedListener(new C01593());
        this.favGroupList.setCustomKeyListener(new grouplistOnKeyListener());
        this.favGroupList.setVisibleItemCount(9);
        this.favGroupList.setCustomSelection(this.mFavgroupFirstVisibleItem);
        this.favGroupList.requestFocus();
    }

    private void initFavChannelList() {
        this.favChannelList = (CustomListView) findViewById(R.id.favprogram_listview);
        this.favChannelAdapter = new FavChannelAdapter(this);
        this.favChannelList.setAdapter(this.favChannelAdapter);
        this.favChannelList.setChoiceMode(1);
        this.favChannelList.setCustomScrollListener(new C01604());
        this.favChannelList.setOnItemSelectedListener(new C01615());
        this.favChannelList.setCustomKeyListener(new channellistOnKeyListener());
        this.favChannelList.setVisibleItemCount(8);
        this.favChannelList.setCustomSelection(this.mFavChannelFirstVisibleItem);
    }

    private void DoRenameAction() {
        Dialog editDialog = new EditDialog(this, R.style.MyDialog);
        editDialog.setContentView(R.layout.pvr_edit_custom_dia);
        editDialog.show();
        LayoutParams lp = editDialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        editDialog.getWindow().setAttributes(lp);
        editDialog.getWindow().addFlags(2);
    }

    private void CancelFavStatus() {
        DeleteDia deletedia = new DeleteDia(this, R.style.MyDialog);
        deletedia.show();
        LayoutParams lp = deletedia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        deletedia.getWindow().setAttributes(lp);
        deletedia.getWindow().addFlags(2);
    }

    private void DoSelectionAction() {
        int selectItem = this.favChannel_list_item_pos;
        if (MW.channel_edit_check_selection(selectItem)) {
            MW.channel_edit_select_single(false, selectItem);
        } else {
            MW.channel_edit_select_single(true, selectItem);
        }
        this.favChannelAdapter.notifyDataSetChanged();
    }

    private void initMessagehandle() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
    }
}
