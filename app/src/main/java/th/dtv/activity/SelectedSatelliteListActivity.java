package th.dtv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import th.dtv.ChannelSearchActivity;
import th.dtv.CustomListView;
import th.dtv.DtvApp;
import th.dtv.DtvBaseActivity;
import th.dtv.MW;
import th.dtv.R;
import th.dtv.SETTINGS;
import th.dtv.mw_data.dvb_s_sat;

public class SelectedSatelliteListActivity extends DtvBaseActivity {
    ArrayList<Boolean> SatSelStatus;
    ArrayList<Integer> SelSatList;
    ArrayList<Integer> SelTpList;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    dvb_s_sat satInfo;
    private int satInfo_item_pos;
    private SateInfoAdapter sateInfoAdapter;
    private CustomListView sate_listView;
    private TextView select_txt;

    /* renamed from: th.dtv.activity.SelectedSatelliteListActivity.1 */
    class C02401 implements OnScrollListener {
        C02401() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            SelectedSatelliteListActivity.this.mFirstVisibleItem = firstVisibleItem;
            SelectedSatelliteListActivity.this.mVisibleItemCount = visibleItemCount;
            System.out.println("mVisibleItemCount" + SelectedSatelliteListActivity.this.mVisibleItemCount);
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    /* renamed from: th.dtv.activity.SelectedSatelliteListActivity.2 */
    class C02412 implements OnItemSelectedListener {
        C02412() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
            SelectedSatelliteListActivity.this.satInfo_item_pos = position;
            if (SelectedSatelliteListActivity.this.SatSelStatus == null) {
                return;
            }
            if (((Boolean) SelectedSatelliteListActivity.this.SatSelStatus.get(SelectedSatelliteListActivity.this.satInfo_item_pos)).booleanValue()) {
                SelectedSatelliteListActivity.this.ShowUnSelectTipsView();
            } else {
                SelectedSatelliteListActivity.this.ShowSelectTipsView();
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: th.dtv.activity.SelectedSatelliteListActivity.3 */
    class C02423 implements OnKeyListener {
        C02423() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                    SelectedSatelliteListActivity.this.RefreshSatSelectImage();
                    return true;
                case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    return true;
                case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    SelectedSatelliteListActivity.this.ScanAction();
                    return true;
            }
            return false;
        }
    }

    private class SateInfoAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mcontext;

        class ViewHolder {
            TextView sate_longitude;
            TextView sate_name;
            TextView sate_no;
            ImageView sate_select_img;

            ViewHolder() {
            }
        }

        public SateInfoAdapter(Context context) {
            this.mcontext = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_dvb_s_get_selected_sat_count(0);
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            System.out.println("SateInfoAdapter pos = " + pos);
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.selected_satellite_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.sate_no = (TextView) convertView.findViewById(R.id.sate_no);
                localViewHolder.sate_select_img = (ImageView) convertView.findViewById(R.id.sate_select_img);
                localViewHolder.sate_name = (TextView) convertView.findViewById(R.id.sate_name);
                localViewHolder.sate_longitude = (TextView) convertView.findViewById(R.id.sate_longitude);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            localViewHolder.sate_no.setText(String.format("%03d", new Object[]{Integer.valueOf(pos + 1)}));
            if (MW.db_dvb_s_get_selected_sat_info(0, pos, SelectedSatelliteListActivity.this.satInfo) == 0) {
                String str;
                if (SelectedSatelliteListActivity.this.SatSelStatus != null) {
                    if (((Boolean) SelectedSatelliteListActivity.this.SatSelStatus.get(pos)).booleanValue()) {
                        localViewHolder.sate_select_img.setVisibility(View.VISIBLE);
                    } else {
                        localViewHolder.sate_select_img.setVisibility(View.INVISIBLE);
                    }
                }
                localViewHolder.sate_name.setText(SelectedSatelliteListActivity.this.satInfo.sat_name);
                TextView textView = localViewHolder.sate_longitude;
                String str2 = "%d.%d %s";
                Object[] objArr = new Object[3];
                objArr[0] = Integer.valueOf(SelectedSatelliteListActivity.this.satInfo.sat_degree_dec);
                objArr[1] = Integer.valueOf(SelectedSatelliteListActivity.this.satInfo.sat_degree_point);
                if (SelectedSatelliteListActivity.this.satInfo.sat_position == 0) {
                    str = "E";
                } else {
                    str = "W";
                }
                objArr[2] = str;
                textView.setText(String.format(str2, objArr));
            } else {
                System.out.println("db_dvb_s_get_sat_info  failed");
                localViewHolder.sate_name.setText("");
                localViewHolder.sate_longitude.setText("");
                localViewHolder.sate_select_img.setVisibility(View.INVISIBLE);
            }
            return convertView;
        }
    }

    public class ScanActionDig extends Dialog {
        private LinearLayout channel_type_ll;
        private TextView channel_type_opt;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private LinearLayout nit_search_ll;
        private TextView nit_search_opt;
        private LinearLayout scan_type_ll;
        private TextView scan_type_opt;
        private LinearLayout service_type_ll;
        private TextView servicetype_opt;
        private void RefreshBg() {
            if (this.channel_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.servicetype_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.scan_type_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
            } else if (this.nit_search_opt.isFocused()) {
                this.channel_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.service_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.scan_type_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
            }
            RefreshNitSearchBg(this.nit_search_opt.isFocusable());
        }

        private void RefreshNitSearchBg(boolean enabled) {
            if (enabled) {
                this.nit_search_opt.setEnabled(true);
                this.nit_search_opt.setFocusable(true);
                if (this.nit_search_opt.isFocused()) {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_foc);
                    return;
                } else {
                    this.nit_search_ll.setBackgroundResource(R.drawable.live_channel_list_item_bg_nor);
                    return;
                }
            }
            this.nit_search_opt.setEnabled(false);
            this.nit_search_opt.setFocusable(false);
            this.nit_search_ll.setBackgroundResource(R.drawable.epg_channel_list_item_selected);
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.satellite_scan_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.satellite_scan_dialog, null);
            this.channel_type_ll = (LinearLayout) findViewById(R.id.channel_type_ll);
            this.service_type_ll = (LinearLayout) findViewById(R.id.service_type_ll);
            this.scan_type_ll = (LinearLayout) findViewById(R.id.scan_type_ll);
            this.nit_search_ll = (LinearLayout) findViewById(R.id.nit_search_ll);
            this.channel_type_opt = (TextView) findViewById(R.id.channel_type_opt);
            this.servicetype_opt = (TextView) findViewById(R.id.servicetype_opt);
            this.scan_type_opt = (TextView) findViewById(R.id.scan_type_opt);
            this.nit_search_opt = (TextView) findViewById(R.id.nit_search_opt);
            if (SETTINGS.get_search_nit()) {
                this.nit_search_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.on));
            } else {
                this.nit_search_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.off));
            }
            if (SETTINGS.get_search_service_type() == 0) {
                this.channel_type_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.All));
            } else {
                this.channel_type_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.FTAOnly));
            }
            if (SETTINGS.get_search_tv_type() == 0) {
                this.servicetype_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.All));
            } else if (SETTINGS.get_search_tv_type() == 1) {
                this.servicetype_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.TV));
            } else if (SETTINGS.get_search_tv_type() == 2) {
                this.servicetype_opt.setText(SelectedSatelliteListActivity.this.getResources().getString(R.string.Radio));
            }
            this.scan_type_opt.requestFocus();
            RefreshBg();
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            int i2 = 1;
            if (keyEvent.getAction() != 0) {
                switch (i) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            switch (i) {
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    if (this.nit_search_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.scan_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.scan_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    if (this.scan_type_opt.isFocused()) {
                        if (this.nit_search_opt.isEnabled()) {
                            this.nit_search_opt.requestFocus();
                        } else {
                            this.channel_type_opt.requestFocus();
                        }
                        RefreshBg();
                        return true;
                    } else if (this.nit_search_opt.isFocused()) {
                        this.channel_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.channel_type_opt.isFocused()) {
                        this.servicetype_opt.requestFocus();
                        RefreshBg();
                        return true;
                    } else if (this.servicetype_opt.isFocused()) {
                        this.scan_type_opt.requestFocus();
                        RefreshBg();
                        return true;
                    }
                    break;
                case MW.RET_INVALID_TYPE /*23*/:
                    boolean z;
                    int i3;
                    int i4;
                    if (this.nit_search_opt.getText().toString().equals(SelectedSatelliteListActivity.this.getResources().getString(R.string.on))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (this.channel_type_opt.getText().toString().equals(SelectedSatelliteListActivity.this.getResources().getString(R.string.All))) {
                        i3 = 0;
                    } else {
                        i3 = 1;
                    }
                    if (this.servicetype_opt.getText().toString().equals(SelectedSatelliteListActivity.this.getResources().getString(R.string.All))) {
                        i4 = 0;
                    } else if (this.servicetype_opt.getText().toString().equals(SelectedSatelliteListActivity.this.getResources().getString(R.string.TV))) {
                        i4 = 1;
                    } else if (this.servicetype_opt.getText().toString().equals(SelectedSatelliteListActivity.this.getResources().getString(R.string.Radio))) {
                        i4 = 2;
                    } else {
                        i4 = 0;
                    }
                    if (!this.scan_type_opt.getText().toString().equals(SelectedSatelliteListActivity.this.getResources().getString(R.string.auto_scan))) {
                        i2 = 0;
                    }
                    SETTINGS.set_search_service_type(i3);
                    SETTINGS.set_search_tv_type(i4);
                    SETTINGS.set_search_nit(z);
                    SelectedSatelliteListActivity.this.getSelSatData();
                    SelectedSatelliteListActivity.this.getSelTpData();
                    Intent intent = new Intent();
                    intent.setFlags(67108864);
                    intent.setClass(SelectedSatelliteListActivity.this, ChannelSearchActivity.class);
                    intent.putExtra("system_type", 0);
                    intent.putExtra("search_type", i2);
                    intent.putIntegerArrayListExtra("search_sat_index", SelectedSatelliteListActivity.this.SelSatList);
                    intent.putIntegerArrayListExtra("search_tp_index", SelectedSatelliteListActivity.this.SelTpList);
                    SelectedSatelliteListActivity.this.startActivity(intent);
                    dismiss();
                    break;
            }
            return super.onKeyDown(i, keyEvent);
        }

        public ScanActionDig(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }
    }

    public SelectedSatelliteListActivity() {
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.satInfo = new dvb_s_sat();
        this.satInfo_item_pos = 0;
        this.SatSelStatus = null;
        this.SelSatList = new ArrayList();
        this.SelTpList = new ArrayList();
    }

    private void initSateListView() {
        this.sate_listView = (CustomListView) findViewById(R.id.satellite_listview);
        this.sateInfoAdapter = new SateInfoAdapter(this);
        this.sate_listView.setAdapter(this.sateInfoAdapter);
        this.sate_listView.setVisibleItemCount(11);
        this.sate_listView.setCustomScrollListener(new C02401());
        this.sate_listView.setOnItemSelectedListener(new C02412());
        this.sate_listView.setCustomKeyListener(new C02423());
        int sat_index = getIntent().getIntExtra("selected_sat_index", 0);
        if (MW.db_dvb_s_get_selected_sat_info(0, sat_index, this.satInfo) == 0) {
            this.sate_listView.setCustomSelection(sat_index);
        }
    }

    private void RefreshSatSelectImage() {
        if (this.SatSelStatus != null) {
            if (((Boolean) this.SatSelStatus.get(this.satInfo_item_pos)).booleanValue()) {
                this.SatSelStatus.set(this.satInfo_item_pos, Boolean.valueOf(false));
                ShowSelectTipsView();
            } else {
                this.SatSelStatus.set(this.satInfo_item_pos, Boolean.valueOf(true));
                ShowUnSelectTipsView();
            }
        }
        this.sateInfoAdapter.notifyDataSetChanged();
    }

    void ShowToastInformation(String text, int duration_mode) {
        LinearLayout toast_view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
        ((TextView) toast_view.findViewById(R.id.toast_text)).setText(text);
        Toast toast = new Toast(this);
        if (duration_mode == 0) {
            toast.setDuration(0);
        } else if (duration_mode == 1) {
            toast.setDuration(1);
        }
        toast.setView(toast_view);
        toast.setGravity(17, 0, -70);
        toast.show();
    }

    private void getSelSatData() {
        this.SelSatList.clear();
        dvb_s_sat satInfo = new dvb_s_sat();
        int i = 0;
        while (i < MW.db_dvb_s_get_selected_sat_count(0)) {
            if (this.SatSelStatus != null && ((Boolean) this.SatSelStatus.get(i)).booleanValue() && MW.db_dvb_s_get_selected_sat_info(0, i, satInfo) == 0) {
                this.SelSatList.add(Integer.valueOf(satInfo.sat_index));
                System.out.println("i  ::::: " + satInfo.sat_index);
            }
            i++;
        }
        if (this.SelSatList.size() == 0 && this.sate_listView != null && this.sate_listView.getCount() != -1 && MW.db_dvb_s_get_selected_sat_info(0, this.satInfo_item_pos, satInfo) == 0) {
            this.SelSatList.add(Integer.valueOf(satInfo.sat_index));
        }
    }

    private void getSelTpData() {
        this.SelTpList.clear();
        this.SelTpList.add(Integer.valueOf(0));
    }

    private void ScanAction() {
        Dialog scanActionDig = new ScanActionDig(this, R.style.MyDialog);
        scanActionDig.setContentView(R.layout.satellite_scan_dialog);
        scanActionDig.show();
        scanActionDig.getWindow().addFlags(2);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.selected_satellite_list_activity);
        if (MW.db_dvb_s_get_selected_sat_count(0) == 0) {
            ShowToastInformation(getResources().getString(R.string.no_selected_satellite), 0);
            finishMyself();
            return;
        }
        initData();
        initSateListView();
        initTipView();
    }

    private void initTipView() {
        this.select_txt = (TextView) findViewById(R.id.select_txt);
    }

    private void ShowSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.SelectSat));
    }

    private void ShowUnSelectTipsView() {
        this.select_txt.setText(getResources().getString(R.string.UnSelectSat));
    }

    private void initData() {
        int count = MW.db_dvb_s_get_selected_sat_count(0);
        if (count > 0) {
            this.SatSelStatus = new ArrayList();
            for (int i = 0; i < count; i++) {
                this.SatSelStatus.add(i, Boolean.valueOf(false));
            }
            return;
        }
        this.SatSelStatus = null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != 0) {
            switch (keyCode) {
                case MW.RET_INVALID_TYPE /*23*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    return true;
                default:
                    break;
            }
        }
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.send_led_msg("NENU");
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
    }
}
