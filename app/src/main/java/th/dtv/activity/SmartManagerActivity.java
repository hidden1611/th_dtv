package th.dtv.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import th.dtv.DtvBaseActivity;
import th.dtv.R;

public class SmartManagerActivity extends DtvBaseActivity {
    private Button biss_btn;
    private Button smartcard_btn;

    /* renamed from: th.dtv.activity.SmartManagerActivity.1 */
    class C02471 implements OnClickListener {
        C02471() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(SmartManagerActivity.this, TestActivity.class);
            SmartManagerActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.activity.SmartManagerActivity.2 */
    class C02482 implements OnClickListener {
        C02482() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(SmartManagerActivity.this, BissActivity.class);
            SmartManagerActivity.this.startActivity(intent);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.smart_manager_activity);
        initView();
        SetupControl();
    }

    private void SetupControl() {
        this.smartcard_btn.setOnClickListener(new C02471());
        this.biss_btn.setOnClickListener(new C02482());
    }

    private void initView() {
        this.smartcard_btn = (Button) findViewById(R.id.smartcard_btn);
        this.biss_btn = (Button) findViewById(R.id.biss_btn);
    }
}
