package th.dtv;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ComboEditText extends LinearLayout implements OnFocusChangeListener {
    private Context mContext;
    private String mEditString;
    protected DigitsEditText mInfoTextView;
    private TextWatcher mListener;
    private int mTitleResId;
    protected TextView mTitleTextView;
    protected TextView unit_textview;

    public ComboEditText(Context context) {
        this(context, null);
    }

    public ComboEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mTitleResId = 0;
        this.mEditString = null;
        this.mListener = null;
        this.mContext = context;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.comboedit_layout, this);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mTitleTextView = (TextView) findViewById(R.id.title_textview);
        this.unit_textview = (TextView) findViewById(R.id.unit_textview);
        this.mInfoTextView = (DigitsEditText) findViewById(R.id.info_textview);
        this.mInfoTextView.setOnFocusChangeListener(this);
    }

    public TextView getTitleTextView() {
        return this.mTitleTextView;
    }

    public DigitsEditText getInfoTextView() {
        return this.mInfoTextView;
    }

    public void initView(int titleResId, String eidtString, String unit, TextWatcher listener) {
        this.mTitleResId = titleResId;
        if (eidtString != null) {
            this.mEditString = eidtString;
        }
        if (listener != null) {
            this.mListener = listener;
        }
        if (this.mTitleResId > 0) {
            this.mTitleTextView.setText(this.mTitleResId);
        }
        if (this.mEditString != null) {
            this.mInfoTextView.setDigitsText(this.mEditString);
        }
        if (!(this.mInfoTextView == null || this.mListener == null)) {
            this.mInfoTextView.addTextChangedListener(this.mListener);
        }
        if (unit != null) {
            this.unit_textview.setText("  " + unit);
        }
    }

    public void SetInfoTextView(String text) {
        this.mInfoTextView.setDigitsText(text);
    }

    public void onFocusChange(View arg0, boolean arg1) {
        if (arg1) {
            this.mInfoTextView.setSelection(this.mInfoTextView.length());
            setBackgroundResource(R.drawable.live_channel_list_item_bg);
            return;
        }
        if (this.mInfoTextView.length() == 0) {
            this.mInfoTextView.setDigitsText(this.mEditString);
        }
        setBackgroundResource(17170445);
    }
}
