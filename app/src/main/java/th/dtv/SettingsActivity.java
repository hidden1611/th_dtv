package th.dtv;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.TextView;
import th.dtv.activity.PswdCB;
import th.dtv.activity.RecordListActivity;
import th.dtv.activity.TimerListActivity;

public class SettingsActivity extends DtvBaseActivity {
    private Dialog dlg;
    private TextView mAdvancedTextView;
    private TextView mGeneralTextView;
    private TextView mPvrListTextView;
    private TextView mPvrSetTextView;
    private TextView mTimerListTextView;

    /* renamed from: th.dtv.SettingsActivity.1 */
    class C00871 implements OnClickListener {
        C00871() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(SettingsActivity.this, GeneralSettingsActivity.class);
            SettingsActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.SettingsActivity.2 */
    class C00902 implements OnClickListener {

        /* renamed from: th.dtv.SettingsActivity.2.1 */
        class C00881 implements OnKeyListener {
            C00881() {
            }

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 1 || (keyCode != 4 && keyCode != 82)) {
                    return false;
                }
                SettingsActivity.this.dlg.dismiss();
                return true;
            }
        }

        /* renamed from: th.dtv.SettingsActivity.2.2 */
        class C00892 implements PswdCB {
            C00892() {
            }

            public void DoCallBackEvent() {
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.setClass(SettingsActivity.this, AdvancedSettingsActivity.class);
                SettingsActivity.this.startActivity(intent);
            }
        }

        C00902() {
        }

        public void onClick(View v) {
//            SettingsActivity.this.dlg = new PasswordDialog(SettingsActivity.this, R.style.MyDialog, new C00881(), new C00892());
//            SettingsActivity.this.dlg.show();
//            ((PasswordDialog) SettingsActivity.this.dlg).setHandleUpDown(true);
        }
    }

    /* renamed from: th.dtv.SettingsActivity.3 */
    class C00913 implements OnClickListener {
        C00913() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(SettingsActivity.this, PvrSettingsActivity.class);
            SettingsActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.SettingsActivity.4 */
    class C00924 implements OnClickListener {
        C00924() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(SettingsActivity.this, RecordListActivity.class);
            SettingsActivity.this.startActivity(intent);
        }
    }

    /* renamed from: th.dtv.SettingsActivity.5 */
    class C00935 implements OnClickListener {
        C00935() {
        }

        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(SettingsActivity.this, TimerListActivity.class);
            SettingsActivity.this.startActivity(intent);
        }
    }

    public SettingsActivity() {
        this.dlg = null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DtvApp.getInstance().addActivity(this);
        setContentView(R.layout.dtv_settings_new);
        initView();
    }

    private void initView() {
        this.mGeneralTextView = (TextView) findViewById(R.id.general_textview);
        this.mGeneralTextView.setOnClickListener(new C00871());
        this.mAdvancedTextView = (TextView) findViewById(R.id.advanced_textview);
        this.mAdvancedTextView.setOnClickListener(new C00902());
        this.mPvrSetTextView = (TextView) findViewById(R.id.pvrset_textview);
        this.mPvrSetTextView.setOnClickListener(new C00913());
        this.mPvrListTextView = (TextView) findViewById(R.id.pvrlist_textview);
        this.mPvrListTextView.setOnClickListener(new C00924());
        this.mTimerListTextView = (TextView) findViewById(R.id.timerlist_textview);
        this.mTimerListTextView.setOnClickListener(new C00935());
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    protected void onResume() {
        super.onResume();
        SETTINGS.send_led_msg("NENU");
    }
}
