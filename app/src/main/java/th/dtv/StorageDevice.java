package th.dtv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class StorageDevice {
    private static String root_path;
    private static String sd_prefix;
    ArrayList<DeviceItem> deviceList;
    String format;
    private Context mContext;
    private DeviceInfoReceiver receiver;
    private String u_prefix;

    public class DeviceInfoReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Uri uri = intent.getData();
            if (uri.getScheme().equals("file")) {
                String path = uri.getPath();
                Intent device_intent;
                if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                    Log.d("StorageDevice", "mount path=" + path);
                    StorageDevice.this.refreshDevice();
                    device_intent = new Intent("android.intent.action.DEVICE_MOUNTED");
                    device_intent.putExtra("device_path", path);
                    context.sendBroadcast(device_intent);
                } else if (action.equals("android.intent.action.MEDIA_REMOVED")) {
                    Log.d("StorageDevice", "unmount path=" + path);
                    StorageDevice.this.refreshDevice();
                    device_intent = new Intent("android.intent.action.DEVICE_REMOVED");
                    device_intent.putExtra("device_path", path);
                    context.sendBroadcast(device_intent);
                }
            }
        }
    }

    public class DeviceItem {
        public String Path;
        public String VolumeName;
        public String format;
        public String spare;
        public String total;
        public String used;
    }

    static {
        root_path = "/storage/external_storage";
        sd_prefix = root_path + "/sdcard";
    }

    public StorageDevice(Context context) {
        this.mContext = null;
        this.deviceList = null;
        this.u_prefix = root_path + "/sd";
        this.format = null;
        this.mContext = context;
        this.deviceList = new ArrayList();
        this.receiver = new DeviceInfoReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.MEDIA_REMOVED");
        filter.addAction("android.intent.action.MEDIA_MOUNTED");
        filter.addDataScheme("file");
        context.registerReceiver(this.receiver, filter);
        refreshDevice();
    }

    public void finish(Context context) {
        if (this.receiver != null) {
            context.unregisterReceiver(this.receiver);
            this.receiver = null;
        }
    }

    public int getDeviceCount() {
        return this.deviceList.size();
    }

    public DeviceItem getDeviceItem(int id) {
        if (id < 0 || id >= this.deviceList.size()) {
            return null;
        }
        return (DeviceItem) this.deviceList.get(id);
    }

    private String deleteExtraSpace(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0 || str.equals(" ")) {
            return new String();
        }
        char[] oldStr = str.toCharArray();
        int len = str.length();
        char[] tmpStr = new char[len];
        boolean keepSpace = false;
        int i = 0;
        int j = 0;
        while (i < len) {
            int j2;
            char tmpChar = oldStr[i];
            if (oldStr[i] != ' ') {
                j2 = j + 1;
                tmpStr[j] = tmpChar;
                keepSpace = true;
            } else if (keepSpace) {
                j2 = j + 1;
                tmpStr[j] = tmpChar;
                keepSpace = false;
            } else {
                j2 = j;
            }
            i++;
            j = j2;
        }
        int newLen = j;
        if (tmpStr[j - 1] == ' ') {
            newLen--;
        }
        char[] newStr = new char[newLen];
        for (i = 0; i < newLen; i++) {
            newStr[i] = tmpStr[i];
        }
        return new String(newStr);
    }

    public void refreshDevice() {
        getDevices();
    }

    private void getDevices() {
        this.deviceList.clear();
        File[] files = new File(root_path).listFiles();
        if (files != null) {
            for (File file : files) {
                String path = file.getPath();
                if (path.startsWith(this.u_prefix) && !path.startsWith(sd_prefix)) {
                    Log.d("*******", "usb device path: " + file.getName());
                    DeviceItem item = getDeviceName(file.getName(), path);
                    if (!(item == null || item.format == null)) {
                        item.Path = path;
                        item.VolumeName += " [" + file.getName() + "]";
                        Log.d("*******", "device path: " + item.Path + " device format: " + item.format + " name: " + item.VolumeName);
                        readUsbDevice(item.Path, item, 0);
                        this.deviceList.add(item);
                    }
                } else if (path.startsWith(sd_prefix)) {
                    Log.d("*******", "sd device path: " + file.getName());
                    sdcard_deal(path);
                }
            }
        }
    }

    DeviceItem getDeviceName(String devname, String path) {
        DeviceItem item = new DeviceItem();
//        devname = SystemProperties.get("volume.label." + devname, null);
        devname = "ehome";
        if (devname == null || devname.length() == 0) {
            devname = this.mContext.getResources().getString(R.string.volume);
            item.format = "UNKNOWN";
            if (findSdcardString(path)) {
                item.format = this.format;
            } else {
                item.format = "UNKNOWN";
            }
        } else {
            if (findSdcardString(path)) {
                item.format = this.format;
            } else {
                item.format = "UNKNOWN";
            }
            String[] byteStrings = devname.split(" ");
            byte[] bytes = new byte[(byteStrings.length - 1)];
            for (int i = 1; i < byteStrings.length; i++) {
                bytes[i - 1] = Integer.decode(byteStrings[i]).byteValue();
            }
            if (byteStrings[0].equals("NTFS") || byteStrings[0].equals("NTFS")) {
                try {
                    devname = new String(bytes, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    devname = this.mContext.getResources().getString(R.string.volume);
                }
            } else if (byteStrings[0].equals("VFAT")) {
                try {
                    devname = new String(bytes, "UTF-8");
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                    devname = this.mContext.getResources().getString(R.string.volume);
                }
            } else {
                try {
                    devname = new String(bytes, "UTF-8");
                } catch (UnsupportedEncodingException e22) {
                    e22.printStackTrace();
                    devname = this.mContext.getResources().getString(R.string.volume);
                }
            }
        }
        item.VolumeName = devname;
        return item;
    }

    private boolean findSdcardString(String path) {
        Runtime runtime = Runtime.getRuntime();
        String cmd = "mount";
        try {
            String strLine;
            String tmp = new File(path).getCanonicalPath();
            BufferedReader br = new BufferedReader(new InputStreamReader(runtime.exec(cmd).getInputStream()));
            loop0:
            while (true) {
                strLine = br.readLine();
                if (strLine == null) {
                    return false;
                }
                Log.d("StorageDevice", ">>>" + strLine);
                for (int i = 0; i < strLine.length(); i++) {
                    if (strLine.regionMatches(i, tmp, 0, tmp.length())) {
                        break loop0;
                    }
                }
            }
            this.format = deleteExtraSpace(strLine).split(" ")[2].toUpperCase();
            if (this.format.equals("VFAT")) {
                this.format = "FAT32";
            } else if (this.format.equals("FUSEBLK")) {
                this.format = "NTFS";
            }
            Log.d("StorageDevice", "format : " + this.format);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void readUsbDevice(String path, DeviceItem item, int mode) {
        try {
            String strLine;
            BufferedReader br = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("df " + path).getInputStream()));
            do {
                strLine = br.readLine();
                if (strLine != null) {
                    Log.d("StorageDevice", ">>>" + strLine);
                } else {
                    return;
                }
            } while (!strLine.startsWith(path));
            String[] byteStrings = deleteExtraSpace(strLine).split(" ");
            Log.d("StorageDevice", "size=" + byteStrings[1] + "free==" + byteStrings[3]);
            item.total = byteStrings[1];
            item.used = byteStrings[2];
            item.spare = byteStrings[3];
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    DeviceItem getSdcardDevice(String path) {
        DeviceItem item = new DeviceItem();
        item.Path = path;
        item.VolumeName = "  SDCARD";
        item.format = this.format;
        return item;
    }

    private void readSDCard(String path, DeviceItem item) {
        readUsbDevice(path, item, 0);
    }

    private boolean sdcard_deal(String path) {
        if (!findSdcardString(path)) {
            return false;
        }
        DeviceItem item = getSdcardDevice(path);
        if (this.deviceList != null) {
            readSDCard(path, item);
            this.deviceList.add(item);
        }
        return true;
    }

    public static long getSpareSize(String str) {
        String string = str;
        String temp = null;
        long size = -1;
        if (str == null) {
            return -1;
        }
        string = string.trim();
        if (string.contains("G")) {
            temp = string.substring(0, string.indexOf("G"));
            size = (long) (((Double.parseDouble(temp) * 1024.0d) * 1024.0d) * 1024.0d);
        } else if (string.contains("M")) {
            temp = string.substring(0, string.indexOf("M"));
            size = (long) ((Double.parseDouble(temp) * 1024.0d) * 1024.0d);
        } else if (string.contains("K")) {
            temp = string.substring(0, string.indexOf("K"));
            size = (long) (Double.parseDouble(temp) * 1024.0d);
        } else if (string.contains("B")) {
            temp = string.substring(0, string.indexOf("B"));
            size = (long) Double.parseDouble(temp);
        }
        if (temp == null) {
            return -1;
        }
        System.out.println("getSpareSize>>>>>>>size " + size);
        return size;
    }
}
