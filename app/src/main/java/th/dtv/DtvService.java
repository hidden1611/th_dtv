package th.dtv;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import th.dtv.mw_data.book_callback_event_data;
import th.dtv.mw_data.book_event_data;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;
import th.dtv.mw_data.epg_pf_event;
import th.dtv.mw_data.service;

public class DtvService extends Service {
    public static final String DTV_PASSWORD_PASS_ACTION = "com.dtv.pswd.ok";
    public static final String DTV_PLAYER_ACTION = "com.dtv.player";
    public static final String DTV_PLAY_STATUS_ACTION = "com.dtv.play.status";
    private static final String DTV_SERVICE_STOP = "com.dtv.service.stop";
    public static final String DTV_SERVICE_TYPE = "com.dtv.service_type";
    public static final String DTV_STOP_ACTION = "com.dtv.stop";
    private static final String DTV_UPDATE_TIMEZONE_ACTION = "th.dtv.timezone";
    private static final int PLAY_STATUS_LOCK = 2;
    private static final int PLAY_STATUS_NO_AUDIO = 3;
    private static final int PLAY_STATUS_NO_CHANNEL = 4;
    private static final int PLAY_STATUS_NO_SIGNAL = 7;
    private static final int PLAY_STATUS_NO_VIDEO = 5;
    private static final int PLAY_STATUS_OK = 1;
    private static final int PLAY_STATUS_SCRAMBLE = 6;
    private static final int PLAY_STATUS_UNKNOW = 0;
    public static final String SHOW_DIALOG = "bookevent_showtimerdialog";
    private static final String TAG = "DtvService";
    private Dialog bookTipsDlg;
    private int count;
    private CountDownTimer counterTimer;
    private date date;
    private date_time dateTime;
    private epg_pf_event epgCurrentEvent;
    private epg_pf_event epgNextEvent;
    book_callback_event_data g_paramsInfo;
    private BroadcastReceiver mBookReceiver;
    private BroadcastReceiver mPlayerReceiver;
    private int playRet;
    private service serviceInfo;
    private boolean threadDisable;
    private book_event_data tmpBookEvent;
    private service tmpServiceInfo;

    /* renamed from: th.dtv.DtvService.1 */
    class C00561 extends CountDownTimer {
        C00561(long x0, long x1) {
            super(x0, x1);
        }

        public void onTick(long millisUntilFinished) {
            long left_time = millisUntilFinished / 1000;
            Log.d(DtvService.TAG, "seconds remaining :" + left_time);
            ((BookTipsDlg) DtvService.this.bookTipsDlg).setMyText(left_time);
        }

        public void onFinish() {
            Log.d(DtvService.TAG, "onFinish");
            if (DtvService.this.bookTipsDlg != null) {
                DtvService.this.bookTipsDlg.dismiss();
                DtvService.this.bookTipsDlg = null;
            }
            int Ret = MW.book_handle_event_by_index(DtvService.this.g_paramsInfo.event_index);
            if (Ret == 16) {
                Log.e(DtvService.TAG, " Book1 : handle event failed ");
            } else if (Ret != 0) {
                Log.e(DtvService.TAG, " Book1 : unknow ");
            } else if (DtvService.this.g_paramsInfo.book_type == DtvService.PLAY_STATUS_NO_AUDIO) {
                DtvService.this.PowerDown();
            } else {
                DtvService.this.gotoPlayActivity();
            }
        }
    }

    /* renamed from: th.dtv.DtvService.2 */
    class C00572 extends BroadcastReceiver {
        C00572() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DtvService.DTV_PLAYER_ACTION)) {
                SETTINGS.set_screen_mode(DtvService.PLAY_STATUS_OK);
                SETTINGS.set_video_enable(true);
                DtvService.this.playRet = MW.ts_player_play_current(false, true);
                SETTINGS.bPass = false;
                if (MW.db_get_current_service_info(DtvService.this.serviceInfo) != 0) {
                    SETTINGS.putSettingsProviderInt(DtvService.DTV_SERVICE_TYPE, DtvService.PLAY_STATUS_UNKNOW);
                } else if (DtvService.this.serviceInfo.service_type == 0) {
                    SETTINGS.putSettingsProviderInt(DtvService.DTV_SERVICE_TYPE, DtvService.PLAY_STATUS_UNKNOW);
                } else {
                    SETTINGS.putSettingsProviderInt(DtvService.DTV_SERVICE_TYPE, DtvService.PLAY_STATUS_OK);
                }
            } else if (action.equals(DtvService.DTV_STOP_ACTION)) {
                MW.ts_player_stop_play();
            } else if (action.equals(DtvService.DTV_PLAY_STATUS_ACTION)) {
                int status = MW.ts_player_get_play_status();
                Log.d(DtvService.TAG, " DTV_PLAY_STATUS_ACTION " + status);
                if (status != DtvService.PLAY_STATUS_LOCK) {
                    DtvService.this.count = DtvService.PLAY_STATUS_UNKNOW;
                }
                if (status == 0) {
                    DtvService.this.CheckChannelLockAndShowPasswd(DtvService.PLAY_STATUS_OK);
                } else if (status == DtvService.PLAY_STATUS_NO_CHANNEL) {
                    DtvService.this.CheckChannelLockAndShowPasswd(DtvService.PLAY_STATUS_NO_AUDIO);
                } else if (status == DtvService.PLAY_STATUS_OK) {
                    SETTINGS.putSettingsProviderInt(DtvService.DTV_PLAY_STATUS_ACTION, DtvService.PLAY_STATUS_NO_CHANNEL);
                } else if (status == DtvService.PLAY_STATUS_NO_AUDIO) {
                    DtvService.this.CheckChannelLockAndShowPasswd(DtvService.PLAY_STATUS_NO_VIDEO);
                } else if (status == DtvService.PLAY_STATUS_NO_VIDEO) {
                    DtvService.this.CheckChannelLockAndShowPasswd(DtvService.PLAY_STATUS_SCRAMBLE);
                } else if (status == DtvService.PLAY_STATUS_LOCK) {
                    DtvService.this.count = DtvService.this.count + DtvService.PLAY_STATUS_OK;
                    if (MW.db_get_current_service_info(DtvService.this.serviceInfo) != 0) {
                        SETTINGS.putSettingsProviderInt(DtvService.DTV_PLAY_STATUS_ACTION, DtvService.PLAY_STATUS_UNKNOW);
                        Log.d(DtvService.TAG, "get current service info failed");
                    } else if (SETTINGS.get_channel_lock_onoff()) {
                        if (DtvService.this.playRet == 44 && SETTINGS.bPass) {
                            DtvService.this.playRet = MW.ts_player_play_current(true, true);
                            if (DtvService.this.count == DtvService.PLAY_STATUS_LOCK) {
                                DtvService.this.count = DtvService.PLAY_STATUS_UNKNOW;
                                SETTINGS.putSettingsProviderInt(DtvService.DTV_PLAY_STATUS_ACTION, DtvService.PLAY_STATUS_NO_SIGNAL);
                            }
                        } else if (DtvService.this.playRet == 0) {
                            DtvService.this.CheckParentLockAndShowPasswd(DtvService.PLAY_STATUS_NO_SIGNAL);
                        } else if (DtvService.this.playRet == 44 && !SETTINGS.bPass) {
                            SETTINGS.putSettingsProviderInt(DtvService.DTV_PLAY_STATUS_ACTION, DtvService.PLAY_STATUS_LOCK);
                        }
                    } else if (DtvService.this.count == DtvService.PLAY_STATUS_LOCK) {
                        DtvService.this.count = DtvService.PLAY_STATUS_UNKNOW;
                        SETTINGS.putSettingsProviderInt(DtvService.DTV_PLAY_STATUS_ACTION, DtvService.PLAY_STATUS_NO_SIGNAL);
                    }
                }
            } else if (action.equals(DtvService.DTV_PASSWORD_PASS_ACTION)) {
                SETTINGS.bPass = true;
                SETTINGS.putSettingsProviderInt(DtvService.DTV_PLAY_STATUS_ACTION, DtvService.PLAY_STATUS_OK);
            } else if (action.equals(DtvService.DTV_SERVICE_STOP)) {
                Log.d(DtvService.TAG, " Intent.DTV_SERVICE_STOP ");
                DtvService.this.threadDisable = true;
                DtvService.this.unregisterReceiver(DtvService.this.mPlayerReceiver);
                DtvService.this.unregisterReceiver(DtvService.this.mBookReceiver);
                Process.killProcess(Process.myPid());
            } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                Log.d(DtvService.TAG, " Intent.ACTION_SCREEN_OFF ");
                MW.ts_player_stop_play_and_tunning();
                DtvService.this.setRTCAlarm();
                MW.enter_standby();
            } else if (action.equals(DtvService.DTV_UPDATE_TIMEZONE_ACTION)) {
                SETTINGS.set_timezone_value(DtvService.PLAY_STATUS_UNKNOW);
            }
        }
    }

    /* renamed from: th.dtv.DtvService.3 */
    class C00583 extends BroadcastReceiver {
        C00583() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(DtvService.TAG, "mBookReceiver: " + action);
            if (action.equals(DtvService.SHOW_DIALOG)) {
                Log.d(DtvService.TAG, "ShowBookTipsDialog");
                DtvService.this.ShowBookTipsDialog(context, DtvService.this.g_paramsInfo);
            }
        }
    }

    public class BookTipsDlg extends Dialog {
        TextView channel_name;
        TextView deldia_title;
        long left_time_ms;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        View mLayoutView;
        TextView mTextView;
        book_callback_event_data mparaminfo;
        TextView program_name;
        TextView time;

        /* renamed from: th.dtv.DtvService.BookTipsDlg.1 */
//        class C00591 implements OnClickListener {
//            C00591() {
//            }
//
//            public void onClick(View v) {
//                DtvService.this.counterTimer.cancel();
//                int Ret = MW.book_handle_event_by_index(DtvService.this.g_paramsInfo.event_index);
//                if (Ret == 16) {
//                    Log.e(DtvService.TAG, " Book2 : handle event failed ");
//                } else if (Ret != 0) {
//                    Log.e(DtvService.TAG, " Book2 : unknow ");
//                }
//                BookTipsDlg.this.dismiss();
//                DtvService.this.bookTipsDlg = null;
//            }
//        }
//
//        /* renamed from: th.dtv.DtvService.BookTipsDlg.2 */
//        class C00602 implements OnClickListener {
//            C00602() {
//            }
//
//            public void onClick(View v) {
//                DtvService.this.counterTimer.cancel();
//                int Ret = MW.book_handle_event_by_index(DtvService.this.g_paramsInfo.event_index);
//                if (Ret == 16) {
//                    Log.e(DtvService.TAG, " Book2 : handle event failed ");
//                } else if (Ret != 0) {
//                    Log.e(DtvService.TAG, " Book2 : unknow ");
//                } else if (BookTipsDlg.this.mparaminfo.book_type == DtvService.PLAY_STATUS_NO_AUDIO) {
//                    DtvService.this.PowerDown();
//                } else {
//                    DtvService.this.gotoPlayActivity();
//                }
//                BookTipsDlg.this.dismiss();
//                DtvService.this.bookTipsDlg = null;
//            }
//        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.book_timerevent__dialog);
            this.mTextView = (TextView) findViewById(R.id.delete_content);
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.deldia_title.setText(DtvService.this.getResources().getString(R.string.tips));
            this.mTextView.setVisibility(View.GONE);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel.setText(R.string.cancle_book);
            String sType1 = DtvService.this.getResources().getString(R.string.Play);
            if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_OK) {
                sType1 = DtvService.this.getResources().getString(R.string.Play);
            } else if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_LOCK) {
                sType1 = DtvService.this.getResources().getString(R.string.Record);
            } else if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_NO_AUDIO) {
                sType1 = DtvService.this.getResources().getStringArray(R.array.timer_type)[DtvService.PLAY_STATUS_LOCK];
            }
            String string = DtvService.this.getResources().getString(R.string.play_right_now);
            Object[] objArr = new Object[DtvService.PLAY_STATUS_OK];
            objArr[DtvService.PLAY_STATUS_UNKNOW] = sType1;
            this.mBtnStart.setText(String.format(string, objArr));
            this.time = (TextView) findViewById(R.id.time);
            this.channel_name = (TextView) findViewById(R.id.channel_name);
            this.program_name = (TextView) findViewById(R.id.program_name);
            if (this.mparaminfo != null) {
                this.left_time_ms = (long) (this.mparaminfo.left_time_seconds * 1000);
                String sType = DtvService.this.getResources().getString(R.string.Play);
                string = DtvService.this.getResources().getString(R.string.left_time);
                objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                objArr[DtvService.PLAY_STATUS_UNKNOW] = Integer.valueOf(this.mparaminfo.left_time_seconds);
                objArr[DtvService.PLAY_STATUS_OK] = sType;
                String str = String.format(string, objArr);
                if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_OK) {
                    sType = DtvService.this.getResources().getString(R.string.Play);
                    string = DtvService.this.getResources().getString(R.string.left_time);
                    objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                    objArr[DtvService.PLAY_STATUS_UNKNOW] = Integer.valueOf(this.mparaminfo.left_time_seconds);
                    objArr[DtvService.PLAY_STATUS_OK] = sType;
                    str = String.format(string, objArr);
                } else if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_LOCK) {
                    sType = DtvService.this.getResources().getString(R.string.Record);
                    string = DtvService.this.getResources().getString(R.string.left_time);
                    objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                    objArr[DtvService.PLAY_STATUS_UNKNOW] = Integer.valueOf(this.mparaminfo.left_time_seconds);
                    objArr[DtvService.PLAY_STATUS_OK] = sType;
                    str = String.format(string, objArr);
                } else if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_NO_AUDIO) {
                    sType = DtvService.this.getResources().getStringArray(R.array.timer_type)[DtvService.PLAY_STATUS_LOCK];
                    string = DtvService.this.getResources().getString(R.string.powerdown_time);
                    objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                    objArr[DtvService.PLAY_STATUS_UNKNOW] = Integer.valueOf(this.mparaminfo.left_time_seconds);
                    objArr[DtvService.PLAY_STATUS_OK] = sType;
                    str = String.format(string, objArr);
                }
                this.time.setText(str);
                if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_OK || this.mparaminfo.book_type == DtvService.PLAY_STATUS_LOCK) {
                    this.channel_name.setVisibility(View.VISIBLE);
                    this.channel_name.setText(DtvService.this.getResources().getString(R.string.channel_name) + " \uff1a " + this.mparaminfo.service_name);
                    this.program_name.setText(DtvService.this.getResources().getString(R.string.Mode) + " : " + DtvService.this.getResources().getStringArray(R.array.timer_mode)[this.mparaminfo.book_mode]);
                } else {
                    this.channel_name.setVisibility(View.GONE);
                    this.program_name.setText(DtvService.this.getResources().getString(R.string.Mode) + " : " + DtvService.this.getResources().getStringArray(R.array.timer_mode)[this.mparaminfo.book_mode]);
                }
            } else {
                System.out.println("mparaminfo is null");
            }
            Log.e(DtvService.TAG, "book id fdf df af ");
//            this.mBtnCancel.setOnClickListener(new C00591());
//            this.mBtnStart.setOnClickListener(new C00602());
        }

        public void setMyText(long left_time_s) {
            String sType = DtvService.this.getResources().getString(R.string.Play);
            String string = DtvService.this.getResources().getString(R.string.left_time);
            Object[] objArr = new Object[DtvService.PLAY_STATUS_LOCK];
            objArr[DtvService.PLAY_STATUS_UNKNOW] = Long.valueOf(left_time_s);
            objArr[DtvService.PLAY_STATUS_OK] = sType;
            String str = String.format(string, objArr);
            if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_OK) {
                sType = DtvService.this.getResources().getString(R.string.Play);
                string = DtvService.this.getResources().getString(R.string.left_time);
                objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                objArr[DtvService.PLAY_STATUS_UNKNOW] = Long.valueOf(left_time_s);
                objArr[DtvService.PLAY_STATUS_OK] = sType;
                str = String.format(string, objArr);
            } else if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_LOCK) {
                SETTINGS.bBookRec = true;
                sType = DtvService.this.getResources().getString(R.string.Record);
                string = DtvService.this.getResources().getString(R.string.left_time);
                objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                objArr[DtvService.PLAY_STATUS_UNKNOW] = Long.valueOf(left_time_s);
                objArr[DtvService.PLAY_STATUS_OK] = sType;
                str = String.format(string, objArr);
            } else if (this.mparaminfo.book_type == DtvService.PLAY_STATUS_NO_AUDIO) {
                sType = DtvService.this.getResources().getStringArray(R.array.timer_type)[DtvService.PLAY_STATUS_LOCK];
                string = DtvService.this.getResources().getString(R.string.powerdown_time);
                objArr = new Object[DtvService.PLAY_STATUS_LOCK];
                objArr[DtvService.PLAY_STATUS_UNKNOW] = Long.valueOf(left_time_s);
                objArr[DtvService.PLAY_STATUS_OK] = sType;
                str = String.format(string, objArr);
            }
            this.time.setText(str);
        }

        public BookTipsDlg(Context context, int theme, book_callback_event_data param) {
            super(context, theme);
            this.mContext = null;
            this.mLayoutView = null;
            this.mTextView = null;
            this.deldia_title = null;
            this.mparaminfo = null;
            this.time = null;
            this.channel_name = null;
            this.program_name = null;
            this.left_time_ms = 0;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
            this.mparaminfo = param;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case DtvService.PLAY_STATUS_NO_CHANNEL /*4*/:
                        if (DtvService.this.bookTipsDlg != null) {
                            DtvService.this.bookTipsDlg.dismiss();
                        }
                        return true;
                }
            }
            return false;
        }
    }

    public static native int register_dtv_service_cb(Object obj);

    public DtvService() {
        this.g_paramsInfo = null;
        this.date = new date();
        this.dateTime = new date_time();
        this.tmpBookEvent = new book_event_data();
        this.serviceInfo = new service();
        this.epgCurrentEvent = new epg_pf_event();
        this.epgNextEvent = new epg_pf_event();
        this.bookTipsDlg = null;
        this.counterTimer = null;
        this.playRet = PLAY_STATUS_UNKNOW;
        this.count = PLAY_STATUS_UNKNOW;
        this.mPlayerReceiver = new C00572();
        this.mBookReceiver = new C00583();
    }

//    static {
//        System.loadLibrary("th_dtv_pi");
//        System.loadLibrary("th_dtv_other");
//        System.loadLibrary("th_dtv_external_c");
//        System.loadLibrary("th_dtv_mw");
//    }

    private void ShowBookTipsDialog(Context context, book_callback_event_data param) {
        if (this.bookTipsDlg == null) {
            this.bookTipsDlg = new BookTipsDlg(context, R.style.MyDialog, param);
            this.bookTipsDlg.getWindow().setType(2003);
            startCountDownTimer((long) (param.left_time_seconds * 1000));
        } else {
            Log.d(TAG, "bookTipsDlg is not null");
        }
        this.bookTipsDlg.show();
    }

    private void startCountDownTimer(long left_time_ms) {
        this.counterTimer = new C00561(left_time_ms, 1000);
        this.counterTimer.start();
        Log.d(TAG, "startCountDownTimer");
    }

    private boolean isDvbPlay(String app) {
        List<RunningTaskInfo> appTask = ((ActivityManager) getSystemService("activity")).getRunningTasks(PLAY_STATUS_OK);
        if (appTask != null && appTask.size() > 0) {
            Log.d(TAG, "#### package = " + ((RunningTaskInfo) appTask.get(PLAY_STATUS_UNKNOW)).topActivity.toString());
            if (((RunningTaskInfo) appTask.get(PLAY_STATUS_UNKNOW)).topActivity.toString().contains(app)) {
                return true;
            }
        }
        return false;
    }

    private void gotoPlayActivity() {
        SETTINGS.set_green_led(false);
        try {
            Intent intent = new Intent();
            if (isDvbPlay("th.dtv.DtvMainActivity")) {
                intent.setComponent(new ComponentName("th.dtv", "th.dtv.DtvMainActivity"));
            } else {
                intent.setComponent(new ComponentName("th.dtv", "th.dtv.BlankActivity"));
            }
            if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                int current_service_type = this.serviceInfo.service_type;
                int current_service_index = this.serviceInfo.service_index;
                intent.putExtra("rec_pre_service_type", current_service_type);
                intent.putExtra("rec_service_Index", current_service_index);
                Log.i(TAG, "@@@@@@@@@ current_service_type = " + current_service_type + " current_service_index = " + current_service_index);
            } else {
                Log.i(TAG, "############db_get_current_service_info fail!!!!!!!!!!!!");
            }
            intent.addFlags(268435456);
            intent.addFlags(67108864);
            intent.putExtra("book_type", this.g_paramsInfo.book_type);
            intent.putExtra("book_service_type", this.g_paramsInfo.service_type);
            intent.putExtra("book_service_Index", this.g_paramsInfo.service_index);
            intent.putExtra("book_record_time", this.g_paramsInfo.duration_seconds);
            intent.putExtra("is_all_region", this.g_paramsInfo.is_all_region_service_index);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.d(TAG, "ActivityNotFoundException ");
        }
    }

    private void PowerDown() {
        if (SETTINGS.getPlatformType() == 0) {
            Intent intent = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
            intent.putExtra("android.intent.extra.KEY_CONFIRM", false);
            intent.setFlags(268435456);
            startActivity(intent);
        } else if (SETTINGS.getPlatformType() == PLAY_STATUS_OK) {
            try {
                Runtime.getRuntime().exec("input keyevent POWER");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onMwEventCallback(int event_type, int sub_event_type, int param1, int param2, Object obj_clazz) {
        Log.e(TAG, " mwEventCallback : " + event_type + " " + sub_event_type);
        switch (event_type) {
            case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                switch (sub_event_type) {
                    case PLAY_STATUS_UNKNOW /*0*/:
                    case PLAY_STATUS_OK /*1*/:
                        book_callback_event_data paramsInfo = (book_callback_event_data) obj_clazz;
                        if (paramsInfo != null) {
                            this.g_paramsInfo = paramsInfo;
                            Log.e(TAG, " Book : book_type = " + paramsInfo.book_type);
                            int Ret;
                            if (paramsInfo.book_type == PLAY_STATUS_NO_AUDIO) {
                                if (sub_event_type == 0) {
                                    Log.e(TAG, " Book : Action ");
                                    Ret = MW.book_handle_event_by_index(paramsInfo.event_index);
                                    if (Ret == 16) {
                                        Log.e(TAG, " Book : handle event failed ");
                                    } else if (Ret == 0) {
                                        PowerDown();
                                    } else {
                                        Log.e(TAG, " Book : unknow ");
                                    }
                                } else if (sub_event_type == PLAY_STATUS_OK) {
                                    Log.e(TAG, " Book : Notify ");
                                    sendBroadcast(new Intent(SHOW_DIALOG));
                                }
                            } else if (paramsInfo.book_type != PLAY_STATUS_NO_CHANNEL) {
                                if (paramsInfo.book_type == PLAY_STATUS_LOCK && SETTINGS.get_RTC_flag()) {
                                    SETTINGS.set_RTC_flag(false);
                                    if (paramsInfo.left_time_seconds > 15) {
                                        SETTINGS.bFromRTC = false;
                                    } else {
                                        SETTINGS.bFromRTC = true;
                                    }
                                }
                                if (this.tmpServiceInfo == null) {
                                    this.tmpServiceInfo = new service();
                                }
                                if (MW.db_get_service_info(paramsInfo.service_type, paramsInfo.service_index, this.tmpServiceInfo) != 0) {
                                    Log.d(TAG, "MW.db_get_service_info no success");
                                } else if (sub_event_type == 0) {
                                    Log.e(TAG, " Book : Action ");
                                    Ret = MW.book_handle_event_by_index(paramsInfo.event_index);
                                    if (Ret == 16) {
                                        Log.e(TAG, " Book : handle event failed ");
                                    } else if (Ret == 0) {
                                        gotoPlayActivity();
                                    } else {
                                        Log.e(TAG, " Book : unknow ");
                                    }
                                } else if (sub_event_type == PLAY_STATUS_OK) {
                                    Log.e(TAG, " Book : Notify ");
                                    sendBroadcast(new Intent(SHOW_DIALOG));
                                }
                                Log.e(TAG, " Book : book_type = " + (paramsInfo.book_type == PLAY_STATUS_OK ? "Play" : "Record"));
                                Log.e(TAG, " Book : event_index = " + paramsInfo.event_index);
                                Log.e(TAG, " Book : service_name = " + paramsInfo.service_name);
                                Log.e(TAG, " Book : event_name = " + paramsInfo.event_name);
                                Log.e(TAG, " Book : left_time_seconds = " + paramsInfo.left_time_seconds);
                            } else if (sub_event_type == 0) {
                                Log.e(TAG, " Book : Action ");
                                Ret = MW.book_handle_event_by_index(paramsInfo.event_index);
                                if (Ret == 16) {
                                    Log.e(TAG, " Book : handle event failed ");
                                } else if (Ret != 0) {
                                    Log.e(TAG, " Book : unknow ");
                                }
                            } else if (sub_event_type == PLAY_STATUS_OK) {
                                Log.e(TAG, " Book : Notify ");
                            }
                        }
                    default:
                }
            default:
        }
    }

    public void onCreate() {
        super.onCreate();
        SETTINGS.init(getApplicationContext(), false);
        register_dtv_service_cb(this);
        IntentFilter filter_mute = new IntentFilter();
        filter_mute.addAction(DTV_PLAYER_ACTION);
        filter_mute.addAction(DTV_STOP_ACTION);
        filter_mute.addAction(DTV_PLAY_STATUS_ACTION);
        filter_mute.addAction(DTV_PASSWORD_PASS_ACTION);
        filter_mute.addAction("android.intent.action.SCREEN_OFF");
        filter_mute.addAction(DTV_UPDATE_TIMEZONE_ACTION);
        filter_mute.addAction(DTV_SERVICE_STOP);
        filter_mute.setPriority(999);
        registerReceiver(this.mPlayerReceiver, filter_mute);
        IntentFilter bookevent = new IntentFilter();
        bookevent.addAction(SHOW_DIALOG);
        registerReceiver(this.mBookReceiver, bookevent);
        Log.e(TAG, " onCreate ");
    }

    public IBinder onBind(Intent arg0) {
        Log.e(TAG, " onBind ");
        return null;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.e(TAG, " onRebind ");
    }

    public boolean onUnbind(Intent intent) {
        Log.e(TAG, " onUnbind ");
        return super.onUnbind(intent);
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.e(TAG, " onStart ");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, " onStartCommand ");
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        this.threadDisable = true;
        unregisterReceiver(this.mPlayerReceiver);
        unregisterReceiver(this.mBookReceiver);
        Log.e(TAG, " onDestroy ");
    }

    private void setRTCAlarm() {
        int book_get_event_count = MW.book_get_event_count();
        Log.e(TAG, "setRTCAlarm book event count = " + book_get_event_count);
        if (book_get_event_count > 0) {
            long UTC;
            long j;
            if (MW.book_get_event_by_index(PLAY_STATUS_UNKNOW, this.tmpBookEvent) == 0) {
                UTC = Date.UTC(this.tmpBookEvent.start_year, this.tmpBookEvent.start_month, this.tmpBookEvent.start_day, this.tmpBookEvent.start_hour, this.tmpBookEvent.start_minute, PLAY_STATUS_UNKNOW) / 1000;
            } else {
                UTC = 0;
            }
            Log.d(TAG, "setRTCAlarm start " + UTC);
            if (MW.get_date(PLAY_STATUS_UNKNOW, null, this.date) == 0) {
            }
            if (MW.get_date_time(this.dateTime) == 0) {
            }
            if (this.date == null && this.dateTime == null) {
                j = UTC;
            } else {
                j = Date.UTC(this.date.year, this.date.month, this.date.day, this.dateTime.hour, this.dateTime.minute, this.dateTime.second) / 1000;
            }
            Log.d(TAG, "setRTCAlarm current_time " + j);
            if (UTC - j > 70 || this.tmpBookEvent.book_type == PLAY_STATUS_NO_CHANNEL) {
                long currentTimeMillis;
                if (this.tmpBookEvent.book_type == PLAY_STATUS_NO_CHANNEL) {
                    currentTimeMillis = (System.currentTimeMillis() / 1000) + (UTC - j);
                } else {
                    currentTimeMillis = ((System.currentTimeMillis() / 1000) + (UTC - j)) - 70;
                }
                if (this.tmpBookEvent.book_type == PLAY_STATUS_LOCK) {
                    SETTINGS.set_RTC_flag(true);
                }
                Log.d(TAG, "setRTCAlarm jiange " + (UTC - j));
                ((AlarmManager) getSystemService("alarm")).set(PLAY_STATUS_UNKNOW, currentTimeMillis * 1000, PendingIntent.getBroadcast(this, PLAY_STATUS_UNKNOW, new Intent(this, AlarmReceiver.class), PLAY_STATUS_UNKNOW));
            }
        }
    }

    private void CheckParentLockAndShowPasswd(int status) {
        Log.d(TAG, "CheckParentLockAndShowPasswd");
        if (MW.epg_get_pf_event(this.serviceInfo, this.epgCurrentEvent, this.epgNextEvent) != 0) {
            Log.d(TAG, "CheckParentLockAndShowPasswd           1111111");
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, status);
        } else if (this.epgCurrentEvent.parental_rating + PLAY_STATUS_NO_CHANNEL <= SETTINGS.get_parental_rating_value()) {
            Log.d(TAG, "CheckParentLockAndShowPasswd           222222");
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, status);
        } else if (SETTINGS.get_parental_rating_value() != 19 && !SETTINGS.bPass) {
            MW.ts_player_stop_play();
            this.playRet = 44;
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, PLAY_STATUS_LOCK);
        }
    }

    private void CheckChannelLockAndShowPasswd(int status) {
        if (MW.db_get_current_service_info(this.serviceInfo) != 0) {
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, PLAY_STATUS_UNKNOW);
            Log.d(TAG, "get current service info failed");
        } else if (!SETTINGS.get_channel_lock_onoff()) {
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, status);
        } else if (this.playRet == 44 && SETTINGS.bPass) {
            Log.d(TAG, "lock pass try play");
            this.playRet = MW.ts_player_play_current(true, true);
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, status);
        } else if (this.playRet == 0) {
            CheckParentLockAndShowPasswd(status);
        } else if (this.playRet == 44 && !SETTINGS.bPass) {
            SETTINGS.putSettingsProviderInt(DTV_PLAY_STATUS_ACTION, PLAY_STATUS_LOCK);
        }
    }

    public static boolean checkNextBookEventWillCome() {
        long start = 0;
        date date = new date();
        date_time dateTime = new date_time();
        book_event_data tmpBookEvent = new book_event_data();
        int bookEventCount = MW.book_get_event_count();
        Log.d(TAG, "checkNextBookEventWillCome book event count = " + bookEventCount);
        if (bookEventCount <= 0) {
            return false;
        }
        int i = PLAY_STATUS_UNKNOW;
        while (i < bookEventCount) {
            if (MW.book_get_event_by_index(i, tmpBookEvent) == 0 && (tmpBookEvent.book_type == PLAY_STATUS_OK || tmpBookEvent.book_type == PLAY_STATUS_LOCK)) {
                start = Date.UTC(tmpBookEvent.start_year, tmpBookEvent.start_month, tmpBookEvent.start_day, tmpBookEvent.start_hour, tmpBookEvent.start_minute, PLAY_STATUS_UNKNOW) / 1000;
                break;
            }
            i += PLAY_STATUS_OK;
        }
        if (i >= bookEventCount) {
            return false;
        }
        Log.d(TAG, "checkNextBookEventWillCome start " + start);
        if (MW.get_date(PLAY_STATUS_UNKNOW, null, date) == 0) {
        }
        long current_time;
        if (MW.get_date_time(dateTime) == 0) {
            if (date == null || dateTime != null) {
                current_time = Date.UTC(date.year, date.month, date.day, dateTime.hour, dateTime.minute, dateTime.second) / 1000;
            } else {
                current_time = start;
            }
            Log.d(TAG, "checkNextBookEventWillCome###### current_time " + current_time);
            if (start - current_time > 300) {
                return true;
            }
            return false;
        }
        if (date == null) {
        }
        current_time = Date.UTC(date.year, date.month, date.day, dateTime.hour, dateTime.minute, dateTime.second) / 1000;
        Log.d(TAG, "checkNextBookEventWillCome###### current_time " + current_time);
        if (start - current_time > 300) {
            return false;
        }
        return true;
    }
}
