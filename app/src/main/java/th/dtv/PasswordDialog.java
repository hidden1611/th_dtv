package th.dtv;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import th.dtv.activity.PswdCB;

public class PasswordDialog extends Dialog {
    private Button mBtnCancel;
    private RelativeLayout mBtnRelativeLayout;
    private Button mBtnStart;
    private Context mContext;
    private LinearLayout mContextLinearLayout;
    private EditText mEditText;
    private OnKeyListener mOnKeyListener;
    private boolean mbHandleUpDown;
    private PswdCB mcallback;

    /* renamed from: th.dtv.PasswordDialog.1 */
    class C00721 implements TextWatcher {
        C00721() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String password = PasswordDialog.this.mEditText.getText().toString();
            if (password.equals(SETTINGS.get_password()) || password.equals(SETTINGS.getSuperPassword())) {
                PasswordDialog.this.dismiss();
                if (PasswordDialog.this.mcallback != null) {
                    PasswordDialog.this.mcallback.DoCallBackEvent();
                }
            } else if (password.length() == 4) {
                PasswordDialog.this.mEditText.setText(null);
                SETTINGS.makeText(PasswordDialog.this.mContext, R.string.invalid_password, 0);
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

//    /* renamed from: th.dtv.PasswordDialog.2 */
//    class C00732 implements OnClickListener {
//        C00732() {
//        }
//
//        public void onClick(View v) {
//            PasswordDialog.this.dismiss();
//        }
//    }
//
//    /* renamed from: th.dtv.PasswordDialog.3 */
//    class C00743 implements OnClickListener {
//        C00743() {
//        }
//
//        public void onClick(View v) {
//            String password = PasswordDialog.this.mEditText.getText().toString();
//            if (password.equals(SETTINGS.get_password()) || password.equals(SETTINGS.getSuperPassword())) {
//                PasswordDialog.this.dismiss();
//                if (PasswordDialog.this.mOnKeyListener != null) {
//                    PasswordDialog.this.mOnKeyListener.onKey(PasswordDialog.this.mEditText, 23, null);
//                    return;
//                }
//                return;
//            }
//            PasswordDialog.this.mEditText.setText(null);
//            SETTINGS.makeText(PasswordDialog.this.mContext, R.string.invalid_password, 0);
//        }
//    }

    /* renamed from: th.dtv.PasswordDialog.4 */
    class C00754 implements DialogInterface.OnKeyListener {
        C00754() {
        }

        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            Log.d("PasswordDialog", "=====onKey() " + keyCode + " event.getAction() " + event.getAction());
            switch (keyCode) {
                case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                case MW.RET_TP_EXIST /*10*/:
                case MW.RET_INVALID_SAT /*11*/:
                case MW.RET_INVALID_AREA /*12*/:
                case MW.RET_INVALID_TP /*13*/:
                case MW.RET_BOOK_EVENT_INVALID /*14*/:
                case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    return false;
                default:
                    if (PasswordDialog.this.mbHandleUpDown && PasswordDialog.this.mOnKeyListener != null) {
//                        PasswordDialog.this.mOnKeyListener.onKey(PasswordDialog.this.mEditText, keyCode, event);
                    }
                    return true;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_input_dialog);
        getWindow().setSoftInputMode(3);
        this.mContextLinearLayout = (LinearLayout) findViewById(R.id.dialog_context_layout);
        this.mBtnRelativeLayout = (RelativeLayout) findViewById(R.id.dialog_button_layout);
        this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
        this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
        this.mEditText = (EditText) findViewById(R.id.dialog_edit_text);
//        this.mEditText.setBackgroundResource(17301529);
        this.mEditText.setGravity(3);
        this.mEditText.addTextChangedListener(new C00721());
        this.mBtnRelativeLayout.setVisibility(View.GONE);
        LayoutParams linearParams = (LayoutParams) this.mContextLinearLayout.getLayoutParams();
        linearParams.height = ((SETTINGS.getWindowHeight() / 720) * 60) + 40;
        this.mContextLinearLayout.setLayoutParams(linearParams);
//        this.mBtnCancel.setOnClickListener(new C00732());
//        this.mBtnStart.setOnClickListener(new C00743());
        setOnKeyListener(new C00754());
    }

    public PasswordDialog(Context context, int theme, OnKeyListener listener, PswdCB callback) {
        super(context, theme);
        this.mContext = null;
        this.mOnKeyListener = null;
        this.mContextLinearLayout = null;
        this.mBtnRelativeLayout = null;
        this.mBtnStart = null;
        this.mBtnCancel = null;
        this.mEditText = null;
        this.mbHandleUpDown = false;
        this.mcallback = null;
        this.mContext = context;
        this.mOnKeyListener = listener;
        this.mcallback = callback;
    }

    public void setHandleUpDown(boolean b) {
        this.mbHandleUpDown = b;
    }
}
