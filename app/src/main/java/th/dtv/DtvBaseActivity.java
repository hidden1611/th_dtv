package th.dtv;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;

public class DtvBaseActivity extends Activity {
    public static final int KEYCODE_0 = 7;
    public static final int KEYCODE_1 = 8;
    public static final int KEYCODE_2 = 9;
    public static final int KEYCODE_3 = 10;
    public static final int KEYCODE_4 = 11;
    public static final int KEYCODE_5 = 12;
    public static final int KEYCODE_6 = 13;
    public static final int KEYCODE_7 = 14;
    public static final int KEYCODE_8 = 15;
    public static final int KEYCODE_9 = 16;
    public static final int KEYCODE_AUDIO = 2006;
    public static final int KEYCODE_BLUE = 140;
    public static final int KEYCODE_CHANNEL_DOWN = 167;
    public static final int KEYCODE_CHANNEL_UP = 166;
    public static final int KEYCODE_DEL = 67;
    public static final int KEYCODE_DOWN = 20;
    public static final int KEYCODE_EPG = 2013;
    public static final int KEYCODE_EXIT = 4;
    public static final int KEYCODE_FAV = 27;
    public static final int KEYCODE_GREEN = 61;
    public static final int KEYCODE_INFO = 185;
    public static final int KEYCODE_LEFT = 21;
    public static final int KEYCODE_MEDIA_FAST_FORWARD = 90;
    public static final int KEYCODE_MEDIA_NEXT = 87;
    public static final int KEYCODE_MEDIA_PLAY_PAUSE = 85;
    public static final int KEYCODE_MEDIA_PREVIOUS = 88;
    public static final int KEYCODE_MEDIA_REWIND = 89;
    public static final int KEYCODE_MEDIA_STOP = 86;
    public static final int KEYCODE_MENU = 82;
    public static final int KEYCODE_MUTE = 91;
    public static final int KEYCODE_OK = 23;
    public static final int KEYCODE_PAGE_DOWN = 93;
    public static final int KEYCODE_PAGE_UP = 92;
    public static final int KEYCODE_POWER = 26;
    public static final int KEYCODE_RECALL = 666;
    public static final int KEYCODE_RECORD = 57;
    public static final int KEYCODE_RECORD_LIST = 61;
    public static final int KEYCODE_RED = 2004;
    public static final int KEYCODE_RIGHT = 22;
    public static final int KEYCODE_SAT = 55;
    public static final int KEYCODE_SUBTITLE = 2012;
    public static final int KEYCODE_TELETEXT = 2004;
    public static final int KEYCODE_TV_RADIO = 555;
    public static final int KEYCODE_UP = 19;
    public static final int KEYCODE_VOLUME_DOWN = 25;
    public static final int KEYCODE_VOLUME_UP = 24;
    public static final int KEYCODE_YELLOW = 169;
    private static final String TAG = "DtvBaseActivity";
    protected Looper looper;
    Handler mwMessageHandler;

    /* renamed from: th.dtv.DtvBaseActivity.1 */
    class C00331 implements Runnable {
        C00331() {
        }

        public void run() {
            DtvBaseActivity.this.finishMyself();
        }
    }

    public DtvBaseActivity() {
        this.looper = Looper.myLooper();
    }

    static {
        System.loadLibrary("th_dtv_pi");
        System.loadLibrary("th_dtv_other");
        System.loadLibrary("th_dtv_external_c");
        System.loadLibrary("th_dtv_mw");
    }

    protected void enableMwMessageCallback(Handler h) {
        this.mwMessageHandler = h;
        if (h != null) {
            MW.register_event_cb(this);
        }
    }

    protected void sendKeyEvent(int keyCode) {
        int eventCode = keyCode;
        long now = SystemClock.uptimeMillis();
        KeyEvent down = new KeyEvent(now, now, 0, eventCode, 0);
        KeyEvent up = new KeyEvent(now, now, 1, eventCode, 0);
//            Stub.asInterface(ServiceManager.getService("input")).injectInputEvent(down, 0);
//            Stub.asInterface(ServiceManager.getService("input")).injectInputEvent(up, 0);
    }

    public void onMwEventCallback(int event_type, int sub_event_type, int param1, int param2, Object obj_clazz) {
        if (this.mwMessageHandler != null) {
            Message messageSend = this.mwMessageHandler.obtainMessage();
            messageSend.what = (event_type << KEYCODE_9) | sub_event_type;
            messageSend.arg1 = param1;
            messageSend.arg2 = param2;
            messageSend.obj = obj_clazz;
            messageSend.sendToTarget();
        }
    }

    public void finishMyself() {
        finish();
    }

    public void finishMyself(int delayMS) {
        if (delayMS > 0) {
            new Handler().postDelayed(new C00331(), (long) delayMS);
        } else {
            finishMyself();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SETTINGS.init(getApplicationContext(), false);
        startService(new Intent(this, DtvService.class));
        MuteIconControl.startMuteIconService(this);
        Log.d(" DtvBaseActivity ", "onCreate ");
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d(" DtvBaseActivity ", "onDestroy ");
    }
}
