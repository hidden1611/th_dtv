package th.dtv;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import th.dtv.activity.PswdCB;

public class ComboLayout extends LinearLayout implements OnClickListener {
    private PswdCB mCb;
    ChooseListDialog mChooseListDialog;
    private ArrayList<String> mChooseStringList;
    private ArrayList<String> mChooseStringList_Temp;
    private Context mContext;
    private int mHighLightIndex;
    protected TextView mInfoTextView;
    protected ImageView mLeftImageView;
    private OnItemClickListener mOnClickListener;
    private OnKeyListener mOnKeyListener;
    protected ImageView mRightImageView;
    private int mSelectedId;
    private int mTitleResId;
    protected TextView mTitleTextView;

    /* renamed from: th.dtv.ComboLayout.1 */
    class C00321 implements OnItemClickListener {
        C00321() {
        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (ComboLayout.this.mOnClickListener != null) {
                ComboLayout.this.mOnClickListener.onItemClick(parent, v, position, id);
            }
            ComboLayout.this.mHighLightIndex = position;
            if (ComboLayout.this.mCb != null) {
                ComboLayout.this.mChooseStringList = ComboLayout.this.mChooseStringList_Temp;
            }
            ComboLayout.this.mInfoTextView.setText(ComboLayout.this.getInfoTextStr(ComboLayout.this.mChooseStringList, ComboLayout.this.mHighLightIndex));
            ComboLayout.this.mChooseListDialog.dismiss();
        }
    }

    public ComboLayout(Context context) {
        this(context, null);
    }

    public ComboLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mSelectedId = 0;
        this.mTitleResId = 0;
        this.mHighLightIndex = 0;
        this.mChooseStringList = null;
        this.mChooseStringList_Temp = null;
        this.mOnKeyListener = null;
        this.mCb = null;
        this.mContext = context;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.combo_layout, this);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mTitleTextView = (TextView) findViewById(R.id.title_textview);
        this.mTitleTextView.setOnClickListener(this);
        this.mInfoTextView = (TextView) findViewById(R.id.info_textview);
        this.mInfoTextView.setOnClickListener(this);
        this.mLeftImageView = (ImageView) findViewById(R.id.left_imageview);
        this.mLeftImageView.setOnClickListener(this);
        this.mRightImageView = (ImageView) findViewById(R.id.right_imageview);
        this.mRightImageView.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == this.mLeftImageView) {
            if (this.mChooseStringList != null) {
                if (this.mHighLightIndex > 0) {
                    this.mHighLightIndex--;
                } else {
                    this.mHighLightIndex = this.mChooseStringList.size() - 1;
                }
                this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, this.mHighLightIndex));
                if (this.mOnClickListener != null) {
                    this.mOnClickListener.onItemClick(null, this.mInfoTextView, this.mHighLightIndex, 0);
                }
            }
        } else if (v == this.mRightImageView) {
            if (this.mChooseStringList != null) {
                if (this.mHighLightIndex < this.mChooseStringList.size() - 1) {
                    this.mHighLightIndex++;
                } else {
                    this.mHighLightIndex = 0;
                }
                this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, this.mHighLightIndex));
                if (this.mOnClickListener != null) {
                    this.mOnClickListener.onItemClick(null, this.mInfoTextView, this.mHighLightIndex, 0);
                }
            }
        } else if (v == this.mInfoTextView || v == this.mTitleTextView) {
            ShowInfoDialog();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                if (this.mChooseStringList != null) {
                    if (this.mHighLightIndex > 0) {
                        this.mHighLightIndex--;
                    } else {
                        this.mHighLightIndex = this.mChooseStringList.size() - 1;
                    }
                    if (this.mCb != null) {
                        this.mChooseStringList = this.mChooseStringList_Temp;
                    }
                    this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, this.mHighLightIndex));
                    if (this.mOnClickListener != null) {
                        this.mOnClickListener.onItemClick(null, this.mInfoTextView, this.mHighLightIndex, 0);
                    }
                }
                return true;
            case MW.RET_MEMORY_ERROR /*22*/:
                if (this.mChooseStringList != null) {
                    if (this.mHighLightIndex < this.mChooseStringList.size() - 1) {
                        this.mHighLightIndex++;
                    } else {
                        this.mHighLightIndex = 0;
                    }
                    if (this.mCb != null) {
                        this.mChooseStringList = this.mChooseStringList_Temp;
                    }
                    this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, this.mHighLightIndex));
                    if (this.mOnClickListener != null) {
                        this.mOnClickListener.onItemClick(null, this.mInfoTextView, this.mHighLightIndex, 0);
                    }
                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case MW.RET_INVALID_TYPE /*23*/:
            case 66:
                if (this.mOnKeyListener != null) {
                    this.mOnKeyListener.onKey(this.mInfoTextView, keyCode, event);
                } else {
                    if (this.mCb != null) {
                        this.mCb.DoCallBackEvent();
                    }
                    ShowInfoDialog();
                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public TextView getTitleTextView() {
        return this.mTitleTextView;
    }

    public TextView getInfoTextView() {
        return this.mInfoTextView;
    }

    public ImageView getLeftImageView() {
        return this.mLeftImageView;
    }

    public ImageView getRightImageView() {
        return this.mRightImageView;
    }

    public int getHighlightIndex() {
        return this.mHighLightIndex;
    }

    public void initView(int titleResId, int textResId, int highlightIndex, OnItemClickListener listener) {
        this.mTitleResId = titleResId;
        this.mHighLightIndex = highlightIndex;
        this.mOnClickListener = listener;
        if (textResId > 0) {
            this.mChooseStringList = new ArrayList();
            for (String secStr : this.mContext.getResources().getStringArray(textResId)) {
                this.mChooseStringList.add(secStr);
            }
        }
        if (this.mTitleResId > 0) {
            this.mTitleTextView.setText(this.mTitleResId);
        }
        if (this.mChooseStringList != null) {
            this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, highlightIndex));
        }
    }

    public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, OnItemClickListener listener) {
        this.mTitleResId = titleResId;
        this.mChooseStringList = textList;
        this.mHighLightIndex = highlightIndex;
        this.mOnClickListener = listener;
        this.mOnKeyListener = null;
        if (this.mTitleResId > 0) {
            this.mTitleTextView.setText(this.mTitleResId);
        }
        this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, highlightIndex));
    }

    public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, OnItemClickListener listener, PswdCB cb) {
        this.mTitleResId = titleResId;
        this.mChooseStringList = textList;
        this.mHighLightIndex = highlightIndex;
        this.mOnClickListener = listener;
        this.mOnKeyListener = null;
        this.mCb = cb;
        this.mChooseStringList_Temp = this.mChooseStringList;
        if (this.mTitleResId > 0) {
            this.mTitleTextView.setText(this.mTitleResId);
        }
        this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, highlightIndex));
    }

    public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, OnKeyListener keylistener) {
        this.mTitleResId = titleResId;
        this.mChooseStringList = textList;
        this.mHighLightIndex = highlightIndex;
        this.mOnKeyListener = keylistener;
        this.mOnClickListener = null;
        if (this.mTitleResId > 0) {
            this.mTitleTextView.setText(this.mTitleResId);
        }
        this.mInfoTextView.setText(getInfoTextStr(this.mChooseStringList, highlightIndex));
    }

    private String getInfoTextStr(List<String> textList, int index) {
        if (textList == null || textList.size() <= 0) {
            return "";
        }
        if (index >= textList.size()) {
            index = 0;
        }
        return (String) textList.get(index);
    }

    void ShowInfoDialog() {
        if (this.mChooseStringList != null) {
            this.mChooseListDialog = new ChooseListDialog(this.mContext, this.mTitleResId, this.mChooseStringList, this.mHighLightIndex, new C00321());
            this.mChooseListDialog.show();
        } else if (this.mOnClickListener != null) {
            this.mOnClickListener.onItemClick(null, null, 0, 0);
        }
    }

    public void UpdateInfotext(ArrayList<String> textList) {
        this.mChooseStringList = textList;
    }
}
