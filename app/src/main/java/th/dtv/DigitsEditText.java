package th.dtv;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class DigitsEditText extends EditText {
    public DigitsEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
//        setShowSoftInputOnFocus(false);
    }

    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService("input_method");
        if (imm != null && imm.isActive(this)) {
            imm.hideSoftInputFromWindow(getApplicationWindowToken(), 0);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean ret = super.onTouchEvent(event);
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService("input_method");
        if (imm != null && imm.isActive(this)) {
            imm.hideSoftInputFromWindow(getApplicationWindowToken(), 0);
        }
        return ret;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("DigitsEditText", " onKeyDown " + keyCode);
        switch (keyCode) {
            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                if (event.getRepeatCount() == 0) {
                    break;
                }
                return true;
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                if (getSelectionStart() > 0) {
                    getText().delete(getSelectionStart() - 1, getSelectionStart());
                    return true;
                }
                break;
            case MW.RET_INVALID_TYPE /*23*/:
            case 66:
                if (hasSelection()) {
                    setSelection(length());
                    return true;
                }
                selectAll();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d("DigitsEditText", " onKeyUp " + keyCode);
        switch (keyCode) {
            case MW.RET_INVALID_TYPE /*23*/:
            case 66:
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent event) {
        if (event.getEventType() == 16) {
            int added = event.getAddedCount();
            int removed = event.getRemovedCount();
            int length = event.getBeforeText().length();
            if (added > removed) {
                event.setRemovedCount(0);
                event.setAddedCount(1);
                event.setFromIndex(length);
            } else if (removed > added) {
                event.setRemovedCount(1);
                event.setAddedCount(0);
                event.setFromIndex(length - 1);
            } else {
                return;
            }
        } else if (event.getEventType() == 8) {
            return;
        }
        super.sendAccessibilityEventUnchecked(event);
    }

    public void setDigitsText(CharSequence text) {
        super.setText(text);
        setSelection(length());
    }
}
