package th.dtv.util;

public class SmartDataInfo {
    public int connectStatus;
    public int enabled;
    public int index;
    public String key;
    public String label;
    public String passwd;
    public int protocol;
    public String servername;
    public int serverport;
    public String username;
}
