package th.dtv.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CfgFile {
    private File cfg_file;
    private List<SmartDataInfo> db_list;

    public CfgFile(File file) {
        this.db_list = null;
        this.cfg_file = null;
        if (this.db_list == null) {
            this.db_list = new ArrayList();
        }
        this.cfg_file = file;
        importDataFromCfgFile(this.cfg_file);
    }

    public boolean importDataFromCfgFile(File file) {
        boolean ret = true;
        this.cfg_file = file;
        if (this.db_list != null) {
            this.db_list.clear();
        }
        try {
            FileReader fr = new FileReader(this.cfg_file);
            BufferedReader br = new BufferedReader(fr);
            for (String cfgData = br.readLine(); cfgData != null; cfgData = br.readLine()) {
                if (!parseCfgData(cfgData)) {
                    ret = false;
                    break;
                }
                System.out.println(cfgData);
            }
            fr.close();
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return getCount() > 0 ? ret : false;
    }

    public List<SmartDataInfo> getAllCfgInfo() {
        return this.db_list;
    }

    private boolean parseCfgData(String cfgData) {
        String mcfgData = cfgData.trim();
        if (mcfgData.equals("")) {
            return true;
        }
        String[] data = mcfgData.split("\\s+");
        if (data.length != 6 && data.length != 6) {
            return false;
        }
        SmartDataInfo cfgInfo = new SmartDataInfo();
        if (cfgInfo == null) {
            return true;
        }
        if (data[0].equals("C:")) {
            cfgInfo.protocol = 0;
            if (data.length != 6) {
                return false;
            }
        } else if (!data[0].equals("N:")) {
            return false;
        } else {
            cfgInfo.protocol = 1;
            if (data.length != 6) {
                return false;
            }
        }
        cfgInfo.servername = data[1];
        cfgInfo.label = cfgInfo.servername;
        cfgInfo.serverport = Integer.parseInt(data[2]);
        cfgInfo.username = data[3];
        cfgInfo.passwd = data[4];
        cfgInfo.enabled = 1;
        if (cfgInfo.protocol == 1) {
            cfgInfo.key = data[5];
            if (cfgInfo.key.length() != 28) {
                return false;
            }
        }
        cfgInfo.index = this.db_list.size();
        this.db_list.add(cfgInfo);
        return true;
    }

    public int getCount() {
        if (this.db_list != null) {
            return this.db_list.size();
        }
        return 0;
    }

    public SmartDataInfo getItemByIndex(int index) {
        if (this.db_list == null || index >= this.db_list.size()) {
            return null;
        }
        return (SmartDataInfo) this.db_list.get(index);
    }

    public void addItem(int index, SmartDataInfo cfgInfo) {
        if (this.db_list != null && index < this.db_list.size() + 1) {
            this.db_list.add(index, cfgInfo);
        }
    }

    public void updateItem(int index, SmartDataInfo cfgInfo) {
        if (this.db_list != null && index < this.db_list.size()) {
            this.db_list.set(index, cfgInfo);
        }
    }

    public void deleteItem(int index) {
        if (this.db_list != null && index < this.db_list.size()) {
            this.db_list.remove(index);
        }
    }

    public void saveAllCfgInfoToFile(File file) {
        String str = "";
        if (this.db_list != null) {
            if (this.db_list.size() == 0) {
                str = "";
            }
            for (int i = 0; i < this.db_list.size(); i++) {
                str = str + parseSmartInfo((SmartDataInfo) this.db_list.get(i));
            }
            try {
                FileWriter fw = new FileWriter(file);
                BufferedWriter writer = new BufferedWriter(fw);
                writer.write(str);
                writer.flush();
                writer.close();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String parseSmartInfo(SmartDataInfo smInfo) {
        String str = "";
        String padding_status = "";
        if (smInfo == null) {
            return "";
        }
        if (smInfo.protocol == 0) {
            str = "C: ";
            padding_status = "yes";
        } else if (smInfo.protocol == 1) {
            str = "N: ";
            padding_status = smInfo.key;
        } else {
            str = "UNKNOWN";
        }
        return str + smInfo.servername + " " + smInfo.serverport + " " + smInfo.username + " " + smInfo.passwd + " " + padding_status + "\r\n";
    }
}
