package th.dtv.util;

import th.dtv.MW;

public class HongKongDTMBHD {
    public static HongKongDTMBHD[] hongKongDTMBHD;
    public int mAudioPid;
    public int mVideoPid;

    static {
        hongKongDTMBHD = new HongKongDTMBHD[]{new HongKongDTMBHD(121, 122), new HongKongDTMBHD(311, 312), new HongKongDTMBHD(331, 332), new HongKongDTMBHD(321, 322), new HongKongDTMBHD(811, 812), new HongKongDTMBHD(821, 822), new HongKongDTMBHD(831, 832), new HongKongDTMBHD(841, 842), new HongKongDTMBHD(851, 852), new HongKongDTMBHD(MW.TUNER_TYPE_DVB_S2, 1001), new HongKongDTMBHD(MW.TUNER_TYPE_ABS_S, 1002), new HongKongDTMBHD(841, 842)};
    }

    public HongKongDTMBHD(int mVideoPid, int mAudioPid) {
        this.mVideoPid = mVideoPid;
        this.mAudioPid = mAudioPid;
    }

    public static boolean isHongKongDTMBHDChannel(int videoPid, int audioPid) {
        boolean bHDChannel = false;
        int i = 0;
        while (i < hongKongDTMBHD.length) {
            if (hongKongDTMBHD[i].mVideoPid == videoPid && hongKongDTMBHD[i].mAudioPid == audioPid) {
                bHDChannel = true;
            }
            i++;
        }
        return bHDChannel;
    }
}
