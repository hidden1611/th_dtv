package th.dtv.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DtvDbHelper extends SQLiteOpenHelper {
    private static DtvDbHelper mInstance;

    static {
        mInstance = null;
    }

    private DtvDbHelper(Context context) {
        super(context, "dtv.db", null, 3);
    }

    public static DtvDbHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DtvDbHelper(context);
        }
        return mInstance;
    }

    public void onCreate(SQLiteDatabase db) {
        Log.e("DtvDbHelper", "create table if not exists biss_info (num Integer, freq Integer, ssid text, key text )");
        db.execSQL("create table if not exists biss_info (num Integer, freq Integer, ssid text, key text )");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS smart_info");
        db.execSQL("DROP TABLE IF EXISTS biss_info");
        onCreate(db);
    }

    public boolean importDB(String fromDbPath, String toDbPath) throws IOException, FileNotFoundException {
        return copyDb(fromDbPath, toDbPath, "dtv.db", null);
    }

    public boolean exportDB(String fromDbPath, String toDbPath) throws IOException, FileNotFoundException {
        return copyDb(fromDbPath, toDbPath, "dtv.db", null);
    }

    private boolean copyDb(String fromDbPath, String toDbPath, String fromDBName, String toDBName) throws IOException {
        if (fromDBName == null) {
            Log.e("DtvDbHelper", "FileName is NULL!");
            return false;
        } else if (new File(fromDbPath).exists()) {
            if (toDBName == null) {
                toDBName = fromDBName;
            }
            File destFile = new File(toDbPath);
            if (!destFile.exists()) {
                destFile.mkdir();
            }
            InputStream is = new FileInputStream(fromDbPath + fromDBName);
            OutputStream os = new FileOutputStream(toDbPath + toDBName);
            byte[] buffer = new byte[1024];
            while (true) {
                int length = is.read(buffer);
                if (length <= 0) {
                    break;
                }
                os.write(buffer, 0, length);
            }
            os.flush();
            ((FileOutputStream) os).getFD().sync();
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
            return true;
        } else {
            Log.e("DtvDbHelper", "File Path Not Found!");
            return false;
        }
    }
}
