package th.dtv.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;

public class BissDataHelper {
    private static BissDataHelper mInstance;
    private DtvDbHelper dbHelper;

    static {
        mInstance = null;
    }

    private BissDataHelper(Context context) {
        this.dbHelper = DtvDbHelper.getInstance(context);
    }

    public static BissDataHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new BissDataHelper(context);
        }
        return mInstance;
    }

    public void insert(BissDataInfo bissDataInfo) {
        System.out.println("insert  :::" + bissDataInfo.index);
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        ContentValues insertValues = new ContentValues();
        insertValues.clear();
        insertValues.put("num", Integer.valueOf(bissDataInfo.index));
        insertValues.put("freq", Integer.valueOf(bissDataInfo.freq));
        insertValues.put("ssid", bissDataInfo.ssid);
        insertValues.put("key", bissDataInfo.key);
        db.insert("biss_info", null, insertValues);
    }

    public void delete(ArrayList<Integer> indexList) {
        if (indexList != null) {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            String[] arrayOfString = new String[1];
            for (int i = 0; i < indexList.size(); i++) {
                arrayOfString[0] = Integer.toString(((Integer) indexList.get(i)).intValue());
                System.out.println("delete index :::: " + arrayOfString[0]);
                db.delete("biss_info", "num=?", arrayOfString);
            }
            String[] arrayOfString1 = new String[1];
            BissDataInfo bissDataInfo = new BissDataInfo();
            Cursor cursor = db.query("biss_info", null, null, null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                bissDataInfo.index = cursor.getInt(cursor.getColumnIndex("num"));
                bissDataInfo.freq = cursor.getInt(cursor.getColumnIndex("freq"));
                bissDataInfo.ssid = cursor.getString(cursor.getColumnIndex("ssid"));
                bissDataInfo.key = cursor.getString(cursor.getColumnIndex("key"));
                System.out.println("pos :::" + cursor.getPosition());
                if (bissDataInfo.index != cursor.getPosition()) {
                    int temp = bissDataInfo.index;
                    bissDataInfo.index = cursor.getPosition();
                    ContentValues updateValues = new ContentValues();
                    arrayOfString1[0] = Integer.toString(temp);
                    updateValues.clear();
                    updateValues.put("num", Integer.valueOf(bissDataInfo.index));
                    updateValues.put("freq", Integer.valueOf(bissDataInfo.freq));
                    updateValues.put("ssid", bissDataInfo.ssid);
                    updateValues.put("key", bissDataInfo.key);
                    if (db.update("biss_info", updateValues, "num=?", arrayOfString1) == 0) {
                    }
                }
                cursor.moveToNext();
            }
        }
    }

    public void update(BissDataInfo bissDataInfo) {
        if (bissDataInfo == null) {
            Log.e("BissDataHelper", "biss data Info is null");
            return;
        }
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        ContentValues updateValues = new ContentValues();
        String[] arrayOfString = new String[]{Integer.toString(bissDataInfo.index)};
        updateValues.clear();
        updateValues.put("num", Integer.valueOf(bissDataInfo.index));
        updateValues.put("freq", Integer.valueOf(bissDataInfo.freq));
        updateValues.put("ssid", bissDataInfo.ssid);
        updateValues.put("key", bissDataInfo.key);
        System.out.println("update ::: " + arrayOfString);
        if (db.update("biss_info", updateValues, "num=?", arrayOfString) == 0) {
            db.insert("biss_info", null, updateValues);
        }
    }

    public BissDataInfo getBissDataInfoByIndex(int index) {
        BissDataInfo bissDataInfo;
        Cursor cursor = this.dbHelper.getReadableDatabase().query("biss_info", null, "num=?", new String[]{Integer.toString(index)}, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            bissDataInfo = new BissDataInfo();
            bissDataInfo.index = cursor.getInt(cursor.getColumnIndex("num"));
            bissDataInfo.freq = cursor.getInt(cursor.getColumnIndex("freq"));
            bissDataInfo.ssid = cursor.getString(cursor.getColumnIndex("ssid"));
            bissDataInfo.key = cursor.getString(cursor.getColumnIndex("key"));
        } else {
            bissDataInfo = null;
        }
        cursor.close();
        return bissDataInfo;
    }

    public int getCount() {
        int count;
        Cursor cursor = this.dbHelper.getReadableDatabase().query("biss_info", null, null, null, null, null, null);
        if (cursor != null) {
            count = cursor.getCount();
        } else {
            count = 0;
        }
        cursor.close();
        return count;
    }

    public BissDataInfo[] getAllBissDataInfo() {
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        String[] arraryOfString = new String[1];
        Cursor cursor = db.query("biss_info", null, null, null, null, null, null);
        if (cursor == null) {
            return null;
        }
        int totalCount = cursor.getCount();
        BissDataInfo[] bissDataInfo = new BissDataInfo[totalCount];
        for (int i = 0; i < totalCount; i++) {
            arraryOfString[0] = Integer.toString(i);
            cursor = db.query("biss_info", null, "num=?", arraryOfString, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                bissDataInfo[i] = new BissDataInfo();
                bissDataInfo[i].index = cursor.getInt(cursor.getColumnIndex("num"));
                bissDataInfo[i].freq = cursor.getInt(cursor.getColumnIndex("freq"));
                bissDataInfo[i].ssid = cursor.getString(cursor.getColumnIndex("ssid"));
                bissDataInfo[i].key = cursor.getString(cursor.getColumnIndex("key"));
            } else {
                bissDataInfo[i] = null;
            }
        }
        cursor.close();
        return bissDataInfo;
    }

    public void CloseDB() {
        this.dbHelper.getReadableDatabase().close();
    }
}
