package th.dtv.util;

import android.util.Log;
import java.util.Date;
import th.dtv.MW;

public class CheckTimeValidate {
    public static boolean validate(Date date) {
        int[] monthLengths = new int[]{0, 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        Log.e("TIMER", "date = " + date);
        if (date.getMonth() < 1 || date.getMonth() > 12) {
            Log.e("TIMER", "date.getMonth = " + date.getMonth());
            return false;
        }
        if (isLeapYear(date.getYear())) {
            monthLengths[2] = 29;
        } else {
            monthLengths[2] = 28;
        }
        int monthLength = monthLengths[date.getMonth()];
        Log.e("TIMER", "date.getDate = " + date.getDate() + ",monthLength = " + monthLength);
        if (date.getDate() >= 1 && date.getDate() <= monthLength) {
            return true;
        }
        Log.e("TIMER", "date.getDate = " + date.getDate());
        return false;
    }

    private static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % MW.TUNER_ID_ISDBT_CXD2838 == 0;
    }
}
