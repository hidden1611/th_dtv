package th.dtv.util;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class SmartCfgDataHelper {
    private static String SMART_CFG_PATH;
    private static SmartCfgDataHelper mInstance;
    public static String mSmartCfgFile;
    private CfgFile cfgFile;

    static {
        mSmartCfgFile = "CCCam.cfg";
        SMART_CFG_PATH = "/data/data/th.dtv/";
        mInstance = null;
    }

    private SmartCfgDataHelper(Context context) {
        this.cfgFile = null;
        initSmartData();
    }

    public static SmartCfgDataHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SmartCfgDataHelper(context);
        }
        return mInstance;
    }

    public boolean initSmartData() {
        File file = new File(SMART_CFG_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(SMART_CFG_PATH + mSmartCfgFile);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.cfgFile = new CfgFile(file);
        return true;
    }

    public boolean checkCfgFile(String sFilePathName) {
        File file = new File(sFilePathName);
        if (file.exists()) {
            return this.cfgFile.importDataFromCfgFile(file);
        }
        return false;
    }

    public void insert(SmartDataInfo smartDataInfo) {
        this.cfgFile.addItem(this.cfgFile.getCount(), smartDataInfo);
    }

    public void delete(int index) {
        this.cfgFile.deleteItem(index);
    }

    public void update(SmartDataInfo smartDataInfo) {
        this.cfgFile.updateItem(smartDataInfo.index, smartDataInfo);
    }

    public SmartDataInfo getSmartDataInfoByIndex(int index) {
        return this.cfgFile.getItemByIndex(index);
    }

    public int getCount() {
        return this.cfgFile.getCount();
    }

    public boolean isExisted(String label) {
        return false;
    }

    public SmartDataInfo[] getAllSamrtDataInfo() {
        List<SmartDataInfo> db_list = this.cfgFile.getAllCfgInfo();
        int totalCount = this.cfgFile.getCount();
        if (totalCount == 0) {
            return null;
        }
        SmartDataInfo[] smartDataInfo = new SmartDataInfo[totalCount];
        for (int i = 0; i < totalCount; i++) {
            smartDataInfo[i] = new SmartDataInfo();
            smartDataInfo[i].index = ((SmartDataInfo) db_list.get(i)).index;
            smartDataInfo[i].protocol = ((SmartDataInfo) db_list.get(i)).protocol;
            smartDataInfo[i].label = ((SmartDataInfo) db_list.get(i)).label;
            smartDataInfo[i].servername = ((SmartDataInfo) db_list.get(i)).servername;
            smartDataInfo[i].serverport = ((SmartDataInfo) db_list.get(i)).serverport;
            smartDataInfo[i].username = ((SmartDataInfo) db_list.get(i)).username;
            smartDataInfo[i].passwd = ((SmartDataInfo) db_list.get(i)).passwd;
            smartDataInfo[i].connectStatus = ((SmartDataInfo) db_list.get(i)).connectStatus;
            smartDataInfo[i].enabled = ((SmartDataInfo) db_list.get(i)).enabled;
            smartDataInfo[i].key = ((SmartDataInfo) db_list.get(i)).key;
        }
        return smartDataInfo;
    }

    public void saveAllSmartDatatoCfgFile() {
        this.cfgFile.saveAllCfgInfoToFile(new File(SMART_CFG_PATH + mSmartCfgFile));
    }

    public boolean importDB(String fromDbPath) throws IOException, FileNotFoundException {
        return copyDb(fromDbPath, SMART_CFG_PATH, mSmartCfgFile, null);
    }

    public boolean exportDB(String toDbPath) throws IOException, FileNotFoundException {
        return copyDb(SMART_CFG_PATH, toDbPath, mSmartCfgFile, null);
    }

    private boolean copyDb(String fromDbPath, String toDbPath, String fromDBName, String toDBName) throws IOException {
        if (fromDBName == null) {
            Log.e("SmartCfgDataHelper", "FileName is NULL!");
            return false;
        } else if (new File(fromDbPath).exists()) {
            if (toDBName == null) {
                toDBName = fromDBName;
            }
            File destFile = new File(toDbPath);
            if (!destFile.exists()) {
                destFile.mkdir();
            }
            Log.e("SmartCfgDataHelper", "src path :" + fromDbPath + fromDBName);
            InputStream is = new FileInputStream(fromDbPath + fromDBName);
            Log.e("SmartCfgDataHelper", "to path :" + toDbPath + toDBName);
            OutputStream os = new FileOutputStream(toDbPath + toDBName);
            byte[] buffer = new byte[1024];
            while (true) {
                int length = is.read(buffer);
                if (length <= 0) {
                    break;
                }
                os.write(buffer, 0, length);
            }
            os.flush();
            ((FileOutputStream) os).getFD().sync();
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
            return true;
        } else {
            Log.e("SmartCfgDataHelper", "File Path Not Found!");
            return false;
        }
    }
}
