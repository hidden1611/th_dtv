package th.dtv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    Context mContext;

    public BootReceiver() {
        this.mContext = null;
    }

    public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        String action = intent.getAction();
        Log.d("th.dtv.BootReceiver", "==== BootReceiver , action : " + action);
        if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            context.startService(new Intent("com.android.dtv.MuteIconService"));
        } else if ("android.intent.action.LOCALE_CHANGED".equals(action)) {
            MW.global_set_system_ISO_639_Language_Code(this.mContext.getResources().getConfiguration().locale.getISO3Language());
        }
    }
}
