package th.dtv;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.UserHandle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;

public class MuteIconService extends Service {
    private AudioManager mAudioManager;
    private Handler mHandler;
    private BroadcastReceiver mMuteIconReceiver;
    private Runnable mMuteIconRunnable;
    private View mView;

    /* renamed from: th.dtv.MuteIconService.1 */
    class C00701 extends BroadcastReceiver {
        C00701() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("com.dtv.mute.icon.show")) {
                MuteIconService.this.muteIconEnable(true);
            } else if (action.equals("com.dtv.mute.icon.hide")) {
                MuteIconService.this.muteIconEnable(false);
            }
        }
    }

    /* renamed from: th.dtv.MuteIconService.2 */
    class C00712 implements Runnable {
        C00712() {
        }

        public void run() {
            MuteIconService.this.GetVolumeValue();
            MuteIconService.this.mHandler.postDelayed(MuteIconService.this.mMuteIconRunnable, 200);
        }
    }

    public MuteIconService() {
        this.mView = null;
        this.mMuteIconReceiver = new C00701();
        this.mAudioManager = null;
        this.mHandler = new Handler();
        this.mMuteIconRunnable = new C00712();
    }

    public void onCreate() {
        IntentFilter filter_mute = new IntentFilter();
        filter_mute.addAction("com.dtv.mute.icon.show");
        filter_mute.addAction("com.dtv.mute.icon.hide");
        registerReceiver(this.mMuteIconReceiver, filter_mute);
        muteIconInit();
    }

    public void onDestroy() {
        this.mView.setVisibility(View.GONE);
        unregisterReceiver(this.mMuteIconReceiver);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void muteIconEnable(boolean show) {
        if (show) {
            this.mView.setVisibility(View.VISIBLE);
        } else {
            this.mView.setVisibility(View.GONE);
        }
    }

    private void muteIconInit() {
        this.mView = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.mute_icon, null);
        ((ImageView) this.mView.findViewById(R.id.mute_icon)).setImageResource(R.drawable.mute_icon);
        LayoutParams params = new LayoutParams(DtvBaseActivity.KEYCODE_AUDIO, 262144, -3);
        params.gravity = 48;
        WindowManager wm = (WindowManager) getSystemService("window");
        muteIconEnable(false);
        wm.addView(this.mView, params);
//        if (!((SystemWriteManager) getSystemService("system_write")).getPropertyString("ro.build.version.release", "0").equals("4.2.2")) {
//            initMuteMonitor();
//        }
    }

    private void initMuteMonitor() {
        this.mAudioManager = (AudioManager) getSystemService("audio");
        this.mHandler.postDelayed(this.mMuteIconRunnable, 100);
    }

    private void GetVolumeValue() {
        if (this.mAudioManager.getStreamVolume(3) == 0) {
            Intent intent = new Intent();
            intent.setAction("com.dtv.mute.icon.show");
//            sendBroadcastAsUser(intent, UserHandle.ALL);
            return;
        }
//        intent = new Intent();
//        intent.setAction("com.dtv.mute.icon.hide");
//        sendBroadcastAsUser(intent, UserHandle.ALL);
    }
}
