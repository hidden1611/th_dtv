package th.dtv;

import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DtvControl {
    private String mControlPath;
    private String mControlValue;
    private int maxValueSize;

    public DtvControl() {
        this.mControlPath = null;
        this.mControlValue = null;
        this.maxValueSize = 32;
    }

    public DtvControl(String c) {
        this.mControlPath = null;
        this.mControlValue = null;
        this.maxValueSize = 32;
        this.mControlPath = c;
    }

    public DtvControl setValue(String v) {
        setString(this.mControlPath, v, false);
        this.mControlValue = v;
        return this;
    }

    public DtvControl setValueForce(String v) {
        setString(this.mControlPath, v, true);
        this.mControlValue = v;
        return this;
    }

    public boolean setString(String c, String v, boolean force) {
        BufferedReader in;
        if (!new File(c).exists()) {
            return false;
        }
        if (!force) {
            try {
                in = new BufferedReader(new FileReader(c), this.maxValueSize);
                String vr = in.readLine();
                if (vr == null || !vr.equals(v)) {
                    in.close();
                } else {
                    Log.d("DtvControl", "control status ready, ignored");
                    in.close();
                    return true;
                }
            } catch (IOException e) {
                Log.e("DtvControl", "IOException when read " + c);
            } catch (Throwable th) {
//                in.close();
            }
        }
        BufferedWriter out;
        try {
            out = new BufferedWriter(new FileWriter(c), 32);
            out.write(v);
            Log.d("DtvControl", "control status.");
            out.close();
            return true;
        } catch (IOException e2) {
            Log.e("DtvControl", "IOException when write " + c);
            return false;
        } catch (Throwable th2) {
//            out.close();
        }
        return true;
    }
}
