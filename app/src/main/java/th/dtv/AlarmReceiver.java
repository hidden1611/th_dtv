package th.dtv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import th.dtv.mw_data.book_event_data;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;

public class AlarmReceiver extends BroadcastReceiver {
    private static WakeLock wakeLock;
    private date date;
    private date_time dateTime;
    private book_event_data tmpBookEvent;

    public AlarmReceiver() {
        this.tmpBookEvent = new book_event_data();
        this.dateTime = new date_time();
        this.date = new date();
    }

    public void onReceive(Context context, Intent arg1) {
        SETTINGS.set_RTC_flag(false);
        Log.d("DVBPlayer.AlarmReceiver", "PVR or booked play will start, please wait..");
        acquire(context);
    }

    public static void acquire(Context ctx) {
        if (wakeLock != null) {
            wakeLock.release();
        }
//        wakeLock = ((PowerManager) ctx.getSystemService("power")).newWakeLock(805306394, "AlarmReceiver");
//        wakeLock.acquire();
    }
}
