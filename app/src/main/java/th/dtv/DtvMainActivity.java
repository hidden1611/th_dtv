package th.dtv;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import th.dtv.StorageDevice.DeviceItem;
import th.dtv.activity.EPGActivity;
import th.dtv.activity.PswdCB;
import th.dtv.activity.RecordListActivity;
import th.dtv.mw_data.date;
import th.dtv.mw_data.date_time;
import th.dtv.mw_data.dvb_s_sat;
import th.dtv.mw_data.dvb_s_tp;
import th.dtv.mw_data.dvb_t_area;
import th.dtv.mw_data.dvb_t_tp;
import th.dtv.mw_data.epg_extended_event;
import th.dtv.mw_data.epg_pf_event;
import th.dtv.mw_data.epg_schedule_event;
import th.dtv.mw_data.region_info;
import th.dtv.mw_data.service;
import th.dtv.mw_data.ttx_info;
import th.dtv.mw_data.ttx_sub_page_info;
import th.dtv.mw_data.tuner_signal_status;
import th.dtv.myctrl.FindCallBack;
import th.dtv.myctrl.FindDialog;
import th.dtv.myctrl.TimeShiftControl;
import th.dtv.util.HongKongDTMBHD;

public class DtvMainActivity extends DtvBaseActivity {
    private static boolean disableShowPlayStatus;
    private static RelativeLayout rl_encryptionStatusBar;
    private static RelativeLayout rl_playStatusBar;
    private static int temp_pos;
    StorageDeviceDialog DeviceInfoDia;
    private int SystemType;
    private int Temp_timeShift_PlayStatus;
    private int Temp_timeshift_speed;
    private AnimationDrawable anim;
    private dvb_t_area areaInfo;
    private boolean bChangetoAll;
    private boolean bChangetoNew;
    private boolean bGotoEPGWin;
    private boolean bInRecording;
    boolean bInit_timeShift;
    private boolean bPwdShow;
    private boolean bRefresh;
    private boolean bSeek;
    private boolean bTeletextShowed;
    private boolean bUpdate;
    private boolean bhandlemediakey;
    private boolean book_enableRecord;
    private int book_type;
    private boolean btimeshift;
    private ChannelListAdapter channelListAdapter;
    private CustomListView channel_list;
    private int channel_number;
    Dialog countdownDialog;
    private CountDownTimer counterTimer;
    private int current_record_service_region_index;
    private int current_record_service_tp_index;
    private int current_service_index;
    private int current_service_type;
    private date date;
    private date_time dateTime;
    private int device_no;
    private Dialog dia_currentDialog;
    private boolean enableRecord;
    private epg_pf_event epgCurrentEvent;
    private epg_extended_event epgExtendedEvent;
    private epg_pf_event epgNextEvent;
    private epg_schedule_event epgScheduleEvent;
    ProgressBar epg_time_progress;
    private EditText et_ttxPageNumber;
    String[] favgroupname;
    private int fbackforward;
    private boolean ftimelimit;
    private ImageView gifbuf;
    private EditText hour;
    private boolean inSubtitleMode;
    private boolean inTeletextMode;
    private TextView infoBar_hd;
    private TextView infoBar_scramble;
    private TextView infoBar_subtitle;
    private TextView infoBar_teletext;
    private int init_time;
    private boolean is_all_region;
    private LinearLayout ll_pvrInfo;
    private Handler localMsgHandler;
    private listOnKeyListener mChannelListListener;
    private editOnKeyListener mEditOnKeyListener;
    private int mFirstVisibleItem;
    Callback mSHCallback;
    private int mVisibleItemCount;
    private int m_hour;
    private int m_minute;
    private int m_second;
    private ImageView mainwnd_bg;
    private EditText minute;
    private Handler mwMsgHandler;
    private boolean one_time;
    private int pre_time;
    ProgressBar prg_infoBar_signalQuality;
    ProgressBar prg_infoBar_signalStrength;
    private TextView pvr_time_txt;
    private int rec_pre_service_index;
    private int rec_pre_service_type;
    private int record_time;
    Dialog recorddialog;
    private region_info regionInfo;
    private RelativeLayout rl_channelList;
    private RelativeLayout rl_channelNumber;
    private RelativeLayout rl_infoBar;
    private RelativeLayout rl_loading;
    private String sCurrentRecordStoreDir;
    private dvb_s_sat satInfo;
    private EditText second;
    private int seektime;
    private service serviceInfo;
    private tuner_signal_status signalStatus;
    private boolean special_case;
    private StorageDevice storageDeviceInfo;
    private StorageDevicePlugReceiver storageDevicePlugReceiver;
    private dvb_t_tp t_tpInfo;
    private int temp_current_service_index;
    private int temp_hour;
    private int temp_minute;
    private int temp_second;
    private int temp_whichone;
    private Dialog timeSelectDialog;
    private int timeShift_PlayStatus;
    TimeShiftControl timeshift;
    private int timeshift_speed;
    private TextView tips_txt;
    Toast toast;
    private int totaltime;
    private dvb_s_tp tpInfo;
    private ttx_sub_page_info ttxSubPageInfo;
    private TeletextSubtitleView ttx_sub;
    private TextView txv_channelNumber;
    private TextView txv_encrytionStatus;
    private TextView txv_infBar_dateTime;
    private TextView txv_infoBar_channelName;
    private TextView txv_infoBar_channelNum;
    private TextView txv_infoBar_currentEpg;
    private TextView txv_infoBar_nextEpg;
    private TextView txv_infoBar_signalQuality;
    private TextView txv_infoBar_signalStrength;
    private TextView txv_listTitle;
    private TextView txv_playStatus;
    VideoView videoView;

    /* renamed from: th.dtv.DtvMainActivity.11 */
    class AnonymousClass11 extends CountDownTimer {
        AnonymousClass11(long x0, long x1) {
            super(x0, x1);
        }

        public void onTick(long millisUntilFinished) {
            long left_time = millisUntilFinished / 1000;
            Log.d("DtvMainActivity", "seconds remaining :" + left_time);
            ((CountdownDialog) DtvMainActivity.this.countdownDialog).setMyText(left_time);
        }

        public void onFinish() {
            Log.d("DtvMainActivity", "onFinish");
            if (DtvMainActivity.this.countdownDialog != null) {
                DtvMainActivity.this.countdownDialog.dismiss();
                DtvMainActivity.this.countdownDialog = null;
            }
            SETTINGS.bFromRTC = false;
            DtvMainActivity.this.PowerDown();
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.1 */
    class C00341 implements OnKeyListener {
        C00341() {
        }

        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            Intent intent;
            if (keyEvent.getAction() != 0) {
                switch (i) {
                    case DtvBaseActivity.KEYCODE_MENU /*82*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        DtvMainActivity.this.MainMenuSetup();
                        return true;
                    case DtvBaseActivity.KEYCODE_EPG /*2013*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        DtvMainActivity.this.bGotoEPGWin = true;
                        intent = new Intent();
                        intent.setFlags(67108864);
                        intent.putExtra("LauncherFormDtv", true);
                        intent.setClass(DtvMainActivity.this, EPGActivity.class);
                        DtvMainActivity.this.startActivity(intent);
                        return true;
                    default:
                        break;
                }
            } else if (DtvMainActivity.this.rl_channelList.getVisibility() == 0) {
                switch (i) {
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    case MW.RET_INVALID_TP_INDEX /*20*/:
                    case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                    case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        DtvMainActivity.this.channel_list.onKey(view, i, keyEvent);
                        return true;
                    default:
                        if (DtvMainActivity.this.mChannelListListener == null) {
                            return true;
                        }
                        DtvMainActivity.this.mChannelListListener.onKey(view, i, keyEvent);
                        return true;
                }
            } else {
                int ts_player_play_up;
                switch (i) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                        if (DtvMainActivity.this.rl_infoBar.getVisibility() != 4) {
                            DtvMainActivity.this.CloseInfoBarDelayed(0);
                        }
                        DtvMainActivity.this.CloseTmpPlayStatusInfo(-1);
                        return true;
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        ts_player_play_up = MW.ts_player_play_up(50, false);
                        if (ts_player_play_up == 0) {
                            DtvMainActivity.this.showInfoBar();
                            DtvMainActivity.this.CheckParentLockAndShowPasswd();
                            return true;
                        } else if (ts_player_play_up != 44) {
                            return true;
                        } else {
                            DtvMainActivity.this.showInfoBar();
                            DtvMainActivity.this.CheckChannelLockAndShowPasswd("2");
                            return true;
                        }
                    case MW.RET_INVALID_TP_INDEX /*20*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        ts_player_play_up = MW.ts_player_play_down(50, false);
                        if (ts_player_play_up == 0) {
                            DtvMainActivity.this.showInfoBar();
                            DtvMainActivity.this.CheckParentLockAndShowPasswd();
                            return true;
                        } else if (ts_player_play_up != 44) {
                            return true;
                        } else {
                            DtvMainActivity.this.showInfoBar();
                            DtvMainActivity.this.CheckChannelLockAndShowPasswd("3");
                            return true;
                        }
                    case MW.RET_INVALID_TYPE /*23*/:
                        if (DtvMainActivity.this.rl_channelNumber.getVisibility() != 4) {
                            return true;
                        }
                        DtvMainActivity.this.RefreshShowChannelList();
                        DtvMainActivity.this.listview_Animation();
                        if (!DtvMainActivity.this.inTeletextMode) {
                            return true;
                        }
                        DtvMainActivity.this.stopTeletextSubtitle();
                        return true;
                    case MW.RET_INVALID_AREA_INDEX_LIST /*27*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        if (MW.db_get_service_available_fav_group_count() <= 0) {
                            SETTINGS.makeText(DtvMainActivity.this, R.string.no_fav_channel, 0);
                            if (SETTINGS.bPass) {
                                MW.ts_player_play_current(true, false);
                                return true;
                            }
                            ts_player_play_up = MW.ts_player_play_current(false, false);
                            if (ts_player_play_up == 0) {
                                DtvMainActivity.this.CheckParentLockAndShowPasswd();
                                return true;
                            } else if (ts_player_play_up != 44) {
                                return true;
                            } else {
                                DtvMainActivity.this.CheckChannelLockAndShowPasswd("15");
                                return true;
                            }
                        }
                        DtvMainActivity.this.DoFavAction();
                        return true;
                    case DtvBaseActivity.KEYCODE_SAT /*55*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        if (DtvMainActivity.this.SystemType == 0 || DtvMainActivity.this.SystemType == 2) {
                            if (MW.db_dvb_s_get_sat_count(0) <= 0) {
                                SETTINGS.makeText(DtvMainActivity.this, R.string.no_satellite, 0);
                                return true;
                            }
                            DtvMainActivity.this.DoSatAction();
                            return true;
                        } else if (MW.db_dvb_t_get_area_count() <= 0) {
                            return true;
                        } else {
                            DtvMainActivity.this.DoSatAction();
                            return true;
                        }
                    case DtvBaseActivity.KEYCODE_RECORD /*57*/:
                        SETTINGS.makeText(DtvMainActivity.this, R.string.channel_is_locked, 1);
                        return true;
                    case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        if (DtvMainActivity.this.storageDeviceInfo == null) {
                            return true;
                        }
                        if (DtvMainActivity.this.storageDeviceInfo.getDeviceCount() > 0) {
                            intent = new Intent();
                            intent.setFlags(67108864);
                            intent.setClass(DtvMainActivity.this, RecordListActivity.class);
                            DtvMainActivity.this.startActivity(intent);
                            return true;
                        }
                        SETTINGS.makeText(DtvMainActivity.this, R.string.no_storage, 0);
                        return true;
                    case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                    case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                        if (DtvMainActivity.this.bInRecording) {
                            DtvMainActivity.this.ShowRecordAlertDialog();
                            return true;
                        }
                        switch (MW.ts_player_play_switch_tv_radio(false)) {
                            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                                if (DtvMainActivity.this.dia_currentDialog != null) {
                                    DtvMainActivity.this.dia_currentDialog.dismiss();
                                }
                                DtvMainActivity.this.showInfoBar();
                                DtvMainActivity.this.RefreshTvRadioBackground();
                                DtvMainActivity.this.CheckParentLockAndShowPasswd();
                                return true;
                            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                                DtvMainActivity.this.ShowTmpPlayStatus(DtvMainActivity.this.getString(R.string.NO_CHANNEL));
                                return true;
                            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                                DtvMainActivity.this.ShowTmpPlayStatus(DtvMainActivity.this.getString(R.string.NO_VIDEO_CHANNEL));
                                return true;
                            case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                                DtvMainActivity.this.ShowTmpPlayStatus(DtvMainActivity.this.getString(R.string.NO_AUDIO_CHANNEL));
                                return true;
                            case MW.RET_SERVICE_LOCK /*44*/:
                                if (DtvMainActivity.this.dia_currentDialog != null) {
                                    DtvMainActivity.this.dia_currentDialog.dismiss();
                                }
                                DtvMainActivity.this.showInfoBar();
                                DtvMainActivity.this.RefreshTvRadioBackground();
                                DtvMainActivity.this.CheckChannelLockAndShowPasswd("4");
                                return true;
                            default:
                                return true;
                        }
                    case DtvBaseActivity.KEYCODE_INFO /*185*/:
                        if (MW.db_get_total_service_count() <= 0) {
                            return true;
                        }
                        if (DtvMainActivity.this.rl_infoBar.getVisibility() != 0) {
                            DtvMainActivity.this.showInfoBar();
                            return true;
                        }
                        DtvMainActivity.this.ShowCurrentServiceInfo();
                        return true;
                    case DtvBaseActivity.KEYCODE_RECALL /*666*/:
                        if (DtvMainActivity.this.dia_currentDialog != null) {
                            DtvMainActivity.this.dia_currentDialog.dismiss();
                        }
                        if (MW.db_get_current_service_info(DtvMainActivity.this.serviceInfo) != 0) {
                            return true;
                        }
                        int i2 = DtvMainActivity.this.serviceInfo.service_type;
                        ts_player_play_up = MW.ts_player_play_previous(false);
                        if (ts_player_play_up == 0) {
                            DtvMainActivity.this.showInfoBar();
                            DtvMainActivity.this.CheckParentLockAndShowPasswd();
                            return true;
                        } else if (ts_player_play_up != 44) {
                            return true;
                        } else {
                            DtvMainActivity.this.showInfoBar();
                            DtvMainActivity.this.RefreshTvRadioBackground();
                            DtvMainActivity.this.CheckChannelLockAndShowPasswd("5");
                            return true;
                        }
                }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.2 */
    class C00352 implements PswdCB {
        C00352() {
        }

        public void DoCallBackEvent() {
            SETTINGS.bPass = true;
            MW.ts_player_play_current(true, false);
            DtvMainActivity.this.temp_whichone = -1;
            DtvMainActivity.this.refreshTeletextSubtitle();
            if (DtvMainActivity.this.rl_channelList.getVisibility() != View.GONE ) {
                DtvMainActivity.this.showInfoBar();
            }
            DtvMainActivity.this.RefreshTvRadioBackground();
            DtvMainActivity.this.bPwdShow = false;
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.4 */
    class C00364 implements TextWatcher {
        C00364() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                DtvMainActivity.this.m_hour = Integer.parseInt(DtvMainActivity.this.hour.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                DtvMainActivity.this.m_hour = DtvMainActivity.this.temp_hour;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.5 */
    class C00375 implements TextWatcher {
        C00375() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                DtvMainActivity.this.m_minute = Integer.parseInt(DtvMainActivity.this.minute.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                DtvMainActivity.this.m_minute = DtvMainActivity.this.temp_minute;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.6 */
    class C00386 implements TextWatcher {
        C00386() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                DtvMainActivity.this.m_second = Integer.parseInt(DtvMainActivity.this.second.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                DtvMainActivity.this.m_second = DtvMainActivity.this.temp_second;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.7 */
    class C00397 implements DialogInterface.OnKeyListener {
        C00397() {
        }

        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                        if (DtvMainActivity.this.timeShift_PlayStatus == 3) {
                            MW.pvr_timeshift_resume();
                            DtvMainActivity.this.timeShift_PlayStatus = 4;
                            DtvMainActivity.this.timeshift.showPlayStatusAndSpeed(4, 0);
                            break;
                        }
                        break;
                    case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                    case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                    case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                    case MW.RET_TP_EXIST /*10*/:
                    case MW.RET_INVALID_SAT /*11*/:
                    case MW.RET_INVALID_AREA /*12*/:
                    case MW.RET_INVALID_TP /*13*/:
                    case MW.RET_BOOK_EVENT_INVALID /*14*/:
                    case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                    case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                        int value = 0;
                        Log.e("LEE", "KeyCode :" + keyCode);
                        if (DtvMainActivity.this.hour.isFocused()) {
                            if (DtvMainActivity.this.hour.getText().toString().length() > 0) {
                                value = Integer.valueOf(DtvMainActivity.this.hour.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100) {
                                value %= 10;
                            }
                            DtvMainActivity.this.hour.setText(Integer.toString(value));
                            return true;
                        } else if (DtvMainActivity.this.minute.isFocused()) {
                            if (DtvMainActivity.this.minute.getText().toString().length() > 0) {
                                value = Integer.valueOf(DtvMainActivity.this.minute.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100) {
                                value %= 10;
                            } else if (value >= 60) {
                                value = 0;
                            }
                            DtvMainActivity.this.minute.setText(Integer.toString(value));
                            return true;
                        } else if (!DtvMainActivity.this.second.isFocused()) {
                            return true;
                        } else {
                            if (DtvMainActivity.this.second.getText().toString().length() > 0) {
                                value = Integer.valueOf(DtvMainActivity.this.second.getText().toString()).intValue();
                            }
                            value = ((value * 10) + keyCode) - 7;
                            if (value >= 100) {
                                value %= 10;
                            } else if (value >= 60) {
                                value = 0;
                            }
                            DtvMainActivity.this.second.setText(Integer.toString(value));
                            return true;
                        }
                    case MW.RET_INVALID_RECORD_INDEX /*21*/:
                        Log.e("LEE ", "focus : " + DtvMainActivity.this.hour.isFocused() + DtvMainActivity.this.minute.isFocused() + DtvMainActivity.this.second.isFocused());
                        if (DtvMainActivity.this.hour.isFocused()) {
                            DtvMainActivity.this.second.requestFocus();
                            return true;
                        } else if (DtvMainActivity.this.second.isFocused()) {
                            DtvMainActivity.this.second.clearFocus();
                            DtvMainActivity.this.minute.requestFocus();
                            return true;
                        } else if (DtvMainActivity.this.minute.isFocused()) {
                            DtvMainActivity.this.minute.clearFocus();
                            if (DtvMainActivity.this.hour.isFocusable()) {
                                DtvMainActivity.this.hour.requestFocus();
                                return true;
                            }
                            DtvMainActivity.this.second.requestFocus();
                            return true;
                        }
                        break;
                    case MW.RET_MEMORY_ERROR /*22*/:
                        if (DtvMainActivity.this.second.isFocused()) {
                            DtvMainActivity.this.second.clearFocus();
                            if (DtvMainActivity.this.hour.isFocusable()) {
                                DtvMainActivity.this.hour.requestFocus();
                                return true;
                            }
                            DtvMainActivity.this.minute.requestFocus();
                            return true;
                        } else if (DtvMainActivity.this.hour.isFocused()) {
                            DtvMainActivity.this.hour.clearFocus();
                            DtvMainActivity.this.minute.requestFocus();
                            return true;
                        } else if (DtvMainActivity.this.minute.isFocused()) {
                            DtvMainActivity.this.minute.clearFocus();
                            DtvMainActivity.this.second.requestFocus();
                            return true;
                        }
                        break;
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_MEDIA_PLAY_PAUSE /*85*/:
                        int m_seektime = (((DtvMainActivity.this.m_hour * 60) * 60) + (DtvMainActivity.this.m_minute * 60)) + DtvMainActivity.this.m_second;
                        Log.e("LEE", "m_seektime : " + m_seektime);
                        Log.e("LEE", "m_hour : " + DtvMainActivity.this.m_hour);
                        Log.e("LEE", "m_minute : " + DtvMainActivity.this.m_minute);
                        Log.e("LEE", " m_second : " + DtvMainActivity.this.m_second);
                        if (m_seektime < 0 || m_seektime > DtvMainActivity.this.totaltime) {
                            DtvMainActivity.this.tips_txt.setText(DtvMainActivity.this.getResources().getString(R.string.seekTimeTitletips_invalid));
                            DtvMainActivity.this.tips_txt.setTextColor(Color.rgb(255, 0, 0));
                            return true;
                        } else if (m_seektime == DtvMainActivity.this.seektime) {
                            MW.pvr_timeshift_resume();
                            DtvMainActivity.this.timeShift_PlayStatus = 4;
                            DtvMainActivity.this.timeshift.showPlayStatusAndSpeed(4, 0);
                            DtvMainActivity.this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                            DtvMainActivity.this.bSeek = true;
                            dialog.dismiss();
                            return true;
                        } else {
                            MW.pvr_timeshift_seek(m_seektime, false);
                            DtvMainActivity.this.timeShift_PlayStatus = 4;
                            DtvMainActivity.this.timeshift.showPlayStatusAndSpeed(4, 0);
                            DtvMainActivity.this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                            DtvMainActivity.this.bSeek = true;
                            dialog.dismiss();
                            return true;
                        }
                }
            }
            return false;
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.8 */
    class C00408 implements FindCallBack {
        C00408() {
        }

        public void DoCallBackEvent(int index) {
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            DtvMainActivity.this.channel_list.setCustomSelection(index);
            if (MW.db_get_current_service_info(DtvMainActivity.this.serviceInfo) == 0) {
                int Ret = MW.ts_player_play(DtvMainActivity.this.current_service_type, DtvMainActivity.this.channel_list.getSelectedItemPosition(), 0, false);
                System.out.println("LEE : Ret = " + Ret);
                if (Ret == 0) {
                    if (MW.db_get_current_service_info(DtvMainActivity.this.serviceInfo) == 0) {
                        DtvMainActivity.this.current_service_index = DtvMainActivity.this.serviceInfo.service_index;
                    }
                    DtvMainActivity.this.channelListAdapter.notifyDataSetChanged();
                    DtvMainActivity.this.CheckParentLockAndShowPasswd();
                } else if (Ret == 44) {
                    if (MW.db_get_current_service_info(DtvMainActivity.this.serviceInfo) == 0) {
                        DtvMainActivity.this.current_service_index = DtvMainActivity.this.serviceInfo.service_index;
                    }
                    DtvMainActivity.this.channelListAdapter.notifyDataSetChanged();
                    DtvMainActivity.this.CheckChannelLockAndShowPasswd("23");
                }
            } else {
                Log.e("LEE", "get current service info failed!!!");
            }
            Log.e("LEE", "FindDialog callback");
        }
    }

    /* renamed from: th.dtv.DtvMainActivity.9 */
    class C00419 implements Callback {
        C00419() {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("DtvMainActivity", "surfaceChanged");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("DtvMainActivity", "surfaceCreated");
            try {
                initSurface(holder);
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("DtvMainActivity", "surfaceDestroyed");
        }

        private void initSurface(SurfaceHolder h) {
            Canvas c = null;
            try {
                Log.d("DtvMainActivity", "initSurface");
                c = h.lockCanvas();
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            } catch (Throwable th) {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }
    }

    public class AudioInfoDialog extends Dialog {
        private ArrayAdapter mAdapter;
        TextView mAudioTrack;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.DtvMainActivity.AudioInfoDialog.1 */
        class C00421 implements OnItemClickListener {
            C00421() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                MW.ts_player_play_switch_audio(position);
                AudioInfoDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.audio_info_dialog);
            DtvMainActivity.this.dia_currentDialog = this;
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != 4) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mAudioTrack = (TextView) findViewById(R.id.info);
            switch (SETTINGS.get_audio_channel()) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    this.mAudioTrack.setText(R.string.stereo);
                    break;
                case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                    this.mAudioTrack.setText(R.string.left);
                    break;
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    this.mAudioTrack.setText(R.string.right);
                    break;
            }
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            if (DtvMainActivity.this.serviceInfo.audio_info.length > 0) {
                List<String> languages = new ArrayList();
                languages.clear();
                int i = 0;
                while (i < DtvMainActivity.this.serviceInfo.audio_info.length) {
                    String audio_type = DtvMainActivity.this.getResources().getStringArray(R.array.audio_type)[DtvMainActivity.this.serviceInfo.audio_info[i].audio_stream_type];
                    if (DtvMainActivity.this.serviceInfo.audio_info[i].ISO_639_language_code.length() <= 0 || DtvMainActivity.this.serviceInfo.audio_info[i].ISO_639_language_code.charAt(0) == '\u0000') {
                        languages.add("Audio " + (i + 1) + " (" + audio_type + ")");
                    } else {
                        languages.add(DtvMainActivity.this.serviceInfo.audio_info[i].ISO_639_language_code + " (" + audio_type + ")");
                    }
                    i++;
                }
                this.mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, languages);
                this.mListView.setAdapter(this.mAdapter);
                this.mListView.setChoiceMode(1);
                this.mListView.setItemChecked(DtvMainActivity.this.serviceInfo.audio_index, true);
                this.mListView.setCustomSelection(DtvMainActivity.this.serviceInfo.audio_index);
                this.mListView.setVisibleItemCount(5);
                this.mListView.setOnItemClickListener(new C00421());
            }
        }

        protected void onStop() {
            super.onStop();
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            DtvMainActivity.this.dia_currentDialog = null;
        }

        public AudioInfoDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mAudioTrack = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            int channel;
            switch (keyCode) {
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                    channel = SETTINGS.get_audio_channel();
                    if (channel == 1) {
                        this.mAudioTrack.setText(R.string.stereo);
                        SETTINGS.set_audio_channel(0);
                        return true;
                    } else if (channel == 2) {
                        this.mAudioTrack.setText(R.string.left);
                        SETTINGS.set_audio_channel(1);
                        return true;
                    } else {
                        this.mAudioTrack.setText(R.string.right);
                        SETTINGS.set_audio_channel(2);
                        return true;
                    }
                case MW.RET_MEMORY_ERROR /*22*/:
                    channel = SETTINGS.get_audio_channel();
                    if (channel == 1) {
                        this.mAudioTrack.setText(R.string.right);
                        SETTINGS.set_audio_channel(2);
                        return true;
                    } else if (channel == 2) {
                        this.mAudioTrack.setText(R.string.stereo);
                        SETTINGS.set_audio_channel(0);
                        return true;
                    } else {
                        this.mAudioTrack.setText(R.string.left);
                        SETTINGS.set_audio_channel(1);
                        return true;
                    }
                case DtvBaseActivity.KEYCODE_AUDIO /*2006*/:
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    private class ChannelListAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater mInflater;

        class ViewHolder {
            TextView channel_name;
            TextView channel_num;
            ImageView channel_quality_icon;
            ImageView fav_icon;

            ViewHolder() {
            }
        }

        public ChannelListAdapter(Context context) {
            this.context = null;
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MW.db_get_service_count(DtvMainActivity.this.current_service_type);
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder localViewHolder;
            System.out.println("pos = " + pos);
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.main_channel_list_item, null);
                localViewHolder = new ViewHolder();
                localViewHolder.channel_num = (TextView) convertView.findViewById(R.id.channellist_num_txt);
                localViewHolder.channel_name = (TextView) convertView.findViewById(R.id.channellist_name_txt);
                localViewHolder.channel_quality_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_sharp_img);
                localViewHolder.fav_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_fav_img);
                convertView.setTag(localViewHolder);
            } else {
                localViewHolder = (ViewHolder) convertView.getTag();
            }
            if (DtvMainActivity.this.current_service_index == pos) {
//                localViewHolder.channel_name.setTextColor(-256);
//                localViewHolder.channel_num.setTextColor(-256);
            } else {
//                localViewHolder.channel_name.setTextColor(-1);
//                localViewHolder.channel_num.setTextColor(-1);
            }
            if (MW.db_get_service_info(DtvMainActivity.this.current_service_type, pos, DtvMainActivity.this.serviceInfo) == 0) {
                localViewHolder.channel_num.setText(Integer.toString(DtvMainActivity.this.serviceInfo.channel_number_display));
                localViewHolder.channel_name.setText(DtvMainActivity.this.serviceInfo.service_name);
                if (SETTINGS.bHongKongDTMB) {
                    if (HongKongDTMBHD.isHongKongDTMBHDChannel(DtvMainActivity.this.serviceInfo.video_pid, DtvMainActivity.this.serviceInfo.audio_info[0].audio_pid) || DtvMainActivity.this.serviceInfo.is_hd) {
                        localViewHolder.channel_quality_icon.setImageResource(R.drawable.imagehd1);
                    } else {
                        localViewHolder.channel_quality_icon.setImageResource(0);
                    }
                } else if (DtvMainActivity.this.serviceInfo.is_hd) {
                    localViewHolder.channel_quality_icon.setImageResource(R.drawable.imagehd1);
                } else {
                    localViewHolder.channel_quality_icon.setImageResource(0);
                }
                if (DtvMainActivity.this.serviceInfo.is_scrambled) {
                    localViewHolder.fav_icon.setImageResource(R.drawable.coin_us_dollar);
                } else {
                    localViewHolder.fav_icon.setImageResource(0);
                }
            }
            return convertView;
        }
    }

    public class CountdownDialog extends Dialog {
        TextView channel_name;
        TextView deldia_title;
        long left_time_ms;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        View mLayoutView;
        TextView mTextView;
        TextView program_name;
        TextView time;

        /* renamed from: th.dtv.DtvMainActivity.CountdownDialog.1 */
//        class C00431 implements OnClickListener {
//            C00431() {
//            }
//
//            public void onClick(View v) {
//                DtvMainActivity.this.counterTimer.cancel();
//                CountdownDialog.this.dismiss();
//                DtvMainActivity.this.countdownDialog = null;
//                SETTINGS.bFromRTC = false;
//            }
//        }
//
//        /* renamed from: th.dtv.DtvMainActivity.CountdownDialog.2 */
//        class C00442 implements OnClickListener {
//            C00442() {
//            }
//
//            public void onClick(View v) {
//                DtvMainActivity.this.counterTimer.cancel();
//                CountdownDialog.this.dismiss();
//                DtvMainActivity.this.countdownDialog = null;
//                SETTINGS.bFromRTC = false;
//                DtvMainActivity.this.PowerDown();
//            }
//        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.book_timerevent__dialog);
            this.mTextView = (TextView) findViewById(R.id.delete_content);
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.time = (TextView) findViewById(R.id.time);
            this.channel_name = (TextView) findViewById(R.id.channel_name);
            this.program_name = (TextView) findViewById(R.id.program_name);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.deldia_title.setText(DtvMainActivity.this.getResources().getString(R.string.tips));
            this.mTextView.setVisibility(View.VISIBLE);
            this.channel_name.setVisibility(View.GONE);
            this.program_name.setVisibility(View.GONE);
            this.mBtnCancel.setText(R.string.cancel);
            this.mBtnStart.setText(String.format(DtvMainActivity.this.getResources().getString(R.string.play_right_now), new Object[]{DtvMainActivity.this.getResources().getStringArray(R.array.timer_type)[2]}));
            this.left_time_ms = 60000;
            this.time.setText(String.format(DtvMainActivity.this.getResources().getString(R.string.powerdown_time), new Object[]{Long.valueOf(this.left_time_ms), DtvMainActivity.this.getResources().getStringArray(R.array.timer_type)[2]}));
//            this.mBtnCancel.setOnClickListener(new C00431());
//            this.mBtnStart.setOnClickListener(new C00442());
        }

        public void setMyText(long left_time_s) {
            String sType = DtvMainActivity.this.getResources().getStringArray(R.array.timer_type)[2];
            this.time.setText(String.format(DtvMainActivity.this.getResources().getString(R.string.powerdown_time), new Object[]{Long.valueOf(left_time_s), sType}));
        }

        public CountdownDialog(Context context, int theme) {
            super(context, theme);
            this.mContext = null;
            this.mLayoutView = null;
            this.mTextView = null;
            this.deldia_title = null;
            this.time = null;
            this.channel_name = null;
            this.program_name = null;
            this.left_time_ms = 0;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                        if (DtvMainActivity.this.countdownDialog != null) {
                            DtvMainActivity.this.countdownDialog.dismiss();
                        }
                        return true;
                }
            }
            return false;
        }
    }

    public class DyWTSDia extends Dialog {
        Button mBtnCancel;
        Button mBtnStart;
        TextView mContent;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView mTitle;

        /* renamed from: th.dtv.DtvMainActivity.DyWTSDia.1 */
//        class C00451 implements OnClickListener {
//            C00451() {
//            }
//
//            public void onClick(View v) {
//                MW.ts_player_require_play_status();
//                DyWTSDia.this.dismiss();
//            }
//        }
//
//        /* renamed from: th.dtv.DtvMainActivity.DyWTSDia.2 */
//        class C00462 implements OnClickListener {
//            C00462() {
//            }
//
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.setFlags(67108864);
//                intent.setClass(DtvMainActivity.this, InstallerActivity.class);
//                DtvMainActivity.this.startActivity(intent);
//                DyWTSDia.this.dismiss();
//            }
//        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mTitle = (TextView) findViewById(R.id.deldia_title);
            this.mContent = (TextView) findViewById(R.id.delete_content);
            this.mBtnStart.requestFocus();
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != 4) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            this.mTitle.setText(R.string.tips);
            this.mContent.setText(R.string.dywts);
//            this.mBtnCancel.setOnClickListener(new C00451());
//            this.mBtnStart.setOnClickListener(new C00462());
        }

        public DyWTSDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mTitle = null;
            this.mContent = null;
            this.mContext = context;
        }

        protected void onStop() {
            DtvMainActivity.disableShowPlayStatus = false;
            super.onStop();
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    MW.ts_player_require_play_status();
                    dismiss();
                    break;
            }
            return super.onKeyUp(keyCode, event);
        }
    }

    public class ExitRecordDia extends Dialog {
        private TextView deldia_title;
        private TextView delete_content;
        Button mBtnCancel;
        Button mBtnStart;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;

        /* renamed from: th.dtv.DtvMainActivity.ExitRecordDia.1 */
//        class C00471 implements OnClickListener {
//            C00471() {
//            }
//
//            public void onClick(View v) {
//                DtvMainActivity.this.bInRecording = true;
//                ExitRecordDia.this.dismiss();
//            }
//        }
//
//        /* renamed from: th.dtv.DtvMainActivity.ExitRecordDia.2 */
//        class C00482 implements OnClickListener {
//            C00482() {
//            }
//
//            public void onClick(View v) {
//                DtvMainActivity.this.bInRecording = false;
//                if (DtvMainActivity.this.bInit_timeShift) {
//                    DtvMainActivity.disableShowPlayStatus = true;
//                    if (DtvMainActivity.rl_playStatusBar.getVisibility() != 4) {
//                        DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
//                    }
//                    DtvMainActivity.this.RemoveDeviceIsFullMsg();
//                    MW.pvr_timeshift_stop();
//                    if (DtvMainActivity.this.timeshift != null) {
//                        DtvMainActivity.this.timeshift.dismiss();
//                    }
//                    DtvMainActivity.this.showInfoBar();
//                    DtvMainActivity.this.btimeshift = false;
//                } else {
//                    DtvMainActivity.this.RemoveDeviceIsFullMsg();
//                    MW.pvr_record_stop();
//                }
//                DtvMainActivity.this.refreshTeletextSubtitle();
//                ExitRecordDia.this.dismiss();
//            }
//        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sate_delete_dialog);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.sate_delete_dialog, null);
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != 4) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.deldia_title = (TextView) findViewById(R.id.deldia_title);
            this.delete_content = (TextView) findViewById(R.id.delete_content);
            this.deldia_title.setText(DtvMainActivity.this.getResources().getString(R.string.tips));
            if (DtvMainActivity.this.bInit_timeShift) {
                this.delete_content.setText(DtvMainActivity.this.getResources().getString(R.string.timeshifting_sure_exit));
            } else {
                this.delete_content.setText(DtvMainActivity.this.getResources().getString(R.string.recording_sure_exit));
            }
            this.mBtnCancel.requestFocus();
//            this.mBtnCancel.setOnClickListener(new C00471());
//            this.mBtnStart.setOnClickListener(new C00482());
        }

        public ExitRecordDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.deldia_title = null;
            this.delete_content = null;
            this.mContext = context;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return super.onKeyUp(keyCode, event);
        }

        protected void onStop() {
            if (!DtvMainActivity.this.bInit_timeShift) {
                DtvMainActivity.disableShowPlayStatus = false;
                MW.ts_player_require_play_status();
            }
            super.onStop();
        }
    }

    public class FavGroupSelectDialog extends Dialog {
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.DtvMainActivity.FavGroupSelectDialog.1 */
        class C00491 implements OnItemClickListener {
            C00491() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                System.out.println("FavGroupSelectDialog   onItemClick");
                if (position >= MW.db_get_service_available_fav_group_count()) {
                    MW.db_service_set_in_region_mode();
                    MW.db_set_service_region_current_pos(0);
                } else {
                    MW.db_service_set_in_fav_mode();
                    if (MW.db_set_service_available_fav_group_current_pos(position)) {
                        System.out.println("set fav mode ");
                        if (MW.db_get_service_count(DtvMainActivity.this.current_service_type) != 0) {
                            if (DtvMainActivity.this.bPwdShow) {
                                DtvMainActivity.this.bPwdShow = false;
                            }
                            DtvMainActivity.temp_pos = position;
                            int Ret = MW.ts_player_play_current(false, false);
                            if (Ret == 0) {
                                if (SETTINGS.bPass) {
                                    MW.ts_player_play_current(true, false);
                                } else {
                                    DtvMainActivity.this.CheckParentLockAndShowPasswd();
                                }
                            } else if (Ret == 44) {
                                if (SETTINGS.bPass) {
                                    MW.ts_player_play_current(true, false);
                                } else {
                                    DtvMainActivity.this.CheckChannelLockAndShowPasswd("14");
                                }
                            }
                        }
                    }
                }
                DtvMainActivity.this.RefreshShowChannelList();
                FavGroupSelectDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            DtvMainActivity.this.dia_currentDialog = this;
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != View.INVISIBLE) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            ((TextView) findViewById(R.id.TextView_Title)).setText(R.string.FavType);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            int favGroupCount = MW.db_get_service_available_fav_group_count();
            List<String> items = new ArrayList();
            items.clear();
            for (int i = 0; i < favGroupCount; i++) {
                items.add(DtvMainActivity.this.favgroupname[MW.db_get_service_available_fav_group_index(i)]);
            }
            items.add(DtvMainActivity.this.getString(R.string.All));
            this.mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setCustomSelection(MW.db_get_service_available_fav_group_current_pos());
            this.mListView.setItemChecked(MW.db_get_service_available_fav_group_current_pos(), true);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C00491());
        }

        protected void onStop() {
            super.onStop();
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            DtvMainActivity.this.dia_currentDialog = null;
        }

        public FavGroupSelectDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                case MW.RET_INVALID_TYPE /*23*/:
                case MW.RET_INVALID_AREA_INDEX_LIST /*27*/:
                    dismiss();
                    if (SETTINGS.bPass) {
                        MW.ts_player_play_current(true, false);
                        return true;
                    }
                    int Ret = MW.ts_player_play_current(false, false);
                    if (Ret == 0) {
                        DtvMainActivity.this.CheckParentLockAndShowPasswd();
                        return true;
                    } else if (Ret != 44) {
                        return true;
                    } else {
                        DtvMainActivity.this.CheckChannelLockAndShowPasswd("15");
                        return true;
                    }
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    private class InputTimeDialog extends Dialog {
        private Button mBtnCancel;
        private Button mBtnStart;
        private Context mContext;
        private DigitsEditText mEditText;

        /* renamed from: th.dtv.DtvMainActivity.InputTimeDialog.1 */
//        class C00501 implements OnClickListener {
//            C00501() {
//            }
//
//            public void onClick(View v) {
//                InputTimeDialog.this.dismiss();
//            }
//        }
//
//        /* renamed from: th.dtv.DtvMainActivity.InputTimeDialog.2 */
//        class C00512 implements OnClickListener {
//            C00512() {
//            }
//
//            public void onClick(View v) {
//                int time = 0;
//                if (!InputTimeDialog.this.mEditText.getText().toString().equals("")) {
//                    time = Integer.parseInt(InputTimeDialog.this.mEditText.getText().toString());
//                }
//                if (DtvMainActivity.this.ftimelimit) {
//                    if (time <= 0 || time > 60) {
//                        SETTINGS.makeText(InputTimeDialog.this.mContext, R.string.timeshift_range, 0);
//                        return;
//                    }
//                    SETTINGS.setCustomTimeShiftLength(time);
//                } else if (time == 0) {
//                    SETTINGS.makeText(InputTimeDialog.this.mContext, R.string.invalid_value, 0);
//                    return;
//                } else {
//                    SETTINGS.setCustomRecordLength(time);
//                }
//                DtvMainActivity.this.record_time = time;
//                if (DtvMainActivity.this.record_time >= 0) {
//                    System.out.println("btimeshift " + DtvMainActivity.this.btimeshift);
//                    String spare;
//                    long spareSize;
//                    if (DtvMainActivity.this.ftimelimit) {
//                        DtvMainActivity.this.ftimelimit = false;
//                        if (DtvMainActivity.this.timeshift == null) {
//                            DtvMainActivity.this.bInRecording = true;
//                            DtvMainActivity.this.record_time = DtvMainActivity.this.record_time * 60;
//                            DtvMainActivity.this.CloseInfoBarDelayed(0);
//                            DtvMainActivity.this.storageDeviceInfo.refreshDevice();
//                            String Path = DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).Path;
//                            System.out.println(Path);
//                            spare = DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).spare;
//                            if (spare != null) {
//                                spareSize = StorageDevice.getSpareSize(spare);
//                                if (spareSize != -1 && spareSize < 2097152) {
//                                    SETTINGS.makeText(DtvMainActivity.this, R.string.spaceisfull, 0);
//                                    DtvMainActivity.this.bInRecording = false;
//                                    return;
//                                }
//                            }
//                            DtvMainActivity.this.stopTeletextSubtitle();
//                            DtvMainActivity.disableShowPlayStatus = true;
//                            switch (MW.pvr_timeshift_start(DtvMainActivity.this.current_service_type, DtvMainActivity.this.serviceInfo.service_index, DtvMainActivity.this.record_time, Path)) {
//                                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
//                                    Log.e("DtvMainActivity", "timeshift RET_OK");
//                                    DtvMainActivity.this.bInit_timeShift = true;
//                                    DtvMainActivity.this.sCurrentRecordStoreDir = new String(DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).Path);
//                                    SETTINGS.makeText(DtvMainActivity.this, R.string.start_timeshift, 0);
//                                    DtvMainActivity.this.SendDeviceIsFullMsg();
//                                    DtvMainActivity.this.bInRecording = true;
//                                    break;
//                                case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
//                                    Log.e("DtvMainActivity", "timeshift RET_NO_THIS_CHANNEL");
//                                    break;
//                                case MW.RET_INVALID_OBJECT /*30*/:
//                                    Log.e("DtvMainActivity", "timeshift RET_INVALID_OBJECT");
//                                    break;
//                                case MW.RET_ALREADY_RECORDING /*41*/:
//                                    Log.e("DtvMainActivity", "timeshift RET_ALREADY_RECORDING");
//                                    break;
//                            }
//                            DtvMainActivity.disableShowPlayStatus = false;
//                            DtvMainActivity.this.refreshTeletextSubtitle();
//                            DtvMainActivity.this.timeshift = new TimeShiftControl(DtvMainActivity.this);
//                            DtvMainActivity.this.timeshift.setChannelNum(DtvMainActivity.this.serviceInfo.service_index + 1);
//                            DtvMainActivity.this.timeshift.setChannelName(DtvMainActivity.this.serviceInfo.service_name);
//                            DtvMainActivity.this.timeshift.setCurrentTime("00:00:00");
//                            DtvMainActivity.this.timeshift.setDuration("00:00:00");
//                            DtvMainActivity.this.timeshift.showTimeShiftLogo();
//                            DtvMainActivity.this.timeshift.show();
//                            DtvMainActivity.this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
//                        }
//                    } else {
//                        int ret;
//                        DtvMainActivity.this.storageDeviceInfo.refreshDevice();
//                        spare = DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).spare;
//                        if (spare != null) {
//                            spareSize = StorageDevice.getSpareSize(spare);
//                            if (spareSize != -1 && spareSize < 2097152) {
//                                SETTINGS.makeText(DtvMainActivity.this, R.string.spaceisfull, 0);
//                                return;
//                            }
//                        }
//                        DtvMainActivity.this.stopTeletextSubtitle();
//                        DtvMainActivity.this.record_time = DtvMainActivity.this.record_time * 60000;
//                        if (!SETTINGS.bHongKongDTMB || DtvMainActivity.this.epgCurrentEvent.event_name.length() <= 0) {
//                            ret = MW.pvr_record_start(DtvMainActivity.this.current_service_type, DtvMainActivity.this.current_service_index, DtvMainActivity.this.record_time, DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).Path, null);
//                        } else {
//                            ret = MW.pvr_record_start(DtvMainActivity.this.current_service_type, DtvMainActivity.this.current_service_index, DtvMainActivity.this.record_time, DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).Path, DtvMainActivity.this.epgCurrentEvent.event_name);
//                        }
//                        if (ret == 0) {
//                            DtvMainActivity.this.sCurrentRecordStoreDir = new String(DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no).Path);
//                            DtvMainActivity.this.bInRecording = true;
//                            SETTINGS.makeText(DtvMainActivity.this, R.string.start_record, 0);
//                            DtvMainActivity.this.SendDeviceIsFullMsg();
//                            Log.e("DtvMainActivity", "start recording....");
//                        } else if (ret == 41) {
//                            Log.e("DtvMainActivity", "already recording....");
//                            DtvMainActivity.this.bInRecording = true;
//                            SETTINGS.makeText(DtvMainActivity.this, R.string.already_record, 0);
//                        } else if (ret == 6) {
//                            Log.e("DtvMainActivity", "record failed, not this channel....");
//                            SETTINGS.makeText(DtvMainActivity.this, R.string.record_failed_no_this_channel, 0);
//                        } else if (ret == 30) {
//                            Log.e("DtvMainActivity", "record failed, invalid object....");
//                            SETTINGS.makeText(DtvMainActivity.this, R.string.record_failed_invalid_object, 0);
//                        }
//                    }
//                    DtvMainActivity.this.refreshTeletextSubtitle();
//                }
//                if (DtvMainActivity.this.dia_currentDialog != null) {
//                    DtvMainActivity.this.dia_currentDialog.dismiss();
//                    DtvMainActivity.this.dia_currentDialog = null;
//                }
//                if (DtvMainActivity.this.timeSelectDialog != null) {
//                    DtvMainActivity.this.timeSelectDialog.dismiss();
//                    DtvMainActivity.this.timeSelectDialog = null;
//                }
//                InputTimeDialog.this.dismiss();
//            }
//        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.time_input_dialog);
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != View.INVISIBLE) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            this.mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            this.mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            this.mEditText = (DigitsEditText) findViewById(R.id.dialog_edit_text);
//            this.mEditText.setBackgroundResource(17301529);
            if (DtvMainActivity.this.ftimelimit) {
                this.mEditText.setText(SETTINGS.getCustomTimeShiftLength() + "");
            } else {
                this.mEditText.setText(SETTINGS.getCustomRecordLength() + "");
            }
            this.mEditText.selectAll();
//            this.mBtnCancel.setOnClickListener(new C00501());
//            this.mBtnStart.setOnClickListener(new C00512());
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                        DtvMainActivity.this.ftimelimit = false;
                        break;
                }
            }
            return super.onKeyDown(keyCode, event);
        }

        public InputTimeDialog(Context context, int theme) {
            super(context, theme);
            this.mContext = null;
            this.mBtnStart = null;
            this.mBtnCancel = null;
            this.mEditText = null;
            this.mContext = context;
        }

        protected void onStop() {
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            super.onStop();
        }
    }

    class LOCALMessageHandler extends Handler {
        public LOCALMessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    DtvMainActivity.this.CloseInfoBarDelayed(0);
                case MW.VIDEO_STREAM_TYPE_MPEG4 /*2*/:
                    DtvMainActivity.disableShowPlayStatus = false;
                    DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
                    MW.ts_player_require_play_status();
                case MW.VIDEO_STREAM_TYPE_VC1 /*3*/:
                    DtvMainActivity.this.PlayByChannelNumber();
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    SETTINGS.set_screen_mode();
                case MW.f0xe3564d8 /*5*/:
                    if (DtvMainActivity.this.storageDeviceInfo != null) {
                        DtvMainActivity.this.storageDeviceInfo.refreshDevice();
                        DeviceItem item = DtvMainActivity.this.storageDeviceInfo.getDeviceItem(DtvMainActivity.this.device_no);
                        if (item != null && item.spare != null) {
                            long spareSize = StorageDevice.getSpareSize(item.spare);
                            if (spareSize != -1 && spareSize == 0) {
                                DtvMainActivity.this.ShowToastInformation(DtvMainActivity.this.getResources().getString(R.string.spaceisfull), 0);
                            }
                        }
                    }
                case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
                    if (!DtvMainActivity.this.bUpdate) {
                        DtvMainActivity.this.rl_loading.setVisibility(View.VISIBLE);
                    }
                case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                    DtvMainActivity.disableShowPlayStatus = false;
                default:
            }
        }
    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
//        public void handleMessage(android.os.Message r37) {
//            /*
//            r36 = this;
//            r0 = r37;
//            r0 = r0.what;
//            r31 = r0;
//            r31 = r31 >> 16;
//            r32 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
//            r12 = r31 & r32;
//            r0 = r37;
//            r0 = r0.what;
//            r31 = r0;
//            r32 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
//            r26 = r31 & r32;
//            switch(r12) {
//                case 0: goto L_0x001c;
//                case 1: goto L_0x001b;
//                case 2: goto L_0x01c5;
//                case 3: goto L_0x001b;
//                case 4: goto L_0x01f7;
//                case 5: goto L_0x03d1;
//                case 6: goto L_0x001b;
//                case 7: goto L_0x03ec;
//                case 8: goto L_0x04fa;
//                case 9: goto L_0x001b;
//                case 10: goto L_0x001b;
//                case 11: goto L_0x109d;
//                default: goto L_0x001b;
//            };
//        L_0x001b:
//            return;
//        L_0x001c:
//            r0 = r37;
//            r0 = r0.arg1;
//            r23 = r0;
//            r0 = r37;
//            r0 = r0.arg2;
//            r22 = r0;
//            r31 = th.dtv.DtvMainActivity.disableShowPlayStatus;
//            if (r31 != 0) goto L_0x001b;
//        L_0x002e:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r31 = th.dtv.MW.db_get_current_service_info(r31);
//            if (r31 != 0) goto L_0x0072;
//        L_0x003e:
//            r31 = 1;
//            r0 = r26;
//            r1 = r31;
//            if (r0 == r1) goto L_0x0072;
//        L_0x0046:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r0 = r31;
//            r0 = r0.service_type;
//            r31 = r0;
//            r0 = r31;
//            r1 = r23;
//            if (r0 != r1) goto L_0x001b;
//        L_0x005c:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r0 = r31;
//            r0 = r0.service_index;
//            r31 = r0;
//            r0 = r31;
//            r1 = r22;
//            if (r0 != r1) goto L_0x001b;
//        L_0x0072:
//            r31 = th.dtv.MW.get_system_tuner_config();
//            if (r31 != 0) goto L_0x00aa;
//        L_0x0078:
//            r31 = "dvbs.has.encrychip";
//            r32 = "true";
//            r31 = android.os.SystemProperties.get(r31, r32);
//            r32 = "true";
//            r31 = r31.equals(r32);
//            r32 = 1;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x00aa;
//        L_0x008e:
//            r31 = th.dtv.activity.TestActivity.ns_check_status();
//            if (r31 != 0) goto L_0x00aa;
//        L_0x0094:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.enableRecord = r32;
//            r31 = th.dtv.DtvMainActivity.rl_encryptionStatusBar;
//            r32 = 0;
//            r31.setVisibility(r32);
//            goto L_0x001b;
//        L_0x00aa:
//            switch(r26) {
//                case 0: goto L_0x00af;
//                case 1: goto L_0x00d3;
//                case 2: goto L_0x0116;
//                case 3: goto L_0x014a;
//                case 4: goto L_0x001b;
//                case 5: goto L_0x0179;
//                case 6: goto L_0x019f;
//                default: goto L_0x00ad;
//            };
//        L_0x00ad:
//            goto L_0x001b;
//        L_0x00af:
//            r31 = 1;
//            th.dtv.SETTINGS.set_green_led(r31);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.enableRecord = r32;
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 4;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.ShowSubtitle();
//            goto L_0x001b;
//        L_0x00d3:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.enableRecord = r32;
//            r31 = "NOCH";
//            th.dtv.SETTINGS.send_led_msg(r31);
//            r31 = 0;
//            th.dtv.SETTINGS.set_green_led(r31);
//            r31 = th.dtv.SETTINGS.showdialog;
//            if (r31 == 0) goto L_0x00fb;
//        L_0x00ec:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.ShowDYWTSDialog();
//            r31 = 0;
//            th.dtv.SETTINGS.showdialog = r31;
//            goto L_0x001b;
//        L_0x00fb:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.txv_playStatus;
//            r32 = 2131165393; // 0x7f0700d1 float:1.7945002E38 double:1.0529356063E-314;
//            r31.setText(r32);
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 0;
//            r31.setVisibility(r32);
//            goto L_0x001b;
//        L_0x0116:
//            r31 = 0;
//            th.dtv.SETTINGS.set_green_led(r31);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.enableRecord = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.txv_playStatus;
//            r32 = 2131165396; // 0x7f0700d4 float:1.7945008E38 double:1.052935608E-314;
//            r31.setText(r32);
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.HideSubtitle();
//            goto L_0x001b;
//        L_0x014a:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.enableRecord = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.txv_playStatus;
//            r32 = 2131165397; // 0x7f0700d5 float:1.794501E38 double:1.0529356083E-314;
//            r31.setText(r32);
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.HideSubtitle();
//            goto L_0x001b;
//        L_0x0179:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.enableRecord = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.txv_playStatus;
//            r32 = 2131165399; // 0x7f0700d7 float:1.7945014E38 double:1.0529356093E-314;
//            r31.setText(r32);
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 0;
//            r31.setVisibility(r32);
//            goto L_0x001b;
//        L_0x019f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.enableRecord = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.txv_playStatus;
//            r32 = 2131165316; // 0x7f070084 float:1.7944846E38 double:1.052935568E-314;
//            r31.setText(r32);
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 0;
//            r31.setVisibility(r32);
//            goto L_0x001b;
//        L_0x01c5:
//            r0 = r37;
//            r0 = r0.obj;
//            r19 = r0;
//            r19 = (th.dtv.mw_data.tuner_signal_status) r19;
//            if (r19 == 0) goto L_0x01ee;
//        L_0x01cf:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_infoBar;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x001b;
//        L_0x01df:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r1 = r19;
//            r0.UpdateInfoBarSignalStatus(r1);
//            goto L_0x001b;
//        L_0x01ee:
//            r31 = java.lang.System.out;
//            r32 = "MW.EVENT_TYPE_TUNER_STATUS  ---paramsInfo is null";
//            r31.println(r32);
//            goto L_0x001b;
//        L_0x01f7:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_infoBar;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x032b;
//        L_0x0207:
//            r0 = r37;
//            r0 = r0.obj;
//            r19 = r0;
//            r19 = (th.dtv.mw_data.date_time) r19;
//            if (r19 == 0) goto L_0x032b;
//        L_0x0211:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r31;
//            r0 = r0.minute;
//            r31 = r0;
//            r0 = r19;
//            r0 = r0.minute;
//            r32 = r0;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x029d;
//        L_0x022d:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r31;
//            r0 = r0.hour;
//            r31 = r0;
//            r0 = r19;
//            r0 = r0.hour;
//            r32 = r0;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x029d;
//        L_0x0249:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r31;
//            r0 = r0.day;
//            r31 = r0;
//            r0 = r19;
//            r0 = r0.day;
//            r32 = r0;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x029d;
//        L_0x0265:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r31;
//            r0 = r0.month;
//            r31 = r0;
//            r0 = r19;
//            r0 = r0.month;
//            r32 = r0;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x029d;
//        L_0x0281:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r31;
//            r0 = r0.year;
//            r31 = r0;
//            r0 = r19;
//            r0 = r0.year;
//            r32 = r0;
//            r0 = r31;
//            r1 = r32;
//            if (r0 == r1) goto L_0x032b;
//        L_0x029d:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r19;
//            r0 = r0.year;
//            r32 = r0;
//            r0 = r32;
//            r1 = r31;
//            r1.year = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r19;
//            r0 = r0.month;
//            r32 = r0;
//            r0 = r32;
//            r1 = r31;
//            r1.month = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r19;
//            r0 = r0.day;
//            r32 = r0;
//            r0 = r32;
//            r1 = r31;
//            r1.day = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r19;
//            r0 = r0.hour;
//            r32 = r0;
//            r0 = r32;
//            r1 = r31;
//            r1.hour = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.dateTime;
//            r0 = r19;
//            r0 = r0.minute;
//            r32 = r0;
//            r0 = r32;
//            r1 = r31;
//            r1.minute = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.txv_infBar_dateTime;
//            r32 = 0;
//            r0 = r19;
//            r1 = r32;
//            r32 = th.dtv.SETTINGS.getDateTimeAdjustSystem(r0, r1);
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.UpdateCurrentNextEpgInfo();
//        L_0x032b:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_channelList;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x001b;
//        L_0x033b:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.bPwdShow;
//            if (r31 != 0) goto L_0x001b;
//        L_0x0347:
//            r31 = th.dtv.SETTINGS.bPass;
//            if (r31 != 0) goto L_0x001b;
//        L_0x034b:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r31 = th.dtv.MW.db_get_current_service_info(r31);
//            if (r31 != 0) goto L_0x001b;
//        L_0x035b:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.epgCurrentEvent;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.epgNextEvent;
//            r31 = th.dtv.MW.epg_get_pf_event(r31, r32, r33);
//            if (r31 != 0) goto L_0x03c4;
//        L_0x037f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.epgCurrentEvent;
//            r0 = r31;
//            r0 = r0.parental_rating;
//            r31 = r0;
//            r31 = r31 + 4;
//            r32 = th.dtv.SETTINGS.get_parental_rating_value();
//            r0 = r31;
//            r1 = r32;
//            if (r0 <= r1) goto L_0x03b7;
//        L_0x039b:
//            r31 = th.dtv.SETTINGS.get_parental_rating_value();
//            r32 = 19;
//            r0 = r31;
//            r1 = r32;
//            if (r0 == r1) goto L_0x001b;
//        L_0x03a7:
//            th.dtv.MW.ts_player_stop_play();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = "30";
//            r31.CheckChannelLockAndShowPasswd(r32);
//            goto L_0x001b;
//        L_0x03b7:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.bPwdShow = r32;
//            goto L_0x001b;
//        L_0x03c4:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.bPwdShow = r32;
//            goto L_0x001b;
//        L_0x03d1:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_infoBar;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x001b;
//        L_0x03e1:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.UpdateCurrentNextEpgInfo();
//            goto L_0x001b;
//        L_0x03ec:
//            if (r26 != 0) goto L_0x04d4;
//        L_0x03ee:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.inTeletextMode;
//            if (r31 != 0) goto L_0x0406;
//        L_0x03fa:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.inSubtitleMode;
//            if (r31 == 0) goto L_0x045f;
//        L_0x0406:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_loading;
//            r32 = 8;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.inTeletextMode;
//            if (r31 == 0) goto L_0x045f;
//        L_0x0421:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.bRefresh;
//            if (r31 == 0) goto L_0x044f;
//        L_0x042d:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.et_ttxPageNumber;
//            r32 = th.dtv.MW.teletext_get_current_page();
//            r32 = java.lang.Integer.toString(r32);
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.et_ttxPageNumber;
//            r31.selectAll();
//        L_0x044f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.ttxSubPageInfo;
//            r31 = th.dtv.MW.teletext_get_sub_page_info(r31);
//            if (r31 != 0) goto L_0x045f;
//        L_0x045f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.inTeletextMode;
//            if (r31 == 0) goto L_0x04b3;
//        L_0x046b:
//            r31 = 1;
//            th.dtv.DtvMainActivity.disableShowPlayStatus = r31;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.bTeletextShowed;
//            if (r31 != 0) goto L_0x0487;
//        L_0x047c:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.bTeletextShowed = r32;
//        L_0x0487:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.et_ttxPageNumber;
//            r31 = r31.getVisibility();
//            if (r31 == 0) goto L_0x04b3;
//        L_0x0497:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.et_ttxPageNumber;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.et_ttxPageNumber;
//            r31.requestFocus();
//        L_0x04b3:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.ttx_sub;
//            r31.update();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.bUpdate = r32;
//            r31 = "DtvMainActivity";
//            r32 = "EVENT_TYPE_TELETEXT_SUBTITLE_UPDATE ";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x04d4:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.inTeletextMode;
//            if (r31 == 0) goto L_0x001b;
//        L_0x04e0:
//            r31 = 1;
//            r0 = r26;
//            r1 = r31;
//            if (r0 != r1) goto L_0x001b;
//        L_0x04e8:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.ttxSubPageInfo;
//            r31 = th.dtv.MW.teletext_get_sub_page_info(r31);
//            if (r31 != 0) goto L_0x001b;
//        L_0x04f8:
//            goto L_0x001b;
//        L_0x04fa:
//            switch(r26) {
//                case 0: goto L_0x04ff;
//                case 1: goto L_0x0cca;
//                case 2: goto L_0x0f1a;
//                case 3: goto L_0x0fd9;
//                case 4: goto L_0x04fd;
//                case 5: goto L_0x04fd;
//                case 6: goto L_0x04fd;
//                case 7: goto L_0x04fd;
//                case 8: goto L_0x04fd;
//                case 9: goto L_0x04fd;
//                case 10: goto L_0x0551;
//                case 11: goto L_0x0caf;
//                case 12: goto L_0x05ab;
//                case 13: goto L_0x08fb;
//                case 14: goto L_0x0957;
//                case 15: goto L_0x0ca6;
//                default: goto L_0x04fd;
//            };
//        L_0x04fd:
//            goto L_0x001b;
//        L_0x04ff:
//            r31 = "DtvMainActivity";
//            r32 = "PVR start record ";
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.bInit_timeShift;
//            r31 = r0;
//            if (r31 != 0) goto L_0x001b;
//        L_0x0514:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 2131165425; // 0x7f0700f1 float:1.7945067E38 double:1.052935622E-314;
//            r33 = 0;
//            th.dtv.SETTINGS.makeText(r31, r32, r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pvr_time_txt;
//            r32 = "00:00:00";
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pvr_time_txt;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.ll_pvrInfo;
//            r32 = 0;
//            r31.setVisibility(r32);
//            goto L_0x001b;
//        L_0x0551:
//            r0 = r37;
//            r13 = r0.arg1;
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "PVR_TIMESHIFT_STATUS_INIT time : ";
//            r32 = r32.append(r33);
//            r0 = r32;
//            r32 = r0.append(r13);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r0 = r32;
//            r1 = r31;
//            r1.bInit_timeShift = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.one_time = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 4;
//            r31.timeShift_PlayStatus = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r13;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.init_time = r13;
//            goto L_0x001b;
//        L_0x05ab:
//            r31 = 0;
//            th.dtv.DtvMainActivity.disableShowPlayStatus = r31;
//            th.dtv.MW.ts_player_require_play_status();
//            r0 = r37;
//            r8 = r0.arg1;
//            r0 = r37;
//            r0 = r0.arg2;
//            r30 = r0;
//            r5 = 0;
//            r6 = 0;
//            r7 = 0;
//            if (r8 != 0) goto L_0x0885;
//        L_0x05c2:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.init_time = r31.init_time + 1;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.init_time;
//            r8 = r31 / 3;
//        L_0x05d7:
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "PVR timeshift update time : ";
//            r32 = r32.append(r33);
//            r0 = r32;
//            r32 = r0.append(r8);
//            r33 = " / ";
//            r32 = r32.append(r33);
//            r0 = r32;
//            r1 = r30;
//            r32 = r0.append(r1);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "PVR timeshift previous time  : ";
//            r32 = r32.append(r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.pre_time;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "PVR timeshift init time  : ";
//            r32 = r32.append(r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.init_time;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r31 = th.dtv.SETTINGS.getTimeShiftTime();
//            r0 = r30;
//            r1 = r31;
//            if (r0 < r1) goto L_0x08ac;
//        L_0x064d:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.bSeek;
//            if (r31 != 0) goto L_0x08ac;
//        L_0x0659:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pre_time;
//            r0 = r31;
//            if (r8 >= r0) goto L_0x089f;
//        L_0x0667:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.timeShift_PlayStatus;
//            r32 = 4;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0892;
//        L_0x0679:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r8 = r31.pre_time;
//        L_0x0683:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.seektime = r8;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r7 = r8 % 60;
//            r0 = r31;
//            r0.m_second = r7;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = r8 / 60;
//            r6 = r32 % 60;
//            r0 = r31;
//            r0.m_minute = r6;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r5 = r8 / 3600;
//            r0 = r31;
//            r0.m_hour = r5;
//            r27 = 0;
//            r28 = 0;
//            r29 = 0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r1 = r30;
//            r0.totaltime = r1;
//            r29 = r30 % 60;
//            r31 = r30 / 60;
//            r28 = r31 % 60;
//            r0 = r30;
//            r0 = r0 / 3600;
//            r27 = r0;
//            r2 = th.dtv.SETTINGS.getTimeShiftTime();
//            r9 = 0;
//            r10 = 0;
//            r11 = 0;
//            r11 = r2 % 60;
//            r31 = r2 / 60;
//            r10 = r31 % 60;
//            r9 = r2 / 3600;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            if (r31 == 0) goto L_0x0861;
//        L_0x06f3:
//            r31 = java.lang.System.out;
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "bhandlemediakey:";
//            r32 = r32.append(r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.bhandlemediakey;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            r31.println(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = "%02d:%02d:%02d";
//            r33 = 3;
//            r0 = r33;
//            r0 = new java.lang.Object[r0];
//            r33 = r0;
//            r34 = 0;
//            r35 = java.lang.Integer.valueOf(r5);
//            r33[r34] = r35;
//            r34 = 1;
//            r35 = java.lang.Integer.valueOf(r6);
//            r33[r34] = r35;
//            r34 = 2;
//            r35 = java.lang.Integer.valueOf(r7);
//            r33[r34] = r35;
//            r32 = java.lang.String.format(r32, r33);
//            r31.setCurrentTime(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = "%02d:%02d:%02d";
//            r33 = 3;
//            r0 = r33;
//            r0 = new java.lang.Object[r0];
//            r33 = r0;
//            r34 = 0;
//            r35 = java.lang.Integer.valueOf(r9);
//            r33[r34] = r35;
//            r34 = 1;
//            r35 = java.lang.Integer.valueOf(r10);
//            r33[r34] = r35;
//            r34 = 2;
//            r35 = java.lang.Integer.valueOf(r11);
//            r33[r34] = r35;
//            r32 = java.lang.String.format(r32, r33);
//            r31.setDuration(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r0.setSeekBarMax(r2);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r1 = r30;
//            r0.setSeekBarSecondProgress(r1);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r0.setSeekBarProgress(r8);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 4;
//            r33 = 0;
//            r31.showPlayStatusAndSpeed(r32, r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.timeShift_PlayStatus;
//            r32 = 4;
//            r0 = r31;
//            r1 = r32;
//            if (r0 == r1) goto L_0x08c4;
//        L_0x07d9:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.timeShift_PlayStatus;
//            r31.Temp_timeShift_PlayStatus = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.timeshift_speed;
//            r31.Temp_timeshift_speed = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 4;
//            r31.timeShift_PlayStatus = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.timeshift_speed = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r31.showTimeShiftLogo();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 4;
//            r33 = 0;
//            r31.showPlayStatusAndSpeed(r32, r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.one_time;
//            if (r31 != 0) goto L_0x0861;
//        L_0x0843:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = th.dtv.SETTINGS.get_banner_time();
//            r31.hideTimeshiftInfoView(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.one_time = r32;
//        L_0x0861:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.enableRecord;
//            r32 = 1;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x087c;
//        L_0x0873:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.ShowSubtitle();
//        L_0x087c:
//            r31 = "LEE";
//            r32 = "MW.PVR_TIMESHIFT_STATUS_PLAY";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0885:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.init_time = r32;
//            goto L_0x05d7;
//        L_0x0892:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0683;
//        L_0x089f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0683;
//        L_0x08ac:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.bSeek = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0683;
//        L_0x08c4:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.timeshift_speed = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.one_time;
//            if (r31 != 0) goto L_0x0861;
//        L_0x08db:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = th.dtv.SETTINGS.get_banner_time();
//            r31.hideTimeshiftInfoView(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.one_time = r32;
//            goto L_0x0861;
//        L_0x08fb:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            if (r31 == 0) goto L_0x001b;
//        L_0x0909:
//            r0 = r37;
//            r0 = r0.arg2;
//            r30 = r0;
//            r31 = 1;
//            th.dtv.DtvMainActivity.disableShowPlayStatus = r31;
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 4;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 3;
//            r31.timeShift_PlayStatus = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 3;
//            r33 = 0;
//            r31.showPlayStatusAndSpeed(r32, r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r1 = r30;
//            r0.setSeekBarSecondProgress(r1);
//            r31 = "TAG";
//            r32 = "PVR_TIMESHIFT_STATUS_PAUSE";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0957:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            if (r31 == 0) goto L_0x001b;
//        L_0x0965:
//            r31 = 1;
//            th.dtv.DtvMainActivity.disableShowPlayStatus = r31;
//            r31 = th.dtv.DtvMainActivity.rl_playStatusBar;
//            r32 = 4;
//            r31.setVisibility(r32);
//            r0 = r37;
//            r8 = r0.arg1;
//            r0 = r37;
//            r0 = r0.arg2;
//            r30 = r0;
//            r15 = 0;
//            r18 = 0;
//            r21 = 0;
//            r27 = 0;
//            r28 = 0;
//            r29 = 0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r1 = r30;
//            r0.totaltime = r1;
//            r29 = r30 % 60;
//            r31 = r30 / 60;
//            r28 = r31 % 60;
//            r0 = r30;
//            r0 = r0 / 3600;
//            r27 = r0;
//            r21 = r8 % 60;
//            r31 = r8 / 60;
//            r18 = r31 % 60;
//            r15 = r8 / 3600;
//            r2 = th.dtv.SETTINGS.getTimeShiftTime();
//            r9 = 0;
//            r10 = 0;
//            r11 = 0;
//            r11 = r2 % 60;
//            r31 = r2 / 60;
//            r10 = r31 % 60;
//            r9 = r2 / 3600;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            if (r31 == 0) goto L_0x0ab4;
//        L_0x09c6:
//            r31 = "LEE";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "timeShift_PlayStatus 1111:::::";
//            r32 = r32.append(r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.timeShift_PlayStatus;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = "%02d:%02d:%02d";
//            r33 = 3;
//            r0 = r33;
//            r0 = new java.lang.Object[r0];
//            r33 = r0;
//            r34 = 0;
//            r35 = java.lang.Integer.valueOf(r15);
//            r33[r34] = r35;
//            r34 = 1;
//            r35 = java.lang.Integer.valueOf(r18);
//            r33[r34] = r35;
//            r34 = 2;
//            r35 = java.lang.Integer.valueOf(r21);
//            r33[r34] = r35;
//            r32 = java.lang.String.format(r32, r33);
//            r31.setCurrentTime(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = "%02d:%02d:%02d";
//            r33 = 3;
//            r0 = r33;
//            r0 = new java.lang.Object[r0];
//            r33 = r0;
//            r34 = 0;
//            r35 = java.lang.Integer.valueOf(r9);
//            r33[r34] = r35;
//            r34 = 1;
//            r35 = java.lang.Integer.valueOf(r10);
//            r33[r34] = r35;
//            r34 = 2;
//            r35 = java.lang.Integer.valueOf(r11);
//            r33[r34] = r35;
//            r32 = java.lang.String.format(r32, r33);
//            r31.setDuration(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r0.setSeekBarMax(r2);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r1 = r30;
//            r0.setSeekBarSecondProgress(r1);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r0 = r31;
//            r0.setSeekBarProgress(r8);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.timeShift_PlayStatus;
//            r32 = 2;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0b0c;
//        L_0x0a99:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 2;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.timeshift_speed;
//            r31.showPlayStatusAndSpeed(r32, r33);
//        L_0x0ab4:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.timeShift_PlayStatus;
//            r32 = 4;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0c99;
//        L_0x0ac6:
//            r31 = "LEE";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "3 tmep timeShift_palystatus =========";
//            r32 = r32.append(r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.Temp_timeShift_PlayStatus;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.special_case;
//            if (r31 == 0) goto L_0x0b3b;
//        L_0x0af4:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.special_case = r32;
//            goto L_0x001b;
//        L_0x0b0c:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.timeShift_PlayStatus;
//            r32 = 1;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0ab4;
//        L_0x0b1e:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 1;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.timeshift_speed;
//            r31.showPlayStatusAndSpeed(r32, r33);
//            goto L_0x0ab4;
//        L_0x0b3b:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.Temp_timeShift_PlayStatus;
//            r32 = 2;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0c10;
//        L_0x0b4d:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 2;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.Temp_timeshift_speed;
//            r31.showPlayStatusAndSpeed(r32, r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 2;
//            r31.timeShift_PlayStatus = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.Temp_timeshift_speed;
//            r31.timeshift_speed = r32;
//            r31 = th.dtv.SETTINGS.getTimeShiftTime();
//            r0 = r30;
//            r1 = r31;
//            if (r0 < r1) goto L_0x0c04;
//        L_0x0b90:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pre_time;
//            r0 = r31;
//            if (r8 >= r0) goto L_0x0bf8;
//        L_0x0b9e:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r8 = r31.pre_time;
//        L_0x0ba8:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.one_time = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 0;
//            r31.hideTimeshiftInfoView(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r31.showTimeshiftInfoView();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.HideSubtitle();
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "PVR_TIMESHIFT_STATUS_FFFB time : ";
//            r32 = r32.append(r33);
//            r0 = r32;
//            r32 = r0.append(r8);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0bf8:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0ba8;
//        L_0x0c04:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0ba8;
//        L_0x0c10:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.Temp_timeShift_PlayStatus;
//            r32 = 1;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0ba8;
//        L_0x0c22:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r32 = 1;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.Temp_timeshift_speed;
//            r31.showPlayStatusAndSpeed(r32, r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.timeShift_PlayStatus = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.Temp_timeshift_speed;
//            r31.timeshift_speed = r32;
//            r31 = th.dtv.SETTINGS.getTimeShiftTime();
//            r0 = r30;
//            r1 = r31;
//            if (r0 < r1) goto L_0x0c8c;
//        L_0x0c65:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pre_time;
//            r0 = r31;
//            if (r8 <= r0) goto L_0x0c7f;
//        L_0x0c73:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r8 = r31.pre_time;
//            goto L_0x0ba8;
//        L_0x0c7f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0ba8;
//        L_0x0c8c:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0ba8;
//        L_0x0c99:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0.pre_time = r8;
//            goto L_0x0ba8;
//        L_0x0ca6:
//            r31 = "TAG";
//            r32 = "PVR_PLAYBACK_STATUS_SEARCH_OK";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0caf:
//            r31 = "TAG";
//            r32 = "PVR_PLAYBACK_STATUS_EXIT";
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.RemoveDeviceIsFullMsg();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.ShowSubtitle();
//            goto L_0x001b;
//        L_0x0cca:
//            r31 = "DtvMainActivity";
//            r32 = "PVR stop record ";
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.bInit_timeShift;
//            r31 = r0;
//            r32 = 1;
//            r0 = r31;
//            r1 = r32;
//            if (r0 != r1) goto L_0x0d7c;
//        L_0x0ce5:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r0 = r32;
//            r1 = r31;
//            r1.bInit_timeShift = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.bhandlemediakey = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 2131165435; // 0x7f0700fb float:1.7945087E38 double:1.052935627E-314;
//            r33 = 0;
//            th.dtv.SETTINGS.makeText(r31, r32, r33);
//            r31 = 1;
//            th.dtv.DtvMainActivity.disableShowPlayStatus = r31;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            if (r31 == 0) goto L_0x0d3c;
//        L_0x0d1f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.timeshift;
//            r31 = r0;
//            r31.dismiss();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r0 = r32;
//            r1 = r31;
//            r1.timeshift = r0;
//        L_0x0d3c:
//            th.dtv.MW.ts_player_stop_play();
//            r31 = 1;
//            r32 = 0;
//            th.dtv.MW.ts_player_play_current(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.TimeShiftEndDelayShowPlayStatus();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.showInfoBar();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.btimeshift = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.bInRecording = r32;
//            r31 = 0;
//            th.dtv.DtvMainActivity.disableShowPlayStatus = r31;
//            r31 = 0;
//            r32 = 1;
//            th.dtv.MW.register_event_type(r31, r32);
//            goto L_0x001b;
//        L_0x0d7c:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pvr_time_txt;
//            r32 = 4;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.ll_pvrInfo;
//            r32 = 4;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.recorddialog;
//            r31 = r0;
//            if (r31 == 0) goto L_0x0dc9;
//        L_0x0da8:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.recorddialog;
//            r31 = r0;
//            r31 = r31.isShowing();
//            if (r31 == 0) goto L_0x0dc9;
//        L_0x0dba:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r31;
//            r0 = r0.recorddialog;
//            r31 = r0;
//            r31.dismiss();
//        L_0x0dc9:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.bInRecording;
//            if (r31 == 0) goto L_0x0eb9;
//        L_0x0dd5:
//            r31 = th.dtv.SETTINGS.bBookRec;
//            if (r31 == 0) goto L_0x0e76;
//        L_0x0dd9:
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "record finish,switch channel to ";
//            r32 = r32.append(r33);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r33 = r0;
//            r33 = r33.rec_pre_service_index;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            android.util.Log.i(r31, r32);
//            r31 = 0;
//            th.dtv.SETTINGS.bBookRec = r31;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rec_pre_service_index;
//            th.dtv.SETTINGS.set_led_channel(r31);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rec_pre_service_type;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.rec_pre_service_index;
//            r33 = 0;
//            r34 = 0;
//            r3 = th.dtv.MW.ts_player_play(r31, r32, r33, r34);
//            if (r3 != 0) goto L_0x0ec4;
//        L_0x0e2a:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r31 = th.dtv.MW.db_get_current_service_info(r31);
//            if (r31 != 0) goto L_0x0e60;
//        L_0x0e3a:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.rec_pre_service_type;
//            r31.current_service_type = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.rec_pre_service_index;
//            r31.current_service_index = r32;
//        L_0x0e60:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.channelListAdapter;
//            r31.notifyDataSetChanged();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.CheckParentLockAndShowPasswd();
//        L_0x0e76:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.book_enableRecord;
//            if (r31 != 0) goto L_0x0ea6;
//        L_0x0e82:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.bInRecording = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 0;
//            r31.sCurrentRecordStoreDir = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 2131165426; // 0x7f0700f2 float:1.7945069E38 double:1.0529356226E-314;
//            r33 = 0;
//            th.dtv.SETTINGS.makeText(r31, r32, r33);
//        L_0x0ea6:
//            r31 = th.dtv.SETTINGS.bFromRTC;
//            if (r31 == 0) goto L_0x0eb9;
//        L_0x0eaa:
//            r31 = th.dtv.DtvService.checkNextBookEventWillCome();
//            if (r31 != 0) goto L_0x0eb9;
//        L_0x0eb0:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.ShowBookEndCountdownDialog();
//        L_0x0eb9:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.RemoveDeviceIsFullMsg();
//            goto L_0x001b;
//        L_0x0ec4:
//            r31 = 44;
//            r0 = r31;
//            if (r3 != r0) goto L_0x0e76;
//        L_0x0eca:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r31 = th.dtv.MW.db_get_current_service_info(r31);
//            if (r31 != 0) goto L_0x0f00;
//        L_0x0eda:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.rec_pre_service_type;
//            r31.current_service_type = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.rec_pre_service_index;
//            r31.current_service_index = r32;
//        L_0x0f00:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.channelListAdapter;
//            r31.notifyDataSetChanged();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = "10";
//            r31.CheckChannelLockAndShowPasswd(r32);
//            goto L_0x0e76;
//        L_0x0f1a:
//            r0 = r37;
//            r0 = r0.arg1;
//            r31 = r0;
//            r20 = r31 % 60;
//            r0 = r37;
//            r0 = r0.arg1;
//            r31 = r0;
//            r31 = r31 / 60;
//            r17 = r31 % 60;
//            r0 = r37;
//            r0 = r0.arg1;
//            r31 = r0;
//            r0 = r31;
//            r14 = r0 / 3600;
//            r31 = "DtvMainActivity";
//            r32 = new java.lang.StringBuilder;
//            r32.<init>();
//            r33 = "PVR update time : ";
//            r32 = r32.append(r33);
//            r0 = r37;
//            r0 = r0.arg1;
//            r33 = r0;
//            r32 = r32.append(r33);
//            r32 = r32.toString();
//            android.util.Log.e(r31, r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = 1;
//            r31.bInRecording = r32;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.bInRecording;
//            if (r31 == 0) goto L_0x0fce;
//        L_0x0f6b:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pvr_time_txt;
//            r32 = "%02d:%02d:%02d";
//            r33 = 3;
//            r0 = r33;
//            r0 = new java.lang.Object[r0];
//            r33 = r0;
//            r34 = 0;
//            r35 = java.lang.Integer.valueOf(r14);
//            r33[r34] = r35;
//            r34 = 1;
//            r35 = java.lang.Integer.valueOf(r17);
//            r33[r34] = r35;
//            r34 = 2;
//            r35 = java.lang.Integer.valueOf(r20);
//            r33[r34] = r35;
//            r32 = java.lang.String.format(r32, r33);
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pvr_time_txt;
//            r31 = r31.getVisibility();
//            if (r31 == 0) goto L_0x001b;
//        L_0x0fae:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.pvr_time_txt;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.ll_pvrInfo;
//            r32 = 0;
//            r31.setVisibility(r32);
//            goto L_0x001b;
//        L_0x0fce:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.HidePvrView();
//            goto L_0x001b;
//        L_0x0fd9:
//            r0 = r37;
//            r0 = r0.arg1;
//            r31 = r0;
//            switch(r31) {
//                case 46: goto L_0x0feb;
//                case 47: goto L_0x0ff4;
//                case 48: goto L_0x0ffd;
//                case 49: goto L_0x1006;
//                case 50: goto L_0x1082;
//                case 51: goto L_0x108b;
//                case 52: goto L_0x1094;
//                default: goto L_0x0fe2;
//            };
//        L_0x0fe2:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : unknown error";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0feb:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : create thread failed";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0ff4:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : busy";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x0ffd:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : can not open file";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x1006:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.storageDeviceInfo;
//            r31.refreshDevice();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.storageDeviceInfo;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.device_no;
//            r16 = r31.getDeviceItem(r32);
//            if (r16 == 0) goto L_0x107a;
//        L_0x102d:
//            r0 = r16;
//            r0 = r0.spare;
//            r31 = r0;
//            if (r31 == 0) goto L_0x1068;
//        L_0x1035:
//            r0 = r16;
//            r0 = r0.spare;
//            r31 = r0;
//            r24 = th.dtv.StorageDevice.getSpareSize(r31);
//            r31 = -1;
//            r31 = (r24 > r31 ? 1 : (r24 == r31 ? 0 : -1));
//            if (r31 == 0) goto L_0x1068;
//        L_0x1045:
//            r31 = 2097152; // 0x200000 float:2.938736E-39 double:1.0361308E-317;
//            r31 = (r24 > r31 ? 1 : (r24 == r31 ? 0 : -1));
//            if (r31 >= 0) goto L_0x1068;
//        L_0x104c:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.getResources();
//            r33 = 2131165482; // 0x7f07012a float:1.7945182E38 double:1.0529356503E-314;
//            r32 = r32.getString(r33);
//            r33 = 0;
//            r31.ShowToastInformation(r32, r33);
//        L_0x1068:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.SendDeviceIsFullMsg();
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : can not write file";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x107a:
//            r31 = "DtvMainActivity";
//            r32 = "item  == null";
//            android.util.Log.e(r31, r32);
//            goto L_0x1068;
//        L_0x1082:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : can not access file";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x108b:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : file too large";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x1094:
//            r31 = "DtvMainActivity";
//            r32 = "PVR record error : dvr error";
//            android.util.Log.e(r31, r32);
//            goto L_0x001b;
//        L_0x109d:
//            switch(r26) {
//                case 0: goto L_0x10a2;
//                case 1: goto L_0x10f9;
//                case 2: goto L_0x1187;
//                default: goto L_0x10a0;
//            };
//        L_0x10a0:
//            goto L_0x001b;
//        L_0x10a2:
//            r0 = r37;
//            r4 = r0.arg1;
//            r32 = "DtvMainActivity";
//            r31 = new java.lang.StringBuilder;
//            r31.<init>();
//            r33 = "service name update : ";
//            r0 = r31;
//            r1 = r33;
//            r33 = r0.append(r1);
//            r31 = 1;
//            r0 = r31;
//            if (r4 != r0) goto L_0x10f6;
//        L_0x10bd:
//            r31 = "current service changed";
//        L_0x10bf:
//            r0 = r33;
//            r1 = r31;
//            r31 = r0.append(r1);
//            r31 = r31.toString();
//            r0 = r32;
//            r1 = r31;
//            android.util.Log.e(r0, r1);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.showInfoBar();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_channelList;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x001b;
//        L_0x10eb:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.RefreshShowChannelList();
//            goto L_0x001b;
//        L_0x10f6:
//            r31 = "current service not changed";
//            goto L_0x10bf;
//        L_0x10f9:
//            r0 = r37;
//            r4 = r0.arg1;
//            r32 = "DtvMainActivity";
//            r31 = new java.lang.StringBuilder;
//            r31.<init>();
//            r33 = "service params update : ";
//            r0 = r31;
//            r1 = r33;
//            r33 = r0.append(r1);
//            r31 = 1;
//            r0 = r31;
//            if (r4 != r0) goto L_0x116a;
//        L_0x1114:
//            r31 = "current service changed";
//        L_0x1116:
//            r0 = r33;
//            r1 = r31;
//            r31 = r0.append(r1);
//            r31 = r31.toString();
//            r0 = r32;
//            r1 = r31;
//            android.util.Log.e(r0, r1);
//            r31 = 1;
//            r0 = r31;
//            if (r4 != r0) goto L_0x1146;
//        L_0x112f:
//            r31 = th.dtv.SETTINGS.bPass;
//            if (r31 != 0) goto L_0x117f;
//        L_0x1133:
//            r31 = 0;
//            r32 = 0;
//            r3 = th.dtv.MW.ts_player_play_current(r31, r32);
//            if (r3 != 0) goto L_0x116d;
//        L_0x113d:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.CheckParentLockAndShowPasswd();
//        L_0x1146:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.showInfoBar();
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_channelList;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x001b;
//        L_0x115f:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.RefreshShowChannelList();
//            goto L_0x001b;
//        L_0x116a:
//            r31 = "current service not changed";
//            goto L_0x1116;
//        L_0x116d:
//            r31 = 44;
//            r0 = r31;
//            if (r3 != r0) goto L_0x1146;
//        L_0x1173:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r32 = "21";
//            r31.CheckChannelLockAndShowPasswd(r32);
//            goto L_0x1146;
//        L_0x117f:
//            r31 = 1;
//            r32 = 0;
//            th.dtv.MW.ts_player_play_current(r31, r32);
//            goto L_0x1146;
//        L_0x1187:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_infoBar;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x1246;
//        L_0x1197:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r31 = th.dtv.MW.db_get_current_service_info(r31);
//            if (r31 != 0) goto L_0x1246;
//        L_0x11a7:
//            r31 = th.dtv.SETTINGS.bHongKongDTMB;
//            if (r31 == 0) goto L_0x12a8;
//        L_0x11ab:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r0 = r31;
//            r0 = r0.video_pid;
//            r31 = r0;
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r32 = r0;
//            r32 = r32.serviceInfo;
//            r0 = r32;
//            r0 = r0.audio_info;
//            r32 = r0;
//            r33 = 0;
//            r32 = r32[r33];
//            r0 = r32;
//            r0 = r0.audio_pid;
//            r32 = r0;
//            r31 = th.dtv.util.HongKongDTMBHD.isHongKongDTMBHDChannel(r31, r32);
//            if (r31 != 0) goto L_0x11ed;
//        L_0x11db:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r0 = r31;
//            r0 = r0.is_hd;
//            r31 = r0;
//            if (r31 == 0) goto L_0x1261;
//        L_0x11ed:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2131165204; // 0x7f070014 float:1.7944619E38 double:1.052935513E-314;
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r31 = r31.getPaint();
//            r32 = 1;
//            r31.setFakeBoldText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 0;
//            r33 = 0;
//            r34 = 0;
//            r32 = android.graphics.Color.rgb(r32, r33, r34);
//            r31.setTextColor(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2130837593; // 0x7f020059 float:1.7280144E38 double:1.0527736516E-314;
//            r31.setBackgroundResource(r32);
//        L_0x1246:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.rl_channelList;
//            r31 = r31.getVisibility();
//            if (r31 != 0) goto L_0x001b;
//        L_0x1256:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31.RefreshShowChannelList();
//            goto L_0x001b;
//        L_0x1261:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2131165205; // 0x7f070015 float:1.794462E38 double:1.0529355134E-314;
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2130837597; // 0x7f02005d float:1.7280153E38 double:1.0527736535E-314;
//            r31.setBackgroundResource(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 255; // 0xff float:3.57E-43 double:1.26E-321;
//            r33 = 255; // 0xff float:3.57E-43 double:1.26E-321;
//            r34 = 255; // 0xff float:3.57E-43 double:1.26E-321;
//            r32 = android.graphics.Color.rgb(r32, r33, r34);
//            r31.setTextColor(r32);
//            goto L_0x1246;
//        L_0x12a8:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.serviceInfo;
//            r0 = r31;
//            r0 = r0.is_hd;
//            r31 = r0;
//            if (r31 == 0) goto L_0x1315;
//        L_0x12ba:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2131165204; // 0x7f070014 float:1.7944619E38 double:1.052935513E-314;
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r31 = r31.getPaint();
//            r32 = 1;
//            r31.setFakeBoldText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 0;
//            r33 = 0;
//            r34 = 0;
//            r32 = android.graphics.Color.rgb(r32, r33, r34);
//            r31.setTextColor(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2130837593; // 0x7f020059 float:1.7280144E38 double:1.0527736516E-314;
//            r31.setBackgroundResource(r32);
//            goto L_0x1246;
//        L_0x1315:
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 0;
//            r31.setVisibility(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2131165205; // 0x7f070015 float:1.794462E38 double:1.0529355134E-314;
//            r31.setText(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 2130837597; // 0x7f02005d float:1.7280153E38 double:1.0527736535E-314;
//            r31.setBackgroundResource(r32);
//            r0 = r36;
//            r0 = th.dtv.DtvMainActivity.this;
//            r31 = r0;
//            r31 = r31.infoBar_hd;
//            r32 = 255; // 0xff float:3.57E-43 double:1.26E-321;
//            r33 = 255; // 0xff float:3.57E-43 double:1.26E-321;
//            r34 = 255; // 0xff float:3.57E-43 double:1.26E-321;
//            r32 = android.graphics.Color.rgb(r32, r33, r34);
//            r31.setTextColor(r32);
//            goto L_0x1246;
//            */
//            throw new UnsupportedOperationException("Method not decompiled: th.dtv.DtvMainActivity.MWmessageHandler.handleMessage(android.os.Message):void");
//        }
    }

    public class RegionSelectDialog extends Dialog {
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.DtvMainActivity.RegionSelectDialog.1 */
        class C00521 implements OnItemClickListener {
            C00521() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                System.out.println("RegionSelectDialog onItemClick");
                MW.db_service_set_in_region_mode();
                if (MW.db_set_service_region_current_pos(position)) {
                    System.out.println("set satellite mode ");
                    int Ret = MW.ts_player_play_current(false, false);
                    if (Ret == 0) {
                        if (SETTINGS.bPass) {
                            MW.ts_player_play_current(true, false);
                        } else {
                            DtvMainActivity.this.CheckParentLockAndShowPasswd();
                        }
                    } else if (Ret == 44) {
                        if (SETTINGS.bPass) {
                            MW.ts_player_play_current(true, false);
                        } else {
                            DtvMainActivity.this.CheckChannelLockAndShowPasswd("12");
                        }
                    }
                }
                DtvMainActivity.this.RefreshShowChannelList();
                RegionSelectDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            DtvMainActivity.this.dia_currentDialog = this;
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != View.INVISIBLE) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            ((TextView) findViewById(R.id.TextView_Title)).setText(R.string.Satellite);
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            int serviceRegionCount = MW.db_get_service_region_count();
            List<String> items = new ArrayList();
            items.clear();
            items.add(DtvMainActivity.this.getString(R.string.All));
            for (int i = 0; i < serviceRegionCount; i++) {
                if (MW.db_get_service_region_info(i + 1, DtvMainActivity.this.regionInfo) == 0) {
                    if (MW.get_system_tuner_config() == 1) {
                        items.add(SETTINGS.get_area_string_by_index(DtvMainActivity.this, DtvMainActivity.this.regionInfo.region_index));
                    } else {
                        items.add(DtvMainActivity.this.regionInfo.region_name);
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setCustomSelection(MW.db_get_service_region_current_pos());
            this.mListView.setItemChecked(MW.db_get_service_region_current_pos(), true);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C00521());
        }

        protected void onStop() {
            super.onStop();
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            DtvMainActivity.this.dia_currentDialog = null;
        }

        public RegionSelectDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                case MW.RET_INVALID_TYPE /*23*/:
                case DtvBaseActivity.KEYCODE_SAT /*55*/:
                    dismiss();
                    if (SETTINGS.bPass) {
                        MW.ts_player_play_current(true, false);
                        return true;
                    }
                    if (DtvMainActivity.this.bPwdShow) {
                        DtvMainActivity.this.bPwdShow = false;
                    }
                    int Ret = MW.ts_player_play_current(false, false);
                    if (Ret == 0) {
                        DtvMainActivity.this.CheckParentLockAndShowPasswd();
                        return true;
                    } else if (Ret != 44) {
                        return true;
                    } else {
                        DtvMainActivity.this.CheckChannelLockAndShowPasswd("13");
                        return true;
                    }
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    public class ServiceInfoDia extends Dialog {
        TextView Current_Tp;
        TextView Current_Tp_txt;
        TextView Current_sat;
        TextView Current_sat_txt;
        TextView audio_pid;
        TextView epg_detail_content;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        TextView pcr_pid;
        TextView plp;
        LinearLayout plp_ll;
        TextView ser_pol;
        TextView ser_pol_txt;
        TextView ser_symbol;
        TextView ser_symbol_txt;
        TextView service_name;
        TextView service_name_txt;
        TextView video_pid;

        protected void onCreate(Bundle savedInstanceState) {
            boolean hasEventText = true;
            super.onCreate(savedInstanceState);
            setContentView(R.layout.serviceinfo);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.serviceinfo, null);
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != View.INVISIBLE) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            this.epg_detail_content = (TextView) findViewById(R.id.epg_detail_content);
            this.epg_detail_content.setText(DtvMainActivity.this.getResources().getString(R.string.no_detail_info));
            MW.get_date(0, null, DtvMainActivity.this.date);
            if (MW.epg_lock_get_schedule_event_count(0, DtvMainActivity.this.serviceInfo, DtvMainActivity.this.date) > 0) {
                int epgScheduleEventId = MW.epg_lock_get_schedule_event_id_by_index(0, 0, DtvMainActivity.this.serviceInfo, DtvMainActivity.this.date);
                MW.epg_unlock_schedule_event();
                if (MW.epg_get_schedule_event_by_event_id(0, epgScheduleEventId, DtvMainActivity.this.serviceInfo, DtvMainActivity.this.date, DtvMainActivity.this.epgScheduleEvent) == 0) {
                    if (DtvMainActivity.this.epgScheduleEvent.event_text == null || DtvMainActivity.this.epgScheduleEvent.event_text.length() <= 0) {
                        hasEventText = false;
                    }
                    if (!hasEventText && !DtvMainActivity.this.epgScheduleEvent.has_extended_event_info) {
                        this.epg_detail_content.setText(DtvMainActivity.this.getResources().getString(R.string.no_detail_info));
                    } else if (hasEventText) {
                        Log.e("DtvMainActivity", "epgScheduleEvent.event_text = " + DtvMainActivity.this.epgScheduleEvent.event_text);
                        if (!DtvMainActivity.this.epgScheduleEvent.has_extended_event_info) {
                            this.epg_detail_content.setText(DtvMainActivity.this.epgScheduleEvent.event_text);
                        } else if (MW.epg_get_schedule_extened_event_by_event_id(0, epgScheduleEventId, DtvMainActivity.this.serviceInfo, DtvMainActivity.this.date, DtvMainActivity.this.epgExtendedEvent) == 0 && DtvMainActivity.this.epgExtendedEvent.event_text != null && DtvMainActivity.this.epgExtendedEvent.event_text.length() > 0) {
                            this.epg_detail_content.setText(DtvMainActivity.this.epgScheduleEvent.event_text + DtvMainActivity.this.epgExtendedEvent.event_text);
                        }
                    } else if (MW.epg_get_schedule_extened_event_by_event_id(0, epgScheduleEventId, DtvMainActivity.this.serviceInfo, DtvMainActivity.this.date, DtvMainActivity.this.epgExtendedEvent) == 0 && DtvMainActivity.this.epgExtendedEvent.event_text != null && DtvMainActivity.this.epgExtendedEvent.event_text.length() > 0) {
                        Log.e("DtvMainActivity", "epgExtendedEvent.event_text = " + DtvMainActivity.this.epgExtendedEvent.event_text);
                        this.epg_detail_content.setText(DtvMainActivity.this.epgExtendedEvent.event_text);
                    }
                } else {
                    this.epg_detail_content.setText(DtvMainActivity.this.getResources().getString(R.string.no_detail_info));
                }
            } else {
                MW.epg_unlock_schedule_event();
                this.epg_detail_content.setText(DtvMainActivity.this.getResources().getString(R.string.no_detail_info));
            }
            if (DtvMainActivity.this.SystemType == 0 || DtvMainActivity.this.SystemType == 2) {
                this.service_name_txt = (TextView) findViewById(R.id.service_name_txt);
                this.Current_sat_txt = (TextView) findViewById(R.id.Current_sat_txt);
                this.Current_Tp_txt = (TextView) findViewById(R.id.Current_Tp_txt);
                this.ser_symbol_txt = (TextView) findViewById(R.id.ser_symbol_txt);
                this.ser_pol_txt = (TextView) findViewById(R.id.ser_pol_txt);
                String colon = DtvMainActivity.this.getResources().getString(R.string.colon);
                this.service_name_txt.setText(DtvMainActivity.this.getResources().getString(R.string.channel_name) + colon);
                this.Current_sat_txt.setText(DtvMainActivity.this.getResources().getString(R.string.sate_title_name) + colon);
                this.Current_Tp_txt.setText(DtvMainActivity.this.getResources().getString(R.string.Frequency) + colon);
                this.ser_symbol_txt.setText(DtvMainActivity.this.getResources().getString(R.string.Symbolrate) + colon);
                this.ser_pol_txt.setText(DtvMainActivity.this.getResources().getString(R.string.Polarization) + colon);
                this.Current_sat = (TextView) findViewById(R.id.Current_sat);
                if (MW.db_dvb_s_get_current_sat_info(0, DtvMainActivity.this.satInfo) == 0) {
                    this.Current_sat.setText(DtvMainActivity.this.satInfo.sat_name);
                } else {
                    this.Current_sat.setText("");
                }
                this.Current_Tp = (TextView) findViewById(R.id.Current_Tp);
                this.ser_pol = (TextView) findViewById(R.id.ser_pol);
                this.ser_symbol = (TextView) findViewById(R.id.ser_symbol);
                if (MW.db_dvb_s_get_current_tp_info(0, DtvMainActivity.this.tpInfo) == 0) {
                    this.Current_Tp.setText("" + DtvMainActivity.this.tpInfo.frq);
                    this.ser_symbol.setText("" + DtvMainActivity.this.tpInfo.sym);
                    if (DtvMainActivity.this.tpInfo.pol == 0) {
                        this.ser_pol.setText(DtvMainActivity.this.getResources().getString(R.string.static_V));
                    } else {
                        this.ser_pol.setText(DtvMainActivity.this.getResources().getString(R.string.static_H));
                    }
                }
            } else {
                this.Current_sat = (TextView) findViewById(R.id.Current_sat);
                if (MW.db_dvb_t_get_current_area_info(DtvMainActivity.this.areaInfo) == 0) {
                    this.Current_sat.setText(SETTINGS.get_area_string_by_index(DtvMainActivity.this, DtvMainActivity.this.areaInfo.area_index));
                } else {
                    this.Current_sat.setText("");
                }
                this.Current_Tp = (TextView) findViewById(R.id.Current_Tp);
                this.ser_pol = (TextView) findViewById(R.id.ser_pol);
                this.ser_symbol = (TextView) findViewById(R.id.ser_symbol);
                this.Current_sat_txt = (TextView) findViewById(R.id.Current_sat_txt);
                this.ser_symbol_txt = (TextView) findViewById(R.id.ser_symbol_txt);
                this.ser_pol_txt = (TextView) findViewById(R.id.ser_pol_txt);
                this.plp_ll = (LinearLayout) findViewById(R.id.plp_ll);
                this.plp = (TextView) findViewById(R.id.plp);
                this.Current_sat_txt.setText(DtvMainActivity.this.getResources().getString(R.string.AreaName) + " :");
                this.ser_symbol_txt.setText(DtvMainActivity.this.getResources().getString(R.string.Band) + " :");
                this.ser_pol_txt.setText(DtvMainActivity.this.getResources().getString(R.string.TransponderNo) + " :");
                if (MW.db_dvb_t_get_current_tp_info(DtvMainActivity.this.t_tpInfo) == 0) {
                    this.Current_Tp.setText("" + DtvMainActivity.this.t_tpInfo.frq);
                    this.ser_symbol.setText("" + DtvMainActivity.this.t_tpInfo.bandwidth);
                    this.ser_pol.setText(DtvMainActivity.this.t_tpInfo.channel_number);
                    if (DtvMainActivity.this.serviceInfo.front_end_type == 4) {
                        this.plp_ll.setVisibility(View.VISIBLE);
                        this.plp.setText("" + DtvMainActivity.this.serviceInfo.dvb_t2_plp_id);
                    } else {
                        this.plp_ll.setVisibility(View.GONE);
                        this.plp.setText("");
                    }
                }
            }
            this.service_name = (TextView) findViewById(R.id.service_name);
            this.service_name.setText(DtvMainActivity.this.serviceInfo.service_name);
            this.video_pid = (TextView) findViewById(R.id.video_pid);
            this.audio_pid = (TextView) findViewById(R.id.audio_pid);
            this.pcr_pid = (TextView) findViewById(R.id.pcr_pid);
            this.video_pid.setText("" + DtvMainActivity.this.serviceInfo.video_pid);
            this.audio_pid.setText("" + DtvMainActivity.this.serviceInfo.audio_info[DtvMainActivity.this.serviceInfo.audio_index].audio_pid);
            this.pcr_pid.setText("" + DtvMainActivity.this.serviceInfo.pcr_pid);
        }

        protected void onStop() {
            if (DtvMainActivity.this.dia_currentDialog == null) {
                DtvMainActivity.disableShowPlayStatus = false;
                MW.ts_player_require_play_status();
            } else if (!DtvMainActivity.this.dia_currentDialog.isShowing()) {
                DtvMainActivity.disableShowPlayStatus = false;
                MW.ts_player_require_play_status();
            }
            super.onStop();
        }

        public ServiceInfoDia(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.epg_detail_content = null;
            this.Current_sat = null;
            this.service_name = null;
            this.video_pid = null;
            this.audio_pid = null;
            this.pcr_pid = null;
            this.Current_Tp = null;
            this.ser_pol = null;
            this.ser_symbol = null;
            this.Current_sat_txt = null;
            this.ser_symbol_txt = null;
            this.ser_pol_txt = null;
            this.plp_ll = null;
            this.plp = null;
            this.service_name_txt = null;
            this.Current_Tp_txt = null;
            this.mContext = context;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                    case DtvBaseActivity.KEYCODE_INFO /*185*/:
                        dismiss();
                        return true;
                }
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    public class StorageDeviceDialog extends Dialog {
        List<String> items;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;
        private int temp_no;

        /* renamed from: th.dtv.DtvMainActivity.StorageDeviceDialog.1 */
        class C00531 implements OnItemSelectedListener {
            C00531() {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Log.e("DtvMainActivity", "mListView position =" + position);
                StorageDeviceDialog.this.temp_no = position;
                StorageDeviceDialog.this.mListView.setItemChecked(position, true);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        }

        /* renamed from: th.dtv.DtvMainActivity.StorageDeviceDialog.2 */
//        class C00542 implements OnKeyListener {
//            C00542() {
//            }
//
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == 0) {
//                    switch (keyCode) {
//                        case MW.RET_INVALID_TYPE /*23*/:
//                            DtvMainActivity.this.device_no = StorageDeviceDialog.this.temp_no;
//                            Log.e("DtvMainActivity", "device_no OK  :" + DtvMainActivity.this.device_no);
//                            if (MW.db_get_current_service_info(DtvMainActivity.this.serviceInfo) == 0 && DtvMainActivity.this.device_no != -1) {
//                                DtvMainActivity.this.current_service_type = DtvMainActivity.this.serviceInfo.service_type;
//                                DtvMainActivity.this.current_service_index = DtvMainActivity.this.serviceInfo.service_index;
//                                DtvMainActivity.this.current_record_service_region_index = DtvMainActivity.this.serviceInfo.region_index;
//                                DtvMainActivity.this.current_record_service_tp_index = DtvMainActivity.this.serviceInfo.tp_index;
//                                if (DtvMainActivity.this.btimeshift) {
//                                    DtvMainActivity.this.ftimelimit = true;
//                                    if (SETTINGS.getTimeShiftLengthIndex() == 4) {
//                                        DtvMainActivity.this.ShowRecordTimeEditTextView();
//                                    } else {
//                                        DtvMainActivity.this.DoTimeShiftFunction(DtvMainActivity.this.device_no, SETTINGS.getTimeShiftTime());
//                                    }
//                                } else {
//                                    DtvMainActivity.this.ftimelimit = false;
//                                    if (SETTINGS.getRecordLengthIndex() == 4) {
//                                        DtvMainActivity.this.ShowRecordTimeEditTextView();
//                                    } else {
//                                        DtvMainActivity.this.DoRecordFunction(DtvMainActivity.this.device_no, SETTINGS.getRecordTime());
//                                    }
//                                }
//                            } else if (DtvMainActivity.this.btimeshift) {
//                                DtvMainActivity.this.btimeshift = false;
//                                SETTINGS.makeText(DtvMainActivity.this, R.string.nosignalcanttimeshift, 0);
//                            } else {
//                                SETTINGS.makeText(DtvMainActivity.this, R.string.nosignalcantrecord, 0);
//                            }
//                            StorageDeviceDialog.this.dismiss();
//                            return true;
//                    }
//                }
//                return false;
//            }
//        }

        private void refreshStorageDevice() {
            this.items.clear();
            if (DtvMainActivity.this.storageDeviceInfo == null) {
                dismiss();
            } else if (DtvMainActivity.this.storageDeviceInfo.getDeviceCount() > 0) {
                for (int i = 0; i < DtvMainActivity.this.storageDeviceInfo.getDeviceCount(); i++) {
                    DeviceItem item = DtvMainActivity.this.storageDeviceInfo.getDeviceItem(i);
                    if (item.VolumeName != null) {
                        if (item.VolumeName.indexOf("[udisk") == 1) {
                            this.items.add(" " + DtvMainActivity.this.getResources().getString(R.string.udisk) + " " + ((DtvMainActivity.this.storageDeviceInfo.getDeviceCount() - 1) - i));
                        } else {
                            this.items.add(item.VolumeName + "");
                        }
                    }
                }
                this.mAdapter.notifyDataSetChanged();
            } else {
                dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.storagedevice_sel_dialog);
            DtvMainActivity.this.dia_currentDialog = this;
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != 4) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            ((TextView) findViewById(R.id.TextView_Title)).setText(DtvMainActivity.this.getResources().getString(R.string.SelStorageDevice));
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            this.items = new ArrayList();
            this.items.clear();
            for (int i = 0; i < DtvMainActivity.this.storageDeviceInfo.getDeviceCount(); i++) {
                DeviceItem item = DtvMainActivity.this.storageDeviceInfo.getDeviceItem(i);
                if (item.VolumeName != null) {
                    if (item.VolumeName.indexOf("[udisk") == 1) {
                        this.items.add(" " + DtvMainActivity.this.getResources().getString(R.string.udisk) + " " + ((DtvMainActivity.this.storageDeviceInfo.getDeviceCount() - 1) - i));
                    } else {
                        this.items.add(item.VolumeName + "");
                    }
                }
            }
            this.mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, this.items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            this.mListView.setItemChecked(0, true);
            this.mListView.setCustomSelection(0);
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemSelectedListener(new C00531());
//            this.mListView.setCustomKeyListener(new C00542());
        }

        protected void onStop() {
            super.onStop();
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            DtvMainActivity.this.dia_currentDialog = null;
        }

        public StorageDeviceDialog(Context context, int theme) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.temp_no = 0;
            this.mContext = context;
        }
    }

    private class StorageDevicePlugReceiver extends BroadcastReceiver {
        private StorageDevicePlugReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
//            if (action.equals("android.intent.action.DEVICE_MOUNTED")) {
////                Log.e("DtvMainActivity", "StorageDevice plug in :  " + intent.getExtra("device_path"));
//                if (DtvMainActivity.this.DeviceInfoDia != null) {
//                    DtvMainActivity.this.DeviceInfoDia.refreshStorageDevice();
//                }
//                SETTINGS.makeText(DtvMainActivity.this, R.string.insert, 0);
//            } else if (action.equals("android.intent.action.DEVICE_REMOVED")) {
//                SETTINGS.makeText(DtvMainActivity.this, R.string.out, 0);
////                Log.e("DtvMainActivity", "StorageDevice plug out :  " + intent.getExtra("device_path"));
//                if (DtvMainActivity.this.DeviceInfoDia != null) {
//                    DtvMainActivity.this.DeviceInfoDia.refreshStorageDevice();
//                }
//                if (DtvMainActivity.this.bInRecording) {
//                    if (DtvMainActivity.this.sCurrentRecordStoreDir != null && DtvMainActivity.this.sCurrentRecordStoreDir.equals(intent.getExtra("device_path"))) {
////                        Log.e("DtvMainActivity", "stop recording :  " + intent.getExtra("device_path"));
//                        DtvMainActivity.this.RemoveDeviceIsFullMsg();
//                        MW.pvr_record_stop();
//                        DtvMainActivity.this.bInRecording = false;
//                    }
//                } else if (DtvMainActivity.this.sCurrentRecordStoreDir != null && DtvMainActivity.this.sCurrentRecordStoreDir.equals(intent.getExtra("device_path"))) {
//                    Log.e("DtvMainActivity", "stop timeshift :  " + intent.getExtra("device_path"));
//                    if (DtvMainActivity.this.bInit_timeShift) {
//                        DtvMainActivity.this.RemoveDeviceIsFullMsg();
//                        MW.pvr_timeshift_stop();
//                        if (DtvMainActivity.this.timeshift != null) {
//                            DtvMainActivity.this.timeshift.dismiss();
//                        }
//                        DtvMainActivity.this.btimeshift = false;
//                    }
//                }
//            }
        }
    }

    public class TeletextSubtitleInfoDialog extends Dialog {
        boolean bTeletext;
        private ArrayAdapter mAdapter;
        private Context mContext;
        private LayoutInflater mInflater;
        View mLayoutView;
        private CustomListView mListView;

        /* renamed from: th.dtv.DtvMainActivity.TeletextSubtitleInfoDialog.1 */
        class C00551 implements OnItemClickListener {
            C00551() {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (TeletextSubtitleInfoDialog.this.bTeletext) {
                    DtvMainActivity.this.startTeletextSubtitle(TeletextSubtitleInfoDialog.this.bTeletext, position, DtvMainActivity.this.serviceInfo);
                } else if (DtvMainActivity.this.temp_whichone == -1 || DtvMainActivity.this.temp_whichone != position) {
                    DtvMainActivity.this.temp_whichone = position;
                    if (position == 0) {
                        DtvMainActivity.this.stopTeletextSubtitle();
                        SETTINGS.set_subtitle_default_on(false);
                    } else {
                        DtvMainActivity.this.startTeletextSubtitle(TeletextSubtitleInfoDialog.this.bTeletext, position - 1, DtvMainActivity.this.serviceInfo);
                        SETTINGS.set_subtitle_default_on(true);
                    }
                } else {
                    TeletextSubtitleInfoDialog.this.dismiss();
                    return;
                }
                TeletextSubtitleInfoDialog.this.dismiss();
            }
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ttx_sub_info_dialog);
            DtvMainActivity.this.dia_currentDialog = this;
            if (DtvMainActivity.rl_playStatusBar.getVisibility() != View.INVISIBLE) {
                DtvMainActivity.rl_playStatusBar.setVisibility(View.INVISIBLE);
            }
            DtvMainActivity.disableShowPlayStatus = true;
            TextView txv_Title = (TextView) findViewById(R.id.TextView_Title);
            if (this.bTeletext) {
                txv_Title.setText(R.string.teletext_language);
            } else {
                txv_Title.setText(R.string.subtitle_language);
            }
            this.mListView = (CustomListView) findViewById(R.id.lvListItem);
            this.mInflater = LayoutInflater.from(this.mContext);
            this.mLayoutView = this.mInflater.inflate(R.layout.single_selection_list_item, null);
            int whichone = 0;
            List<String> items = new ArrayList();
            items.clear();
            int i;
            if (this.bTeletext) {
                for (ttx_info th_dtv_mw_data_ttx_info : DtvMainActivity.this.serviceInfo.teletext_info) {
                    items.add(th_dtv_mw_data_ttx_info.ISO_639_language_code);
                    whichone = 0;
                }
            } else {
                items.add(DtvMainActivity.this.getString(R.string.off));
                i = 0;
                while (i < DtvMainActivity.this.serviceInfo.subtitle_info.length) {
                    if (DtvMainActivity.this.serviceInfo.subtitle_info[i].subtitling_type == 2 || DtvMainActivity.this.serviceInfo.subtitle_info[i].subtitling_type == 5) {
                        items.add(DtvMainActivity.this.serviceInfo.subtitle_info[i].ISO_639_language_code + " (T) ");
                    } else {
                        items.add(DtvMainActivity.this.serviceInfo.subtitle_info[i].ISO_639_language_code + " (D) ");
                    }
                    i++;
                }
                i = 0;
                while (i < DtvMainActivity.this.serviceInfo.subtitle_info.length) {
                    if (!DtvMainActivity.this.serviceInfo.subtitle_info[i].ISO_639_language_code.equals(SETTINGS.get_subtitle_language())) {
                        if (DtvMainActivity.this.serviceInfo.subtitle_info[i].subtitling_type != 2 && DtvMainActivity.this.serviceInfo.subtitle_info[i].subtitling_type != 5) {
                            whichone = i + 1;
                            break;
                        } else {
                            whichone = 1;
                            i++;
                        }
                    } else if (DtvMainActivity.this.serviceInfo.subtitle_info[i].subtitling_type == 2 || DtvMainActivity.this.serviceInfo.subtitle_info[i].subtitling_type == 5) {
                        int j = i;
                        while (j < DtvMainActivity.this.serviceInfo.subtitle_info.length) {
                            if (DtvMainActivity.this.serviceInfo.subtitle_info[j].subtitling_type != 2 && DtvMainActivity.this.serviceInfo.subtitle_info[j].subtitling_type != 5) {
                                whichone = j + 1;
                                break;
                            } else {
                                whichone = i + 1;
                                j++;
                            }
                        }
                    } else {
                        whichone = i + 1;
                    }
                }
                if (!SETTINGS.get_subtitle_default_on()) {
                    whichone = 0;
                }
            }
            this.mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setChoiceMode(1);
            if (DtvMainActivity.this.temp_whichone == -1) {
                this.mListView.setItemChecked(whichone, true);
                this.mListView.setCustomSelection(whichone);
                DtvMainActivity.this.temp_whichone = whichone;
            } else {
                this.mListView.setItemChecked(DtvMainActivity.this.temp_whichone, true);
                this.mListView.setCustomSelection(DtvMainActivity.this.temp_whichone);
            }
            this.mListView.setVisibleItemCount(5);
            this.mListView.setOnItemClickListener(new C00551());
        }

        protected void onStop() {
            super.onStop();
            DtvMainActivity.disableShowPlayStatus = false;
            MW.ts_player_require_play_status();
            DtvMainActivity.this.dia_currentDialog = null;
        }

        public TeletextSubtitleInfoDialog(Context context, int theme, boolean bTTX) {
            super(context, theme);
            this.mInflater = null;
            this.mListView = null;
            this.mContext = null;
            this.mLayoutView = null;
            this.mContext = context;
            this.bTeletext = bTTX;
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                    if (!this.bTeletext) {
                        return true;
                    }
                    dismiss();
                    return true;
                case DtvBaseActivity.KEYCODE_SUBTITLE /*2012*/:
                    if (this.bTeletext) {
                        return true;
                    }
                    dismiss();
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
    }

    class editOnKeyListener implements OnKeyListener {
        editOnKeyListener() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                switch (keyCode) {
                    case MW.RET_INVALID_TYPE /*23*/:
                    case DtvBaseActivity.KEYCODE_DEL /*67*/:
                        return true;
                    default:
                        break;
                }
            }
            EditText et = (EditText) v;
            int value;
            switch (keyCode) {
                case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                case MW.SEARCH_STATUS_SEARCH_END /*9*/:
                case MW.RET_TP_EXIST /*10*/:
                case MW.RET_INVALID_SAT /*11*/:
                case MW.RET_INVALID_AREA /*12*/:
                case MW.RET_INVALID_TP /*13*/:
                case MW.RET_BOOK_EVENT_INVALID /*14*/:
                case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                    DtvMainActivity.this.bRefresh = false;
                    value = 0;
                    if (et.hasSelection()) {
                        value = keyCode - 7;
                    } else {
                        if (et.getText().toString().length() > 0) {
                            value = Integer.valueOf(et.getText().toString()).intValue();
                        }
                        value = ((value * 10) + keyCode) - 7;
                        if (value >= 100000) {
                            value %= 100000;
                        }
                    }
                    et.setText(Integer.toString(value));
                    et.setSelection(et.length());
                    return true;
                case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                    DtvMainActivity.this.bRefresh = true;
                    if (DtvMainActivity.this.et_ttxPageNumber != null) {
                        DtvMainActivity.this.et_ttxPageNumber.selectAll();
                    }
                    if (MW.teletext_goto_next_page() != 0) {
                        return true;
                    }
                    DtvMainActivity.this.ShowLoadingIcon();
                    return true;
                case MW.RET_INVALID_TP_INDEX /*20*/:
                    DtvMainActivity.this.bRefresh = true;
                    if (DtvMainActivity.this.et_ttxPageNumber != null) {
                        DtvMainActivity.this.et_ttxPageNumber.selectAll();
                    }
                    if (MW.teletext_goto_prev_page() != 0) {
                        return true;
                    }
                    DtvMainActivity.this.ShowLoadingIcon();
                    return true;
                case MW.RET_INVALID_RECORD_INDEX /*21*/:
                case DtvBaseActivity.KEYCODE_DEL /*67*/:
                    DtvMainActivity.this.bRefresh = false;
                    if (et.hasSelection()) {
                        et.setSelection(et.length());
                        return true;
                    }
                    value = 0;
                    if (et.getText().toString().length() > 0) {
                        value = Integer.valueOf(et.getText().toString()).intValue();
                    }
                    et.setText(Integer.toString(value / 10));
                    et.setSelection(et.length());
                    return true;
                case MW.RET_MEMORY_ERROR /*22*/:
                    DtvMainActivity.this.bRefresh = false;
                    if (!et.hasSelection()) {
                        return true;
                    }
                    et.setSelection(et.length());
                    return true;
                case MW.RET_INVALID_TYPE /*23*/:
                    DtvMainActivity.this.bRefresh = true;
                    if (et.hasSelection()) {
                        et.setSelection(et.length());
                        return true;
                    }
                    value = 0;
                    if (et.getText().toString().length() > 0) {
                        value = Integer.valueOf(et.getText().toString()).intValue();
                    }
                    et.selectAll();
                    if (MW.teletext_goto_page(value) != 0) {
                        return true;
                    }
                    DtvMainActivity.this.ShowLoadingIcon();
                    return true;
                case DtvBaseActivity.KEYCODE_PAGE_UP /*92*/:
                case DtvBaseActivity.KEYCODE_PAGE_DOWN /*93*/:
                    DtvMainActivity.this.bRefresh = true;
                    if (DtvMainActivity.this.et_ttxPageNumber != null) {
                        DtvMainActivity.this.et_ttxPageNumber.selectAll();
                    }
                    if (keyCode == 92) {
                        if (MW.teletext_goto_offset_page(100) != 0) {
                            return true;
                        }
                        DtvMainActivity.this.ShowLoadingIcon();
                        return true;
                    } else if (MW.teletext_goto_offset_page(-100) != 0) {
                        return true;
                    } else {
                        DtvMainActivity.this.ShowLoadingIcon();
                        return true;
                    }
            }
            return false;
        }
    }

    class listOnKeyListener implements OnKeyListener {
        listOnKeyListener() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onKey(android.view.View r8, int r9, android.view.KeyEvent r10) {
            /*
            r7 = this;
            r5 = 4;
            r6 = 44;
            r1 = 1;
            r2 = 0;
            r3 = r10.getAction();
            if (r3 != 0) goto L_0x000e;
        L_0x000b:
            switch(r9) {
                case 4: goto L_0x01c0;
                case 21: goto L_0x0010;
                case 22: goto L_0x0010;
                case 23: goto L_0x00d3;
                case 27: goto L_0x02d1;
                case 55: goto L_0x0222;
                case 57: goto L_0x000f;
                case 140: goto L_0x0041;
                case 169: goto L_0x0334;
                case 555: goto L_0x0041;
                default: goto L_0x000e;
            };
        L_0x000e:
            r1 = r2;
        L_0x000f:
            return r1;
        L_0x0010:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.bInRecording;
            if (r2 != 0) goto L_0x003b;
        L_0x0018:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            if (r2 == 0) goto L_0x0029;
        L_0x0020:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            r2.dismiss();
        L_0x0029:
            r2 = th.dtv.MW.db_check_service_in_fav_mode();
            if (r2 != 0) goto L_0x0035;
        L_0x002f:
            r2 = th.dtv.DtvMainActivity.this;
            r2.SwitchRegions(r9);
            goto L_0x000f;
        L_0x0035:
            r2 = th.dtv.DtvMainActivity.this;
            r2.SwitchFavGroup(r9);
            goto L_0x000f;
        L_0x003b:
            r2 = th.dtv.DtvMainActivity.this;
            r2.ShowRecordAlertDialog();
            goto L_0x000f;
        L_0x0041:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bInRecording;
            if (r3 != 0) goto L_0x00cc;
        L_0x0049:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.rl_channelNumber;
            r3 = r3.getVisibility();
            if (r3 == r5) goto L_0x005a;
        L_0x0055:
            r3 = th.dtv.DtvMainActivity.this;
            r3.CloseChannelNumber();
        L_0x005a:
            r2 = th.dtv.MW.ts_player_play_switch_tv_radio(r2);
            switch(r2) {
                case 0: goto L_0x0062;
                case 1: goto L_0x009d;
                case 7: goto L_0x008d;
                case 8: goto L_0x007e;
                case 44: goto L_0x00ad;
                default: goto L_0x0061;
            };
        L_0x0061:
            goto L_0x000f;
        L_0x0062:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            if (r2 == 0) goto L_0x0073;
        L_0x006a:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            r2.dismiss();
        L_0x0073:
            r2 = th.dtv.DtvMainActivity.this;
            r2.RefreshShowChannelList();
            r2 = th.dtv.DtvMainActivity.this;
            r2.CheckParentLockAndShowPasswd();
            goto L_0x000f;
        L_0x007e:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = th.dtv.DtvMainActivity.this;
            r4 = 2131165395; // 0x7f0700d3 float:1.7945006E38 double:1.0529356073E-314;
            r3 = r3.getString(r4);
            r2.ShowTmpPlayStatus(r3);
            goto L_0x000f;
        L_0x008d:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = th.dtv.DtvMainActivity.this;
            r4 = 2131165394; // 0x7f0700d2 float:1.7945004E38 double:1.052935607E-314;
            r3 = r3.getString(r4);
            r2.ShowTmpPlayStatus(r3);
            goto L_0x000f;
        L_0x009d:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = th.dtv.DtvMainActivity.this;
            r4 = 2131165393; // 0x7f0700d1 float:1.7945002E38 double:1.0529356063E-314;
            r3 = r3.getString(r4);
            r2.ShowTmpPlayStatus(r3);
            goto L_0x000f;
        L_0x00ad:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            if (r2 == 0) goto L_0x00be;
        L_0x00b5:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            r2.dismiss();
        L_0x00be:
            r2 = th.dtv.DtvMainActivity.this;
            r2.RefreshShowChannelList();
            r2 = th.dtv.DtvMainActivity.this;
            r3 = "9";
            r2.CheckChannelLockAndShowPasswd(r3);
            goto L_0x000f;
        L_0x00cc:
            r2 = th.dtv.DtvMainActivity.this;
            r2.ShowRecordAlertDialog();
            goto L_0x000f;
        L_0x00d3:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.rl_channelNumber;
            r3 = r3.getVisibility();
            if (r3 != r5) goto L_0x01b9;
        L_0x00df:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.serviceInfo;
            r3 = th.dtv.MW.db_get_current_service_info(r3);
            if (r3 != 0) goto L_0x01c0;
        L_0x00eb:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.serviceInfo;
            r3 = r3.service_index;
            r4 = th.dtv.DtvMainActivity.this;
            r4 = r4.channel_list;
            r4 = r4.getSelectedItemPosition();
            if (r3 == r4) goto L_0x01c0;
        L_0x00ff:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bInRecording;
            if (r3 == 0) goto L_0x0126;
        L_0x0107:
            r3 = th.dtv.DtvMainActivity.this;
            r4 = th.dtv.DtvMainActivity.this;
            r4 = r4.current_service_type;
            r5 = th.dtv.DtvMainActivity.this;
            r5 = r5.channel_list;
            r5 = r5.getSelectedItemPosition();
            r3 = r3.checkEnableSameTPRecordService(r4, r5);
            if (r3 != 0) goto L_0x0126;
        L_0x011f:
            r2 = th.dtv.DtvMainActivity.this;
            r2.ShowRecordAlertDialog();
            goto L_0x000f;
        L_0x0126:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.channel_list;
            r3 = r3.getSelectedItemPosition();
            r3 = r3 + 1;
            th.dtv.SETTINGS.set_led_channel(r3);
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.current_service_type;
            r4 = th.dtv.DtvMainActivity.this;
            r4 = r4.channel_list;
            r4 = r4.getSelectedItemPosition();
            r0 = th.dtv.MW.ts_player_play(r3, r4, r2, r2);
            r2 = java.lang.System.out;
            r3 = new java.lang.StringBuilder;
            r3.<init>();
            r4 = "LEE : Ret = ";
            r3 = r3.append(r4);
            r3 = r3.append(r0);
            r3 = r3.toString();
            r2.println(r3);
            if (r0 != 0) goto L_0x018c;
        L_0x0163:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.serviceInfo;
            r2 = th.dtv.MW.db_get_current_service_info(r2);
            if (r2 != 0) goto L_0x017c;
        L_0x016f:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.serviceInfo;
            r3 = r3.service_index;
            r2.current_service_index = r3;
        L_0x017c:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.channelListAdapter;
            r2.notifyDataSetChanged();
            r2 = th.dtv.DtvMainActivity.this;
            r2.CheckParentLockAndShowPasswd();
            goto L_0x000f;
        L_0x018c:
            if (r0 != r6) goto L_0x000f;
        L_0x018e:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.serviceInfo;
            r2 = th.dtv.MW.db_get_current_service_info(r2);
            if (r2 != 0) goto L_0x01a7;
        L_0x019a:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.serviceInfo;
            r3 = r3.service_index;
            r2.current_service_index = r3;
        L_0x01a7:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.channelListAdapter;
            r2.notifyDataSetChanged();
            r2 = th.dtv.DtvMainActivity.this;
            r3 = "10";
            r2.CheckChannelLockAndShowPasswd(r3);
            goto L_0x000f;
        L_0x01b9:
            r2 = th.dtv.DtvMainActivity.this;
            r2.PlayByChannelNumber();
            goto L_0x000f;
        L_0x01c0:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.rl_channelNumber;
            r3 = r3.getVisibility();
            if (r3 == r5) goto L_0x01d3;
        L_0x01cc:
            r2 = th.dtv.DtvMainActivity.this;
            r2.CloseChannelNumber();
            goto L_0x000f;
        L_0x01d3:
            r3 = th.dtv.DtvMainActivity.this;
            r3.listview_AnimationExit();
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.rl_channelList;
            r4 = 8;
            r3.setVisibility(r4);
            r3 = th.dtv.DtvMainActivity.this;
            r3.showInfoBar();
            r3 = th.dtv.MW.ts_player_get_play_status();
            if (r3 == 0) goto L_0x000f;
        L_0x01ee:
            r3 = th.dtv.SETTINGS.bPass;
            if (r3 != 0) goto L_0x000f;
        L_0x01f2:
            r0 = th.dtv.MW.ts_player_play_current(r2, r2);
            r2 = java.lang.System.out;
            r3 = new java.lang.StringBuilder;
            r3.<init>();
            r4 = "LEE exit : Ret = ";
            r3 = r3.append(r4);
            r3 = r3.append(r0);
            r3 = r3.toString();
            r2.println(r3);
            if (r0 != 0) goto L_0x0217;
        L_0x0210:
            r2 = th.dtv.DtvMainActivity.this;
            r2.CheckParentLockAndShowPasswd();
            goto L_0x000f;
        L_0x0217:
            if (r0 != r6) goto L_0x000f;
        L_0x0219:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = "11";
            r2.CheckChannelLockAndShowPasswd(r3);
            goto L_0x000f;
        L_0x0222:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bInRecording;
            if (r3 != 0) goto L_0x02ca;
        L_0x022a:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.dia_currentDialog;
            if (r3 == 0) goto L_0x023b;
        L_0x0232:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.dia_currentDialog;
            r3.dismiss();
        L_0x023b:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.SystemType;
            if (r3 == 0) goto L_0x024c;
        L_0x0243:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.SystemType;
            r4 = 2;
            if (r3 != r4) goto L_0x028f;
        L_0x024c:
            r3 = th.dtv.MW.db_dvb_s_get_sat_count(r2);
            if (r3 > 0) goto L_0x0288;
        L_0x0252:
            r3 = th.dtv.DtvMainActivity.this;
            r4 = 2131165290; // 0x7f07006a float:1.7944793E38 double:1.0529355554E-314;
            th.dtv.SETTINGS.makeText(r3, r4, r2);
            r3 = th.dtv.SETTINGS.bPass;
            if (r3 != 0) goto L_0x0283;
        L_0x025e:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bPwdShow;
            if (r3 != r1) goto L_0x026b;
        L_0x0266:
            r3 = th.dtv.DtvMainActivity.this;
            r3.bPwdShow = r2;
        L_0x026b:
            r0 = th.dtv.MW.ts_player_play_current(r2, r2);
            if (r0 != 0) goto L_0x0278;
        L_0x0271:
            r2 = th.dtv.DtvMainActivity.this;
            r2.CheckParentLockAndShowPasswd();
            goto L_0x000f;
        L_0x0278:
            if (r0 != r6) goto L_0x000f;
        L_0x027a:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = "28";
            r2.CheckChannelLockAndShowPasswd(r3);
            goto L_0x000f;
        L_0x0283:
            th.dtv.MW.ts_player_play_current(r1, r2);
            goto L_0x000f;
        L_0x0288:
            r2 = th.dtv.DtvMainActivity.this;
            r2.DoSatAction();
            goto L_0x000f;
        L_0x028f:
            r3 = th.dtv.MW.db_dvb_t_get_area_count();
            if (r3 > 0) goto L_0x02c3;
        L_0x0295:
            r3 = th.dtv.SETTINGS.bPass;
            if (r3 != 0) goto L_0x02be;
        L_0x0299:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bPwdShow;
            if (r3 != r1) goto L_0x02a6;
        L_0x02a1:
            r3 = th.dtv.DtvMainActivity.this;
            r3.bPwdShow = r2;
        L_0x02a6:
            r0 = th.dtv.MW.ts_player_play_current(r2, r2);
            if (r0 != 0) goto L_0x02b3;
        L_0x02ac:
            r2 = th.dtv.DtvMainActivity.this;
            r2.CheckParentLockAndShowPasswd();
            goto L_0x000f;
        L_0x02b3:
            if (r0 != r6) goto L_0x000f;
        L_0x02b5:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = "28";
            r2.CheckChannelLockAndShowPasswd(r3);
            goto L_0x000f;
        L_0x02be:
            th.dtv.MW.ts_player_play_current(r1, r2);
            goto L_0x000f;
        L_0x02c3:
            r2 = th.dtv.DtvMainActivity.this;
            r2.DoSatAction();
            goto L_0x000f;
        L_0x02ca:
            r2 = th.dtv.DtvMainActivity.this;
            r2.ShowRecordAlertDialog();
            goto L_0x000f;
        L_0x02d1:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bInRecording;
            if (r3 != 0) goto L_0x032d;
        L_0x02d9:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.dia_currentDialog;
            if (r3 == 0) goto L_0x02ea;
        L_0x02e1:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.dia_currentDialog;
            r3.dismiss();
        L_0x02ea:
            r3 = th.dtv.MW.db_get_service_available_fav_group_count();
            if (r3 > 0) goto L_0x0326;
        L_0x02f0:
            r3 = th.dtv.DtvMainActivity.this;
            r4 = 2131165382; // 0x7f0700c6 float:1.794498E38 double:1.052935601E-314;
            th.dtv.SETTINGS.makeText(r3, r4, r2);
            r3 = th.dtv.SETTINGS.bPass;
            if (r3 != 0) goto L_0x0321;
        L_0x02fc:
            r3 = th.dtv.DtvMainActivity.this;
            r3 = r3.bPwdShow;
            if (r3 != r1) goto L_0x0309;
        L_0x0304:
            r3 = th.dtv.DtvMainActivity.this;
            r3.bPwdShow = r2;
        L_0x0309:
            r0 = th.dtv.MW.ts_player_play_current(r2, r2);
            if (r0 != 0) goto L_0x0316;
        L_0x030f:
            r2 = th.dtv.DtvMainActivity.this;
            r2.CheckParentLockAndShowPasswd();
            goto L_0x000f;
        L_0x0316:
            if (r0 != r6) goto L_0x000f;
        L_0x0318:
            r2 = th.dtv.DtvMainActivity.this;
            r3 = "29";
            r2.CheckChannelLockAndShowPasswd(r3);
            goto L_0x000f;
        L_0x0321:
            th.dtv.MW.ts_player_play_current(r1, r2);
            goto L_0x000f;
        L_0x0326:
            r2 = th.dtv.DtvMainActivity.this;
            r2.DoFavAction();
            goto L_0x000f;
        L_0x032d:
            r2 = th.dtv.DtvMainActivity.this;
            r2.ShowRecordAlertDialog();
            goto L_0x000f;
        L_0x0334:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.bInRecording;
            if (r2 != 0) goto L_0x0354;
        L_0x033c:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            if (r2 == 0) goto L_0x034d;
        L_0x0344:
            r2 = th.dtv.DtvMainActivity.this;
            r2 = r2.dia_currentDialog;
            r2.dismiss();
        L_0x034d:
            r2 = th.dtv.DtvMainActivity.this;
            r2.DoFindChannelFunction();
            goto L_0x000f;
        L_0x0354:
            r2 = th.dtv.DtvMainActivity.this;
            r2.ShowRecordAlertDialog();
            goto L_0x000f;
            */
            throw new UnsupportedOperationException("Method not decompiled: th.dtv.DtvMainActivity.listOnKeyListener.onKey(android.view.View, int, android.view.KeyEvent):boolean");
        }
    }

    class listOnScroll implements OnScrollListener {
        listOnScroll() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            DtvMainActivity.this.mFirstVisibleItem = firstVisibleItem;
            DtvMainActivity.this.mVisibleItemCount = visibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            System.out.println("scrollState---------" + scrollState);
        }
    }

    public DtvMainActivity() {
        this.mChannelListListener = null;
        this.mFirstVisibleItem = 0;
        this.mVisibleItemCount = 8;
        this.device_no = -1;
        this.record_time = 0;
        this.bRefresh = true;
        this.bPwdShow = false;
        this.regionInfo = new region_info();
        this.satInfo = new dvb_s_sat();
        this.tpInfo = new dvb_s_tp();
        this.serviceInfo = new service();
        this.signalStatus = new tuner_signal_status();
        this.dateTime = new date_time();
        this.epgCurrentEvent = new epg_pf_event();
        this.epgNextEvent = new epg_pf_event();
        this.ttxSubPageInfo = new ttx_sub_page_info();
        this.bGotoEPGWin = false;
        this.bInRecording = false;
        this.enableRecord = false;
        this.timeSelectDialog = null;
        this.ftimelimit = false;
        this.favgroupname = null;
        this.book_enableRecord = false;
        this.temp_current_service_index = -1;
        this.book_type = 1;
        this.is_all_region = false;
        this.bChangetoAll = false;
        this.bChangetoNew = false;
        this.SystemType = 0;
        this.areaInfo = new dvb_t_area();
        this.t_tpInfo = new dvb_t_tp();
        this.date = new date();
        this.epgExtendedEvent = new epg_extended_event();
        this.epgScheduleEvent = new epg_schedule_event();
        this.temp_whichone = -1;
        this.mEditOnKeyListener = new editOnKeyListener();
        this.timeshift = null;
        this.bInit_timeShift = false;
        this.Temp_timeShift_PlayStatus = 5;
        this.Temp_timeshift_speed = 0;
        this.timeShift_PlayStatus = 5;
        this.timeshift_speed = 0;
        this.one_time = false;
        this.special_case = false;
        this.pre_time = 0;
        this.bSeek = false;
        this.init_time = 0;
        this.seektime = 0;
        this.totaltime = 0;
        this.m_hour = 0;
        this.m_minute = 0;
        this.m_second = 0;
        this.temp_hour = 0;
        this.temp_minute = 0;
        this.temp_second = 0;
        this.btimeshift = false;
        this.bhandlemediakey = false;
        this.fbackforward = 0;
        this.mSHCallback = new C00419();
        this.videoView = null;
        this.toast = null;
        this.countdownDialog = null;
        this.counterTimer = null;
    }

    static {
        temp_pos = 0;
    }

    private void CheckParentLockAndShowPasswd() {
        if (MW.epg_get_pf_event(this.serviceInfo, this.epgCurrentEvent, this.epgNextEvent) != 0) {
            this.temp_whichone = -1;
            refreshTeletextSubtitle();
            this.bPwdShow = false;
        } else if (this.epgCurrentEvent.parental_rating + 4 <= SETTINGS.get_parental_rating_value()) {
            this.bPwdShow = false;
            this.temp_whichone = -1;
            refreshTeletextSubtitle();
        } else if (SETTINGS.get_parental_rating_value() != 19) {
            MW.ts_player_stop_play();
            CheckChannelLockAndShowPasswd("1");
        }
    }

    private void CheckChannelLockAndShowPasswd(String pos) {
        System.out.println("called place : " + pos);
//        if (SETTINGS.get_channel_lock_onoff()) {
//            if (this.inSubtitleMode) {
//                Log.e("LEE", "stop txt_sub");
//                stopTeletextSubtitle();
//            }
//            CloseTmpPlayStatusInfo(-1);
//            if (this.dia_currentDialog != null) {
//                this.dia_currentDialog.dismiss();
//                this.dia_currentDialog = null;
//            }
//            this.dia_currentDialog = new PasswordDialog(this, R.style.MyDialog, new C00341(), new C00352());
//            this.dia_currentDialog.show();
//            ((PasswordDialog) this.dia_currentDialog).setHandleUpDown(true);
//            SETTINGS.bPass = false;
//            this.book_enableRecord = false;
//            this.bPwdShow = true;
//            return;
//        }
        MW.ts_player_play_current(true, false);
    }

    private void SendDeviceIsFullMsg() {
        this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(5), 2000);
    }

    private void RemoveDeviceIsFullMsg() {
        this.localMsgHandler.removeMessages(5);
    }

    private void CloseInfoBarDelayed(int delayMS) {
        System.out.println("delayMs" + delayMS);
        this.localMsgHandler.removeMessages(0);
        if (delayMS > 0) {
            this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(0), (long) delayMS);
            return;
        }
        this.rl_infoBar.setVisibility(View.INVISIBLE);
        this.anim.stop();
    }

    private void CloseTmpPlayStatusInfo(int delayMS) {
        this.localMsgHandler.removeMessages(2);
        if (delayMS > 0) {
            this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(2), (long) delayMS);
        } else if (delayMS == 0) {
            disableShowPlayStatus = false;
            rl_playStatusBar.setVisibility(View.INVISIBLE);
            MW.ts_player_require_play_status();
        }
    }

    private void CloseChannelNumber() {
        this.localMsgHandler.removeMessages(3);
        this.rl_channelNumber.setVisibility(View.INVISIBLE);
    }

    private void PlayByChannelNumberDelayed(int delayMS) {
        this.localMsgHandler.removeMessages(3);
        if (delayMS > 0) {
            this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(3), (long) delayMS);
        } else {
            this.rl_channelNumber.setVisibility(View.INVISIBLE);
        }
    }

    private void PlayByChannelNumber() {
        CloseChannelNumber();
        if (this.bInRecording && MW.db_get_service_info_by_channel_number(this.channel_number, this.serviceInfo) == 0 && !checkEnableSameTPRecordService(this.serviceInfo.service_type, this.serviceInfo.service_index)) {
            ShowRecordAlertDialog();
            return;
        }
        switch (MW.ts_player_play_by_channel_number(this.channel_number, false)) {
            case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                if (this.rl_channelList.getVisibility() != View.GONE) {
                    this.rl_channelList.setVisibility(View.GONE);
                }
                CloseTmpPlayStatusInfo(1000);
                showInfoBar();
                CheckParentLockAndShowPasswd();
            case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                ShowTmpPlayStatus(getString(R.string.NO_CHANNEL));
            case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
                ShowTmpPlayStatus(getString(R.string.NO_THIS_CHANNEL));
                CloseTmpPlayStatusInfo(3000);
            case MW.RET_SERVICE_LOCK /*44*/:
                if (this.rl_channelList.getVisibility() != View.GONE) {
                    this.rl_channelList.setVisibility(View.GONE);
                }
                CloseTmpPlayStatusInfo(1000);
                showInfoBar();
                CheckChannelLockAndShowPasswd("6");
            default:
        }
    }

    private void UpdateInfoBarSignalStatus(tuner_signal_status signalInfo) {
        if (signalInfo.locked) {
            this.prg_infoBar_signalStrength.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_locked));
            this.prg_infoBar_signalQuality.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_locked));
        } else {
            this.prg_infoBar_signalStrength.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_unlock));
            this.prg_infoBar_signalQuality.setProgressDrawable(getResources().getDrawable(R.drawable.signal_progbar_unlock));
        }
        this.prg_infoBar_signalStrength.setProgress(signalInfo.strength);
        this.prg_infoBar_signalQuality.setProgress(signalInfo.quality);
        if (signalInfo.error) {
            this.txv_infoBar_signalStrength.setText("I2C");
            this.txv_infoBar_signalQuality.setText("Error");
            return;
        }
        this.txv_infoBar_signalStrength.setText(signalInfo.strength + "%");
        this.txv_infoBar_signalQuality.setText(signalInfo.quality + "%");
    }

    private void UpdateCurrentNextEpgInfo() {
        Log.e("EPG", "UpdateCurrentNextEpgInfo");
        if (MW.epg_get_pf_event(this.serviceInfo, this.epgCurrentEvent, this.epgNextEvent) == 0) {
            if (this.epgCurrentEvent.event_name.length() > 0) {
                Log.e("EPG", " current : " + String.format("%02d:%02d - %02d:%02d  ", new Object[]{Integer.valueOf(this.epgCurrentEvent.start_hour), Integer.valueOf(this.epgCurrentEvent.start_minute), Integer.valueOf(this.epgCurrentEvent.end_hour), Integer.valueOf(this.epgCurrentEvent.end_minute)}) + this.epgCurrentEvent.event_name + " :    " + this.epgCurrentEvent.play_progress + "%    Rating : " + this.epgCurrentEvent.parental_rating);
            }
            if (this.epgNextEvent.event_name.length() > 0) {
                Log.e("EPG", "    next : " + String.format("%02d:%02d - %02d:%02d  ", new Object[]{Integer.valueOf(this.epgNextEvent.start_hour), Integer.valueOf(this.epgNextEvent.start_minute), Integer.valueOf(this.epgNextEvent.end_hour), Integer.valueOf(this.epgNextEvent.end_minute)}) + this.epgNextEvent.event_name);
            }
            if (this.epgCurrentEvent.event_name.length() > 0) {
                this.txv_infoBar_currentEpg.setText(String.format("%02d:%02d - %02d:%02d  ", new Object[]{Integer.valueOf(this.epgCurrentEvent.start_hour), Integer.valueOf(this.epgCurrentEvent.start_minute), Integer.valueOf(this.epgCurrentEvent.end_hour), Integer.valueOf(this.epgCurrentEvent.end_minute)}) + this.epgCurrentEvent.event_name);
            } else {
                this.txv_infoBar_currentEpg.setText(R.string.no_current_event);
            }
            if (this.epgNextEvent.event_name.length() > 0) {
                this.txv_infoBar_nextEpg.setText(String.format("%02d:%02d - %02d:%02d  ", new Object[]{Integer.valueOf(this.epgNextEvent.start_hour), Integer.valueOf(this.epgNextEvent.start_minute), Integer.valueOf(this.epgNextEvent.end_hour), Integer.valueOf(this.epgNextEvent.end_minute)}) + this.epgNextEvent.event_name);
            } else {
                this.txv_infoBar_nextEpg.setText(R.string.no_next_event);
            }
            int i = ((this.epgCurrentEvent.end_hour - this.epgCurrentEvent.start_hour) * 60) + (this.epgCurrentEvent.end_minute - this.epgCurrentEvent.start_minute);
            int i2 = ((this.dateTime.hour - this.epgCurrentEvent.start_hour) * 60) + (this.dateTime.minute - this.epgCurrentEvent.start_minute);
            if (i > 0) {
                ShowEpgTimeProg(i, i2);
            } else {
                HideEpgTimeProg();
            }
            if (this.epgCurrentEvent.parental_rating + 4 > SETTINGS.get_parental_rating_value() && SETTINGS.get_parental_rating_value() != 19) {
                Log.e("EPG", "parent lock ,stop play ");
                if (!SETTINGS.bPass) {
                    MW.ts_player_stop_play();
                    CheckChannelLockAndShowPasswd("24");
                    return;
                }
                return;
            }
            return;
        }
        this.txv_infoBar_currentEpg.setText(R.string.no_current_event);
        this.txv_infoBar_nextEpg.setText(R.string.no_next_event);
        HideEpgTimeProg();
        Log.e("EPG", "get epg pf event failed..");
    }

    private void ShowDYWTSDialog() {
        Dialog dywtsDig = new DyWTSDia(this, R.style.MyDialog);
        dywtsDig.show();
        LayoutParams lp = dywtsDig.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dywtsDig.getWindow().setAttributes(lp);
        dywtsDig.getWindow().addFlags(2);
    }

    private void TimeShiftEndDelayShowPlayStatus() {
        this.localMsgHandler.removeMessages(7);
        this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(7), 5000);
    }

    private void ShowTimeShiftSeekTimeSetDialog() {
        this.temp_second = this.seektime % 60;
        this.temp_minute = (this.seektime / 60) % 60;
        this.temp_hour = this.seektime / 3600;
        View inflate = LayoutInflater.from(this).inflate(R.layout.seektimeedit, null);
        this.hour = (EditText) inflate.findViewById(R.id.seektime_hours);
        this.minute = (EditText) inflate.findViewById(R.id.seektime_minutes);
        this.second = (EditText) inflate.findViewById(R.id.seektime_seconds);
        this.tips_txt = (TextView) inflate.findViewById(R.id.tips_txt);
        this.tips_txt.setText(getResources().getString(R.string.seekTimeTitletips));
        this.hour.setText(String.format("%02d", new Object[]{Integer.valueOf(this.m_hour)}));
        this.minute.setText(String.format("%02d", new Object[]{Integer.valueOf(this.m_minute)}));
        this.second.setText(String.format("%02d", new Object[]{Integer.valueOf(this.m_second)}));
        if (this.totaltime / 3600 == 0) {
            this.hour.setFocusable(false);
            this.hour.setBackgroundResource(R.drawable.seektime_eidtback);
        } else {
            this.hour.setFocusable(true);
        }
        if (this.totaltime / 3600 != 0 || (this.totaltime / 60) % 60 != 0) {
            this.minute.setFocusable(true);
        } else if (this.totaltime % 60 != 0) {
            this.minute.setFocusable(false);
            this.minute.setBackgroundResource(R.drawable.seektime_eidtback);
        }
        this.hour.setInputType(0);
        this.minute.setInputType(0);
        this.second.setInputType(0);
        this.hour.addTextChangedListener(new C00364());
        this.minute.addTextChangedListener(new C00375());
        this.second.addTextChangedListener(new C00386());
        Builder builder = new Builder(this);
        builder.setView(inflate);
        Log.e("LEE", "hour is focused " + this.hour.isFocused());
        builder.setOnKeyListener(new C00397());
        this.dia_currentDialog = builder.show();
        LayoutParams attributes = this.dia_currentDialog.getWindow().getAttributes();
        attributes.dimAmount = 0.0f;
        this.dia_currentDialog.getWindow().setAttributes(attributes);
        this.dia_currentDialog.getWindow().addFlags(2);
    }

    private void ShowTmpPlayStatus(String s) {
        this.txv_playStatus.setText(s);
        disableShowPlayStatus = true;
        rl_playStatusBar.setVisibility(View.VISIBLE);
        CloseTmpPlayStatusInfo(3000);
    }

    private void SwitchRegions(int i) {
        int db_get_service_region_current_pos = MW.db_get_service_region_current_pos();
        int db_get_service_region_count = MW.db_get_service_region_count();
        switch (i) {
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                db_get_service_region_current_pos--;
                if (db_get_service_region_current_pos < 0) {
                    db_get_service_region_current_pos = db_get_service_region_count;
                    break;
                }
                break;
            case MW.RET_MEMORY_ERROR /*22*/:
                db_get_service_region_current_pos++;
                if (db_get_service_region_current_pos > db_get_service_region_count) {
                    db_get_service_region_current_pos = 0;
                    break;
                }
                break;
        }
//        if (db_get_service_region_count > 0 && MW.db_set_service_region_current_pos(r0)) {
//            db_get_service_region_current_pos = MW.ts_player_play_current(false, false);
//            if (db_get_service_region_current_pos == 0) {
//                RefreshShowChannelList();
//                CheckParentLockAndShowPasswd();
//            } else if (db_get_service_region_current_pos == 44) {
//                RefreshShowChannelList();
//                CheckChannelLockAndShowPasswd("7");
//            }
//        }
    }

    private void SwitchFavGroup(int i) {
        int db_get_service_available_fav_group_current_pos = MW.db_get_service_available_fav_group_current_pos();
        int db_get_service_available_fav_group_count = MW.db_get_service_available_fav_group_count() - 1;
        System.out.println("favGroupCount : " + db_get_service_available_fav_group_count + "postion : " + db_get_service_available_fav_group_current_pos);
        switch (i) {
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                db_get_service_available_fav_group_current_pos--;
                if (db_get_service_available_fav_group_current_pos < 0) {
                    db_get_service_available_fav_group_current_pos = db_get_service_available_fav_group_count;
                    break;
                }
                break;
            case MW.RET_MEMORY_ERROR /*22*/:
                db_get_service_available_fav_group_current_pos++;
                if (db_get_service_available_fav_group_current_pos > db_get_service_available_fav_group_count) {
                    db_get_service_available_fav_group_current_pos = 0;
                    break;
                }
                break;
        }
        if (db_get_service_available_fav_group_count > 0 && MW.db_set_service_available_fav_group_current_pos(db_get_service_available_fav_group_current_pos)) {
            if (MW.db_get_service_count(this.current_service_type) != 0) {
                temp_pos = db_get_service_available_fav_group_current_pos;
                db_get_service_available_fav_group_current_pos = MW.ts_player_play_current(false, false);
                if (db_get_service_available_fav_group_current_pos == 0) {
                    CheckParentLockAndShowPasswd();
                } else if (db_get_service_available_fav_group_current_pos == 44) {
                    CheckChannelLockAndShowPasswd("8");
                }
            }
            RefreshShowChannelList();
        }
    }

    private void DoSatAction() {
        RegionSelectDialog dia = new RegionSelectDialog(this, R.style.MyDialog);
        dia.show();
        LayoutParams lp = dia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dia.getWindow().setAttributes(lp);
        dia.getWindow().addFlags(2);
    }

    private void DoFavAction() {
        FavGroupSelectDialog dia = new FavGroupSelectDialog(this, R.style.MyDialog);
        dia.show();
        LayoutParams lp = dia.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dia.getWindow().setAttributes(lp);
        dia.getWindow().addFlags(2);
    }

    private void showInfoBar() {
        if (!MW.db_check_service_in_fav_mode() || MW.db_get_service_count(this.current_service_type) != 0) {
            this.anim.start();
            if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                if (SETTINGS.bHongKongDTMB) {
                    if (HongKongDTMBHD.isHongKongDTMBHDChannel(this.serviceInfo.video_pid, this.serviceInfo.audio_info[0].audio_pid) || this.serviceInfo.is_hd) {
                        this.infoBar_hd.setVisibility(View.VISIBLE);
                        this.infoBar_hd.setText(R.string.static_string_HD);
                        this.infoBar_hd.getPaint().setFakeBoldText(true);
                        this.infoBar_hd.setTextColor(Color.rgb(0, 0, 0));
                        this.infoBar_hd.setBackgroundResource(R.drawable.infomessage_blue1);
                    } else {
                        this.infoBar_hd.setVisibility(View.VISIBLE);
                        this.infoBar_hd.setText(R.string.static_string_SD);
                        this.infoBar_hd.setBackgroundResource(R.drawable.infomessage_yellow);
                        this.infoBar_hd.setTextColor(Color.rgb(255, 255, 255));
                    }
                } else if (this.serviceInfo.is_hd) {
                    this.infoBar_hd.setVisibility(View.VISIBLE);
                    this.infoBar_hd.setText(R.string.static_string_HD);
                    this.infoBar_hd.getPaint().setFakeBoldText(true);
                    this.infoBar_hd.setTextColor(Color.rgb(0, 0, 0));
                    this.infoBar_hd.setBackgroundResource(R.drawable.infomessage_blue1);
                } else {
                    this.infoBar_hd.setVisibility(View.VISIBLE);
                    this.infoBar_hd.setText(R.string.static_string_SD);
                    this.infoBar_hd.setBackgroundResource(R.drawable.infomessage_yellow);
                    this.infoBar_hd.setTextColor(Color.rgb(255, 255, 255));
                }
                if (this.serviceInfo.is_scrambled) {
                    this.infoBar_scramble.setVisibility(View.VISIBLE);
                } else {
                    this.infoBar_scramble.setVisibility(View.GONE);
                }
                if (this.serviceInfo.teletext_info.length > 0) {
                    this.infoBar_teletext.setVisibility(View.VISIBLE);
                } else {
                    this.infoBar_teletext.setVisibility(View.GONE);
                }
                if (this.serviceInfo.subtitle_info.length > 0) {
                    this.infoBar_subtitle.setVisibility(View.VISIBLE);
                } else {
                    this.infoBar_subtitle.setVisibility(View.GONE);
                }
                SETTINGS.set_led_channel(this.serviceInfo.channel_number_display);
                this.txv_infoBar_channelNum.setText(this.serviceInfo.channel_number_display + "");
                this.txv_infoBar_channelName.setText(this.serviceInfo.service_name);
                UpdateCurrentNextEpgInfo();
            }
            if (MW.get_date_time(this.dateTime) == 0) {
                this.txv_infBar_dateTime.setText(SETTINGS.getDateTimeAdjustSystem(this.dateTime, false));
            }
            if (this.rl_infoBar.getVisibility() != View.VISIBLE) {
                this.rl_infoBar.setVisibility(View.VISIBLE);
            }
            MW.tuner_require_tuner_status(0);
            if (this.serviceInfo.service_type != 1) {
                CloseInfoBarDelayed(SETTINGS.get_banner_time());
            }
        }
    }

    private void ShowCurrentServiceInfo() {
        Dialog serviceInfo = new ServiceInfoDia(this, R.style.MyDialog);
        serviceInfo.show();
        LayoutParams lp = serviceInfo.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        serviceInfo.getWindow().setAttributes(lp);
        serviceInfo.getWindow().addFlags(2);
    }

    private void RefreshTvRadioBackground() {
        if (MW.db_get_current_service_info(this.serviceInfo) != 0) {
            return;
        }
        if (this.serviceInfo.service_type == 0) {
            System.out.println("TV");
            this.mainwnd_bg.setVisibility(View.INVISIBLE);
            return;
        }
        System.out.println("Radio");
        this.localMsgHandler.removeMessages(0);
        this.mainwnd_bg.setVisibility(View.VISIBLE);
    }

    private void RefreshShowChannelList() {
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            this.current_record_service_region_index = this.serviceInfo.region_index;
            this.current_record_service_tp_index = this.serviceInfo.tp_index;
            if (MW.db_check_service_in_fav_mode()) {
                int db_get_service_available_fav_group_current_pos = MW.db_get_service_available_fav_group_current_pos();
                int db_get_service_available_fav_group_index = MW.db_get_service_available_fav_group_index(db_get_service_available_fav_group_current_pos);
                System.out.println("pos :" + db_get_service_available_fav_group_current_pos + "---------" + db_get_service_available_fav_group_index);
                this.txv_listTitle.setText(this.favgroupname[db_get_service_available_fav_group_index]);
            } else if (MW.db_get_service_region_current_pos() == 0) {
                this.txv_listTitle.setText(R.string.All);
            } else if (this.SystemType == 0 || this.SystemType == 2) {
                if (MW.db_dvb_s_get_current_sat_info(0, this.satInfo) == 0) {
                    this.txv_listTitle.setText(this.satInfo.sat_name);
                }
            } else if (MW.db_dvb_t_get_current_area_info(this.areaInfo) == 0) {
                this.txv_listTitle.setText(SETTINGS.get_area_string_by_index(this, this.areaInfo.area_index));
            }
            this.mFirstVisibleItem = this.current_service_index;
            this.channel_list.setFocusableInTouchMode(true);
            this.channel_list.requestFocus();
            this.channel_list.requestFocusFromTouch();
            this.channel_list.setCustomSelection(this.mFirstVisibleItem);
            this.channelListAdapter.notifyDataSetChanged();
            this.rl_channelList.setVisibility(View.VISIBLE);
            CloseInfoBarDelayed(0);
        }
        RefreshTvRadioBackground();
    }

    private void ShowRecordTimeEditTextView() {
        this.storageDeviceInfo.refreshDevice();
        System.out.println(this.storageDeviceInfo.getDeviceItem(this.device_no).Path);
        String str = this.storageDeviceInfo.getDeviceItem(this.device_no).spare;
        if (str != null) {
            long spareSize = StorageDevice.getSpareSize(str);
            if (spareSize != -1 && spareSize < 2097152) {
                SETTINGS.makeText(this, R.string.spaceisfull, 0);
                this.bInRecording = false;
                return;
            }
        }
        Dialog inputTimeDialog = new InputTimeDialog(this, R.style.MyDialog);
        inputTimeDialog.show();
        LayoutParams attributes = inputTimeDialog.getWindow().getAttributes();
        attributes.dimAmount = 0.0f;
        inputTimeDialog.getWindow().setAttributes(attributes);
        inputTimeDialog.getWindow().addFlags(2);
    }

    private void DoRecordFunction(int i, int i2) {
        Log.e("LEE", "DoRecordFunction record_time = " + i2);
        if (i2 >= 0) {
            int pvr_record_start;
            this.storageDeviceInfo.refreshDevice();
            String str = this.storageDeviceInfo.getDeviceItem(i).spare;
            if (str != null) {
                long spareSize = StorageDevice.getSpareSize(str);
                if (spareSize != -1 && spareSize < 2097152) {
                    SETTINGS.makeText(this, R.string.spaceisfull, 0);
                    return;
                }
            }
            stopTeletextSubtitle();
            if (!SETTINGS.bHongKongDTMB || this.epgCurrentEvent.event_name.length() <= 0) {
                pvr_record_start = MW.pvr_record_start(this.current_service_type, this.current_service_index, i2, this.storageDeviceInfo.getDeviceItem(i).Path, null);
            } else {
                pvr_record_start = MW.pvr_record_start(this.current_service_type, this.current_service_index, i2, this.storageDeviceInfo.getDeviceItem(i).Path, this.epgCurrentEvent.event_name);
            }
            if (pvr_record_start == 0) {
                this.sCurrentRecordStoreDir = new String(this.storageDeviceInfo.getDeviceItem(i).Path);
                this.bInRecording = true;
                SETTINGS.makeText(this, R.string.start_record, 0);
                SendDeviceIsFullMsg();
                Log.e("DtvMainActivity", "start recording....");
            } else if (pvr_record_start == 41) {
                Log.e("DtvMainActivity", "already recording....");
                this.bInRecording = true;
                SETTINGS.makeText(this, R.string.already_record, 0);
            } else if (pvr_record_start == 6) {
                Log.e("DtvMainActivity", "record failed, not this channel....");
                SETTINGS.makeText(this, R.string.record_failed_no_this_channel, 0);
            } else if (pvr_record_start == 30) {
                Log.e("DtvMainActivity", "record failed, invalid object....");
                SETTINGS.makeText(this, R.string.record_failed_invalid_object, 0);
            }
            refreshTeletextSubtitle();
        }
    }

    private void DoTimeShiftFunction(int i, int i2) {
        Log.e("LEE", "DoTimeShiftFunction record_time = " + i2);
        if (i2 <= 0) {
            SETTINGS.makeText(this, R.string.please_select_correct_time, 0);
        } else if (this.timeshift == null) {
            this.bInRecording = true;
            CloseInfoBarDelayed(0);
            String str = this.storageDeviceInfo.getDeviceItem(i).Path;
            System.out.println(str);
            this.storageDeviceInfo.refreshDevice();
            String str2 = this.storageDeviceInfo.getDeviceItem(i).spare;
            if (str2 != null) {
                long spareSize = StorageDevice.getSpareSize(str2);
                if (spareSize != -1 && spareSize < 2097152) {
                    SETTINGS.makeText(this, R.string.spaceisfull, 0);
                    this.bInRecording = false;
                    return;
                }
            }
            stopTeletextSubtitle();
            switch (MW.pvr_timeshift_start(this.current_service_type, this.serviceInfo.service_index, i2, str)) {
                case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                    Log.e("DtvMainActivity", "timeshift RET_OK");
                    this.bInit_timeShift = true;
                    this.sCurrentRecordStoreDir = new String(this.storageDeviceInfo.getDeviceItem(i).Path);
                    SETTINGS.makeText(this, R.string.start_timeshift, 0);
                    SendDeviceIsFullMsg();
                    break;
                case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO /*6*/:
                    Log.e("DtvMainActivity", "timeshift RET_NO_THIS_CHANNEL");
                    break;
                case MW.RET_INVALID_OBJECT /*30*/:
                    Log.e("DtvMainActivity", "timeshift RET_INVALID_OBJECT");
                    break;
                case MW.RET_ALREADY_RECORDING /*41*/:
                    Log.e("DtvMainActivity", "timeshift RET_ALREADY_RECORDING");
                    break;
            }
            refreshTeletextSubtitle();
            this.timeshift = new TimeShiftControl(this);
            this.timeshift.setChannelNum(this.serviceInfo.service_index + 1);
            this.timeshift.setChannelName(this.serviceInfo.service_name);
            this.timeshift.setCurrentTime("00:00:00");
            this.timeshift.setDuration("00:00:00");
            this.timeshift.showTimeShiftLogo();
            this.timeshift.setSeekBarMax(10);
            this.timeshift.setSeekBarSecondProgress(0);
            this.timeshift.setSeekBarProgress(0);
            this.timeshift.show();
            this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
        }
    }

    void startTeletextSubtitle(boolean z, int i, service th_dtv_mw_data_service) {
        if (z) {
            this.inTeletextMode = true;
            if (this.rl_loading.getVisibility() != View.VISIBLE) {
                this.rl_loading.setVisibility(View.VISIBLE);
            }
            Log.i("LEE", "startTeletextSubtitle");
            this.rl_channelList.setVisibility(View.GONE);
            CloseInfoBarDelayed(0);
        } else {
            this.inSubtitleMode = true;
        }
        this.ttx_sub.start(z, i, th_dtv_mw_data_service);
    }

    void refreshTeletextSubtitle() {
        stopTeletextSubtitle();
        if (this.temp_whichone != 0) {
            if (!SETTINGS.get_subtitle_default_on()) {
                this.temp_whichone = 0;
            } else if (MW.db_get_current_service_info(this.serviceInfo) == 0 && this.serviceInfo.subtitle_info.length > 0) {
                if (this.temp_whichone == -1) {
                    int i = 0;
                    int i2 = 0;
                    boolean z = false;
                    while (i2 < this.serviceInfo.subtitle_info.length) {
                        if (!this.serviceInfo.subtitle_info[i2].ISO_639_language_code.equals(SETTINGS.get_subtitle_language())) {
                            if (this.serviceInfo.subtitle_info[i2].subtitling_type != 2 && this.serviceInfo.subtitle_info[i2].subtitling_type != 5) {
                                i = i2 + 1;
                                break;
                            } else {
                                z = true;
                                i2++;
                            }
                        } else if (this.serviceInfo.subtitle_info[i2].subtitling_type == 2 || this.serviceInfo.subtitle_info[i2].subtitling_type == 5) {
//                            i = z;
                            int i3 = i2;
                            while (i3 < this.serviceInfo.subtitle_info.length) {
                                if (this.serviceInfo.subtitle_info[i3].subtitling_type != 2 && this.serviceInfo.subtitle_info[i3].subtitling_type != 5) {
                                    i = i3 + 1;
                                    break;
                                } else {
                                    i = i2 + 1;
                                    i3++;
                                }
                            }
                        } else {
                            i = i2 + 1;
                        }
                    }
                    boolean z2 = z;
                    startTeletextSubtitle(false, i - 1, this.serviceInfo);
                    return;
                }
                System.out.println("temp_which != -1  and position = " + (this.temp_whichone - 1));
                startTeletextSubtitle(false, this.temp_whichone - 1, this.serviceInfo);
            }
        }
    }

    void stopTeletextSubtitle() {
        this.inTeletextMode = false;
        this.bTeletextShowed = false;
        this.inSubtitleMode = false;
        this.ttx_sub.stop();
        if (this.rl_loading.getVisibility() != View.GONE) {
            Log.i("LEE", "stopTeletextSubtitle");
            this.rl_loading.setVisibility(View.GONE);
        }
        if (this.et_ttxPageNumber.getVisibility() != View.GONE) {
            this.et_ttxPageNumber.setVisibility(View.GONE);
        }
    }

    private void HideSubtitle() {
        if (!this.inTeletextMode && this.bUpdate) {
            this.ttx_sub.hide();
            this.ttx_sub.update();
        }
    }

    private void ShowSubtitle() {
        if (!this.inTeletextMode && this.bUpdate) {
            this.ttx_sub.show();
            this.ttx_sub.update();
        }
    }

    private void ShowLoadingIcon() {
        this.bUpdate = false;
        this.localMsgHandler.sendMessageDelayed(this.localMsgHandler.obtainMessage(6), 500);
    }

    private boolean onTeletextKeyDown(int i) {
        switch (i) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                refreshTeletextSubtitle();
                disableShowPlayStatus = false;
                MW.ts_player_require_play_status();
                return true;
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                Log.e("LEE", "keyup keyup keyup");
                if (MW.teletext_goto_next_page() != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case MW.RET_INVALID_TP_INDEX /*20*/:
                if (MW.teletext_goto_prev_page() != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case MW.RET_INVALID_TYPE /*23*/:
            case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                return true;
            case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                if (MW.teletext_goto_color_link(1) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
                if (MW.teletext_goto_color_link(3) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_YELLOW /*169*/:
                if (MW.teletext_goto_color_link(2) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_RECALL /*666*/:
                if (MW.teletext_goto_home() != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                if (MW.teletext_goto_color_link(0) != 0) {
                    return true;
                }
                ShowLoadingIcon();
                return true;
            default:
                return false;
        }
    }

    private void ShowRecordAlertDialog() {
        this.recorddialog = new ExitRecordDia(this, R.style.MyDialog);
        this.recorddialog.show();
        LayoutParams lp = this.recorddialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        this.recorddialog.getWindow().setAttributes(lp);
        this.recorddialog.getWindow().addFlags(2);
    }

    private void MainMenuSetup() {
        SETTINGS.send_led_msg("NENU");
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.android.DTVLauncher", "com.android.DTVLauncher.DTVLauncher"));
        intent.addFlags(268435456);
        intent.addFlags(67108864);
        startActivity(intent);
    }

    private void DoFindChannelFunction() {
        if (MW.db_get_total_service_count() > 0) {
            if (MW.db_get_service_region_current_pos() != 0) {
                MW.db_service_set_in_region_mode();
                if (MW.db_set_service_region_current_pos(0)) {
                    int ts_player_play_current = MW.ts_player_play_current(false, false);
                    if (ts_player_play_current == 0) {
                        RefreshShowChannelList();
                        CheckParentLockAndShowPasswd();
                    } else if (ts_player_play_current == 44) {
                        RefreshShowChannelList();
                        CheckChannelLockAndShowPasswd("7");
                    }
                }
            }
            ShowFindDialog();
        }
    }

    private void ShowFindDialog() {
        if (this.dia_currentDialog != null) {
            this.dia_currentDialog.dismiss();
            this.dia_currentDialog = null;
        }
        if (rl_playStatusBar.getVisibility() != View.INVISIBLE) {
            rl_playStatusBar.setVisibility(View.INVISIBLE);
        }
        disableShowPlayStatus = true;
        this.dia_currentDialog = new FindDialog(this, R.style.MyDialog, this.current_service_type, this.current_service_index, new C00408());
        this.dia_currentDialog.show();
        this.dia_currentDialog.getWindow().addFlags(2);
    }

    boolean checkEnableSameTPRecordService(int service_type, int service_index) {
        if (SETTINGS.ENABLE_SWITCH_CHANNEL_WHILE_RECORDING && service_index >= 0 && service_index < MW.db_get_service_count(this.current_service_type) && MW.db_get_service_info(service_type, service_index, this.serviceInfo) == 0 && this.serviceInfo.region_index == this.current_record_service_region_index && this.serviceInfo.tp_index == this.current_record_service_tp_index) {
            return true;
        }
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean z = false;
        if (this.bTeletextShowed && onTeletextKeyDown(i)) {
            return true;
        }
        Intent intent;
        boolean z2 = false;
        int i2;
        int ts_player_play_up;
        LayoutParams attributes;
        LayoutParams attributes2;
        switch (i) {
            case MW.VIDEO_STREAM_TYPE_H265 /*4*/:
                if (this.rl_channelNumber.getVisibility() != View.INVISIBLE) {
                    CloseChannelNumber();
                    z = true;
                } else {
                    if (this.rl_channelList.getVisibility() == View.VISIBLE) {
                        if (MW.db_get_service_count(this.current_service_type) == 0) {
                            System.out.println("temp_pos" + temp_pos);
                            MW.db_set_service_total_fav_group_current_index(temp_pos);
                            MW.ts_player_play_current(false, false);
                        }
                        listview_AnimationExit();
                        this.rl_channelList.setVisibility(View.GONE);
                        showInfoBar();
                        z = true;
                    } else if (this.rl_infoBar.getVisibility() != View.INVISIBLE) {
                        CloseInfoBarDelayed(0);
                        z = true;
                    }
                    CloseTmpPlayStatusInfo(-1);
                }
                if (this.rl_loading.getVisibility() == 0) {
                    if (!this.bTeletextShowed) {
                        refreshTeletextSubtitle();
                    }
                    z = true;
                }
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    z = true;
                }
                if (z) {
                    Log.d("DtvMainActivity", "nodiaflag is true\n");
                    return true;
//                } else if (SystemProperties.get("tv.dtv.standard", "").equals("")) {
//                    intent = new Intent();
//                    String str = SystemProperties.get("ro.platform.launcher.package", "com.amlogic.mediaboxlauncher");
//                    String str2 = SystemProperties.get("ro.platform.launcher.start", "com.amlogic.mediaboxlauncher.Launcher");
//                    if (SystemProperties.get("sw.has.dtv", "").equals("")) {
//                        intent.setComponent(new ComponentName(str, str2));
//                    } else {
//                        intent.setComponent(new ComponentName("com.android.DTVLauncher", "com.android.DTVLauncher.DTVLauncher"));
//                    }
//                    intent.addFlags(268435456);
//                    intent.addFlags(67108864);
//                    startActivity(intent);
//                    return true;
                } else {
                    finish();
                    return true;
                }
            case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
            case MW.SERVICE_SORT_TYPE_CAS /*8*/:
            case MW.SEARCH_STATUS_SEARCH_END /*9*/:
            case MW.RET_TP_EXIST /*10*/:
            case MW.RET_INVALID_SAT /*11*/:
            case MW.RET_INVALID_AREA /*12*/:
            case MW.RET_INVALID_TP /*13*/:
            case MW.RET_BOOK_EVENT_INVALID /*14*/:
            case MW.RET_BOOK_CONFLICT_EVENT /*15*/:
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_NO_RATIO /*16*/:
                if (this.bInit_timeShift) {
                    ShowRecordAlertDialog();
                    return true;
                }
                if (this.rl_channelNumber.getVisibility() == 4) {
                    this.channel_number = i - 7;
                    this.rl_channelNumber.setVisibility(View.VISIBLE);
                } else {
                    this.channel_number = (((this.channel_number * 10) + i) - 7) % 10000;
                }
                this.txv_channelNumber.setText(Integer.toString(this.channel_number));
                PlayByChannelNumberDelayed(3000);
                return true;
            case MW.SUBTITLING_TYPE_DVB_SUBTITLE_2_21_1_RATIO /*19*/:
                if (this.bInRecording) {
                    if (this.bInit_timeShift) {
                        z2 = false;
                    } else if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                        i2 = this.serviceInfo.service_index + 1;
                        if (i2 >= MW.db_get_service_count(this.current_service_type)) {
                            i2 = 0;
                        }
                        if (!checkEnableSameTPRecordService(this.current_service_type, i2)) {
                            z2 = false;
                        }
                    }
                    if (z2) {
                        ShowRecordAlertDialog();
                        return true;
                    }
                    if (this.rl_channelNumber.getVisibility() != 4) {
                        CloseChannelNumber();
                    }
                    ts_player_play_up = MW.ts_player_play_up(50, false);
                    if (ts_player_play_up == 0) {
                        showInfoBar();
                        CheckParentLockAndShowPasswd();
                        return true;
                    } else if (ts_player_play_up == 44) {
                        return true;
                    } else {
                        showInfoBar();
                        CheckChannelLockAndShowPasswd("18");
                        return true;
                    }
                }
                z2 = true;
                if (z2) {
                    ShowRecordAlertDialog();
                    return true;
                }
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                ts_player_play_up = MW.ts_player_play_up(50, false);
                if (ts_player_play_up == 0) {
                    showInfoBar();
                    CheckParentLockAndShowPasswd();
                    return true;
                } else if (ts_player_play_up == 44) {
                    return true;
                } else {
                    showInfoBar();
                    CheckChannelLockAndShowPasswd("18");
                    return true;
                }
            case MW.RET_INVALID_TP_INDEX /*20*/:
                if (this.bInRecording) {
                    if (this.bInit_timeShift) {
                        z2 = false;
                    } else if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                        i2 = this.serviceInfo.service_index - 1;
                        if (i2 < 0) {
                            i2 = MW.db_get_service_count(this.current_service_type) - 1;
                        }
                        if (!checkEnableSameTPRecordService(this.current_service_type, i2)) {
                            z2 = false;
                        }
                    }
                    if (z2) {
                        ShowRecordAlertDialog();
                        return true;
                    }
                    if (this.rl_channelNumber.getVisibility() != 4) {
                        CloseChannelNumber();
                    }
                    ts_player_play_up = MW.ts_player_play_down(50, false);
                    if (ts_player_play_up == 0) {
                        showInfoBar();
                        CheckParentLockAndShowPasswd();
                        return true;
                    } else if (ts_player_play_up == 44) {
                        return true;
                    } else {
                        showInfoBar();
                        CheckChannelLockAndShowPasswd("19");
                        return true;
                    }
                }
                z2 = true;
                if (z2) {
                    ShowRecordAlertDialog();
                    return true;
                }
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                ts_player_play_up = MW.ts_player_play_down(50, false);
                if (ts_player_play_up == 0) {
                    showInfoBar();
                    CheckParentLockAndShowPasswd();
                    return true;
                } else if (ts_player_play_up == 44) {
                    return true;
                } else {
                    showInfoBar();
                    CheckChannelLockAndShowPasswd("19");
                    return true;
                }
            case MW.RET_INVALID_RECORD_INDEX /*21*/:
                sendKeyEvent(25);
                return true;
            case MW.RET_MEMORY_ERROR /*22*/:
                sendKeyEvent(24);
                return true;
            case MW.RET_INVALID_TYPE /*23*/:
                if (this.bInit_timeShift) {
                    if (this.timeShift_PlayStatus == 4) {
                        MW.pvr_playback_pause();
                        this.bhandlemediakey = true;
                        this.timeShift_PlayStatus = 3;
                        this.timeshift_speed = 0;
                        this.timeshift.showPlayStatusAndSpeed(3, 0);
                        ShowTimeShiftSeekTimeSetDialog();
                        this.timeshift.hideTimeshiftInfoView(0);
                        this.timeshift.showTimeshiftInfoView();
                        return true;
                    } else if (this.timeShift_PlayStatus == 3) {
                        MW.pvr_playback_resume();
                        this.timeShift_PlayStatus = 4;
                        this.timeshift_speed = 0;
                        this.timeshift.showPlayStatusAndSpeed(4, 0);
                        this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                        return true;
                    } else if (this.timeShift_PlayStatus == 2) {
                        MW.pvr_playback_fast_forward(0);
                        this.timeShift_PlayStatus = 4;
                        this.timeshift_speed = 0;
                        this.special_case = true;
                        this.timeshift.showPlayStatusAndSpeed(4, 0);
                        this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                        return true;
                    } else if (this.timeShift_PlayStatus != 1) {
                        return true;
                    } else {
                        MW.pvr_playback_fast_backward(0);
                        this.timeShift_PlayStatus = 4;
                        this.timeshift_speed = 0;
                        this.special_case = true;
                        this.timeshift.showPlayStatusAndSpeed(4, 0);
                        this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                        return true;
                    }
                } else if (this.rl_channelNumber.getVisibility() == 4) {
                    RefreshShowChannelList();
                    listview_Animation();
                    if (!this.inTeletextMode) {
                        return true;
                    }
                    stopTeletextSubtitle();
                    return true;
                } else {
                    PlayByChannelNumber();
                    return true;
                }
            case MW.RET_INVALID_AREA_INDEX_LIST /*27*/:
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                } else if (MW.db_get_total_service_count() <= 0) {
                    return true;
                } else {
                    if (MW.db_get_service_available_fav_group_count() <= 0) {
                        SETTINGS.makeText(this, R.string.no_fav_channel, 0);
                        return true;
                    }
                    DoFavAction();
                    return true;
                }
            case DtvBaseActivity.KEYCODE_SAT /*55*/:
                if (this.SystemType == 0 || this.SystemType == 2) {
                    if (this.bInRecording) {
                        ShowRecordAlertDialog();
                        return true;
                    } else if (MW.db_get_total_service_count() <= 0) {
                        return true;
                    } else {
                        if (MW.db_dvb_s_get_sat_count(0) <= 0) {
                            SETTINGS.makeText(this, R.string.no_satellite, 0);
                            return true;
                        }
                        DoSatAction();
                        return true;
                    }
                } else if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                } else if (MW.db_get_total_service_count() <= 0 || MW.db_dvb_t_get_area_count() <= 0) {
                    return true;
                } else {
                    DoSatAction();
                    return true;
                }
            case DtvBaseActivity.KEYCODE_RECORD /*57*/:
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                if (MW.db_get_current_service_info(this.serviceInfo) == 0 && this.serviceInfo.service_type == 1) {
                    SETTINGS.makeText(this, R.string.radiocantrecord, 0);
                    return true;
                } else if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                } else if (MW.db_get_total_service_count() <= 0 || this.storageDeviceInfo == null) {
                    return true;
                } else {
                    if (this.storageDeviceInfo.getDeviceCount() > 0) {
                        Log.e("DtvMainActivity", "have device : " + this.storageDeviceInfo.getDeviceCount());
                        if (this.storageDeviceInfo.getDeviceCount() <= 1) {
                            this.device_no = 0;
                            if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                                this.current_service_type = this.serviceInfo.service_type;
                                this.current_service_index = this.serviceInfo.service_index;
                                this.current_record_service_region_index = this.serviceInfo.region_index;
                                this.current_record_service_tp_index = this.serviceInfo.tp_index;
                                if (this.enableRecord) {
                                    this.ftimelimit = false;
                                    if (SETTINGS.getRecordLengthIndex() == 4) {
                                        ShowRecordTimeEditTextView();
                                        return true;
                                    }
                                    DoRecordFunction(this.device_no, SETTINGS.getRecordTime());
                                    return true;
                                }
                                SETTINGS.makeText(this, R.string.channel_no_play_cant_record, 0);
                                return true;
                            }
                            SETTINGS.makeText(this, R.string.nosignalcantrecord, 0);
                            return true;
                        } else if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                            this.current_service_type = this.serviceInfo.service_type;
                            this.current_service_index = this.serviceInfo.service_index;
                            this.current_record_service_region_index = this.serviceInfo.region_index;
                            this.current_record_service_tp_index = this.serviceInfo.tp_index;
                            if (this.enableRecord) {
                                this.DeviceInfoDia = new StorageDeviceDialog(this, R.style.MyDialog);
                                this.DeviceInfoDia.show();
                                attributes = this.DeviceInfoDia.getWindow().getAttributes();
                                attributes.dimAmount = 0.0f;
                                this.DeviceInfoDia.getWindow().setAttributes(attributes);
                                this.DeviceInfoDia.getWindow().addFlags(2);
                                return true;
                            }
                            SETTINGS.makeText(this, R.string.channel_no_play_cant_record, 0);
                            return true;
                        } else {
                            SETTINGS.makeText(this, R.string.nosignalcantrecord, 0);
                            return true;
                        }
                    }
                    SETTINGS.makeText(this, R.string.no_storage, 0);
                    Log.e("DtvMainActivity", "not device");
                    return true;
                }
            case DtvBaseActivity.KEYCODE_RECORD_LIST /*61*/:
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                } else if (this.storageDeviceInfo == null) {
                    return true;
                } else {
                    if (this.storageDeviceInfo.getDeviceCount() > 0) {
                        intent = new Intent();
                        intent.setFlags(67108864);
                        intent.setClass(this, RecordListActivity.class);
                        startActivity(intent);
                        return true;
                    }
                    SETTINGS.makeText(this, R.string.no_storage, 0);
                    return true;
                }
            case DtvBaseActivity.KEYCODE_DEL /*67*/:
                return true;
            case DtvBaseActivity.KEYCODE_MEDIA_PLAY_PAUSE /*85*/:
                if (!this.bInit_timeShift) {
                    if (this.rl_channelNumber.getVisibility() != 4) {
                        CloseChannelNumber();
                    }
                    if (this.bInRecording) {
                        ShowRecordAlertDialog();
                        return true;
                    } else if (MW.db_get_total_service_count() <= 0) {
                        return true;
                    } else {
                        this.btimeshift = true;
                        if (this.storageDeviceInfo == null) {
                            this.btimeshift = false;
                            return true;
                        } else if (this.storageDeviceInfo.getDeviceCount() > 0) {
                            Log.e("DtvMainActivity", "have device : " + this.storageDeviceInfo.getDeviceCount());
                            if (this.storageDeviceInfo.getDeviceCount() <= 1) {
                                this.device_no = 0;
                                if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                                    this.current_service_type = this.serviceInfo.service_type;
                                    this.current_service_index = this.serviceInfo.service_index;
                                    this.current_record_service_region_index = this.serviceInfo.region_index;
                                    this.current_record_service_tp_index = this.serviceInfo.tp_index;
                                    if (this.enableRecord) {
                                        this.ftimelimit = true;
                                        if (SETTINGS.getTimeShiftLengthIndex() == 4) {
                                            ShowRecordTimeEditTextView();
                                            return true;
                                        }
                                        DoTimeShiftFunction(this.device_no, SETTINGS.getTimeShiftTime());
                                        return true;
                                    }
                                    SETTINGS.makeText(this, R.string.channel_no_play_cant_timeshift, 0);
                                    this.btimeshift = false;
                                    return true;
                                }
                                SETTINGS.makeText(this, R.string.nosignalcanttimeshift, 0);
                                this.btimeshift = false;
                                return true;
                            } else if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                                this.current_service_type = this.serviceInfo.service_type;
                                this.current_service_index = this.serviceInfo.service_index;
                                this.current_record_service_region_index = this.serviceInfo.region_index;
                                this.current_record_service_tp_index = this.serviceInfo.tp_index;
                                if (this.enableRecord) {
                                    this.DeviceInfoDia = new StorageDeviceDialog(this, R.style.MyDialog);
                                    this.DeviceInfoDia.show();
                                    attributes = this.DeviceInfoDia.getWindow().getAttributes();
                                    attributes.dimAmount = 0.0f;
                                    this.DeviceInfoDia.getWindow().setAttributes(attributes);
                                    this.DeviceInfoDia.getWindow().addFlags(2);
                                    return true;
                                }
                                SETTINGS.makeText(this, R.string.channel_no_play_cant_timeshift, 0);
                                this.btimeshift = false;
                                return true;
                            } else {
                                SETTINGS.makeText(this, R.string.nosignalcanttimeshift, 0);
                                this.btimeshift = false;
                                return true;
                            }
                        } else {
                            SETTINGS.makeText(this, R.string.no_storage, 0);
                            Log.e("DtvMainActivity", "not device");
                            this.btimeshift = false;
                            return true;
                        }
                    }
                } else if (!this.bInit_timeShift || this.timeshift == null) {
                    System.out.println("timeShift_PlayStatus111 : " + this.timeShift_PlayStatus);
                    return true;
                } else {
                    System.out.println("timeShift_PlayStatus : " + this.timeShift_PlayStatus);
                    if (this.timeShift_PlayStatus == 4) {
                        this.bhandlemediakey = true;
                        MW.pvr_playback_pause();
                        this.timeShift_PlayStatus = 3;
                        this.timeshift_speed = 0;
                        this.timeshift.hideTimeshiftInfoView(0);
                        this.timeshift.showTimeshiftInfoView();
                        ShowTimeShiftSeekTimeSetDialog();
                        return true;
                    } else if (this.timeShift_PlayStatus == 3) {
                        MW.pvr_timeshift_resume();
                        this.timeShift_PlayStatus = 4;
                        this.timeshift_speed = 0;
                        this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                        return true;
                    } else if (this.timeShift_PlayStatus == 2) {
                        MW.pvr_timeshift_fast_forward(0);
                        this.timeShift_PlayStatus = 4;
                        this.timeshift_speed = 0;
                        this.special_case = true;
                        this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                        return true;
                    } else if (this.timeShift_PlayStatus != 1) {
                        return true;
                    } else {
                        MW.pvr_timeshift_fast_backward(0);
                        this.timeShift_PlayStatus = 4;
                        this.timeshift_speed = 0;
                        this.special_case = true;
                        this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                        return true;
                    }
                }
            case DtvBaseActivity.KEYCODE_MEDIA_STOP /*86*/:
                if (!this.bInit_timeShift || this.timeshift == null) {
                    if (!this.bInRecording) {
                        return true;
                    }
                    ShowRecordAlertDialog();
                    return true;
                } else if (!this.bInRecording) {
                    return true;
                } else {
                    ShowRecordAlertDialog();
                    return true;
                }
            case DtvBaseActivity.KEYCODE_MEDIA_REWIND /*89*/:
                if (!this.bInit_timeShift || this.timeshift == null) {
                    return true;
                }
                Log.e("LEE", "KEYCODE_MEDIA_REWIND timeShift_palystatus =========" + this.timeShift_PlayStatus);
                this.one_time = false;
                if (this.timeShift_PlayStatus == 2) {
                    this.timeShift_PlayStatus = 4;
                    this.timeshift_speed = 0;
                    this.Temp_timeShift_PlayStatus = 5;
                    Log.e("LEE", "2 tmep timeShift_palystatus =========" + this.Temp_timeShift_PlayStatus);
                    MW.pvr_timeshift_fast_forward(this.timeshift_speed);
                    this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                    return true;
                }
                this.bhandlemediakey = true;
                this.timeShift_PlayStatus = 1;
                if (this.timeshift_speed == 0) {
                    this.timeshift_speed = 2;
                } else {
                    this.timeshift_speed *= 2;
                }
                if (this.timeshift_speed > 32) {
                    this.timeshift_speed = 0;
                    this.timeShift_PlayStatus = 4;
                    MW.pvr_timeshift_fast_backward(this.timeshift_speed);
                    this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                } else {
                    Log.e("LEE", "KEYCODE_MEDIA_REWIND timeshift_speed =========" + this.timeshift_speed);
                    this.timeShift_PlayStatus = 1;
                    MW.pvr_timeshift_fast_backward(this.timeshift_speed);
                    this.timeshift.hideTimeshiftInfoView(0);
                    this.timeshift.showTimeshiftInfoView();
                }
                MW.register_event_type(0, false);
                return true;
            case DtvBaseActivity.KEYCODE_MEDIA_FAST_FORWARD /*90*/:
                if (!this.bInit_timeShift || this.timeshift == null) {
                    return true;
                }
                Log.e("LEE", "KEYCODE_MEDIA_FAST_FORWARD timeShift_palystatus =========" + this.timeShift_PlayStatus);
                this.one_time = false;
                if (this.timeShift_PlayStatus == 1) {
                    this.timeShift_PlayStatus = 4;
                    this.timeshift_speed = 0;
                    this.Temp_timeShift_PlayStatus = 5;
                    Log.e("LEE", "1 tmep timeShift_palystatus =========" + this.Temp_timeShift_PlayStatus);
                    MW.pvr_timeshift_fast_forward(this.timeshift_speed);
                    this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                    return true;
                }
                this.timeShift_PlayStatus = 2;
                if (this.timeshift_speed == 0) {
                    this.timeshift_speed = 2;
                } else {
                    this.timeshift_speed *= 2;
                }
                if (this.timeshift_speed > 32) {
                    this.timeshift_speed = 0;
                    this.timeShift_PlayStatus = 4;
                    MW.pvr_timeshift_fast_forward(this.timeshift_speed);
                    this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                } else {
                    Log.e("LEE", "KEYCODE_MEDIA_FAST_FORWARD timeshift_speed =========" + this.timeshift_speed);
                    this.timeShift_PlayStatus = 2;
                    MW.pvr_timeshift_fast_forward(this.timeshift_speed);
                    this.timeshift.hideTimeshiftInfoView(0);
                    this.timeshift.showTimeshiftInfoView();
                }
                MW.register_event_type(0, false);
                return true;
            case DtvBaseActivity.KEYCODE_BLUE /*140*/:
            case DtvBaseActivity.KEYCODE_TV_RADIO /*555*/:
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                }
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                switch (MW.ts_player_play_switch_tv_radio(false)) {
                    case MW.VIDEO_STREAM_TYPE_MPEG2 /*0*/:
                        showInfoBar();
                        RefreshTvRadioBackground();
                        CheckParentLockAndShowPasswd();
                        return true;
                    case MW.VIDEO_STREAM_TYPE_H264 /*1*/:
                        ShowTmpPlayStatus(getString(R.string.NO_CHANNEL));
                        return true;
                    case MW.SEARCH_STATUS_SAVE_DATA_START /*7*/:
                        ShowTmpPlayStatus(getString(R.string.NO_VIDEO_CHANNEL));
                        return true;
                    case MW.SERVICE_SORT_TYPE_CAS /*8*/:
                        ShowTmpPlayStatus(getString(R.string.NO_AUDIO_CHANNEL));
                        return true;
                    case MW.RET_SERVICE_LOCK /*44*/:
                        showInfoBar();
                        RefreshTvRadioBackground();
                        CheckChannelLockAndShowPasswd("16");
                        return true;
                    default:
                        return true;
                }
            case DtvBaseActivity.KEYCODE_INFO /*185*/:
                if (!this.bInit_timeShift) {
                    if (MW.db_get_total_service_count() > 0) {
                        if (this.rl_infoBar.getVisibility() != 0) {
                            showInfoBar();
                        } else {
                            ShowCurrentServiceInfo();
                        }
                    }
                    System.out.println("KEYCODE_INFO");
                    return true;
                } else if (this.timeshift == null) {
                    return true;
                } else {
                    if (this.timeshift.isTimeshiftInfoBarShow()) {
                        System.out.println("isTimeshiftInfoBarShow == true");
                        this.timeshift.hideTimeshiftInfoView(0);
                        return true;
                    }
                    System.out.println("isTimeshiftInfoBarShow == false");
                    this.timeshift.showTimeshiftInfoView();
                    this.timeshift.hideTimeshiftInfoView(SETTINGS.get_banner_time());
                    return true;
                }
            case DtvBaseActivity.KEYCODE_RECALL /*666*/:
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                }
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                if (MW.db_get_current_service_info(this.serviceInfo) != 0) {
                    return true;
                }
                i2 = this.serviceInfo.service_type;
                ts_player_play_up = MW.ts_player_play_previous(false);
                if (ts_player_play_up == 0) {
                    if (this.rl_channelList.getVisibility() != 0) {
                        showInfoBar();
                        RefreshTvRadioBackground();
                    } else if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                        RefreshShowChannelList();
                    }
                    CheckParentLockAndShowPasswd();
                    return true;
                } else if (ts_player_play_up != 44) {
                    return true;
                } else {
                    if (this.rl_channelList.getVisibility() != 0) {
                        showInfoBar();
                        RefreshTvRadioBackground();
                    } else if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                        RefreshShowChannelList();
                    }
                    CheckChannelLockAndShowPasswd("17");
                    return true;
                }
            case DtvBaseActivity.KEYCODE_TELETEXT /*2004*/:
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                if (this.rl_loading.getVisibility() == 0 || MW.db_get_current_service_info(this.serviceInfo) != 0 || this.serviceInfo.teletext_info.length <= 0) {
                    return true;
                }
                if (this.serviceInfo.teletext_info.length == 1) {
                    Log.i("LEE", "startTeletextSubtitle(true, 0, serviceInfo)");
                    stopTeletextSubtitle();
                    startTeletextSubtitle(true, 0, this.serviceInfo);
                    return true;
                }
                TeletextSubtitleInfoDialog teletextSubtitleInfoDialog = new TeletextSubtitleInfoDialog(this, R.style.MyDialog, true);
                teletextSubtitleInfoDialog.show();
                attributes2 = teletextSubtitleInfoDialog.getWindow().getAttributes();
                attributes2.dimAmount = 0.0f;
                teletextSubtitleInfoDialog.getWindow().setAttributes(attributes2);
                teletextSubtitleInfoDialog.getWindow().addFlags(2);
                if (!this.inTeletextMode) {
                    return true;
                }
                stopTeletextSubtitle();
                return true;
            case DtvBaseActivity.KEYCODE_AUDIO /*2006*/:
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                if (MW.db_get_current_service_info(this.serviceInfo) != 0) {
                    return true;
                }
                AudioInfoDialog audioInfoDialog = new AudioInfoDialog(this, R.style.MyDialog);
                audioInfoDialog.show();
                attributes2 = audioInfoDialog.getWindow().getAttributes();
                attributes2.dimAmount = 0.0f;
                audioInfoDialog.getWindow().setAttributes(attributes2);
                audioInfoDialog.getWindow().addFlags(2);
                if (!this.inTeletextMode) {
                    return true;
                }
                stopTeletextSubtitle();
                return true;
            case DtvBaseActivity.KEYCODE_SUBTITLE /*2012*/:
                if (this.rl_channelNumber.getVisibility() != 4) {
                    CloseChannelNumber();
                }
                if (MW.db_get_current_service_info(this.serviceInfo) != 0 || this.serviceInfo.subtitle_info.length <= 0) {
                    return true;
                }
                TeletextSubtitleInfoDialog teletextSubtitleInfoDialog2 = new TeletextSubtitleInfoDialog(this, R.style.MyDialog, false);
                teletextSubtitleInfoDialog2.show();
                attributes = teletextSubtitleInfoDialog2.getWindow().getAttributes();
                attributes.dimAmount = 0.0f;
                teletextSubtitleInfoDialog2.getWindow().setAttributes(attributes);
                teletextSubtitleInfoDialog2.getWindow().addFlags(2);
                if (!this.inTeletextMode) {
                    return true;
                }
                stopTeletextSubtitle();
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        switch (i) {
            case DtvBaseActivity.KEYCODE_MENU /*82*/:
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                }
                MainMenuSetup();
                return true;
            case DtvBaseActivity.KEYCODE_EPG /*2013*/:
                if (this.bInRecording) {
                    ShowRecordAlertDialog();
                    return true;
                }
                this.bGotoEPGWin = true;
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.putExtra("LauncherFormDtv", true);
                intent.setClass(this, EPGActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onKeyUp(i, keyEvent);
        }
    }

    private void initLayoutView() {
        this.mainwnd_bg = (ImageView) findViewById(R.id.mainwnd_bg);
        this.rl_channelList = (RelativeLayout) findViewById(R.id.RelativeLayout_ChannelList);
        rl_playStatusBar = (RelativeLayout) findViewById(R.id.RelativeLayout_PlayStatusBar);
        this.rl_infoBar = (RelativeLayout) findViewById(R.id.RelativeLayout_InfoBar);
        this.rl_loading = (RelativeLayout) findViewById(R.id.RelativeLayout_Loading);
        this.rl_channelNumber = (RelativeLayout) findViewById(R.id.RelativeLayout_ChannelNumber);
        rl_encryptionStatusBar = (RelativeLayout) findViewById(R.id.RelativeLayout_EncryptionStatusBar);
    }

    private void initVideoView() {
        this.videoView = (VideoView) findViewById(R.id.VideoView);
        this.videoView.setVisibility(View.VISIBLE);
        this.videoView.getHolder().addCallback(this.mSHCallback);
        SETTINGS.videoViewSetFormat(this, this.videoView);
    }

    private void initttx_subView() {
        this.ttx_sub = (TeletextSubtitleView) findViewById(R.id.TeletextSubtitleView);
        this.ttx_sub.setLayerType(1, null);
        this.et_ttxPageNumber = (EditText) findViewById(R.id.EditText_TeletextPageNumber);
        this.et_ttxPageNumber.setOnKeyListener(this.mEditOnKeyListener);
    }

    private void initMessageHandler() {
        this.mwMsgHandler = new MWmessageHandler(this.looper);
        this.localMsgHandler = new LOCALMessageHandler(this.looper);
    }

    private void initPlayStatusView() {
        this.txv_playStatus = (TextView) findViewById(R.id.TextView_PlayStatus);
    }

    private void initEncryptionStatusView() {
        this.txv_encrytionStatus = (TextView) findViewById(R.id.TextView_EncryptionStatus);
    }

    private void initInfoBar() {
        this.txv_channelNumber = (TextView) findViewById(R.id.TextView_ChannelNumber);
        this.txv_infoBar_channelName = (TextView) findViewById(R.id.live_control_main_channelname_txt);
        this.txv_infoBar_channelNum = (TextView) findViewById(R.id.live_control_main_channelnum_txt);
        this.txv_infoBar_currentEpg = (TextView) findViewById(R.id.live_control_main_channelepg1_txt);
        this.txv_infoBar_nextEpg = (TextView) findViewById(R.id.live_control_main_channelepg2_txt);
        this.txv_infBar_dateTime = (TextView) findViewById(R.id.current_datetime);
        this.txv_infoBar_signalStrength = (TextView) findViewById(R.id.live_control_main_strength_txt);
        this.txv_infoBar_signalQuality = (TextView) findViewById(R.id.live_control_main_quality_txt);
        this.gifbuf = (ImageView) findViewById(R.id.live_control_main_bufgif_img);
        this.anim = (AnimationDrawable) this.gifbuf.getDrawable();
        this.prg_infoBar_signalStrength = (ProgressBar) findViewById(R.id.live_control_main_strength_prgbar);
        this.prg_infoBar_signalStrength.setMax(100);
        this.prg_infoBar_signalQuality = (ProgressBar) findViewById(R.id.live_control_main_quality_prgbar);
        this.prg_infoBar_signalQuality.setMax(100);
        this.prg_infoBar_signalStrength.setProgress(0);
        this.prg_infoBar_signalQuality.setProgress(0);
        this.infoBar_teletext = (TextView) findViewById(R.id.InfoBar_Teletext);
        this.infoBar_subtitle = (TextView) findViewById(R.id.InfoBar_Subtitle);
        this.infoBar_hd = (TextView) findViewById(R.id.InfoBar_HD);
        this.infoBar_scramble = (TextView) findViewById(R.id.InfoBar_Scramble);
        this.epg_time_progress = (ProgressBar) findViewById(R.id.epg_time_prgbar);
    }

    private void ShowEpgTimeProg(int max, int progress) {
        this.anim.stop();
        this.gifbuf.setVisibility(View.GONE);
        this.epg_time_progress.setVisibility(View.VISIBLE);
        this.epg_time_progress.setMax(max);
        this.epg_time_progress.setProgress(progress);
    }

    private void HideEpgTimeProg() {
        this.anim.start();
        this.gifbuf.setVisibility(View.VISIBLE);
        this.epg_time_progress.setVisibility(View.GONE);
    }

    private void initChannelListView() {
        this.txv_listTitle = (TextView) findViewById(R.id.channellist_layout_channeltype_txt);
        this.channel_list = (CustomListView) findViewById(R.id.channel_list_listview);
        this.mChannelListListener = new listOnKeyListener();
        this.channelListAdapter = new ChannelListAdapter(this);
        this.channel_list.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.channel_list.setCustomScrollListener(new listOnScroll());
        this.channel_list.setCustomKeyListener(this.mChannelListListener);
        this.channel_list.setVisibleItemCount(10);
        this.channel_list.setAdapter(this.channelListAdapter);
        this.channel_list.setChoiceMode(1);
    }

    private void listview_Animation() {
        this.rl_channelList.startAnimation(AnimationUtils.loadAnimation(this, R.anim.live_channellist_enter_anim));
    }

    private void listview_AnimationExit() {
        this.rl_channelList.startAnimation(AnimationUtils.loadAnimation(this, R.anim.live_channellist_exit_anim));
    }

    private void initPvrInfoView() {
        this.ll_pvrInfo = (LinearLayout) findViewById(R.id.pvr_ll);
        this.pvr_time_txt = (TextView) findViewById(R.id.pvr_time_txt);
        this.pvr_time_txt.setVisibility(View.INVISIBLE);
        this.ll_pvrInfo.setVisibility(View.INVISIBLE);
    }

    private void HidePvrView() {
        this.ll_pvrInfo.setVisibility(View.INVISIBLE);
    }

    private void initView() {
        initLayoutView();
        initVideoView();
        initttx_subView();
        initInfoBar();
        initPlayStatusView();
        initEncryptionStatusView();
        initChannelListView();
        initPvrInfoView();
        RefreshTvRadioBackground();
    }

    private void initData() {
//        this.favgroupname = SETTINGS.getFavGroupListArray();
        this.favgroupname = new String[]{"aaa"};
        initMessageHandler();
        if (MW.db_check_service_in_fav_mode() && MW.db_get_service_available_fav_group_count() == 0) {
            MW.db_service_set_in_region_mode();
        }
        if (MW.db_get_total_service_count() > 0) {
            SETTINGS.showdialog = false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
//        DtvApp.getInstance().exit();
        Intent intent = new Intent();
        intent.setAction("com.android.music.musicservicecommand.pause");
        intent.putExtra("command", "stop");
//        sendBroadcastAsUser(intent, UserHandle.ALL);
        setContentView(R.layout.dtv_main);
        System.out.println("temp_current_service_index1  : " + this.temp_current_service_index);
        this.current_service_type = getIntent().getIntExtra("book_service_type", 0);
        this.temp_current_service_index = getIntent().getIntExtra("book_service_Index", -1);
        this.book_type = getIntent().getIntExtra("book_type", 1);
        this.is_all_region = getIntent().getBooleanExtra("is_all_region", false);
        this.record_time = getIntent().getIntExtra("book_record_time", 0);
        this.rec_pre_service_type = getIntent().getIntExtra("rec_pre_service_type", 0);
        this.rec_pre_service_index = getIntent().getIntExtra("rec_service_Index", -1);
        Log.i("DtvMainActivity", "rec_pre_service_type = " + this.rec_pre_service_type + ", rec_pre_service_index = " + this.rec_pre_service_index);
        System.out.println("temp_current_service_index2  : " + this.temp_current_service_index);
        this.bChangetoAll = getIntent().getBooleanExtra("change_to_all", false);
        this.bChangetoNew = getIntent().getBooleanExtra("change_to_new", false);
        if (getIntent().getBooleanExtra("LaunchByDirectLink", false)) {
            SETTINGS.setVideoWindow(this);
        }
        Log.d("DtvMainActivity", " onCreate");
    }

    protected void onStart() {
        super.onStart();
        Log.d("DtvMainActivity", " onStart");
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("DtvMainActivity", " onNewIntent");
    }

    protected void onPause() {
        super.onPause();
        this.videoView.setVisibility(View.INVISIBLE);
        this.ftimelimit = false;
        if (this.bInit_timeShift) {
            disableShowPlayStatus = true;
            RemoveDeviceIsFullMsg();
            MW.pvr_timeshift_stop();
            if (this.timeshift != null) {
                this.timeshift.dismiss();
            }
            MW.ts_player_stop_play();
            MW.ts_player_play_current(true, false);
            disableShowPlayStatus = false;
            this.bInit_timeShift = false;
            this.btimeshift = false;
            this.bhandlemediakey = false;
        }
        this.storageDeviceInfo.finish(this);
        if (this.storageDevicePlugReceiver != null) {
            unregisterReceiver(this.storageDevicePlugReceiver);
            this.storageDevicePlugReceiver = null;
        }
        if (this.bInRecording) {
            this.bInRecording = false;
            this.pvr_time_txt.setVisibility(View.INVISIBLE);
            this.ll_pvrInfo.setVisibility(View.INVISIBLE);
            RemoveDeviceIsFullMsg();
            MW.pvr_record_stop();
        }
        if (this.recorddialog != null && this.recorddialog.isShowing()) {
            this.recorddialog.dismiss();
        }
        stopTeletextSubtitle();
        this.rl_channelList.setVisibility(View.GONE);
        rl_playStatusBar.setVisibility(View.INVISIBLE);
        CloseInfoBarDelayed(0);
        CloseTmpPlayStatusInfo(-1);
        CloseChannelNumber();
        if (this.dia_currentDialog != null) {
            this.dia_currentDialog.dismiss();
        }
        if (!this.bGotoEPGWin) {
            MW.ts_player_stop_play();
        }
        enableMwMessageCallback(null);
        Log.d("DtvMainActivity", " onPause");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d("DtvMainActivity", " onRestart");
    }

    protected void onResume() {
        int ts_player_play;
        super.onResume();
        SETTINGS.bPass = false;
        GetSystemType();
        initData();
        initView();
        SETTINGS.set_screen_mode();
        SETTINGS.set_timezone_value(0);
        SETTINGS.set_video_enable(true);
        this.bGotoEPGWin = false;
        disableShowPlayStatus = false;
        enableMwMessageCallback(this.mwMsgHandler);
        MW.register_event_type(0, true);
        MW.register_event_type(2, true);
        MW.register_event_type(4, true);
        MW.register_event_type(5, true);
        this.storageDeviceInfo = new StorageDevice(this);
        this.storageDevicePlugReceiver = new StorageDevicePlugReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_MOUNTED");
        intentFilter.addAction("android.intent.action.DEVICE_REMOVED");
        registerReceiver(this.storageDevicePlugReceiver, intentFilter);
        if (this.bChangetoAll) {
            MW.db_service_set_in_region_mode();
            MW.db_set_service_region_current_pos(0);
            this.bChangetoAll = false;
        } else if (this.bChangetoNew) {
            MW.db_service_set_in_region_mode();
            MW.db_set_service_region_current_pos(0);
            this.bChangetoNew = false;
        }
        if (this.temp_current_service_index != -1) {
            if (this.is_all_region) {
                MW.db_service_set_in_region_mode();
                MW.db_set_service_region_current_pos(0);
            }
            this.book_enableRecord = true;
            if (this.bInRecording) {
                MW.pvr_record_stop();
                this.bInRecording = false;
            }
            if (this.bInit_timeShift) {
                RemoveDeviceIsFullMsg();
                MW.pvr_timeshift_stop();
                if (this.timeshift != null) {
                    this.timeshift.dismiss();
                }
                MW.ts_player_stop_play();
                MW.ts_player_play_current(true, false);
                this.btimeshift = false;
            }
            ts_player_play = MW.ts_player_play(this.current_service_type, this.temp_current_service_index, 0, false);
            System.out.println("Ret 1 : " + ts_player_play);
            if (ts_player_play == 0) {
                RefreshTvRadioBackground();
                CheckParentLockAndShowPasswd();
            } else if (ts_player_play == 44) {
                RefreshTvRadioBackground();
                CheckChannelLockAndShowPasswd("22");
            }
            this.temp_current_service_index = -1;
            if (this.book_type == 1) {
                this.book_enableRecord = false;
            } else if (this.book_type == 2) {
                if (this.book_enableRecord) {
                    this.book_enableRecord = false;
                    if (this.storageDeviceInfo != null) {
                        if (this.storageDeviceInfo.getDeviceCount() == 0) {
                            SETTINGS.makeText(this, R.string.no_storage, 1);
                        } else {
                            this.device_no = 0;
                            if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
                                this.current_service_type = this.serviceInfo.service_type;
                                this.current_service_index = this.serviceInfo.service_index;
                                this.current_record_service_region_index = this.serviceInfo.region_index;
                                this.current_record_service_tp_index = this.serviceInfo.tp_index;
                                System.out.println("record time " + this.record_time);
                                this.storageDeviceInfo.refreshDevice();
                                String str = this.storageDeviceInfo.getDeviceItem(this.device_no).spare;
                                if (str != null) {
                                    long spareSize = StorageDevice.getSpareSize(str);
                                    if (spareSize != -1 && spareSize < 2097152) {
                                        ShowToastInformation(getResources().getString(R.string.spaceisfull), 0);
                                        return;
                                    }
                                }
                                stopTeletextSubtitle();
                                if (!SETTINGS.bHongKongDTMB || this.epgCurrentEvent.event_name.length() <= 0) {
                                    ts_player_play = MW.pvr_record_start(this.current_service_type, this.current_service_index, this.record_time * 1000, this.storageDeviceInfo.getDeviceItem(this.device_no).Path, null);
                                } else {
                                    ts_player_play = MW.pvr_record_start(this.current_service_type, this.current_service_index, this.record_time * 1000, this.storageDeviceInfo.getDeviceItem(this.device_no).Path, this.epgCurrentEvent.event_name);
                                }
                                if (ts_player_play == 0) {
                                    this.sCurrentRecordStoreDir = new String(this.storageDeviceInfo.getDeviceItem(this.device_no).Path);
                                    this.bInRecording = true;
                                    SETTINGS.makeText(this, R.string.start_record, 1);
                                    SendDeviceIsFullMsg();
                                    this.pvr_time_txt.setVisibility(View.VISIBLE);
                                    this.ll_pvrInfo.setVisibility(View.VISIBLE);
                                    Log.e("DtvMainActivity", "start recording....");
                                } else if (ts_player_play == 41) {
                                    Log.e("DtvMainActivity", "already recording....");
                                    this.bInRecording = true;
                                    SETTINGS.makeText(this, R.string.already_record, 1);
                                } else if (ts_player_play == 6) {
                                    Log.e("DtvMainActivity", "record failed, not this channel....");
                                    SETTINGS.makeText(this, R.string.record_failed_no_this_channel, 1);
                                } else if (ts_player_play == 30) {
                                    Log.e("DtvMainActivity", "record failed, invalid object....");
                                    SETTINGS.makeText(this, R.string.record_failed_invalid_object, 1);
                                }
                                refreshTeletextSubtitle();
                            } else {
                                SETTINGS.makeText(this, R.string.channel_no_play_cant_record, 1);
                            }
                        }
                    }
                } else {
                    SETTINGS.makeText(this, R.string.channel_is_locked, 1);
                }
            }
        }
        if (MW.db_get_current_service_info(this.serviceInfo) == 0) {
            this.current_service_type = this.serviceInfo.service_type;
            this.current_service_index = this.serviceInfo.service_index;
            this.current_record_service_region_index = this.serviceInfo.region_index;
            this.current_record_service_tp_index = this.serviceInfo.tp_index;
        }
        ts_player_play = MW.ts_player_play_current(false, false);
        if (ts_player_play == 0) {
            showInfoBar();
            if (SETTINGS.bPass) {
                MW.ts_player_play_current(true, false);
            } else {
                CheckParentLockAndShowPasswd();
            }
        } else if (ts_player_play == 44) {
            showInfoBar();
            if (SETTINGS.bPass) {
                MW.ts_player_play_current(true, false);
            } else {
                CheckChannelLockAndShowPasswd("20");
            }
        }
        Log.d("DtvMainActivity", " onResume");
    }

    private void GetSystemType() {
        this.SystemType = MW.get_system_tuner_config();
    }

    protected void onStop() {
        super.onStop();
        Log.d("DtvMainActivity", " onStop");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d("DtvMainActivity", " onDestroy");
        this.anim.stop();
    }

    void ShowToastInformation(String str, int i) {
        if (this.bInRecording) {
            LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.toast_main, null);
            TextView textView = (TextView) linearLayout.findViewById(R.id.toast_text);
            textView.setText(str);
            if (this.toast != null) {
                textView.setText(str);
            } else {
                this.toast = new Toast(this);
            }
            this.toast.setView(linearLayout);
            this.toast.setDuration(i);
            this.toast.setGravity(17, 0, -70);
            this.toast.show();
        }
    }

    private void PowerDown() {
        if (SETTINGS.getPlatformType() == 0) {
            Intent intent = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
            intent.putExtra("android.intent.extra.KEY_CONFIRM", false);
            intent.setFlags(268435456);
            startActivity(intent);
        } else if (SETTINGS.getPlatformType() == 1) {
            try {
                Runtime.getRuntime().exec("input keyevent POWER");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void ShowBookEndCountdownDialog() {
        if (this.countdownDialog == null) {
            this.countdownDialog = new CountdownDialog(this, R.style.MyDialog);
            this.countdownDialog.getWindow().setType(2003);
            startCountDownTimer(60000);
        } else {
            Log.d("DtvMainActivity", "countdownDialog is not null");
        }
        this.countdownDialog.show();
    }

    private void startCountDownTimer(long j) {
        this.counterTimer = new AnonymousClass11(j, 1000);
        this.counterTimer.start();
        Log.d("DtvMainActivity", "startCountDownTimer");
    }
}
