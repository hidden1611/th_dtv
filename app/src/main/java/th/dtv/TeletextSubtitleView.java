package th.dtv;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import th.dtv.mw_data.pvr_record;
import th.dtv.mw_data.service;

public class TeletextSubtitleView extends View {
    private static Bitmap bmp;
    private static Object lock;
    private static Paint f1p;
    private static Rect rectDest;
    private static Rect rectSrc;
    private boolean isTeletextMode;
    private boolean isTeletextSubtitle;
    private service mServiceInfo;
    private boolean visible;

    static {
        bmp = Bitmap.createBitmap(1920, 1080, Config.ARGB_8888);
        lock = new Object();
        f1p = new Paint();
        rectSrc = new Rect();
        rectDest = new Rect();
    }

    public void show() {
        synchronized (lock) {
            if (!this.visible) {
                this.visible = true;
            }
        }
    }

    public void hide() {
        synchronized (lock) {
            if (this.visible) {
                this.visible = false;
            }
        }
    }

    public void update() {
        synchronized (lock) {
            if (!this.visible) {
                setVisibility(View.GONE);
            } else if (getVisibility() != 0) {
                setVisibility(View.VISIBLE);
            }
            postInvalidate();
        }
    }

    public TeletextSubtitleView(Context context) {
        super(context);
        this.isTeletextMode = false;
        this.isTeletextSubtitle = false;
        this.mServiceInfo = new service();
        this.visible = false;
    }

    public TeletextSubtitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.isTeletextMode = false;
        this.isTeletextSubtitle = false;
        this.mServiceInfo = new service();
        this.visible = false;
    }

    public TeletextSubtitleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.isTeletextMode = false;
        this.isTeletextSubtitle = false;
        this.mServiceInfo = new service();
        this.visible = false;
    }

    public void start(boolean bTeletext, int ttxSubIndex, service serviceInfo) {
        synchronized (lock) {
            int ret;
            boolean bTtxSub = false;
            if (bTeletext) {
                ret = MW.teletext_start(serviceInfo.teletext_info[ttxSubIndex].pid, serviceInfo.teletext_info[ttxSubIndex].teletext_magazine_number, serviceInfo.teletext_info[ttxSubIndex].teletext_page_number, bmp);
            } else {
                if (serviceInfo.subtitle_info[ttxSubIndex].subtitling_type == 2 || serviceInfo.subtitle_info[ttxSubIndex].subtitling_type == 5) {
                    bTtxSub = true;
                }
                ret = MW.subtitle_start(serviceInfo.subtitle_info[ttxSubIndex].pid, bTtxSub, serviceInfo.subtitle_info[ttxSubIndex].composition_page_id, serviceInfo.subtitle_info[ttxSubIndex].ancillary_page_id, bmp);
            }
            if (ret == 0) {
                this.isTeletextMode = bTeletext;
                if (!bTeletext) {
                    this.isTeletextSubtitle = bTtxSub;
                }
                show();
            } else {
                hide();
            }
            setVisibility(View.GONE);
        }
    }

    public void start(boolean bTeletext, int ttxSubIndex, pvr_record pvrRecordInfo) {
        synchronized (lock) {
            int ret;
            boolean bTtxSub = false;
            if (bTeletext) {
                ret = MW.teletext_start(pvrRecordInfo.teletext_info[ttxSubIndex].pid, pvrRecordInfo.teletext_info[ttxSubIndex].teletext_magazine_number, pvrRecordInfo.teletext_info[ttxSubIndex].teletext_page_number, bmp);
            } else {
                if (pvrRecordInfo.subtitle_info[ttxSubIndex].subtitling_type == 2 || pvrRecordInfo.subtitle_info[ttxSubIndex].subtitling_type == 5) {
                    bTtxSub = true;
                }
                ret = MW.subtitle_start(pvrRecordInfo.subtitle_info[ttxSubIndex].pid, bTtxSub, pvrRecordInfo.subtitle_info[ttxSubIndex].composition_page_id, pvrRecordInfo.subtitle_info[ttxSubIndex].ancillary_page_id, bmp);
            }
            if (ret == 0) {
                this.isTeletextMode = bTeletext;
                if (!bTeletext) {
                    this.isTeletextSubtitle = bTtxSub;
                }
                show();
            } else {
                hide();
            }
            setVisibility(View.GONE);
        }
    }

    public void stop() {
        synchronized (lock) {
            if (this.isTeletextMode) {
                MW.teletext_stop();
            } else {
                MW.subtitle_stop();
            }
            hide();
            if (getVisibility() != 8) {
                setVisibility(View.GONE);
            }
        }
    }

    public void onDraw(Canvas canvas) {
        synchronized (lock) {
            if (this.visible && bmp != null) {
                int x = 0;
                int y = 0;
                int w = 0;
                int h = 0;
                int sw = 0;
                int sh = 0;
                f1p.setAntiAlias(true);
                if (this.isTeletextMode) {
                    f1p.setAlpha(160);
                } else {
                    f1p.setAlpha(255);
                }
                MW.subtitle_lock();
                if (!this.isTeletextMode && !this.isTeletextSubtitle) {
                    x = 0;
                    y = 0;
                    w = getWidth();
                    h = getHeight();
                    sw = MW.subtitle_get_picture_width();
                    sh = MW.subtitle_get_picture_height();
                } else if (SETTINGS.getPlatformType() == 0) {
                    x = 70;
                    y = 60;
                    w = getWidth() - 80;
                    h = getHeight() - 30;
                    sw = 492;
                    sh = 250;
                } else if (SETTINGS.getPlatformType() == 1) {
                    x = 100;
                    y = 90;
                    w = getWidth() - 100;
                    h = getHeight() - 80;
                    sw = 492;
                    sh = 250;
                }
                rectSrc.left = 0;
                rectSrc.top = 0;
                rectSrc.right = sw;
                rectSrc.bottom = sh;
                rectDest.left = x;
                rectDest.top = y;
                rectDest.right = w;
                rectDest.bottom = h;
                canvas.drawBitmap(bmp, rectSrc, rectDest, f1p);
                MW.subtitle_unlock();
            }
        }
    }

    protected void onAttachedToWindow() {
        synchronized (lock) {
            super.onAttachedToWindow();
        }
    }

    protected void onDetachedFromWindow() {
        synchronized (lock) {
            super.onDetachedFromWindow();
        }
    }
}
